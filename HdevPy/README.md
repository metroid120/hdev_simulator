## About

This is the HdevPy Python package. It allows to interact with the Hdev simulator in a convenient way. 

## Installation

In the folder HdevPy, where the file setup.py exists, run:

    pip install -e . 

## License 

HdevPy is licensed under the GPL v3.

