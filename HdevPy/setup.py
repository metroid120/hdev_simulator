from setuptools import setup

setup(
    name="hdevpy",
    version="0.1.0",
    author="Markus Müller",
    author_email="metroid120@googlemail.com",
    packages=["hdevpy", "hdevpy.testing"],
    scripts=["scripts/run_tests.py"],
    url="https://gitlab.com/metroid120/hdev_simulator",
    license="LICENSE.txt",
    description="The Hdev python interface.",
    long_description=open("README.md").read(),
    install_requires=[
        "numpy",
        "DMT-core",
    ],
)
