"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
name = "materials"


# materials
from .material_database import Si
from .material_database import SS
from .material_database import Ge
from .material_database import GS
from .material_database import InAs
from .material_database import GaAs
from .material_database import GaSb
from .material_database import InP
from .material_database import InGaAs
from .material_database import GaAsSb
from .material_database import SiGe
from .material_database import SSGS
from .material_database import materials
from .material_database import ALLOYS
