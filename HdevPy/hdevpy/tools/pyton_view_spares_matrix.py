"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


"""
This script can be used to visualize a sparse matrix from hdev. 
"""

from DMT.core import read_elpa, DataFrame
import scipy
from PIL import Image
import numpy as np
import sys

df = read_elpa("dut_op2_f_psi_dphi_niter003_dd_debug.elpa")  # read in

dim_row = int(df["i_row"].max() - 1)
dim_col = int(df["i_col"].max())
data, indices, indptr = (
    df["values"].values,
    df["i_col"].values - 1,
    np.array([i for i in df["i_row"].values if i != 0]) - 1,
)
csr_matrix = scipy.sparse.csr_matrix(
    (data, indices, indptr), shape=(dim_col, dim_row)
).toarray()
sprs = np.zeros((dim_col, dim_row, 3), dtype=np.uint8)
for row in range(len(csr_matrix)):
    for col in range(len(csr_matrix[row])):
        if csr_matrix[row][col] != 0:
            sprs[row][col] = [250, 0, 0]
# np.savetxt("foo.csv", csr_matrix, delimiter=",")
img = Image.fromarray(sprs, "RGB")
img.save("my.png")
img.show()
