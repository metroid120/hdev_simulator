"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
import copy
import numpy as np
from DMT.core import (
    read_data,
    specifiers,
    Sweep,
    DutType,
    Plot,
    sub_specifiers,
    constants,
)
from hdevpy.materials import ALLOYS, materials


def set_bias(inp, cont_name=None, bias_val=None, bias_fun=None):
    """Adds bias blocks to a hdev inp dictionary according to parameters.

    Input
    -----
    inp : dict
        A dictionary that defines a hdev input file.
    cont_name : char, None
        The identifier of the contact to be excited.
    bias_val : float64, [float64]
        A value that defines the bias, see the hdev manual for details.
    bias_val : str, None
        A str that defines the bias function, see the hdev manual for details.


    Returns
    -------
    inp : dict
        The dictionary with the new BIAS_DEF block.
    """
    if bias_fun is None:
        bias_fun = "TAB"

    bias_info = {
        "cont_name": cont_name,
        "bias_fun": bias_fun,
        "bias_val": bias_val,
    }

    if not "BIAS_INFO" in inp:
        inp["BIAS_INFO"] = [bias_info]

    else:
        try:
            # there is already a matching bias info for this contact
            index = next(
                index
                for index, bias_info in enumerate(inp["BIAS_INFO"])
                if bias_info["cont_name"] == cont_name
            )
            inp["BIAS_INFO"][index] = bias_info
        except StopIteration:
            inp["BIAS_INFO"].append(bias_info)

    return inp


def get_hdev_dfs(dut_hdev, sweep, index=None):
    """Return the internal quantities and the iv data of a hdev simulation. The inqu data is taken at bias point with index.

    Input
    -----
    dut_hdev : DMT.Hdev.Duthdev
        The DUT object whose dataframe are investigated.
    sweep : DMT.core.Sweep
        The sweep that has been simulated
    index : integer
        The operating point index that corresponds to the requested inqu data. If not given, return the inqu data of the first operating point.

    Returns
    -------
    (df_iv,df_inqu): (pd.DataFrame, pd.DataFrame)
        A tuple with the first element containing the iv data and the second the inqu data at index index.
    """
    if index is None:
        index = 1

    key_sweep = dut_hdev.get_sweep_key(sweep)
    key = key_sweep + "/iv"
    df_iv = dut_hdev.data[key]
    try:
        freqs = np.unique(df_iv[specifiers.FREQUENCY].to_numpy())
        freqs.sort()
        if len(freqs) > 1:
            df_iv = df_iv[df_iv[specifiers.FREQUENCY] != 0]
    except KeyError:  # pure DC Simulation
        pass

    if index is None:  # dirty: assume first op is equ. Should be checked.
        str_search = "op1_inqu"
    else:
        str_search = "op{0:1.0f}_inqu".format(index)

    for key in dut_hdev.data.keys():
        if str_search in key and key_sweep in key:
            df_inqu = dut_hdev.data[key]
            break
    else:
        df_inqu = None  # not always present

    return df_iv, df_inqu


def scale_hdev_df(df, area):
    """Scale a 1D simulation result to 3D using an area.

    Input
    -----
    df : pd.DataFrame
        A dataframe that is the IV output of a 1D hdev simulation.
    area : float64
        The area in m^2 that the 3D device corresponds to.

    """
    cols = df.columns
    for col in cols:
        if col[:2] in ["I_", "Y_", "C_"]:
            df[col] = df[col].to_numpy() * area

    return df


def get_sweep(
    vbe=None, vbc=None, vce=None, ib=None, ie=None, ac=True, temp=300, freq=1e3
):
    """Return a a DMT Sweep Definition suitable for HBT simulation in hdev. The contacts of a HBT are B, E and C.
    Note that only two of the parameters vbe, vbc, ib, ic and vce must be defined.

    input
    -----
    vbe : float64, np.array(), list, None
        The vbe sweep definition. A constant value means constant Vbe voltage, a list means values for Vbe from vbe[0] to vbe[1] in vbe[2] equally spaced steps.
    vbc : float64, np.array(), list, None
        The vbc sweep definition. A constant value means constant Vbc voltage, a list means values for Vbc from vbc[0] to vbc[1] in vbc[2] equally spaced steps.
    vce : float64, np.array(), list, None
        The vce sweep definition. A constant value means constant Vce voltage, a list means values for Vce from vce[0] to vce[1] in vce[2] equally spaced steps.
    ib : float64, np.array(), list, None
        The ib sweep definition. A constant value means constant Ib current, a list means values for Ib from ib[0] to ib[1] in ib[2] equally spaced steps.
    ie : float64, np.array(), list, None
        The ie sweep definition. A constant value means constant Ie current, a list means values for Ie from ie[0] to ie[1] in ie[2] equally spaced steps.
    ac   : bool, optional, default=True
        If True: return a sweep that contains low frequency AC simulation definitions.
    temp : float,[float], optional, default=300
        Temperature of the simulation in Kelvin. If a list of floats is passed, the temperature is swept during simulation.
    freq : float64, 1e3
        The frequency for AC simulations at every DC operating point in Hertz.

    returns
    -------
    sweep : DMT.core.Sweep
        DMT sweep definition
    """
    if vbe is not None and vbc is not None and vce is not None:
        raise IOError(
            "You can not specify three voltages when two already define all potentials."
        )

    if vbe is not None:
        try:
            _a = len(vbe)
        except TypeError:
            vbe = np.array([vbe])
    if vbc is not None:
        try:
            _a = len(vbc)
        except TypeError:
            vbc = np.array([vbc])
    if vce is not None:
        try:
            _a = len(vce)
        except TypeError:
            vce = np.array([vce])
    if ib is not None:
        try:
            _a = len(ib)
        except TypeError:
            ib = np.array([ib])

    if ie is not None:
        try:
            _a = len(ie)
        except TypeError:
            ie = np.array([ie])

    if vbe is not None and vbc is not None:
        if len(vbe) > 1 and len(vbc) == 1:
            swp_name = "gum_vbc"
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 2,
                    "sweep_type": "SYNC",
                    "master": "V_B",
                    "offset": -vbc[0],
                },
                {
                    "var_name": specifiers.VOLTAGE + "B",
                    "sweep_order": 2,
                    "sweep_type": "LIN",
                    "value_def": vbe,
                },
                {
                    "var_name": specifiers.VOLTAGE + "E",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
            ]
            n_sweep = 2
        elif len(vbc) > 1 and len(vbe) == 1:
            swp_name = "rev_vbc"
            vbc_rev = vbc
            vbc_rev[0] = -vbc_rev[0]
            vbc_rev[1] = -vbc_rev[1]
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 3,
                    "sweep_type": "LIN",
                    "value_def": vbc_rev,
                },
                {
                    "var_name": specifiers.VOLTAGE + "B",
                    "sweep_order": 2,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
                {
                    "var_name": specifiers.VOLTAGE + "E",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": -np.asarray(vbe),
                },
            ]
            n_sweep = 3
    elif vbe is not None and vce is not None:
        if len(vbe) > 1 and len(vce) == 1:
            swp_name = "gum_vce"
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 3,
                    "sweep_type": "CON",
                    "value_def": vce,
                },
                {
                    "var_name": specifiers.VOLTAGE + "B",
                    "sweep_order": 2,
                    "sweep_type": "LIN",
                    "value_def": vbe,
                },
                {
                    "var_name": specifiers.VOLTAGE + "E",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
            ]
            n_sweep = 3
        else:
            swp_name = "out_vbe"
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 3,
                    "sweep_type": "LIN",
                    "value_def": vce,
                },
                {
                    "var_name": specifiers.VOLTAGE + "B",
                    "sweep_order": 2,
                    "sweep_type": "CON",
                    "value_def": vbe,
                },
                {
                    "var_name": specifiers.VOLTAGE + "E",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
            ]
            n_sweep = 3
    elif vce is not None and ib is not None:
        if len(vce) > 1 and len(ib) == 1:
            swp_name = "out_ib"
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 2,
                    "sweep_type": "LIN",
                    "value_def": vce,
                },
                {
                    "var_name": specifiers.CURRENT + "B",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": ib,
                },
                {
                    "var_name": specifiers.VOLTAGE + "E",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
            ]
            n_sweep = 2
    elif vbc is not None and ie is not None:  # common base
        if len(vbc) > 1 and len(ie) == 1:
            swp_name = "out_ie"
            vcb = [-vbc[0], -vbc[1], vbc[2]]
            sweepdef = [
                {
                    "var_name": specifiers.VOLTAGE + "C",
                    "sweep_order": 3,
                    "sweep_type": "LIN",
                    "value_def": vcb,
                },
                {
                    "var_name": specifiers.CURRENT + "E",
                    "sweep_order": 2,
                    "sweep_type": "CON",
                    "value_def": ie,
                },
                {
                    "var_name": specifiers.VOLTAGE + "B",
                    "sweep_order": 1,
                    "sweep_type": "CON",
                    "value_def": [0],
                },
            ]
            n_sweep = 2
    else:
        raise NotImplementedError

    if ac:
        if isinstance(freq, float):
            freq = [freq]
        if len(freq) == 1:
            sweepdef.append(
                {
                    "var_name": specifiers.FREQUENCY,
                    "sweep_order": n_sweep + 1,
                    "sweep_type": "CON",
                    "value_def": freq,
                },  # small freq as QUAS
            )
        else:
            sweepdef.append(
                {
                    "var_name": specifiers.FREQUENCY,
                    "sweep_order": n_sweep + 1,
                    "sweep_type": "LOG",
                    "value_def": freq,
                },  # small freq as QUAS
            )

    if isinstance(temp, list):
        othervar = {}
        for i, _swd in enumerate(sweepdef):
            sweepdef[i]["sweep_order"] = sweepdef[i]["sweep_order"] + 1

        sweepdef.append(
            {
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": temp,
                "var_name": specifiers.TEMPERATURE,
            },
        )
    else:
        othervar = {"TEMP": temp}
    sweep = Sweep(swp_name, sweepdef=sweepdef, outputdef=[], othervar=othervar)
    return sweep


def makeinp(inp, file=None):
    """Create a hdev input file from inp, a dict of dicts that contains the inp blocks as defined in the hdev manual.
    If file=None, this method returns the input file as a string, else the file is written to 'file'.

    Input
    ------
    inp : dict
        A Python dict that defines a hdev inpu file according to the hdev manual.
    file : str, optional, None
        If not None, the hdev input file is written to this path.

    Returns
    -------
    inp_file : str or Bool
        If file is None, the string that corresponds to the hdev input structure is returned. Else True upon success.

    """

    # write file
    inp_file = ""
    for block_name, block_dict in inp.items():
        if type(block_dict) is dict:
            inp_file = inp_file + "&" + block_name
            for para_name, para_value in block_dict.items():
                if type(para_value) is str:
                    para_value = "'" + para_value + "'"
                elif type(para_value) is list or type(para_value) is tuple:
                    para_value = [str(elem) + " " for elem in para_value]
                    para_value = "".join(para_value)

                inp_file = inp_file + " " + para_name + "=" + str(para_value)

        else:  # probably a list
            for block in block_dict:
                inp_file += "&" + block_name
                for para_name, para_value in block.items():
                    if type(para_value) is str:
                        para_value = "'" + para_value + "'"
                    elif (
                        type(para_value) is list
                        or type(para_value) is np.ndarray
                        or type(para_value) is tuple
                    ):
                        para_value = [str(elem) + " " for elem in para_value]
                        para_value = "".join(para_value)

                    inp_file = inp_file + " " + para_name + "=" + str(para_value)

                inp_file = inp_file + "/\n"

        inp_file = inp_file + "/\n"

    inp_file = inp_file + "\n"

    if file is None:
        return inp_file
    else:
        with open(file, "w") as text_file:
            text_file.write(inp_file)

        return True  # return True to confirm that writting the file worked


def run_test(inp, hdev_ver):
    """Run a hdev test, for hdev with version hdev_ver and an input file given by the inp dictionary.

    Input
    -----
    inp : dict
        A Python dict that defines a hdev inpu file according to the hdev manual.
    hdev_ver : str
        A string that specifies the hdev version to be tested.
    """
    hdev_ver = "hdev_v" + str(hdev_ver)

    print("simulating " + str(hdev_ver) + "..")
    run_hdev(hdev_ver, inp, 1)

    # try to create dirs
    path_inp = str(inp["OUTPUT"]["path"]) + "/inp/" + inp["OUTPUT"]["name"] + ".inp"
    path_out = str(inp["OUTPUT"]["path"]) + "/out/" + inp["OUTPUT"]["name"] + ".out"
    for path in [path_inp, path_out]:
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
    os.rename(inp["OUTPUT"]["name"] + ".inp", path_inp)
    os.rename(inp["OUTPUT"]["name"] + ".out", path_out)
    print(" ")


def run_hdev(hdev, inp, out):
    """Run a hdev simulation, for hdev binary with name hdev and an input file given by the inp dictionary.

    Input
    -----
    hdev : str
        A string that specifies the hdev version to be tested.
    inp : dict
        A Python dict that defines a hdev inpu file according to the hdev manual.
    out : Bool
        If True, the simulation output on the command line is written to a file.

    """
    input_file = inp["OUTPUT"]["name"] + ".inp"
    output_file = inp["OUTPUT"]["name"] + ".out"

    makeinp(inp, input_file)
    if out:
        os.system(hdev + " " + input_file + " > " + output_file)
    else:
        os.system(hdev + " " + input_file)


def turn_off_recombination(inp):
    """Will turn off all recombination models in a hdev input structure."""
    try:
        del inp["RECOMB_DEF"]
    except KeyError:
        pass
    return inp


def set_all_materials_to(inp, str_mat_name):
    """set all materials in REGION_DEF to one specific material.

    Input
    -----
    inp : dict
        A Python dictionary that defines the simulation input.
    str_mat_name : str
        The name of the semiconductor material that shall be used for alle SEMI regions in REGION_DEF blocks.

    Returns
    --------
    inp : dict
        The modified inp dictionary, with all REGION_DEF blocks adjusted to the new material.
    """
    for i in range(len(inp["REGION_DEF"])):
        if inp["REGION_DEF"][i]["reg_mat"] == "SEMI":
            inp["REGION_DEF"][i]["mod_name"] = str_mat_name
    return inp


def set_force(inp, force, mobility=True, recombination=True):
    """set the force parameter of MOB_DEF or RECOMB_DEF blocks to type force.

    Input
    -----
    inp : dict
        A Python dictionary that defines the simulation input.
    force : str, {"phi","psi","mix"}
        The force parameter value that shall be set. See the hdev manual for details.
    mobility : bool, True
        If True: Set force for MOB_DEF blocks.
    recombination : bool, True
        If True: Set force for RECOMB_DEF blocks.

    Returns
    --------
    inp : dict
        The modified inp dictionary, with all REGION_DEF blocks adjusted to the new material.

    """
    if mobility:
        for mob_model in inp["MOB_DEF"]:
            mob_model["force"] = force

    try:
        if recombination:
            for rec_model in inp["RECOMB_DEF"]:
                rec_model["force"] = force
    except KeyError:
        pass

    return inp


def get_band_diagram(dut, sweep, op=1):
    """Given a Duthdev and a sweep object, return the band diagram at op.

    Input
    -----
    dut : DMT.Hdev.Duthdev
        Dut object that has been simulated.
    sweep : DMT.core.Sweep
        Sweep object that has been simulated
    op : int
        The operating point number to plot
    """
    if op > 1:
        raise NotImplementedError

    _df_iv, df_inqu = get_hdev_dfs(dut, sweep)

    plt_band = Plot(
        "E(x)",
        style="color",
        x_specifier=specifiers.X,
        y_label=r"$E/\si{\eV}$",
        x_scale=1e9,
        legend_location="upper left",
    )

    x = df_inqu["X"].to_numpy()
    ec = df_inqu["EC"].to_numpy()
    ev = df_inqu["EV"].to_numpy()
    efn = df_inqu["EF|ELECTRONS"].to_numpy()
    efp = df_inqu["EF|HOLES"].to_numpy()

    plt_band.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
    plt_band.add_data_set(x, ev, r"$E_{\mathrm{V}}$")
    plt_band.add_data_set(x, efn, r"$E_{\mathrm{Fn}}$")
    plt_band.add_data_set(x, efp, r"$E_{\mathrm{Fp}}$")

    return plt_band


def get_profile(dut, sweep, op=1):
    """Given a Duthdev and a sweep object, return the profile diagram.

    Input
    -----
    dut : DMT.Hdev.Duthdev
        Dut object that has been simulated.
    sweep : DMT.core.Sweep
        Sweep object that has been simulated
    op : int
        The operating point number to plot
    """
    if op > 1:
        raise NotImplementedError

    _df_iv, df_inqu = get_hdev_dfs(dut, sweep)

    # profile plot
    import matplotlib.pyplot as plt
    from random import randint

    fig_band, ax1_band = plt.subplots()
    ax1_band.plot(
        df_inqu["X"] * 1e9, np.abs(df_inqu["NNET"]) * 1e-6, "-k", label="initial"
    )
    ax1_band.set_yscale("log")
    ax2_band = ax1_band.twinx()
    ax2_band.plot(df_inqu["X"] * 1e9, df_inqu["MOL"] * 100, "--k")
    ax2_band.set_yscale("linear")
    # ax2.set_ylim(y2_lim)
    ax1_band.set_xlabel(r"$x/\si{\nano\meter}$")
    ax1_band.set_ylabel(r"$N/\si{\per\cubic\centi\meter}$")
    ax2_band.set_ylabel(r"$C/\si{\percent}$")
    ax1_band.legend(loc="upper right")
    plt.tight_layout()
    return plt, fig_band, ax1_band, ax2_band


def set_hdev_material_def(inp, material_name):
    """Using the hdev material databae, add the material definition of material with name material_name to the inp dictionary.

    Input
    -----
    inp : dict
        A Python dictionary that defines the simulation input.
    material_name : str
        One of the materials available in the material_database.py file.

    Returns
    --------
    inp : dict
        The modified inp dictionary, with the material definition of material_name added.

    """
    material = copy.deepcopy(materials[material_name])
    inp_keys = inp.keys()
    for key, val in material.items():
        if not key in inp_keys:
            inp[key] = val
        else:
            if isinstance(inp[key], list):
                try:
                    if isinstance(val, list):
                        for val_i in val:
                            if not val_i in inp[key]:
                                inp[key] = inp[key] + [val_i]
                    else:
                        inp[key] = inp[key] + val
                except TypeError:
                    inp[key].append(val)
            else:
                if isinstance(val, list):
                    inp[key] = [inp[key], *val]
                else:
                    inp[key] = [inp[key], val]
    return inp
    # check if this is an alloy
    # if material_name in ALLOYS.keys():
    #     alloy = ALLOYS[material_name]
    #     material_A = alloy['SEMI']['material_A']
    #     material_A = materials[material_A]
    #     material_B = alloy['SEMI']['material_B']
    #     material_B = materials[material_B]

    #     for key in alloy.keys():
    #         left_val  = InP[key]
    #         right_val = InGaAs[key]
    #         if isinstance(left_val, dict) and isinstance(right_val, dict):
    #             material[key] = [left_val, right_val]
    #         elif isinstance(left_val, list) and isinstance(right_val, dict):
    #             material[key] = left_val + [right_val]
    #         elif isinstance(left_val, dict) and isinstance(right_val, list):
    #             material[key] = [left_val] + right_val
    #         elif isinstance(left_val, list) and isinstance(right_val, list):
    #             material[key] = left_val + right_val
    #         else:
    #             raise IOError('Error occured.')

    #     inp.update(material)

    return inp


def fd1h(x):
    # !
    # ! an analytical approximation for the fermi-dirac integral F1/2
    # !
    # ! Author: Aymerich
    # !
    # ! TODO: maybe replace with"
    # ! A generalized approximation of the Fermi–Dirac integrals
    # ! "
    a = 9.6
    b = 2.13
    c = 2.4
    pi = np.pi
    two_over_pi = 2 / np.sqrt(pi)
    return two_over_pi * (
        4.24264068712 / (b + x + (abs(x - b) ** c + a) ** (1 / c)) ** 1.5
        + np.exp(-x) / (0.5 * np.sqrt(pi))
    ) ** (-1)


def init_user(paras, hdev):
    """Set the paras to the hdev instance"""
    if not "REGION_DEF" in paras.keys():
        # if no structure defined in paras, we add a "dummy" structure
        paras["REGION_DEF"] = []

        paras["REGION_DEF"].append(
            {
                "reg_mat": "SEMI",
                "low_xyz": 0,
                "upp_xyz": 100e-9,
                "mod_name": "Si",
            }
        )

        paras["REGION_INFO"] = {}
        paras["REGION_INFO"]["spat_dim"] = 1

        paras["RANGE_GRID"] = {}
        paras["RANGE_GRID"]["disc_dir"] = "x"
        paras["RANGE_GRID"]["disc_set"] = "pnts"
        paras["RANGE_GRID"]["intv_pnts"] = [0, 100e-9]
        paras["RANGE_GRID"]["n_pnts"] = 11

    inp_str = makeinp(paras)
    home = os.path.expanduser("~")
    tmp_path = os.path.join(home, "tmp", "hdev_inp.din")
    inp_file = open(tmp_path, "w+")
    inp_file.write(inp_str)
    inp_file.close()
    hdev.f90wrap_init(inp_file.name)


def get_hbt_zero_bias_quantities(df_equ, df_iv):
    """Gets several zero bias parameters for 1D HBTs in natural units.

    Input
    -----
    df_equ : DMT.core.DataFrame
        DataFrame that contains the internal simulation quantities in thermal equilibrium.
    df_iv : DMT.core.DataFrame
        DataFrame that contains the iv characteristics.

    Result
    ------
    qp0 : float
        Zero bias hole charge in fC/um^2.
    rsbi0 : float
        Zero bias base sheet resistance.
    cjei0 : float
        Zero bias BE junction capacitance.
    cjci0 : float
        Zero bias BC junction capacitance.
    """
    qp0 = np.trapz(df_equ["P"], df_equ["X"]) * 1e15 * constants.P_Q * 1e-12  # fC/um^2
    rsbi0 = 1 / (
        constants.P_Q * np.trapz(df_equ["P"] * df_equ["MU|P|YDIR"], df_equ["X"])
    )  # Ohm
    try:
        cjei0 = df_iv["C_BE"].to_numpy()[0] * 1e15 * 1e-12  # fF/um^2
        cjci0 = df_iv["C_BC"].to_numpy()[0] * 1e15 * 1e-12  # fF/um^2
    except:
        df_iv["C_BE"] = 0
        df_iv["C_BC"] = 0
        cjci0 = 0
        cjei0 = 0

    return qp0, rsbi0, cjei0, cjci0


def get_default_plot(plot_type, prefix=None, x_limits=None, y_limits=None, **kwargs):
    """Return a plot that is regularly used for TCAD in HBTs.

    Input
    -----
    plot_type : str
        Either "gum", "ft", "fmax", "dop", "mol", "profile", "out", "cbe" or "cbc".
    prefix : str
        Prefix for plot name
    style : str
        Valid plot style, see the DMT.core.Plot class.
    x_limits : (float, float)
        X limits as used for DMT plots
    y_limits : (float, float)
        Y limits as used for DMT plots

    Returns
    -------
    hbt_plot
    """
    if prefix == None:
        prefix = ""

    plt = None
    if plot_type == "gum":
        plt = Plot(
            prefix + "I_C(V_BE)",
            x_specifier=specifiers.VOLTAGE + "B" + "E",
            y_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            y_scale=1e3 / (1e6 * 1e6),
            y_log=True,
            **kwargs
        )
    elif plot_type == "out":
        plt = Plot(
            prefix + "I_C(V_CE)",
            x_specifier=specifiers.VOLTAGE + "C" + "E",
            y_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            y_scale=1e3 / (1e6 * 1e6),
            **kwargs
        )
    elif plot_type == "charge":
        plt = Plot(
            prefix + "Q(V_BE)",
            x_specifier=specifiers.VOLTAGE + "B" + "E",
            y_label=r"$Q/\si{\femto\coulomb\per\square\micro\meter}$",
            y_scale=1e15 / (1e6 * 1e6),
            **kwargs
        )
    elif plot_type == "chargei":
        plt = Plot(
            prefix + "Q(I_C)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            x_log=True,
            y_label=r"$Q/\si{\femto\coulomb\per\square\micro\meter}$",
            y_scale=1e15 / (1e6 * 1e6),
            **kwargs
        )
    elif plot_type == "velo":
        plt = Plot(
            prefix + "V(x)",
            x_label=r"$x\left(\si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$v \left( \SI{1e7}{\centi\meter\per\second} \right) $",
            y_scale=1e-7*1e2,
            **kwargs
        )
    elif plot_type == "jdens":
        plt = Plot(
            prefix + "J(x)",
            x_label=r"$x\left(\si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$J \left( \si{\milli\ampere\per\square\micro\meter} \right) $",
            y_scale=1e3/1e12,
            **kwargs
        )
    elif plot_type == "mob":
        plt = Plot(
            prefix + "mu(x)",
            x_label=r"$x\left(\si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$\mu \left( \si{\square\centi\meter\per\volt\second} \right) $",
            y_scale=1e4,
            **kwargs
        )
    elif plot_type == "weight":  # GICCR weight factor against Vbe
        plt = Plot(
            prefix + "h(V_BE)",
            x_specifier=specifiers.VOLTAGE + "B" + "E",
            y_label=r"$h$",
            y_scale=1,
            **kwargs
        )
    elif plot_type == "weighti":  # GICCR weight factor against Ic
        plt = Plot(
            prefix + "h(I_CE)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_label=r"$h$",
            y_scale=1,
            x_log=True,
            **kwargs
        )
    elif plot_type == "cbe":
        plt = Plot(
            prefix + "C_BE(V_BE)",
            x_specifier=specifiers.VOLTAGE + "B" + "E",
            y_label=r"$C_{\mathrm{BE}}\left(\si{\femto\farad\per\square\micro\meter}\right)$",
            y_scale=1e15 / (1e6 * 1e6),
            **kwargs
        )
    elif plot_type == "cbc":
        plt = Plot(
            prefix + "C_BC(V_BC)",
            x_specifier=specifiers.VOLTAGE + "B" + "C",
            y_label=r"$C_{\mathrm{BC}}\left(\si{\femto\farad\per\square\micro\meter}\right)$",
            y_scale=1e15 / (1e6 * 1e6),
            **kwargs
        )
    elif plot_type == "ft":
        plt = Plot(
            prefix + "F_T(J_C)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_specifier=specifiers.TRANSIT_FREQUENCY,
            y_scale=1e-9,
            x_log=True,
            **kwargs
        )
    elif plot_type == "fmax":
        plt = Plot(
            prefix + "F_MAX(J_C)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_specifier=specifiers.MAXIMUM_OSCILLATION_FREQUENCY,
            y_scale=1e-9,
            x_log=True,
            **kwargs
        )
    elif plot_type == "tau":
        plt = Plot(
            prefix + "TAU(J_C)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_label=r"$\tau\left(\si{\pico\second}\right)$",
            y_scale=1e12,
            x_log=True,
            **kwargs
        )
    elif plot_type == "tauf":
        plt = Plot(
            prefix + "TAUF(J_C)",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_label=r"$\tau_{\mathrm{f}}\left(\si{\pico\second}\right)$",
            y_scale=1e12,
            x_log=True,
            **kwargs
        )
    elif plot_type == "tau0":
        plt = Plot(
            prefix + "TAU0(V_BC)",
            x_label=r"$V_{\mathrm{BC}}\left(\si{\volt}\right)$",
            y_label=r"$\tau\left(\si{\pico\second}\right)$",
            y_scale=1e12,
            **kwargs
        )
    elif plot_type == "gm":
        plt = Plot(
            prefix + "G_M(J_C)",
            y_label=r"$\bar{g}_{\mathrm{m}}V_{\mathrm{T}}/J_{\mathrm{C}}$",
            x_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
            x_scale=1e3 / (1e6 * 1e6),
            y_scale=1,
            x_log=True,
            **kwargs
        )
    elif plot_type == "band":
        plt = Plot(
            prefix + "band_diagram",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$E\left(\si{\eV} \right)$",
            y_log=False,
            y_scale=1,
            **kwargs
        )
    elif plot_type == "eg":
        plt = Plot(
            prefix + "bandgap",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$E_{\mathrm{G}}\left(\si{\eV} \right)$",
            y_log=False,
            y_scale=1,
            **kwargs
        )
    elif plot_type == "psi":
        plt = Plot(
            prefix + "psi",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$\Psi\left(\si{\volt} \right)$",
            y_log=False,
            y_scale=1,
            **kwargs
        )
    elif plot_type == "field":
        plt = Plot(
            prefix + "field",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$-\nabla \psi \left(\si{\kilo\volt\per\centi\meter} \right)$",
            y_scale=1e-3 / 1e2,
            **kwargs
        )
    elif plot_type == "rho":
        plt = Plot(
            prefix + "rho",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$\rho\left(\si{\femto\coulomb\per\cubic\centi\meter} \right)$",
            y_scale=1e15 / 1e6,
            **kwargs
        )
    elif plot_type == "ec":
        plt = Plot(
            prefix + "ec",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$E_{\mathrm{C}}\left(\si{\eV} \right)$",
            y_log=False,
            y_scale=1,
            **kwargs
        )
    elif plot_type == "dens":
        plt = Plot(
            prefix + "carrier_density",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$n\left(\si{\per\cubic\centi\meter} \right)$",
            y_log=True,
            y_scale=1e-6,
            **kwargs
        )
    elif plot_type == "rand":
        plt = Plot( #random quantity
            prefix + "rand",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$r$",
            **kwargs
        )
    elif plot_type == "grading":
        plt = Plot(
            prefix + "grading",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$C\left( \si{\percent} \right) $",
            y_scale=1e2,
            **kwargs
        )
    elif plot_type == "dop":
        plt = Plot(
            prefix + "dop",
            x_label=r"$x\left( \si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$N_{\mathrm{net}}\left( \si{\per\cubic\centi\meter} \right) $",
            y_scale=1e-6,
            y_log=True,
            **kwargs
        )
    elif plot_type == "taux":
        plt = Plot(
            prefix + "taux",
            x_label=r"$x\left(\si{\nano\meter}\right)$",
            x_scale=1e9,
            y_label=r"$\tau\left( \si{\pico\second} \right)$",
            y_log=False,
            y_scale=1e12,
            **kwargs
        )
    else:
        raise IOError("plot_type " + plot_type + " not implemented.")

    if x_limits is not None:
        plt.x_limits = x_limits
    if y_limits is not None:
        plt.y_limits = y_limits

    return plt
