"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import triode
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers
import numpy as np
import re
import os


class TestCaseTriode(hdevTestCase):
    def __init__(self, material, doping):
        name = "Triode" + material
        inp_func = lambda version, mat: triode(version, doping, mat)
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "B",
                "sweep_order": 3,
                "sweep_type": "LIN",
                "value_def": [0, 1, 11],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "CON",
                "value_def": [0],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "A 2D structure with three contacts and constant doping."
        super().__init__(
            name,
            description,
            DutType.res,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="I-V " + self.name + ".",
            x_label=r"$V_{\mathrm{BC}}\left( \si{\milli\ampere\per\micro\meter} \right)$",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\micro\meter} \right)$",
            style="color",
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_B"].to_numpy() - df["V_C"].to_numpy())
                i = np.abs(df["I_C"].to_numpy())
                plt.add_data_set(
                    v,
                    i * 1e3 / (1e6),
                )

        return plt

    @plot
    def plot_cap(self):
        import matplotlib.pyplot as mplt
        import tikzplotlib

        for key in self.dut.data.keys():
            if "cap" in key:
                df = self.dut.data[key]
                cap_name, cap_val = [], []
                for col in list(df.columns):
                    contacts = str(col)[2:]
                    # if contacts[0] == contacts[1]:
                    #     continue

                    col_tex = str(col).upper().replace("_", "_{\mathrm{", 1)
                    col_tex = col_tex + r"}}"
                    cap_name.append(r"$" + col_tex + r"$")
                    cap_val.append(np.abs(df[col][0]))

                mplt.bar(
                    cap_name,
                    np.array(cap_val) * 1e15 / 1e6,
                    align="center",
                    tick_label=cap_name,
                )
                mplt.ylabel(r"$C \left( \mathrm{fF}/\mu\mathrm{m}\right)$")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_caps" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Capacitance coefficients of metallization structure. ",
        )

    @plot
    def plot_eps(self):
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                eps = df["eps"].to_numpy()
                eps = eps.reshape(len(x), len(y))

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                cs = ax.contourf(x * 1e6, y * 1e6, eps, 100)
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$\varepsilon_r $")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_eps" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Dielectric permittivity contours in the triode.",
        )

    @plot
    def plot_laplace_a(self):
        for key in self.dut.data.keys():
            if "laplace_A" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                psi = df["psi"].to_numpy()
                psi = psi.reshape(len(x), len(y))

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$\Psi \left( \mathrm{V} \right) $")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_laplace_A" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Potential in the triode with contact A at 1V, all others grounded.",
        )

    # @plot
    # def plot_box(self):
    #     for key in self.dut.data.keys():
    #         if "op1_inqu" in key:
    #             import matplotlib.pyplot as mplt
    #             import tikzplotlib

    #             mplt.rc("text", usetex=True)

    #             df = self.dut.data[key]
    #             x = np.unique(df["X"].to_numpy())
    #             y = np.unique(df["Y"].to_numpy())
    #             psi = df["box"].to_numpy()
    #             psi = psi.reshape(len(x), len(y))

    #             fig = mplt.figure()
    #             ax = fig.add_subplot(111)
    #             cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
    #             ax.set_xlabel(r"$x ( \mu m )$")
    #             ax.set_ylabel(r"$y ( \mu m )$")
    #             ax.set_xlim(0, 1)
    #             ax.set_ylim(0, 1)
    #             ax.set_aspect("equal")
    #             cbar = fig.colorbar(cs)
    #             cbar.ax.set_ylabel(r"$\Psi \left( \mathrm{V} \right) $")
    #             path_ = "/home/markus/Documents/Gitprojects/hdevmanual/fig/triode_box.eps"
    #             mplt.savefig(fname=path_)
    #             # tikzplotlib.save(path_)

    #             break

    #     return (
    #         path_,
    #         r"Box ids.",
    #     )

    @plot
    def plot_laplace_b(self):
        for key in self.dut.data.keys():
            if "laplace_B" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                psi = df["psi"].to_numpy()
                psi = psi.reshape(len(x), len(y))

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$\Psi \left( \mathrm{V} \right) $")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_laplace_B" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Potential in the triode with contact B at 1V, all others grounded.",
        )

    @plot
    def plot_field(self):
        for key in self.dut.data.keys():
            if "laplace_B" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                psi = df["psi"].to_numpy()
                psi = psi.reshape(len(x), len(y))
                psix, psiy = np.gradient(psi, 10e-9, 10e-9)
                psi = np.sqrt(psix ** 2 + psiy ** 2)

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                cs = ax.contourf(x * 1e6, y * 1e6, psi * 1e5, 100)
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$F \left( \mathrm{kV/cm} \right) $")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_laplace_field" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Field in the triode with contact B at 1V, all others grounded.",
        )

    @plot
    def plot_laplace_c(self):
        for key in self.dut.data.keys():
            if "laplace_C" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                psi = df["psi"].to_numpy()
                psi = psi.reshape(len(x), len(y))

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$\Psi \left( \mathrm{V} \right) $")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "triode_laplace_C" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Potential in the triode with contact C at 1V, all others grounded..",
        )
