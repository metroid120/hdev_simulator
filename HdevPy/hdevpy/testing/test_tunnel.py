"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
# this script generates a simple SiGe HBT and calculates the tunneling current and recombination rate
# in DMT and compares it with the solution of hdev
from DMT.core import SimCon, Sweep, DutType, specifiers, Plot, constants
from DMT.Hdev import sige_hbt, Duthdev

import numpy as np

# get hdev input file
inp = sige_hbt("hdev", fermi=False)

# get sweep
sweepdef = [
    {
        "var_name": specifiers.VOLTAGE + "E",
        "sweep_order": 3,
        "sweep_type": "CON",
        "value_def": [0],
    },
    {
        "var_name": specifiers.VOLTAGE + "B",
        "sweep_order": 2,
        "sweep_type": "CON",
        "value_def": [0],
    },
    {
        "var_name": specifiers.VOLTAGE + "C",
        "sweep_order": 1,
        "sweep_type": "CON",
        "value_def": [0],
    },
]
outputdef = []
othervar = {"TEMP": 300}
sweep = Sweep("gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

# init Duthdev object
dut_hdev = Duthdev(None, DutType.npn, inp, reference_node="E")

# start the simulation
sim_con = SimCon(n_core=1, t_max=30)
sim_con.append_simulation(dut_hdev, sweep)
sim_con.run_and_read(force=True)

# get conduction and valence band
for key in dut_hdev.data.keys():
    if "op1_" in key and "inqu" in key:
        df = dut_hdev.data[key]
        x = df["X"].to_numpy()
        ec = df["EC"].to_numpy()
        ev = df["EV"].to_numpy()
        efn = df["EF|ELECTRONS"].to_numpy()

# get barriers
for key in dut_hdev.data.keys():
    dummy = 1

# plot conduction and valence band
plt_bands = Plot(
    "E(x)",
    style="color",
    x_log=False,
    y_scale=1,
    x_label=r"$x(\si{\micro\meter})$",
    y_label=r"$pop$",
    x_scale=1e6,
)
plt_tunnel = Plot(
    "T(E)",
    style="color",
    x_log=False,
    y_log=True,
    y_scale=1,
    x_label=r"$E(\si{\eV})$",
    y_label=r"$T$",
    x_scale=1,
)
plt_supply = Plot(
    "N(E)",
    style="color",
    x_log=False,
    y_log=True,
    y_scale=1,
    x_label=r"$E(\si{\eV})$",
    y_label=r"$N$",
    x_scale=1,
)
plt_gtun = Plot(
    "gtun(x)",
    style="color",
    x_log=False,
    y_log=False,
    y_scale=1,
    x_label=r"$x(um)$",
    y_label=r"$G$",
    x_scale=1e6,
)
plt_itun = Plot(
    "itun(x)",
    style="color",
    x_log=False,
    y_log=False,
    y_scale=1,
    x_label=r"$x(um)$",
    y_label=r"$I$",
    x_scale=1e6,
)
plt_bands.add_data_set(x, ec, label="ec")
plt_bands.add_data_set(x, efn, label="efn")

# first we do the implementation in DMT ...
# step 1: for every energy, see if we can find a barrier
wd_tunnel_max = 40e-9
tunnel_points = []
f = np.gradient(ec, x)
for i in range(len(x)):
    ec_i = ec[i]
    try:
        ec_m = ec[i - 1]
    except IndexError:
        ec_m = ec[i]
    try:
        ec_p = ec[i + 1]
    except IndexError:
        ec_p = ec[i]

    if ec_p > ec_i:  # tunneling to the right
        index_end = None
        for j in range(i, len(x)):
            ec_j = ec[j]
            if ec_j < ec_i:
                index_end = j
                break

        # reached end of device
        if index_end is None:
            continue

        wd = np.abs(x[i] - x[index_end])
        print("wd is " + str(wd * 1e9) + " nm.")
        if np.abs(x[i] - x[index_end]) > wd_tunnel_max:
            continue  # too long

        tunnel_points.append(
            {
                "indices": [i, j],
                "x": [x[i], x[j]],
                "ef": [efn[i], efn[j]],
                "ec": [ec[i], ec[j]],
                "f": [f[i], f[j]],
            }
        )

# now we calculate the tunneling probability for each tunnel_point
for point in tunnel_points:
    ec_barrier = ec[point["indices"][0] : point["indices"][1]]
    x_barrier = x[point["indices"][0] : point["indices"][1]]
    e_point = ec[point["indices"][0]]
    # kappa=[1/m]
    # sqrt(kg*s^2/(m^2*kg)^2*kg*m^2/s^2) => alles in Grundeinheiten, energy in Joule
    m = 9.10938e-31  # kg
    hbar = 6.626e-34 / (2 * np.pi)  # Js
    ev_to_joule = 1.602e-19
    kappa = np.sqrt(2 * m / hbar / hbar * (ec_barrier - e_point) * ev_to_joule)
    kappa_max = np.max(kappa)
    tunnel_integral = np.trapz(kappa, x_barrier)
    t = np.exp(-2 * tunnel_integral)
    point["t"] = t

# now we calculate the supply function
for point in tunnel_points:
    kb = 1.3806e-23
    T = 300
    q = 1.602e-19
    vt = kb * T / q
    ecl = point["ec"][0]
    ecr = point["ec"][1]
    efl = point["ef"][0]
    efr = point["ef"][1]
    ex = (ecl + ecr) / 2
    point["supply"] = (
        kb * T * np.log((1 + np.exp(-(ex - efl) / vt)) / (1 + np.exp(-(ex - efr) / vt)))
    )
    point["supply"] = 1

# now we calculate the tunneling generation rate
for point in tunnel_points:
    supply = point["supply"]
    t = point["t"]
    f_l = point["f"][0]
    f_r = point["f"][1]
    m0 = 9.109e-31  # kg
    kb = 1.3806e-23  # m^2kg/s^2/K
    T = 300  # K
    hbar = 6.626e-34 / (2 * np.pi)  # Js
    c = m0 * kb * T / (2 * np.pi * hbar ** 3)
    gtun_l = c * f_l * t * supply
    gtun_r = c * f_r * t * supply
    point["gtun"] = [gtun_l, gtun_r]

# create x dependent gtun
gtun = []
xtun = []
for point in tunnel_points:
    gtun.append(point["gtun"][0])
    gtun.append(point["gtun"][1])
    xtun.append(point["x"][0])
    xtun.append(point["x"][1])

xtun = np.array(xtun)
gtun = np.array(gtun)
indices = np.argsort(xtun)
xtun = xtun[indices]
gtun = gtun[indices]

for point in tunnel_points:
    plt_bands.add_data_set(
        x[point["indices"]],
        ec[point["indices"]],
        style="-r+",
    )

plt_tunnel.add_data_set(
    [ec[point["indices"][0]] for point in tunnel_points],  # t
    [point["t"] for point in tunnel_points],  # t
)
plt_gtun.add_data_set(
    xtun,
    gtun,
    style=" k+",
)

for key in dut_hdev.data.keys():
    if "tunnel" in key:
        df = dut_hdev.data[key]
        x_l = df["x_l"].to_numpy()
        x_r = df["x_r"].to_numpy()
        ec_l = df["ec_l"].to_numpy()
        ec_r = df["ec_r"].to_numpy()
        t = df["t"].to_numpy()
        gtun_hdev = df["gtun"].to_numpy()

        for i in range(len(x_l)):
            plt_bands.add_data_set(
                [x_l[i], x_r[i]],
                [ec_l[i], ec_r[i]],
                style="-k+",
            )
        plt_tunnel.add_data_set(
            ec_l,
            t,
            style=" kx",
        )
        plt_gtun.add_data_set(
            x,
            gtun_hdev,
            style="-r",
        )


itun = []
for i in range(len(xtun)):
    itun.append(np.trapz(gtun[:i], x[:i]))
plt_itun.add_data_set(
    xtun,
    itun,
    style=" k+",
)


plt_supply.add_data_set(
    [ec[point["indices"][0]] for point in tunnel_points],  # t
    [point["supply"] for point in tunnel_points],  # t
)

plt_itun.plot_py(show=False)
plt_gtun.plot_py(show=False)
plt_supply.plot_py(show=False)
plt_tunnel.plot_py(show=False)
plt_bands.plot_py(show=True)
