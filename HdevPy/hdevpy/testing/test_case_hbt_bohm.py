"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import bjt, sige_hbt
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers, constants
import numpy as np
import scipy.integrate as integrate


class TestCaseHBTBohm(hdevTestCase):
    """HBT with activated Bohm Potential"""

    def __init__(self, material, fermi=False, normalization=False):
        name = "hbt_bohm_" + material
        self.fermi = fermi
        self.normalization = normalization
        if self.fermi:
            name = name + "_fermi_dirac"

        inp_func = lambda ver, mat: sige_hbt(ver, fermi, normalization, bohm=True)
        sweepdef = [
            {
                "var_name": specifiers.FREQUENCY,
                "sweep_order": 3,
                "sweep_type": "CON",
                "value_def": [1e3],
            },
            {
                "var_name": specifiers.VOLTAGE + "E",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            # {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':2, 'sweep_type':'CON', 'value_def':[1]},
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "SYNC",
                "master": "V_B",
                "offset": 0,
            },
            # {'var_name':specifiers.VOLTAGE+'B', 'sweep_order':1, 'sweep_type':'LIN', 'value_def':[0.01,0.7,101]},
            {
                "var_name": specifiers.VOLTAGE + "B",
                "sweep_order": 1,
                "sweep_type": "LIN",
                # "value_def": [0, 1, 101],
                "value_def": [0, 0.05, 6],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "ITRS Node X SiGe HBT."
        super().__init__(
            name,
            description,
            DutType.npn,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        if self.normalization:
            super().compare_all_dfs()
        print(results)
        return results

    @plot
    def plot_band(self):
        plt = Plot(
            "E(x)",
            num="e" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                ec = df["EC"]
                ev = df["EV"]
                efn = df["EF|ELECTRONS"]
                efp = df["EF|HOLES"]

                # get electron density and hole density, plot Bohm Potential
                cn = df["N"].to_numpy()
                cp = df["P"].to_numpy()

                hbar = 1.054e-34  # m^2 kg /s
                q = 1.602e-19  # C
                m = 9.1e-31  # kg effective mass silicon
                n_hat = np.sqrt(cn)
                b = (1 * hbar ** 2) / (6 * q * m)
                # m^4 kg^2 / s /s / C / kg * /m /m = m^2*kg/s^2/C = J/C = V
                un = (
                    (1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cn)))
                    / np.sqrt(cn)
                    / np.gradient(x)
                    / np.gradient(x)
                )
                up = (
                    -(1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cp)))
                    / np.sqrt(cp)
                    / np.gradient(x)
                    / np.gradient(x)
                )

                jdg = np.gradient(n_hat) / np.gradient(x)
                rhs = un * np.sqrt(cn) - b * np.gradient(jdg) / np.gradient(x)

                ##solutions from hdev
                # jdgn = df["JDGN"].to_numpy()
                l_n = df["L_N"].to_numpy()
                # rhs_hdev = df["R_N"].to_numpy()
                # r_n = np.gradient(x) * un
                # rhs = np.gradient(jdgn) - un * n_hat * np.gradient(x)

                # discrete version
                jdg_n = np.gradient(n_hat, x) * b
                un_discrete = np.gradient(jdg_n) / np.gradient(x) / n_hat

                plt.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
                plt.add_data_set(x, ev, r"$E_{\mathrm{V}}$")
                plt.add_data_set(x, un, r"$\Lambda_{\mathrm{n}} DMT$")
                plt.add_data_set(x, up, r"$\Lambda_{\mathrm{p}}$")
                plt.add_data_set(x, l_n, r"$\Lambda_{\mathrm{n}} hdev$")
                plt.add_data_set(
                    x, ec - l_n, r"$E_{\mathrm{C}}-\Lambda_{\mathrm{n}} hdev$"
                )
                plt.y_limits = (-2, 2)

        return plt

    @plot
    def plot_jdgn(self):
        plt = Plot(
            "Jdgn(x)",
            num="jdgn" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_label=r"$J_{\mathrm{dgn}}$",
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                # get electron density and hole density, plot Bohm Potential
                cn = df["N"].to_numpy()
                cp = df["P"].to_numpy()

                hbar = 1.054e-34  # m^2 kg /s
                q = 1.602e-19  # C
                m = 9.1e-31  # kg effective mass silicon
                n_hat = np.sqrt(cn)
                b = (1 * hbar ** 2) / (6 * q * m)
                # m^4 kg^2 / s /s / C / kg * /m /m = m^2*kg/s^2/C = J/C = V
                un = (
                    (1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cn)))
                    / np.sqrt(cn)
                    / 1.602e-19
                )
                up = (
                    -(1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cp)))
                    / np.sqrt(cp)
                    / 1.602e-19
                )

                ##solutions from hdev
                try:
                    jdgn = df["JDGN"].to_numpy()
                except:
                    jdgn = np.gradient(n_hat) / np.gradient(x) * b

                # discrete version
                jdg_n = np.gradient(n_hat) / np.gradient(x) * b
                plt.add_data_set(x, jdgn, r"$hdev$")
                plt.add_data_set(x, jdg_n, r"$numerical$")

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="iv" + self.name,
            x_specifier=specifiers.VOLTAGE,
            y_specifier=specifiers.CURRENT,
            y_label=r"$J\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            style="color",
            y_log=True,
            legend_location="upper left",
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_B"].to_numpy() - df["V_E"].to_numpy())
                ic = np.abs(df["I_C"].to_numpy())
                ib = np.abs(df["I_B"].to_numpy())
                plt.add_data_set(v, ic * 1e3 / (1e6 * 1e6), r"$J_{\mathrm{C}}$")
                plt.add_data_set(v, ib * 1e3 / (1e6 * 1e6), r"$J_{\mathrm{B}}$")

        return plt

    @plot
    def plot_ft(self):
        plt = Plot(
            "F_T(I_C)",
            num="transit frequency " + self.name,
            x_specifier=specifiers.CURRENT + "C",
            x_label=r"$J_{\mathrm{C}} \left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            y_specifier=specifiers.TRANSIT_FREQUENCY,
            y_scale=1e-9,
            x_scale=1e3 / (1e6 * 1e6),
            style="color",
            x_log=True,
        )
        for key in self.dut.data.keys():
            if "iv" in key:  # conductance from numerical derivative of DC
                df = self.dut.data[key]
                freq = np.unique(df[specifiers.FREQUENCY].to_numpy())

                df_ac = df[df[specifiers.FREQUENCY] == freq[0]]
                v = np.abs(df_ac["V_B"].to_numpy() - df_ac["V_E"].to_numpy())
                i = df_ac["I_C"].to_numpy()
                ft = df_ac[specifiers.TRANSIT_FREQUENCY].to_numpy()

                plt.add_data_set(
                    i,
                    ft,
                )

        plt.x_limits = (1e-3, None)
        return plt
