"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from DMT.config import DATA_CONFIG
from DMT.core import DutType, SimCon
from DMT.exceptions import SimulationUnsuccessful
from DMT.Hdev import DutHdev
from hdevpy.materials import materials
from abc import abstractclassmethod
from functools import wraps
import pandas as pd
import re
import numpy as np
import os


def plot(func):
    """Decorates a function as a supplier for a plot suitable the manual."""
    # pylint: disable=protected-access
    func._plot = True

    @wraps(func)
    def func_wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return func_wrapper


class hdevTestCase(object):
    def __init__(
        self,
        name,
        description,
        dut_type,
        inp_func,
        material,
        sweep,
        material_2=None,
    ):
        """Defines a hdev test case.

        Parameters
        ----------
        name : str
            The name of the test-case, should be unique.
        description : str
            A short description of the structure.
        dut_type : DMT.core.DutType
            A enumerator object that defines the type of the structure for post processing.
        inp_func : callable
            This is a function that returns a hdev input file when supplied with a hdev database material.
        material : dict
            A dictionary that defines the material for the simulation.
        hdev_old  : string
            Old hdev version to test.
        hdev_new  : string
            New hdev version to test and compare.
        sweep : DMT.core.Sweep
            A Sweep definition in accordance with DMT.
        material_2 : dict,None
            A possible second material for structures made of in-homogenous material.
        """
        material = materials[material]
        self.name = name
        self.description = description
        self.inp_func = inp_func
        self.material = material
        self.material_2 = None
        if material_2 is not None:
            self.material_2 = materials[material_2]
        self.sweep = sweep
        self.dut = None
        if dut_type is None:
            raise IOError("DutType is required.")
        self.dut_type = dut_type

    def test(self):
        """Run simulation and evaluate results."""
        self.simulate()
        try:
            results = self.eval_test()
            return results
        except SimulationUnsuccessful:
            print("Simulation failed!")
            print("Content of simlog:")
            path_log = os.path.join(self.dut.get_sim_folder(self.sweep), "sim.log")
            with open(path_log, "r") as f:
                print(f.read())

            return 0

    def simulate(self):
        """Generate input files and run simulations"""
        # get the simulator commands for both hdev versions
        pattern = r"hdev_v(.*)"
        COMMAND_hdev = "hdev"

        if self.material_2 is None:
            inp_new = self.inp_func("test", self.material)
        else:
            inp_new = self.inp_func("test", self.material, self.material_2)

        self.dut = DutHdev(
            None,
            self.dut_type,
            inp_new,
            simulator_command=COMMAND_hdev,
            reference_node="A",
            force=True,
        )

        dmt = SimCon(t_max=360, n_core=1)
        dmt.append_simulation(self.dut, self.sweep)
        dmt.run_and_read(force=True)

    def eval_test(self, tests):
        """return some generic information on the simulation."""
        results = pd.DataFrame()
        # simulation time
        sim_time = self.dut.validate_simulation_successful(self.sweep)
        # inp file
        inp_file = os.path.join(self.dut.get_sim_folder(self.sweep), "hdev_inp.din")
        for _test_name, test_settings in tests.items():
            test_result = self.eval_test_generic(test_settings)
            test_result = pd.DataFrame.from_dict(test_result)
            results = pd.concat([results, test_result])

        print(results)
        return inp_file, results, sim_time

    def eval_test_generic(self, test):
        """Function that checks the results of a test dictionary.
        Parameters
        ----------
        test     : dict
            A dictionary that defines variables to be tested in the output data of hdev.
        """
        # evaluate selected inqu quantities
        test_result = {}
        for key in self.dut.data.keys():
            if test["key"] in key:
                df = self.dut.data[key]
                for col in test["cols"]:
                    name = key.split("/")[-1]
                    test_result["dataset"] = [name]
                    test_result["col"] = [col]
                    test_result["n_col"] = [test["n_col"]]
                    test_result["value"] = [df[col].to_numpy()[test["n_col"]]]

        return test_result

    def get_plots(self):
        """Go through all plot functions and plot them"""
        methods = [func for func in dir(self) if callable(getattr(self, func))]
        plots = []
        for method in methods:
            attr = getattr(self, method)
            if hasattr(attr, "_plot"):
                plots.append(attr())

        return plots
        # if (len(plots)>1):
        #     for plot in plots[-2]:
        #         plot.plot_py(show=False)

        # plots[-1].plot_py(show=True)
