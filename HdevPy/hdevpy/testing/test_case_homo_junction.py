"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import homo_junction
from hdevpy.tools import get_default_plot
from DMT.core import specifiers, Sweep, Plot, DutType, Plot2YAxis
import numpy as np


class TestCaseHomoJunction(hdevTestCase):
    def __init__(
        self,
        material,
        doping_left,
        doping_right,
        length,
        normalization=False,
    ):
        name = "Homo_Junction_" + material
        self.normalization = normalization
        inp_func = lambda ver, mat: homo_junction(
            ver, length, doping_left, doping_right, mat, normalization=normalization
        )
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0.01, 1, 26],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "A 1D homo junction structure with a contact on the left and the right side. The left contact is 'C', the right one 'A'."
        super().__init__(
            name,
            description,
            DutType.diode,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_profile(self):
        plt_dop = get_default_plot("dop", y_limits=(1e13, 1e16))
        plt_grading = get_default_plot("grading", y_limits=(-10, 100))
        plt_profile = Plot2YAxis(
            "Profile of " + self.name + ".",
            plt_dop,
            plt_grading,
            legend_location="upper right outer",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df_inqu = self.dut.data[key]
                plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
                plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")
                break

        return plt_profile

    @plot
    def plot_band(self):
        plt = Plot(
            "E(x)",
            num="Band diagram of " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                ec = df["EC"]
                ev = df["EV"]
                efn = df["EF|ELECTRONS"]
                efp = df["EF|HOLES"]
                plt.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
                plt.add_data_set(x, ev, r"$E_{\mathrm{V}}$")
                plt.add_data_set(x, efn, r"$E_{\mathrm{Fn}}$")
                plt.add_data_set(x, efp, r"$E_{\mathrm{Fp}}$")

        return plt

    @plot
    def plot_quants(self):
        plt = Plot(
            "Pot(x)",
            num="Electrostatic potential " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                psi = df["psi_semi"]
                plt.add_data_set(x, psi, r"$\Psi$")

        return plt

    @plot
    def plot_dens(self):
        plt = Plot(
            "Dens(x)",
            num="Carrier density " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_label=r"$c/\left( \si{\per\cubic\meter} \right)$",
            y_log=True,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                cn = df["N"]
                cp = df["P"]
                plt.add_data_set(x, cn, r"$n$")
                plt.add_data_set(x, cp, r"$p$")

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="Current density at contact 'C' of " + self.name + ".",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            x_label=r"$V_{\mathrm{CA}}\left( \si{\volt} \right)$",
            style="color",
            y_log=True,
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_A"].to_numpy() - df["V_C"].to_numpy())
                i = np.abs(df["I_C"].to_numpy())
                in_ = np.abs(df["I_C|ELECTRONS"].to_numpy())
                ip_ = np.abs(df["I_C|HOLES"].to_numpy())
                plt.add_data_set(v, i * 1e3 / (1e6 * 1e6), style="xk", label="total")
                plt.add_data_set(
                    v, in_ * 1e3 / (1e6 * 1e6), style="-b", label="electrons"
                )
                plt.add_data_set(
                    v, ip_ * 1e3 / (1e6 * 1e6), style="-.sm", label="holes"
                )

        return plt
