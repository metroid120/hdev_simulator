"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import resistor
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers
import numpy as np
import re


class TestCaseResistor(hdevTestCase):
    def __init__(self, material, doping, length, ydir=False):
        self.ydir = ydir
        if self.ydir:
            name = "2D_Resistor_" + material
        else:
            name = "Resistor_" + material

        inp_func = lambda version, mat: resistor(
            version, length, doping, mat, ydir=ydir
        )
        sweepdef = [
            {
                "var_name": specifiers.FREQUENCY,
                "sweep_order": 3,
                "sweep_type": "CONST",
                "value_def": [1e3],
            },
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0, 1, 11],
            },
            # {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':1, 'sweep_type':'LIST', 'value_def':[0.5,1]},
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        if not self.ydir:
            description = "A 1D resistor structure with constant doping, a contact on the left and one at the right. The left contact is called 'C', the right one 'A'."
        else:
            description = "A 2D resistor structure with constant doping, a contact on the bottom and one on the top. The width of the structure is one um. The bottom contact is called 'C', the top one 'A'."

        super().__init__(
            name,
            description,
            DutType.res,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_band(self):
        if self.ydir:
            x_label = r"$y\left(\si{\micro\meter}\right)$"
        else:
            x_label = r"$x\left(\si{\micro\meter}\right)$"

        plt = Plot(
            "E(x)",
            num="Band Diagram " + self.name + ".",
            x_label=x_label,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "op5_inqu" in key:
                df = self.dut.data[key]
                x = df["X"].to_numpy()
                if self.ydir:
                    y = df["Y"].to_numpy()
                ec = df["EC"].to_numpy()
                ev = df["EV"].to_numpy()
                efn = df["EF|ELECTRONS"].to_numpy()
                efp = df["EF|HOLES"].to_numpy()
                if self.ydir:
                    n = len(np.unique(x))
                    plt.add_data_set(y[::n], ec[::n], r"$E_{\mathrm{C}}$")
                    plt.add_data_set(y[::n], ev[::n], r"$E_{\mathrm{V}}$")
                    plt.add_data_set(y[::n], efn[::n], r"$E_{\mathrm{Fn}}$")
                    plt.add_data_set(y[::n], efp[::n], r"$E_{\mathrm{Fp}}$")
                else:
                    plt.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
                    plt.add_data_set(x, ev, r"$E_{\mathrm{V}}$")
                    plt.add_data_set(x, efn, r"$E_{\mathrm{Fn}}$")
                    plt.add_data_set(x, efp, r"$E_{\mathrm{Fp}}$")

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="I-V characteristics " + self.name + ".",
            x_label=r"$V_{\mathrm{CA}}\left( \si{\volt} \right)$",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            style="color",
        )
        scale = 1e3 / (1e6 * 1e6)
        if self.ydir:
            scale = scale * 1e6
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_A"].to_numpy() - df["V_C"].to_numpy())
                i = np.abs(df["I_C"].to_numpy())
                plt.add_data_set(
                    v,
                    i * scale,
                )

        return plt

    @plot
    def plot_conductance(self):
        plt = Plot(
            "G(V)",
            num="Conductance " + self.name + ".",
            x_label=r"$V_{\mathrm{CA}}\left( \si{\volt} \right)$",
            y_label=r"$\mathrm{d}J_{\mathrm{C}}/\mathrm{d}V_{\mathrm{CA}}\left( \si{\milli\siemens\per\square\micro\meter} \right)$",
            style="color",
        )
        # ADD DC Data
        for key in self.dut.data.keys():
            if "iv" in key:  # conductance from numerical derivative of DC
                df = self.dut.data[key]
                freq = np.unique(df[specifiers.FREQUENCY].to_numpy())

                df_dc = df[df[specifiers.FREQUENCY] == freq[0]]
                v = np.abs(df_dc["V_C"].to_numpy() - df_dc["V_A"].to_numpy())
                i = np.abs(df_dc["I_C"].to_numpy())

                df_ac = df[df[specifiers.FREQUENCY] == freq[0]]
                v = np.abs(df_ac["V_C"].to_numpy() - df_ac["V_A"].to_numpy())
                rey11 = np.real(df_ac[specifiers.SS_PARA_Y + "C" + "C"].to_numpy())

                scale = 1e3 / (1e6 * 1e6)
                if self.ydir:
                    scale = scale * 1e6

                plt.add_data_set(np.flip(v), np.gradient(i, v) * scale, "DC")
                plt.add_data_set(np.flip(v), rey11 * scale, "AC")

        return plt
