"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import resistor_u
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers
import numpy as np
import re
import os


class TestCaseUResistor(hdevTestCase):
    def __init__(self, material, doping, extrude=False):
        self.extrude = extrude

        if self.extrude:
            name = "2D_Resistor_2D_Contacts_U_Shape" + material
        else:
            name = "2D_Resistor_1D_Contacts_U_Shape" + material

        inp_func = lambda version, mat: resistor_u(
            version, doping, mat, extrude=extrude
        )
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0, 1, 11],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        if self.extrude:
            description = "A 2D u-shaped resistor structure with constant doping and some oxide between two 2D contacts. One contact is called 'C', the other one 'A'."
        else:
            description = "A 2D u-shaped resistor structure with constant doping and some oxide between two 1D line contacts. One contact is called 'C', the other one 'A'."
        super().__init__(
            name,
            description,
            DutType.res,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5_dd_inqu",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="I-V " + self.name + ".",
            x_label=r"$V_{\mathrm{CA}}\left( \si{\volt} \right)$",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\micro\meter} \right)$",
            style="color",
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_A"].to_numpy() - df["V_C"].to_numpy())
                i = np.abs(df["I_C"].to_numpy())
                plt.add_data_set(
                    v,
                    i * 1e3 / (1e6),
                )

        return plt

    @plot
    def plot_curr_dens(self):
        for key in self.dut.data.keys():
            if "op5_inqu" in key:
                import matplotlib.pyplot as mplt
                import tikzplotlib

                mplt.rc("text", usetex=True)

                df = self.dut.data[key]
                x = np.unique(df["X"].to_numpy())
                y = np.unique(df["Y"].to_numpy())
                jx = df["J|XDIR"].to_numpy()
                jy = df["J|YDIR"].to_numpy()
                psi = df["psi_semi"].to_numpy()

                jx = jx.reshape(len(x), len(y))
                jy = jy.reshape(len(x), len(y))
                psi = psi.reshape(len(x), len(y))
                j = np.sqrt(jx ** 2 + jy ** 2)
                # mplt.contourf(x, y, j)
                # mplt.show()

                fig = mplt.figure()
                ax = fig.add_subplot(111)
                # color = 2 * np.log(np.hypot(jx, jy))
                cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
                ax.streamplot(
                    x * 1e6,
                    y * 1e6,
                    jx,
                    jy,
                    # color=color,
                    linewidth=1,
                    cmap=mplt.cm.inferno,
                    density=2,
                    arrowstyle="->",
                    arrowsize=1.5,
                )
                ax.set_xlabel(r"$x ( \mu m )$")
                ax.set_ylabel(r"$y ( \mu m )$")
                ax.set_xlim(0, 1)
                ax.set_ylim(0, 1)
                ax.set_aspect("equal")
                cbar = fig.colorbar(cs)
                cbar.ax.set_ylabel(r"$\Psi ( \mathrm{V} )$")
                cwd = os.getcwd()
                path_ = os.path.join(
                    cwd, "documentation/manual/fig", "curr_dens" + self.name
                )
                mplt.savefig(fname=path_)
                # tikzplotlib.save(path_)

                break

        return (
            path_,
            r"Current density (arrows) and potential (contours) in the u-shaped resistor.",
        )
