"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from DMT.core import SimCon, Sweep, DutType, specifiers, Plot, constants
from DMT.Hdev import DutHdev
from hdevpy.devices import n3_hbt, sige_diode
from hdevpy.tools import get_sweep, get_hdev_dfs, get_default_plot
import numpy as np

"""This script can be tested to check the BGN narrowing implementation with Fermi-Dirac statistics."""


# can use either a diode or an hbt for the test:
diode = False
hbt = True

if diode and hbt:
    raise IOError

if not diode and not hbt:
    raise IOError

if diode:
    inp_boltz = sige_diode("hdev", fermi=False)
    inp_fermi = sige_diode("hdev", fermi=True)
else:
    inp_boltz = n3_hbt("hdev", fermi=False)
    inp_fermi = n3_hbt("hdev", fermi=True)


if diode:
    sweepdef = [
        {
            "var_name": specifiers.VOLTAGE + "A",
            "sweep_order": 2,
            "sweep_type": "CON",
            "value_def": [0],
        },
        {
            "var_name": specifiers.VOLTAGE + "C",
            "sweep_order": 1,
            "sweep_type": "LIN",
            "value_def": [0, 0.2, 21],
        },
    ]
    outputdef = []
    othervar = {"TEMP": 200}
    sweep = Sweep("gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

elif hbt:
    sweep = get_sweep(vbe=[0, 1, 101], vbc=[0.0])


dut_hdev_boltz = DutHdev(None, DutType.npn, inp_boltz, reference_node="E")
dut_hdev_fermi = DutHdev(None, DutType.npn, inp_fermi, reference_node="E")

# start the simulation
sim_con = SimCon(n_core=1, t_max=30)
sim_con.append_simulation(dut_hdev_boltz, sweep)
sim_con.run_and_read(force=False)
sim_con.append_simulation(dut_hdev_fermi, sweep)
sim_con.run_and_read(force=True)

df_iv_boltz, df_equ_boltz = get_hdev_dfs(dut_hdev_boltz, sweep)
df_iv_fermi, df_equ_fermi = get_hdev_dfs(dut_hdev_fermi, sweep)

plt_iv = get_default_plot(
    "gum", prefix="IV", x_limits=(0.3, None), y_limits=(1e-9, None)
)
if hbt:
    plt_iv.add_data_set(df_iv_boltz["V_B"], df_iv_boltz["I_C"], label="Fermi")
    plt_iv.add_data_set(df_iv_fermi["V_B"], df_iv_fermi["I_C"], label="Boltz")
    plt_ft = get_default_plot(
        "ft", prefix="IV", x_limits=(0.3, None), y_limits=(1e-9, None)
    )
    plt_ft.add_data_set(df_iv_boltz["I_C"], df_iv_boltz["F_T"], label="Boltz")
    plt_ft.add_data_set(df_iv_fermi["I_C"], df_iv_fermi["F_T"], label="Fermi")
    plt_ft.plot_py(show=False)
if diode:
    plt_iv.add_data_set(df_iv_boltz["V_C"], df_iv_boltz["I_C"], label="Fermi")
    plt_iv.add_data_set(df_iv_fermi["V_C"], df_iv_fermi["I_C"], label="Boltz")

plt_iv.plot_py(show=False)
plt_bands = get_default_plot("band", style="xtraction_color")
plt_bands.add_data_set(df_equ_boltz["X"], df_equ_boltz["EC"], label="Ec Boltz")
plt_bands.add_data_set(df_equ_fermi["X"], df_equ_fermi["EC"], label="Ec Fermi")
plt_bands.add_data_set(df_equ_boltz["X"], df_equ_boltz["EV"], label="Ev Boltz")
plt_bands.add_data_set(df_equ_fermi["X"], df_equ_fermi["EV"], label="Ev Fermi")
plt_bands.plot_py(show=False)
plt_dens = get_default_plot("dens", prefix="dens", style="xtraction_color")
plt_dens.add_data_set(df_equ_boltz["X"], df_equ_boltz["N"], label="n boltz")
plt_dens.add_data_set(df_equ_boltz["X"], df_equ_fermi["N"], label="n fermi")
plt_dens.add_data_set(df_equ_boltz["X"], df_equ_boltz["P"], label="p boltz")
plt_dens.add_data_set(df_equ_boltz["X"], df_equ_fermi["P"], label="p fermi")
plt_dens.add_data_set(df_equ_boltz["X"], df_equ_fermi["NNET"], style="-k")
plt_dens.plot_py(show=False)
plt_un = Plot(
    "bgn(x)",
    y_label=r"bgn",
    style="mix",
    x_label=r"$x\left( \si{\nano\meter}\right)$",
    x_scale=1e9,
    legend_location="upper left",
)
plt_un.add_data_set(df_equ_boltz["X"], df_equ_boltz["BGN"], label="boltz")
plt_un.add_data_set(df_equ_fermi["X"], df_equ_fermi["BGN"], label="fermi")
plt_un.plot_py(show=True)
