"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import hetero_junction
from hdevpy.tools import get_default_plot
from DMT.core import specifiers, Sweep, Plot, DutType, Plot2YAxis
import numpy as np


class TestCaseHeteroJunction(hdevTestCase):
    def __init__(
        self,
        material_left,
        material_right,
        length,
        p_doping=False,
        thermionic_emission=False,
    ):
        name = "Hetero_Junction_" + material_left + "_" + material_right
        if p_doping:
            name = name + "_Doped"
        if thermionic_emission:
            name = name + "_Thermionic_Field_Emission_BC"

        inp_func = lambda version, mat1, mat2: hetero_junction(
            version,
            length,
            mat1,
            mat2,
            p_doping=p_doping,
            thermionic_emission=thermionic_emission,
        )
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0.01, 1, 101],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "A 1D hetero junction structure with a contact on the left and the right side. Constant doping. Two materials."
        super().__init__(
            name,
            description,
            DutType.diode,
            inp_func,
            material_left,
            sweep,
            material_2=material_right,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_band(self):
        plt_band = Plot(
            "E(x)",
            num="Band diagram " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df_inqu = self.dut.data[key]
                ec = df_inqu["EC"]
                ev = df_inqu["EV"]
                plt_band.add_data_set(
                    df_inqu["X"], ec, label=r"$E_{\mathrm{C}}$", style="-k"
                )
                plt_band.add_data_set(
                    df_inqu["X"], ev, label=r"$E_{\mathrm{V}}$", style="--r"
                )
                plt_band.add_data_set(
                    df_inqu["X"],
                    -df_inqu["PHI|N"],
                    label=r"$E_{\mathrm{Fn}}$",
                    style="--bx",
                )
                plt_band.add_data_set(
                    df_inqu["X"],
                    -df_inqu["PHI|P"],
                    label=r"$E_{\mathrm{Fp}}$",
                    style="-.m",
                )

        plt_band.y_limits = (np.min(ev) - 0.1, np.max(ec) + 0.1)

        return plt_band

    @plot
    def plot_quants(self):
        plt = Plot(
            "Pot(x)",
            num="Electrostatic potential " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_log=False,
            y_label=r"$\Psi\left(\si{\volt}\right)$",
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op5_" in key:
                df = self.dut.data[key]
                x = df["X"]
                psi = df["psi_semi"]
                plt.add_data_set(x, psi, r"$\Psi$")

        return plt

    @plot
    def plot_dens(self):
        plt = Plot(
            "Dens(x)",
            num="Carrier density " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e-6,
            y_label=r"$c/\left( \si{\per\cubic\centi\meter} \right)$",
            y_log=True,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op5_" in key:
                df = self.dut.data[key]
                x = df["X"]
                cn = df["N"]
                cp = df["P"]
                plt.add_data_set(x, cn, r"$n$")
                plt.add_data_set(x, cp, r"$p$")

        plt.y_limits = (np.min(cp) / 0.5, np.max(cn) * 2)

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="I-V characteristics " + self.name + ".",
            x_label=r"$V_{\mathrm{CA}}\left( \si{\volt} \right)$",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            style="color",
            y_log=True,
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_A"].to_numpy() - df["V_C"].to_numpy())
                i = np.abs(df["I_C"].to_numpy())
                plt.add_data_set(
                    v,
                    i * 1e3 / (1e6 * 1e6),
                )

        return plt

    @plot
    def plot_profile(self):
        plt_dop = get_default_plot("dop", y_limits=(1e13, 1e16))
        plt_grading = get_default_plot("grading", y_limits=(-10, 100))
        plt_profile = Plot2YAxis(
            "Profile of " + self.name + ".",
            plt_dop,
            plt_grading,
            legend_location="upper right outer",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df_inqu = self.dut.data[key]
                plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
                plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")
                break

        return plt_profile
