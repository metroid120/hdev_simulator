"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import bjt, n3_hbt
from hdevpy.tools import fd1h, set_force, get_default_plot
from DMT.core import (
    specifiers,
    Sweep,
    Plot,
    DutType,
    sub_specifiers,
    constants,
    Plot2YAxis,
)
import numpy as np
import scipy.integrate as integrate


def get_n3_hbt(*args, nl=False, tunnel=False, **kwargs):
    inp = n3_hbt(*args, **kwargs, spring=True)
    for i, mob_def in enumerate(inp["MOB_DEF"]):
        if mob_def["mod_name"] == "SSGS":
            if mob_def["valley"] == "H":
                inp["MOB_DEF"][i]["hc_scat_type"] = "default"
            if mob_def["valley"] in ["X", "x"]:
                inp["MOB_DEF"][i]["beta"] = 1.3

    if nl:
        # active nonlocal model
        inp = set_force(inp, "phi")
        inp["NON_LOCAL"] = {}
        inp["NON_LOCAL"]["type"] = "simple"
        inp["NON_LOCAL"]["lambda"] = 10e-9
        inp["NON_LOCAL"]["xj_l"] = 20e-9
        for i in range(len(inp["MOB_DEF"])):
            inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1

        # decrease lattice mobility
        for l, mob in enumerate(inp["MOB_DEF"]):
            if inp["MOB_DEF"][l]["valley"] == "X":
                inp["MOB_DEF"][l]["mu_min_a"] = inp["MOB_DEF"][l]["mu_min_a"] * 0.4
                inp["MOB_DEF"][l]["mu_min_d"] = inp["MOB_DEF"][l]["mu_min_d"] * 0.1

    if tunnel:
        inp["TUNNEL"] = {}
        inp["TUNNEL"]["activate"] = 1
        inp["TUNNEL"]["factor"] = 100
        inp["TUNNEL"]["m"] = 1  # m_tunnel/m0

    return inp


class TestCaseN3HBT(hdevTestCase):
    def __init__(
        self,
        material,
        nl=False,
        et=False,
        bohm=False,
        fermi=False,
        normalization=False,
        tunnel=False,
    ):
        name = "N3_HBT_" + material
        self.fermi = fermi
        self.bohm = bohm
        self.nl = nl
        self.et = et
        self.tunnel = tunnel
        if self.et:
            name = name + "_Energy_Transport"
        elif self.nl:
            name = name + "_augmented_Drift_Diffusion"
        else:
            name = name + "_Drift_Diffusion"
        if self.fermi:
            name = name + "_Fermi_Dirac"
        if self.bohm:
            name = name + "_Bohm"

        inp_func = lambda ver, mat: get_n3_hbt(
            ver,
            fermi=self.fermi,
            bohm=self.bohm,
            tn=self.et,
            nl=self.nl,
            tunnel=self.tunnel,
        )
        sweepdef = [
            {
                "var_name": specifiers.FREQUENCY,
                "sweep_order": 3,
                "sweep_type": "CON",
                "value_def": [1e3],
            },
            {
                "var_name": specifiers.VOLTAGE + "E",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            # {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':2, 'sweep_type':'CON', 'value_def':[1]},
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "SYNC",
                "master": "V_B",
                "offset": 0,
            },
            # {'var_name':specifiers.VOLTAGE+'B', 'sweep_order':1, 'sweep_type':'LIN', 'value_def':[0.01,0.7,101]},
            {
                "var_name": specifiers.VOLTAGE + "B",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0, 0.95, 96],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "ITRS Node 1 SiGe HBT."
        super().__init__(
            name,
            description,
            DutType.npn,
            inp_func,
            material,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test3": {
                "name": "jp",
                "cols": ["J|P|XDIR"],
                "key": "op5_inqu",
                "n_col": 10,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 3,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_dens(self):
        """Plot of the density alongside the evaluation of the boltzmann or fermi-dirac integral to check the hdev implementation"""
        plt = Plot(
            "Dens(x)",
            num="Carrier Density " + self.name + ".",
            style="xtraction_color",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_label=r"$c/\left( \si{\per\cubic\centi\meter} \right)$",
            y_scale=1e-6,
            y_log=True,
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                cn = df["N"]
                cp = df["P"]

                plt.add_data_set(x, cn, r"$n$ hdev")
                # now we evaluate the boltzmann and fermi dirac-integral in DMT
                n_eff = df["n_eff"].to_numpy()
                ec = df["EC"].to_numpy()
                ef = df["EF|ELECTRONS"].to_numpy()
                vt = constants.P_K * 300 / constants.P_Q
                nu = (ef - ec) / vt
                if self.fermi:

                    def integrand_fermi_i(x, ef, i):
                        return 2 / np.sqrt(np.pi) * x ** i / (np.exp(x - ef) + 1)

                    n = np.zeros(np.size(cn))
                    for i, nu_ in enumerate(nu):
                        fermi_05 = integrate.quad(
                            lambda e: integrand_fermi_i(e, nu_, 0.5), 0, np.infty
                        )[0]
                        # fermi_05  = fd1h(nu_) #approximation that is also in hdev
                        n[i] = n_eff[i] * fermi_05
                else:
                    n = n_eff * np.exp((ef - ec) / vt)

                if self.fermi:
                    plt.add_data_set(
                        x,
                        n,
                        label=r"DMT :$N_{\mathrm{C}}\mathcal{F}_{1/2}\left(\eta_{\mathrm{n}}\right)$",
                    )
                else:
                    plt.add_data_set(
                        x,
                        n,
                        label=r"DMT :$N_{\mathrm{C}}\exp\left(\eta_{\mathrm{n}}\right)$",
                    )

                plt.add_data_set(x, cp, r"$p$ hdev")

                # now we evaluate the boltzmann and fermi dirac-integral in DMT
                p_eff = df["p_eff"].to_numpy()
                ev = df["EV"].to_numpy()
                ef = df["EF|HOLES"].to_numpy()
                vt = constants.P_K * 300 / constants.P_Q
                nu = -(ef - ev) / vt
                if self.fermi:

                    def integrand_fermi_i(x, ef, i):
                        return 2 / np.sqrt(np.pi) * x ** i / (np.exp(x - ef) + 1)

                    p = np.zeros(np.size(cp))
                    for i, nu_ in enumerate(nu):
                        fermi_05 = integrate.quad(
                            lambda e: integrand_fermi_i(e, nu_, 0.5), 0, np.infty
                        )[0]
                        fermi_05 = fd1h(nu_)
                        p[i] = p_eff[i] * fermi_05
                else:
                    p = p_eff * np.exp(nu)

                if self.fermi:
                    plt.add_data_set(
                        x,
                        p,
                        label=r"DMT :$N_{\mathrm{V}}\mathcal{F}_{1/2}\left(\eta_{\mathrm{p}}\right)$",
                    )
                else:
                    plt.add_data_set(
                        x,
                        p,
                        label=r"DMT :$N_{\mathrm{V}}\exp\left(\eta_{\mathrm{p}}\right)$",
                    )

        return plt

    @plot
    def plot_profile(self):
        plt_dop = get_default_plot("dop", y_limits=(1e13, 1e21))
        plt_grading = get_default_plot("grading", y_limits=(-10, 40))
        plt_profile = Plot2YAxis(
            "Profile of " + self.name + ".",
            plt_dop,
            plt_grading,
            legend_location="upper right outer",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df_inqu = self.dut.data[key]
                plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
                plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")
                break

        return plt_profile

    @plot
    def plot_band(self):
        plt_band = Plot(
            "E(x)",
            num="Band diagram " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key and not "ac" in key:
                df_inqu = self.dut.data[key]
                ec = df_inqu["EC"]
                ev = df_inqu["EV"]
                plt_band.add_data_set(
                    df_inqu["X"], ec, label=r"$E_{\mathrm{C}}$", style="-k"
                )
                plt_band.add_data_set(
                    df_inqu["X"], ev, label=r"$E_{\mathrm{V}}$", style="--r"
                )
                plt_band.add_data_set(
                    df_inqu["X"],
                    -df_inqu["PHI|N"],
                    label=r"$E_{\mathrm{Fn}}$",
                    style="--bx",
                )
                plt_band.add_data_set(
                    df_inqu["X"],
                    -df_inqu["PHI|P"],
                    label=r"$E_{\mathrm{Fp}}$",
                    style="-.m",
                )

        plt_band.y_limits = (np.min(ev) - 0.1, np.max(ec) + 0.1)

        return plt_band

    @plot
    def plot_quants(self):
        plt = Plot(
            "Pot(x)",
            num="Electrostatic Potential " + self.name + ".",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                psi = df["psi_semi"]
                plt.add_data_set(x, psi, r"$\Psi$")

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="I-V Characteristics " + self.name + ".",
            x_label=r"$V_{\mathrm{BE}}\left( \si{\volt} \right)$",
            y_label=r"$J_{\mathrm{C}}\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            style="color",
            y_log=True,
            legend_location="upper left",
        )
        plt.y_limits = (1e-5, 1e2)
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_B"].to_numpy() - df["V_E"].to_numpy())
                ic = np.abs(df["I_C"].to_numpy())
                ib = np.abs(df["I_B"].to_numpy())
                plt.add_data_set(v, ic * 1e3 / (1e6 * 1e6), r"$J_{\mathrm{C}}$")
                plt.add_data_set(v, ib * 1e3 / (1e6 * 1e6), r"$J_{\mathrm{B}}$")

        return plt

    @plot
    def plot_conductance(self):
        plt = Plot(
            "F_T(I_C)",
            num="Transit Frequency " + self.name + ".",
            x_specifier=specifiers.CURRENT + "C",
            x_label=r"$J_{\mathrm{C}} \left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            x_scale=1,
            y_specifier=specifiers.TRANSIT_FREQUENCY,
            y_scale=1e-9,
            style="color",
            x_log=True,
        )
        plt.x_limits = (1e-5, None)
        for key in self.dut.data.keys():
            if "iv" in key:  # conductance from numerical derivative of DC
                df = self.dut.data[key]
                freq = np.unique(df[specifiers.FREQUENCY].to_numpy())

                df_ac = df[df[specifiers.FREQUENCY] == freq[0]]
                i = df_ac["I_C"].to_numpy()
                ft = df_ac[specifiers.TRANSIT_FREQUENCY].to_numpy()

                plt.add_data_set(
                    i * 1e3 / (1e6 * 1e6),
                    ft,
                )

        return plt
