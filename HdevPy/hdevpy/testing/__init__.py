"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
name = "testing"

from .hdev_test_case import hdevTestCase
from .test_case_2dplate_cap import TestCase2DPlateCap
from .test_case_bjt import TestCaseBjt
from .test_case_graded_hetero_junction import TestCaseGradedHeteroJunction
from .test_case_hbt_bohm import TestCaseHBTBohm
from .test_case_hbt import TestCaseHBT
from .test_case_hetero_junction import TestCaseHeteroJunction
from .test_case_homo_junction import TestCaseHomoJunction
from .test_case_n3_hbt import TestCaseN3HBT
from .test_case_npnnp_bohm import TestCaseNPNNPBohm
from .test_case_resistor import TestCaseResistor
from .test_case_triode import TestCaseTriode
from .test_case_tunnel import TestCaseTunnel
from .test_case_u_resistor import TestCaseUResistor
