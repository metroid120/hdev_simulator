"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import bjt, tunnel_resistor
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers, constants
import numpy as np
import scipy.integrate as integrate


class TestCaseTunnel(hdevTestCase):
    def __init__(self, material, hdev_old, hdev_new, fermi=False):
        name = "tunnel_resistor" + material
        self.fermi = fermi
        if self.fermi:
            name = name + "_fermi_dirac"

        inp_func = lambda ver, mat: tunnel_resistor(ver, fermi)
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "LIN",
                "value_def": [0.1, 0.2, 21],
            },
            # {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':1, 'sweep_type':'CON', 'value_def':[0.1]},
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "Silicon Test structure for tunneling model."
        super().__init__(
            name,
            description,
            DutType.npn,
            inp_func,
            material,
            hdev_old,
            hdev_new,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op1",
                "n_col": 10,
            },
            # 'test2':{
            #     'name' :'jn',
            #     'cols' :['J|N|XDIR'],
            #     'key'  : 'op1_dd_inqu',
            #     'n_col': 10,
            # },
            # 'test4':{
            #     'name' :'iv',
            #     'cols' :['I_C'],
            #     'key'  : 'iv',
            #     'n_col': 1,
            # },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_band(self):
        plt = Plot(
            "E(x)",
            num="e" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                ec = df["EC"]
                efn = df["EF|ELECTRONS"]
                plt.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
                plt.add_data_set(x, efn, r"$E_{\mathrm{Fn}}$")
        return plt

    @plot
    def plot_quants(self):
        plt = Plot(
            "Pot(x)",
            num="psi" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                psi = df["psi_semi"]
                plt.add_data_set(x, psi, r"$\Psi$")

        return plt

    @plot
    def plot_iv(self):
        plt = Plot(
            "I(V)",
            num="iv" + self.name,
            x_specifier=specifiers.VOLTAGE,
            y_specifier=specifiers.CURRENT,
            y_label=r"$J\left( \si{\milli\ampere\per\square\micro\meter} \right)$",
            style="color",
            y_log=False,
            legend_location="upper left",
        )
        for key in self.dut.data.keys():
            if "iv" in key:
                df = self.dut.data[key]
                v = np.abs(df["V_C"].to_numpy() - df["V_A"].to_numpy())
                ic = np.abs(df["I_C"].to_numpy())
                plt.add_data_set(v, ic * 1e3 / (1e6 * 1e6), r"$J_{\mathrm{C}}$")

        return plt

    @plot
    def plot_doping(self):
        plt = Plot(
            "N(x)",
            num="doping " + self.name,
            x_label=r"$x \left( \si{\micro\meter} \right)$",
            y_label=r"$\left| N_{\mathrm{I}} \right| \left( \si{\per\cubic\centi\meter} \right)$",
            y_scale=1e-6,
            x_scale=1e6,
            style="color",
            y_log=True,
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                dop = df["NNET"]
                plt.add_data_set(
                    x,
                    np.abs(dop),
                )

        return plt

    @plot
    def plot_gtun(self):
        plt = Plot(
            "G(x)",
            num="gtun " + self.name,
            x_label=r"$x \left( \si{\micro\meter} \right)$",
            y_label=r"$G_{\mathrm{tun}} \left( \si{\per\cubic\centi\meter} \right)$",
            y_scale=1e-6,
            x_scale=1e6,
            style="color",
            y_log=False,
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                gen_n_tun = df["gen_n_tun"]
                plt.add_data_set(
                    x,
                    gen_n_tun,
                )
        # for key in self.dut.data.keys():
        #     if 'tunnel' in key:
        #         df   = self.dut.data[key]
        #         x_l  = df['x_l'].to_numpy()
        #         x_r  = df['x_r'].to_numpy()
        #         ec_l = df['ec_l'].to_numpy()
        #         ec_r = df['ec_r'].to_numpy()
        #         t    = df['t'].to_numpy()
        #         gtun_hdev = df['gtun'].to_numpy()
        #         plt.add_data_set(
        #             x,
        #             gtun_hdev,
        #         )

        plt.x_limits = (0.4, 0.6)

        return plt

    # @plot
    # def plot_curr(self):
    #     plt = Plot(
    #         'J_n(x)',
    #         num='curr dens '+self.name,
    #         x_label=r'$x \left( \si{\micro\meter} \right)$',
    #         y_label=r'$J_{\mathrm{n}} \right| \left( \si{\milli\ampere\per\square\micro\meter} \right)$',
    #         x_scale=1e6,
    #         y_scale=1e3/(1e6*1e6),
    #         style='color',
    #         y_log=True,
    #     )
    #     for key in self.dut.data.keys():
    #         if 'inqu' in key and 'op50_' in key:
    #             df       = self.dut.data[key]
    #             x        = df['X']
    #             j        = df['J|N|XDIR'].to_numpy()
    #             cn       = df['N'].to_numpy()
    #             grad_phi = df['GPHI|N'].to_numpy()
    #             mu       = df['MU|N'].to_numpy()
    #             j_dmt    = -constants.P_Q*mu*cn*grad_phi
    #             plt.add_data_set(
    #                 x,
    #                 j,
    #                 label='hdev'
    #             )
    #             plt.add_data_set(
    #                 x,
    #                 j_dmt,
    #                 label=r'DMT :$-qn\mu_{\mathrm{n}}\nabla\phi_{\mathrm{n}}$'
    #             )

    #     return plt
