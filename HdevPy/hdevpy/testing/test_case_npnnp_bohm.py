"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from .hdev_test_case import hdevTestCase, plot
from hdevpy.devices import bjt, np_n_np_abrupt
from hdevpy.tools import fd1h
from DMT.core import specifiers, Sweep, Plot, DutType, sub_specifiers, constants
import numpy as np
import scipy.integrate as integrate


class TestCaseNPNNPBohm(hdevTestCase):
    """N+ N N+ with activated Bohm Potential"""

    def __init__(self, material, hdev_old, hdev_new):
        name = "npnnp_bohm_" + material

        inp_func = lambda ver, mat: np_n_np_abrupt(ver)
        sweepdef = [
            {
                "var_name": specifiers.VOLTAGE + "A",
                "sweep_order": 2,
                "sweep_type": "CON",
                "value_def": [0],
            },
            {
                "var_name": specifiers.VOLTAGE + "C",
                "sweep_order": 1,
                "sweep_type": "CON",
                "value_def": [0],
            },
        ]
        outputdef = []
        othervar = {"TEMP": 300}
        sweep = Sweep(
            "gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar
        )

        description = "Test structure for bohm potential."
        super().__init__(
            name,
            description,
            DutType.npn,
            inp_func,
            material,
            hdev_old,
            hdev_new,
            sweep,
        )

    def eval_test(self):
        tests = {
            "test1": {
                "name": "n dens",
                "cols": ["N"],
                "key": "op1_inqu",
                "n_col": 0,
            },
            "test2": {
                "name": "jn",
                "cols": ["J|N|XDIR"],
                "key": "op1_inqu",
                "n_col": 0,
            },
            "test4": {
                "name": "iv",
                "cols": ["I_C"],
                "key": "iv",
                "n_col": 0,
            },
        }
        results = super().eval_test(tests)
        print(results)
        return results

    @plot
    def plot_band(self):
        plt = Plot(
            "E(x)",
            num="e" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_specifier=specifiers.ENERGY,
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                ec = df["EC"]

                # get electron density and hole density, plot Bohm Potential
                cn = df["N"].to_numpy()

                hbar = 1.054e-34  # m^2 kg /s
                q = 1.602e-19  # C
                m = 9.1e-31  # kg effective mass silicon
                n_hat = np.sqrt(cn)
                b = (1 * hbar ** 2) / (6 * q * m)
                # m^4 kg^2 / s /s / C / kg * /m /m = m^2*kg/s^2/C = J/C = V
                un = (
                    (1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cn)))
                    / np.sqrt(cn)
                    / np.gradient(x)
                    / np.gradient(x)
                )

                jdg = np.gradient(n_hat) / np.gradient(x)
                rhs = un * np.sqrt(cn) - b * np.gradient(jdg) / np.gradient(x)

                ##solutions from hdev
                try:
                    jdgn = df["JDGN"].to_numpy()
                except:
                    jdgn = df["N"] * 0
                l_n = df["L_N"].to_numpy()
                try:
                    rhs_hdev = df["R_N"].to_numpy()
                except:
                    rhs_hdev = 0
                r_n = np.gradient(x) * un
                rhs = np.gradient(jdgn) - un * n_hat * np.gradient(x)

                # discrete version
                jdg_n = np.gradient(n_hat) / np.gradient(x) * b
                un_discrete = np.gradient(jdg_n) / np.gradient(x) / n_hat

                plt.add_data_set(x, ec, r"$E_{\mathrm{C}}$")
                plt.add_data_set(x, un, r"$\Lambda_{\mathrm{n}} DMT$")
                plt.add_data_set(x, l_n, r"$\Lambda_{\mathrm{n}} hdev$")
                plt.add_data_set(x, ec - l_n, r"$E_{\mathrm{C}}-\Lambda_{\mathrm{n}}$")
                plt.y_limits = (-0.1, 0.1)

        plt.x_limits = (0.08, 0.1 + 0.01 + 0.02)
        plt.y_limits = (-0.02, 0.12)

        return plt

    @plot
    def plot_jdgn(self):
        plt = Plot(
            "Jdgn(x)",
            num="jdgn" + self.name,
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_label=r"$J_{\mathrm{dgn}}$",
            y_log=False,
            style="color",
        )
        for key in self.dut.data.keys():
            if "inqu" in key and "op1_" in key:
                df = self.dut.data[key]
                x = df["X"]
                # get electron density and hole density, plot Bohm Potential
                cn = df["N"].to_numpy()

                hbar = 1.054e-34  # m^2 kg /s
                q = 1.602e-19  # C
                m = 9.1e-31  # kg effective mass silicon
                n_hat = np.sqrt(cn)
                b = (1 * hbar ** 2) / (6 * q * m)
                # m^4 kg^2 / s /s / C / kg * /m /m = m^2*kg/s^2/C = J/C = V
                un = (
                    (1 * hbar ** 2)
                    / (6 * q * m)
                    * np.gradient(np.gradient(np.sqrt(cn)))
                    / np.sqrt(cn)
                    / 1.602e-19
                )

                ##solutions from hdev
                try:
                    jdgn = df["JDGN"].to_numpy()
                except:
                    jdgn = df["N"].to_numpy() * 0

                # discrete version
                jdg_n = np.gradient(n_hat) / np.gradient(x) * b
                plt.add_data_set(x, jdgn, r"$hdev$")
                plt.add_data_set(x, jdg_n, r"$numerical$")

        return plt

    # @plot
    # def plot_ft(self):
    #     plt = Plot(
    #         'F_T(I_C)',
    #         num='transit frequency '+self.name,
    #         x_specifier=specifiers.CURRENT+'C',
    #         x_label=r'$J_{\mathrm{C}} \left( \si{\milli\ampere\per\square\micro\meter} \right)$',
    #         y_specifier=specifiers.TRANSIT_FREQUENCY,
    #         y_scale=1e-9,
    #         x_scale=1e3/(1e6*1e6),
    #         style='color',
    #         x_log=True
    #     )
    #     for key in self.dut.data.keys():
    #         if 'iv' in key: # conductance from numerical derivative of DC
    #             df    = self.dut.data[key]
    #             freq  = np.unique(df[specifiers.FREQUENCY].to_numpy())

    #             df_ac = df[df[specifiers.FREQUENCY]==freq[1]]
    #             v     = np.abs(df_ac['V_B'].to_numpy() - df_ac['V_E'].to_numpy())
    #             i     = df_ac['I_C'].to_numpy()
    #             ft    = df_ac[specifiers.TRANSIT_FREQUENCY].to_numpy()

    #             plt.add_data_set(
    #                 i,
    #                 ft,
    #             )

    #     plt.x_limits = (1e-3,None)
    #     return plt

    @plot
    def plot_dens(self):
        """Plot of the density alongside the evaluation of the boltzmann or fermi-dirac integral to check the hdev implementation"""
        plt = Plot(
            "Dens(x)",
            num="dens" + self.name,
            style="xtraction_color",
            x_specifier=specifiers.X,
            x_scale=1e6,
            y_label=r"$c/\left( \si{\per\cubic\centi\meter} \right)$",
            y_scale=1e-6,
            y_log=True,
        )
        for key in self.dut.data.keys():
            if "op1_inqu" in key:
                df = self.dut.data[key]
                x = df["X"]
                cn = df["N"]

                plt.add_data_set(x, cn, r"$n$ hdev")
        return plt
