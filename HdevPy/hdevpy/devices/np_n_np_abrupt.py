"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from hdevpy import materials


def np_n_np_abrupt(*args, elec2=False, length=0.01e-6):
    """Creates the hdev input definition for an abrupt n+nn+ structure.

    Input
    -----
    elec2 : Bool, False
        If true, the second conduction band is taken into account by another continuitiy equation (III-V material).
    length : float64, 0.01e-6
        Length of the structure in m.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    xlow = 0
    xhigh = 2 * 0.1e-6 + length  # two contact regions

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": xlow,
            "upp_xyz": xhigh,
            "d_con": 1e21,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 0.1e-6,
            "d_con": 1e25,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.1e-6 + length,
            "upp_xyz": 0.1e-6 + length + 0.1e-6,
            "d_con": 1e25,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": xhigh,
            "mod_name": "Si",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xlow,
            "upp_xyz": xlow,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xhigh,
            "upp_xyz": xhigh,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [xlow, xhigh],
            "n_pnts": 1001,
        }
    )

    silicon = materials["Si"]
    inp.update(silicon)

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["DD"] = {}
    inp["DD"]["remove_zeros"] = -1
    inp["DD"]["n_iter"] = 500
    inp["DD"]["simul"] = 100
    inp["DD"]["elec"] = 1
    inp["DD"]["bohm_n"] = 1
    inp["DD"]["hole"] = 0
    if elec2:
        inp["DD"]["elec2"] = 1
        inp["SEMI"]["elec2"] = 1
        inp["SEMI"]["hole"] = 0

    inp["SEMI"]["fermi"] = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "npnnp"
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.1

    return inp
