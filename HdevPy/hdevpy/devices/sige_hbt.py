"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""

from hdevpy.tools import set_hdev_material_def, turn_off_recombination, set_force


def sige_hbt(version, fermi=False, normalization=False, bohm=False):
    """Creates the inp dict for a 1D SiGe HBT.

    Input
    -----
    version : str
        hdev version specifier.
    fermi : Bool, False
        If True: Use Fermi-Dirac statistics to calculate the carrier densities, else Boltzmann.
    normalization : Bool, False
        If True: use a normalized equation system.
    bohm : Bool, False
        If True: calculate everything taking the quantum Bohm potential into account.

    Returns
    -------
    inp : dict
        A dictionary that defines the input file for a SiGe diode.
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    # &CONST_DOP d_con=-2.27e20 xyz_low=-0.1 -0.1 xyz_upp=0.0105 2.0 / dop_type
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 10.5e-9,
            "d_con": +2.27e26,
        }
    )

    # &EXP_PROD d_max=-2.27e20 xyz_max=0.0105 0  a_xyz=0.0075 0
    # beta_xyz=2.0 0  xyz_low=0.010501 -0.1 xyz_upp=10.0 2.0 / dop_type
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 10.5e-9,
            "upp_xyz": 130e-9,
            "xyz_0": 10.5e-9,
            "d_con": +2.27e26,
            "a_xyz": 7.5e-3 * 1e-6,
            "b_xyz": 2,
        }
    )

    # &EXP_PROD d_max=6.0e+019 xyz_max=0.031 0  a_xyz=0.004668 0
    # beta_xyz=2.5 0  xyz_low=-0.1 -0.1 xyz_upp=10.0 2.0 / dop_type
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0,
            "upp_xyz": 130e-9,
            "xyz_0": 31e-9,
            "d_con": -6e25,
            "a_xyz": 4.668e-3 * 1e-6,
            "b_xyz": 2.5,
        }
    )

    # &EXP_PROD d_max=-4.5E19 xyz_max=0.13 a_xyz=0.021 0
    # beta_xyz= 1.0 0 xyz_low=0.03 -0.1 xyz_upp=10 10 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 30e-9,
            "upp_xyz": 130e-9,
            "xyz_0": 0.13e-6,
            "d_con": +4.5e25,
            "a_xyz": 21e-3 * 1e-6,
            "b_xyz": 1,
        }
    )

    # &ERFC_EXP d_max=-2.5976E20 xyz_max=0.13 0  a_xyz=-0.015 0.0
    # beta_xyz=0.0 0  xyz_low=0.05 -0.1 xyz_upp=10.0 2.0 / dop_type
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 50e-9,
            "upp_xyz": 130e-9,
            "xyz_0": 0.13e-6,
            "d_con": +2.5e26,
            "a_xyz": -15e-3 * 1e-6,
            "b_xyz": 0,
        }
    )

    # &COMP_TRAP mat_type='GE' c_max=0.10 xyz_low=0.0217 -0.1 xyz_upp=0.044 10.0 xyz_dlow=0.0 0.0 xyz_dupp=0.001 0.0
    # xyz_rlow=0.0 0.01 xyz_rupp=0.0 0.0 /
    # xyz_low = np.array([0.0217e-6])
    # xyz_upp = np.array([0.044e-6])
    # xyz_dlow = np.array([1e-9])
    # xyz_dupp = np.array([1e-9])
    # xyz_rlow = np.array([0])
    # xyz_rupp = np.array([0])
    # c_max    = 0.1
    inp["GRADING"].append(
        {
            "profile": "trap",
            "c_max": 0.1,
            "low_xyz": 21.7e-9,
            "upp_xyz": 44e-9,
            "xyz_dlow": 1e-9,
            "xyz_dupp": 10e-9,
            "xyz_rlow": 0,
            "xyz_rupp": 0,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 130e-9,
            "mod_name": "SiGe",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 130e-9,
            "upp_xyz": 130e-9,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 30e-9,
            "upp_xyz": 32e-9,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = {}
    inp["RANGE_GRID"]["disc_dir"] = "x"
    inp["RANGE_GRID"]["disc_set"] = "pnts"
    inp["RANGE_GRID"]["intv_pnts"] = [0, 130e-9]
    inp["RANGE_GRID"]["n_pnts"] = 1501

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["tn"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, "SiGe")
    inp = turn_off_recombination(inp)
    inp = set_force(inp, "mix")

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-9

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if bohm:
        inp["CONT_BOHM_N"] = {}
        inp["CONT_BOHM_N"]["atol"] = 1
        inp["CONT_BOHM_N"]["rtol"] = 0.1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    return inp
