"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


def resistor(version, length, doping, material, grading=None, ydir=False):
    """Generate a 1D resistor of length length doped with ND=doping, made of homogenous material.

    Parameters
    ----------
    version : str
        hdev version specification.
    length : float64
        Length in [m] of the resistor.
    doping : float64
        Doping in [1/m^3] of the resistor.
    material : dict
        hdev material definition blocks as a dict.
    grading : float64, None
        If not None, the structure is graded with this number.
    ydir : Bool, False
        If true, the resistor is extended to a 2D structure.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file using makeinp in DMT.Hdev.Duthdev
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    x_low = 0
    x_high = length

    try:
        mod_name = material["SEMI"]["mod_name"]
    except TypeError:
        mod_name = material["SEMI"][0]["mod_name"]

    inp.update(material)

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_low,
            "upp_xyz": x_high,
            "d_con": doping,
            #'d_con'   : +1e3*1e6,
        }
    )
    if grading is not None:
        if grading == "linear":
            inp["GRADING"].append(
                {
                    "profile": "const",
                    "low_xyz": x_low,
                    "upp_xyz": x_high / 3,
                    "c_max": 0,
                    #'d_con'   : +1e3*1e6,
                }
            )
            inp["GRADING"].append(
                {
                    "profile": "const",
                    "low_xyz": x_high / 3,
                    "upp_xyz": 2 * x_high / 3,
                    "c_max": 0.5,
                    #'d_con'   : +1e3*1e6,
                }
            )
            inp["GRADING"].append(
                {
                    "profile": "const",
                    "low_xyz": 2 * x_high / 3,
                    "upp_xyz": x_high,
                    "c_max": 1,
                    #'d_con'   : +1e3*1e6,
                }
            )
        elif grading == "step":
            inp["GRADING"].append(
                {
                    "profile": "const",
                    "low_xyz": x_high / 2,
                    "upp_xyz": x_high,
                    "c_max": 1,
                    #'d_con'   : +1e3*1e6,
                }
            )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": x_high,
            "mod_name": mod_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_low,
            "upp_xyz": x_low,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_high,
            "upp_xyz": x_high,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []

    if ydir:
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "y",
                "disc_set": "pnts",
                "intv_pnts": [x_low, x_high],
                "n_pnts": 1001,
            }
        )

        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [0, 1e-6],
                "n_pnts": 6,
            }
        )

    else:
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [x_low, x_high],
                "n_pnts": 1001,
            }
        )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    if ydir:  # 1 um in y dir
        inp["REGION_INFO"]["spat_dim"] = 2
        for i, _grading in enumerate(inp["GRADING"]):
            inp["GRADING"][i]["low_xyz"] = [0, inp["GRADING"][i]["low_xyz"]]
            inp["GRADING"][i]["upp_xyz"] = [1e-6, inp["GRADING"][i]["upp_xyz"]]
        for i, _doping in enumerate(inp["DOPING"]):
            inp["DOPING"][i]["low_xyz"] = [0, inp["DOPING"][i]["low_xyz"]]
            inp["DOPING"][i]["upp_xyz"] = [1e-6, inp["DOPING"][i]["upp_xyz"]]
        for i, _region_def in enumerate(inp["REGION_DEF"]):
            inp["REGION_DEF"][i]["low_xyz"] = [0, inp["REGION_DEF"][i]["low_xyz"]]
            inp["REGION_DEF"][i]["upp_xyz"] = [1e-6, inp["REGION_DEF"][i]["upp_xyz"]]

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "resistor_" + mod_name + "_"
    inp["OUTPUT"]["path"] = version

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.02

    return inp
