"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def tunnel_resistor(version, fermi=False):
    """Generate a 1D resistor structure for testing the tunneling implementation.

    Parameters
    ----------
    version : str
        hdev version specification.
    fermi : Bool, False
        IF True, use Fermi-Dirac statistics.
    material : dict
        A dict that defines the material properties of the semiconductor to be used.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file using makeinp.
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 1e-6,
            "d_con": +1e24,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 0.48e-6,
            "d_con": +1e26,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.52e-6,
            "upp_xyz": 1e-6,
            "d_con": +1e26,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 1e-6,
            "mod_name": "Si",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 1e-6,
            "upp_xyz": 1e-6,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0, 0.4e-6],
            "n_pnts": 21,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0.4e-6, 0.6e-6],
            "n_pnts": 201,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0.6e-6, 1e-6],
            "n_pnts": 21,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 0
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0

    inp["TUNNEL"] = {}
    inp["TUNNEL"]["activate"] = 1
    inp["TUNNEL"]["factor"] = 100
    inp["TUNNEL"]["m"] = 1  # m_tunnel/m0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "resistor_tunnel"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, "Si")
    inp = turn_off_recombination(inp)

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    return inp
