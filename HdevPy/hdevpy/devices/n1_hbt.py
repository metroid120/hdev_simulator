"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def n1_hbt(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    reco=False,
    ge_max=None,
    spring=False,
    tn=False,
):
    """Creates the hdev input definition for the node 2 SiGe HBT of the ITRS roadmap.

    Input
    -----
    version : str
        Name of the hdev version.
    fermi : Bool, False
        If true, use Fermi statistics, else Boltzmann.
    normalization : Bool, False
        If true, normalize the equation system.
    reco : Bool, False
        If true, recombination models are activated.
    bohm : Bool, False
        If true, simulate with the Bohm potential.
    sat : Bool, True
        Activate the saturation velocity model.
    ge_max : float64, None
        If given, the peak Ge grading is set to this value.
    spring : Boolean, False
        If true, the simulation grid is chosen minimal so the result is suitable for SPRING BTE simulations.
    tn : Boolean, False
        If true, do a hydrodynamic simulation, else drift-diffusion.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """

    mat_name = "SiGe"
    if spring:
        mat_name = "SSGS"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    #  &ERFC_EXP d_max=-6e+19 xyz_max=0.012063393105 0 a_xyz=0.010818909397 0
    #            xyz_low=0 0 xyz_upp=0.013833429522 1
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0,
            "upp_xyz": 0.013833429522e-6,
            "xyz_0": 0.012063393105e-6,
            "d_con": 6e19 * 1e6,
            "a_xyz": 0.010818909397e-6,
            "b_xyz": 0,
        }
    )
    #  &REXP_DOP d_max=-5.5e+19 xyz_0=0.012911200887 0 a_xyz=0.0042179767581 0
    #            beta_xyz=1.4237118076 xyz_low=0.013833521744 0 xyz_upp=0.15492793052 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.15492793052e-6,
            "low_xyz": 0.013833521744e-6,
            "xyz_0": 0.012911200887e-6,
            "d_con": +5.5e19 * 1e6,
            "a_xyz": 0.0042179767581e-6,
            "b_xyz": 1.4237118076,
        }
    )
    #  &REXP_DOP d_max=5.2e+19 xyz_0=0.034648960621 0 a_xyz=0.0038999529886 0
    #            beta_xyz=3.0657584554 xyz_low=0.00036024636672 0 xyz_upp=0.15492793052 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.15492793052e-6,
            "low_xyz": 0.00036024636672e-6,
            "xyz_0": 0.034648960621e-6,
            "d_con": -5.2e19 * 1e6,
            "a_xyz": 0.0038999529886e-6,
            "b_xyz": 3.0657584554,
        }
    )
    #  &ERFC_EXP d_max=-5.4382506562e+18 xyz_max=0.073881878642 0 a_xyz=-0.013833429522 0
    #            xyz_low=0 0 xyz_upp=0.067024135792 1 /
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0,
            "upp_xyz": 0.067024135792e-6,
            "xyz_0": 0.073881878642e-6,
            "d_con": 5.4382506562e18 * 1e6,
            "a_xyz": -0.013833429522e-6,
            "b_xyz": 0,
        }
    )
    #  &REXP_DOP d_max=-1.1180143017e+20 xyz_0=0.12936725262 0 a_xyz=0.0057592756544 0
    #            beta_xyz=0.555 xyz_low=0.067024198135 0 xyz_upp=0.12313294094 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.12313294094e-6,
            "low_xyz": 0.067024198135e-6,
            "xyz_0": 0.12936725262e-6,
            "d_con": 1.1180143017e20 * 1e6,
            "a_xyz": 0.0057592756544e-6,
            "b_xyz": 0.555,
        }
    )
    #  &ERFC_EXP d_max=-6.7015736169e+19 xyz_max=0.13186097729 0 a_xyz=-0.022702807265 0
    #            xyz_low=0.12313300329 0 xyz_upp=0.15492793052 1 /
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0.12313300329e-6,
            "upp_xyz": 0.15492793052e-6,
            "xyz_0": 0.13186097729e-6,
            "d_con": 6.7015736169e19 * 1e6,
            "a_xyz": -0.022702807265e-6,
            "b_xyz": 0,
        }
    )
    #  &COMP_EXP mat_type='GE' c_max=0.21543721792 xyz_max=0.034414565713 0 a_xyz=0.0084742527075 0
    #            beta_xyz=5.1627628756 1 xyz_low=0 0 xyz_upp=0.15492793052 1 /
    inp["GRADING"].append(
        {
            "profile": "exp",
            "c_max": 0.21543721792,
            "xyz_0": 0.034414565713e-6,
            "low_xyz": 0,
            "upp_xyz": 0.15492793052e-6,
            "a_xyz": 0.0084742527075e-6,
            "b_xyz": 5.1627628756,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 0.15492793052e-6,
            "mod_name": mat_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0.15492793052e-6,
            "upp_xyz": 0.15492793052e-6,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 0.034175963394e-6,
            "upp_xyz": 0.035121957848e-6,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []
    # inp["RANGE_GRID"].append(
    #     {
    #         "disc_dir": "x",
    #         "disc_set": "pnts",
    #         "intv_pnts": [0, 0.034175963394e-9],
    #         "n_pnts": 55,
    #     }
    # )
    # inp["RANGE_GRID"].append(
    #     {
    #         "disc_dir": "x",
    #         "disc_set": "pnts",
    #         "intv_pnts": [0.034175963394e-9, 0.035121957848e-9],
    #         "n_pnts": 10,
    #     }
    # )
    # inp["RANGE_GRID"].append(
    #     {
    #         "disc_dir": "x",
    #         "disc_set": "pnts",
    #         "intv_pnts": [0.035121957848e-9, 0.15492793052e-6],
    #         "n_pnts": 101,
    #     }
    # )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0, 0.15492793052e-6],
            "n_pnts": 251,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1  # war 30... wieso
    if tn:
        inp["DD"]["tn"] = 1
        # inp['DD']['simul']    = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)

    if not reco:
        inp = turn_off_recombination(inp)
    else:
        to_del = []
        for i in range(len(inp["RECOMB_DEF"])):
            if inp["RECOMB_DEF"][i]["type"]=="avalanche":
                to_del.append(i)
        inp["RECOMB_DEF"] = [el for i,el in enumerate(inp["RECOMB_DEF"]) if i not in to_del]

    for i in range(len(inp["MOB_DEF"])):
        if not sat:
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        else:
            inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-11
    inp["CONT_ELEC"]["delta_max"] = 1e-3

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    return inp
