"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from hdevpy.tools import set_hdev_material_def, turn_off_recombination
import numpy as np


def n3_hbt_2D(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    ge_max=None,
    spring=False,
    tn=False,
    redused=True,
    exten_step=1,
    turn=False,
    bE=0.09e-6 / 2,
    rbx_st=False,
):
    """Creates the hdev input definition for the node 3 SiGe HBT of the ITRS roadmap with 2D extension to resemble a
    more realistic structure. Work in progress.

    TODO
    """

    mat_name = "SiGe"
    mat_name_si = "Si"
    mat_name_ge = "Ge"
    if spring:
        mat_name = "SSGS"
        mat_name_si = "SS"
        mat_name_ge = "GS"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    #  &CONST_DOP d_con=-1.8e+20 xyz_low=0 0 xyz_upp=0.007 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {"profile": "const", "low_xyz": 0, "upp_xyz": 0.007e-6, "d_con": +1.8e20 * 1e6}
    )
    #  &REXP_DOP d_max=-1.8e+20 xyz_0=0.007 0 a_xyz=0.003 0
    #            beta_xyz=2.8 xyz_low=0.007001 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.007001e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.007e-6,
            "d_con": +1.8e20 * 1e6,
            "a_xyz": 0.003e-6,
            "b_xyz": 2.8,
        }
    )
    #  &CONST_DOP d_con=-2.27e+18 xyz_low=0 0 xyz_upp=0.016667 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 0.016667e-6,
            "d_con": +2.27e18 * 1e6,
        }
    )
    #  &REXP_DOP d_max=-2.27e+18 xyz_0=0.016668 0 a_xyz=0.0011910138915 0
    #            beta_xyz=2.8 xyz_low=0.016668 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.016668e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.016668e-6,
            "d_con": +2.27e18 * 1e6,
            "a_xyz": 0.0011910138915e-6,
            "b_xyz": 2.8,
        }
    )
    #  &REXP_DOP d_max=1e+20 xyz_0=0.023259581033 0 a_xyz=0.0025474512739 0
    #            beta_xyz=3.1344195605 xyz_low=0 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.016668e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.023259581033e-6,
            "d_con": -1e20 * 1e6,
            "a_xyz": 0.0025474512739e-6,
            "b_xyz": 3.1344195605,
        }
    )
    #  &REXP_DOP d_max=-7.9564915229e+20 xyz_0=0.08 0 a_xyz=0.0039575264888 0
    #            beta_xyz=0.77809654453 xyz_low=0 0 xyz_upp=0.061799 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0,
            "upp_xyz": 0.061799e-6,
            "xyz_0": 0.08e-6,
            "d_con": +7.9564915229e20 * 1e6,
            "a_xyz": 0.0039575264888e-6,
            "b_xyz": 0.77809654453,
        }
    )
    #  &ERFC_EXP d_max=-1.0030119201e+20 xyz_max=0.07 0 a_xyz=-0.011168063946 0
    #            xyz_low=0.0618 0 xyz_upp=0.08 1 /
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0.0618e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.07e-6,
            "d_con": +1.0030119201e20 * 1e6,
            "a_xyz": -0.011168063946e-6,
            "b_xyz": 0,
        }
    )

    #  &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.012248 -1e-10 xyz_upp=0.029928 1
    #             xyz_dlow=0.01278 0.0 xyz_dupp=0.0018 0.0 xyz_rlow=0.0004 0.0 xyz_rupp=0.001 0.0 /
    if ge_max is None:  # original version
        inp["GRADING"].append(
            {
                "profile": "trap",
                "c_max": 0.3,
                "low_xyz": 0.012248e-6,
                "upp_xyz": 0.029928e-6,
                "xyz_dlow": 0.01278e-6,
                "xyz_dupp": 0.0018e-6,
                "xyz_rlow": 0.0004e-6,
                "xyz_rupp": 0.001e-6,
            }
        )
    else:
        inp["GRADING"].append(
            {
                "profile": "trap",
                "c_max": ge_max,
                "low_xyz": 0.012248e-6,
                "upp_xyz": 0.029928e-6,
                "xyz_dlow": 0.01278e-6,
                "xyz_dupp": 0.0018e-6,
                "xyz_rlow": 0.0004e-6,
                "xyz_rupp": 0.001e-6,
            }
        )
    # inp['GRADING'].append({
    #     'profile' : 'trap',
    #     'c_max'   : 0.3,
    #     'low_xyz' : 0.012e-6,
    #     #'upp_xyz' : 0.029928e-6,
    #     'upp_xyz' : 0.033e-6,
    #     'xyz_dlow': 0.012e-6,
    #     'xyz_dupp': 0.002e-6,
    #     'xyz_rlow': 0.0004e-6,
    #     'xyz_rupp': 0.001e-6,
    # })

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 80e-9,
            "mod_name": mat_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 80e-9,
            "upp_xyz": 80e-9,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 22.75e-9,
            "upp_xyz": 23.75e-9,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 2

    inp["AREA_GRID"] = []
    inp["RANGE_GRID"] = []

    if not redused:

        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [0, 22.75e-9],
                "n_pnts": 55,
            }
        )
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [22.75e-9, 23.75e-9],
                "n_pnts": 3,
            }
        )
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [23.75e-9, 80e-9],
                "n_pnts": 195,
            }
        )
    else:

        inp["AREA_GRID"].append(
            {
                "disc_dir": "x",
                # "disc_set": "pnts",
                "intv_pnts": [0, 22.75e-9],
                "intv_diff": [0.5e-9, 3e-9, 0.5e-9],
            }
        )
        inp["AREA_GRID"].append(
            {
                "disc_dir": "x",
                # "disc_set": "pnts",
                "intv_pnts": [23.75e-9, 80e-9],
                "intv_diff": [0.5e-9, 5e-9, 0.5e-9],
            }
        )
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "x",
                "disc_set": "pnts",
                "intv_pnts": [22.75e-9, 23.75e-9],
                "n_pnts": 3,  # 3
            }
        )
        # inp["RANGE_GRID"].append(
        #     {
        #         "disc_dir": "x",
        #         "disc_set": "pnts",
        #         "intv_pnts": [0, 22.75e-9],
        #         "n_pnts": 38,#35
        #     }
        # )

        # inp["RANGE_GRID"].append(
        #     {
        #         "disc_dir": "x",
        #         "disc_set": "pnts",
        #         "intv_pnts": [23.75e-9, 80e-9],
        #         "n_pnts": 50,#50
        #     }
        # )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1  # war 30... wieso
    if tn:
        inp["DD"]["tn"] = 1
        # inp['DD']['simul']    = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 1

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)

    inp = turn_off_recombination(inp)

    for i in range(len(inp["MOB_DEF"])):
        if not sat:
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        else:
            inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-7

    inp["CONT_TN"] = {}
    inp["CONT_TN"]["rtol"] = 1e-9  # 1e-11
    # inp["CONT_TN"]["delta_max"] = 1e-3

    # inp["CONT_ELEC"] = {}
    # inp["CONT_ELEC"]["atol"] = 1e-5#1e-11
    # inp["CONT_ELEC"]["delta_max"] = 1e-3

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    inp["OUTPUT"]["inqu_lev"] = 2

    # extension to 2D
    # adds second dimension in y direction for existing in 1D regions
    #
    # set more ext for oxidintersection
    ints_ext = 0  # additional extension for base-emitter region under oxide
    if exten_step == 2:  # exten_step/2*b_E0
        ints_ext = 5e-9
    bE0 = bE
    y_ext = bE0 + 0.09e-6 / 2 * exten_step * (exten_step - 1)
    y_int = 0

    if rbx_st:
        pass  # y_int = bE0

    for inp_sec in ["DOPING", "REGION_DEF", "GRADING"]:
        for i, _sec in enumerate(inp[inp_sec]):

            inp[inp_sec][i]["low_xyz"] = [inp[inp_sec][i]["low_xyz"], y_int]
            if inp_sec == "DOPING":
                inp[inp_sec][i]["upp_xyz"] = [
                    inp[inp_sec][i]["upp_xyz"],
                    bE0 + ints_ext,
                ]
            else:
                if inp_sec == "GRADING":
                    inp[inp_sec][i]["upp_xyz"] = [
                        inp[inp_sec][i]["upp_xyz"],
                        bE0 + ints_ext * 2,
                    ]
                else:
                    inp[inp_sec][i]["upp_xyz"] = [inp[inp_sec][i]["upp_xyz"], y_ext]

    if exten_step == 2:
        # extend inner transistor
        ext_dop_list = []
        for i, _sec in enumerate(inp["DOPING"]):
            new_x_lim = 20e-9
            ext_dop = dict(inp["DOPING"][i])
            if not inp["DOPING"][i]["d_con"] < 0:  # basis
                if not inp["DOPING"][i]["low_xyz"][0] < new_x_lim:
                    ext_dop["low_xyz"] = [new_x_lim, ext_dop["upp_xyz"][1] + 1e-12]
                    ext_dop["upp_xyz"] = [ext_dop["upp_xyz"][0], y_ext]
                    ext_dop_list.append(ext_dop)
            else:
                # xi = inp["DOPING"][i]["low_xyz"][0] #+(inp["DOPING"][i]["upp_xyz"][0] - inp["DOPING"][i]["low_xyz"][0])/2
                basis_dop_ext = dict(inp["DOPING"][i])
                # xi = ind[0]
                # x0 = basis_dop_ext['xyz_0']
                # #yi = ind[1]
                # alfa = basis_dop_ext['a_xyz']
                # beta = basis_dop_ext['b_xyz']
                # N_net = basis_dop_ext['d_con']*np.exp(-abs((xi-x0)/alfa)**beta)
                # basis_dop_ext['d_con'] = N_net
                # inp["DOPING"][i]["upp_xyz"][1] =bE0
                basis_dop_ext["low_xyz"] = [0, bE0 + 5e-9 + 1e-12]
                basis_dop_ext["upp_xyz"] = [30e-9, y_ext]
                basis_dop_ext["profile"] = "const"
                ext_dop_list.append(basis_dop_ext)

        for ext_dop in ext_dop_list:
            inp["DOPING"].append(ext_dop)
    for i, _sec in enumerate(inp["DOPING"]):
        if (
            inp["DOPING"][i]["profile"] == "exp"
            or inp["DOPING"][i]["profile"] == "erfc"
        ):
            inp["DOPING"][i]["xyz_0"] = [inp["DOPING"][i]["xyz_0"], y_int]
            for const_set in ["a_xyz", "b_xyz"]:
                inp["DOPING"][i][const_set] = [inp["DOPING"][i][const_set], 1]
    for i, _sec in enumerate(inp["GRADING"]):

        for const_set in ["xyz_dlow", "xyz_dupp", "xyz_rlow", "xyz_rupp"]:
            inp["GRADING"][i][const_set] = [inp["GRADING"][i][const_set], y_int]

    else:  # if exten_step == 1:

        for i, _sec in enumerate(inp["REGION_DEF"]):
            if inp["REGION_DEF"][i]["reg_mat"] == "SUPPLY":
                inp["REGION_DEF"].remove(inp["REGION_DEF"][i])
                inp["REGION_DEF"].append(
                    {
                        "reg_mat": "CONT",
                        "low_xyz": [2.275e-08, y_ext],
                        "upp_xyz": [2.375e-08, y_ext],
                        "mod_name": "CONT",
                        "cont_name": "B",
                    }
                )

    # discretization in y direction

    inp["AREA_GRID"].append(
        {
            "disc_dir": "y",
            # "disc_set": "pnts",
            "intv_pnts": [0, bE0],
            "intv_diff": [2e-9, 10e-9, 2e-9],
        }
    )
    if exten_step == 2:
        inp["AREA_GRID"].append(
            {
                "disc_dir": "y",
                # "disc_set": "pnts",
                "intv_pnts": [bE0 + 10e-9, y_ext],
                "intv_diff": [5e-9, 20e-9, 5e-9],
            }
        )
        # inp["RANGE_GRID"].append(
        #     {   "disc_dir": "y",
        #         "disc_set": "pnts",
        #         "intv_pnts": [0, bE0+5e-9],
        #         "n_pnts": disc[0],
        #     })
        inp["RANGE_GRID"].append(
            {
                "disc_dir": "y",
                "disc_set": "pnts",
                "intv_pnts": [bE0, bE0 + 10e-9],
                "n_pnts": 4,
            }
        )
        # inp["RANGE_GRID"].append(
        #     {   "disc_dir": "y",
        #         "disc_set": "pnts",
        #         "intv_pnts": [bE0+15e-9, y_ext],#-bE0-1e-9],
        #         "n_pnts": disc[2],
        #     })
        # inp["RANGE_GRID"].append(
        #     {   "disc_dir": "y",
        #         "disc_set": "pnts",
        #         "intv_pnts": [ y_ext-0.09e-6-1e-9, y_ext],
        #         "n_pnts": disc[3],
        #     })

        # basis_dop_ext_list = []
        #     if inp["DOPING"][i]['d_con']<0:
        #         xi = inp["DOPING"][i]["low_xyz"][0] #+(inp["DOPING"][i]["upp_xyz"][0] - inp["DOPING"][i]["low_xyz"][0])/2
        #         basis_dop_ext = dict(inp["DOPING"][i])
        #         #xi = ind[0]
        #         x0 = basis_dop_ext['xyz_0']
        #         #yi = ind[1]
        #         alfa = basis_dop_ext['a_xyz']
        #         beta = basis_dop_ext['b_xyz']
        #         N_net = basis_dop_ext['d_con']*np.exp(-abs((xi-x0)/alfa)**beta)
        #         #basis_dop_ext['d_con'] = N_net
        #         #inp["DOPING"][i]["upp_xyz"][1] =bE0
        #         basis_dop_ext["low_xyz"] = [0,bE0+1e-9]
        #         basis_dop_ext["upp_xyz"] = [35e-9,y_ext]
        #         basis_dop_ext["profile"] = 'const'
        #         basis_dop_ext_list.append(basis_dop_ext)
        #         dummy = 1
        # for ext_dop in ext_dop_list:
        #     inp["DOPING"].append(ext_dop)
        #  if inp["DOPING"][i]["low_xyz"][0]<0.017e-6:
        #      inp["DOPING"][i]["upp_xyz"][1] = bE0

        # set contacts for 2D 3/2*b_E0 extention
        for i, _sec in enumerate(inp["REGION_DEF"]):
            if inp["REGION_DEF"][i]["reg_mat"] == "SUPPLY":
                inp["REGION_DEF"].remove(inp["REGION_DEF"][i])
                inp["REGION_DEF"].append(
                    {
                        "reg_mat": "CONT",
                        "low_xyz": [0, y_ext - 0.09e-6 / 2],
                        "upp_xyz": [0, y_ext],
                        # "low_xyz": [0,y_ext],
                        # "upp_xyz": [0.027e-6,y_ext],
                        "mod_name": "CONT",
                        "cont_name": "B",
                    }
                )
        for i, _sec in enumerate(inp["REGION_DEF"]):
            if inp["REGION_DEF"][i]["reg_mat"] == "CONT":
                if inp["REGION_DEF"][i]["cont_name"] == "E":
                    # inp["REGION_DEF"][i]["low_xyz"][1] = 0
                    inp["REGION_DEF"][i]["upp_xyz"][1] = bE0
                if inp["REGION_DEF"][i]["cont_name"] == "C":
                    inp["REGION_DEF"][i]["upp_xyz"][1] = y_ext

        # define oxides regions

        inp["REGION_DEF"].append(
            {
                "reg_mat": "OXID",
                "low_xyz": [0, bE0 + 1e-12],
                "upp_xyz": [17e-9, bE0 + 2 * 5e-9],
                "mod_name": "SiO2",
            }
        )
        # if rbx_st:
        #     inp["REGION_DEF"].append(
        #     {
        #         "reg_mat": "OXID",
        #         "low_xyz": [0, bE0+1e-12],
        #         "upp_xyz": [80e-9, bE0+1*5e-9],
        #         "mod_name": "SiO2",
        #     }
        # )

        inp["REGION_DEF"].append(
            {
                "reg_mat": "OXID",
                "low_xyz": [0.027e-6, bE0 + 0.09e-6 / 2],
                "upp_xyz": [65e-9, y_ext],
                "mod_name": "SiO2",
            }
        )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "OXID",
                "low_xyz": [65e-9, bE0 + 0.09e-6 / 2 + 30e-9],
                "upp_xyz": [80e-9, y_ext],
                "mod_name": "SiO2",
            }
        )

    # turn

    if turn:
        for reg_type in ["REGION_DEF", "DOPING", "GRADING"]:
            for i, reg in enumerate(inp[reg_type]):
                low_xyz = reg["low_xyz"]
                upp_xyz = reg["upp_xyz"]
                inp[reg_type][i]["low_xyz"] = [low_xyz[1], low_xyz[0]]
                inp[reg_type][i]["upp_xyz"] = [upp_xyz[1], upp_xyz[0]]
                if "profile" in inp[reg_type][i].keys():
                    if (
                        inp[reg_type][i]["profile"] == "exp"
                        or inp[reg_type][i]["profile"] == "erfc"
                    ):
                        for const_set in ["xyz_0", "a_xyz", "b_xyz"]:
                            values = inp["DOPING"][i][const_set]
                            inp["DOPING"][i][const_set] = [values[1], values[0]]
                    # if inp[reg_type][i]['profile'] == 'trap':
                    #     for const_set in ['xyz_dlow', 'xyz_dupp', 'xyz_rlow', 'xyz_rupp']:
                    #         values = inp["GRADING"][i][const_set]
                    #         inp["GRADING"][i][const_set] =  [values[1], values[0]]
                    # convergence problems arise

        for i, reg in enumerate(inp["RANGE_GRID"]):
            if reg["disc_dir"] == "x":
                inp["RANGE_GRID"][i]["disc_dir"] = "y"
            else:
                inp["RANGE_GRID"][i]["disc_dir"] = "x"
        for i, reg in enumerate(inp["AREA_GRID"]):
            if reg["disc_dir"] == "x":
                inp["AREA_GRID"][i]["disc_dir"] = "y"
            else:
                inp["AREA_GRID"][i]["disc_dir"] = "x"

    return inp
