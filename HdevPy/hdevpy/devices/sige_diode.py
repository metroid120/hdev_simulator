"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def sige_diode(version, fermi=False, sat=True, tn=True):
    """Creates the inp dict for a SiGe diode.

    Input
    -----
    version : str
        hdev version specifier.
    fermi : Bool, False
        If True: Use Fermi-Dirac statistics to calculate the carrier densities, else Boltzmann.

    Returns
    -------
    inp : dict
        A dictionary that defines the input file for a SiGe diode.
    """

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 130e-9 / 2,
            "d_con": +2.27e26,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "upp_xyz": 130e-9,
            "low_xyz": 130e-9 / 2 + 0.001e-9,
            "d_con": -2.27e26,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 130e-9,
            "mod_name": "SS",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 130e-9,
            "upp_xyz": 130e-9,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp = set_hdev_material_def(inp, "SS")
    inp = turn_off_recombination(inp)

    inp["RANGE_GRID"] = {}
    inp["RANGE_GRID"]["disc_dir"] = "x"
    inp["RANGE_GRID"]["disc_set"] = "pnts"
    inp["RANGE_GRID"]["intv_pnts"] = [0, 130e-9]
    inp["RANGE_GRID"]["n_pnts"] = 101

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "schottky",
            "v_n":1e-7,
            "v_p":0,
        }
    )

    inp["SOLVE"] = {}
    inp["SOLVE"]["n_iter"] = 500
    inp["SOLVE"]["elec"] = 1
    inp["SOLVE"]["hole"] = 1
    inp["SOLVE"]["simul"] = 1
    inp["SOLVE"]["damp_method"] = "none"
    inp["SOLVE"]["damp"] = 0.3
    inp["SOLVE"]["debug"] = 0
    if tn:
        inp["SOLVE"]["tn"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 1

    if fermi:
        inp["SEMI"]["fermi"] = 1

    if not sat:
        for i in range(len(inp["MOB_DEF"])):
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"


    # inp["NORMALIZATION"] = {}
    # inp["NORMALIZATION"]["x_norm"] = 1

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01

    return inp
