"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


def bjt(we, wb, wc, de, db, dc, material, ver):
    """Generate a 1D bjt of homogenous material with emitter length and doping we/de, base length and doping wb/db and collector length and doping wc/dc.
    Parameters
    ----------
    we : float64
        Emitter length
    wb : float64
        Base length
    wc : float64
        Collector length
    de : float64
        Emitter doping
    db : float64
        Base doping
    dc : float64
        Collector doping
    material : dict
        A hdev material definition.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file using makeinp in DMT.Hdev.Duthdev
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    x_low = 0
    x_high = we + wb + wc

    try:
        mod_name = material["SEMI"]["mod_name"]
    except TypeError:
        mod_name = material["SEMI"][0]["mod_name"]

    inp.update(material)

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_low,
            "upp_xyz": we,
            "d_con": de,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": we,
            "upp_xyz": we + wb,
            "d_con": db,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": we + wb,
            "upp_xyz": we + wb + wc,
            "d_con": dc,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": x_high,
            "mod_name": mod_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_low,
            "upp_xyz": x_low,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_high,
            "upp_xyz": x_high,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": we + wb / 4,
            "upp_xyz": we + wb / 4,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [x_low, we + wb / 4],
            "n_pnts": 101,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [we + wb / 4, we + wb],
            "n_pnts": 51,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [we + wb, x_high],
            "n_pnts": 101,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )
    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["simul"] = 1
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "bjt_" + mod_name + "_"

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    inp["BIAS_DEF"]["displ"] = 0

    return inp
