"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def n5_hbt(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    ge_max=None,
    spring=False,
    tn=False,
):
    """Creates the hdev input definition for the node 5 SiGe HBT of the ITRS roadmap.

    Input
    -----
    version : str
        Name of the hdev version.
    fermi : Bool, False
        If true, use Fermi statistics, else Boltzmann.
    normalization : Bool, False
        If true, normalize the equation system.
    bohm : Bool, False
        If true, use the bohm potential.
    sat : Bool, True
        Activate the saturation velocity model.
    ge_max : float64, None
        If given, the peak Ge grading is set to this value.
    spring : Boolean, False
        If true, the simulation grid is chosen minimal so the result is suitable for SPRING BTE simulations.
    tn : Boolean, False
        If true, do a hydrodynamic simulation, else drift-diffusion.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """

    mat_name = "SiGe"
    if spring:
        mat_name = "SSGS"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    #  &CONST_DOP d_con=-2.3e+20 xyz_low=0 0 xyz_upp=0.0046299 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 0.0046299e-6,
            "d_con": +2.3e20 * 1e6,
        }
    )
    #  &REXP_DOP d_max=-2.3e+20 xyz_0=0.00463 0 a_xyz=0.003182 0
    #            beta_xyz=2.8 xyz_low=0.00463 0 xyz_upp=0.015 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.015e-6,
            "low_xyz": 0.00463e-6,
            "xyz_0": 0.00463e-6,
            "d_con": +2.3e20 * 1e6,
            "a_xyz": 0.003182e-6,
            "b_xyz": 2.8,
        }
    )
    #  &REXP_DOP d_max=1e+18 xyz_0=0.010456 0 a_xyz=0.001 0
    #            beta_xyz=2 xyz_low=0.009 0 xyz_upp=0.010456 1
    #            r_max=0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.010456e-6,
            "low_xyz": 0.009e-6,
            "xyz_0": 0.010456e-6,
            "d_con": -1e18 * 1e6,
            "a_xyz": 0.001e-6,
            "b_xyz": 2,
        }
    )
    #  &CONST_DOP d_con=1e+18 xyz_low=0.010456 0 xyz_upp=0.014 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.010456e-6,
            "upp_xyz": 0.014e-6,
            "d_con": -1e18 * 1e6,
        }
    )
    #  &REXP_DOP d_max=2.4e+20 xyz_0=0.0158802 0 a_xyz=0.0011386 0
    #            beta_xyz=2.5 xyz_low=0 0 xyz_upp=0.041325 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.041325e-6,
            "low_xyz": 0,
            "xyz_0": 0.0158802e-6,
            "d_con": -2.4e20 * 1e6,
            "a_xyz": 0.0011386e-6,
            "b_xyz": 2.5,
        }
    )
    #  &CONST_DOP d_con=-1e+16 xyz_low=0.015975 0 xyz_upp=0.03 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.015975e-6,
            "upp_xyz": 0.03e-6,
            "d_con": 1e16 * 1e6,
        }
    )
    #  &REXP_DOP d_max=-7.86454e+19 xyz_0=0.03 0 a_xyz=1e-05 0
    #            beta_xyz=0.4 xyz_low=0.018475 0 xyz_upp=0.02999 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.02999e-6,
            "low_xyz": 0.018475e-6,
            "xyz_0": 0.03e-6,
            "d_con": 7.86454e19 * 1e6,
            "a_xyz": 1e-11,
            "b_xyz": 0.4,
        }
    )
    #  &REXP_DOP d_max=-6.8e+20 xyz_0=0.041325 0 a_xyz=0.00525 0
    #            beta_xyz=1 xyz_low=0.03 0 xyz_upp=0.034631 1
    #            r_max=0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.034631e-6,
            "low_xyz": 0.03e-6,
            "xyz_0": 0.041325e-6,
            "d_con": 6.8e20 * 1e6,
            "a_xyz": 0.00525e-6,
            "b_xyz": 1,
        }
    )
    #  &CONST_DOP d_con=-1.9e+20 xyz_low=0.034631 0 xyz_upp=0.041325 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.034631e-6,
            "upp_xyz": 0.041325e-6,
            "d_con": 1.9e20 * 1e6,
        }
    )
    #  &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.0175127 -1e-10 xyz_upp=0.0206045 1
    #             xyz_dlow=0 0.0 xyz_dupp=0.0009367 0.0 xyz_rlow=0 0.0 xyz_rupp=0.00025 0.0 /
    inp["GRADING"].append(
        {
            "profile": "trap",
            "c_max": 0.3,
            "low_xyz": 0.0175127e-6,
            "upp_xyz": 0.0206045e-6,
            "xyz_dlow": 0,
            "xyz_dupp": 0.0009367e-6,
            "xyz_rlow": 0.000e-6,
            "xyz_rupp": 0.00025e-6,
        }
    )
    # &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.007 -1e-10 xyz_upp=0.0175127 1
    #             xyz_dlow=0.0105127 0.0 xyz_dupp=0 0.0 xyz_rlow=0 0.0 xyz_rupp=0 0.0 /
    inp["GRADING"].append(
        {
            "profile": "trap",
            "c_max": 0.3,
            "low_xyz": 0.007e-6,
            "upp_xyz": 0.0175127e-6,
            "xyz_dlow": 0.0105127e-6,
            "xyz_dupp": 0e-6,
            "xyz_rlow": 0.00e-6,
            "xyz_rupp": 0.00e-6,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 0.041325e-6,
            "mod_name": mat_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0.041325e-6,
            "upp_xyz": 0.041325e-6,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 0.0157e-6,
            "upp_xyz": 0.0161e-6,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0, 0.0157e-6],
            "n_pnts": 55,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0.0157e-6, 0.0161e-6],
            "n_pnts": 3,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0.0161e-6, 0.041325e-6],
            "n_pnts": 101,
        }
    )
    # inp["RANGE_GRID"].append(
    #     {
    #         "disc_dir": "x",
    #         "disc_set": "pnts",
    #         "intv_pnts": [0, 0.041325e-6],
    #         "n_pnts": 195,
    #     }
    # )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1  # war 30... wieso
    if tn:
        inp["DD"]["tn"] = 1
        # inp['DD']['simul']    = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)

    inp = turn_off_recombination(inp)

    for i in range(len(inp["MOB_DEF"])):
        if not sat:
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        else:
            inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-11
    inp["CONT_ELEC"]["delta_max"] = 1e-3

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    return inp
