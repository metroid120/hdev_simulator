"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""

from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def n3_hbt(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    reco=False,
    ge_max=None,
    spring=False,
    tn=False,
    cont_type = 'ohmic',
):
    """Creates the hdev input definition for the node 3 SiGe HBT of the ITRS roadmap.

    Input
    -----
    version : str
        Name of the hdev version.
    fermi : Bool, False
        If true, use Fermi statistics, else Boltzmann.
    normalization : Bool, False
        If true, normalize the equation system.
    reco : Bool, False
        If true, recombination models are activated.
    bohm : Bool, False
        If true, use the bohm potential.
    sat : Bool, True
        Activate the saturation velocity model.
    ge_max : float64, None
        If given, the peak Ge grading is set to this value.
    spring : Boolean, False
        If true, the simulation grid is chosen minimal so the result is suitable for SPRING BTE simulations.
    tn : Boolean, False
        If true, do a hydrodynamic simulation, else drift-diffusion.
    cont_type: char
        ohmic or schottky
    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """

    mat_name = "SiGe"
    mat_name_si = "Si"
    mat_name_ge = "Ge"
    if spring:  # material parameters adjusted to BTE simulator SPRING
        mat_name = "SSGS"
        mat_name_si = "SS"
        mat_name_ge = "GS"

    inp = {}

    inp["DOPING"] = [
        {
            "profile": "const", 
            "low_xyz": 0, 
            "upp_xyz": 0.007e-6, 
            "d_con": +1.8e20 * 1e6
        },{
            "profile": "exp",
            "low_xyz": 0.007001e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.007e-6,
            "d_con": +1.8e20 * 1e6,
            "a_xyz": 0.003e-6,
            "b_xyz": 2.8,
        },{
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": 0.016667e-6,
            "d_con": +2.27e18 * 1e6,
        },{
            "profile": "exp",
            "low_xyz": 0.016668e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.016668e-6,
            "d_con": +2.27e18 * 1e6,
            "a_xyz": 0.0011910138915e-6,
            "b_xyz": 2.8,
        },{
            "profile": "exp",
            "low_xyz": 0.016668e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.023259581033e-6,
            "d_con": -1e20 * 1e6,
            "a_xyz": 0.0025474512739e-6,
            "b_xyz": 3.1344195605,
        },{
            "profile": "exp",
            "low_xyz": 0,
            "upp_xyz": 0.061799e-6,
            "xyz_0": 0.08e-6,
            "d_con": +7.9564915229e20 * 1e6,
            "a_xyz": 0.0039575264888e-6,
            "b_xyz": 0.77809654453,
        },{
            "profile": "erfc",
            "low_xyz": 0.0618e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.07e-6,
            "d_con": +1.0030119201e20 * 1e6,
            "a_xyz": -0.011168063946e-6,
            "b_xyz": 0,
        }
    ]

    if ge_max is None:  # original version
        inp["GRADING"] = {
            "profile": "trap",
            "c_max": 0.3,
            "low_xyz": 0.012248e-6,
            "upp_xyz": 0.029928e-6,
            "xyz_dlow": 0.01278e-6,
            "xyz_dupp": 0.0018e-6,
            "xyz_rlow": 0.0004e-6,
            "xyz_rupp": 0.001e-6,
        }
    else:
        inp["GRADING"] = {
            "profile": "trap",
            "c_max": ge_max,
            "low_xyz": 0.012248e-6,
            "upp_xyz": 0.029928e-6,
            "xyz_dlow": 0.01278e-6,
            "xyz_dupp": 0.0018e-6,
            "xyz_rlow": 0.0004e-6,
            "xyz_rupp": 0.001e-6,
        }

    inp["REGION_DEF"] = [
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": 80e-9,
            "mod_name": mat_name,
        },{
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        },{
            "reg_mat": "CONT",
            "low_xyz": 80e-9,
            "upp_xyz": 80e-9,
            "mod_name": "CONT",
            "cont_name": "C",
        },{
            "reg_mat": "SUPPLY",
            "low_xyz": 22.75e-9,
            "upp_xyz": 23.75e-9,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    ]
    inp["REGION_INFO"] = {
        "spat_dim" : 1
    }

    inp["RANGE_GRID"] = [
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0, 22.75e-9],
            "n_pnts": 155,
        },{
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [22.75e-9, 23.75e-9],
            "n_pnts": 3,
        },{
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [23.75e-9, 80e-9],
            "n_pnts": 155,
        }
    ]
    if cont_type == "ohmic":
        inp["CONTACT"] = {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    elif  cont_type == "schottky":
        inp["CONTACT"] = {
            "mod_name": "CONT",
            "con_type": "schottky", 
            "v_n":0,
            "v_p":1e7,
        }    
    else:
        raise ValueError("Invalid input of cont_type, only input of ohmic and schottky are valid!")


    inp["SUPPLY"] = {
        "mod_name": "BASE",
        "supply_type": "p",
    }

    inp["DD"] = {
        "n_iter" : 500,
        "elec" : 1,
        "hole" : 1,
        "simul" : 1,
        "damp_method" : "none",
        "damp" : 0.3,
        "debug" : 0,
    }
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1
    if tn:
        inp["DD"]["tn"] = 1

    inp["OUTPUT"] = {
        "name" : "sige",
        "path" : version,
        "inqu_lev" : 1,
    }
    inp["BIAS_DEF"] = {
        "zero_bias_first" : 1,
        "dv_max" : 0.01,
    }
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)

    if not reco:
        inp = turn_off_recombination(inp)
    else:
        to_del = []
        for i in range(len(inp["RECOMB_DEF"])):
            if inp["RECOMB_DEF"][i]["type"]=="avalanche":
                to_del.append(i)
        inp["RECOMB_DEF"] = [el for i,el in enumerate(inp["RECOMB_DEF"]) if i not in to_del]

    for i in range(len(inp["MOB_DEF"])):
        if not sat or inp["MOB_DEF"][i]["valley"] == "H":
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"

    inp["POISSON"] = {
        "init_psi" : "buildin",
        "atol" : 1e-9,
    }
    inp["CONT_ELEC"] = {
        "atol" : 1e-11,
        "delta_max" : 1e-3,
    }
    inp["CONT_HOLE"] = {
        "atol" : 1e-9,
    }

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {
                    "x_norm" : 1,
                    "dens_norm" : "ni",
                }

        except ValueError:
            pass

    return inp
