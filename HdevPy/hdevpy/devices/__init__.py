"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""

name = "devices"

# some device definitions for convenience
from .sige_diode import sige_diode
from .sige_hbt import sige_hbt
from .n3_hbt import n3_hbt
from .n3_hbt_SG13G2 import n3_hbt_SG13G2
from .n3_hbt_2D import n3_hbt_2D
from .n1_hbt import n1_hbt
from .n2_hbt import n2_hbt
from .n4_hbt import n4_hbt
from .n5_hbt import n5_hbt
from .bjt import bjt
from .resistor import resistor
from .resistor_u import resistor_u
from .triode import triode
from .plate_cap import plate_cap
from .homo_junction import homo_junction
from .hetero_junction import hetero_junction
from .graded_hetero_junction import graded_hetero_junction
from .tunnel_resistor import tunnel_resistor
from .np_n_np import np_n_np
from .np_n_np_abrupt import np_n_np_abrupt
from .inp_hbt import getInPHBT
