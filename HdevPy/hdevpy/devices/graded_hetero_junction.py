"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


def graded_hetero_junction(length, material, version):
    """Generate a 1D structure with a graded semiconductor material like SiGe or InGaAs of length length doped with
    ND=doping.

    Parameters
    ----------
    length : float64
        Length in m of the resistor.
    material : dict
        A hdev material definition.
    version : float64
        hdev version identifier.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file using makeinp in DMT.Hdev.Duthdev
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    x_low = 0
    x_high = length

    try:
        mod_name = material["SEMI"]["mod_name"]
    except TypeError:
        mod_name = material["SEMI"][0]["mod_name"]

    inp.update(material)

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_low,
            "upp_xyz": x_high / 2,
            "d_con": 1e20,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_high / 2 + 1e-9,
            "upp_xyz": x_high,
            "d_con": 1e20,
        }
    )

    inp["GRADING"].append(
        {
            "profile": "const",
            "low_xyz": x_low,
            "upp_xyz": x_high / 3,
            "c_max": 0,
        }
    )
    inp["GRADING"].append(
        {
            "profile": "const",
            "low_xyz": x_high / 3,
            "upp_xyz": 2 * x_high / 3,
            "c_max": 0.5,
        }
    )
    inp["GRADING"].append(
        {
            "profile": "const",
            "low_xyz": 2 * x_high / 3,
            "upp_xyz": x_high,
            "c_max": 0,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": x_low,
            "upp_xyz": x_high / 2,
            "mod_name": mod_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": x_high / 2,
            "upp_xyz": x_high,
            "mod_name": mod_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_low,
            "upp_xyz": x_low,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": x_high,
            "upp_xyz": x_high,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [x_low, x_high],
            "n_pnts": 201,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "junctiion_" + mod_name + "_"

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.1

    return inp
