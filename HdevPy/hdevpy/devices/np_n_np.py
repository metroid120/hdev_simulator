"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""
from hdevpy.materials import materials
import copy
import json


def np_n_np(
    material,
    elec2=False,
    length=1.5e-6,
    dop=1e17 * 1e6,
    diff=None,
    l_valley_force_off=True,
    profile="none",
    dop_left=None,
    dop_right=None,
    d_left=None,
    d_right=None,
    iv=True,
    nl=False,
    nl_lambda=1000e-9,
    fermi=False,
    l_scat_type="default",
    l_force="phi",
    g_scat_type="default",
    g_force="phi",
    iv_force="psi",
    nl_fmax=False,
    fmax=10e5,
    tn=False,
    af=1,
    ai=1,
    beta=5,
):
    """Creates the hdev input definition for a non-abrupt n+nn+ structure used in paper https://ieeexplore.ieee.org/abstract/document/9361768?casa_token=AU1CrUscQwsAAAAA:c4dBRJ28YXIm_cZ0vihPt_KPtZdh2OiPcB5dLnfd9i7YcZuRT_VL3bJR4upep8y0n47yoLHLsCKG.

    Input
    -----
    material : str
        Defines the material to be used for the structure, possible values: Si InGaAs GaAs GaSb InP InAs
    elec2 : Bool,False
        If True: take into account conduction on the upper L valley of III-V semiconductors.
    length : float64,1.5e-6
        Defines the length of the structure
    dop : float64, 1e17 * 1e6
        The doping density of the n region in 1/m^3
    diff : float64,None
        Defines the smoothness of the transition from n+ to n doping.
    l_valley_force_off : bool,True
        If True, the carrier temperature in the L valley is assumed to be equal to the lattice temperature.
    profile :str, "none"
        Controls the doping profile. "none" or "smo" => smooth profile, "disc" => discrete doping profile.
    dop_left : float64, None
        If not none, defines the doping density at the left side of the structure in [1/m^3].
    dop_right : float64, None
        If not none, defines the doping density at the right side of the structure in [1/m^3].
    d_left : float64, None
        If not none, defines the length of the transition from n+ to n at the left side of the structure in [m].
    d_right : float64, None
        If not none, defines the length of the transition from n+ to n at the left side of the structure in [m].
    iv : Bool, True
        If True: take intervalley scattering into account.
    nl : Bool, False
        If True: Use non-local energy acquisition model, see the paper.
    nl_lambda : float64,1000e-9
        The energy relaxation length for calculating the non-local energy acquisition (fitting parameter).
    fermi : Bool, False
        If True: use Fermi statistics to calculate the carrier densities.
    l_scat_type : str,"default"
        Defines the hc_scat_type parameter of the BAND_DEF block for the L valley, see the manual.
    l_force : str, "phi"
        Defines the force parameter of the BAND_DEF block for the L valley, see the manual.
    g_scat_type : str, "default"
        Defines the hc_scat_type parameter of the BAND_DEF block for the G valley, see the manual.
    g_force : str, "phi"
        Defines the force parameter of the BAND_DEF block for the G valley, see the manual.
    iv_force : str, "psi"
        Defines the force parameter of the intervalley recombination RECOMB_DEF block, see the manual.
    nl_fmax : float64,False
        If True: use fmax parameter of the intervalley recombination RECOMB_DEF block, see the manual.
    fmax : float64, 10e5
        The value of the fmax parameter of the intervalley recombination RECOMB_DEF block, see the manual.
    beta : float65, 5
        The value of the beta parameter of the intervalley recombination RECOMB_DEF block, see the manual.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    xlow = 0

    xhigh = 2 * 1.5e-6 + length  # two contact regions
    if profile in ["disc", "smoo"]:
        xhigh = length + d_left + d_right

    if diff is None:
        diff = 50e-9

    if False:
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": xlow,
                "upp_xyz": xhigh,
                "d_con": +1e19 * 1e6,
                #'d_con'   : +1e3*1e6,
            }
        )
    elif profile == "disc":
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": xlow,
                "upp_xyz": d_left,
                "d_con": dop_left,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": d_left + 0.01e-9,
                "upp_xyz": d_left + length,
                "d_con": dop,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": d_left + length + 0.01e-9,
                "upp_xyz": d_left + length + d_right,
                "d_con": dop_right,
            }
        )

    elif profile == "smoo":
        # constant part in the middle
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": 0,
                "upp_xyz": xhigh,
                # 'd_con'   : +1e19*1e6,
                "d_con": dop,
                #'d_con'   : +1e19*1e6, #for testing
            }
        )

        # left smooth
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": d_left + 0.01e-9,
                "upp_xyz": d_left + length,
                "xyz_0": d_left,
                "a_xyz": diff * 0.5,
                "b_xyz": 1,
                "d_con": (dop_left - dop) / 2,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": 0,
                "upp_xyz": d_left,
                "xyz_0": d_left,
                "a_xyz": diff * 0.5,
                "b_xyz": 0,
                "d_con": (dop_left - dop) / 2,
            }
        )
        # right smooth
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": 0,
                "upp_xyz": d_left + length,
                "xyz_0": d_left + length,
                "a_xyz": -diff * 0.5,
                "b_xyz": 1,
                "d_con": (dop_left - dop) / 2,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": d_left + length + 0.01e-9,
                "upp_xyz": xhigh,
                "xyz_0": d_left + length,
                "a_xyz": -diff * 0.5,
                "b_xyz": 0,
                "d_con": (dop_left - dop) / 2,
            }
        )
    elif True:
        # inp['DOPING'].append({
        #     'profile' : 'const',
        #     'low_xyz' : xlow,
        #     'upp_xyz' : xhigh/4,
        #     'd_con'   : +1e19*1e6,
        # })
        # constant part in the middle
        dop_left = dop
        dop_right = dop
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": 0,
                "upp_xyz": xhigh,
                # 'd_con'   : +1e19*1e6,
                "d_con": dop_left,
                #'d_con'   : +1e19*1e6, #for testing
            }
        )
        # inp['DOPING'].append({
        #     'profile' : 'const',
        #     'low_xyz' : xhigh/2+0.01e-9,
        #     'upp_xyz' : xhigh,
        #     # 'd_con'   : +1e19*1e6,
        #     'd_con'   : dop_right,
        #     #'d_con'   : +1e19*1e6, #for testing
        # })

        # left smooth
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": 3 / 4 * 1.5e-6 + 0.1e-9,
                "upp_xyz": xhigh,
                "xyz_0": 3 / 4 * 1.5e-6,
                "a_xyz": diff,
                "b_xyz": 1,
                "d_con": dop_left * 1e2 * 0.5,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": 0,
                "upp_xyz": 3 / 4 * 1.5e-6,
                "xyz_0": 3 / 4 * 1.5e-6,
                "a_xyz": diff,
                "b_xyz": 0,
                "d_con": dop_left * 1e2 * 0.5,
            }
        )
        # right smooth
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": 0,
                "upp_xyz": xhigh - 3 / 4 * 1.5e-6 - 0.1e-9,
                "xyz_0": xhigh - 3 / 4 * 1.5e-6,
                "a_xyz": -diff,
                "b_xyz": 1,
                "d_con": dop_right * 1e2 * 0.5,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "erfc",
                "low_xyz": xhigh - 3 / 4 * 1.5e-6,
                "upp_xyz": xhigh,
                "xyz_0": xhigh - 3 / 4 * 1.5e-6,
                "a_xyz": -diff,
                "b_xyz": 0,
                "d_con": dop_right * 1e2 * 0.5,
            }
        )

    else:
        dx = 0.1e-6
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": xlow,
                "upp_xyz": dx,
                "d_con": +1e17 * 1e6,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": 0.1e-6,
                "upp_xyz": xhigh / 4,
                "d_con": +1e19 * 1e6,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": xhigh / 4,
                "upp_xyz": 3 * xhigh / 4,
                "d_con": +1e17 * 1e6,
                #'d_con'   : +1e19*1e6, #for testing
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": 3 * xhigh / 4,
                "upp_xyz": xhigh - dx,
                "d_con": +1e19 * 1e6,
            }
        )
        inp["DOPING"].append(
            {
                "profile": "const",
                "low_xyz": xhigh - dx,
                "upp_xyz": xhigh,
                "d_con": +1e17 * 1e6,
            }
        )

    str_material = material
    if type( material ) is not str:
        str_material = material["SEMI"]["mod_name"]

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            "upp_xyz": xhigh,
            "mod_name": str_material,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xlow,
            "upp_xyz": xlow,
            "mod_name": "CONT",
            "cont_name": "A",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xhigh,
            "upp_xyz": xhigh,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []
    # from left to right
    # first n+
    dx = xhigh / 3 / 5
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [xlow, xhigh],
            "n_pnts": 301,
        }
    )

    # inp['RANGE_GRID'].append({
    #     'disc_dir'  : 'x',
    #     'disc_set'  : 'pnts',
    #     'intv_pnts' : [xlow ,xhigh/3 - dx],
    #     'n_pnts'    : 41*2,
    # })
    # #intermediate region
    # inp['RANGE_GRID'].append({
    #     'disc_dir'  : 'x',
    #     'disc_set'  : 'pnts',
    #     'intv_pnts' : [xhigh/3 - dx ,xhigh/3 + dx],
    #     'n_pnts'    : 21*2,
    # })
    # #middle n region
    # inp['RANGE_GRID'].append({
    #     'disc_dir'  : 'x',
    #     'disc_set'  : 'pnts',
    #     'intv_pnts' : [xhigh/3+dx ,2*xhigh/3-dx],
    #     'n_pnts'    : 41*2,
    # })
    # #intermediate region
    # inp['RANGE_GRID'].append({
    #     'disc_dir'  : 'x',
    #     'disc_set'  : 'pnts',
    #     'intv_pnts' : [2*xhigh/3-dx ,2*xhigh/3+dx],
    #     'n_pnts'    : 21*2,
    # })
    # #right n+ region
    # inp['RANGE_GRID'].append({
    #     'disc_dir'  : 'x',
    #     'disc_set'  : 'pnts',
    #     'intv_pnts' : [2*xhigh/3+dx,xhigh],
    #     'n_pnts'    : 41*2,
    # })

    if type( material ) is not str:
        inp.update(material)
    elif material == "Si":
        silicon = materials["Si"]
        inp.update(silicon)

    elif material == "InGaAs" or material == "GaAsSb":
        material_paras = copy.deepcopy(materials[material])
        inp.update(material_paras)
        inp["GRADING"].append(
            {
                "profile": "const",
                "low_xyz": xlow,
                "upp_xyz": xhigh,
                "c_max": 0.5,
            }
        )

    elif (
        material == "GaAs"
        or material == "InP"
        or material == "InAs"
        or material == "GaSb"
    ):
        material_paras = copy.deepcopy(materials[material])
        if elec2:
            # use extracted material parameters from BTE
            try:
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material
                    + "_mob_G.json"
                )
                mob_G = json.load(open(para_path))
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material
                    + "_mob_L.json"
                )
                mob_L = json.load(open(para_path))
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material
                    + "_v.json"
                )
                iv_model = json.load(open(para_path))
            except FileNotFoundError:
                material_tmp = "GaAs"  # as long as no data available take GaAs
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material_tmp
                    + "_mob_G.json"
                )
                mob_G = json.load(open(para_path))
                mob_G["mod_name"] = material
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material_tmp
                    + "_mob_L.json"
                )
                mob_L = json.load(open(para_path))
                mob_L["mod_name"] = material
                para_path = (
                    "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                    + material_tmp
                    + "_v.json"
                )
                iv_model = json.load(open(para_path))
                iv_model["mod_name"] = material

            # replace models in default GaAs file
            index = 0
            for index in range(len(material_paras["MOB_DEF"])):
                if material_paras["MOB_DEF"][index]["valley"] == "G":
                    break
            index = 0
            material_paras["MOB_DEF"][index] = mob_G
            for index in range(len(material_paras["MOB_DEF"])):
                if material_paras["MOB_DEF"][index]["valley"] == "L":
                    break
            material_paras["MOB_DEF"][index] = mob_L
            # if iv:
            #     # add intervalley model
            #     material_paras["RECOMB_DEF"].append(iv_model)
        else:
            para_path = (
                "/home/viktor/Documents/Gitprojects/inp_hbt_cal/BulkBte/material_paras/"
                + material
                + "_mob_G_single.json"
            )
            mob_G = json.load(open(para_path))

            # replace models in default GaAs file
            index = 0
            for index in range(len(material_paras["MOB_DEF"])):
                if material_paras["MOB_DEF"][index]["valley"] == "G":
                    break
            material_paras["MOB_DEF"][index] = mob_G
            material_paras["SEMI"]["elec2"] = 0
            material_paras["SEMI"]["hole"] = 0

            inp["CONT_ELEC"] = {}
            inp["CONT_ELEC"]["atol"] = 1e-9
            inp["CONT_ELEC"]["rtol"] = 1e-5
            inp["CONT_ELEC"]["ctol"] = 0

        inp.update(material_paras)
        # relevant papers:
        # Band parameters for III–V compound semiconductors and their alloys
        # Concentration-dependent absorption and photoluminescence of n-type inP
        # Effective electron mass and plasma filter characterizationofn-type InGaAs and InAsP
        # The electron effective mass in heavily doped GaAs
        # ndm
        # inp['MOB_DEF'].append({
        #     'mod_name': 'silicon',
        #     'band'    : 'cb',
        #     'type'    : 'ndm',
        #     'mu_0'    : 1646*1e-4, #cm^2/Vs
        #     'mob_max' : 4212*1e-4, #cm^2/vs
        #     'mob_dop' : 6.62e16*1e6,      #cm^2/vs
        #     'mob_beta': 0.732,        #beta for doping dependence
        #     'beta'    : 2.365,
        #     'v_sat'   : 8.189e6*1e-2, #cm/s
        #     'mob_f0'  : 12.19*1e3*1e2,
        # })
        # silicon

    elif material == "inp_single_electron_gas_upper_band":
        # testcase: same as above, however we now use only the uppermost band in dd_solver.f90

        # carrier density in gamma band
        inp["BAND_DEF"] = []
        inp["BAND_DEF"].append(
            {
                "mod_name": "silicon",
                "type": "para",
                "e_0": +1.344 / 2,
                "deg": 1,
                "band": "cb",
                "m_eff": 0.08,
                "alpha": -0.7,  # see eff_mass_def.py and publictions
            }
        )

        # simple ndm mobility model
        inp["MOB_DEF"] = []
        inp["MOB_DEF"].append(
            {
                "mod_name": "silicon",
                "band": "cb1",
                "type": "const",
                "mu_0": 80 * 1e-4,  # cm^2/Vs
                "beta": 1.4,
                "v_sat": 1.07e7 * 1e-2,  # cm/s
            }
        )

        inp["SEMI"] = {}
        inp["SEMI"]["mod_name"] = "silicon"  # just a name, actually InP
        inp["SEMI"]["elec2"] = 1
        inp["SEMI"]["elec"] = 0
        inp["SEMI"]["hole"] = 0
        inp["SEMI"]["fermi"] = 1
        inp["SEMI"]["dim"] = 3
        inp["SEMI"]["eps"] = 12.5
        inp["SEMI"]["elaf_0"] = 4.38  # should have no influence

        inp["SEMI"]["temp"] = 300  # semiconductor temperature
        inp["SEMI"]["temp0"] = 300  # reference temperature of models

        inp["SEMI"]["bgap_vhd0"] = 0
        inp["SEMI"]["bgap_narrowing_model"] = "jain"
        inp["SEMI"]["bgap_a"] = 2.25e-9 * 1e-6  # https://doi.org/10.1063/1.334786
        # maybe need to recheck those parameters later .... Band parameters for III–V compound semiconductors and their alloys
        inp["SEMI"]["bgap_at1"] = (
            0.363e-3 * 300
        )  # conversion to T/Tmat formulation in hdev
        inp["SEMI"]["bgap_at2"] = 162 / 300
        inp["SEMI"]["bgap_fvgt"] = 2  # select model equation (as in device)

    elif material == "inp_two_electron_gas":
        # testcase: same as above, however we now use only the uppermost band in dd_solver.f90

        # carrier density in gamma band
        inp["BAND_DEF"] = []
        inp["BAND_DEF"].append(
            {
                "mod_name": "silicon",
                "type": "para",
                "e_0": +1.4236 / 2,
                "deg": 1,
                "band": "cb1",
                "m_eff": 0.08,
                "alpha": -0.7,  # see eff_mass_def.py and publictions
            }
        )

        inp["BAND_DEF"].append(
            {
                "mod_name": "silicon",
                "type": "para",
                "e_0": +2.014 / 2,
                "deg": 4,
                "band": "cb2",
                "m_eff": 0.47,
                "alpha": -0.7,  # see eff_mass_def.py and publictions
            }
        )

        # simple ndm mobility model
        inp["MOB_DEF"] = []
        # silicon
        inp["MOB_DEF"].append(
            {
                "mod_name": "silicon",
                "band": "cb1",
                "type": "const",
                "mu_0": 200 * 1e-4,  # cm^2/Vs
                "beta": 3,
                "v_sat": 1.07e7 * 1e-2,  # cm/s
            }
        )
        inp["MOB_DEF"].append(
            {
                "mod_name": "silicon",
                "band": "cb2",
                "type": "const",
                "mu_0": 50 * 1e-4,  # cm^2/Vs
                "beta": 2,
                "v_sat": 1.07e7 * 1e-2,  # cm/s
            }
        )

        inp["RECOMB_DEF"] = {}
        inp["RECOMB_DEF"]["mod_name"] = "silicon"
        inp["RECOMB_DEF"]["type"] = "intervalley"
        inp["RECOMB_DEF"]["tau_21"] = 0.3e-12
        inp["RECOMB_DEF"]["beta"] = 3e0  # 1e1 good for testing convergence

        inp["SEMI"] = {}
        inp["SEMI"]["mod_name"] = "silicon"  # just a name, actually InP
        inp["SEMI"]["elec2"] = 1
        inp["SEMI"]["elec"] = 1
        inp["SEMI"]["hole"] = 0
        inp["SEMI"]["fermi"] = 0
        inp["SEMI"]["dim"] = 3
        inp["SEMI"]["eps"] = 12.5
        inp["SEMI"]["elaf_0"] = 4.38  # should have no influence

        inp["SEMI"]["temp"] = 300  # semiconductor temperature
        inp["SEMI"]["temp0"] = 300  # reference temperature of models

        inp["SEMI"]["bgap_vhd0"] = 0
        inp["SEMI"]["bgap_narrowing_model"] = "jain"
        inp["SEMI"]["bgap_a"] = 2.25e-9 * 1e-6  # https://doi.org/10.1063/1.334786
        # maybe need to recheck those parameters later .... Band parameters for III–V compound semiconductors and their alloys
        inp["SEMI"]["bgap_at1"] = (
            0.363e-3 * 300
        )  # conversion to T/Tmat formulation in hdev
        inp["SEMI"]["bgap_at2"] = 162 / 300
        inp["SEMI"]["bgap_fvgt"] = 2  # select model equation (as in device)

    else:
        raise IOError("material not implemented")

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    # for two electron gas model, quasi Fermi potential is not known!
    if elec2:
        # inp['CONTACT'][0]['ohmic_bc'] = 'neutrality' #ohmic should work now
        pass

    for rec in inp["RECOMB_DEF"]:
        if rec["type"] == "intervalley":
            rec["force"] = iv_force

    # mobility in L valley smaller
    for i in range(len(inp["MOB_DEF"])):
        if inp["MOB_DEF"][i]["valley"] == "L":
            inp["MOB_DEF"][i]["hc_scat_type"] = l_scat_type
            inp["MOB_DEF"][i]["force"] = l_force

    # mobility in G valley smaller
    for i in range(len(inp["MOB_DEF"])):
        if inp["MOB_DEF"][i]["valley"] == "G":
            inp["MOB_DEF"][i]["hc_scat_type"] = g_scat_type
            inp["MOB_DEF"][i]["force"] = g_force

    inp["DD"] = {}
    inp["DD"]["remove_zeros"] = -1
    inp["DD"]["n_iter"] = 500
    inp["DD"]["simul"] = 1
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 0
    if elec2:
        inp["DD"]["elec2"] = 1
        try:
            inp["SEMI"]["elec2"] = 1
            inp["SEMI"]["hole"] = 1
        except TypeError:
            for semi_i in inp["SEMI"]:
                semi_i["elec2"] = 1
                semi_i["hole"] = 1
    if tn:
        inp["DD"]["elec2"] = 0
        inp["SEMI"]["elec2"] = 0
        inp["DD"]["tn"] = 1
        inp["SEMI"]["hole"] = 1

    try:
        inp["SEMI"]["fermi"] = 0
        if fermi:
            inp["SEMI"]["fermi"] = 1
    except TypeError:
        for semi_i in inp["SEMI"]:
            semi_i["fermi"] = 0
            if fermi:
                semi_i["fermi"] = 1

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.1

    if nl:
        inp["NON_LOCAL"] = {}
        inp["NON_LOCAL"]["type"] = "npnnp"
        inp["NON_LOCAL"]["xj_l"] = 1e-6 #1200e-9 
        inp["NON_LOCAL"]["xj_r"] = 3e-6 #2900e-9
        inp["NON_LOCAL"]["lambda"] = nl_lambda
        inp["NON_LOCAL"]["af"] = af
        inp["NON_LOCAL"]["ai"] = ai
        if nl_fmax:
            inp["NON_LOCAL"]["fmax"] = fmax
            inp["NON_LOCAL"]["beta"] = beta

    return inp
