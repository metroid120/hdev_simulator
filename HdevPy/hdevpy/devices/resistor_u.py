"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


def resistor_u(version, doping, material, extrude=False):
    """Generates a 2D resistor structure  with some oxide and constant doping in a rectangular box.

    Parameters
    ----------
    version : str
        hdev version specifier.
    doping : float64
        Doping in [1/m^3].
    material : dict
        Dict that specifies the hdev material to be used.
    extrude : False,Bool
        If True, the contacts are 2d boxes, else 1d lines at the side of the simulation domain.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file using makeinp in DMT.Hdev.Duthdev
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    try:
        mod_name = material["SEMI"]["mod_name"]
    except TypeError:
        mod_name = material["SEMI"][0]["mod_name"]

    inp.update(material)

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": [0, 0],
            "upp_xyz": [1e-6, 1e-6],
            "d_con": doping,
            #'d_con'   : +1e3*1e6,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": [0, 0],
            "upp_xyz": [1e-6, 1e-6],
            "mod_name": mod_name,
        }
    )
    if extrude:
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": [0, 0],
                "upp_xyz": [0.1e-6, 0.25e-6],
                "mod_name": "CONT",
                "cont_name": "C",
            }
        )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": [0, 0.75e-6],
                "upp_xyz": [0.1e-6, 1e-6],
                "mod_name": "CONT",
                "cont_name": "A",
            }
        )

    else:
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": [0, 0],
                "upp_xyz": [0, 0.25e-6],
                "mod_name": "CONT",
                "cont_name": "C",
            }
        )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": [0, 0.75e-6],
                "upp_xyz": [0, 1e-6],
                "mod_name": "CONT",
                "cont_name": "A",
            }
        )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "OXID",
            "low_xyz": [0, 0.25e-6],
            "upp_xyz": [0.5e-6, 0.75e-6],
            "mod_name": "SiO2",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 2

    inp["RANGE_GRID"] = []

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "y",
            "disc_set": "pnts",
            "intv_pnts": [0, 1e-6],
            "n_pnts": 101,
        }
    )

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [0, 1e-6],
            "n_pnts": 101,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 20
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "resistor_" + mod_name + "_"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["cap_lev"] = 1

    inp["POISSON"] = {}
    inp["POISSON"]["atol"] = 1e-5
    inp["POISSON"]["rtol"] = 1e-3
    inp["POISSON"]["ctol"] = 0

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.02

    return inp
