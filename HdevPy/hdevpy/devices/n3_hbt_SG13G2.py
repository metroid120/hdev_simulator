"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""

from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def n3_hbt_SG13G2(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    reco=False,
    ge_max=None,
    spring=True,
    tn=False,
):
    """Creates the hdev input definition for the node 3 SiGe HBT of the ITRS roadmap.

    Input
    -----
    version : str
        Name of the hdev version.
    fermi : Bool, False
        If true, use Fermi statistics, else Boltzmann.
    normalization : Bool, False
        If true, normalize the equation system.
    reco : Bool, False
        If true, recombination models are activated.
    bohm : Bool, False
        If true, use the bohm potential.
    sat : Bool, True
        Activate the saturation velocity model.
    ge_max : float64, None
        If given, the peak Ge grading is set to this value.
    spring : Boolean, False
        If true, the simulation grid is chosen minimal so the result is suitable for SPRING BTE simulations.
    tn : Boolean, False
        If true, do a hydrodynamic simulation, else drift-diffusion.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """
    colcoord = 140e-9#+1200e-9
    emmcoord = -0e-6
    mat_name = "SiGe"
    mat_name_si = "Si"
    mat_name_ge = "Ge"
    if spring:  # material parameters adjusted to SPRING
        mat_name = "SSGS"
        mat_name_si = "SS"
        mat_name_ge = "GS"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    #  &CONST_DOP d_con=-1.8e+20 xyz_low=0 0 xyz_upp=0.007 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {"profile": "const", "low_xyz": emmcoord, "upp_xyz": 0.01e-6, "d_con": +1.0e20 * 1e6}
    )

    # inp["DOPING"].append(
    #     {"profile": "const", "low_xyz": 0.02e-6, "upp_xyz": 0.04e-6, "d_con": +1.0e18* 1e6}
    # )
    #  &REXP_DOP d_max=-1.8e+20 xyz_0=0.007 0 a_xyz=0.003 0
    #            beta_xyz=2.8 xyz_low=0.007001 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.010001e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.01e-6,
            "d_con": +1.0e20 * 1e6,
            "a_xyz": 0.006e-6,
            "b_xyz":2.3,
        }
    )
    #  &CONST_DOP d_con=-2.27e+18 xyz_low=0 0 xyz_upp=0.016667 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": emmcoord,
            "upp_xyz": 0.016667e-6,
            "d_con": +1.17e18 * 1e6,
        }
    )
    #  &REXP_DOP d_max=-2.27e+18 xyz_0=0.016668 0 a_xyz=0.0011910138915 0
    #            beta_xyz=2.8 xyz_low=0.016668 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.016668e-6,#0.016668e-6
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.016668e-6,
            "d_con": +2.27e18 * 1e6,
            "a_xyz": 0.0011910138915e-6,
            "b_xyz": 2.8,
        }
    )
    #  &REXP_DOP d_max=1e+20 xyz_0=0.023259581033 0 a_xyz=0.0025474512739 0
    #            beta_xyz=3.1344195605 xyz_low=0 0 xyz_upp=0.08 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.016668e-6,
            "upp_xyz": 0.08e-6,
            "xyz_0": 0.026355581033e-6,
            "d_con": -1.4e20 * 1e6,
            "a_xyz": 0.0024474512739e-6,
            "b_xyz": 2.0344195605,
        }
    )
    #  &REXP_DOP d_max=-7.9564915229e+20 xyz_0=0.08 0 a_xyz=0.0039575264888 0
    #            beta_xyz=0.77809654453 xyz_low=0 0 xyz_upp=0.061799 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    # inp["DOPING"].append(
    #     {
    #         "profile": "exp",
    #         "low_xyz": 0,
    #         "upp_xyz": 0.061799e-6,
    #         "xyz_0": 0.08e-6,
    #         "d_con": +7.9564915229e20 * 1e6,
    #         "a_xyz": 0.0037775264888e-6,
    #         "b_xyz": 0.77809654453,
    #     }
    # )
    #  &ERFC_EXP d_max=-1.0030119201e+20 xyz_max=0.07 0 a_xyz=-0.011168063946 0
    #            xyz_low=0.0618 0 xyz_upp=0.08 1 /
    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0.0318e-6,
            "upp_xyz": colcoord,
            "xyz_0": 0.0495e-6,
            "d_con": +6.5530119201e18 * 1e6,
            "a_xyz": -0.008e-6,
            "b_xyz": 0,
        }
    )
    # inp["DOPING"].append(
    #     {
    #         "profile": "erfc",
    #         "low_xyz": 0.0618e-6,
    #         "upp_xyz": 0.14e-6,
    #         "xyz_0": 0.1225e-6,
    #         "d_con": +7.520e19 * 1e6,
    #         "a_xyz": -0.0192e-6,
    #         "b_xyz": 0,
    #     }
    # )

    inp["DOPING"].append(
        {
            "profile": "erfc",
            "low_xyz": 0.0618e-6,
            "upp_xyz": colcoord,
            "xyz_0": 0.1215e-6,
            "d_con": +7.4e19 * 1e6,
            "a_xyz": -0.0183e-6,
            "b_xyz": 0,
        }
    )    
    # inp["DOPING"].append(
    #     {"profile": "const", "low_xyz": 140e-9, "upp_xyz": 160e-9, "d_con": +1.5e20 * 1e6}
    # )
    # inp["DOPING"].append(
    #     {
    #         "profile": "erfc",
    #         "low_xyz": 0,#0.0618e-6,
    #         "upp_xyz": 0.14e-6,
    #         "xyz_0": 0.8e-6,
    #         "d_con": +1.3030119201e20 * 1e6,
    #         "a_xyz": 2.25168063946,
    #         "b_xyz": 0,
    #     }
    # )
    # inp["DOPING"].append(
    #     {
    #         "profile": "exp",
    #         "low_xyz": 0.0618e-6,
    #         "upp_xyz": 0.14e-6,
    #         "xyz_0": 0.14e-6,
    #         "d_con": +1.3030119201e20 * 1e6,
    #         "a_xyz": -0.015168063946e-6,
    #         "b_xyz": 2.1,
    #     }
    # )
    #  &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.012248 -1e-10 xyz_upp=0.029928 1
    #             xyz_dlow=0.01278 0.0 xyz_dupp=0.0018 0.0 xyz_rlow=0.0004 0.0 xyz_rupp=0.001 0.0 /
    # if ge_max is None:  # original version
    #     inp["GRADING"].append(
    #         {
    #             "profile": "trap",
    #             "c_max": 0.28,
    #             "low_xyz": 0.0215e-6,
    #             "upp_xyz": 0.036e-6,
    #             "xyz_dlow": 0.004e-6,
    #             "xyz_dupp": 0.0033e-6,
    #             "xyz_rlow": 0.0022e-6,
    #             "xyz_rupp": 0.002e-6,
    #         }
    #     )
    # else:
    #     inp["GRADING"].append(
    #         {
    #             "profile": "trap",
    #             "c_max": ge_max,
    #             "low_xyz": 0.012248e-6,
    #             "upp_xyz": 0.029928e-6,
    #             "xyz_dlow": 0.01278e-6,
    #             "xyz_dupp": 0.0018e-6,
    #             "xyz_rlow": 0.0004e-6,
    #             "xyz_rupp": 0.001e-6,
    #         }
    #     )
    # inp["GRADING"].append(
    #     {
    #         "profile": "gauss",
    #         "c_max": 0.281,
    #         "low_xyz": 0.019e-6,
    #         "upp_xyz": 0.039e-6,
    #         "sigma_xyz": 0.005474512739e-6,
    #         "xyz_0": 0.0289e-6,
    #         # "b_xyz": 3.4,

    #         # "xyz_dlow": 0.004e-6,
    #         # "xyz_dupp": 0.0033e-6,
    #         # "xyz_rlow": 0.0022e-6,
    #         # "xyz_rupp": 0.002e-6,
    #     }
    # )

    inp["GRADING"].append(
        {
            "profile": "exp",
            "low_xyz": 0.02e-6,
            "upp_xyz": 0.036e-6,
            "xyz_0": 0.0289e-6,
            "c_max": 0.281,
            "a_xyz": 0.0066474512739e-6,
            "b_xyz": 3.4,
        }
    )


    # inp["GRADING"].append(
    #     {
    #         "profile": "exp",
    #         "low_xyz": 0.02e-6,
    #         "upp_xyz": 0.036e-6,
    #         "xyz_0": 0.0289e-6,
    #         "c_max": 0.281,
    #         "a_xyz": 0.0062e-6,
    #         "b_xyz": 3.7,
    #     }
    # )

    # inp['GRADING'].append({
    #     'profile' : 'trap',
    #     'c_max'   : 0.3,
    #     'low_xyz' : 0.012e-6,
    #     #'upp_xyz' : 0.029928e-6,
    #     'upp_xyz' : 0.033e-6,
    #     'xyz_dlow': 0.012e-6,
    #     'xyz_dupp': 0.002e-6,
    #     'xyz_rlow': 0.0004e-6,
    #     'xyz_rupp': 0.001e-6,
    # })

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": emmcoord,
            "upp_xyz": colcoord,
            "mod_name": mat_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": emmcoord,
            "upp_xyz": emmcoord,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": colcoord,
            "upp_xyz": colcoord,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 25.75e-9,
            "upp_xyz": 26.75e-9,
            
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [emmcoord, 22.75e-9],
            "n_pnts": 155,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [25.75e-9, 26.75e-9],
            "n_pnts": 30,
        }
    )
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [23.75e-9, colcoord],
            "n_pnts": 195,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1  # war 30... wieso
    if tn:
        inp["DD"]["tn"] = 1
        # inp['DD']['simul']    = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)
    x = 0.28

    tw0 = 0.391*(1-x) +0.449*x -0.05e-12*(1-x)*x
    tw1 = -0.144434e-12
    c1 = 0.00135*(1-x) + 0.0028**x -0.00181*(1-x)*x
    c2 = -0.059
    c3 = 0.0107



    t0 = tw0
    t1 = tw1

    # inp['TAU_E'][0] = {  # tau_e parameters from Diss. Michaillat
    #     "mod_name": mat_name,
    #     "valley": "x",
    #     "tau_type": "default",
    #     "t0": t0,
    #     "t1": t1,
    #     "c0": 0,
    #     "c1": c1,
    #     "c2": c2,
    #     "c3": c3,
    # }
    # inp['TAU_E'][0] = {  # tau_e parameters from Diss. Michaillat
    #     "mod_name": "SSGS",
    #     "valley": "x",
    #     "tau_type": "michaillat",
    #     "t0": 0.38e-12,
    #     "t1": 0.04e-12,
    #     "c0": 0.18,
    # }
    # inp['TAU_E'][0] = {  # tau_e parameters from Diss. Michaillat
    #     "mod_name": mat_name,
    #     "valley": "x",
    #     "tau_type": "michaillat",
    #     "t0": 0.08e-12,
    #     "t1": 0.02e-12,
    #     "c0": 0.1,
    # }
    # inp['TAU_E'][0] = {  # tau_e parameters from Diss. Michaillat
    #     "mod_name": mat_name,
    #     "valley": "x",
    #     "tau_type": "michaillat",
    #     "t0": 0.08e-12,
    #     "t1": 0.01e-12,
    #     "c0": 0.1,
    # }
    if not reco:
        inp = turn_off_recombination(inp)

    for i in range(len(inp["MOB_DEF"])):
        if not sat or inp["MOB_DEF"][i]["valley"] == "H":
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        inp["MOB_DEF"][i]["fac_driving_force"] = (1.95)/(1.5)#7/2/(2.5)#7/2/(1.5)#(1.225)#(1.125)
        # inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1
        # inp["MOB_DEF"][i]["beta"] = 1.4

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-11
    inp["CONT_ELEC"]["delta_max"] = 1e-3

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    return inp
