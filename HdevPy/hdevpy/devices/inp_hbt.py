""" Return an InP HBT
"""
from hdevpy.materials import (
    InP,
    InGaAs,
    GaAsSb,
)
from hdevpy.tools import (
    set_hdev_material_def,
    turn_off_recombination,
)

from DMT.Hdev import (
    DutHdev,
)
from DMT.core import DutType
import numpy as np
import copy
import os
from scipy.interpolate import interp1d


def getInPHBT(
    vary={},
    no_inqu=False,
    fermi=False,
    elec2=False,
    nl=False,
    sat=True,
    te=False,
    iv=False,
    tn=False,
    **kwargs,
):
    """Get a DutHdev for the InP HBT with several possible modifications

    Input
    -------
    vary : {}, dict
        This dictionary can be used to change the keyword arguments given to the tsc250_hbt function, called herein for generating the raw input file
    no_inqu : False, Bool
        If True, no inqu data is written for improving simulation speed.
    fermi : False, Bool
        If True, Fermi-Dirac statistics are used for all calculations.
    elec2 : False, Bool
        If True, transport in the L band is also considered.
    nl : False, Bool
        If True, non-local transport effects as discussed in our two-valley paper are considered.
    sat : False, Bool
        If True, velocity saturation is activated.
    te : False, Bool
        If True, a thermionic emission boundary condition is applied at the abrupt BE heterojunction.
    iv : False, Bool
        If True, intervalley transport between the G and L valley is considered as described in our two-valley paper.
    tn : False, Bool
        If True, the hydrodynamic transport model is activated.

    Returns
    -------
    dut : DutHdev
        A DutHdev for simulations with DMT_core.
    """
    for key, val in vary.items():
        kwargs[key] = val

    inp = inp_hbt_tsc250(**kwargs)
    if no_inqu:
        inp["OUTPUT"]["inqu_lev"] = 0

    fermi_ = 0
    if fermi:
        fermi_ = 1
    for i, _semi in enumerate(inp["SEMI"]):
        inp["SEMI"][i]["fermi"] = fermi_

    if elec2:
        inp["DD"]["elec2"] = 1
        for i, _semi in enumerate(inp["SEMI"]):
            inp["SEMI"][i]["elec2"] = 1

    if nl:
        inp["NON_LOCAL"] = {
            "type": "simple",
            "lambda": 10e-9,
            "xj_l": 180e-9,
        }

    if tn:
        inp["DD"]["tn"] = 1

    # adjust saturation models
    for i, mob_def in enumerate(inp["MOB_DEF"]):
        if mob_def["valley"] == "G":  # deactivate saturation in G valley
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        elif mob_def["valley"] == "L":  # L valley mix force
            inp["MOB_DEF"][i]["force"] = "mix"

    if not sat:
        for i, mob in enumerate(inp["MOB_DEF"]):
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"

    # Thermionic emission boundary condition
    if te:
        inp["THERMIONIC_EMISSION"] = {"x": 130e-9}

    if not iv:
        reco = copy.deepcopy(inp["RECOMB_DEF"])
        inp = turn_off_recombination(inp)
        inp["RECOMB_DEF"] = []
        for reco_i in reco:
            # if reco_i["type"] not in ["intervalley", "aug", "langevin"]:
            if reco_i["type"] not in ["intervalley"]:
                inp["RECOMB_DEF"].append(reco_i)
                pass

    else:
        reco = copy.deepcopy(inp["RECOMB_DEF"])
        del inp["RECOMB_DEF"]
        inp["RECOMB_DEF"] = []
        for reco_i in reco:
            if reco_i["type"] in ["intervalley"]:
                inp["RECOMB_DEF"].append(reco_i)

    for i, mob_def in enumerate(inp["MOB_DEF"]):
        if mob_def["valley"] == "H":
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"

    return DutHdev(
        None,
        DutType.npn,
        inp,
        reference_node="E",
        list_copy=os.path.join("tmp/tabular_data.hdf5"),
    )


def inp_hbt_tsc250(
    d_emitter3=80e-9,
    d_emitter4=10e-9,
    d_emitter5=30e-9,
    d_inasp=10e-9,
    d_base=35e-9,
    d_setback=15e-9,
    d_bc_grade=24e-9,
    d_sig=3e-9,
    d_collector=150e-9,
    dop_emitter_cap3=3e19,
    dop_emitter1=8e17,
    dop_emitter2=5e17,
    dop_base_start=-8e19,
    dop_base_end=-4e19,
    dop_setback=3.5e16,
    dop_bc_grade=3.5e16,
    dop_sig=3.5e18,
    dop_collector=3.5e16,
    grad_be=0.34,  # InAsP at BE junc.
    c_spike=True,  # InAsP at BE junc.
):
    # InP HBT from "Sub-300 nm InGaAs/InP Type-I DHBTs with a 150 nm collector,
    # 30 nm base demonstrating 755 GHz fmax and 416 GHz ft"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    # distances of each region
    distances = {}
    distances["d_emitter3"] = d_emitter3
    distances["d_emitter4"] = d_emitter4
    distances["d_emitter5"] = d_emitter5
    distances["d_inasp"] = d_inasp
    distances["d_base"] = d_base
    distances["d_setback"] = d_setback
    distances["d_bc_grade"] = d_bc_grade
    distances["d_sig"] = d_sig
    distances["d_collector"] = d_collector
    distances["d_subcollector1"] = 5e-9
    xlow = 0
    xhigh = 0
    for key, val in distances.items():
        xhigh += val

    # doping densities
    dopings = {}
    dopings["dop_emitter_cap3"] = dop_emitter_cap3
    dopings["dop_emitter1"] = dop_emitter1
    dopings["dop_emitter2"] = dop_emitter2
    dopings["dop_emitter3"] = dop_emitter2
    dopings["dop_base"] = (dop_base_start, dop_base_end)
    dopings["dop_setback"] = dop_setback
    dopings["dop_bc_grade"] = dop_bc_grade
    dopings["dop_sig"] = dop_sig if c_spike else dop_collector  #
    dopings["dop_collector"] = dop_collector
    dopings["dop_subcollector1"] = 1e19  # debug

    # materials
    mats = [
        "InP",
        "InP",
        "InP",
        "InAsP",
        "InGaAs",
        "InGaAs",
        "InGaAs",
        "InP",
        "InP",
        "InP",
    ]
    gradings = [  # Indium concentration
        0,  # 'InP',
        0,  # 'InP',
        0,  # 'InP',
        (0, grad_be),  # 'InAsP',
        0.53,  # 'InGaAs
        (0.53, 0.35),  # 'InGaAs
        (0.35, 0.25),  # 'InGaAs
        (0.25, 0),  # InP
        0,  # 'InP',
        0,  # 'InP',
        0,  # 'InP',
        0,  # 'InP',
    ]
    # create grading in Python and pass it to COOS with HDF5 below
    gradings_x = []
    gradings_y = []

    xlow_ = 0
    xhigh_ = 0
    grad_last = 0
    grad_next = 0
    for i, (dop, dist, mat, grad) in enumerate(
        zip(dopings.values(), distances.values(), mats, gradings)
    ):
        if xlow_ == 0:
            xlow_ = -1e-9

        xhigh_ += dist
        if i == len(dopings) - 1:
            ppp = 1e-3
        else:
            ppp = 0

        if not isinstance(dop, float):
            # d = exp(-|x-x0|/a)
            # d(x) = dcon*exp(-|x-x0|/a) = dop(2)
            # x0=xhigh
            # d(xhigh) = dcon
            # d(xlow) = dcon*exp(-|xlow-xhigh|/a)
            # a=/log(d(xlow)/d(xhigh))
            # a=-|x-x0|/ln(dop2)
            a = -np.abs(xlow_ - xhigh_) / np.log((dop[0] / dop[1]))
            inp["DOPING"].append(
                {
                    "profile": "exp",
                    "low_xyz": xlow_,
                    "upp_xyz": xhigh_ - 0.1e-10 + ppp,
                    "d_con": dop[0] * 1e6,
                    "a_xyz": a,
                    "xyz_0": xlow_,
                }
            )
        else:
            inp["DOPING"].append(
                {
                    "profile": "const",
                    "low_xyz": xlow_,
                    "upp_xyz": xhigh_ - 0.1e-10 + ppp,
                    "d_con": dop * 1e6,
                    "a_xyz": 1e-9,
                }
            )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "SEMI",
                "low_xyz": xlow_,
                "upp_xyz": xhigh_,
                "mod_name": mat,
            }
        )
        gradings_x.append((xlow_ + 0e-9, xhigh_ + 0e-9 - 0.001e-9))
        if not isinstance(grad, tuple):  # constant grading
            gradings_y.append((grad, grad))
        else:  # linear grading
            gradings_y.append((grad[0], grad[1]))
        if xlow_ == -1e-9:
            xlow_ = 0
        xlow_ += dist

    # create smooth grading, maybe plot for debugging
    x = np.array(gradings_x).flatten()
    y = np.array(gradings_y).flatten()
    f2 = interp1d(x, y, kind="linear")
    xnew = np.linspace(100e-9, 250e-9, 1000)
    mol = f2(xnew)

    # diffuse the grading
    index_bn = np.argmin(np.abs(xnew - 140e-9))
    mol_p = copy.deepcopy(mol)
    for i in range(20):
        for j, _x in enumerate(xnew):
            if j > index_bn and j < len(xnew) - 2:
                arg = (
                    5e-20
                    * (mol[j + 1] - 2 * mol[j] + mol[j - 1])
                    / (xnew[j + 2] - xnew[j - 1]) ** 2
                )
                if arg > 0:
                    dummy = 1
                mol_p[j] = mol[j] + arg

        mol = mol_p

    # use this plot to visualize the grading
    # import matplotlib.pyplot as plt
    # plt.plot(x, y, "o")
    # plt.plot(x, y, "o", xnew, mol, "-")
    # plt.legend(["data", "cubic"], loc="best")
    # plt.show()
    inp["GRADING"].append(
        {
            "profile": "hdf",
            "low_xyz": np.min(xnew),
            "upp_xyz": np.min(xhigh),
        }
    )
    import h5py

    # checking if the directory tmp exist or not.
    if not os.path.exists("tmp"):
        os.makedirs("tmp")

    h5f_doping = h5py.File(
        os.path.join("tmp/tabular_data.hdf5"),
        "w",
    )
    h5f_doping.create_dataset("X", data=xnew)
    h5f_doping.create_dataset("grading", data=mol)
    h5f_doping.close()

    # doping from "Sub-300 nm InGaAs/InP Type-I DHBTs with a 150 nm collector,
    # 30 nm base demonstrating 755 GHz fmax and 416 GHz ft"

    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xlow,
            "upp_xyz": xlow,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": xhigh,
            "upp_xyz": xhigh,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 140e-9,
            "upp_xyz": 142e-9,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1
    inp["REGION_INFO"]["hdf"] = "tabular_data.hdf5"

    inp["RANGE_GRID"] = []
    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [xlow, xhigh],
            "n_pnts": 501,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 100
    inp["DD"]["elec"] = 1
    inp["DD"]["elec2"] = 0
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    inp["DD"]["diffuse"] = 1e-19  # apply diffusion equation for doping

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "teledyne"
    inp["OUTPUT"]["path"] = "coos"
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, "InP")
    inp = set_hdev_material_def(inp, "InGaAs")
    inp = set_hdev_material_def(inp, "InAsP")
    inp = copy.deepcopy(inp)

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9
    inp["POISSON"]["rtol"] = 1e-4

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-10
    inp["CONT_ELEC"]["delta_max"] = 0.1
    inp["CONT_ELEC2"] = {}
    inp["CONT_ELEC2"]["atol"] = 1e-10
    inp["CONT_ELEC2"]["delta_max"] = 0.01

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-10

    return inp
