"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""


def hetero_junction(
    version,
    length,
    material_left,
    material_right,
    p_doping=False,
    thermionic_emission=False,
):
    """Generate a 1D structure of length length with two different materials and constant doping,
    forming a heterojunction.

    Parameters
    ----------
    version : float64
        hdev version identifier.
    length : float64
        Length in m of the structure.
    material_left : dict
        A hdev material definition for the material on the left half of the structure.
    material_right : dict
        A hdev material definition for the material on the right half of the structure.
    p_doping : float64
        If given, the doping is acceptor doping with density specified by this parameter.
    thermionic_emission : Bool, False
        If given, a thermionic emission boundary condition is used at the heterojunction interface.

    Returns
    -------
    inp : dict
        inp dict that can be used to create a hdev input file.
    """
    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    x_low = 0
    x_high = length

    if thermionic_emission:
        inp["THERMIONIC_EMISSION"] = {}
        inp["THERMIONIC_EMISSION"]["x"] = x_high / 2

    try:
        mod_name_left = material_left["SEMI"]["mod_name"]
        mod_name_right = material_right["SEMI"]["mod_name"]
    except TypeError:
        mod_name_left = material_left["SEMI"][0]["mod_name"]
        mod_name_right = material_right["SEMI"][0]["mod_name"]

    material = dict()
    for key in material_left.keys():
        left_val = material_left[key]
        right_val = material_right[key]
        if isinstance(left_val, dict) and isinstance(right_val, dict):
            material[key] = [left_val, right_val]
        elif isinstance(left_val, list) and isinstance(right_val, list):
            material[key] = left_val + right_val
        else:
            raise IOError("Error occured.")

    inp.update(material)

    dop = 1e23
    if p_doping:
        dop = -dop

    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_low,
            "upp_xyz": x_high / 2,
            "d_con": dop,
        }
    )
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": x_high / 2 + 1e-9,
            "upp_xyz": x_high,
            "d_con": dop,
        }
    )

    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": x_low,
            "upp_xyz": x_high / 2,
            "mod_name": mod_name_left,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": x_high / 2,
            "upp_xyz": x_high,
            "mod_name": mod_name_right,
        }
    )
    if p_doping:
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": x_low,
                "upp_xyz": x_low,
                "mod_name": "CONT",
                "cont_name": "C",
            }
        )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": x_high,
                "upp_xyz": x_high,
                "mod_name": "CONT",
                "cont_name": "A",
            }
        )
    else:
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": x_low,
                "upp_xyz": x_low,
                "mod_name": "CONT",
                "cont_name": "A",
            }
        )
        inp["REGION_DEF"].append(
            {
                "reg_mat": "CONT",
                "low_xyz": x_high,
                "upp_xyz": x_high,
                "mod_name": "CONT",
                "cont_name": "C",
            }
        )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    inp["RANGE_GRID"] = []

    inp["RANGE_GRID"].append(
        {
            "disc_dir": "x",
            "disc_set": "pnts",
            "intv_pnts": [x_low, x_high],
            "n_pnts": 201,
        }
    )

    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 500
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 1

    # inp['CONT_HOLE']={}
    # inp['CONT_HOLE']['atol']           = 1e-1
    # inp['CONT_HOLE']['rtol']           = 1e-3
    # inp['CONT_HOLE']['ctol']           = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "junctiion_" + mod_name_left + "_" + mod_name_right + "_"
    inp["OUTPUT"]["path"] = version

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.1

    return inp
