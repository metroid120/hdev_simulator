"""
This file is part of hdevpy.

hdevpy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

hdevpy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with hdevpy.  If not, see <https://www.gnu.org/licenses/>.
"""

from hdevpy.tools import set_hdev_material_def, turn_off_recombination


def n4_hbt(
    version,
    fermi=False,
    normalization=False,
    bohm=False,
    sat=True,
    ge_max=None,
    spring=False,
    tn=False,
    red=False,
    extended_C=True,
    **kwargs,
):
    """Creates the hdev input definition for the node 4 SiGe HBT of the ITRS roadmap.

    Input
    -----
    version : str
        Name of the hdev version.
    fermi : Bool, False
        If true, use Fermi statistics, else Boltzmann.
    normalization : Bool, False
        If true, normalize the equation system.
    bohm : Bool, False
        If true, use the bohm potential.
    sat : Bool, True
        Activate the saturation velocity model.
    ge_max : float64, None
        If given, the peak Ge grading is set to this value.
    spring : Boolean, False
        If true, the simulation grid is chosen minimal so the result is suitable for SPRING BTE simulations.
    tn : Boolean, False
        If true, do a hydrodynamic simulation, else drift-diffusion.
    red : Boolean, False
        If true, the simulation grid is selected quite coarse.

    Output
    ------
    inp : dict
        A dictionary that can be used to create a hdev input file.
    """

    mat_name = "SiGe"
    if spring:
        mat_name = "SSGS"

    inp = {}
    inp["DOPING"] = []
    inp["GRADING"] = []
    inp["REGION_DEF"] = []

    x_c = 0.054692125106e-6,
    if extended_C:
        x_c = 220e-9

    # profile parameters
    wE_const = 5.749146e-9
    wE_drop = 12.877e-9
    nE_const = 2.3e20 * 1e6

    #  &CONST_DOP d_con=-2.3e+20 xyz_low=0 0 xyz_upp=0.005749146 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0,
            "upp_xyz": wE_const,
            "d_con": nE_const,
        }
    )
    #  &REXP_DOP d_max=-2.3e+20 xyz_0=0.005749146 0 a_xyz=0.003951255 0
    #            beta_xyz=2.8 xyz_low=0.005749146 0 xyz_upp=0.018626412 1
    #            r_max=0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": wE_const + wE_drop,
            "low_xyz": wE_const,
            "xyz_0": wE_const,
            "d_con": nE_const,
            "a_xyz": 0.003951255e-6,
            "b_xyz": 2.8,
        }
    )
    #  &REXP_DOP d_max=1e+18 xyz_0=0.012983628 0 a_xyz=0.001241823 0
    #            beta_xyz=2 xyz_low=0.011175474 0 xyz_upp=0.012983628 1
    #            r_max=0 r_low=0 r_upp=1e+100
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.012983628e-6,
            "low_xyz": 0.011175474e-6,
            "xyz_0": 0.012983628e-6,
            "d_con": -1e18 * 1e6,
            "a_xyz": 0.001241823e-6,
            "b_xyz": 2,
        }
    )
    #  &CONST_DOP d_con=1e+18 xyz_low=0.012983628 0 xyz_upp=0.017384589 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.012983628e-6,
            "upp_xyz": 0.017384589e-6,
            "d_con": -1e18 * 1e6,
        }
    )
    #  &REXP_DOP d_max=1.9436494007e+20 xyz_0=0.019718955 0 a_xyz=0.0014134502987 0
    #            beta_xyz=2.5 xyz_low=0 0 xyz_upp=0.054692125106 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.054692125106e-6,
            "low_xyz": 0,
            "xyz_0": 0.019718955e-6,
            "d_con": -1.9436494007e20 * 1e6,
            "a_xyz": 0.0014134502987e-6,
            "b_xyz": 2.5,
        }
    )
    #  &CONST_DOP d_con=-9.9961205827e+15 xyz_low=0.019836513 0 xyz_upp=0.040629016106 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.019836513e-6,
            "upp_xyz": 0.040629016106e-6,
            "d_con": 9.9961205827e15 * 1e6,
        }
    )
    #  &REXP_DOP d_max=-7.86454e+19 xyz_0=0.040629016106 0 a_xyz=1.2129e-05 0
    #            beta_xyz=0.4 xyz_low=0.026318662106 0 xyz_upp=0.040629016106 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.040629016106e-6,
            "low_xyz": 0.026318662106e-6,
            "xyz_0": 0.040629016106e-6,
            "d_con": 7.86454e19 * 1e6,
            "a_xyz": 1.2129e-11,
            "b_xyz": 0.4,
        }
    )
    #  &REXP_DOP d_max=-6.8e+20 xyz_0=0.054692125106 0 a_xyz=0.006518871 0
    #            beta_xyz=1 xyz_low=0.040629016106 0 xyz_upp=0.046380028106 1
    #            r_max=0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "exp",
            "upp_xyz": 0.046380028106e-6,
            "low_xyz": 0.040629016106e-6,
            "xyz_0": 0.054692125106e-6,
            "d_con": 6.8e20 * 1e6,
            "a_xyz": 0.006518871e-6,
            "b_xyz": 1,
        }
    )
    #  &CONST_DOP d_con=-1.9e+20 xyz_low=0.046380028106 0 xyz_upp=0.054692125106 1
    #            xyz_0=0 0 r_low=0 r_upp=1e+100 /
    inp["DOPING"].append(
        {
            "profile": "const",
            "low_xyz": 0.046380028106e-6,
            # "upp_xyz": 0.054692125106e-6,
            "upp_xyz": x_c,
            "d_con": 1.9e20 * 1e6,
        }
    )
    #  &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.021746364 -1e-10 xyz_upp=0.025585659 1
    #             xyz_dlow=0 0.0 xyz_dupp=0.001163451 0.0 xyz_rlow=0 0.0 xyz_rupp=0.00025 0.0 /
    #  &COMP_TRAP mat_type='GE' c_max=0.3 xyz_low=0.008691828 -1e-10 xyz_upp=0.021746364 1
    #             xyz_dlow=0.013053603 0.0 xyz_dupp=0 0.0 xyz_rlow=0 0.0 xyz_rupp=0 0.0 /
    inp["GRADING"].append(
        {
            "profile": "trap",
            "c_max": 0.3 if ge_max is None else ge_max,
            #"c_max": 0.3,
            "low_xyz": 8.691828e-9,
            "upp_xyz": 21.746364e-9,
            # "upp_xyz": 25.746364e-9,
            "xyz_dlow": 13.053603e-9,
            "xyz_dupp": 0e-6,
            "xyz_rlow": 0.00e-6,
            "xyz_rupp": 0.00e-6,
        }
    )
    inp["GRADING"].append(
        {
            "profile": "trap",
            "c_max": 0.3 if ge_max is None else ge_max,
            # "c_max": 0.3,
            "low_xyz": 0.021746364e-6,
            "upp_xyz": 0.025585659e-6,
            "xyz_dlow": 0,
            "xyz_dupp": 0.001163451e-6,
            "xyz_rlow": 0.000e-6,
            "xyz_rupp": 0.00025e-6,
        }
    )


    inp["REGION_DEF"].append(
        {
            "reg_mat": "SEMI",
            "low_xyz": 0,
            #"upp_xyz": 0.054692125106e-6,
            "upp_xyz": x_c,
            "mod_name": mat_name,
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            "low_xyz": 0,
            "upp_xyz": 0,
            "mod_name": "CONT",
            "cont_name": "E",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "CONT",
            # "low_xyz": 0.054692125106e-6,
            # "upp_xyz": 0.054692125106e-6,
            "low_xyz": x_c,
            "upp_xyz": x_c,
            "mod_name": "CONT",
            "cont_name": "C",
        }
    )
    inp["REGION_DEF"].append(
        {
            "reg_mat": "SUPPLY",
            "low_xyz": 0.020895e-6,
            "upp_xyz": 0.021428e-6,
            "mod_name": "BASE",
            "cont_name": "B",
        }
    )

    inp["REGION_INFO"] = {}
    inp["REGION_INFO"]["spat_dim"] = 1

    if not red:
        inp["RANGE_GRID"] = [
                {
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [0, 0.020895e-6],
                    "n_pnts": 55,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [0.020895e-6, 0.021428e-6],
                    "n_pnts": 3,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    #"intv_pnts": [0.021428e-6, 0.054692125106e-6],
                    "intv_pnts": [0.021428e-6, x_c],
                    "n_pnts": 101,
                }
        ]
    else:
        inp["RANGE_GRID"] = [
                {
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [0, 5e-9],
                    "n_pnts": 6,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [5e-9, 0.020895e-6],
                    "n_pnts": 35,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [0.020895e-6, 0.021428e-6],
                    "n_pnts": 3,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [0.021428e-6, 50e-9],
                    "n_pnts": 21,
                },{
                    "disc_dir": "x",
                    "disc_set": "pnts",
                    "intv_pnts": [50e-9, x_c],
                    "n_pnts": 16,
                }
        ]


    inp["CONTACT"] = []
    inp["CONTACT"].append(
        {
            "mod_name": "CONT",
            "con_type": "ohmic",
        }
    )

    inp["SUPPLY"] = []
    inp["SUPPLY"].append(
        {
            "mod_name": "BASE",
            "supply_type": "p",
        }
    )

    inp["DD"] = {}
    inp["DD"]["n_iter"] = 50
    inp["DD"]["elec"] = 1
    inp["DD"]["hole"] = 1
    inp["DD"]["simul"] = 10
    inp["DD"]["damp_method"] = "none"
    inp["DD"]["damp"] = 0.3
    inp["DD"]["debug"] = 0
    if bohm:
        inp["DD"]["bohm_n"] = 1
        inp["DD"]["simul"] = 1  # war 30... wieso
    if tn:
        inp["DD"]["tn"] = 1
        # inp['DD']['simul']    = 0

    inp["OUTPUT"] = {}
    inp["OUTPUT"]["name"] = "sige"
    inp["OUTPUT"]["path"] = version
    inp["OUTPUT"]["inqu_lev"] = 2

    inp["BIAS_DEF"] = {}
    inp["BIAS_DEF"]["zero_bias_first"] = 1
    inp["BIAS_DEF"]["dv_max"] = 0.01
    if fermi:
        inp["BIAS_DEF"]["dv_max"] = 0.01

    inp = set_hdev_material_def(inp, mat_name)

    inp = turn_off_recombination(inp)

    for i in range(len(inp["MOB_DEF"])):
        if not sat or inp["MOB_DEF"][i]["valley"] == "H":
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"

    for i in range(len(inp["MOB_DEF"])):
        if not sat:
            inp["MOB_DEF"][i]["hc_scat_type"] = "default"
        else:
            inp["MOB_DEF"][i]["v_sat"] = inp["MOB_DEF"][i]["v_sat"] * 1

    inp["POISSON"] = {}
    inp["POISSON"]["init_psi"] = "buildin"
    inp["POISSON"]["atol"] = 1e-9

    inp["CONT_ELEC"] = {}
    inp["CONT_ELEC"]["atol"] = 1e-11
    inp["CONT_ELEC"]["delta_max"] = 1e-3

    inp["CONT_HOLE"] = {}
    inp["CONT_HOLE"]["atol"] = 1e-9

    if fermi:
        for semi in inp["SEMI"]:
            semi["fermi"] = 1

    if normalization:
        try:
            if float(version) > 1.12:
                inp["NORMALIZATION"] = {}
                inp["NORMALIZATION"]["x_norm"] = 1
                inp["NORMALIZATION"]["dens_norm"] = "ni"
        except ValueError:
            pass

    return inp
