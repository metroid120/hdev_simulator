from DMT.core import Plot

import sys

from hdevpy.testing import *

import os
import shutil
from DMT.config import COMMAND_TEX

# if True: print results for TeX documentation
create_doc = False

# set this path to the main hdev_simulator directory:
hdev_directory = os.path.join(".")

# relative paths to main hdev_simulator directory (need not be set)
examples_dir = os.path.join(hdev_directory, "tests")
examples_dir = os.path.abspath(examples_dir)
fig_dir = os.path.join(hdev_directory, "documentation", "manual", "fig")
fig_dir = os.path.abspath(fig_dir)
main_tex_dir = os.path.join(hdev_directory, "documentation", "manual", "hdev.tex")

results = {}
test_cases = []

# resistors
test_cases.append(TestCaseResistor("InP", 1e20, 1e-6))
test_cases.append(TestCaseResistor("GaAs", 1e20, 1e-6))
test_cases.append(TestCaseResistor("InAs", 1e20, 1e-6))
test_cases.append(TestCaseResistor("Si", 1e20, 1e-6, ydir=False))
test_cases.append(TestCaseResistor("Si", 1e20, 1e-6, ydir=True))
test_cases.append(TestCaseUResistor("Si", 1e20))  # 2d resistor slow plot
test_cases.append(TestCaseUResistor("Si", 1e20, extrude=True))
test_cases.append(TestCase2DPlateCap("Si", 1e20))
test_cases.append(TestCase2DPlateCap("Si", 1e20, turn=True))
test_cases.append(TestCaseTriode("Si", 1e20))


# homo junctions with different doping on each side
test_cases.append(TestCaseHomoJunction("InP", 1e20, -3e20, 10e-6))
test_cases.append(TestCaseHomoJunction("Si", 1e20, -3e20, 10e-6, normalization=True))

# hetero junctions with different material on each side, 1e20 constant doping
test_cases.append(TestCaseHeteroJunction("InP", "GaAs", 50e-6))
test_cases.append(TestCaseHeteroJunction("InP", "GaAs", 50e-6, p_doping=True))
test_cases.append(TestCaseHeteroJunction("InP", "InAs", 500e-6))
test_cases.append(TestCaseHeteroJunction("GaAs", "InAs", 50e-6))
test_cases.append(
    TestCaseHeteroJunction("GaAs", "InAs", 50e-6, thermionic_emission=True)
)

# graded hetero junction, with 0 grading left and right and 50% in the middle
test_cases.append(TestCaseGradedHeteroJunction("InGaAs", 50e-6))
test_cases.append(TestCaseGradedHeteroJunction("SiGe", 50e-6))

# diffusion bjts
test_cases.append(TestCaseBjt("Si", 200e-9, 800e-9, 3000e-9, 1e26, -1e22, 1e20))

# hbts
test_cases.append(TestCaseHBT("SiGe", fermi=False))
test_cases.append(TestCaseN3HBT("SiGe", nl=False, et=False, bohm=False))
test_cases.append(TestCaseN3HBT("Si", nl=False, et=True, bohm=False))
test_cases.append(TestCaseN3HBT("Si", nl=False, et=True, bohm=False, fermi=True))
test_cases.append(TestCaseN3HBT("Si", nl=True, et=False, bohm=False))

# Bohm potential work in progress
# test_cases.append(TestCaseN3HBT("Si", nl=False, et=False, bohm=True))
# test_cases.append(TestCaseHBTBohm("Si", fermi=False, normalization=False))
# test_cases.append(TestCaseNPNNPBohm("Si", hdev_old, hdev_new))

# tunnel work in progress
# test_cases.append(TestCaseTunnel("Si", hdev_old, hdev_new, fermi=False))
# test_cases.append(
#     TestCaseN3HBT("Si", hdev_old, hdev_new, nl=False, et=False, bohm=False, tunnel=True)
# )

###PRINT TO DOCUMENT FROM HERE#####################
from pylatex import (
    Section,
    Subsection,
    Figure,
    NoEscape,
)
from pylatex.base_classes import Arguments
from DMT.external import SubFile, CommandInput, CommandLabel

for test_case in test_cases:
    doc = SubFile()
    print("running test " + str(test_case.name))
    inp_file, result, time = test_case.test()
    print("inp_file:")
    print(str(inp_file))
    if not create_doc:
        continue

    # create dir
    os.makedirs(
        os.path.dirname(
            os.path.join(examples_dir, test_case.name, test_case.name + ".inp")
        ),
        exist_ok=True,
    )
    # move files
    shutil.move(
        inp_file, os.path.join(examples_dir, test_case.name, test_case.name + ".inp")
    )
    bias_file = "/".join(os.path.abspath(inp_file).split("/")[:-1]) + "/bias.h5"
    shutil.move(bias_file, os.path.join(examples_dir, test_case.name, "bias.h5"))
    try:
        time = time.replace("s", "")
    except:
        time = 0
    print("finished test " + str(test_case.name))

    with doc.create(Section(test_case.name.replace("_", " "))):
        with doc.create(Subsection("Test Description")):
            doc.append(NoEscape(test_case.description))
            file_name = r" The test input and bias file can be found in the folder \textbf{{tests/{0:s}}}.".format(
                test_case.name.replace("_", "\_")
            )
            doc.append(NoEscape(file_name))
        # with doc.create(Subsection("Test Result")):
        #     doc.append(
        #         NoEscape(r"The simulation time was {0:2.2f}s. ".format(float(time)))
        #     )
        #     doc.append(
        #         NoEscape(
        #             r"The test case results are summarized in the following table:\\"
        #         )
        #     )
        #     doc.append("\n")
        #     doc.append(NoEscape(r"\centering "))
        #     table = result.to_latex(index=False, float_format="{:2.2e}".format)
        #     doc.append(NoEscape(table))

    with doc.create(Subsection("Test Plots")):
        for plot in test_case.get_plots():
            try:  # normal DMT plot
                # save the plot
                plot.legend_location = "upper right outer"
                fig_name = plot.save_tikz(fig_dir, width=".3\\textwidth")
                fig_captions = plot.name

                doc.append(NoEscape(r"\FloatBarrier "))
                with doc.create(Figure(position="h!")) as _plot:
                    _plot.append(NoEscape(r"\centering "))
                    rel_dir = os.path.relpath(fig_dir, main_tex_dir)
                    _plot.append(
                        CommandInput(arguments=Arguments(os.path.join("fig", fig_name)))
                    )
                    _plot.add_caption(
                        fig_name[:-4].replace("_", " ").replace("dot", ".")
                    )
                    _plot.append(CommandLabel(arguments=Arguments(fig_name)))
                doc.append(NoEscape(r"\FloatBarrier "))
            except AttributeError:  # matplotlib plot or pandas Dataframe?

                doc.append(NoEscape(r"\FloatBarrier "))
                with doc.create(Figure(position="h!")) as _plot:
                    _plot.append(NoEscape(r"\centering "))
                    _plot.append(
                        NoEscape(
                            r" \includegraphics[width=0.75\textwidth]{"
                            + "/".join(plot[0].split("/")[-2:])
                            + r"} "
                        )
                    )
                    _plot.add_caption(plot[1])
                doc.append(NoEscape(r"\FloatBarrier "))

    subfile_dir = os.path.join(
        hdev_directory,
        "documentation",
        "manual",
        "test_results",
        test_case.name + ".tex",
    )
    with open(subfile_dir, "w", encoding="utf-8") as subf:
        doc.dump(subf)

if create_doc:
    # read in main documentation file
    with open(main_tex_dir, "r", encoding="utf-8") as documentation:
        main_doc_file = documentation.read()

    # place subfiles in main hdev.tex manual
    subfiles_tex = r"%_LIB"
    for test_case in test_cases:
        subfile_tex = r"\\subfile{test_results/" + test_case.name + r".tex}"
        subfiles_tex = subfiles_tex + "\n " + subfile_tex

    subfiles_tex = subfiles_tex + "\n" + r"%_LIB" + "\n"

    import re

    pattern = r"%_LIB\n(.*)\n%_LIB"
    main_doc_file = re.sub(pattern, subfiles_tex, main_doc_file, flags=re.DOTALL)

    # write main documentation file hdev.tex back
    with open(main_tex_dir, "w", encoding="utf-8") as documentation:
        documentation.write(main_doc_file)
