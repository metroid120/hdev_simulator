# this file demonstrates the Drift-Diffusion 1D simulation of a very simple resistor ,
# including how to define a device structure in hdev. Together with the hdev manual, you can learn how
# hdev input files work.
from DMT.core import (
    DutType,
    specifiers,
    Sweep,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
    set_hdev_material_def,
)
import numpy as np

# define a hdev input structure, look into the manual for details how these blocks work
# in Python, we use a dict of dicts instead of Fortran namelist, but the content is exactly the same!
inp = {}
x_low = 0
x_high = 1e-6

# constant doping throughout structure
inp["DOPING"] = {
    "profile": "const",
    "low_xyz": x_low,
    "upp_xyz": x_high,
    "d_con": 1e26,  # unit in hdev always in natural units, so here 1e26 1/m^3
}

# three regions to define: 2 contacts and 1 semiconductor
inp["REGION_DEF"] = []
# semiconductor Si throughout whole structure
inp["REGION_DEF"].append(
    {
        "reg_mat": "SEMI",
        "low_xyz": 0,
        "upp_xyz": x_high,
        "mod_name": "Si",
    }
)
# C contact at x_low
inp["REGION_DEF"].append(
    {
        "reg_mat": "CONT",
        "low_xyz": x_low,
        "upp_xyz": x_low,
        "mod_name": "CONT",
        "cont_name": "C",
    }
)
# A contact at x_high
inp["REGION_DEF"].append(
    {
        "reg_mat": "CONT",
        "low_xyz": x_high,
        "upp_xyz": x_high,
        "mod_name": "CONT",
        "cont_name": "A",
    }
)

inp["REGION_INFO"] = {
    "spat_dim": 1,  # 1D simulation domain
}

inp["RANGE_GRID"] = {
    "disc_dir": "x",
    "disc_set": "pnts",
    "intv_pnts": [x_low, x_high],
    "n_pnts": 1001,  # 1001 equally spaced points from 0 to 1 um
}

inp["CONTACT"] = {
    "mod_name": "CONT",
    "con_type": "ohmic",  # ohmic boundary conditon
}

inp["SOLVE"] = {
    "n_iter": 100,  # maximum 100 iterations per operating point before giving up
    "elec": 1,  # solve electron continutiy equation (DD if HD not activated)
    "hole": 1,  # solve hole continutiy equation (DD if HD not activated)
}

inp["OUTPUT"] = {
    "path": "hdev_v1p21",
    "name": "resistor_1d",
}

inp["BIAS_DEF"] = {
    "zero_bias_first": 1,  # start at zero volt at all contacts
    "dv_max": 0.1,  # maximum 0.1V step between operating points
}

# use the hdevpy function set_hdev_material to easily set all material properties of Silicon
inp = set_hdev_material_def(inp, "Si")

# Create a DUT using DMT_core, which is used for simulation control
dut = DutHdev(
    None,
    DutType.res,
    inp,
    reference_node="A",
)

# define a Sweep
sweepdef = [
    {
        "var_name": specifiers.VOLTAGE + "C",
        "sweep_order": 1,
        "sweep_type": "LIN",
        "value_def": [0, 20, 51],
    },
    {
        "var_name": specifiers.VOLTAGE + "A",
        "sweep_order": 2,
        "sweep_type": "CON",
        "value_def": [0],
    },
]
outputdef = []
othervar = {"TEMP": 300}
sweep = Sweep("iv", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=False)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=5)  # inqu data at 5th operating point

# define plots using the get_default_plot function provided by hdevpy
plts = []
plt_iv = Plot(
    "I_C(V_C)",
    x_specifier=specifiers.VOLTAGE + "C",
    y_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
    y_scale=1e3 / (1e6 * 1e6),
)
plts.append(plt_iv)
plt_dens = get_default_plot(
    "dens", style="xtraction_color", legend_location="lower right"
)
plts.append(plt_dens)
plt_band = get_default_plot(
    "band", style="black_linestyle", legend_location="upper right outer"
)
plts.append(plt_band)
plt_dop = get_default_plot("dop", y_limits=(1e16, 1e21))
plts.append(plt_dop)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
plt_iv.add_data_set(df_iv["V_C"], df_iv["I_C"], r"$J_{\mathrm{C}}$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=r"$n$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["P"], label=r"$p$")
plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
plt_band.add_data_set(df_inqu["X"], df_inqu["EC"], label=r"$E_{\mathrm{C}}$")
plt_band.add_data_set(df_inqu["X"], df_inqu["EV"], label=r"$E_{\mathrm{V}}$")
plt_band.add_data_set(df_inqu["X"], -df_inqu["PHI|N"], label=r"$E_{\mathrm{Fn}}$")
plt_band.add_data_set(df_inqu["X"], -df_inqu["PHI|P"], label=r"$E_{\mathrm{Fp}}$")

save_or_show(
    plts=plts,
    show=True,
)
