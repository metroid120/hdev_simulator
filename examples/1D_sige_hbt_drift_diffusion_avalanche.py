# this file demonstrates the 1D Drift Diffusion simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
# also the avalanche effect is analyzed.

# As can be seen, in the DD framework, the avalanche effect is starting at very low Vce as the assumption of a direct energy
# relaxation is very bad for highly scaled HBTs
from DMT.core import (
    DutType,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import n3_hbt
import numpy as np

# define a hdev input structure, look into the "n3_hbt" function for details
# hint: have a look into the n3_hbt function to understand how a 1D HBT is defined in detail!
inp = n3_hbt(
    "1.20", reco=True, tn=True
)  # tn=True => Energy Transport Simulation, tn=False => Drift-Diffusion simulation

# if you use drift-diffusion transport, the non local model can be used to more realistically predict the driving force:
# inp["NON_LOCAL"] = {
#     "type": "simple",
#     "lambda": 10e-9,
#     "xj_l": 20e-9,
# }

# Create a DUT using DMT_core, which is used for simulation control
dut = DutHdev(
    None,
    DutType.npn,
    inp,
    reference_node="E",
)

# define an output sweep using the hdevpy function get_sweep
sweep = get_sweep(vce=[0, 9, 201], vbe=[0.65], ac=False)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=False)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=10)

# # define plots using the get_default_plot function provided by hdevpy
plts = []
plt_out_ic = get_default_plot(
    "out",
    x_limits=(0, None),
    style="mix",
    legend_location="upper left",
)
plts.append(plt_out_ic)
plt_out_ib = Plot(
    "I_B(V_CE)",
    x_label=r"$V_{\mathrm{CE}}\left(\si{\volt}\right)$",
    y_label=r"$J_{\mathrm{B}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
    y_scale=1e3 / (1e6 * 1e6),
    style="mix",
    legend_location="upper left",
    y_log=True,
)
plts.append(plt_out_ib)
plt_band = get_default_plot("band", style="mix", legend_location="upper right outer")
plts.append(plt_band)
plt_reco = Plot(
    "R(x)",
    x_label=r"$x\left(\si{\nano\meter}\right)$",
    y_label=r"$R\left(\si{\per\cubic\centi\meter\per\s}\right)$",
    y_scale=1 / (1e6),
    x_scale=1e9,
    style="mix",
    legend_location="upper left",
)
plts.append(plt_reco)
plt_dens = get_default_plot(
    "dens", style="xtraction_color", legend_location="lower right"
)
plts.append(plt_dens)
plt_dop = get_default_plot("dop", y_limits=(1e16, 1e21))
plts.append(plt_dop)
plt_grading = get_default_plot("grading", y_limits=(0, 40))
plts.append(plt_grading)
plt_profile = Plot2YAxis(
    "profile_",
    plt_dop,
    plt_grading,
    legend_location="upper right outer",
)
plts.append(plt_profile)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
plt_out_ic.add_data_set(df_iv["V_C"] - df_iv["V_E"], df_iv["I_C"], r"$J_{\mathrm{C}}$")
plt_out_ic.add_data_set(
    df_iv["V_C"] - df_iv["V_E"], -df_iv["I_E"], r"$-J_{\mathrm{E}}$"
)
plt_out_ib.add_data_set(
    df_iv["V_C"] - df_iv["V_E"], np.abs(df_iv["I_B"]), r"$J_{\mathrm{B}}$"
)
plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=r"$n$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["P"], label=r"$p$")
plt_reco.add_data_set(df_inqu["X"], df_inqu["rec"])
plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")

plt_band.add_data_set(
    df_inqu["X"], df_inqu["EC"], label=r"$E_{\mathrm{C}}$", style="-k"
)
plt_band.add_data_set(
    df_inqu["X"], df_inqu["EV"], label=r"$E_{\mathrm{V}}$", style="--r"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|N"], label=r"$E_{\mathrm{Fn}}$", style="--bx"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|P"], label=r"$E_{\mathrm{Fp}}$", style="-.m"
)

save_or_show(
    plts=plts,
    show=True,
)

# use this to generate the plots for the JOSS paper in pdf format:
# import os

# save_or_show(
#     plts=plts,
#     show=False,
#     location=os.path.join("documentation/paper/figures"),
#     width=r"0.4\textwidth",
#     height=r"0.3\textwidth",
#     fontsize="LARGE",
#     mark_repeat=5,
# )
