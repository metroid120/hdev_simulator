# This script runs a 2D simulation of a quasi 1D N3 HBT, but in 2D mode. 
# The script is based on examples/1D_sige_hbt_hydrodynamic_simulation.py.
from DMT.core import (
    Plot,
    DutType,
    SimCon,
    Plot2YAxis,
    save_or_show,
    specifiers, 
    sub_specifiers
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import n3_hbt
import numpy as np
import copy

force = True

# define a hdev input structure, look into the "n3_hbt" function for details
# hint: have a look into the n3_hbt function to understand how a 1D HBT is defined in detail!
inp_1D = n3_hbt("1.20", tn=True, fermi=True)

# we extend the 1D input to a quasi 1D input with 2 dimensions
wE = 4e-9
inp_2D = copy.deepcopy(inp_1D)
inp_2D["REGION_INFO"]["spat_dim"] = 2
inp_2D["GRADING"] = [inp_2D["GRADING"]]
for inp_sec in ["DOPING", "REGION_DEF", "GRADING"]:
    for i, _sec in enumerate(inp_2D[inp_sec]):
        inp_2D[inp_sec][i]["low_xyz"] = [inp_2D[inp_sec][i]["low_xyz"], 0]
        inp_2D[inp_sec][i]["upp_xyz"] = [inp_2D[inp_sec][i]["upp_xyz"], wE / 2]

for i, _reg in enumerate(inp_2D["REGION_DEF"]):
    if inp_2D["REGION_DEF"][i]["reg_mat"] == "SUPPLY":
        low_x = inp_2D["REGION_DEF"][i]["low_xyz"][0]
        upp_x = inp_2D["REGION_DEF"][i]["upp_xyz"][0]
        inp_2D["REGION_DEF"][i]["low_xyz"] = [low_x, 0]
        # inp["REGION_DEF"][i]["low_xyz"] = [low_x, 0]
        inp_2D["REGION_DEF"][i]["upp_xyz"] = [upp_x, wE / 2]
        break

# create y-discretization in internal transistor region
inp_2D["RANGE_GRID"].append(
    {
        "disc_dir": "y",
        "disc_set": "pnts",
        "intv_pnts": [0, wE / 2],
        "n_pnts": 9,
    }
)

# Create a DUT using DMT_core, which is used for simulation control
dut_1D = DutHdev(
    None,
    DutType.npn,
    inp_1D,
    reference_node="E",
)
dut_2D = DutHdev(
    None,
    DutType.npn,
    inp_2D,
    reference_node="E",
)

# define two sweeps, one gummel sweep and one equilibrium sweep using the hdevpy function get_sweep
sweep = get_sweep(vbe=[0, 1.1, 111], vbc=[0], freq=1e9)
omega = 2*np.pi*10e9

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut_1D, sweep)
simcon.run_and_read(force=force)
simcon.append_simulation(dut_2D, sweep)
simcon.run_and_read(force=force)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data "df_iv", one with internal quantities "df_inqu"
df_iv, df_inqu = get_hdev_dfs(dut_1D, sweep, index=1)
df_iv_2D, df_inqu_2D = get_hdev_dfs(dut_2D, sweep, index=1)

col_rey21 = specifiers.SS_PARA_Y + "C" + "B" + sub_specifiers.REAL
col_imy11 = specifiers.SS_PARA_Y + "B" + "B" + sub_specifiers.IMAG
df_iv.ensure_specifier_column(col_rey21, ports=["B", "C"])
df_iv_2D.ensure_specifier_column(col_rey21, ports=["B", "C"])
df_iv.ensure_specifier_column(col_imy11, ports=["B", "C"])
df_iv_2D.ensure_specifier_column(col_imy11, ports=["B", "C"])

# find peak ft from IV data
jc = df_iv["I_C"].to_numpy() * 1e3 / 1e12  # convert units to mA/um^2 (hdev)
index_jc_1 = np.argmin(np.abs(jc - 1))  # take only Jc>1mA/um^2
ft = df_iv["F_T"].to_numpy()[index_jc_1:]
index_peak = np.argmax(ft) + index_jc_1

jc_2D = df_iv_2D["I_C"].to_numpy()/(wE/2) * 1e3 / 1e12 # convert units to mA/um^2 (hdev)
ft_2D = df_iv_2D["F_T"].to_numpy()[index_jc_1:]

# define plots using the get_default_plot function provided by hdevpy
plts = []
plt_gum = get_default_plot(
    "gum",
    x_limits=(0.6, 1.1),
    y_limits=(1e-6, None),
    style="mix",
    legend_location="upper left",
)
plts.append(plt_gum)
plt_cbe = get_default_plot(
    "cbe",
    style="mix",
    legend_location="upper left",
)
plts.append(plt_cbe)
plt_cbc = get_default_plot(
    "cbc",
    style="mix",
    legend_location="upper left",
)
plts.append(plt_cbc)
plt_ft = get_default_plot("ft", x_limits=(1e-4, 1e2), y_limits=(0, 800))
plts.append(plt_ft)
plt_dens = get_default_plot(
    "dens", style="xtraction_color", legend_location="lower right"
)
plts.append(plt_dens)
plt_band = get_default_plot("band", style="mix", legend_location="upper right outer")
plts.append(plt_band)
plt_dop = get_default_plot("dop", y_limits=(1e16, 1e21))
plts.append(plt_dop)
plt_grading = get_default_plot("grading", y_limits=(0, 40))
plts.append(plt_grading)
plt_profile = Plot2YAxis(
    "profile_",
    plt_dop,
    plt_grading,
    legend_location="upper right outer",
)
plts.append(plt_profile)
plt_rey21 = Plot(
    "G_M(J_C)",
    y_label=r"$\bar{g}_{\mathrm{m}}V_{\mathrm{T}}/J_{\mathrm{C}}$",
    x_label=r"$V_{\mathrm{BE}}\left(\si{\volt}\right)$",
    x_scale=1,
    y_scale=1,
)
plt_rey21.y_limits = (0, 1)
plts.append(plt_rey21)
plt_imy11 = Plot(
    "Y11(V_B)",
    y_label=r"$ImY11$",
    x_label=r"$V_{\mathrm{BE}}\left(\si{\volt}\right)$",
    x_scale=1,
    y_scale=1e15/1e12,
    x_log=False,
)
plts.append(plt_imy11)

# add data to plots, the different columns of the DataFrames are discussed in the Hdev manual
plt_gum.add_data_set(df_iv["V_B"], df_iv["I_C"], label=r"$J_{\mathrm{C}}$", style="-k")
plt_gum.add_data_set(df_iv["V_B"], df_iv["I_B"], label=r"$J_{\mathrm{B}}$", style="-r")
plt_gum.add_data_set(df_iv_2D["V_B"], df_iv_2D["I_C"]/(wE/2), label=r"2D $J_{\mathrm{C}}$", style="x k")
plt_gum.add_data_set(df_iv_2D["V_B"], df_iv_2D["I_B"]/(wE/2), label=r"2D $J_{\mathrm{B}}$", style="s r")

plt_rey21.add_data_set(df_iv["V_B"], df_iv[col_rey21]*25e-3/df_iv["I_C"], style="-k")
plt_rey21.add_data_set(df_iv_2D["V_B"], df_iv_2D[col_rey21]*25e-3/df_iv_2D["I_C"], label=r"2D $J_{\mathrm{C}}$", style="x k")

plt_imy11.add_data_set(df_iv["V_B"], df_iv[col_imy11]/omega, style="-k")
plt_imy11.add_data_set(df_iv_2D["V_B"], df_iv_2D[col_imy11]/(wE/2)/omega, label=r"2D $J_{\mathrm{C}}$", style="x k")

plt_ft.add_data_set(df_iv["I_C"], df_iv["F_T"], style="k-")
plt_ft.add_data_set(df_iv_2D["I_C"]/(wE/2), df_iv_2D["F_T"], style="rx ", label="2D")

plt_cbc.add_data_set(df_iv["V_BE"], df_iv["C_BC"], style="k-")
plt_cbc.add_data_set(df_iv_2D["V_BE"], df_iv_2D["C_BC"]/(wE/2), style="rx ", label="2D")

plt_cbe.add_data_set(df_iv["V_BE"], df_iv["C_BE"], style="k-")
plt_cbe.add_data_set(df_iv_2D["V_BE"], df_iv_2D["C_BE"]/(wE/2), style="rx ", label="2D")


plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=r"$n$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["P"], label=r"$p$")

plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")

plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")

plt_band.add_data_set(
    df_inqu["X"], df_inqu["EC"], label=r"$E_{\mathrm{C}}$", style="-k"
)
plt_band.add_data_set(
    df_inqu["X"], df_inqu["EV"], label=r"$E_{\mathrm{V}}$", style="--r"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|N"], label=r"$E_{\mathrm{Fn}}$", style="--bx"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|P"], label=r"$E_{\mathrm{Fp}}$", style="-.m"
)

save_or_show(
    plts=plts,
    show=True,
)

