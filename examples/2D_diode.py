# this file demonstrates the 1D Drift Diffusion simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
# also the avalanche effect is analyzed.

# As can be seen, in the DD framework, the avalanche effect is starting at very low Vce as the assumption of a direct energy
# relaxation is very bad for highly scaled HBTs
from DMT.core import (
    specifiers,
    sub_specifiers,
    Sweep,
    DutType,
    SimCon,
    Plot,
    save_or_show,
)
from DMT.Hdev import DutHdev
import copy
from hdevpy.tools import (
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import sige_diode
import numpy as np

force = False

# Create a 1D diode
inp = sige_diode(
    "hdev",
    fermi=True,
    sat=True,
    tn=True,
)

# we extend the 1D input to a quasi 1D input with 2 dimensions
wE = 10e-9
inp_2D = copy.deepcopy(inp)
inp_2D["REGION_INFO"]["spat_dim"] = 2
for inp_sec in ["DOPING", "REGION_DEF"]:
    for i, _sec in enumerate(inp_2D[inp_sec]):
        inp_2D[inp_sec][i]["low_xyz"] = [inp_2D[inp_sec][i]["low_xyz"], 0]
        inp_2D[inp_sec][i]["upp_xyz"] = [inp_2D[inp_sec][i]["upp_xyz"], wE]

inp_2D["RANGE_GRID"] = [inp_2D["RANGE_GRID"]]
inp_2D["RANGE_GRID"].append(
    {
        "disc_dir": "y",
        "disc_set": "pnts",
        "intv_pnts": [0, wE],
        "n_pnts": 6,
    }
)

dut1d = DutHdev(
    None,
    DutType.pn_diode,
    inp,
    reference_node="A",
)

dut = DutHdev(
    None,
    DutType.pn_diode,
    inp_2D,
    reference_node="A",
)

# a DUT turned 90 degrees:
inp_turn = copy.deepcopy(inp_2D)
for reg_type in ["REGION_DEF", "DOPING"]:
    for i, reg in enumerate(inp_turn[reg_type]):
        low_xyz = reg["low_xyz"]
        upp_xyz = reg["upp_xyz"]
        inp_turn[reg_type][i]["low_xyz"] = [low_xyz[1], low_xyz[0]]
        inp_turn[reg_type][i]["upp_xyz"] = [upp_xyz[1], upp_xyz[0]]
        if "profile" in inp_turn[reg_type][i].keys():
            if (
                inp_turn[reg_type][i]["profile"] == "exp"
                or inp_turn[reg_type][i]["profile"] == "erfc"
            ):
                for const_set in ["xyz_0", "a_xyz", "b_xyz"]:
                    values = inp_turn["DOPING"][i][const_set]
                    inp_turn["DOPING"][i][const_set] = [values[1], values[0]]

for i, reg in enumerate(inp_turn["RANGE_GRID"]):
    if reg["disc_dir"] == "x":
        inp_turn["RANGE_GRID"][i]["disc_dir"] = "y"
    else:
        inp_turn["RANGE_GRID"][i]["disc_dir"] = "x"

dut_turn = DutHdev(
    None,
    DutType.pn_diode,
    inp_turn,
    reference_node="A",
)

# define an array of sweeps
sweepdef = [
    {
        "var_name": specifiers.FREQUENCY,
        "sweep_order": 3,
        "sweep_type": "CON",
        "value_def": [1e9],
    },
    {
        "var_name": specifiers.VOLTAGE + "A",
        "sweep_order": 2,
        "sweep_type": "LIN",
        "value_def": [0, 1.3, 131],
    },
    {
        "var_name": specifiers.VOLTAGE + "C",
        "sweep_order": 1,
        "sweep_type": "CON",
        "value_def": [0],
    },
]
outputdef = []
othervar = {"TEMP": 300}
sweep = Sweep("iv", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.append_simulation(dut1d, sweep)
simcon.append_simulation(dut_turn, sweep)
simcon.run_and_read(force=force)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"
col_imy11 = specifiers.SS_PARA_Y + "B" + "B" + sub_specifiers.IMAG

# # define plots using the get_default_plot function provided by hdevpy
plts = []
plt_gum = get_default_plot(
    "gum",
    style="comparison_3",
    legend_location="upper left",
)
plts.append(plt_gum)
plt_imy11 = Plot(
    "Y11(V_B)",
    y_label=r"$ImY11 fF/um1$",
    x_label=r"$V_{\mathrm{BE}}\left(\si{\volt}\right)$",
    x_scale=1,
    y_scale=1e15/1e12,
    x_log=False,
)
plts.append(plt_imy11)
plt_imy11_1d_2 = Plot(
    "YCC(V_B)1D2D",
    y_label=r"$ImY11 fF/um$",
    x_label=r"$V_{\mathrm{BE}}\left(\si{\volt}\right)$",
    x_scale=1,
    y_scale=1e15/1e12,
    x_log=False,
)
plts.append(plt_imy11_1d_2)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=40)
df_iv1D, _df_inqu = get_hdev_dfs(dut1d, sweep, index=40)
df_iv_turn, df_inqu = get_hdev_dfs(dut_turn, sweep, index=40)

omega = 2*np.pi*1e9
plt_gum.add_data_set(df_iv["V_A"] - df_iv["V_C"], df_iv["I_A"]/wE, label="2D")
plt_gum.add_data_set(df_iv_turn["V_A"] - df_iv_turn["V_C"], df_iv_turn["I_A"]/wE, label="2D turned") #A/m
plt_gum.add_data_set(df_iv_turn["V_A"] - df_iv_turn["V_C"], df_iv1D["I_A"], label="1D") #A/m^2

plt_imy11.add_data_set(df_iv["V_A"] - df_iv["V_C"], np.imag(df_iv["Y_CC"])/omega/wE, style="-k", label="Y CC")
plt_imy11.add_data_set(df_iv["V_A"] - df_iv["V_C"], np.imag(df_iv["Y_AA"])/omega/wE, style="-r", label="Y AA")
plt_imy11.add_data_set(df_iv_turn["V_A"] - df_iv_turn["V_C"], np.imag(df_iv_turn["Y_CC"])/omega/wE, style="s k", label="Y CC turn")
plt_imy11.add_data_set(df_iv_turn["V_A"] - df_iv_turn["V_C"], np.imag(df_iv_turn["Y_AA"])/omega/wE, style="x r", label="Y AA turn")

plt_imy11_1d_2.add_data_set(df_iv["V_A"] - df_iv["V_C"], np.imag(df_iv["Y_CC"])/omega/wE, style=" ko", label="2D")
plt_imy11_1d_2.add_data_set(df_iv["V_A"] - df_iv["V_C"], np.imag(df_iv1D["Y_CC"])/omega, style="--r", label="1D")

save_or_show(
    plts=plts,
    show=True,
)

