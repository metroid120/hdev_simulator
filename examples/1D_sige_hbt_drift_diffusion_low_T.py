# this file demonstrates the 1D Drift Diffusion simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
# also the avalanche effect is analyzed.

# As can be seen, in the DD framework, the avalanche effect is starting at very low Vce as the assumption of a direct energy
# relaxation is very bad for highly scaled HBTs
from DMT.core import (
    DutType,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import n3_hbt
import numpy as np

force = True

# define a hdev input structure, look into the "n3_hbt" function for details
# hint: have a look into the n3_hbt function to understand how a 1D HBT is defined in detail!
inp = n3_hbt(
    "1.20", reco=True, tn=False, fermi=False, sat=True
)  # tn=True => Energy Transport Simulation, tn=False => Drift-Diffusion simulation

# for Chenyang Yu thesis, modify mobility model parameters of Si:
# Electrons:
inp["MOB_DEF"][2] = {
    # general information
    "mod_name": "Si",
    "valley": "X",
    # model equation selection
    "l_scat_type": "michaillatn",
    "li_scat_type": "cryo",
    "hc_scat_type": "sat",
    "v_sat_type": "default",
    "bowing": "",
    # parameter specification
    "mu_l": 1417*1e-4,
    "gamma_L": -2.285,
    "mu_max": 1417*1e-4,
    "mu_min": 52.2*1e-4,
    "c_ref": 9.68e16*1e6,
    "meff": 1.258, # m_eff_hole/m0
    "f_e": 1.0,
    "f_h": 1.0,
    "f_CW": 2.459,
    "f_bh": 3.828,
    "alpha": 0.68,
    "ag": 4.41804,
    "bg": 39.9014,
    "cg": 0.52896,
    "gammag": 0.25948,
    "betag": 0.38297,
    "alphag": 0.0001,
    "alphag2": 1.595187,

    # parameters for velocity-field effect
    "v_sat": 1e5,
    "a": 0.26,
    "force": "phi",
    "beta": 2,
}
# Holes: (TODO)
inp["MOB_DEF"][3] = {
    # general information
    "mod_name": "Si",
    "valley": "H",
    # model equation selection
    "l_scat_type": "michaillatn",
    "li_scat_type": "cryo",
    "hc_scat_type": "sat",
    "v_sat_type": "default",
    "bowing": "",
    # parameter specification
    "mu_l": 1417*1e-4,
    "gamma_L": -2.285,
    "mu_max": 1417*1e-4,
    "mu_min": 52.2*1e-4,
    "c_ref": 9.68e16*1e6,
    "meff": 1.258, # m_eff_hole/m0
    "f_e": 1.0,
    "f_h": 1.0,
    "f_CW": 2.459,
    "f_bh": 3.828,
    "alpha": 0.68,
    "ag": 4.41804,
    "bg": 39.9014,
    "cg": 0.52896,
    "gammag": 0.25948,
    "betag": 0.38297,
    "alphag": 0.0001,
    "alphag2": 1.595187,

    # parameters for velocity-field effect
    "v_sat": 1e5,
    "a": 0.26,
    "force": "phi",
    "beta": 2,
}

# if you use drift-diffusion transport, the non local model can be used to more realistically predict the driving force:
# inp["NORMALIZATION"] = {
#     "x_norm": 1,
#     "dens_norm": "p",
# }
inp["DD"]["simul"] = 5
inp["BIAS_DEF"]["dv_max"] = 0.01
inp["CONT_HOLE"]["delta_max"] = 0.01

# Create a DUT using DMT_core, which is used for simulation control
dut = DutHdev(
    None,
    DutType.npn,
    inp,
    reference_node="E",
)

# define an array of sweeps
sweeps = []
# for temp in [30,80,100,150,200,250,300]:
temps = [6, 10, 100, 200, 300]
temps = [100, 200, 300]
temps = [300]
for temp in temps:
    vbe_max = 1.1
    vbe_min = 0
    if temp <= 100:
        vbe_max = 1.1
        vbe_min = 0.8
    elif temp <= 30:
        vbe_max = 1.15
        vbe_min = 0.9
    elif temp <= 10:
        vbe_max = 1.05
        vbe_min = 0.8
    sweep = get_sweep(
        vbe=[vbe_min, vbe_max, int(vbe_max / 0.01) + 1], vbc=[0], ac=False, temp=temp
    )
    sweeps.append(sweep)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
for sweep in sweeps:
    simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=force)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"

# # define plots using the get_default_plot function provided by hdevpy
plts = []
plt_gum = get_default_plot(
    "gum",
    x_limits=(0, None),
    style="xtraction_color",
    legend_location="upper left",
)
plts.append(plt_gum)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
for sweep, temp in zip(sweeps, temps):
    try:
        df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=10)
        plt_gum.add_data_set(
            df_iv["V_B"] - df_iv["V_E"],
            df_iv["I_C"],
            label=r"$\SI{" + str(temp) + r"}{\kelvin}$",
        )
        plt_gum.add_data_set(df_iv["V_B"] - df_iv["V_E"], df_iv["I_B"])
    except:
        pass

save_or_show(
    plts=plts,
    show=True,
)
