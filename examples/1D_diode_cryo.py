# this file demonstrates the 1D Drift Diffusion simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
# also the avalanche effect is analyzed.

# As can be seen, in the DD framework, the avalanche effect is starting at very low Vce as the assumption of a direct energy
# relaxation is very bad for highly scaled HBTs
from DMT.core import (
    specifiers,
    Sweep,
    DutType,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import sige_diode
import numpy as np

force = True

# Create a DUT using DMT_core, which is used for simulation control
inp = sige_diode(
    "hdev",
    fermi=True,
    sat=True,
    tn=False,
)

dut = DutHdev(
    None,
    DutType.pn_diode,
    inp,
    reference_node="A",
)

# define an array of sweeps
sweeps = []
temps = [4,10,30,80,100,150,200,250,300]
# for temp in temps:
for temp in [4]:
    vmin=0
    vmax=1.2
    if temp in [4,10]:
        vmin=1
        vmax=1.2
    sweepdef = [
        {
            "var_name": specifiers.VOLTAGE + "A",
            "sweep_order": 2,
            "sweep_type": "CON",
            "value_def": [0],
        },
        {
            "var_name": specifiers.VOLTAGE + "C",
            "sweep_order": 1,
            "sweep_type": "LIN",
            "value_def": [-vmin, -vmax, 121],
        },
    ]
    outputdef = []
    othervar = {"TEMP": temp}
    sweep = Sweep("gummel", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)
    sweeps.append(sweep)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
for sweep in sweeps:
    simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=force)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"

# # define plots using the get_default_plot function provided by hdevpy
plts = []
plt_gum = get_default_plot(
    "gum",
    style="mix",
    legend_location="upper left",
)
plts.append(plt_gum)
plt_dens = get_default_plot(
    "dens",
    style="xtraction_color",
    legend_location="upper left",
)
plts.append(plt_dens)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
for sweep,temp in zip(sweeps,temps):
    try:
        df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=40)
    except:
        continue
    plt_gum.add_data_set(df_iv["V_A"] - df_iv["V_C"], df_iv["I_A"], label=str(temp))
    plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=str(temp))
    plt_dens.add_data_set(df_inqu["X"], df_inqu["P"])

save_or_show(
    plts=plts,
    show=True,
)

