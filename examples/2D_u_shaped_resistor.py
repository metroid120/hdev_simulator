# this file demonstrates the Hydrodynamic 1D simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
from DMT.core import (
    DutType,
    specifiers,
    Sweep,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
    set_hdev_material_def,
)
import numpy as np

# define a hdev input structure, look into the manual for details how these blocks work
# in Python, we use a dict of dicts instead of Fortran namelist, but the content is exactly the same!
inp = {}

# a 2D structure
inp["REGION_INFO"] = {
    "spat_dim": 2,
}


# we define a u shaped resistor with oxide to the left using boxes on a 1umx1um square
inp["REGION_DEF"] = [
    {
        "reg_mat": "SEMI",
        "low_xyz": [0, 0],
        "upp_xyz": [1e-6, 1e-6],
        "mod_name": "Si",
    },
    {
        "reg_mat": "CONT",
        "low_xyz": [0, 0],
        "upp_xyz": [0.1e-6, 0.25e-6],
        "mod_name": "CONT",
        "cont_name": "C",
    },
    {
        "reg_mat": "CONT",
        "low_xyz": [0, 0.75e-6],
        "upp_xyz": [0.1e-6, 1e-6],
        "mod_name": "CONT",
        "cont_name": "A",
    },
    {
        "reg_mat": "OXID",
        "low_xyz": [0, 0.25e-6],
        "upp_xyz": [0.5e-6, 0.75e-6],
        "mod_name": "SiO2",
    },
]


# constant doping in the structure
inp["DOPING"] = {
    "profile": "const",
    "low_xyz": [0, 0],
    "upp_xyz": [1e-6, 1e-6],
    "d_con": 1e26,
}


# discretization in x and y direction
inp["RANGE_GRID"] = [
    {
        "disc_dir": "y",
        "disc_set": "pnts",
        "intv_pnts": [0, 1e-6],
        "n_pnts": 101,
    },
    {
        "disc_dir": "x",
        "disc_set": "pnts",
        "intv_pnts": [0, 1e-6],
        "n_pnts": 101,
    },
]

# ohmic contact model
inp["CONTACT"] = {
    "mod_name": "CONT",
    "con_type": "ohmic",
}


# define equations to be solved
inp["SOLVE"] = {
    "n_iter": 20,  # 20 iterations per operating point
    "elec": 1,  # solve electron continuity equation
    "hole": 0,  # ignore holes (n doped structure => saves computation time)
}


# define information for storing data
inp["OUTPUT"] = {
    "name": "resistor_u",
    "path": "1p21",
    "cap_lev": 1,  # calculate isolation capacitances between contacts
}


# we define some solution critera for Poisson equation
inp["POISSON"] = {
    "atol": 1e-5,
    "rtol": 1e-3,
    "ctol": 0,
}


# bias information
inp["BIAS_DEF"] = {
    "zero_bias_first": 1,  # run an equilibrium simulation prior to the user defined operating point list
    "dv_max": 0.1,  # maximum 0.1V between operating points
}

# use the hdevpy function set_hdev_material to easily set all material properties of Silicon
inp = set_hdev_material_def(inp, "Si")

# Create a DUT using DMT_core, which is used for simulation control
dut = DutHdev(
    None,
    DutType.res,
    inp,
    reference_node="A",
)

# define a Sweep
sweepdef = [
    {
        "var_name": specifiers.VOLTAGE + "C",
        "sweep_order": 1,
        "sweep_type": "LIN",
        "value_def": [0, 1, 11],
    },
    {
        "var_name": specifiers.VOLTAGE + "A",
        "sweep_order": 2,
        "sweep_type": "CON",
        "value_def": [0],
    },
]
outputdef = []
othervar = {"TEMP": 300}
sweep = Sweep("iv", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=False)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=5)  # inqu data at 5th operating point

# define plots using the get_default_plot function provided by hdevpy
plts = []
plt_iv = Plot(
    "I_C(V_C)",
    x_specifier=specifiers.VOLTAGE + "C",
    y_label=r"$J_{\mathrm{C}}\left(\si{\milli\ampere\per\micro\meter}\right)$",
    y_scale=1e3 / (1e6),
)
plts.append(plt_iv)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
plt_iv.add_data_set(df_iv["V_C"], df_iv["I_C"], r"$J_{\mathrm{C}}$")

# some 2D plots with matplotlib
import matplotlib.pyplot as mplt
import tikzplotlib

mplt.rc("text", usetex=True)

x = np.unique(df_inqu["X"].to_numpy())
y = np.unique(df_inqu["Y"].to_numpy())
jx = df_inqu["J|XDIR"].to_numpy()
jy = df_inqu["J|YDIR"].to_numpy()
psi = df_inqu["psi_semi"].to_numpy()
jx = jx.reshape(len(x), len(y))
jy = jy.reshape(len(x), len(y))
psi = psi.reshape(len(x), len(y))
j = np.sqrt(jx ** 2 + jy ** 2)
ey, ex = np.gradient(psi, x, y)


fig = mplt.figure(num="potential contour and current streamlines")
ax = fig.add_subplot(111)
cs = ax.contourf(x * 1e6, y * 1e6, psi, 100)
ax.streamplot(
    x * 1e6,
    y * 1e6,
    jx,
    jy,
    linewidth=1,
    cmap=mplt.cm.inferno,
    density=2,
    arrowstyle="->",
    arrowsize=1.5,
)
ax.set_xlabel(r"$x ( \mu m )$")
ax.set_ylabel(r"$y ( \mu m )$")
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_aspect("equal")
cbar = fig.colorbar(cs)
cbar.ax.set_ylabel(r"$\Psi ( \mathrm{V} )$")
mplt.show(block=False)

fig = mplt.figure(num="electric field streamlines")
ax = fig.add_subplot(111)
ax.streamplot(
    x * 1e6,
    y * 1e6,
    ex,
    ey,
    linewidth=1,
    density=2,
    arrowstyle="->",
    arrowsize=1.5,
)
ax.set_xlabel(r"$x ( \mu m )$")
ax.set_ylabel(r"$y ( \mu m )$")
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
ax.set_aspect("equal")
mplt.show(block=False)

save_or_show(
    plts=plts,
    show=True,
)
