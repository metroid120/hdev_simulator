# this file demonstrates the Hydrodynamic 1D simulation of the Node 3 HBT from the ITRS roadmap using a device definition provided by hdevpy
from DMT.core import (
    DutType,
    SimCon,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import n3_hbt
import numpy as np

# define a hdev input structure, look into the "n3_hbt" function for details
# hint: have a look into the n3_hbt function to understand how a 1D HBT is defined in detail!
inp = n3_hbt("1.20", tn=True, fermi=True)

# Create a DUT using DMT_core, which is used for simulation control
dut = DutHdev(
    None,
    DutType.npn,
    inp,
    reference_node="E",
)

# define two sweeps, one gummel sweep and one equilibrium sweep using the hdevpy function get_sweep
sweep = get_sweep(vbe=[0, 1.1, 111], vbc=[-0.6], freq=10e9)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=True)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data "df_iv", one with internal quantities "df_inqu"
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=1)

# find peak ft from IV data
jc = df_iv["I_C"].to_numpy() * 1e3 / 1e12  # convert units to mA/um^2 (hdev)
index_jc_1 = np.argmin(np.abs(jc - 1))  # take only Jc>1mA/um^2
ft = df_iv["F_T"].to_numpy()[index_jc_1:]
index_peak = np.argmax(ft) + index_jc_1

# define plots using the get_default_plot function provided by hdevpy
plts = []
plt_gum = get_default_plot(
    "gum",
    x_limits=(0.6, 1.1),
    y_limits=(1e-6, None),
    style="mix",
    legend_location="upper left",
)
plts.append(plt_gum)
plt_ft = get_default_plot("ft", x_limits=(1e-2, 1e2), y_limits=(0, 1600))
plts.append(plt_ft)
plt_dens = get_default_plot(
    "dens", style="xtraction_color", legend_location="lower right"
)
plts.append(plt_dens)
plt_band = get_default_plot("band", style="mix", legend_location="upper right outer")
plts.append(plt_band)
plt_dop = get_default_plot("dop", y_limits=(1e16, 1e21))
plts.append(plt_dop)
plt_grading = get_default_plot("grading", y_limits=(0, 40))
plts.append(plt_grading)
plt_profile = Plot2YAxis(
    "profile_",
    plt_dop,
    plt_grading,
    legend_location="upper right outer",
)
plts.append(plt_profile)

# add data to plots, the different columns of the DataFrames are discussed in the Hdev manual
plt_gum.add_data_set(df_iv["V_B"], df_iv["I_C"], r"$J_{\mathrm{C}}$")
plt_gum.add_data_set(df_iv["V_B"], df_iv["I_B"], r"$J_{\mathrm{B}}$")

plt_ft.add_data_set(df_iv["I_C"], df_iv["F_T"])

plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=r"$n$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["P"], label=r"$p$")

plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")

plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")

plt_band.add_data_set(
    df_inqu["X"], df_inqu["EC"], label=r"$E_{\mathrm{C}}$", style="-k"
)
plt_band.add_data_set(
    df_inqu["X"], df_inqu["EV"], label=r"$E_{\mathrm{V}}$", style="--r"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|N"], label=r"$E_{\mathrm{Fn}}$", style="--bx"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|P"], label=r"$E_{\mathrm{Fp}}$", style="-.m"
)

save_or_show(
    plts=plts,
    show=True,
)

# use this to generate the plots for the JOSS paper in pdf format:
# import os

# save_or_show(
#     plts=plts,
#     show=False,
#     location=os.path.join("documentation/paper/figures"),
#     width=r"0.4\textwidth",
#     height=r"0.3\textwidth",
#     fontsize="LARGE",
#     mark_repeat=5,
# )
