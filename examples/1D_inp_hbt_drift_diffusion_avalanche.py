# this file demonstrates the 1D Drift Diffusion simulation of an InP HBT according to
# Z. Griffith et al., "Sub-300 nm InGaAs/InP Type-I DHBTs with a 150 nm collector, 30 nm base demonstrating 755 GHz
# fmax and 416 GHz fT," 2007 IEEE 19th International Conference on Indium Phosphide & Related Materials, 2007,
# pp. 403-406, doi: 10.1109/ICIPRM.2007.381209.
#
# The avalanche effect is analyzed.

# As can be seen, in the DD framework, the avalanche effect is starting at very low Vce as the assumption of a direct energy
# relaxation is very bad for highly scaled HBTs
from DMT.core import (
    DutType,
    SimCon,
    Plot,
    Plot2YAxis,
    save_or_show,
)
from DMT.Hdev import DutHdev
from hdevpy.tools import (
    get_sweep,
    get_hdev_dfs,
    get_default_plot,
)
from hdevpy.devices import getInPHBT
import numpy as np

# define a DutHdev object, look into the "getInPHBT" function for details
dut = getInPHBT(
    fermi=True,  # calculate with Fermi Dirac statistics
    nl=False,  # nonlocal force model from paper
    sat=True,  # velocity saturation
    elec2=False,  # transport in L band
    iv=False,  # intervalley transfer
    te=True,  # thermionic boundary condition activated
    tn=False,  # energy transport model not activated
    vary={"c_spike": True},  # If True: turn off spike of n doping in Collector
)

# define an output sweep using the hdevpy function get_sweep
sweep = get_sweep(vce=[0, 10, 201], vbe=[0.7], ac=False)

# start simulations
simcon = SimCon(n_core=4, t_max=460)
simcon.append_simulation(dut, sweep)
simcon.run_and_read(force=True)

# retrieve simulation data for sweep "sweep" => one pandas DataFrame with IV data, one with internal quantities "inqu"
df_iv, df_inqu = get_hdev_dfs(dut, sweep, index=100)

# # define plots using the get_default_plot function provided by hdevpy
plts = []
plt_out_ic = get_default_plot(
    "out",
    x_limits=(0, None),
    style="mix",
    legend_location="upper left",
)
plts.append(plt_out_ic)
plt_out_ib = Plot(
    "I_B(V_CE)",
    x_label=r"$V_{\mathrm{CE}}\left(\si{\volt}\right)$",
    y_label=r"$J_{\mathrm{B}}\left(\si{\milli\ampere\per\square\micro\meter}\right)$",
    y_scale=1e3 / (1e6 * 1e6),
    style="mix",
    legend_location="upper left",
    y_log=True,
)
plts.append(plt_out_ib)
plt_band = get_default_plot("band", style="mix", legend_location="upper right outer")
plts.append(plt_band)
plt_reco = Plot(
    "R(x)",
    x_label=r"$x\left(\si{\nano\meter}\right)$",
    y_label=r"$R\left(\si{\per\cubic\centi\meter\per\s}\right)$",
    y_scale=1 / (1e6),
    x_scale=1e9,
    y_log=True,
    style="mix",
    legend_location="upper left",
)
plts.append(plt_reco)
plt_tn = Plot(
    "Tn(x)",
    x_label=r"$x\left(\si{\nano\meter}\right)$",
    y_label=r"$T_{\mathrm{n}}\left(\si{\kelvin}\right)$",
    y_scale=1,
    x_scale=1e9,
    style="mix",
    legend_location="upper left",
)
plts.append(plt_tn)
plt_dens = get_default_plot(
    "dens", style="xtraction_color", legend_location="lower right"
)
plts.append(plt_dens)
plt_dop = get_default_plot("dop", y_limits=(1e16, 1e21))
plts.append(plt_dop)
plt_grading = get_default_plot("grading", y_limits=(0, 70))
plts.append(plt_grading)
plt_profile = Plot2YAxis(
    "profile_",
    plt_dop,
    plt_grading,
    legend_location="upper right outer",
)
plts.append(plt_profile)

# add data to plots, the different columns of the DataFrames are discussed in the hdev manual
plt_out_ic.add_data_set(df_iv["V_C"] - df_iv["V_E"], df_iv["I_C"], r"$J_{\mathrm{C}}$")
plt_out_ic.add_data_set(
    df_iv["V_C"] - df_iv["V_E"], -df_iv["I_E"], r"$-J_{\mathrm{E}}$"
)
plt_out_ib.add_data_set(
    df_iv["V_C"] - df_iv["V_E"], np.abs(df_iv["I_B"]), r"$J_{\mathrm{B}}$"
)
plt_dens.add_data_set(df_inqu["X"], df_inqu["N"], label=r"$n$")
plt_dens.add_data_set(df_inqu["X"], df_inqu["P"], label=r"$p$")
plt_reco.add_data_set(df_inqu["X"], df_inqu["rec"])
try:
    plt_tn.add_data_set(df_inqu["X"], df_inqu["NL"])
except KeyError:
    pass
plt_dop.add_data_set(df_inqu["X"], df_inqu["NNET"], style="-k")
plt_grading.add_data_set(df_inqu["X"], df_inqu["MOL"], style="--k")

plt_band.add_data_set(
    df_inqu["X"], df_inqu["EC"], label=r"$E_{\mathrm{C}}$", style="-k"
)
plt_band.add_data_set(
    df_inqu["X"], df_inqu["EV"], label=r"$E_{\mathrm{V}}$", style="--r"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|N"], label=r"$E_{\mathrm{Fn}}$", style="--bx"
)
plt_band.add_data_set(
    df_inqu["X"], -df_inqu["PHI|P"], label=r"$E_{\mathrm{Fp}}$", style="-.m"
)

# analytical calculations
# gradient qfn and qfp:
# gphin = df_inqu["GPHI|N"].to_numpy()
# gphip = df_inqu["GPHI|P"].to_numpy()
# n = df_inqu["N"].to_numpy()
# p = df_inqu["N"].to_numpy()
# # material parameters
# inp = materials["InP"]

save_or_show(
    plts=plts,
    show=True,
)
