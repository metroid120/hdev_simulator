The Dockerfile in this folder is used to generate a DockerImage suitable for statically building Hdev. 
All dependencies are compiled and the static dependencies are available, allowing to statically link Hdev.
The image is used in the Hdev CI on Gitlab for the build stage. 

To build the image, run: 

    cd dockerfile_build
    docker build -t registry.gitlab.com/metroid120/hdev_simulator .

To upload the image into the Gitlab container registry, run 

    docker push registry.gitlab.com/metroid120/hdev_simulator