!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!this module contains important global variables needed in the DD solver and related modules. hdev developers should know these variables.
module dd_types
implicit none

logical                             :: CRYO    = .FALSE. !activate certain Hdev parts that require modification for low T simulation

!this type stores all parameters of the recombination models -> see manual to understand these parameters
type rec_model
    integer   :: semi_id       = 0  !< index of model's semiconductor
    character(len=64) :: model = '' !< model name

    !langevin
    real(8)           :: beta = 0

    !logicals
    logical :: is_srh              = .false.
    logical :: lifetime_is_default = .false.
    logical :: lifetime_is_device  = .false.
    logical :: is_auger            = .false.
    logical :: is_avalanche        = .false.
    logical :: is_langevin         = .false.
    logical :: is_intervalley      = .false.
    logical :: force_is_phi        = .false.
    logical :: force_is_psi        = .false.
    logical :: force_is_mix        = .false.

    !srh
    character(len=64) :: lifetime     = '' !< model name
    real(8)           :: e_trap       = 0
    real(8)           :: n_t          = 0
    real(8)           :: beta_n       = 0
    real(8)           :: beta_p       = 0
    real(8)           :: c_ref_n      = 0
    real(8)           :: c_ref_p      = 0
    real(8)           :: tau_n_min    = 0
    real(8)           :: tau_p_min    = 0
    real(8)           :: tau_n_max    = 0
    real(8)           :: tau_p_max    = 0
    real(8)           :: tau_n_min_a1 = 0
    real(8)           :: tau_n_min_a2 = 0
    real(8)           :: tau_p_min_a1 = 0
    real(8)           :: tau_p_min_a2 = 0
    real(8)           :: tau_n_max_a1 = 0
    real(8)           :: tau_n_max_a2 = 0
    real(8)           :: tau_p_max_a1 = 0
    real(8)           :: tau_p_max_a2 = 0
    real(8)           :: sigma_n      = 0
    real(8)           :: sigma_p      = 0
    real(8)           :: zeta_n       = 0
    real(8)           :: zeta_p       = 0

    !auger
    real(8)           :: c_n    = 0
    real(8)           :: c_p    = 0
    real(8)           :: c_n_a1 = 0
    real(8)           :: c_n_a2 = 0
    real(8)           :: c_p_a1 = 0
    real(8)           :: c_p_a2 = 0
    !zetas from srh def

    !avalanche
    real(8)           :: alpha_n  = 0
    real(8)           :: alpha_p  = 0
    real(8)           :: e_crit_n = 0
    real(8)           :: e_crit_p = 0

    ! invervalley model -> see Paper:
    ! M. Müller, P. Dollfus and M. Schröter, "1-D Drift-Diffusion Simulation of Two-Valley Semiconductors and Devices," 
    ! in IEEE Transactions on Electron Devices, vol. 68, no. 3, pp. 1221-1227, March 2021, doi: 10.1109/TED.2021.3051552.I
    real(8)                          :: v_21     = 0   
    real(8)                          :: dv_21    = 0  
    logical                          :: v21off   = .FALSE.  
    real(8)                          :: xl       = -1e9
    real(8)                          :: xr       = 1
    real(8),dimension(:),allocatable :: v_12         
    real(8)                          :: a        = 0 
    real(8)                          :: f0       = 0 
    real(8)                          :: f1       = 0 
    real(8)                          :: dop_crit = 0 
    real(8)                          :: zeta_dop = 0 
    real(8)                          :: af       = 0 

endtype

!information stored at every grid point inside semiconductor for every band
!for holes qf=phip_ for elec1 qf=phin_ and for elec2 qf=phin2_
type dd_point_semi
    ! --> from dd_point ---------------------------
    real(8)                      :: dens_dqf   =0        !< dervative density d/dqf
    real(8)                      :: dens_dl    =0        !< dervative density dn/dl (l is bohm potential)
    real(8)                      :: dens_dtl   =0        !< dervative density dn/dT_L (lattice temperature)
    real(8)                      :: recomb     =0        !< recombination rate (only between elec1 and hole band)
    real(8),dimension(3)         :: recomb_dqf =0        !< derivative recombination rate to qf
    real(8),dimension(3)         :: recomb_dtl =0        !< derivative recombination rate to TL
    real(8)                      :: recomb_dtn =0        !< derivative recombination rate to TN
    real(8),dimension(3)         :: recomb_dpsi=0        !< derivative recombination rate to psi
    real(8),dimension(3)         :: recomb_l   =0        !< derivative recombination rate to bohm potential
    real(8)                      :: bohm_integral     =0 !< bohm integral at point (see diss. Mueller)
    real(8),dimension(3)         :: bohm_integral_dqf =0 !< derivative recombination rate to qf
    real(8),dimension(3)         :: bohm_integral_dtl =0 !< derivative recombination rate to tl
    real(8),dimension(3)         :: bohm_integral_dpsi=0 !< derivative recombination rate to psi
    real(8),dimension(3)         :: bohm_integral_dl  =0 !< derivative recombination rate to l
    ! --------------------------------------------- 
    real(8)                      :: intervalley     =0   !< intervalley recombination rate
    real(8),dimension(3)         :: intervalley_dpsi=0   !< derivative to psi
    real(8),dimension(3)         :: intervalley_dqf =0   !< derivative to qf
    real(8),dimension(3)         :: intervalley_dqf2=0   !< derivative to qf2
    real(8)                      :: zeta            = 0  !< relation between intervalley recombination rates -> OBSOLETE probably
    real(8),dimension(3)         :: zeta_dqf        = 0  !< derivative relation between intervalley recombination rates -> OBSOLETE probably
    ! -- current at contacts (only relevant for points at a contact) --
    real(8)                      :: j_con      =0 !< contact density current
    real(8)                      :: j_con_dqf  =0 !< derivate contact current to qf
    real(8)                      :: j_con_dpsi =0 !< derivate contact current to psi
    real(8)                      :: j_con_dtl  =0 !< derivate contact current to tl
    ! -- mobility inside semiconductor --
    real(8),dimension(2)         :: mu_lf    =0
    ! -- current inside semiconductor --
    real(8)                      :: j_xh       =0 !< carrier density x-current at (i+1/2,j,k)
    real(8)                      :: j_xhdg     =0 !< density gradient current at (i+1/2,j,k), see diss. Mueller 
    real(8),dimension(2)         :: j_xhdg_dqf =0 !< derivative density gradient current to qf
    real(8),dimension(2)         :: j_xhdg_dpsi=0 !< derivative density gradient current to psi
    real(8),dimension(2)         :: j_xhdg_dtl =0 !< derivative density gradient current to tl
    real(8),dimension(2)         :: j_xh_dqf   =0 !< derivative x-current to qf  d(i+1/2,j,k)/d(i,j,k) & d(i+1/2,j,k)/d(i+1,j,k)
    real(8),dimension(2)         :: j_xh_dpsi  =0 !< derivative x-current to psi d(i+1/2,j,k)/d(i,j,k) & d(i+1/2,j,k)/d(i+1,j,k)
    real(8),dimension(2)         :: j_xh_dtl   =0 !< derivative x-current to qf  d(i+1/2,j,k)/d(i,j,k) & d(i+1/2,j,k)/d(i+1,j,k)
    complex(8)                   :: j_xh_ac      =0 !< small signal current density x direction
    real(8)                      :: j_yh       =0 !< carrier density y-current at  (i,j+1/2,k)
    real(8)                      :: j_yhdg     =0 !< density gradient current, see diss. Mueller
    real(8),dimension(2)         :: j_yhdg_dqf =0 !< derivative y-current to qf  d(i,j+1/2,k)/d(i,j,k) & d(i,j+1/2,k)/d(i,j+1,k)
    real(8),dimension(2)         :: j_yhdg_dpsi=0 !< derivative y-current to qf  d(i,j+1/2,k)/d(i,j,k) & d(i,j+1/2,k)/d(i,j+1,k)
    real(8),dimension(2)         :: j_yhdg_dtl =0 !< derivative y-current to qf  d(i,j+1/2,k)/d(i,j,k) & d(i,j+1/2,k)/d(i,j+1,k)
    real(8),dimension(2)         :: j_yh_dqf   =0 !< derivative y-current to qf  d(i,j+1/2,k)/d(i,j,k) & d(i,j+1/2,k)/d(i,j+1,k)
    real(8),dimension(2)         :: j_yh_dpsi  =0 !< derivative y-current to psi d(i,j+1/2,k)/d(i,j,k) & d(i,j+1/2,k)/d(i,j+1,k)
    real(8),dimension(2)         :: j_yh_dtl   =0 !< derivative x-current to qf  d(i+1/2,j,k)/d(i,j,k) & d(i+1/2,j,k)/d(i+1,j,k)
    complex(8)                   :: j_yh_ac      =0 !< small signal current density y direction
    real(8),dimension(2)         :: mob        =0 !< (x,y)-mobility
    real(8)                      :: velo       =0 !< (x,y)-mobility
    ! energy flow density
    real(8)                      :: s_xh       =0 
    real(8),dimension(2)         :: s_xh_dqf   =0 
    real(8),dimension(2)         :: s_xh_dpsi  =0 
    real(8),dimension(2)         :: s_xh_dtl   =0 
    real(8)                      :: s_yh       =0 
    real(8),dimension(2)         :: s_yh_dqf   =0 
    real(8),dimension(2)         :: s_yh_dpsi  =0 
    real(8),dimension(2)         :: s_yh_dtl   =0 
    ! -- tunnel current --
    real(8)                      :: j_x_tun    =0   !< tunnel current in x-direction
    real(8)                      :: g_tun      =0   !< generation rate due to tunneling
    real(8),dimension(:),allocatable :: g_tun_dqf   !< derivative generation rate tunneling to qf
    real(8),dimension(:),allocatable :: g_tun_dpsi  !< derivative generation rate tunneling to psi
endtype dd_point_semi

! ---> properties of tau type ------------------------------------------------------------------------------------
type tau_model
    character(len=64)    :: mod_name      = ''      !< model of semiconductor
    character(len=64)    :: valley        = ''      !< valley of semiconductor
    character(len=64)    :: tau_type      = ''      !< lattice scattering model

    !logicals 
    !=> self explanatory with manual. Logicals are fast in if condiditions, so we convert string user input to logicals
    !=> memory is "free" on a modern computer
    logical :: is_michaillat                = .false.
    logical :: is_default                   = .false.

    !ids
    integer              :: semi_id      = 0        !< index to semiconductor model that the band of the carrier belongs to
    integer              :: band_id      = 0        !< index to the band within the semiconductor that this carrier belongs to

    !SiGe parameters
    real(8)              :: t0           = 0
    real(8)              :: t1           = 0
    real(8)              :: c0           = 0
    !default parameters
    real(8)              :: c1           = 0
    real(8)              :: c2           = 0
    real(8)              :: c3           = 0

    !bowing
    real(8)              :: t0_bow           = 0
    real(8)              :: c0_bow           = 0

endtype

! ---> properties of mobility type ------------------------------------------------------------------------------------
type mob_model
    character(len=64)    :: mod_name      = ''      !< model of semiconductor
    character(len=64)    :: valley        = ''      !< valley of semiconductor
    character(len=64)    :: l_scat_type   = ''      !< lattice scattering model
    character(len=64)    :: li_scat_type  = ''      !< impurity scattering model
    character(len=64)    :: hc_scat_type  = ''      !< hot carrier scattering model
    character(len=64)    :: v_sat_type    = ''      !< saturation veloctiy model
    character(len=64)    :: bowing        = ''      !< alloy model
    character(len=64)    :: force         = ''      !< alloy model

    !logicals 
    !=> self explanatory with manual. Logicals are fast in if condiditions, so we convert string user input to logicals
    !=> memory is "free" on a modern computer
    logical :: l_scat_is_default         = .false.
    logical :: l_scat_is_temp            = .false.
    logical :: l_scat_is_michaillat_n    = .false.
    logical :: l_scat_is_michaillat_p    = .false.
    logical :: li_scat_is_default        = .false.
    logical :: li_scat_is_cryo           = .false.
    logical :: li_scat_is_caughey_thomas = .false.
    logical :: li_scat_is_reggiani       = .false.
    logical :: li_scat_is_device         = .false.
    logical :: li_scat_is_tuwien         = .false.
    logical :: hc_scat_is_default        = .false.
    logical :: hc_scat_is_sat            = .false.
    logical :: hc_scat_is_ndm            = .false.
    logical :: hc_scat_is_ndm2           = .false.
    logical :: v_sat_is_default          = .false.
    logical :: v_sat_is_device           = .false.
    logical :: bowing_is_default         = .false.
    logical :: bowing_is_michaillat      = .false.
    logical :: is_elec                   = .false.
    logical :: force_is_grad_psi         = .false.
    logical :: force_is_grad_phi         = .false.
    logical :: force_is_grad_mix         = .false.
    logical :: force_is_hd               = .false.

    !ids
    integer              :: semi_id      = 0        !< index to semiconductor model that the band of the carrier belongs to
    integer              :: band_id      = 0        !< index to the band within the semiconductor that this carrier belongs to

    real(8)                             :: mu_fac       = 0

    !lattice scattering parameters
    !default
    real(8),dimension(3) :: mu_L         = 0
    !michaillat
    real(8),dimension(3) :: mu_cs        = 0
    real(8),dimension(3) :: z_0          = 0
    real(8),dimension(3) :: beta_l       = 0
    !temp
    real(8)              :: gamma_L      = 0

    !impurity scattering parameters
    !default
    !caughey-thomas
    real(8)              :: mu_min       = 0
    real(8)              :: c_ref        = 0
    real(8)              :: alpha        = 0
    real(8)              :: gamma_1      = 0 
    real(8)              :: gamma_2      = 0  
    real(8)              :: gamma_3      = 0   
    real(8)              :: gamma_4      = 0 
    real(8)              :: gamma_1_a    = 0 
    real(8)              :: gamma_2_a    = 0 
    real(8)              :: gamma_1_d    = 0 
    real(8)              :: gamma_2_d    = 0      
    real(8)              :: gamma_3_a    = 0   
    real(8)              :: gamma_4_a    = 0  
    real(8)              :: gamma_3_d    = 0   
    real(8)              :: gamma_4_d    = 0  

    !reggiani
    real(8)              :: c_ref_a      = 0
    real(8)              :: c_ref_d      = 0
    real(8)              :: mu_min_a     = 0
    real(8)              :: mu_min_d     = 0
    real(8)              :: alpha_a      = 0
    real(8)              :: alpha_d      = 0
    real(8)              :: gamma        = 0  
    real(8)              :: c            = 0  
    !device
    real(8)              :: alpha_2      = 0
    real(8)              :: c_ref_2      = 0
    real(8)              :: mu_hd        = 0     
    real(8)              :: r            = 0          

    !hot carrier scattering parameters
    !default
    !sat
    real(8)              :: beta          = 0          
    !ndm
    real(8)              :: beta1         = 0  
    real(8)              :: beta2         = 0   
    real(8)              :: beta3         = 0   
    real(8)              :: f0            = 0
    real(8)              :: f1            = 0                
    !ndm 22
    real(8)              :: mu_HC         = 0          

    !saturation velocity parameters
    !default
    real(8)              :: v_sat         = 0          
    real(8)              :: A             = 0          
    real(8)              :: c_ref_vsat    = 0          
    real(8)              :: al_vsat       = 0          
    !device
    real(8)              :: zeta_v        = 0          
    !driving force
    real(8)              :: fac_driving_force = 0
    !bowing
    !default
    real(8)              :: mu_bow        = 0
    real(8)              :: v_sat_bow     = 0
    !michaillat
    real(8)              :: mu_bow1       = 0
    real(8)              :: mu_bow2       = 0
    real(8)              :: mu_bowy       = 0
    real(8)              :: mu_bowxy      = 0
    real(8),dimension(4) :: chi           = 0
    real(8)              :: z_1           = 0
    !device
    real(8)              :: v_sat_a1
    real(8)              :: v_sat_a2
    real(8)              :: mu_min_a1
    real(8)              :: mu_min_a2
    real(8)              :: mu_L_a1
    real(8)              :: mu_L_a2

    !parameters for cryo mobility model
    real(8)              :: mu_max       = 0
    real(8)              :: meff         = 0
    real(8)              :: f_e          = 0
    real(8)              :: f_h          = 0
    real(8)              :: f_CW         = 0
    real(8)              :: f_bh         = 0
    real(8)              :: ag           = 0
    real(8)              :: bg           = 0
    real(8)              :: cg           = 0
    real(8)              :: gammag       = 0
    real(8)              :: betag        = 0
    real(8)              :: alphag       = 0
    real(8)              :: alphag2      = 0

endtype

! ---> band structure type -------------------------------------------------------------------------------------------
type :: type_band
    character(len=64)            :: name      = ''            !< name of band
    character(len=64)            :: type      = ''            !< band structure type, e.g. parabolic
    character(len=64)            :: valley    = ''            !< valley identifier, e.g. X,L,G Valley

    !logicals
    logical                      :: type_para = .false.       !< .true.:parabolic band .false.:other band type 
    logical                      :: iscb      = .true.        !< .true.:cond. band    .false.: valence band
    logical                      :: fermi     = .true.        !< .true.:Fermi distr.  .false.: Boltzmann distr.
    logical                      :: is_gamma  = .false.       !< .true.: Gamma valley .false: other valley
    logical                      :: is_x      = .false.       !< .true.: X valley .false: other valley
    logical                      :: is_l      = .false.       !< .true.: L valley .false: other valley
    logical                      :: is_hole   = .false.       !< .true.: Degenerate hole valley .false: other valley

    !band edge and bowing parameter
    real(8)                      :: e0        = 0             !< [eV] energy postition relative to intrinsic fermi level
    real(8)                      :: e0_c      = 0             !< bowing parameter of band edge relative to intrinsic fermi level

    !effective_mass model
    real(8)                          :: me       = 0       !< [kg] eff. mass
    real(8)                          :: me_c     = 0       !< bowing of effective mass
    real(8)                          :: e0_alpha = 0       ! first oder temperature coefficient, see Manual
    real(8)                          :: e0_beta  = 0       ! second oder temperature coefficient, see Manual

    !information on the band structure
    integer                          :: dim = 3             !< dimension, e.g. classic BJT=3
    real(8)                          :: deg = 2             !< band degeneracy factor
    real(8)                          :: al = 0              !< [1/eV] nonparabolic factor
    real(8)                          :: n_eff = 0           !< prefactor for gaussian DOS or parabolic band
    real(8),dimension(:),allocatable :: pos_dep_n_eff       !< position dependent prefactor for DOS => calculated by hdev
    real(8),dimension(:),allocatable :: pos_dep_n_eff_output !< if n_eff becomes bias dependent, e.g. in nonparabolic case, this one is used to store Nceff in bandstructure routine
    real(8),dimension(:),allocatable :: pos_dep_al          !< position dependent non parabolic factor of DOS function
    real(8),dimension(:),allocatable :: pos_dep_m_eff       !< position dependent DOS effective mass

    !mobility paramaters
    type(mob_model) :: mob
    !tau_e paramaters
    type(tau_model) :: tau_parameters

    contains
endtype

type norm_type
    real(8)      :: l_N         = 1   !< spatial norm
    real(8)      :: l_N_inv     = 1   !< inverse spatial norm

    real(8)      :: N_N         = 1   !< density norm
    real(8)      :: N_N_inv     = 1   !< inverse density norm

    real(8)     :: J_N          = 1   !< current density norm
    real(8)     :: J_N_inv      = 1   !< inverse current densit norm

    real(8)     :: R_N          = 1   !< recombination norm
    real(8)     :: R_N_inv      = 1   !< inverse recombination norm

    real(8)     :: t_N          = 1   !< time norm
    real(8)     :: t_N_inv      = 1   !< inverse time norm

    real(8)     :: lambda_N     = 1   !< x norm ???
    real(8)     :: lambda_N_inv = 1   !< inverse x norm ???
endtype norm_type

logical DD_BBT !if true        : band to band tunneling needs to be calculated
logical DD_TUNNEL !if true     : intra band tunneling needs to be calculated
logical DD_HOLE !if true       : hole continutiy equation shall be solved
logical DD_ELEC !if true       : electron continuity equation shall be solved
logical DD_ELEC2 !if true      : second electron 2 continuity equation shall be solved
logical DD_TL !if true         : lattice temperature equation shall be solved
logical DD_TN !if true         : electron temperature equation shall be solved
logical DD_CONT_BOHM_N !if true: differential equation for bohm potential electrons shall be solved along other equations

type(norm_type) :: NORM !store norms

public DD_BBT
public DD_TUNNEL
public DD_HOLE
public DD_ELEC
public DD_ELEC2
public DD_TL
public DD_TN
public DD_CONT_BOHM_N

private

public NORM
public dd_point_semi
public mob_model
public tau_model
public rec_model
public type_band
public CRYO


contains
endmodule