!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! profile variables: doping, grading
module profile

implicit none

real(8),dimension(:),pointer          :: CD          !net doping concentration   ND-NA
real(8),dimension(:),pointer          :: DONATORS
real(8),dimension(:),pointer          :: ACCEPTORS
real(8),dimension(:),pointer          :: CDTOT       !total doping concentration |ND| + |NA|
real(8),dimension(:),pointer          :: GRADING     ! Grading with material A
real(8),dimension(:),pointer          :: GRADING2    ! Grading with material B
real(8),dimension(:),pointer          :: STRAIN      ! Strain due to lattice missmatch

public CD    
public CDTOT 
public GRADING
public STRAIN
public GRADING2
public DONATORS
public ACCEPTORS

contains

end module
