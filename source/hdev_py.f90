!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!hdev modules that may be exported to Python (experimental!)
module hdev_py

USE Dual_Num_Auto_Diff
use bandstructure, only : SEMI,get_n_equ_band_dual,get_fermi_equ                 !< semiconductor properties
use read_user    , only : init_user
use simulation , only   : init_simulation
use math_oper , only    : to_lower
use dd_types , only     : mob_model
use dd_types , only     : rec_model
use profile, only : CDTOT
use recombination, only: get_intervalley_rates
use continuity_elec   , only : CONT_ELEC
use phys_const
use heterostructure, only : bow

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use mobility, only: get_mobility                         !< Physical constants

implicit none
! ---> global module parameters ---------------------------------------------------------------------------------------

INTEGER :: var_test = 1

contains

integer function set(r) 
    integer :: r
    var_test = r
endfunction set

integer function get() 
    integer :: r
    r = var_test
endfunction get

function test2(i) result(r)
    integer,intent(in) :: i
    integer :: r
    r = i+1
endfunction test2

subroutine init(path)
    character(len=1024),intent(in) :: path
    CHARACTER(len=255) :: cwd
    write(*,*) '***note*** changing directory.'
    CALL chdir("~/tmp")
    write(*,*) '***note*** print cwd.'
    CALL getcwd(cwd)
    WRITE(*,*) TRIM(cwd)
    ! --> init user parameters
    write(*,*) '***note*** before init_user.'
    call init_user(path)
    write(*,*) '***note*** after init_user.'

    ! --> initialize simulation parameters
    call init_simulation
    write(*,*) '***note*** after init_simulation.'
endsubroutine

function get_mobility_py(semi_name,valley,f,ec_l,ec_r,dim,t,dens,don,acc,grad) result(mu)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get mobility value for given electric field, direction and temperature for SiGe, function taken from device
    !
    ! Input
    ! ------
    ! semi_name : string
    !   name of semiconductor.
    ! valley : string
    !   name of valley in semiconductor.
    ! f : real
    !   driving force strength.
    ! ec_l : real
    !   Value of conduction band at left point.
    ! ec_r : real
    !   Value of conduction band at right point.
    ! dim : integer
    !   transport direction
    ! t : real
    !   temperature
    ! don : real
    !   donor concentration at point
    ! acc : real
    !   acceptor concentration at point
    ! grad : real
    !   grading at point
    !
    ! Result
    ! ----------
    ! mu : real
    !   mobility
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout) :: semi_name
    character,intent(inout) :: valley
    real(8),intent(in)   :: f
    real(8),intent(in)   :: ec_l
    real(8),intent(in)   :: ec_r
    real(8),intent(in)   :: t
    integer,intent(in)   :: dim
    real(8),intent(in)   :: dens
    real(8),intent(in)   :: don
    real(8),intent(in)   :: acc
    real(8),intent(in)   :: grad

    real(8) :: phi_l   !< quasi fermi level at left side
    real(8) :: phi_r   !< quasi fermi level at right side

    real(8) :: mu      !< [m^2/Vs] mobility
    real(8) :: grad2=0      !< [m^2/Vs] mobility
    real(8) :: stress=0      !< [m^2/Vs] mobility
    real(8) :: dx      !< [V/m] electric field
    integer :: ib = 0, is=0 , i

    dx    = 10e-9
    phi_r = 0
    phi_l = f*dx

    call to_lower(valley)
    call to_lower(semi_name)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:4).eq.semi_name(1:4)) is = i
    enddo

    do i=1,size(SEMI(is)%band_pointer)
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq.valley) ib = i
    enddo

    mu = 0
    if (is.eq.0) then
        write(*,*) '***error*** did not find semi.'
    elseif (ib.eq.0) then
        write(*,*) '***error*** did not find band.'
    else
        mu = get_mobility(is,ib,1  ,phi_l,phi_r,ec_l,ec_r,t   ,t   ,dx,dim,t,dens,don,acc,grad,grad2,stress)
    endif

endfunction get_mobility_py

subroutine get_mobility_paras_py(semi_name,valley,mob)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! return all mobility model parameters
    !
    ! Input
    ! -----
    ! semi_name : string
    !   Name of semiconductor.
    ! valley : string
    !   Name of valley in semiconductor.
    !
    ! Returns 
    ! -------
    ! mob : mob_model
    !   All mobility parameters.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout) :: semi_name 
    character,intent(inout)         :: valley
    integer                         :: is=0, ib=0,i

    type(mob_model),intent(out)     :: mob
    call to_lower(valley)
    call to_lower(semi_name)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:4).eq.semi_name(1:4)) is = i
    enddo

    do i=1,size(SEMI(is)%band_pointer)
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq.valley) ib = i
    enddo

    mob = SEMI(is)%band_pointer(ib)%band%mob

endsubroutine

subroutine get_recombination_paras_py(semi_name,type,reco)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get all recombination model parameters 
    ! 
    ! Input
    ! -----
    ! semi_name : string
    !   name of semiconductor
    ! type : string
    !   recombination type 
    ! 
    ! Returns
    ! -------
    ! reco : rec_model
    !   All parameters of recombination model
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout)    :: semi_name  
    character(len=10),intent(inout)    :: type      
    type(rec_model),intent(out)        :: reco

    integer                            :: k=0, is=0, i=0

    call to_lower(semi_name)
    call to_lower(type)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:2).eq.semi_name(1:2)) is = i
    enddo

    do k=1,size(SEMI(is)%rec_models)
        !take correct model for given semiconductor
        if (type(1:3).eq.'int') then
            if (.not.SEMI(is)%rec_models(k)%is_intervalley) then
                cycle
            endif
            reco = SEMI(is)%rec_models(k)

        else
            write(*,*) '***error*** type not implemented'
            stop

        endif
    enddo

endsubroutine

subroutine set_mobility_paras_py(semi_name,valley,mob_new)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all mobility model parameters
    !
    ! Input
    ! -----
    ! semi_name : string
    !   Name of semiconductor.
    ! valley : string
    !   Name of valley in semiconductor.
    ! mob_new : mob_model
    !   All mobility parameters to set.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout)    :: semi_name      !< index to band
    character,intent(inout)            :: valley      !< index to band
    type(mob_model),intent(in)     :: mob_new

    type(mob_model),pointer     :: mob
    integer                         :: is=0, ib=0,i

    call to_lower(valley)
    call to_lower(semi_name)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:4).eq.semi_name(1:4)) is = i
    enddo

    do i=1,size(SEMI(is)%band_pointer)
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq.valley) ib = i
    enddo

    mob => SEMI(is)%band_pointer(ib)%band%mob

    mob = mob_new

endsubroutine

subroutine set_recombination_paras_py(semi_name,rec_new)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all recombination model parameters
    !
    ! Input
    ! -----
    ! semi_name : string
    !   Name of semiconductor.
    ! mob_new : mob_model
    !   All mobility parameters to set.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout)    :: semi_name      !< index to band
    type(rec_model),intent(in)         :: rec_new

    type(rec_model),pointer         :: rec
    integer                         :: is=0, i, k=0

    call to_lower(semi_name)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:2).eq.semi_name(1:2)) is = i
    enddo

    do k=1,size(SEMI(is)%rec_models)
        !take correct model for given semiconductor
        if (rec_new%is_intervalley) then
            if (.not.SEMI(is)%rec_models(k)%is_intervalley) then
                cycle
            endif
            rec => SEMI(is)%rec_models(k) 

        else
            write(*,*) '***error*** type not implemented'
            stop

        endif
    enddo

    rec = rec_new

endsubroutine

subroutine get_intervalley_rate_py(semi_name,valley_1,valley_2,f,dop,temp,grad,rate)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the intervalley recombiation rate between valley_1 and valley_2
    !
    ! Input
    ! -----
    ! semi_name : string
    !   Name of semiconductor.
    ! valley_1 : char
    !   Identifier of first valley.
    ! valley_2 : char
    !   Identifier of second valley.
    ! f : real
    !   driving force
    ! dop : real
    !   doping density
    ! temp : real
    !   temperature
    !
    ! Returns
    ! -------
    ! rate : real
    !   intervalley recombination rate
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout) :: semi_name
    character(len=1),intent(inout)  :: valley_1
    character(len=1),intent(inout)  :: valley_2
    real(8),intent(in)              :: f
    real(8),intent(in)              :: dop
    real(8),intent(in)              :: grad
    real(8),intent(in)              :: temp

    real(8),intent(out)        :: rate

    TYPE(DUAL_NUM)  :: null   = DUAL_NUM(0,(/1,0,0,0,0,0,0,0/))
    TYPE(DUAL_NUM)  :: result = DUAL_NUM(0,(/0,0,0,0,0,0,0,0/))
    real(8)         :: dx,phi_l,phi_r,cd_tot_old
    TYPE(rec_model),pointer :: rec, rec_A, rec_B
    integer         :: i=0, is=0, k=0
    integer         :: index_valley_1=0, index_valley_2=0, id_materialA, id_materialB


    nullify(rec)

    !f = (phi_l - phi_r) /dx
    dx    = 10e-9
    phi_r = 0
    phi_l = f*dx

    call to_lower(semi_name)
    call to_lower(valley_1)
    call to_lower(valley_2)

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:4).eq.semi_name(1:4)) is = i
    enddo

    do i=1,size(SEMI(is)%band_pointer)
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq.valley_1(1:1)) index_valley_1 = i
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq.valley_2(1:1)) index_valley_2 = i
    enddo

    do k=1,size(SEMI(is)%rec_models)
        !take correct model for given semiconductor
        if (.not.SEMI(is)%rec_models(k)%is_intervalley) then
            cycle
        endif
        rec => SEMI(is)%rec_models(k) 
    enddo

    if (.not.associated(rec)) then
        write (*,*) '***error*** did not find rec model.' 
    endif
    if ((index_valley_1.eq.0).or.(index_valley_2.eq.0)) then
        write (*,*) '***error*** did not find valley index.' 
    endif
    if (is.eq.0) then
        write (*,*) '***error*** did not find semi index.' 
    endif

    !fix doping (temperature todo)
    cd_tot_old = CDTOT(1)
    CDTOT(1)   = abs(dop)

    write(*,*) 'after cdtot'

    ! apply bowing
    if (SEMI(is)%is_alloy) then
        id_materialA     = SEMI(is)%id_materialA
        id_materialB     = SEMI(is)%id_materialB
        rec_A            => SEMI(id_materialA)%rec_models(k)
        rec_B            => SEMI(id_materialB)%rec_models(k)
        ! bowing parameters for alloy not yet implemented => take them as zero
        rec%f0       = bow(rec_A%f0      , rec_B%f0      , dble(0), grad)
        rec%zeta_dop = bow(rec_A%zeta_dop, rec_B%zeta_dop, dble(0), grad)
        rec%dop_crit = bow(rec_A%dop_crit, rec_B%dop_crit, dble(0), grad)
        rec%a        = bow(rec_A%a       , rec_B%a       , dble(0), grad)
        rec%af       = bow(rec_A%af      , rec_B%af      , dble(0), grad)
        rec%v_21     = bow(rec_A%v_21    , rec_B%v_21    , dble(0), grad)
        rec%dv_21    = bow(rec_A%dv_21   , rec_B%dv_21   , dble(0), grad)

    endif
 
    if (index_valley_1.eq.CONT_ELEC%valley) then !GL
        !null to cast to dual number
        !at semi point 1
        result  = get_intervalley_rates(1         ,is ,rec, 1+null, null , null, null,  null,  null, phi_l+null,    null, dx  ,  null,   null,    null, dble(0))
                ! get_intervalley_rates(index_sp  ,is,rec, n     , n2   , psi , phin, phin2, psi_m, phin_m    , phin2_m, dx_m, psi_p, phin_p, phin2_p, dx_p) result(r)
    else
        result  = get_intervalley_rates(1         , is,rec,   null, 1+null, null, null,  null,  null, null     , phi_l+ null  , dx   ,  null ,   null ,    null, dble(0))
                ! get_intervalley_rates(index_sp, is,rec , n     , n2    , psi , phin, phin2, psi_m, phin_m   , phin2_m      , dx_m , psi_p , phin_p , phin2_p, dx_p) result(r)
    endif
    write(*,*) 'after get_intervalley'

    CDTOT(1)  = cd_tot_old

    rate    = abs(result%x_ad_)


endsubroutine

function get_pop_py(semi_name,t,dop) result(pop)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get G Valley population nG/(nG+nL) for given doping value 
    !
    ! Input
    ! ------
    ! semi_name : string
    !   name of semiconductor.
    ! t : real
    !   temperature
    ! dop : real
    !   donor concentration at point
    !
    ! Result
    ! ----------
    ! pop : real
    !   relative G valley population
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=10),intent(inout) :: semi_name
    real(8),intent(in)   :: t
    real(8),intent(in)   :: dop

    real(8) :: ef
    real(8) :: pop,nL,nG
    real(8) :: vt
    real(8) :: grad2=0      !< [m^2/Vs] mobility
    real(8) :: stress=0      !< [m^2/Vs] mobility
    integer :: ib_G = 0, ib_L = 0, is=0 , i

    call to_lower(semi_name)

    vt   = t*BK/Q

    do i=1,size(SEMI)
        if (SEMI(i)%mod_name(1:4).eq.semi_name(1:4)) is = i
    enddo

    do i=1,size(SEMI(is)%band_pointer)
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq."g") ib_G = i
        if (SEMI(is)%band_pointer(i)%band%valley(1:1).eq."l") ib_L = i
    enddo

    pop = 0
    if (is.eq.0) then
        write(*,*) '***error*** did not find semi.'
    elseif ((ib_G.eq.0).or.(ib_L.eq.0)) then
        write(*,*) '***error*** did not find band.'
    else
        ef = get_fermi_equ(is,dop,int(1),dble(0),dble(0),dble(0))
        nG = get_n_equ_band_dual(is,ib_G,ef,vt,int(1))
        nL = get_n_equ_band_dual(is,ib_L,ef,vt,int(1))
        pop = nG/(nG+nL)
    endif

endfunction get_pop_py



endmodule