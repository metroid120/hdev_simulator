!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! module containing DD normalization factors. This module can help when numeric error causing non-convergence
module normalization

use phys_const, only : Q             !< elementary charge
use phys_const, only : BK            !< boltzmann constant
use phys_const, only : EP0           !< permittivity of vacuum
use structure, only : N_SEMI,X,Y,Z,NX1,NZ1,NY1  !< total number of semiconductor models
use structure, only : normalize_structure
use read_user, only : US            !< US input structure
use bandstructure                    !< equired for calulating n_i
use profile      , only : CD,CDTOT,DONATORS,ACCEPTORS,CDTOT                           !< (N_SP) semiconductor doping density
use semiconductor_globals, only : NI_SQ
use semiconductor, only : normalize_semiconductor
use dd_types, only: NORM

implicit none



private

public init_normalization

contains

subroutine init_normalization
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the normalization factors from user input.
    ! see Schroter Script for details 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: min_n,max_n

    if (US%norm%x_norm.gt.0) then 
        !normalize to maximum spatial dimension
        NORM%l_N = max(X(NX1),Y(NY1),Z(NZ1))

    endif
    if (US%norm%dens_norm(1:2).eq.'ni') then !normalize to minimum carrier density
        ! --> normalization after "MOS Device Modeling at 77 K" by Siegfried Selberherr
        min_n    = minval(sqrt(NI_SQ))
        max_n    = maxval(CD)
        ! NORM%N_N = sqrt(min_n*max_n)
        NORM%N_N = 1e5
    endif
    if (US%norm%dens_norm(1:1).eq.'p') then !normalize to minimum carrier density
        max_n    = 1e-5
        NORM%N_N = max_n
    endif

    !dependend normalization variables
    NORM%J_N          = Q*NORM%N_N/NORM%l_N
    NORM%R_N          = NORM%N_N/NORM%l_N/NORM%l_N
    NORM%t_N          = NORM%l_N*NORM%l_N
    NORM%lambda_N     = EP0/Q/NORM%l_N/NORM%l_N/NORM%N_N

    NORM%l_N_inv      = 1/NORM%l_N
    NORM%N_N_inv      = 1/NORM%N_N
    NORM%J_N_inv      = 1/NORM%J_N
    NORM%R_N_inv      = 1/NORM%R_N
    NORM%t_N_inv      = 1/NORM%t_N
    NORM%lambda_N_inv = 1/NORM%lambda_N

    call normalize_structure(NORM%l_N)
    call normalize_semiconductor(NORM%l_N)

    !scaling of rth equation
    US%dd%rth = US%dd%rth * NORM%J_N

    !scaling of doping profile
    CD        = CD/NORM%N_N
    CDTOT     = CDTOT/NORM%N_N
    DONATORS  = DONATORS/NORM%N_N
    ACCEPTORS = ACCEPTORS/NORM%N_N

endsubroutine

endmodule
