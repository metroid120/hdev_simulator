!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



!> all contact related functions and properties
module contact

USE Dual_Num_Auto_Diff

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use phys_const                              !< Physical constants

use math_oper    , only : init_grid         !< init grid vector

use read_user    , only : US                !< all parameters from user input file

use save_global  , only : DEB_ID            !< debug file id
use save_global  , only : FORMAT_REAL       !< output format for real numbers
use save_global  , only : format_int        !< output format for integer numbers
use save_global  , only : write_strunit     !< write parameter

use structure    , only : BOX               !< properties of boxes
use structure    , only : N_BOX             !< number of boxed
use structure    , only : POINT             !< (NX1,NY1,NZ1) parameter for each disc. point
use structure    , only : STRUC_DIM         !< dimension for poisson equation
use structure    , only : BULK              !< if true: bulk simulation
use structure    , only : NX1               !< number of x-points
use structure    , only : NY1               !< number of y-points
use structure    , only : NZ1               !< number of z-points

use bandstructure, only : band_pointer      !< band structure type
use bandstructure, only : SEMI              !< semiconductor properties
use bandstructure, only : BS                !< contact band structure properties
use bandstructure, only : get_fermi_equ     !< get fermi potential for given carrier density

use semiconductor_globals, only : N_SP      !< total number of semiconductor grid points
use semiconductor_globals, only : UN        !< band potential electrons
use semiconductor_globals, only : UN2        !< band potential electrons
use semiconductor_globals, only : UP        !< band potential holes
use semiconductor_globals, only : HETERO    !< true if heterostructure
use semiconductor_globals, only : TEMP_LATTICE   !< lattice temperature
use semiconductor, only : SP                !< semiconductor grid points
use semiconductor, only : SEMI_EXIST        !< if true: semiconductor region exists
use semiconductor, only : SEMI_DIM          !< dimension of semiconductor
use profile, only : CD                      !< doping density
use semiconductor, only : PSI_BUILDIN       !< build in voltage
use semiconductor, only : E0_ADD            !< if true: band gap offset
use semiconductor, only : E0_ADD_SEMI       !< band gap offset

implicit none

! ---> contact parameter type -------------------------------------------------------
type cont_type
    character(len=64)                  :: name            = ''         !< source/drain contact name
    logical                            :: limiting        = .false.    !< if true: injection contact (S/D)
    logical                            :: injection       = .false.    !< if true: injection contact (S/D)
    logical                            :: fermi           = .true.     !< if true fermi distribution for landauer
    real(8),dimension(:),pointer       :: bias            => null()    !< bias points
    real(8)                            :: bias_diff       = 0          !< offset to next bias point
    real(8)                            :: bias_mm         = 0          !< offset due to contact resistance
    real(8)                            :: bias_mm_last    = 0          !< offset due to contact resistance from last operation point
    real(8)                            :: current         = 0          !< current through contact
    complex(8)                         :: ac_current      = 0          !< ac current through contact
    real(8)                            :: efi             = 0          !< energy fermi level contact 
    real(8)                            :: efi_dtl         = 0          !< energy fermi level contact 
    real(8)                            :: v_n             = 0          !< velocity
    real(8)                            :: v_n2            = 0          !< velocity
    real(8)                            :: v_p             = 0          !< velocity
    integer                            :: sp_idx          = 1          !< index to semiconductor
    integer                            :: semi_id         = 1          !< index to semiconductor
    real(8)                            :: sbcb            = 0          !< shottky barrier height electrons
    real(8)                            :: sbvb            = 0          !< shottky barrier height holes
    logical                            :: shky            = .false.    !< if true: Schottky contact
    logical                            :: diri            = .true.     !< if true: dirichlet contact
    logical                            :: neumann         = .false.    !< if true: neumann contact
    logical                            :: setfloat        = .true.     !< if true: set floating band
    character(len=64)                  :: mod_name        = 'default'  !< contact model name
    logical                            :: injcharge       = .true.     !< true: separation between elec/hole
    integer                            :: n_band          = 0          !< number of subbands
    type(band_pointer),dimension(:),allocatable  :: band_pointer       !< band structure parameters
    logical                            :: supply          = .false.    !< if true: Supply contact
    logical                            :: supply_n        =.false.     !< if true: Supply contact for electron continuity 
    logical                            :: supply_p        =.false.     !< if true: Supply contact for hole continuity 
endtype cont_type

! ---> global module parameters ---------------------------------------------------------------------------------------
type(cont_type),dimension(:),allocatable,target :: CON          !< injection contact properties
integer                         :: N_CON        !< number of contacts
integer                         :: SOURCE       !< id to source contact
integer                         :: TAMB_ID      !< id to ambient temperature contact
integer                         :: DRAIN        !< id to drain contact
integer                         :: GATE         !< id to gate contact
logical                         :: OP_ADD_OFF   !< if true: poisson equation is initialized

! -- DC operation points ------------------------------------------------------
integer                         :: OP           !< actual operation point
integer                         :: N_OP         !< number of operation points
real(8)                         :: OP_FRAC      !< fraction of bias difference to next operation point
logical                         :: OP_FIRST     !< if true: first operation point
logical                         :: OP_FORCE     !< if true: force saving although no convergence reached
logical                         :: OP_SAVE      !< if true: next operation point reached
logical                         :: FX_RAND      !< if true: random bulk electron field
real(8)                         :: FX_ABS       !< max. random field

! -- transient simulation -----------------------------------------------------
logical                         :: TR_ON        !< if true: transient simulation activated
integer                         :: TR_NT        !< number of time steps
integer                         :: TR_IT        !< actual time step
real(8),dimension(:),allocatable    :: TR_DT        !< time step size
real(8),dimension(:),allocatable    :: TR_TOT       !< integral over all TR_DT(1:TR_IT)
real(8),dimension(:,:),allocatable  :: TR_OP        !< transient operation points

! -- AC simulation ------------------------------------------------------------
logical                         :: AC_SIM       !< if true: AC simulation activated
integer                         :: F_LEN        !< size of frequency vector
real(8),dimension(:),allocatable:: FREQ         !< frequency vector


private

! ---> public types ---------------------------------------------------------------------------------------------------
public cont_type

! ---> public variables -----------------------------------------------------------------------------------------------
public CON
public N_CON
public SOURCE
public TAMB_ID
public DRAIN
public GATE
public OP
public N_OP
public OP_FRAC
public OP_FORCE
public OP_FIRST
public OP_SAVE
public OP_ADD_OFF
public FX_RAND
public FX_ABS
public TR_ON
public TR_DT
public TR_NT
public TR_TOT
public TR_IT
public TR_OP
public AC_SIM
public F_LEN
public FREQ

! ---> public subroutines/ functions ----------------------------------------------------------------------------------
public init_contact
! public get_econok
public get_bias
public write_op_start
! public get_einj_con_sub
public set_bias_diff
public close_contact
public init_energylevels


contains

subroutine init_contact
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the properties of contact module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> init contact properties
    call init_con

    ! --> init list of operation points
    call init_dcbias

    ! --> init ac simulation
    call init_acbias

    ! --> init transient simulation
    call init_trbias  
endsubroutine

subroutine init_con
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the properties of all contacts using the user input information
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer                        :: n,i,j,k,m,c,id
    character(len=64),dimension(2) :: model_con

    ! --> get total number of contacts
    if (STRUC_DIM.eq.0) then
        ! --> 1D BULK
        N_CON = 1 ! todo: check

    else
        ! --> DEVICE simulation
        N_CON = maxval(BOX(:)%cont_id) !+ maxval(BOX(:)%supply_id)
        N_CON = N_CON + 1 !temperature contact

    endif

    ! --> init contact struct
    allocate(CON(N_CON))

    ! --> set semiconductor point next to contact
    do n=1,N_CON
        CON(n)%injection = .false.
        do k=1,NZ1
            do j=1,NY1
                do i=1,NX1
                    if (POINT(i,j,k)%cont_id.eq.n) then
                        if (POINT(i,j,k)%semi_cont) then
                            CON(n)%injection = .true.
                            CON(n)%sp_idx    = POINT(i,j,k)%sp_idx ! index to semiconductor

                        endif
                    endif
                enddo
            enddo
        enddo
    enddo

    ! --> set semiconductor point next to contact for gate contact
    do n=1,N_CON
        if (.not.CON(n)%injection) then
            CON(n)%sp_idx = 1 ! todo: what is a good index here?

        endif
    enddo

    ! --> set contact parameters
    if (STRUC_DIM.eq.0) then
        CON(1)%name = 'f_x'

    else
        do i=1,N_BOX
            !add supply contacts after "normal contacts"
            if ((BOX(i)%cont_id.gt.0).and.(BOX(i)%supply_id.eq.0)) then
                m                  = BOX(i)%model_id
                c                  = BOX(i)%cont_id
                write(CON(c)%mod_name,'(A)') US%contact(m)%mod_name
                CON(c)%name        = BOX(i)%cont_name
                CON(c)%shky        = US%contact(m)%con_type(1:8).eq.'schottky'
                CON(c)%supply      = US%supply(m)%mod_name(1:6).eq.'supply' !base boundary condition
                CON(c)%supply_n    = US%supply(m)%supply_type.eq.'n' !base boundary condition
                CON(c)%supply_p    = US%supply(m)%supply_type.eq.'p' !base boundary condition
                CON(c)%limiting    = US%contact(m)%con_type(1:8).eq.'limiting' !limiting injection contact
                CON(c)%diri        = BOX(i)%diri
                CON(c)%v_n         = US%contact(m)%v_n
                CON(c)%v_n2        = US%contact(m)%v_n2
                CON(c)%v_p         = US%contact(m)%v_p
                if (SEMI_EXIST) then
                  CON(c)%semi_id   = SP(CON(c)%sp_idx)%model_id
                  CON(c)%injcharge = SEMI(CON(c)%semi_id)%ELEC.and.SEMI(CON(c)%semi_id)%HOLE
                endif
                

                if (CON(c)%shky) then
                  ! ------- schottky contact -------------------------------------------
                  CON(c)%neumann         = .false.
                  CON(c)%setfloat        = .false.

                elseif (CON(c)%limiting) then
                  ! ------- limiting contact -------------------------------------------
                  CON(c)%neumann         = .false.
                  CON(c)%setfloat        = .false.


                elseif (CON(c)%supply) then
                    ! ------ supply contact ------------------------------------------------
                    CON(c)%neumann         = .false.
                    CON(c)%setfloat        = .false.
                    
                else
                    ! ------ ohmic contact ------------------------------------------------
                    if (US%contact(m)%ohmic_bc(1: 5).eq.'float') then
                        CON(c)%neumann         = .true.
                        CON(c)%setfloat        = .false.
                    
                    elseif (US%contact(m)%ohmic_bc(1:15).eq.'setfloat_couple') then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .true.

                    elseif ((US%contact(m)%ohmic_bc(1: 8).eq.'setfloat').or.(US%contact(m)%ohmic_bc(1:10).eq.'neutrality')) then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .true.

                    elseif (US%contact(m)%ohmic_bc(1:18).eq.'dirichlet_injconst') then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .false.

                    elseif (US%contact(m)%ohmic_bc(1:16).eq.'dirichlet_couple') then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .false.

                    elseif (US%contact(m)%ohmic_bc(1: 9).eq.'dirichlet') then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .false.
                    
                    elseif (US%contact(m)%ohmic_bc(1: 7).eq.'efconst') then
                        CON(c)%neumann         = .false.
                        CON(c)%setfloat        = .false.

                    else
                        write(*,*) 'error: contact type not valid'
                        stop

                    endif
                endif
            endif
        enddo
        !supply contacts
        do i=1,N_BOX
            if (BOX(i)%supply_id.gt.0) then
                m                  = BOX(i)%model_id
                c                  = BOX(i)%cont_id
                write(CON(c)%mod_name,'(A)') US%contact(m)%mod_name
                CON(c)%name        = BOX(i)%cont_name
                CON(c)%supply      = .true.
                CON(c)%supply_n    = US%supply(m)%supply_type.eq.'n' !base boundary condition
                CON(c)%supply_p    = US%supply(m)%supply_type.eq.'p' !base boundary condition
                CON(c)%diri        = .false.
                if (SEMI_EXIST) then
                    CON(c)%semi_id   = SP(CON(c)%sp_idx)%model_id
                    CON(c)%injcharge = SEMI(CON(c)%semi_id)%ELEC.and.SEMI(CON(c)%semi_id)%HOLE
                endif
                CON(c)%neumann         = .false.
                CON(c)%setfloat        = .false.
            endif
        enddo
    endif

    !temperature contact
    CON(N_CON)%name = trim('t')

    ! --> define source, drain & gate contact id
    SOURCE  = 0
    DRAIN   = 0
    GATE    = 0
    TAMB_ID = N_CON
    do n=1,N_CON
        ! first try to find the names from the conventional naming schemes
        if ( CON(n)%name.eq.'b' ) then
            GATE = n
        elseif ( CON(n)%name.eq.'c' ) then
            DRAIN = n
        elseif ( CON(n)%name.eq.'e' ) then
            SOURCE = n
        elseif (CON(n)%injection.and.(.not.CON(n)%supply)) then
            if (SOURCE.eq.0) then
                SOURCE   = n

            else
                if (SEMI_EXIST) then
                    if (CON(SOURCE)%sp_idx.gt.CON(n)%sp_idx) then
                        DRAIN  = SOURCE
                        SOURCE = n

                    else
                        DRAIN  = n

                    endif
                else
                    DRAIN = n

                endif
              endif
        elseif ( CON(n)%name.eq.'t' ) then
            TAMB_ID = n
        else
            GATE = n

        endif
    enddo

    if (GATE.eq.0) then
        write(*,*) '***warning*** no GATE or BASE contact detected. This may cause unexpected behavior.'
    endif
    if (DRAIN.eq.0) then
        write(*,*) '***warning*** no DRAIN or COLLECTOR contact detected. This may cause unexpected behavior.'
    endif
    if (SOURCE.eq.0) then
        write(*,*) '***warning*** no SOURCE or EMITTER contact detected. This may cause unexpected behavior.'
    endif

    if (.not.SEMI_EXIST) return

    ! --> set conduction band structure
    if (SOURCE.gt.0) then
        write(model_con(1),'(A)') CON(SOURCE)%mod_name

    endif
    if (DRAIN.gt.0) then
        write(model_con(2),'(A)') CON(DRAIN)%mod_name

    endif

    id=0
    do n=1,N_CON
        CON(n)%n_band = 0
    enddo

    if ( any(CON(:)%sp_idx.eq.0) ) then
        write(*,*) '***error*** a contact is not located inside semiconductor region.'
        stop
    endif

endsubroutine


subroutine init_energylevels
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the intrinsic equilibirium contact energy level
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8)              :: nd
    integer              :: n,id,hetero_id

    hetero_id = 0

    ! --> contacts
    do n=1,N_CON
        ! ohmic contact
        if (CON(n)%injection) then
            id = CON(n)%sp_idx
            if (HETERO) hetero_id=id
            nd = CD(id)
            CON(n)%efi  = get_fermi_equ(CON(n)%semi_id,nd,hetero_id,UN(id),UP(id),UN2(id))

            !derivative of efi
            TEMP_LATTICE   = TEMP_LATTICE + 0.01
            CON(n)%efi_dtl = (get_fermi_equ(CON(n)%semi_id,nd,hetero_id,UN(id),UP(id),UN2(id)) - CON(n)%efi)/0.01
            TEMP_LATTICE   = TEMP_LATTICE - 0.01


        else
            CON(n)%efi    = dble(0)
        endif
        
    enddo
endsubroutine


subroutine init_dcbias
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize list of DC bias points for each contact
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),dimension(:),allocatable :: oplist
    integer                          :: fx_len
    integer                          :: i,j,k,l,n,op_max,wied,rep
    logical                          :: first
    integer,dimension(:)  ,pointer   :: op_lens
    real(8),dimension(:,:),pointer   :: op_temp,op_temp1
    logical                          :: found_contact = .false.

    nullify(op_lens)
    nullify(op_temp)
    nullify(op_temp1)

    FX_RAND    = BULK.and.(US%biasdef%random(1:2).eq.'on')
    OP_ADD_OFF = .false.

    found_contact = .false.
    do i=1,US%n_biasinfo
        if (US%biasinfo(i)%cont_name.eq.'t') then
            found_contact = .true.
            exit
        endif
    enddo
    if (.not.found_contact) then
        write(*,*) '***error*** no contact with cont_name t found in &BIAS_INFO input. You need to specify the temperature explicitly.'
        stop
    endif

    if (US%biasdef%list.gt.0) then
        ! --> get list for each contact
        first = .true.
        do i=1,US%n_biasinfo
            found_contact = .false.
            do j=1,N_CON
                if (US%biasinfo(i)%cont_name.eq.CON(j)%name) then
                    found_contact = .true.
                    call init_grid(US%biasinfo(i)%bias_val, US%biasinfo(i)%bias_fun, US%biasinfo(i)%cont_name,oplist ,N_OP)
                    if (first) then
                        allocate(op_temp(N_OP,N_CON))
                        first        = .false.
                        op_temp      = 0

                    endif
                    if (N_OP.eq.size(op_temp,1)) then
                        ! same size
                        op_temp(:,j) = oplist

                    elseif (N_OP.eq.1) then
                        ! only one op point --> copy
                        do k=1,size(op_temp,1)
                            op_temp(k,j) = oplist(1)
                        enddo
                        N_OP = size(op_temp,2)

                    elseif ((N_OP.gt.size(op_temp,1)).and.(size(op_temp,1).eq.1)) then
                        ! until now only one point, now more --> enlarge op_temp
                        allocate(op_temp1(1,N_CON))
                        do l=1,N_CON
                            op_temp1(1,l) = op_temp(1,l)
                        enddo
                        deallocate(op_temp)
                        nullify(op_temp)
                        allocate(op_temp(N_OP,N_CON))
                        do l=1,N_CON
                            if (l.eq.j) then
                                do k=1,N_OP
                                    op_temp(k,j) = oplist(k)
                                enddo
                            else
                                do k=1,N_OP
                                    op_temp(k,l) = op_temp1(1,l)
                                enddo
                            endif
                        enddo
                        deallocate(op_temp1)
                        nullify(op_temp1)
                      

                    else
                        write(*,*) '***error*** length of bias point list for each contact must be equal'
                        stop

                    endif
                endif
            enddo
            if (.not.found_contact) then
                write(*,*) '***error*** no contact found for &BIAS_INFO with cont_name  ' // US%biasinfo(i)%cont_name
                stop
            endif
        enddo
      
    else
        ! --> combine lists
        allocate(op_lens(N_CON))
        op_lens = 0
        do i=1,US%n_biasinfo
            do j=1,N_CON
                if (US%biasinfo(i)%cont_name.eq.CON(j)%name) then
                    call init_grid(US%biasinfo(i)%bias_val, US%biasinfo(i)%bias_fun, US%biasinfo(i)%cont_name ,oplist ,fx_len)
                    op_lens(j) = fx_len

                endif
            enddo
        enddo
        N_OP   = 1  ! total ap list length
        op_max = 1  ! longest ap contact
        do j=1,N_CON
            N_OP = N_OP*op_lens(j)
            if (op_lens(j).gt.op_max) then
                op_max = op_lens(j)
            endif
        enddo

        allocate(op_temp1(op_max,N_CON))
        allocate(op_temp(N_OP,N_CON))
        op_temp = 0
        do i=1,US%n_biasinfo
            do j=1,N_CON
                if (US%biasinfo(i)%cont_name.eq.CON(j)%name) then
                    call init_grid(US%biasinfo(i)%bias_val, US%biasinfo(i)%bias_fun, US%biasinfo(i)%cont_name ,oplist ,fx_len)
                    do k=1,op_lens(j)
                        op_temp1(k,j) = oplist(k) ! fill first entries
                    enddo
                endif
            enddo
        enddo
        
        ! --> very complicated ----
        do j=1,N_CON
            rep = 1
            do i=j+1,N_CON
                rep = rep * op_lens(i)
            enddo
            wied = 1
            do i=1,j-1
                wied = wied * op_lens(i)
            enddo
            n = 0
            do i=1,rep
                do k=1,op_lens(j)
                    do l=1,wied
                        n = (i-1)*op_lens(j)*wied + (k-1)*wied + l
                        op_temp(n,j) = op_temp1(k,j)
                    enddo
                enddo
            enddo
        enddo
        
        deallocate(op_lens)
    endif
    N_OP = size(op_temp,1)

    if (N_OP.le.0) then
        write(*,*) '***warning*** no operation points defined!'
    endif

    ! --> list of all contact
    do i=1,N_CON
        allocate(CON(i)%bias(N_OP))
        CON(i)%bias = op_temp(:,i)

    enddo

    ! deallocate pointer
    if (associated(op_lens))  deallocate(op_lens)
    if (associated(op_temp))  deallocate(op_temp)
    if (associated(op_temp1)) deallocate(op_temp1)

endsubroutine

subroutine set_bias_diff(dv_step)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! set additional bias steps to improove convergence
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real(8), intent(out) :: dv_step

  integer :: c

  ! --> go through contacts and get max. voltage step
  dv_step=0
  do c=1,N_CON 
      if (c.eq.TAMB_ID) cycle
      if (OP.eq.1) then
          CON(c)%bias_diff = CON(c)%bias(OP)
      else
          CON(c)%bias_diff = CON(c)%bias(OP)-CON(c)%bias(OP-1)
      endif
      if (abs(CON(c)%bias_diff).gt.dv_step) dv_step=abs(CON(c)%bias_diff)
  enddo

  !zero_first = (US%biasdef%zero_bias_first.gt.0).and.(OP.eq.1)
  !if (zero_first) n_op1 = n_op1+1
endsubroutine


subroutine init_acbias
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the AC simulation frequency vector
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    CHARACTER(len=64) :: cont_name

    ! --> check if ac simulation can be performed
    AC_SIM   = .true.

    ! --> get frequency vector
    cont_name = trim('F')
    call init_grid(US%acinfo%freq_val, US%acinfo%sweep_type, cont_name, FREQ ,F_LEN)
    !if (F_LEN.eq.0) then
    if (FREQ(1).eq.0) then
        AC_SIM = .false.

    endif
    ! 0 FREQUENCY IS QUASI STATIC SIMULATION!
    ! elseif (maxval(FREQ).le.dble(0)) then
    !     AC_SIM = .false.

    ! endif

endsubroutine

subroutine init_trbias
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize transient simulation, i.e. the time dependent voltage at the contacts
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j = 0

    ! this may be incorrect, check when implementing transient
    i = 0

    ! this allows to use a logarithmic timescale.
    if (US%biasdef%t_func_type(1:6).eq.'log_dt') then
        call init_grid(US%biasdef%t_val, US%biasdef%t_func_type(1:3), US%biasinfo(i)%cont_name, TR_DT, TR_NT)
        allocate(TR_TOT(TR_NT))
        TR_ON = TR_NT.gt.0
        if (.not.TR_ON) return
        
        TR_TOT(1)    = TR_DT(1)
        do i=2,TR_NT
            TR_TOT(i)  = TR_DT(i) + TR_TOT(i-1)
        enddo

    elseif (US%biasdef%t_func_type(1:3).eq.'log') then
        call init_grid(US%biasdef%t_val, US%biasdef%t_func_type(1:3), US%biasinfo(i)%cont_name, TR_TOT, TR_NT)
        allocate(TR_DT(TR_NT))
        TR_ON = TR_NT.gt.0
        if (.not.TR_ON) return
        
        do i=1,(TR_NT-1)
            TR_DT(i)   = TR_TOT(i+1 ) -TR_TOT(i)
        enddo
        TR_DT(TR_NT) = TR_DT(TR_NT-1)

    else
        TR_NT = US%biasdef%n_t
        allocate(TR_TOT(TR_NT))
        allocate(TR_DT(TR_NT))
        TR_ON = TR_NT.gt.0
        if (.not.TR_ON) return
        
        TR_DT= US%biasdef%d_t
        do i=1,TR_NT
            TR_TOT(i)  = i*TR_DT(1) !all TR_DT are equal in this case
        enddo

    endif

    allocate(TR_OP(N_CON,TR_NT))

    do i=1,N_CON
        TR_OP(i,:) = dble(0)
        do n=1,US%n_trinfo
            if (CON(i)%name.eq.US%trinfo(n)%cont_name) then
                if (US%trinfo(n)%func_type(1:4).eq.'step') then
                    do j=1,TR_NT
                        if (US%trinfo(n)%T(1).ge.(TR_TOT(j))) then
                            TR_OP(i,j) = dble(0)
                        else
                            TR_OP(i,j) = US%trinfo(n)%v_max(1)
                        endif
                    enddo

                elseif (US%trinfo(n)%func_type(1:3).eq.'sin') then
                    do j=1,TR_NT
                        TR_OP(i,j) = US%trinfo(n)%v_max(1)*sin(2*PI*TR_TOT(j)/US%trinfo(n)%T(1) + US%trinfo(n)%phase(1))
                    enddo

                elseif (US%trinfo(n)%func_type(1:4).eq.'ramp') then
                    
                elseif (US%trinfo(n)%func_type(1:5).eq.'pulse') then

                elseif (US%trinfo(n)%func_type(1:3).eq.'tab') then
                    !call init_grid(US%trinfo(n)%v_max, 'TAB', IR_TV, IR_LEN, dummyl, dummy1, dummy2, dummy3)

                endif
                exit
            endif
        enddo
    enddo


endsubroutine


function get_bias(c) result(vcon)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get bias voltage for given contact id
    ! 
    ! input
    ! -----
    ! c : integer
    !   The id of the contact in the CON structure
    !
    ! output
    ! ------
    ! vcon : real
    !   The bias [v] of the contact at the current operating point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: c
    real(8) :: vcon

    if (OP.eq.0) then 
        vcon = dble(0)
    else
        vcon   = CON(c)%bias(OP) - CON(c)%bias_diff*(dble(1)-OP_FRAC) + CON(c)%bias_mm
        if (TR_IT.gt.0) then
            vcon = vcon + TR_OP(c,TR_IT)

        endif
    endif

endfunction


subroutine write_op_start
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! output information for actual operation point, start
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer           :: j

    ! --> number of operation points output on console
    if (.not.OP_SAVE) then
        write(*,'(A19,I3,A2,I3,A2,I2,A19)') '>----------------- ',OP,' /',N_OP,' (',int(100*OP_FRAC),'%) ---------------<'

    else
        write(*,'(A21,I3,A2,I3,A21)') '>------------------- ',OP,' /',N_OP,' -------------------<'

    endif

    ! --> number of operation points output in debug file
    write(DEB_ID,*) 'op:',OP, '  op_frac:',OP_FRAC,'  op_save:',OP_SAVE,'  op_force:',OP_FORCE
    
    ! --> bias voltage output on console
    do j=1,N_CON
        if (BULK) then
            call write_strunit(6     ,trim(CON(j)%name)//': ',CON(j)%bias(OP)*0.01,'V/cm')
            call write_strunit(DEB_ID,trim(CON(j)%name)//': ',CON(j)%bias(OP)*0.01,'V/cm')

        else
            if (j.eq.TAMB_ID) then
                call write_strunit(6     ,'Ambient Temperature: ',get_bias(j),'K')
                call write_strunit(DEB_ID,'Ambient Temperature: ',get_bias(j),'K')
        
            else
                call write_strunit(6     ,'V_'//trim(CON(j)%name)//': ',get_bias(j),'V')
                call write_strunit(DEB_ID,'V_'//trim(CON(j)%name)//': ',get_bias(j),'V')
            endif

        endif
    enddo

endsubroutine


subroutine close_contact
    !!!!!!!!!!!!!!!!!!!!!!!!!
    ! close module
    !!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n

    do n=1,size(CON)
        if (allocated(CON(n)%band_pointer))    deallocate(CON(n)%band_pointer)
        if (associated(CON(n)%bias))    deallocate(CON(n)%bias)
    enddo
    if (allocated(CON))    deallocate(CON)
    if (allocated(TR_OP))  deallocate(TR_OP)
    if (allocated(TR_TOT)) deallocate(TR_TOT)
    if (allocated(TR_DT))  deallocate(TR_DT)
    if (allocated(FREQ))   deallocate(FREQ)

endsubroutine

endmodule
