!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! implementation of semiconductor  heterostructure models
module heterostructure

use phys_const
use profile, only: CD, CDTOT, DONATORS, ACCEPTORS
use profile, only: GRADING,GRADING2,STRAIN
use semiconductor_globals, only: N_SP, TEMP_LATTICE, VT_LATTICE, UN, UN2, UP, HETERO, BGN_SEMI, EC0, EV0
use bandstructure, only: SEMI,get_fermi_equ,get_fermi_e,get_valence_band_index,get_conduction_band_index,get_conduction_band_index2
use bandstructure, only: semi_prop
use read_user  , only : US                   !< all parameters from user input file
use structure, only: POINT
use structure, only: NZ1, NX1, NY1, X
use math_oper
use strainedsige

implicit none

interface bow !this interface allows to call bowing function with real and dual valued arguments
  module procedure bow_real, bow_dual
end interface

public init_heterostructure
public bow

contains

subroutine init_heterostructure()
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all heterostructure related models/parameters and BGN that do not depend on bias (but on T)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer  :: i,j,n,k
    integer  :: sp_id, semi_id
    logical  :: has_cb,has_vb,fermi,bgn_apparent
    real(8)  :: n_eff,dbgn
    real(8)  :: meff, meff_A, meff_B, deg_A, deg_B, deg = 0
    real(8)  :: bgn = 0
    real(8)  :: bandgap,bandgap_A, bandgap_B = 0
    real(8)  :: ec,ev = 0
    real(8)  :: al, al_B, al_A = 0
    real(8)  :: eoff, eoff_A, eoff_B = 0
    real(8)  :: eps, eps_A, eps_B = 0
    real(8)  :: fac = 0 
    real(8)  :: nun, nup, fermi_05n,fermi_05p,  ef = 0 
    integer  :: id_materialA, id_materialB,index = 0
    type(semi_prop),pointer :: alloy,semi_A,semi_B
    !real(8),dimension(:),allocatable :: BGN_p
    !real(8) :: arg
    !integer  :: id_x

    ! for alloy test:
    ! integer :: id_inasp, id_inp, id_ingaas, i_left, i_right
    ! real(8) :: ec_left, ec_right, ev_left, ev_right, ec2_left, ec2_right, dx, val_right, val_left

    EC0  = 0
    EV0  = 0

    UN  = 0
    UN2 = 0
    UP  = 0

    !calculate n_eff for every point in the semiconductor
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%semi_id.GT.0) then
                    semi_id = POINT(i,j,k)%semi_id
                    do n=1,SEMI(semi_id)%n_band
                        if (.not.allocated(SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff)) then
                            allocate(SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff(N_SP))
                            n_eff = SEMI(semi_id)%band_pointer(n)%band%n_eff
                            SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff = n_eff
                        endif
                        if (.not.allocated(SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff_output)) then
                            allocate(SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff_output(N_SP))
                            SEMI(semi_id)%band_pointer(n)%band%pos_dep_n_eff_output = 0
                        endif
                        if (.not.allocated(SEMI(semi_id)%band_pointer(n)%band%pos_dep_al)) then
                            allocate(SEMI(semi_id)%band_pointer(n)%band%pos_dep_al(N_SP))
                            al = SEMI(semi_id)%band_pointer(n)%band%al
                            SEMI(semi_id)%band_pointer(n)%band%pos_dep_al = al
                        endif
                        if (.not.allocated(SEMI(semi_id)%band_pointer(n)%band%pos_dep_m_eff)) then
                            allocate(SEMI(semi_id)%band_pointer(n)%band%pos_dep_m_eff(N_SP))
                            SEMI(semi_id)%band_pointer(n)%band%pos_dep_m_eff = SEMI(semi_id)%band_pointer(n)%band%me
                        endif
                    enddo
                endif
            enddo
        enddo
    enddo

    !calculate strain
    if (US%n_strain.eq.1) then
        STRAIN = US%strain(1)%a1*GRADING2 + US%strain(1)%a2*GRADING + US%strain(1)%a3*GRADING
    elseif (US%n_strain.ge.2) then
        write(*,*) '***error*** only one strain block allowed.'
        stop
    endif

    ! bgn calculation here
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1

                if (POINT(i,j,k)%semi_id.GT.0) then
                    alloy        => SEMI(semi_id)
                    semi_id = POINT(i,j,k)%semi_id
                    sp_id   = POINT(i,j,k)%sp_idx

                    bgn=0 !interpolation of BGN parameters for alloy just for JAIN model
                    if (alloy%bgn_is_default) then
                        !TODO:FIX me, get in contact with TU Wien
                        !bgn   = calc_bgn_default(semi_id,i,j,k,sp_id,CDTOT(sp_id),TEMP_LATTICE )
                        bgn   = 0
                    elseif (alloy%bgn_is_jain) then
                        bgn   = calc_bgn_jain(semi_id,DONATORS(sp_id),ACCEPTORS(sp_id), GRADING(sp_id))
                    elseif (alloy%bgn_is_classic) then
                        bgn   = calc_bgn_classic(semi_id,CDTOT(sp_id),TEMP_LATTICE )
                    endif

                    BGN_SEMI(sp_id) = bgn
                endif
            enddo
        enddo
    enddo
    ! BGN_SEMI = 0

    ! diffusion equation in X direction
    ! https://scipython.com/book/chapter-7-matplotlib/examples/the-two-dimensional-diffusion-equation/
    ! if (US%dd%diffuse.gt.0) then
    ! ! if (.TRUE.) then
    !     allocate(BGN_p(size(BGN_SEMI)))
    !     BGN_p = BGN_SEMI
    !     do n=1,10
    !         do j=1,NY1
    !             do i=1+2,NX1-2
    !                 id_x = POINT(i,j,1)%sp_idx
    !                 arg = 5e-19*(BGN_SEMI(id_x+1)-2*BGN_SEMI(id_x)+BGN_SEMI(id_x-1))/(X(i+2)-X(i-1))**2
    !                 BGN_p(id_x) = BGN_SEMI(id_x) + arg
    !             enddo
    !         enddo
    !         BGN_SEMI = BGN_p
    !     enddo
    !     deallocate(BGN_p)
    ! endif

    !bowing and setting of first order material parameters
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1

                if (POINT(i,j,k)%semi_id.GT.0) then
                    semi_id = POINT(i,j,k)%semi_id
                    sp_id   = POINT(i,j,k)%sp_idx

                    alloy        => SEMI(semi_id)
                    if (alloy%is_alloy) then
                        id_materialA = alloy%id_materialA
                        id_materialB = alloy%id_materialB
                        semi_A       => SEMI(id_materialA)
                        semi_B       => SEMI(id_materialB)
                    endif

                    if (size(alloy%band_pointer).eq.0) then
                        write(*,*) '***error*** No bands allocated in SEMI.'
                        stop
                    endif

                    has_cb = .false.
                    do n=1,size(alloy%band_pointer)
                        if (alloy%band_pointer(n)%band%iscb) then
                            has_cb=.true.
                        endif
                    enddo

                    has_vb = .false. 
                    do n=1,size(alloy%band_pointer)
                        if (.not.(alloy%band_pointer(n)%band%iscb)) then
                            has_vb=.true.
                        endif
                    enddo

                    !catch more or less than two bands
                    if (size(alloy%band_pointer).GT.3) then
                        write(*,*) '***error*** Currently heterostructure is only supported with no more than 3 bands.'
                        stop
                    endif

                    !1: calculate Eoff, offset energy
                    if (.not.alloy%is_alloy) then
                        eoff    = alloy%eoff
                    else
                        eoff_A  = semi_A%eoff
                        eoff_B  = semi_B%eoff
                        eoff    = bow(eoff_A, eoff_B, alloy%eoff_c, GRADING(sp_id))
                    endif

                    !2: calculate permittivity eps 
                    if (.not.alloy%is_alloy) then
                        eps          = alloy%eps
                    else
                        eps_A        = semi_A%eps
                        eps_B        = semi_B%eps
                        eps          = bow(eps_A, eps_B, alloy%eps_c, GRADING(sp_id))
                    endif
                    POINT(i,j,k)%eps = eps

                    if ((.not.has_cb).or.(.not.has_vb)) then
                        cycle
                    endif

                    !3: calculate n_eff of valence band
                    if (.not.SEMI(semi_id)%is_alloy) then
                        index        = get_valence_band_index(SEMI(semi_id))
                        meff         = alloy%band_pointer(index)%band%me/AM0 !convert back to relative
                        deg          = alloy%band_pointer(index)%band%deg
                        al          = alloy%band_pointer(index)%band%al
                    else
                        index        = get_valence_band_index(semi_A)
                        meff_A       = semi_A%band_pointer(index)%band%me/AM0
                        deg_A        = semi_A%band_pointer(index)%band%deg
                        al_A         = semi_A%band_pointer(index)%band%al
                        index        = get_valence_band_index(semi_B)
                        meff_B       = semi_B%band_pointer(index)%band%me/AM0
                        deg_B        = semi_B%band_pointer(index)%band%deg
                        al_B         = semi_B%band_pointer(index)%band%al
                        index        = get_valence_band_index(alloy)
                        meff         = bow(meff_A, meff_B, alloy%band_pointer(index)%band%me_c, GRADING(sp_id))
                        deg          = bow(deg_A, deg_B, dble(0), GRADING(sp_id))
                        al           = bow(al_A, al_B, dble(0), GRADING(sp_id))

                    endif
                    meff = meff*AM0 !convert to kg

                    !3.1: change of effective mass for strained SiGe--------------------------------------------
                    if ((alloy%is_alloy).and.(alloy%strain_is_sige)) then
                            meff = get_meff_valence(GRADING(sp_id), SEMI(semi_id)%band_pointer(index)%band%dim)
                        
                            meff = meff*AM0
                    endif
                    
                    index = get_valence_band_index(alloy)
                    n_eff = deg*2*(meff*Q/HBAR/HBAR/dble(2)/PI)**(SEMI(semi_id)%band_pointer(index)%band%dim/dble(2))
                                                    !this%deg*2*(this%me*Q/HBAR/HBAR/dble(2)/PI)**(this%dim/dble(2))
 
                    if ((alloy%is_alloy).and.(alloy%strain_is_DOS)) then
                        meff = meff_A*AM0 
                        deg  = deg_A
                        n_eff = deg*2*(meff*Q/HBAR/HBAR/dble(2)/PI)**(SEMI(semi_id)%band_pointer(index)%band%dim/dble(2))
                        n_eff  = get_strained_DOS_val(GRADING(sp_id), n_eff)
                        ! n_eff  = get_fermi_e(dble(1),dble(1),dble(1))*n_eff
                    endif  

                    alloy%band_pointer(index)%band%pos_dep_n_eff(sp_id) = n_eff
                    alloy%band_pointer(index)%band%pos_dep_m_eff(sp_id) = meff
                    alloy%band_pointer(index)%band%pos_dep_al(sp_id)    = al

                    !now go through conduction bands at this location and fix their energies
                    do n=1,size(alloy%band_pointer)
                        if (.not.alloy%band_pointer(n)%band%iscb) then !we map all changes onto the conduction bands
                            cycle
                        endif

                        !4: calculate bandgap(s) as a function of temperature and grading --------
                        if (.not.alloy%is_alloy) then
                            bandgap   = get_bandgap_0(alloy,n)
                            bandgap   = bandgap_temp(bandgap, alloy%band_pointer(n)%band%e0_alpha, SEMI(semi_id)%band_pointer(n)%band%e0_beta, TEMP_LATTICE)
                        else
                            !get bandgap of material A
                            bandgap_A    = get_bandgap_0(semi_A,n)
                            bandgap_A    = bandgap_temp(bandgap_A, semi_A%band_pointer(n)%band%e0_alpha, semi_A%band_pointer(n)%band%e0_beta, TEMP_LATTICE)

                            !get bandgap of material B
                            bandgap_B    = get_bandgap_0(semi_B,n)
                            bandgap_B    = bandgap_temp(bandgap_B, semi_B%band_pointer(n)%band%e0_alpha, semi_B%band_pointer(n)%band%e0_beta, TEMP_LATTICE)

                            !bow
                            bandgap      = bow(bandgap_A, bandgap_B, alloy%band_pointer(n)%band%e0_c, GRADING(sp_id))
                        endif

                        !5: change of effective mass --------------------------------------------
                        if (.not.alloy%is_alloy) then
                            meff         = alloy%band_pointer(n)%band%me/AM0!convert back to relative
                            deg          = alloy%band_pointer(n)%band%deg
                            al           = alloy%band_pointer(n)%band%al
                        else
                            ! index        = get_conduction_band_index(semi_A)
                            meff_A       = semi_A%band_pointer(n)%band%me/AM0!convert back to relative
                            deg_A        = semi_A%band_pointer(n)%band%deg
                            al_A         = semi_A%band_pointer(n)%band%al
                            ! index        = get_conduction_band_index(semi_B)
                            meff_B       = semi_B%band_pointer(n)%band%me/AM0!convert back to relative
                            deg_B        = semi_B%band_pointer(n)%band%deg
                            al_B         = semi_B%band_pointer(n)%band%al

                            meff         = bow(meff_A, meff_B, alloy%band_pointer(n)%band%me_c, GRADING(sp_id))
                            deg          = bow(deg_A, deg_B, dble(0), GRADING(sp_id))
                            al           = bow(al_A, al_B, dble(0), GRADING(sp_id))

                        endif
                        meff = meff*AM0 !convert to kg
                        
                        !5.1: change of effective mass for strained SiGe--------------------------------------------
                        if ((alloy%is_alloy).and.(alloy%strain_is_sige)) then
                            meff = get_mcond(GRADING(sp_id))
                            deg  = get_deg(GRADING(sp_id))
                            meff = meff*AM0
                        endif

                        !6: calculate n_eff with new effective mass
                        n_eff = deg*2*(meff*Q/HBAR/HBAR/dble(2)/PI)**(SEMI(semi_id)%band_pointer(n)%band%dim/dble(2))

                        if ((alloy%is_alloy).and.(alloy%strain_is_DOS)) then
                            meff = meff_A*AM0 
                            deg  = deg_A
                            n_eff = deg*2*(meff*Q/HBAR/HBAR/dble(2)/PI)**(SEMI(semi_id)%band_pointer(n)%band%dim/dble(2))
                            n_eff  = get_strained_DOS_cond(GRADING(sp_id), n_eff)
                        endif                  
                         
                        alloy%band_pointer(n)%band%pos_dep_n_eff(sp_id) = n_eff
                        alloy%band_pointer(n)%band%pos_dep_m_eff(sp_id) = meff
                        alloy%band_pointer(n)%band%pos_dep_al(sp_id)    = al

                        !intermediate step: put results so far on UN, UP since they are needed for BGN
                        ev        = eoff
                        ec        = eoff + bandgap

                        index     = get_valence_band_index(alloy)
                        UP(sp_id) =-alloy%band_pointer(index)%band%e0 - ev
                        if (n.eq.get_conduction_band_index(alloy)) then
                            UN(sp_id)  = alloy%band_pointer(n)%band%e0 - ec
                            EV0(sp_id) = ev
                            EC0(sp_id) = ec
                        elseif (n.eq.get_conduction_band_index2(alloy)) then
                            UN2(sp_id) = alloy%band_pointer(n)%band%e0 - ec
                        endif

                        !7: BGN application (distribute bgn over valence and conduction band---)
                        bgn       = BGN_SEMI(sp_id) !was alreacy calculated before to enable smoothing

                        ! apparaent BGN correction if needed
                        fermi = SEMI(semi_id)%fermi
                        bgn_apparent = SEMI(semi_id)%bgn_apparent
                        if ((fermi.and.bgn_apparent).or.(.not.fermi.and..not.bgn_apparent)) then !correct bgn, typical for SiGe

                            ef    = get_fermi_equ(semi_id,CD(sp_id),sp_id,UN(sp_id),UP(sp_id),UN2(sp_id)) !Efi calculation
                            index = get_conduction_band_index(SEMI(semi_id))
                            ec    = SEMI(semi_id)%band_pointer(index)%band%e0 - UN(sp_id) 
                            index = get_valence_band_index(SEMI(semi_id))
                            ev    = -SEMI(semi_id)%band_pointer(index)%band%e0 - UP(sp_id)
                            nun   = (ef-ec)/VT_LATTICE
                            nup   = (ev-ef)/VT_LATTICE

                            ! n-doping:
                            ! (1) dbgn = ec'-ec
                            ! (2) fd1h(ef-ec) = Nd
                            ! (3) exp(ef-ec') = Nd
                            ! (3)=(2)
                            ! fd1h(ef-ec)=exp(ef-ec')
                            ! -log(fd1h(ef-ec))-ec+ef=-bgn
                            ! p-doping same calculation just with -1*(ec-ef) and ec replace with ev

                            !evaluate fermi integral
                            if (nun.ge.nup) then
                                fermi_05n   = fd1h(nun)
                                dbgn =  ef - ec - VT_LATTICE * log(fermi_05n) ! ef -ec - (ef-ec)
                            else
                                fermi_05p   = fd1h(nup)
                                dbgn =  ev - ef - VT_LATTICE * log(fermi_05p) ! ev - ef - (ev-ef)
                            endif

                            if (.not.bgn_apparent) dbgn = -dbgn
                        
                        elseif (fermi.and..not.bgn_apparent) then !this case no correction needed, III-V HBTs
                            dbgn = 0

                        elseif (.not.fermi.and.bgn_apparent) then ! no correction needed in this  case (classical SiGe case)
                            dbgn = 0
                        endif

                        bgn = bgn+dbgn
                        BGN_SEMI(sp_id) = bgn 

                        fac       = alloy%bgap_gamhd
                        ev        = eoff + bgn*(1-fac)
                        ec        = eoff + bandgap - bgn*fac

                        index     = get_valence_band_index(SEMI(semi_id))
                        UP(sp_id) =-alloy%band_pointer(index)%band%e0 - ev

                        if (n.eq.get_conduction_band_index(alloy)) then
                            UN(sp_id) = alloy%band_pointer(n)%band%e0 - ec

                        elseif (n.eq.get_conduction_band_index2(alloy)) then
                            UN2(sp_id) = alloy%band_pointer(n)%band%e0 - ec

                        endif
                        !8: TODO: calculate alpha
                    enddo
                endif
            enddo
        enddo
    enddo

    ! these points were first written with eps=0 and then not updated
    ! find closest semi point and take its eps
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                ! POINT(i,j,k)%eps = POINT(i,j,k)%eps*0.5
                if (POINT(i,j,k)%eps.eq.0) then
                    if ((POINT(i,j,k)%cont)) cycle
                    if (i.gt.1) then 
                        if (POINT(i-1,j,k)%semi_id.gt.0) then 
                            POINT(i,j,k)%eps = POINT(i-1,j,k)%eps
                            if (POINT(i,j,k)%eps.ne.0) cycle
                        endif
                    endif
                    if (i.lt.NX1) then 
                        if (POINT(i+1,j,k)%semi_id.gt.0) then 
                            POINT(i,j,k)%eps = POINT(i+1,j,k)%eps
                            if (POINT(i,j,k)%eps.ne.0) cycle
                        endif
                    endif
                    if (j.gt.1) then 
                        if (POINT(i,j-1,k)%semi_id.gt.0) then 
                            POINT(i,j,k)%eps = POINT(i,j-1,k)%eps
                            if (POINT(i,j,k)%eps.ne.0) cycle
                        endif
                    endif
                    if (j.lt.NY1) then 
                        if (POINT(i,j+1,k)%semi_id.gt.0) then 
                            POINT(i,j,k)%eps = POINT(i,j+1,k)%eps
                            if (POINT(i,j,k)%eps.ne.0) cycle
                        endif
                    endif
                endif
            enddo
        enddo
    enddo

    ! set epsilon at contact/oxide boundaries
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if ((POINT(i,j,k)%eps.eq.0).and.(POINT(i,j,k)%cont_b)) then
                    !look for oxide next to point
                    if (POINT(i,j,k)%cont_b_xlow) POINT(i,j,k)%eps = POINT(i-1,j  ,k)%eps
                    if (POINT(i,j,k)%cont_b_xupp) POINT(i,j,k)%eps = POINT(i+1,j  ,k)%eps
                    if (POINT(i,j,k)%cont_b_ylow) POINT(i,j,k)%eps = POINT(i  ,j-1,k)%eps
                    if (POINT(i,j,k)%cont_b_yupp) POINT(i,j,k)%eps = POINT(i  ,j+1,k)%eps
                endif
            enddo
        enddo
    enddo

    ! !now alloy InAsP from left InP to right InGaAs
    ! ! sandwich
    ! id_inasp = 2
    ! id_inp = 1
    ! id_ingaas = 3
    ! do i=1,NX1
    !     if (POINT(i+1,1,1)%semi_id.eq.id_inasp) then
    !         i_left= i
    !         exit
    !     endif
    ! enddo
    ! do i=NX1,1,-1
    !     if (POINT(i-1,1,1)%semi_id.eq.id_inasp) then 
    !         i_right= i
    !         exit
    !     endif
    ! enddo

    ! m = (r-l)/dx
    ! y = m*(x-xl) + l
    ! index     = get_conduction_band_index(alloy)
    ! ec_left   = - UN(i_left) + SEMI(id_inp)%band_pointer(index)%band%e0
    ! ec_right  = - UN(i_right) + SEMI(id_ingaas)%band_pointer(index)%band%e0

    ! index     = get_conduction_band_index2(alloy)
    ! ec2_left  = - UN2(i_left) + SEMI(id_inp)%band_pointer(index)%band%e0
    ! ec2_right = - UN2(i_right) + SEMI(id_ingaas)%band_pointer(index)%band%e0

    ! index     = get_valence_band_index(alloy)
    ! ev_left   = - UP(i_left) - SEMI(id_inp)%band_pointer(index)%band%e0
    ! ev_right  = - UP(i_right) - SEMI(id_ingaas)%band_pointer(index)%band%e0

    ! do i=i_left+1,i_right-1
    !     ! UN(i)  = ( UN(i_right)  - UN(i_left)  )/( X(i_right) - X(i_left) ) * ( X(i) - X(i_left) ) + UN(i_left)
    !     ! UP(i)  = ( UP(i_right)  - UP(i_left)  )/( X(i_right) - X(i_left) ) * ( X(i) - X(i_left) ) + UP(i_left)
    !     ! UN2(i) = ( UN2(i_right) - UN2(i_left) )/( X(i_right) - X(i_left) ) * ( X(i) - X(i_left) ) + UN2(i_left)

    !     dx = ( X(i) - X(i_left) )

    !     POINT(i,1,1)%eps = ( POINT(i_right,1,1)%eps - POINT(i_left,1,1)%eps )/( X(i_right) - X(i_left) ) * dx  + POINT(i_left,1,1)%eps

    !     semi_id = POINT(i,1,1)%semi_id
    !     alloy   => SEMI(semi_id)

    !     !ec
    !     index   = get_conduction_band_index(alloy)
    !     ec      = ( ec_right  - ec_left  )/( X(i_right) - X(i_left) ) * dx + ec_left
    !     UN(i)   = SEMI(semi_id)%band_pointer(index)%band%e0 - ec

    !     index   = get_conduction_band_index2(alloy)
    !     ec      = ( ec2_right  - ec2_left  )/( X(i_right) - X(i_left) ) * dx + ec2_left
    !     UN2(i)  = SEMI(semi_id)%band_pointer(index)%band%e0 - ec

    !     index   = get_valence_band_index(alloy)
    !     ev      = ( ev_right  - ev_left  )/( X(i_right) - X(i_left) ) * dx + ev_left
    !     UP(i)   = -SEMI(semi_id)%band_pointer(index)%band%e0 - ev

    !     do n=1,size(alloy%band_pointer)
    !         !n_eff
    !         val_left   = SEMI(id_inp)%band_pointer(n)%band%pos_dep_n_eff(i) 
    !         val_right  = SEMI(id_ingaas)%band_pointer(n)%band%pos_dep_n_eff(i) 
    !         alloy%band_pointer(n)%band%pos_dep_n_eff(i)  =  ( val_right  - val_left  )/( X(i_right) - X(i_left) ) * dx + val_left

    !         val_left   = SEMI(id_inp)%band_pointer(n)%band%pos_dep_m_eff(i) 
    !         val_right  = SEMI(id_ingaas)%band_pointer(n)%band%pos_dep_m_eff(i) 
    !         alloy%band_pointer(n)%band%pos_dep_m_eff(i)  =  ( val_right  - val_left  )/( X(i_right) - X(i_left) ) * dx + val_left

    !         val_left   = SEMI(id_inp)%band_pointer(n)%band%pos_dep_al(i) 
    !         val_right  = SEMI(id_ingaas)%band_pointer(n)%band%pos_dep_al(i) 
    !         alloy%band_pointer(n)%band%pos_dep_al(i)  =  ( val_right  - val_left  )/( X(i_right) - X(i_left) ) * dx + val_left
    !     enddo

    ! enddo



    ! do k=1,NZ1
    !     do j=1,NY1
    !         do i=1,NX1
    !             if (POINT(i,j,k)%eps.eq.0) then
    !                 if (.not.(POINT(i,j,k)%cont)) then
    !                     write(*,*) '***error*** found point with eps=0, this is a bug.'
    !                     stop
    !                 endif
    !             endif
    !         enddo
    !     enddo
    ! enddo
    ! HETERO = .false.
    ! if (.not.(all(UN.eq.0).and.all(UP.eq.0))) then
    HETERO = .true. !historical: we not always use heterostructure, since this makes the code easier and removes case distinctions.
    !   write(*,*) 'heterostructure found'
    ! endif

endsubroutine

function bow_real(para_A, para_B, bowing_para, grading) result (para)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !implementation of the bowing function for alloys
    !
    ! input
    ! -----
    ! para_A : real(8)
    !   Parameter value in material A.
    ! para_A : real(8)
    !   Parameter value in material B.
    ! bowing_para : real(8)
    !   Nonlinear bowing parameter.
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! para : real(8)
    !   The bowed parameter.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: para_a, para_b, bowing_para, grading
    real(8) :: para
    if (grading.eq.0) then
        para = para_A
    elseif (grading.eq.1) then
        para = para_B
    else
        para = (1-grading)*para_A + grading*para_B - grading*(1-grading)*bowing_para
    endif
endfunction

function bow_dual(para_A, para_B, bowing_para, grading) result (para)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !implementation of the bowing function for alloys
    !
    ! input
    ! -----
    ! para_A : DUAL_NUM
    !   Parameter value in material A.
    ! para_A : DUAL_NUM
    !   Parameter value in material B.
    ! bowing_para : real(8)
    !   Nonlinear bowing parameter.
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! para : DUAL_NUM
    !   The bowed parameter.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8)        :: grading, bowing_para
    TYPE(DUAL_NUM) :: para_a, para_b
    TYPE(DUAL_NUM) :: para
    if (grading.eq.0) then
        para = para_A
    elseif (grading.eq.1) then
        para = para_B
    else
        para = (1-grading)*para_A + grading*para_B - grading*(1-grading)*bowing_para
    endif
endfunction

! function get_effective_mass_conduction_band(alloy, sp_id) result(meff)
!   !return the effective mass of the conduction band in kg, taking into account grading
!   real(8) :: meff
!   real(8) :: meff_A, meff_B

!   type(semi_prop) :: semi_A, semi_B, alloy
!   integer :: id_materialA, id_materialB

!   if (alloy%is_alloy) then
!     id_materialA = semi%id_materialA
!     id_materialB = semi%id_materialB
!     semi_A       = SEMI(id_materialA)
!     semi_B       = SEMI(id_materialB)
!   endif

!   if (.not.alloy%is_alloy) then
!     index        = get_conduction_band_index(alloy)
!     meff         = alloy%band_pointer(index)%band%me/AM0 !convert back to relative
!   else
!     index        = get_conduction_band_index(semi_A)
!     meff_A       = semi_A%band_pointer(index)%band%me/AM0
!     index        = get_conduction_band_index(semi_B)
!     meff_B       = semi_B%band_pointer(index)%band%me/AM0
!     index        = get_conduction_band_index(alloy)
!     meff         = bow(meff_A, meff_B, alloy%band_pointer(index)%band%me_c, GRADING(sp_id))

!   endif
!   meff = meff*AM0 !convert to kg

! endfunction

! function get_effective_mass_valence_band(alloy, sp_id) result(meff)
!   !return the effective mass of the conduction band in kg, taking into account grading
!   real(8) :: meff
!   real(8) :: meff_A, meff_B

!   type(semi_prop) :: semi_A, semi_B, alloy
!   integer :: id_materialA, id_materialB

!   if (alloy%is_alloy) then
!     id_materialA = semi%id_materialA
!     id_materialB = semi%id_materialB
!     semi_A       = SEMI(id_materialA)
!     semi_B       = SEMI(id_materialB)
!   endif

!   if (.not.alloy%is_alloy) then
!     index        = get_conduction_band_index(alloy)
!     meff         = alloy%band_pointer(index)%band%me/AM0 !convert back to relative
!   else
!     index        = get_conduction_band_index(semi_A)
!     meff_A       = semi_A%band_pointer(index)%band%me/AM0
!     index        = get_conduction_band_index(semi_B)
!     meff_B       = semi_B%band_pointer(index)%band%me/AM0
!     index        = get_conduction_band_index(alloy)
!     meff         = bow(meff_A, meff_B, alloy%band_pointer(index)%band%me_c, GRADING(sp_id))

!   endif
!   meff = meff*AM0 !convert to kg

! endfunction

function get_bandgap_0(semi, ib) result(bandgap_0)
    !returns the bandgap at reference temperature of given semiconductor with respect to valley of band.
    type(semi_prop)   :: semi !semiconductor
    integer           :: ib   !index of band
    integer           :: index_valence = 0
    real(8)           :: bandgap_0,ec_0,ev_0

    !find conduction band energy
    ec_0             = semi%band_pointer(ib)%band%e0

    index_valence    = get_valence_band_index(semi)
    ev_0             = semi%band_pointer(index_valence)%band%e0

    !calculate bandgap
    bandgap_0        = ec_0 + ev_0 !valence band energy is "flipped" in hdev -> same calculations for holes and electrons possible.
endfunction

function bandgap_temp(bandgap_0, alpha, beta, temp) result (bandgap)
    !implementation of the varnishi temperature equation for the bandgap. May be applied to both silicon and III-V semiconductors
    real(8) :: bandgap_0    ! bandgap at 0 Kelvin
    real(8) :: alpha, beta  ! varshni parameters
    real(8) :: temp         ! temperature in Kelvin
    real(8) :: bandgap      ! temperature scaled bandgap
    bandgap = bandgap_0 - alpha*temp**2/(temp+beta)
endfunction

function bandgap_temp_green(bandgap_0, e_1, e_2, e_3, t_t0) result (bandgap)
  !implementation of the varnishi temperature equation for the bandgap. May be applied to both silicon and III-V semiconductors
  real(8) :: bandgap_0    ! bandgap at 0 Kelvin
  real(8) :: e_1, e_2, e_3  ! varshni parameters
  real(8) :: t_t0         ! temperature in Kelvin
  real(8) :: bandgap      ! temperature scaled bandgap

  bandgap = bandgap_0 + e_1*t_t0 + e_2*t_t0**2 + e_3*t_t0**3

endfunction


function calc_bgn_default(is,i,j,k,sp_id,cd_i,temp) result(bgn)
    integer         :: is    !index to semiconductor
    integer         :: i,j,k !index to point
    integer         :: sp_id !index to semi point
    real(8)         :: bgn
    real(8)         :: cd_i !net doping concentration at point
    real(8)         :: temp !temperature at point
    real(8)         :: ec,ev,ef
    real(8)         :: nu  !ec-ef/vt
    integer         :: index
    real(8)         :: fermi_05,fermi_m05,eps_r,c_k,beta,beta_sq

    !TU WIEN MODEL
    !find ef, such that density=cd_i
    bgn = 0
    if (cd_i.gt.0) then
        ef    = get_fermi_equ(is,cd_i,sp_id,UN(i),UP(i),UN2(i)) !fermi level before bgn
        index = get_conduction_band_index(SEMI(is))
        ec    = SEMI(is)%band_pointer(index)%band%e0 - UN(sp_id)
        index = get_valence_band_index(SEMI(is))
        ev    = SEMI(is)%band_pointer(index)%band%e0 - UP(sp_id)
        nu    = (ef-ec)/VT_LATTICE

        !evaluate fermi integral
        fermi_05   = fd1h(nu)
        fermi_m05  = dfd1h(nu)

        !read out variables
        eps_r      = POINT(i,j,k)%eps

        beta_sq    = cd_i*Q*Q*fermi_m05/fermi_05/EP0/eps_r/BK/temp
        beta       = sqrt(beta_sq)
        c_k        = 0.24
        ! alpha_0    = 0.53e-10
        ! alpha_I    = Z_I**(1/3)/c_k/alpha_0/1*(1-2*(Z_I/N_I))/(5/3-4*(Z_I/N_I)**(1/3))
        ! alpha_SEMI = Z_SEMI**(1/3)/c_k/alpha_0/1*(1-2*(Z_SEMI/N_SEMI))/(5/3-4*(Z_SEMI/N_SEMI)**(1/3))
        bgn        = -Q*Q*beta/4/PI/EP0/eps_r!*( (beta*N_I)/(beta+alpha_I) - (beta*N_SEMI)/(beta+alpha_SEMI) +  Z_I - N_I )
        bgn        = bgn/1.602e-19 !conversion to eV
    endif
endfunction

function calc_bgn_classic(is,cd_i,temp) result(bgn)
  integer         :: is    !index to semiconductor
  real(8)         :: bgn
  real(8)         :: cd_i !net doping concentration at point
  real(8)         :: temp !temperature at point

  !Slotboom model
  bgn = 0
  if (cd_i.gt.0) then
      bgn = SEMI(is)%v_hd0*(1+SEMI(is)%bgn_alpha*(temp-SEMI(is)%temp0))
      if (SEMI(is)%c_ref.ne.0) then
          bgn = bgn * (log(cd_i/SEMI(is)%c_ref) + sqrt(log(cd_i/SEMI(is)%c_ref)**2 + SEMI(is)%c_hd))
      else
          bgn = 0
      endif
  endif

endfunction

function calc_bgn_jain(is,don_i,acc_i,grad) result(bgn)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate the band-gap-narrowing according to Jain modl. For
    ! alloy semiconductors, interpolate the parameters.
    !
    ! Input
    ! -----
    ! is : integer
    !   Id of the semiconductor whose BGN shall be calculated.
    ! don_i : real(8)
    !   The Donor concentration.
    ! acc_i : real(8)
    !   The Acceptor concentration.
    ! grad : real(8)
    !   The grading at the point.
    !
    ! Returns
    ! -------
    ! bgn : real(8)
    !   The BGN value according to the model.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer         :: is    
    real(8)         :: bgn,dop_i
    real(8),intent(in) :: don_i 
    real(8),intent(in) :: acc_i 
    real(8),intent(in) :: grad 
    integer :: id_para = 0
    real(8) :: bgn_a,bgn_b,bgn_c

    type(semi_prop),pointer :: semi_A,semi_B
        
    if (don_i.gt.acc_i) then
        id_para = 1
        dop_i = don_i
    else
        id_para = 2
        dop_i = acc_i
    endif

    if (SEMI(is)%is_alloy) then !alloy, interpolate parameters
        semi_A       => SEMI(SEMI(is)%id_materialA)
        semi_B       => SEMI(SEMI(is)%id_materialB)
        bgn_a = bow(semi_A%bgn_a(id_para),semi_B%bgn_a(id_para), dble(0), grad)
        bgn_b = bow(semi_A%bgn_b(id_para),semi_B%bgn_b(id_para), dble(0), grad)
        bgn_c = bow(semi_A%bgn_c(id_para),semi_B%bgn_c(id_para), dble(0), grad)
 
    else ! pristine semicondutor
        bgn_a = SEMI(is)%bgn_a(id_para)
        bgn_b = SEMI(is)%bgn_b(id_para)
        bgn_c = SEMI(is)%bgn_c(id_para)
 
    endif
 
    bgn =       bgn_a*    (dop_i*1e-6)**(dble(1./3.)) 
    bgn = bgn + bgn_b*    (dop_i*1e-6)**(dble(1./4.)) 
    bgn = bgn + bgn_c*sqrt(dop_i*1e-6)

endfunction

end module


