!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Parameters for hdev output
module save_global

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use read_user, only : US                   !< all user parameters
use m_strings
use utils      , only : string                 !< cast string to upper

implicit none

! --- global module parameters ----------------------------------------------------------------------------------------
logical              :: WIN           !< if true: hdev runs on windows
character(len=10000) :: FILE          !< file name for putput
character(len=1024)  :: VERSION       !< program version number
integer              :: OP_REC        !< actual save op number
integer              :: OP_OFFSET     !< op offset
integer              :: OP_STEP       !< op step
logical              :: SAVE_OP
character(len=9)     :: FORMAT_REAL   !< output format for real numbers
character(len=3)     :: LEN_REAL      !< number of charaters for real output format
integer              :: INP_ID        !< file id for input file saving
integer              :: DEB_ID        !< file id for debug file
integer              :: FILE_ID       !< counter for file id's
real(8)              :: TCPU_OP       !< time at start of operation point
real(8)              :: TCPU_START    !< time at start of simulation

private

! ---> public variables -----------------------------------------------------------------------------------------------
public INP_ID
public DEB_ID
public FILE
public VERSION
public FORMAT_REAL
public SAVE_OP
public OP_REC
public TCPU_OP
public TCPU_START
public save_elpa

! ---> public subroutines/ functions ----------------------------------------------------------------------------------
public init_saveglobal
public set_op_rec_number
public write_colnames
public add_str_int
public get_file_id
public set_filename
public write_elpa_value
public write_parameter_real
public write_parameter_int
public write_parameter_char
public read_parameter_real
public read_parameter_int
public read_parameter_char
public get_int_len
public format_int
public format_int_vec
public write_strunit
public welcome_screen
public write_vec
public write_charvec1
public write_charvec2
public get_valnorm
public get_varout
public read_colnames
public get_col_id
public close_saveglobal

contains

subroutine init_saveglobal
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize the saving parameters
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,dimension(8) :: time
    character(len=1024)  :: name,path
    character(len=10000) :: sim_name,outp_path
    character(len=1)     :: path_separator
    character(len=1024)  :: fdat
    integer              :: io,digits
    logical              :: isok

    ! --> set hdev version for output
    VERSION = ''
    write(VERSION,'(A)') 'hdev_v032'

    ! --> windows or unix
    CALL GET_ENVIRONMENT_VARIABLE('PATH',path)
    if (path(3:3).eq.'\') then
        WIN            = .true.
        path_separator = '\'

    else
        WIN            = .false.
        path_separator = '/'

    endif! --> operation point numbering
    OP_OFFSET = 0
    OP_STEP   = 1

    ! --> in case of multi-input simulations, save material properties only for first op
    SAVE_OP = OP_OFFSET.eq.0

    ! --> user input
    write(path,'(A)') US%output%path
    write(name,'(A)') US%output%name
    call set_str_separator(path,WIN)
    call check_str_separator(name, isok)
    if (.not.isok) then
        write(*,*) '***error*** output file name contains invalid characters'
        stop
    endif
    call set_str_ending_blank(path)

    ! --> simulation name
    write(sim_name,'(A)') trim(name)

    ! --> mkdir output path
    if (len_trim(path).gt.0) then
        write(outp_path,'(A,A,A)') trim(path), path_separator, trim(sim_name)
        write(FILE,'(A,A,A,A,A)') trim(path),path_separator,trim(sim_name),path_separator,trim(sim_name)
    else
        write(outp_path,'(A)') trim(sim_name)
        write(FILE,'(A,A,A)') trim(sim_name),path_separator,trim(sim_name)
    endif
    call mkdir_str(outp_path,WIN)

    ! --> init file id
    FILE_ID = 1000

    ! --> open debug file
    call DATE_AND_TIME(values=time)
    DEB_ID = get_file_id()-time(7)
    write(fdat,'(A)') trim(FILE)
    call set_filename(fdat,'op',1,'_debug.out')
    open(DEB_ID,file=trim(fdat),status='replace',action='write',iostat=io)
    write(*,'(A,A)') ' Debug file     : ',trim(fdat)
    write(*,'(A)') ''

    ! --> filename for user file reading
    INP_ID = get_file_id()
    write(fdat,'(A)') trim(FILE)
    call set_filename(fdat,'op',1,'_input.inp')
    open(INP_ID,file=trim(fdat),status='replace',action='write',iostat=io)

    ! --> set number of digits for output
    digits = US%output%digits
    if (digits.lt. 6) digits=6
    if (digits.gt.16) digits=16
    if (digits.lt.10) then
        write(FORMAT_REAL,'(A,I2,A,I1,A)') 'ES',digits+9,'.',digits,'E3 '

    else
        write(FORMAT_REAL,'(A,I2,A,I2,A)') 'ES',digits+9,'.',digits,'E3'

    endif
    write(LEN_REAL,'(A,I2)') 'A',digits+9

    ! --> output parameters
    call outp_saveglobal(DEB_ID)

endsubroutine

subroutine set_op_rec_number(i)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set operation point saving number
    !
    ! input
    ! -----
    ! i : integer 
    !   Actual operating point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in) :: i !< actual operation point

    OP_REC  = OP_OFFSET + i*OP_STEP  ! actual saving record number
endsubroutine

function get_file_id() result(id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! return new file id for user output
    !
    ! returns
    ! -------
    ! id : integer
    !   Get the id of the next output file.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: id

    FILE_ID = FILE_ID + 1
    id      = FILE_ID
endfunction

subroutine write_colnames(str,col_name,first)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! add column name in string with column name line for elpa file
    !
    ! input
    ! ----- 
    ! str : character(len=*)
    !   string that contains all colnames.
    ! col_name : character(len=*)
    !   new column name.
    ! first : logical
    !   If True: this is the first entry.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=*),intent(inout)          :: str
    character(len=*),intent(in   )          :: col_name
    logical,         intent(in   ),optional :: first

    logical :: is_first
    integer :: n,m,i,len

    if (present(first)) then
        is_first = first
    else
        is_first = .false.
    endif

    ! --> add append column name to existing column names
    if (is_first) then
        if (len_trim(col_name).lt.17) then
            write(str,'('//LEN_REAL//')') trim(col_name)
            write(str(1:1),'(I1)') 1
            
        else
            write(str,'(I1,A,A)') 1,' ',trim(col_name)
            
        endif

    else
        read(str(1:1),'(I1)') m
        do i=2,5
            read(str(1:i),'(I'//char(48+i)//')') n
            if (m.eq.n) exit
            m=n
        enddo
        n   = n+1 ! number of column names
        len = get_int_len(n)
        write(str(1:len),'(I'//char(48+len)//')') n
        if (len_trim(col_name).lt.18) then
            write(str,'(A,'//LEN_REAL//')') trim(str),trim(col_name)
          
        else
            write(str,'(A,A,A)') trim(str),' ',trim(col_name)
          
        endif
    endif

endsubroutine

subroutine write_elpa_value(fid,value,islast)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write value to file
    !
    ! input
    ! -----
    ! fid : integer
    !   File id of the file to write to.
    ! value : real(8)
    !   The value to write.
    ! islast : logical
    !   If True: last value in line.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in)          :: fid    !< file id
    real(8),intent(in)          :: value  !< value to write
    logical,intent(in),optional :: islast !< if true: last value in line

    real(8) :: val

    val = value

    ! d_qnan   = TRANSFER((/ Z'00000000', Z'7FF80000' /),1.0_8)
    ! infinity = HUGE(val)
    ! if ((value.gt.infinity).or.(isnan(value)))  val=d_qnan
    ! if ((value.lt.-infinity).or.(isnan(value))) val=d_qnan


    ! --> add value and make new line if needed
    if (present(islast)) then
        if (islast) then
            write(fid,'('//FORMAT_REAL//')',advance='yes') val

        else
            write(fid,'('//FORMAT_REAL//')',advance='no') val

        endif

    else
        write(fid,'('//FORMAT_REAL//')',advance='no') val

    endif
endsubroutine

subroutine write_parameter_real(fid,name,value)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write parameter name ,(real value)
    !
    ! input
    ! -----
    ! fid : integer
    !   File id to write to.
    ! name : character(len=*)
    !   Name of the parameter.
    ! real(8) : value
    !   Value of the parameter.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in) :: fid    !< file id
    character(len=*),intent(in) :: name   !< name of parameter
    real(8),         intent(in) :: value  !< value of parameter

    write(fid,'(A,A,'//FORMAT_REAL//',A)',advance='no') trim(name),'=',value,' '
endsubroutine

subroutine write_parameter_int(fid,name,value)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write parameter name pair with integer value
    !
    ! input
    ! -----
    ! fid : integer
    !   File id to write to.
    ! name : character(len=*)
    !   Name of the parameter.
    ! value : integer
    !   Value of the parameter.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in) :: fid    !< file id
    character(len=*),intent(in) :: name   !< name of parameter
    integer,         intent(in) :: value  !< value of parameter

    write(fid,'(A,A,'//format_int(value)//',A)',advance='no') trim(name),'=',value,' '
endsubroutine

subroutine write_parameter_char(fid,name,value)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write parameter that has a char value
    !
    ! input
    ! -----
    ! fid : integer
    !   File id to write to.
    ! name : character(len=*)
    !   Name of the parameter.
    ! value : character(len=*)
    !   Value of the parameter.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in) :: fid
    character(len=*),intent(in) :: name
    character(len=*),intent(in) :: value

    write(fid,'(A,A,A,A)',advance='no') trim(name),'=',trim(value),' '
endsubroutine

function get_int_len(n) result(len)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get the number of digits needed for integer outpt
    !
    ! input
    ! -----
    ! n : integer
    !   Integer to consider.
    !
    ! result
    ! ------
    ! len : integer
    !   Nuber of digits.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n
    integer :: len

    if     (abs(n).lt.1e1) then
        len=1
      
    elseif (abs(n).lt.1e2) then
        len=2
      
    elseif (abs(n).lt.1e3) then
        len=3
      
    elseif (abs(n).lt.1e4) then
        len=4

    elseif (abs(n).lt.1e5) then
        len=5

    elseif (abs(n).lt.1e6) then
        len=6

    elseif (abs(n).lt.1e7) then
        len=7

    elseif (abs(n).lt.1e8) then
        len=8

    elseif (abs(n).lt.1e9) then
        len=9
      
    else
        len=10
      
    endif

    if (n.lt.0) len=len+1
endfunction

function format_int(n) result(str)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get the output format for integer numbers
    !
    ! input
    ! -----
    ! n : integer
    !   The integer to consider.
    !
    ! output
    ! ------
    ! str : character(len=3)
    !   The output format.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer          :: n
    character(len=3) :: str

    integer :: m

    m = get_int_len(n)
    if (m.ge.10) then
        write(str,'(A)')  'I1'//char(48+m-10)

    else
        write(str,'(A)')  'I'//char(48+get_int_len(n))

    endif

endfunction

subroutine save_elpa(elpa_name, elpa_cols, elpa_vals, wr_op)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! function that stores an elpa file
    !
    ! input
    ! -----
    ! elpa_name : character(len=*)
    !   Name of the file to create.
    ! elpa_cols : type(string),dimension(:)
    !   The names of the columns.
    ! elpa_vals : real(8),dimension(:,:)
    !   For every column, one 1D array of values. Hence, a 2D array.
    ! wr_op : logical
    !   If true, add operating point number to file
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in),optional                   :: wr_op
    integer                                       :: n,io,i_max,i,fid
    character(len=*), intent(in)                  :: elpa_name
    type(string),dimension(:),intent(in)          :: elpa_cols !columns of elpa file
    real(8),dimension(:,:),allocatable,intent(in)     :: elpa_vals !values of elpa file
    character(len=15)                             :: tmp_str
    character(len=10000)                          :: header          !< column names for iv-file
    character(len=1024)                           :: fdat
    logical                   :: wr_op_

    wr_op_ = .True.
    if (present(wr_op)) wr_op_ = wr_op

    ! --> open file -------
    fdat = FILE
    fid  = get_file_id()
    if (wr_op_) then
        call set_filename(fdat,'op',OP_REC)
    endif
    write(fdat,'(A,A,A)')  trim(fdat),trim(elpa_name),trim(".elpa")
    open(fid,file=trim(fdat),status='replace',action='write',form='formatted',iostat=io)
    ! --> first line -------
    call write_parameter_char(fid, 'hdev by M.Mueller version',VERSION)
    write(fid,'(A)',advance='yes') '/'

    !write column names
    do i=1,size(elpa_cols)
        tmp_str = trim(elpa_cols(i)%str)
        if (tmp_str.eq.'') then
            i_max = i
            exit
        endif
        if (i.eq.1) then
            call write_colnames(header,tmp_str,.true.)
        else
            call write_colnames(header,tmp_str)
        endif
    enddo

    write(fid,'(A)') trim(header)

    !write values
    do n=1,size(elpa_vals,2)
        do i=1,size(elpa_cols)
            if (i.eq.i_max) exit
            call write_elpa_value(fid, elpa_vals(i,n))
        enddo
        write(fid,'(A)',advance='yes') ''
    enddo
    close(fid)

endsubroutine

function format_int_vec(n) result(str)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get output format for integer vectors
    !
    ! input
    ! -----
    ! n : integer
    !   Integer whose format is sought.
    !
    ! output
    ! -----
    ! str :character(len=16)
    !   Format.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,dimension(:) :: n
    character(len=16)     :: str

    integer :: m,len,i,p


    len = max(1,size(n,1))
    m   = get_int_len(len)
    p   = 0
    do i=1,m
        p        = p+1
        str(p:p) = char(48+floor(dble(len)/dble(10**(m-i))))
        len      = modulo(len,10**(m-i))
    enddo
    p          = p+1
    str(p:p)   = 'I'

    m = get_int_len(maxval(abs(n)))+2

    if (m.ge.10) then
        str(p+1:p+1) = '1'
        str(p+2:p+2) = char(48+m-10)
        p            = p+2

    else
        str(p+1:p+1) = char(48+m)
        p            = p+1

    endif

    do i=p+1,16
        str(i:i) = char(32)
    enddo

endfunction

subroutine set_filename(fdat,fterm,ct,fend)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! set the filename
  !
  ! input
  ! -----
  ! fdat : character(len=*)
  !   file name first part
  ! fterm : character(len=*)
  !   additional string
  ! ct : integer
  !   number of file.
  ! fend : character(len=*),optional
  !   File ending
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  character(len=*),         intent(inout) :: fdat   !< file name first part
  character(len=*),         intent(in)    :: fterm  !< additional string
  integer,                  intent(in)    :: ct     !< number
  character(len=*),optional,intent(in)    :: fend   !< file ending

  write(fdat,'(A,A,A,'//format_int(ct)//')') trim(fdat),'_',trim(fterm),ct

  if (present(fend)) then
      write(fdat,'(A,A,A)') trim(fdat),trim(fend)
  endif

endsubroutine

function add_str_int(str,n) result(str1)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Add number at the end of a string.
    ! input
    ! -----
    ! str : character(len=*)
    !   Add a number at the end of a string.
    ! output
    ! ------
    ! str1 : character(len=*)
    !   The resulting string.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=*)   :: str
    integer            :: n
    character(len=128) :: str1

    write(str1,'(A)') trim(str)
    write(str1,'(A,'//format_int(n)//')') trim(str1),n

endfunction

subroutine write_charvec1(fv,cv,fid,col)
    ! output a 1D vector oc characers 
    ! TODO
    character(len=*),dimension(:) :: cv
    character(len=*)              :: fv
    integer                       :: fid
    logical                       :: col

    integer :: n,i

    n = size(cv)
    if (col) then
        write(fid,'(A,A)',advance='no') trim(fv),'='
        do i=1,n
            if (len_trim(cv(i)).gt.0) then
                write(fid,'(A,A)',advance='no') trim(cv(i)),'    '
            endif
        enddo
        write(fid,'(A)',advance='yes') ''
        flush(fid)

    else
        do i=1,n
            write(fid,'(A,A,'//format_int(i)//',A,A)') trim(fv),'(',i,')=',trim(cv(i))
            flush(fid)
        enddo
      
    endif

endsubroutine

subroutine write_charvec2(fv,cv,id,col)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! output of 2dim vector of characters
    ! TODO
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=*)                :: fv
    character(len=*),dimension(:,:) :: cv
    integer                         :: id
    logical                         :: col

    integer :: n,m,i,j

    n = size(cv,1)
    m = size(cv,2)

    if (col) then
        do i=1,n
            do j=1,m
                if (len_trim(cv(i,j)).gt.0) then
                    write(id,'(A,A)',advance='no') trim(cv(i,j)),'    '
                endif
            enddo
        enddo
        write(id,'(A)',advance='yes') ''
        flush(id)
      
    else
        do i=1,n
            do j=1,m
                if ((i.lt.100).and.(j.lt.100).and.(len_trim(cv(i,j)).gt.0)) then
                    write(id,'(A,A,I2,A,I2,A,A)') trim(fv),'(',i,',',j,')=',trim(cv(i,j))
                else
                endif
                flush(id)
            enddo
        enddo
      
    endif

endsubroutine

subroutine write_strunit(id,str,vec,unit)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write unit
    !TODO
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer          :: id
    character(len=*) :: str
    real(8)          :: vec
    character(len=*) :: unit

    if (vec.eq.0) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec      ,' ',trim(unit)

    elseif ((abs(vec*1e-18).ge.0.99).and.(abs(vec*1e-18).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-18,'E',trim(unit)

    elseif ((abs(vec*1e-15).ge.0.99).and.(abs(vec*1e-15).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-15,'P',trim(unit)

    elseif ((abs(vec*1e-12).ge.0.99).and.(abs(vec*1e-12).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-12,'T',trim(unit)

    elseif ((abs(vec*1e-9 ).ge.0.99).and.(abs(vec*1e-9 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-9 ,'G',trim(unit)

    elseif ((abs(vec*1e-6 ).ge.0.99).and.(abs(vec*1e-6 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-6 ,'M',trim(unit)

    elseif ((abs(vec*1e-3 ).ge.0.99).and.(abs(vec*1e-3 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e-3 ,'k',trim(unit)

    elseif ((abs(vec*1e0  ).ge.0.99).and.(abs(vec*1e0  ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec      ,' ',trim(unit)

    elseif ((abs(vec*1e3  ).ge.0.99).and.(abs(vec*1e3  ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e3  ,'m',trim(unit)

    elseif ((abs(vec*1e6  ).ge.0.99).and.(abs(vec*1e6  ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e6  ,'u',trim(unit)

    elseif ((abs(vec*1e9  ).ge.0.99).and.(abs(vec*1e9  ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e9  ,'n',trim(unit)

    elseif ((abs(vec*1e12 ).ge.0.99).and.(abs(vec*1e12 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e12 ,'p',trim(unit)

    elseif ((abs(vec*1e15 ).ge.0.99).and.(abs(vec*1e15 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e15 ,'f',trim(unit)

    elseif ((abs(vec*1e18 ).ge.0.99).and.(abs(vec*1e18 ).lt.1e3)) then
      write(id,'(A,A,F8.3,A,A)') trim(str),' ',vec*1e18 ,'a',trim(unit)

    else
      write(id,'(A,A,E17.9,A)' ) trim(str),' ',vec          ,trim(unit)

    endif

endsubroutine

subroutine get_varout(vec,digits, vec_norm,norm,str)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get normalized value TODO
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in)           :: vec
    integer,intent(in)           :: digits
    real(8),intent(out)          :: vec_norm
    character(len=1),intent(out) :: norm
    character(len=8),intent(out) :: str

    integer :: dig

    call get_valnorm(vec, vec_norm,norm,dig)
    if (dig.eq.0) then
        write(str,'(A2,'//format_int(digits+7)//',A1,'//format_int(digits-1)//',A2)')   'ES',digits+7,'.',digits-1,'E2'

    else
        write(str,'(A1,'//format_int(digits+2)//',A1,'//format_int(digits-dig)//')') 'F',digits+2,'.',digits-dig

    endif

endsubroutine

subroutine get_valnorm(vec, vec_norm,norm,dig)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get normalized value TODO
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in)           :: vec
    real(8),intent(out)          :: vec_norm
    character(len=1),intent(out) :: norm
    integer,intent(out)          :: dig

    if (vec.eq.0) then
        vec_norm = vec
        norm     = ' '

    elseif ((abs(vec*1e-18).ge.0.99).and.(abs(vec*1e-18).lt.1e3)) then
        vec_norm = vec*1e-18
        norm     = 'E'

    elseif ((abs(vec*1e-15).ge.0.99).and.(abs(vec*1e-15).lt.1e3)) then
        vec_norm = vec*1e-15
        norm     = 'P'

    elseif ((abs(vec*1e-12).ge.0.99).and.(abs(vec*1e-12).lt.1e3)) then
        vec_norm = vec*1e-12
        norm     = 'T'

    elseif ((abs(vec*1e-9 ).ge.0.99).and.(abs(vec*1e-9 ).lt.1e3)) then
        vec_norm = vec*1e-9
        norm     = 'G'

    elseif ((abs(vec*1e-6 ).ge.0.99).and.(abs(vec*1e-6 ).lt.1e3)) then
        vec_norm = vec*1e-6
        norm     = 'M'

    elseif ((abs(vec*1e-3 ).ge.0.99).and.(abs(vec*1e-3 ).lt.1e3)) then
        vec_norm = vec*1e-3
        norm     = 'k'

    elseif ((abs(vec*1e0  ).ge.0.99).and.(abs(vec*1e0  ).lt.1e3)) then
        vec_norm = vec
        norm     = ' '

    elseif ((abs(vec*1e3  ).ge.0.99).and.(abs(vec*1e3  ).lt.1e3)) then
        vec_norm = vec*1e3
        norm     = 'm'

    elseif ((abs(vec*1e6  ).ge.0.99).and.(abs(vec*1e6  ).lt.1e3)) then
        vec_norm = vec*1e6
        norm     = 'u'

    elseif ((abs(vec*1e9  ).ge.0.99).and.(abs(vec*1e9  ).lt.1e3)) then
        vec_norm = vec*1e9
        norm     = 'n'

    elseif ((abs(vec*1e12 ).ge.0.99).and.(abs(vec*1e12 ).lt.1e3)) then
        vec_norm = vec*1e12
        norm     = 'p'

    elseif ((abs(vec*1e15 ).ge.0.99).and.(abs(vec*1e15 ).lt.1e3)) then
        vec_norm = vec*1e15
        norm     = 'f'

    elseif ((abs(vec*1e18 ).ge.0.99).and.(abs(vec*1e18 ).lt.1e3)) then
        vec_norm = vec*1e18
        norm     = 'a'

    elseif ((abs(vec*1e21 ).ge.0.99).and.(abs(vec*1e21 ).lt.1e3)) then
        vec_norm = vec*1e21
        norm     = 'z'

    else
        vec_norm = vec
        norm     = ' '

    endif

    if ((abs(vec_norm).ge.9.99).and.(abs(vec_norm).lt.99)) then
        dig=2

    elseif ((abs(vec_norm).ge.99).and.(abs(vec_norm).lt.999)) then
        dig=3

    elseif (abs(vec_norm).ge.999) then
        dig=0

    else
        dig=1

    endif

endsubroutine


subroutine write_vec(vec,sgn,str)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write vector TODO
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in)          :: vec
    logical,intent(in)          :: sgn
    character(len=*),intent(in) :: str

    real(8)          :: vec_norm
    character(len=1) :: norm
    integer          :: dig = 0

    call get_valnorm(vec, vec_norm,norm,dig)

    if (sgn) then
        if     (dig.eq.0) then
            write(6,'(A7,A$)') ' ***** ',str
        elseif     (dig.eq.1) then
            write(6,'(F6.3,A,A$)') vec_norm,norm,str
        elseif (dig.eq.2) then
            write(6,'(F6.2,A,A$)') vec_norm,norm,str
        elseif (dig.eq.3) then
            write(6,'(F6.1,A,A$)') vec_norm,norm,str
        endif
    else
        if     (dig.eq.0) then
            write(6,'(A6,A$)') '***** ',str
        elseif (dig.eq.1) then
            write(6,'(F5.3,A,A$)') vec_norm,norm,str
        elseif (dig.eq.2) then
            write(6,'(F5.2,A,A$)') vec_norm,norm,str
        elseif (dig.eq.3) then
            write(6,'(F5.1,A,A$)') vec_norm,norm,str
        endif
    endif

endsubroutine


subroutine set_str_separator(str,win)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set correct folder separator for unix or windows
    !
    ! input
    ! -----
    ! str : charater(len=*)
    !   The string to adjust.
    ! win : logical
    !   If true, assume windows os. Else Linux.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=*) :: str
    logical          :: win

    integer :: n,i

    n = len_trim(str)

    do i=1,n
        if (win) then
            if (str(i:i).eq.'/') then
                str(i:i) = '\'
            endif

        else
            if (str(i:i).eq.'\') then
                str(i:i) = '/'
            endif

        endif

    enddo

endsubroutine

subroutine set_str_ending_blank(str)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! set folder separator
  !
  ! input
  ! -----
  ! str : character(len=*)
  !   The string to analyze.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  character(len=*) :: str

  integer :: n

  n = len_trim(str)

  if (n.gt.0) then
      if (str(n:n).eq.'/') then
          str(n:n) = ' '
      elseif (str(n:n).eq.'\') then
          str(n:n) = ' '
      endif
  endif
endsubroutine

subroutine check_str_separator(str,isok)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! check if folder separator in string
  !
  ! input
  ! -----
  ! str : character(len=*)
  !   the string to check
  ! isok : logical
  !   True if no separator in string.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  character(len=*) :: str
  logical          :: isok

  integer :: n,i

  n    = len_trim(str)
  isok = .true.

  do i=1,n
      if (str(i:i).eq.'/') then
          isok = .false.
      endif

      if (str(i:i).eq.'\') then
          isok = .false.
      endif

  enddo

endsubroutine

subroutine mkdir_str(str,win)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! make new directory
    !
    ! input
    ! -----
    ! str : character(len=*)
    !   The path to the directory.
    ! win : logical
    !   If true: on windows os, else linux.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=*) :: str
    logical          :: win

    integer :: n,i

    if (win) then
      call system('if not exist '// trim(str)//' mkdir '// trim(str))

    else
      n = len_trim(str)
      do i=2,n
        if (str(i:i).eq.'/') then
          call system('mkdir '// trim(str(1:i-1)))
        endif
      enddo
      call system('mkdir '// trim(str))
    endif

endsubroutine

subroutine read_parameter_real(fid,para_name,para_value,success)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read real parameter from first line of elpa file
    !
    ! input
    ! -----
    ! fid       : integer
    !   The fid to read from.
    ! para_name : character(len=*)
    !   The parameter to read.
    ! para_value: real(8) => OUTPUT TOO
    !   The value of the parameter
    ! success   : logical
    !   =true if reading worked.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in)  :: fid
    character(len=*),intent(in)  :: para_name
    real(8),         intent(out) :: para_value
    logical,         intent(out) :: success

    integer             :: j,idx
    character(len=32)   :: name
    character(len=32)   :: name_read
    character(len=1000) :: line


    success=.false.
    write(name_read,'(A)') trim(para_name)
    rewind(1)
    read(1,'(A1000)') line
    idx=0
    do
        do
            idx=idx+1
            if (idx.gt.1000) exit
            j=scan(line(idx:),' ')
            if (j.ne.1) then
                exit
            endif
        enddo
        name = line(idx:idx+j-2)
        
        if (name(1:len_trim(name_read)).eq.name_read(1:len_trim(name_read))) then
            do
                idx=idx+1
                j=scan(line(idx:),'=')
                if (j.ne.1) then
                    exit
                endif
            enddo
            idx = idx+j-1
            do
                idx=idx+1
                j=scan(line(idx:),' ')
                if (j.ne.1) then
                    exit
                endif
            enddo
            read(line(idx:idx+j-1),*) para_value
            success = .true.
            exit
          
        else
            idx  = idx+j-1
          
        endif
        if (idx.gt.1000) exit
    enddo

endsubroutine

subroutine read_parameter_int(fid,para_name,para_value,success)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read integer parameter from first line of elpa file
    !
    ! input
    ! -----
    ! fid : integer
    !   Fid to read from.
    ! para_name : character(len=*)
    !   The name of the parameter
    ! para_value : integer =>OUTPUT
    !   The read value.
    ! success : logical
    !   True if reading worked.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in)  :: fid
    character(len=*),intent(in)  :: para_name
    integer,         intent(out) :: para_value
    logical,         intent(out) :: success

    integer             :: j,idx
    character(len=32)   :: name
    character(len=32)   :: name_read
    character(len=1000) :: line

    success=.false.
    write(name_read,'(A)') trim(para_name)
    rewind(1)
    read(1,'(A1000)') line
    idx=0
    do
        do
            idx=idx+1
            if (idx.gt.1000) exit
            j=scan(line(idx:),' ')
            if (j.ne.1) then
                exit
            endif
        enddo
        name = line(idx:idx+j-2)
        
        if (name(1:len_trim(name_read)).eq.name_read(1:len_trim(name_read))) then
            do
                idx=idx+1
                j=scan(line(idx:),'=')
                if (j.ne.1) then
                    exit
                endif
            enddo
            idx = idx+j-1
            do
                idx=idx+1
                j=scan(line(idx:),' ')
                if (j.ne.1) then
                    exit
                endif
            enddo
            read(line(idx:idx+j-1),*) para_value
            success = .true.
            exit
          
        else
            idx  = idx+j-1
          
        endif
        if (idx.gt.1000) exit
    enddo

endsubroutine

subroutine read_parameter_char(fid,para_name,para_value,success)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read real parameter from first line of elpa file
    !
    ! input
    ! -----
    ! fid       : integer
    !   The fid to read from.
    ! para_name : character(len=*)
    !   The name of the parameter to read.
    ! para_value: real(8) => OUTPUT
    !   The read value.
    ! success   : logical
    !   True, if reading worked.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,         intent(in)  :: fid
    character(len=*),intent(in)  :: para_name
    character(len=*),intent(out) :: para_value
    logical,         intent(out) :: success

    integer             :: j,idx
    character(len=32)   :: name
    character(len=32)   :: name_read
    character(len=1000) :: line

    success=.false.
    write(name_read,'(A)') trim(para_name)
    rewind(1)
    read(1,'(A1000)') line
    idx=0
    do
        do
            idx=idx+1
            if (idx.gt.1000) exit
            j=scan(line(idx:),' ')
            if (j.ne.1) then
                exit
            endif
        enddo
        name = line(idx:idx+j-2)
        
        if (name(1:len_trim(name_read)).eq.name_read(1:len_trim(name_read))) then
            do
                idx=idx+1
                j=scan(line(idx:),'=')
                if ((line(idx:idx).eq.'=').or.(j.ne.1)) then
                    exit
                endif
            enddo
            idx = idx+j-1
            do
                idx=idx+1
                j=scan(line(idx:),' ')
                if (j.ne.1) then
                    exit
                endif
            enddo
            read(line(idx+1:idx+j-3),*) para_value ! todo: find ' ' 
            success = .true.
            exit
          
        else
            idx  = idx+j-1
          
        endif
        if (idx.gt.1000) exit
    enddo

endsubroutine

subroutine read_colnames(fid,cols)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read column line from elpa file
    !
    ! input
    ! -----
    ! fid : integer
    !   The file id to read from.
    ! cols : character(len=32),dimension(:),pointer
    !   The found column names.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in)                     :: fid
    character(len=32),dimension(:),pointer :: cols

    integer             :: j,i,ncol,idx
    character(len=1000) :: elpacol

    rewind(1)
    read(1,*)
    read(1,'(I4,A1000)') ncol,elpacol
    allocate(cols(ncol))
    idx=0
    do i=1,ncol
        do
            idx=idx+1
            j=scan(elpacol(idx:),' ')
            if (j.ne.1) then
                exit
            endif
        enddo
        cols(i) = elpacol(idx:idx+j-1)
        idx     = idx+j-1
    enddo

endsubroutine


! ---------------------------------------------------------------------------------------------------------------------
!> \details
function get_col_id(cols,colname) result(idx)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get column id from elpa file column line
    !
    ! input
    ! -----
    ! cols : character(len=32),dimension(:)
    !   The columns of the elpa file, one character(len=32) for every column.
    ! colname : character(len=32)
    !   The name of the column to look for.
    !
    ! output
    ! ------
    ! idx : integer
    !    Id of the column.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=32),dimension(:) :: cols
    character(len=32)               :: colname
    integer                        :: idx

    integer :: i

    idx = 0
    do i=1,size(cols)
        if (cols(i).eq.colname) then
            idx = i
            exit
        endif
    enddo

endfunction

subroutine welcome_screen
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! display hdev welcome screen
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character           :: a=''
    character           :: b=''
    character           :: c=''
    character           :: d=''
    character           :: e=''
    character           :: f=''
    character           :: g=''
    character           :: h=''
    logical             :: win

    ! --> windows or unix
    !CALL GET_ENVIRONMENT_VARIABLE('PATH',path)
    !if (path(3:3).eq.'\') then
    !  win            = .true.

    !else
    win            = .false.


    ! --> welcome screen -------------------------------------
    if (win) then   ! windows (extended ascii from dos)
        a = char(201) ! top left corner
        b = char(205) ! horizontal line
        c = char(186) ! vertical line
        d = char(32)  ! space
        e = char(187) ! top right corner
        f = char(219) ! filled
        g = char(200) ! bottom left corner
        h = char(188) ! bottom right corner

    else ! unix (pure ascii)
        a = char(43)  ! top left corner
        b = char(45)  ! horizontal line
        c = char(124) ! vertical line
        d = char(32)  ! space
        e = char(43)  ! top right corner
        f = char(35)  ! filled
        g = char(43)  ! bottom left corner
        h = char(43)  ! bottom right corner

    endif

    write(*,'(A)')  ''
    !                             1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
    write(*,'(7A,36A)') '       ',a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,e
    write(*,'(7A,36A)') '       ',c,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,f,d,d,f,d,d,d,d,d,d,f,d,d,d,d,f,f,f,f,f,d,d,d,f,d,d,d,f,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,f,d,d,f,d,d,d,d,d,d,f,d,d,d,d,f,d,d,d,f,d,d,d,f,d,d,d,f,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,f,f,f,f,d,d,d,f,f,f,f,d,d,d,d,f,f,f,f,f,d,d,d,f,d,d,d,f,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,f,d,d,f,d,d,d,f,d,d,f,d,d,d,d,f,d,d,d,d,d,d,d,d,f,d,f,d,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,f,d,d,f,d,d,d,f,f,f,f,d,d,d,d,f,f,f,f,f,d,d,d,d,d,f,d,d,d,d,d,c
    write(*,'(7A,36A)') '       ',c,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,c
    write(*,'(7A,36A)') '       ',g,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,h

    write(*,'(50A)')  ''
    write(*,'(50A)')  'Sven Mothes (sven.mothes AT tu-dresden.de)'
    write(*,'(50A)')  'Markus Mueller (Markus.Mueller3 AT tu-dresden.de)'
    write(*,'(50A)')  ''
    !write(*,'(50A)')  'Documentation:'
    !write(*,'(50A)')  'http://eeebcd.et.tu-dresden.de/mc-bte/wiki'
    write(*,'(50A)')  b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
    write(*,'(50A)')  ''

endsubroutine

subroutine outp_saveglobal(fid)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! output parameters from module
    ! fid : integer
    !   Id of the file to output save_global parameters.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in) :: fid !< file id

    write(fid,'(A)'                            ) 'parameters from module SAVE_GLOBAL:'
    write(fid,'(A,L)'                          ) ' WIN           =',WIN
    write(fid,'(A,A)'                          ) ' FILE          = ',trim(FILE)
    write(fid,'(A,A)'                          ) ' VERSION       = ',trim(VERSION)
    write(fid,'(A,'//format_int(INP_ID)//')'   ) ' INP_ID        = ',INP_ID
    write(fid,'(A,'//format_int(DEB_ID)//')'   ) ' DEB_ID        = ',DEB_ID
    write(fid,'(A,'//format_int(FILE_ID)//')'  ) ' FILE_ID       = ',FILE_ID
    write(fid,'(A,'//format_int(OP_OFFSET)//')') ' OP_OFFSET     = ',OP_OFFSET
    write(fid,'(A,'//format_int(OP_STEP)//')'  ) ' OP_STEP       = ',OP_STEP
    write(fid,'(A,L)'                          ) ' SAVE_OP       =',SAVE_OP
    write(fid,'(A,A)'                          ) ' FORMAT_REAL   = ',FORMAT_REAL
    write(fid,'(A,A)'                          ) ' LEN_REAL      = ',LEN_REAL
    write(fid,'(A)'                            ) ''                      

endsubroutine

subroutine close_saveglobal
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! deallocate all pointers of the module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: tcpu_end

    call cpu_time(tcpu_end)
    write(DEB_ID,*) 't_cpu=',tcpu_end-TCPU_START
    close(DEB_ID)

endsubroutine


endmodule