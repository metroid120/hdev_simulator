!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Parameters for hdev output
module hdf_functions

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use m_strings
USE HDF5 !reading and writing to HDF5 files. AWESOME
use hdf5_utils
use utils      , only : string                 !< cast string to upper

implicit none

integer(HID_T) :: HDF5_FILE_ID ! HDF5 File Id

private

! ---> public variables -----------------------------------------------------------------------------------------------
public save_hdf5
public read_col_hdf5
public init_hdf_functions
public close_hdf_functions

contains

subroutine init_hdf_functions
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize the module parameters
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> init HDF5 file
    call hdf_set_print_messages(.false.) !for debugging turn this on
    call hdf_open_file(HDF5_FILE_ID, "simulation_data.h5", STATUS='NEW')
    call hdf_create_group(HDF5_FILE_ID, "inqu")
    call hdf_create_group(HDF5_FILE_ID, "laplace")
    call hdf_create_group(HDF5_FILE_ID, "ac")
    call hdf_create_group(HDF5_FILE_ID, "acinqu")

endsubroutine

subroutine save_hdf5(group, dataset_name, dataset_cols, dataset_vals, save_cols)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! function that stores an hdf5 file https://github.com/jterwin/HDF5_utils
    !
    ! input
    ! -----
    ! group : character(len=*)
    !   Name of the group in the HDF file. Must be created in the init routine of save_global. 
    !   If equals '', the dataset is not written to any group.
    ! dataset_name : character(len=*)
    !   The names of the dataframe to be created in the group.
    ! elpa_cols : type(string),dimension(:)  
    !   Columns names of the file
    ! dataset_vals    : real(8),dimension(:,:)
    !   For every column, one 1D array of values. Hence, a 2D array.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in),optional                   :: save_cols
    integer                                       :: i_max,i
    character(len=*), intent(in)                  :: group
    character(len=*), intent(in)                  :: dataset_name
    type(string),dimension(:),intent(in)          :: dataset_cols !columns of elpa file
    real(8),dimension(:,:),allocatable,intent(in) :: dataset_vals !values of elpa file
    character(len=15)                             :: tmp_str
    character(len=100)                            :: df_key
    logical                                       :: save_cols_

    if (present(save_cols)) save_cols_ = save_cols

    do i=1,size(dataset_cols)
        tmp_str = trim(dataset_cols(i)%str)
        if (tmp_str.eq.'') then
            i_max = i
            exit
        endif
    enddo

    !write data to dataframe with key df_key
    if (group.eq.'') then
        df_key = trim(dataset_name)
    else
        df_key = trim(group)//'/'//trim(dataset_name)
    endif
    ! HINT: IF FILE OPEN IN HDFVIEW WRITING DOES NOT WORK
    call hdf_write_dataset(HDF5_FILE_ID, trim(df_key), dataset_vals(:i_max-1,:))

    !add information regarding columns
    if ( save_cols_ ) then
        do i=1,size(dataset_cols)
            if (i.eq.i_max) then
                exit
            endif
            call value_to_string(i-1,tmp_str)
            if (group.eq.'') then
                call hdf_write_attribute(HDF5_FILE_ID, trim(df_key), "col"//trim(tmp_str), trim(dataset_cols(i)%str))
            else
                call hdf_write_attribute(HDF5_FILE_ID, trim(group), "col"//trim(tmp_str), trim(dataset_cols(i)%str))
            endif
        enddo
    endif

endsubroutine

subroutine read_col_hdf5(filename,column_in,data,hdferr)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read in column from HDF5 file and return it with format float
    ! try to read in vector with name column_in from filename in local cwd, return nothing and hdferr=-1 if 
    ! column does not exist might be smart to use https://github.com/tiasus/HDF5_utils/blob/master/hdf5_utils.f90 
    ! in the future
    !
    ! input
    ! -----
    ! filename : character(len=64)
    !   Filenam to read.
    ! column_in : chacter(len=*)
    !   The column to look for in the file.
    ! data : real(8),dimension(:)
    !   The found data in the hdf5 file.
    ! hdferr : integer
    !   Integer that can be used to identify errors.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    CHARACTER(len=64),intent(in) :: filename
    CHARACTER(len=*),intent(in) :: column_in
    CHARACTER(len=20)       :: column !trimed version of column_in

    real(8),dimension(:),allocatable,intent(out) :: data    ! Read buffer
    INTEGER,intent(out)                          :: hdferr !hdf5 error variable

    INTEGER(HID_T)                 :: file, dset, dspace ! HDF5 Handles
    integer(hsize_t), dimension(1) :: dims,maxdims
    logical :: exists

    !column_in = '/InP'
    column    = trim(column_in)

    ! Initialize FORTRAN interface.
    CALL h5open_f(hdferr)

    ! Open file and dataset using the default properties.
    CALL h5fopen_f(filename, H5F_ACC_RDWR_F, file, hdferr)

    if (hdferr.eq.-1) then
        write(*,*) '***error*** cannot open hdf5 file.'
        stop
    endif

    !open group if it exists
    call h5lexists_f(file, column, exists, hdferr)
    if (exists) then
        CALL h5dopen_f(file, column, dset, hdferr)
    else 
        CALL h5fclose_f(file  , hdferr)
        hdferr = -1
        return
    endif

    ! Get the dataspace ID
    call h5dget_space_f(dset,dspace,hdferr)
    ! Getting dims from dataspace
    call h5sget_simple_extent_dims_f(dspace, dims, maxdims, hdferr)
    !
    ! allocate array to read
    allocate(data(dims(1)))

    ! Read the data using the default properties.
    CALL h5dread_f(dset, H5T_NATIVE_DOUBLE, data, dims, hdferr)
    if (hdferr.eq.-1) return

    !close resources
    !CALL h5pclose_f(dcpl  , hdferr)
    CALL h5dclose_f(dset  , hdferr)
    CALL h5sclose_f(dspace, hdferr)
    CALL h5fclose_f(file  , hdferr)

endsubroutine

subroutine close_hdf_functions
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! deallocate all pointers of the module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! close HDF5 file
    call hdf_close_file(HDF5_FILE_ID)

endsubroutine

endmodule