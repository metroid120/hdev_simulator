!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! mathematical functions that are implemented in hdev
module math_oper


use phys_const
USE Dual_Num_Auto_Diff
USE Dual_Num_2_Auto_Diff
use hdf_functions, only : read_col_hdf5
use m_strings, only: upper

implicit none

interface fd1h !interface allows to call this routine with dual and real valoued arguments
  module procedure fd1h_dual, fd1h
end interface

interface fd3h !interface allows to call this routine with dual and real valoued arguments
  module procedure fd3h_dual, fd3h
end interface

interface dfd1h  !interface allows to call this routine with dual and real valoued arguments
  module procedure dfd1h_dual, dfd1h
end interface


interface fd1h_inv  !interface allows to call this routine with dual and real valoued arguments
  module procedure fd1h_inv_dual, fd1h_inv
end interface

interface bernoulli  !interface allows to call this routine with dual and real valoued arguments
  module procedure bernoulli_real, bernoulli_dual
end interface

real(8),dimension(4) :: BNLI = (/1.0e-6, 1.0e-4, 37.43, 300.0/)

contains



! -------------------------------------------------------------------------------------------<<<<--------ROT--------<<<
subroutine rot(x,k,mat)
! rotation matrix from basis vector 'x','y','z' to any direction

real(8),dimension(3),   intent(in)  :: x   ! new direction
character(len=1),       intent(in)  :: k   ! 'x','y' or 'z' start
real(8),dimension(3,3), intent(out) :: mat ! rotation matrix

real(8) :: ki,sk

if ((k.eq.'z').and.(.not.((x(1).eq.0.).and.(x(2).eq.0.)))) then ! x not in z-direction
  ki       = sqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
  sk       = sqrt(x(1)*x(1)+x(2)*x(2))
  mat(1,1) = x(2)/sk
  mat(1,2) = x(1)*x(3)/sk/ki
  mat(1,3) = x(1)/ki
  mat(2,1) = -x(1)/sk
  mat(2,2) = x(2)*x(3)/sk/ki
  mat(2,3) = x(2)/ki
  mat(3,1) = 0.
  mat(3,2) = -sk/ki
  mat(3,3) = x(3)/ki

elseif ((k.eq.'x').and.(.not.((x(2).eq.0.).and.(x(3).eq.0.)))) then ! x not in x-direction
  ki       = sqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
  sk       = sqrt(x(2)*x(2)+x(3)*x(3))
  mat(2,2) = x(3)/sk
  mat(2,3) = x(2)*x(1)/sk/ki
  mat(2,1) = x(2)/ki
  mat(3,2) = -x(2)/sk
  mat(3,3) = x(3)*x(1)/sk/ki
  mat(3,1) = x(3)/ki
  mat(1,2) = 0.
  mat(1,3) = -sk/ki
  mat(1,1) = x(1)/ki

elseif ((k.eq.'y').and.(.not.((x(1).eq.0.).and.(x(3).eq.0.)))) then ! x not in y-direction
  ki       = sqrt(x(1)*x(1)+x(2)*x(2)+x(3)*x(3))
  sk       = sqrt(x(3)*x(3)+x(1)*x(1))
  mat(3,3) = x(1)/sk
  mat(3,1) = x(3)*x(2)/sk/ki
  mat(3,2) = x(3)/ki
  mat(1,3) = -x(3)/sk
  mat(1,1) = x(1)*x(2)/sk/ki
  mat(1,2) = x(1)/ki
  mat(2,3) = 0.
  mat(2,1) = -sk/ki
  mat(2,2) = x(2)/ki

else
  mat(1,1) = 1.
  mat(1,2) = 0.
  mat(1,3) = 0.
  mat(2,1) = 0.
  mat(2,2) = 1.
  mat(2,3) = 0.
  mat(3,1) = 0.
  mat(3,2) = 0.
  mat(3,3) = 1.

endif

endsubroutine

! -------------------------------------------------------------------------------------------<<<<----RAND_SPHERE----<<<
subroutine rand_sphere(a,x,cs)
! random point on a surface of a sphere
real(8),intent(in)                 :: a   ! radius of sphere
real(8),intent(in)                 :: cs  ! cosine of angle
real(8),dimension(3),intent(inout) :: x   ! output point on surface

real(8),dimension(3,3)             :: T
real(8),dimension(2)               :: r
real(8),dimension(3)               :: xs
real(8)                            :: css,sn,fai


call random_number(r)
if (abs(cs).gt.1.) then ! random
  css = 1-2*r(1)
else
  css = cs
  xs  = x
endif
sn    = sqrt(1-css*css)
fai   = 2*pi*r(2)

! random vector with cos css between x und x-axis
x(1) = a*css
x(2) = a*sn*cos(fai)
x(3) = a*sn*sin(fai)

if (abs(cs).le.1.) then
  call rot(xs,'x',T) ! T rotates from x-axis to vector x
  x = matmul(T,x)
endif

endsubroutine

! -------------------------------------------------------------------------------------------<<<<------DET_MAT------<<<
subroutine det_mat(mat,det)
! determinante of 3x3  matrix
real(8),dimension(3,3), intent(in)  :: mat
real(8),                intent(out) :: det

det =       mat(1,1)*mat(2,2)*mat(3,3)
det = det + mat(1,2)*mat(2,3)*mat(3,1)
det = det + mat(1,3)*mat(2,1)*mat(3,2)
det = det - mat(1,1)*mat(2,3)*mat(3,2)
det = det - mat(1,2)*mat(2,1)*mat(3,3)
det = det - mat(1,3)*mat(2,2)*mat(3,1)

endsubroutine

! -------------------------------------------------------------------------------------------<<<<------INV_MAT------<<<
subroutine inv_mat(mat,out)
! inverse of 3x3  matrix
real(8),dimension(3,3), intent(in)  :: mat
real(8),dimension(3,3), intent(out) :: out

real(8)                             :: det

out(1,1) = -mat(2,3)*mat(3,2) + mat(2,2)*mat(3,3)
out(1,2) =  mat(1,3)*mat(3,2) - mat(1,2)*mat(3,3)
out(1,3) = -mat(1,3)*mat(2,2) + mat(1,2)*mat(2,3)
out(2,1) =  mat(2,3)*mat(3,1) - mat(2,1)*mat(3,3)
out(2,2) = -mat(1,3)*mat(3,1) + mat(1,1)*mat(3,3)
out(2,3) = -mat(1,1)*mat(2,3) + mat(1,3)*mat(2,1)
out(3,1) = -mat(2,2)*mat(3,1) + mat(2,1)*mat(3,2)
out(3,2) =  mat(1,2)*mat(3,1) - mat(1,1)*mat(3,2)
out(3,3) = -mat(1,2)*mat(2,1) + mat(1,1)*mat(2,2)
call det_mat(mat,det)
out      = out/det

!(a b c)
!(d e f) = A
!(g h i)
!            ( -f h + e i    c h - b i     - c e + b f )
!1/det(A) *  ( f g  - d i    - c g + a i   - a f + c d )
!            ( - e g + d h   b g - a h     - b d + a e )

endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine range_grid(intv_pnts,intv_diff,n_pnts,set_diff, x,nx,problem)

real(8),dimension(:),      intent(in)  :: intv_pnts ! input interval
real(8),dimension(:),      intent(in)  :: intv_diff ! input interval
integer,dimension(:),      intent(in)  :: n_pnts    ! input interval
logical,                   intent(in)  :: set_diff  ! how to read input interval
real(8),dimension(:),pointer           :: x         ! output interval
integer                                :: nx
logical                                :: problem

integer :: i,j,n,m,ilen
real(8) :: dx,int_x

problem=.false.

! --> interval length
ilen = 0
do i=1,size(intv_pnts)
  if (intv_pnts(i).gt.(-9.9D90)) then
    ilen = ilen + 1
  else
    exit
  endif
enddo

! --> check intervall
do i=2,ilen
  if (intv_pnts(i).lt.intv_pnts(i)) then
    problem=.true.
    return
  endif
enddo

if (set_diff) then
  ! --> check intv_diff
  do i=1,ilen-1
    if (intv_diff(i).le.dble(0)) then
      problem=.true.
      return
    endif
  enddo
else
  ! --> check n_pnts
  do i=1,ilen-1
    if (n_pnts(i).le.0) then
      problem=.true.
      return
    endif
  enddo
endif

! --> get number of points
n=0
do i=1,ilen-1
  if (set_diff) then
    dx = intv_diff(i)
    n = n + ceiling(abs(intv_pnts(i+1)-intv_pnts(i))/dx-1e-9)
  else
    n = n + n_pnts(i) - 1
  endif
  if (i.eq.1) n=n+1
enddo

! --> init x
nx=n
nullify(x)
allocate(x(nx))

! --> fill x
n    = 1
do i=1,ilen-1
  x(n) = intv_pnts(i)
  int_x = intv_pnts(i+1)-intv_pnts(i)
  if (set_diff) then
    dx = intv_diff(i)
    m  = ceiling(int_x/dx-1e-9)
  else
    m  = n_pnts(i) - 1
  endif
  do j=1,m
    x(n+1) = intv_pnts(i) + int_x*dble(j)/dble(m)
    n      = n+1
  enddo
enddo
x(n) = intv_pnts(ilen)


endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine init_grid(in_intv,fun,cont_name,intv,len,equ,xmin,xmax,dx)
! get discretization vector

real(8),dimension(:,:),      intent(in)  :: in_intv ! input interval
character(len=3),            intent(in)  :: fun     ! how to read input interval
character(len=64),            intent(in) :: cont_name ! contact name
real(8),dimension(:),allocatable         :: intv    ! output interval
integer,                     intent(out) :: len     ! length of output vector
logical,optional :: equ     ! if true: equidistant output vector
real(8),optional :: xmin    ! minimum value output vector
real(8),optional :: xmax    ! maximum value output vector
real(8),optional :: dx      ! dx of equidistant output vector

integer                                  :: i,j,n,hdferr
real(8)                                  :: dp,x_last

!for reading in hdf5
real(8),dimension(:),allocatable:: grid_hdf5
character(len=64)       :: hdf_str, col_str

! ---> get length of output vector ----------
len = 0
if (present(equ)) equ = .false.

if (fun.eq.'lin') then
    ! FUN = 'LIN' --> linear vector
    do i=1,size(in_intv,2)
        if ((in_intv(3,i)).gt.0) then
            if (len.eq.0) then
                len = len + int(in_intv(3,i))
            else
                len = len + int(in_intv(3,i))
                if (x_last.eq.in_intv(1,i)) then
                    len = len - 1
                endif
            endif
            x_last = in_intv(2,i)
        endif
    enddo
  
    if (int(in_intv(3,1)).eq.len) then
        if (present(equ))  equ  = .true. ! equidistant vector
        if (present(xmin)) xmin = in_intv(1,1)
        if (present(xmax)) xmax = in_intv(2,1)
        if (present(dx))   dx   = (xmax-xmin)/(int(in_intv(3,1))-1)
    endif
  
elseif ((fun.eq.'log').or.(fun.eq.'mlg')) then
    ! FUN='LOG' or 'MLOG' --> logarithmic vector
    do i=1,size(in_intv,2)
        if ((in_intv(3,i)).gt.0) then
            if (len.eq.0) then
                len = len + int(in_intv(3,i))
            else
                len = len + int(in_intv(3,i)) - 1
            endif
        endif
    enddo
  
elseif (fun.eq.'tab') then
    ! FUN='TAB' --> take input vector as is
    do j=1,size(in_intv,2)
        do i=1,size(in_intv,1)
            if ((in_intv(i,j)).gt.(-9.9D90)) then
                len = len + 1
            endif
        enddo
    enddo
elseif (fun.eq.'hdf') then
    ! FUN='HDF' --> read file bias.h5 and get data from there
    hdf_str = trim('bias.h5')
    col_str = trim(upper(cont_name))
    call read_col_hdf5(hdf_str,col_str,grid_hdf5,hdferr)
    if (.not.(allocated(grid_hdf5))) then
        write(*,*) '***error*** did not find array for contact in bias.h5 ',cont_name
        stop
    endif
    len = size(grid_hdf5)

else
    ! todo: error

endif

! --- init fields --------------
if (allocated(intv)) deallocate(intv)
allocate(intv(len))

! --- set discretization ----------
if (fun.eq.'tab') then
  n=0
  do j=1,size(in_intv,2)
    do i=1,size(in_intv,1)
      n=n+1
      if (n.le.len) then
        intv(n) = in_intv(i,j)
      endif
    enddo
  enddo

elseif (fun.eq.'hdf') then
  intv = grid_hdf5
  deallocate(grid_hdf5)
  
elseif (fun.eq.'lin') then
  n = 0
  do i=1,size(in_intv,2)
    if ((in_intv(3,i)).gt.0) then
      if ((in_intv(3,i)-1).eq.0) then 
        dp = 0
      else
        dp = (in_intv(2,i) - in_intv(1,i))/(in_intv(3,i)-1)
      endif
      
      if (n.eq.0) then
        ! --> first point
        intv(1) = in_intv(1,i)
        n       = n + 1
      elseif (x_last.ne.in_intv(1,i)) then
        ! --> first point of next intervall
        n       = n + 1
        intv(n) = in_intv(1,i)
      endif
      do j=1,(int(in_intv(3,i))-1)
        intv(n+j) = in_intv(1,i) + j * dp
      enddo
      n = n + int(in_intv(3,i)) - 1
      x_last = in_intv(2,i)
    endif
  enddo
  
elseif (fun.eq.'log') then
  n = 0
  do i=1,size(in_intv,2)
    if ((in_intv(3,i)).gt.0) then
      dp = (in_intv(2,i) - in_intv(1,i))/(in_intv(3,i)-1)
      if (n==0) then
        intv(1) = exp(in_intv(1,i)*log(dble(10)))
        n       = n + 1
      endif
      do j=1,(int(in_intv(3,i))-1)
        intv(n+j) = exp((in_intv(1,i) + j * dp)*log(10.))
      enddo
      n = n + int(in_intv(3,i)) - 1
    endif
  enddo
  
elseif (fun.eq.'mlg') then
  n = 0
  do i=1,size(in_intv,2)
    if ((in_intv(3,i)).gt.0) then
      dp = (in_intv(2,i) - in_intv(1,i))/(in_intv(3,i)-1)
      if (n==0) then
        intv(1) = -exp(in_intv(1,i)*log(10.))
        n           = n + 1
      endif
      do j=1,(int(in_intv(3,i))-1)
        intv(n+j) = -exp((in_intv(1,i) + j * dp)*log(10.))
      enddo
      n = n + int(in_intv(3,i)) - 1
    endif
  enddo

else
  ! todo: error

endif

endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine box_grid(in_intv,intv,len)
! get discretization vector

real(8),dimension(:,:),      intent(in)  :: in_intv ! input interval
real(8),dimension(:),pointer             :: intv    ! output interval
integer,                     intent(out) :: len     ! length of output vector

integer                                  :: i,j,nb,n
real(8)                                  :: y1,y2,dx1,dx2
real(8),dimension(:),pointer             :: x

nullify(intv)
nullify(x)

! ---> get length of output vector ----------
len = 0
do j=1,size(in_intv,2)
  do i=1,size(in_intv,1)
    if ((in_intv(i,j)).gt.(-9.9D90)) then
      len = len + 1
    endif
  enddo
enddo


if (mod(len,4).ne.0) then
  ! problem
endif

nb = int(dble(len)/dble(4)+1e-5)

len=0
do i=1,nb
  y1  = in_intv(1,i)
  y2  = in_intv(2,i)
  dx1 = in_intv(3,i)
  dx2 = in_intv(4,i)
  call get_grid(y1,y2,dx1,dx2,n,x)
  deallocate(x)
  nullify(x)
  len = len + n
enddo
len   = len-nb+1

allocate(intv(len))

len=0
do i=1,nb
  y1  = in_intv(1,i)
  y2  = in_intv(2,i)
  dx1 = in_intv(3,i)
  dx2 = in_intv(4,i)
  call get_grid(y1,y2,dx1,dx2,n,x)
  do j=1,n
    intv(len+j) = x(j)
  enddo
  deallocate(x)
  nullify(x)
  if (i.ne.nb) then
    len = len+n-1
  else
    len = len+n
  endif
enddo

endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine area_grid(intv_pnts,intv_diff,x,nx)
! get discretization vector

real(8),dimension(:),        intent(in)  :: intv_pnts ! input interval
real(8),dimension(:,:),      intent(in)  :: intv_diff ! input interval
real(8),dimension(:),pointer             :: x         ! output interval
integer,                     intent(out) :: nx        ! length of output vector

integer                                  :: i,j,nb,n,nmax
real(8)                                  :: y1,y2,dx1,dx2,dx3
real(8),dimension(:),pointer             :: xi

nullify(x)
nullify(xi)

! ---> get length of output vector ----------
nb = -1
do i=1,size(intv_pnts)
  if (intv_pnts(i).gt.(-9.9D90)) then
    nb = nb + 1
  endif
enddo

nx=0
do i=1,nb
  y1  = intv_pnts(i)
  y2  = intv_pnts(i+1)
  dx1 = intv_diff(1,i)
  dx2 = intv_diff(2,i)
  dx3 = intv_diff(3,i)
  nmax= 200 ! !
  call get_boxgrid(y1,y2,dx1,dx2,dx3,nmax,n,x)
  deallocate(x)
  nullify(x)
  nx = nx + n
enddo
nx   = nx-nb+1

allocate(x(nx))

nx=0
do i=1,nb
  y1  = intv_pnts(i)
  y2  = intv_pnts(i+1)
  dx1 = intv_diff(1,i)
  dx2 = intv_diff(2,i)
  dx3 = intv_diff(3,i)
  nmax= 200 ! 
  call get_boxgrid(y1,y2,dx1,dx2,dx3,nmax,n,xi)
  do j=1,n
    x(nx+j) = xi(j)
  enddo
  deallocate(xi)
  nullify(xi)
  if (i.ne.nb) then
    nx = nx+n-1
  else
    nx = nx+n
  endif
enddo

endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
function get_disclen(dx1,dx2,n) result(xlen)

real(8) :: dx1
real(8) :: dx2
integer :: n
real(8) :: xlen

real(8) :: dxmin
real(8) :: dxmax


if (dx1.eq.dx2) then
  xlen = dx1
  return
elseif (dx1.lt.dx2) then
  dxmin = dx1
  dxmax = dx2
else
  dxmin = dx2
  dxmax = dx1
endif
if (n.eq.2) then
  xlen=0
else
  xlen = dxmax*(dble(1)-(dxmin/dxmax)**((dble(n)-1)/(dble(n)-2)))/(dble(1)-(dxmin/dxmax)**(dble(1)/(dble(n)-2)))
endif

endfunction

! -------------------------------------------------------------------------------------------<<<<-----SET SCALING VECTOR-----<<<
function get_scal(dx,dy,dim) result(scal)

real(8) :: dx
real(8) :: dy
integer :: dim

real(8),dimension(:),allocatable :: scal

allocate(scal(dim))

if (dim.eq.3) then
  scal(1)=dx
  scal(2)=dx
  scal(3)=dx*dy
elseif (dim.ge.1) then
  scal=dx
else
  write(*,*) '***error*** scaling vector'
  stop
endif

scal = scal*Q
endfunction
! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine get_grid(y1,y2,dx1,dx2,n,x)
    real(8) :: y1
    real(8) :: y2
    real(8) :: dx1
    real(8) :: dx2
    integer :: n
    real(8),dimension(:),pointer :: x

    integer(4) :: k,m,nn
    real(8) :: y,xmin,xmax,q,dx
    complex(8),dimension(:,:),pointer :: a
    !complex(8),dimension(:),pointer   :: tau
    complex(8),dimension(:),pointer   :: work
    logical,dimension(:),pointer      :: bwork
    real(8),dimension(:),pointer      :: rwork
    complex(8),dimension(:),pointer   :: w
    complex(8),dimension(:,:),pointer :: vs
    logical                           :: sel,sdim
    integer(8)                        :: info

    nullify(x)
    nullify(a)
    !nullify(tau)
    nullify(work)
    nullify(w)
    nullify(rwork)
    nullify(bwork)
    nullify(vs)

    y = y2-y1
    if (abs(y).lt.1e-13) then
      n = 1
      allocate(x(n))
      x = 0.5*(y1+y2)
      return
    endif

    if (abs(dx1-dx2).lt.1e-13) then
        if (dx1.ge.y) then
            n = 2
        else
            n = floor(y/dx1+1e-13) + 1
        endif
        if ((n.lt.2).or.(n.gt.10000)) then
            write(*,*) '***error*** discretization'
            stop
        endif
        allocate(x(n))
        x(1)   = y1
        x(n)   = y2
        do k=2,n-1
            x(k) = y1 + y*(dble(k-1)/dble(n-1))
        enddo
      
    elseif (min(dx1,dx2).gt.y) then
        n = 2
        allocate(x(n))
        x(1)   = y1
        x(n)   = y2

    elseif (min(dx1,dx2).eq.(y*0.5)) then
        n = 3
        allocate(x(n))
        x(1)   = y1
        x(2)   = 0.5*(y1+y2)
        x(3)   = y2
      
    else
        xmax = max(dx1,dx2)
        if (xmax.gt.(0.5*y)) xmax=0.5*y
        xmin = min(dx1,dx2)

        q = (y-xmax)/(y-xmin)
        n = floor(log(xmin/xmax)/log(q)) + 2
        if ((n.lt.2).or.(n.gt.10000)) then
            write(*,*) '***error*** discretization'
            stop
        endif
        m = n-1
        !if (m.lt.60) then
          ! correction for q
          ! solve: q^(n-1) + q^(n-2)*y/(xmin-y) - xmin/(xmin-y)=0
          ! with characteristic polynom and eigenvalues
          allocate(a(m,m))
          !allocate(tau(m-1))
          allocate(vs(1,m))
          allocate(rwork(m))
          allocate(bwork(m))
          allocate(work(1))
          allocate(w(m))

          a = 0
          do k=1,m-1
              a(k+1,k) = 1
          enddo

          a(1,1) =     y/(y-xmin)
          a(1,m) = -xmin/(y-xmin)

          !nn = int8(n)
          !call zgehrd(m,int8(1),m,a,m,tau,work,nn,info)
          !call zhseqr('E', 'N', m, int8(1), m, a, m, w, int8(1), int8(1), work, m, info)

          call zgees('N','N',sel,m,a,m, sdim, w, vs, 1, work, -1 ,rwork,bwork,info)
          nn=work(1)
          deallocate(work)
          allocate(work(nn))
          call zgees('N','N',sel,m,a,m, sdim, w, vs, 1, work, nn ,rwork,bwork,info)

          do k=1,m
              if (abs(imag(w(k))).lt.1e-6) then
                  if ((real(w(k)).gt.0).and.(real(w(k)).lt.(0.99999))) then
                      q = real(w(k))
                  endif
              endif
          enddo

          deallocate(a)
          !deallocate(tau)
          deallocate(work)
          deallocate(w)
          deallocate(rwork)
          deallocate(bwork)
          deallocate(vs)

        !endif

        allocate(x(n))
        x(1)   = y1
        x(n)   = y2
        dx     = xmin
        if (dx1.lt.dx2) then
            do k=2,n-1
                x(k) = x(k-1) + dx
                dx   = dx/q
            enddo
        else
            do k=n-1,2,-1
                x(k) = x(k+1) - dx
                dx   = dx/q
            enddo
        endif

        !xmax = xmin/q**(n-2)
        !allocate(x(n))
        !x(1)   = y1
        !x(n)   = y2
        !dx     = xmax
        !if (dx1.gt.dx2) then
        !  do k=2,n-1
        !    x(k) = x(k-1) + dx
        !    dx   = dx*q
        !  enddo
        !else
        !  do k=n-1,2,-1
        !    x(k) = x(k+1) - dx
        !    dx   = dx*q
        !  enddo
        !endif
    endif
endsubroutine

! -------------------------------------------------------------------------------------------<<<<-----INIT_GRID-----<<<
subroutine get_boxgrid(y1,y2,dx1,dx2,dx3,nmax,n,x)
    real(8) :: y1
    real(8) :: y2
    real(8) :: dx1
    real(8) :: dx2
    real(8) :: dx3
    integer :: nmax
    integer :: n
    real(8),dimension(:),pointer :: x

    real(8),dimension(:),pointer :: x1,x2,x3
    real(8) :: len0,len1,len2,len_min
    integer :: n1,n2,n3,j,i

    nullify(x,x1,x2,x3)

    ! intervall length
    len0    = y2-y1;


    n1      = floor(dble(nmax)/dble(2))
    len1    = get_disclen(dx1,dx2,n1)
    len_min = len0/dble(2)
    do while ((len1<len_min).and.(n1>2))
        n1      = n1 -1
        len1    = get_disclen(dx1,dx2,n1)
        len_min = len_min - max(dx2,dx1)
    enddo
    len1    = min(len0/dble(2),len1)

    n2      = floor(dble(nmax)/dble(2))
    len2    = get_disclen(dx2,dx3,n2)
    len_min = len0/dble(2)
    do while ((len2<len_min).and.(n2>2))
        n2      = n2 -1
        len2    = get_disclen(dx2,dx3,n2)
        len_min = len_min - max(dx2,dx3)
    enddo
    len2    = min(len0/dble(2),len2)

    call get_grid(y1,y1+len1,dx1,dx2,n1,x1)

    if (abs(len1+len2-len0).lt.dx1*1e-6) then
        n2 = 0
    else
        call get_grid(y1+len1,y2-len2,dx2,dx2,n2,x2)
    endif
    call get_grid(y2-len2,y2,dx2,dx3,n3,x3)

    if (n2.eq.0) then
        n = n1+n3-1
    else
        n = n1+n2+n3-2
    endif

    allocate(x(n))
    j=0
    do i=1,n1
        j = j+1
        x(j) = x1(i)
    enddo

    do i=2,n2
        j = j+1
        x(j) = x2(i)
    enddo

    do i=2,n3
        j = j+1
        x(j) = x3(i)
    enddo

    deallocate(x1)
    if (n2.gt.0) deallocate(x2)
    deallocate(x3)

endsubroutine

! -------------------------------------------------------------------------------------------<<<<--------GCD--------<<<
function gcd(a,b) result(c)
! greatest common divisor
integer :: a
integer :: b
integer :: c

integer :: a1,b1

a1 = a
b1 = b
if (a1.lt.b1) then
  c  = a1         
  a1 = b1
  b1 = c
endif

do                
  c = mod(a1, b1) 
  if (c.eq.0) exit
  a1 = b1         
  b1 = c
                  
enddo             

c = b1

endfunction

! -------------------------------------------------------------------------------------------<<<<------MY_ASINH-----<<<
function my_asinh(x) result(y)

real(8) :: x
real(8) :: y

!y = asinh(x)

y = log(x+sqrt(1+x*x))

endfunction

! -------------------------------------------------------------------------------------------<<<<------HEAPSORT-----<<<
subroutine heapsort(a)
 
real(8), intent(inout) :: a(0:)

integer :: start, n, bottom
real(8) :: temp
 
n = size(a)
do start = (n - 2) / 2, 0, -1
   call siftdown(a, start, n);
enddo
 
do bottom = n - 1, 1, -1
  temp = a(0)
  a(0) = a(bottom)
  a(bottom) = temp;
  call siftdown(a, 0, bottom)
enddo
 
endsubroutine
 
! -------------------------------------------------------------------------------------------<<<<-----SHIFTDOWN-----<<<
subroutine siftdown(a, start, bottom)
 
real(8), intent(inout) :: a(0:)
integer, intent(in) :: start, bottom

integer             :: child, root
real(8)              :: temp
 
root = start
do while(root*2 + 1 < bottom)
  child = root * 2 + 1
 
  if (child + 1 < bottom) then
    if (a(child) < a(child+1)) child = child + 1
  endif
 
  if (a(root) < a(child)) then
    temp = a(child)
    a(child) = a (root)
    a(root) = temp
    root = child
  else
    return
  endif  
enddo      
 
endsubroutine

! -------------------------------------------------------------------------------------------<<<<------D_MRGRNK-----<<<
Subroutine D_mrgrnk (XDONT, IRNGT)
! __________________________________________________________
!   MRGRNK = Merge-sort ranking of an array
!   For performance reasons, the first 2 passes are taken
!   out of the standard loop, and use dedicated coding.
! __________________________________________________________
! __________________________________________________________
      Real (8), Dimension (:), Intent (In) :: XDONT
      Integer, Dimension (:), Intent (Out) :: IRNGT
! __________________________________________________________
      Real (8) :: XVALA, XVALB
!
      Integer, Dimension (SIZE(IRNGT)) :: JWRKT
      Integer :: LMTNA, LMTNC, IRNG1, IRNG2
      Integer :: NVAL, IIND, IWRKD, IWRK, IWRKF, JINDA, IINDA, IINDB
!
      NVAL = Min (SIZE(XDONT), SIZE(IRNGT))
      Select Case (NVAL)
      Case (:0)
         Return
      Case (1)
         IRNGT (1) = 1
         Return
      Case Default
         Continue
      End Select
!
!  Fill-in the index array, creating ordered couples
!
      Do IIND = 2, NVAL, 2
         If (XDONT(IIND-1) <= XDONT(IIND)) Then
            IRNGT (IIND-1) = IIND - 1
            IRNGT (IIND) = IIND
         Else
            IRNGT (IIND-1) = IIND
            IRNGT (IIND) = IIND - 1
         End If
      End Do
      If (Modulo(NVAL, 2) /= 0) Then
         IRNGT (NVAL) = NVAL
      End If
!
!  We will now have ordered subsets A - B - A - B - ...
!  and merge A and B couples into     C   -   C   - ...
!
      LMTNA = 2
      LMTNC = 4
!
!  First iteration. The length of the ordered subsets goes from 2 to 4
!
      Do
         If (NVAL <= 2) Exit
!
!   Loop on merges of A and B into C
!
         Do IWRKD = 0, NVAL - 1, 4
            If ((IWRKD+4) > NVAL) Then
               If ((IWRKD+2) >= NVAL) Exit
!
!   1 2 3
!
               If (XDONT(IRNGT(IWRKD+2)) <= XDONT(IRNGT(IWRKD+3))) Exit
!
!   1 3 2
!
               If (XDONT(IRNGT(IWRKD+1)) <= XDONT(IRNGT(IWRKD+3))) Then
                  IRNG2 = IRNGT (IWRKD+2)
                  IRNGT (IWRKD+2) = IRNGT (IWRKD+3)
                  IRNGT (IWRKD+3) = IRNG2
!
!   3 1 2
!
               Else
                  IRNG1 = IRNGT (IWRKD+1)
                  IRNGT (IWRKD+1) = IRNGT (IWRKD+3)
                  IRNGT (IWRKD+3) = IRNGT (IWRKD+2)
                  IRNGT (IWRKD+2) = IRNG1
               End If
               Exit
            End If
!
!   1 2 3 4
!
            If (XDONT(IRNGT(IWRKD+2)) <= XDONT(IRNGT(IWRKD+3))) Cycle
!
!   1 3 x x
!
            If (XDONT(IRNGT(IWRKD+1)) <= XDONT(IRNGT(IWRKD+3))) Then
               IRNG2 = IRNGT (IWRKD+2)
               IRNGT (IWRKD+2) = IRNGT (IWRKD+3)
               If (XDONT(IRNG2) <= XDONT(IRNGT(IWRKD+4))) Then
!   1 3 2 4
                  IRNGT (IWRKD+3) = IRNG2
               Else
!   1 3 4 2
                  IRNGT (IWRKD+3) = IRNGT (IWRKD+4)
                  IRNGT (IWRKD+4) = IRNG2
               End If
!
!   3 x x x
!
            Else
               IRNG1 = IRNGT (IWRKD+1)
               IRNG2 = IRNGT (IWRKD+2)
               IRNGT (IWRKD+1) = IRNGT (IWRKD+3)
               If (XDONT(IRNG1) <= XDONT(IRNGT(IWRKD+4))) Then
                  IRNGT (IWRKD+2) = IRNG1
                  If (XDONT(IRNG2) <= XDONT(IRNGT(IWRKD+4))) Then
!   3 1 2 4
                     IRNGT (IWRKD+3) = IRNG2
                  Else
!   3 1 4 2
                     IRNGT (IWRKD+3) = IRNGT (IWRKD+4)
                     IRNGT (IWRKD+4) = IRNG2
                  End If
               Else
!   3 4 1 2
                  IRNGT (IWRKD+2) = IRNGT (IWRKD+4)
                  IRNGT (IWRKD+3) = IRNG1
                  IRNGT (IWRKD+4) = IRNG2
               End If
            End If
         End Do
!
!  The Cs become As and Bs
!
         LMTNA = 4
         Exit
      End Do
!
!  Iteration loop. Each time, the length of the ordered subsets
!  is doubled.
!
      Do
         If (LMTNA >= NVAL) Exit
         IWRKF = 0
         LMTNC = 2 * LMTNC
!
!   Loop on merges of A and B into C
!
         Do
            IWRK = IWRKF
            IWRKD = IWRKF + 1
            JINDA = IWRKF + LMTNA
            IWRKF = IWRKF + LMTNC
            If (IWRKF >= NVAL) Then
               If (JINDA >= NVAL) Exit
               IWRKF = NVAL
            End If
            IINDA = 1
            IINDB = JINDA + 1
!
!   Shortcut for the case when the max of A is smaller
!   than the min of B. This line may be activated when the
!   initial set is already close to sorted.
!
!          IF (XDONT(IRNGT(JINDA)) <= XDONT(IRNGT(IINDB))) CYCLE
!
!  One steps in the C subset, that we build in the final rank array
!
!  Make a copy of the rank array for the merge iteration
!
            JWRKT (1:LMTNA) = IRNGT (IWRKD:JINDA)
!
            XVALA = XDONT (JWRKT(IINDA))
            XVALB = XDONT (IRNGT(IINDB))
!
            Do
               IWRK = IWRK + 1
!
!  We still have unprocessed values in both A and B
!
               If (XVALA > XVALB) Then
                  IRNGT (IWRK) = IRNGT (IINDB)
                  IINDB = IINDB + 1
                  If (IINDB > IWRKF) Then
!  Only A still with unprocessed values
                     IRNGT (IWRK+1:IWRKF) = JWRKT (IINDA:LMTNA)
                     Exit
                  End If
                  XVALB = XDONT (IRNGT(IINDB))
               Else
                  IRNGT (IWRK) = JWRKT (IINDA)
                  IINDA = IINDA + 1
                  If (IINDA > LMTNA) Exit! Only B still with unprocessed values
                  XVALA = XDONT (JWRKT(IINDA))
               End If
!
            End Do
         End Do
!
!  The Cs become As and Bs
!
         LMTNA = 2 * LMTNA
      End Do
!
      Return
!
End Subroutine D_mrgrnk

! -------------------------------------------------------------------------------------------<<<<--------CACOS------<<<
function cacos(z) result(c)

complex(8) :: z
complex(8) :: c


complex(8) :: j

j = cmplx(dble(0),dble(1),kind=8)

c = -j * log(z + j*sqrt(1 - z*z))

endfunction

! -------------------------------------------------------------------------------------------<<<<--------CACOS------<<<
function get_detnorm(a,b,c) result(d)

complex(8),dimension(:) :: a
complex(8),dimension(:) :: b
complex(8),dimension(:) :: c
complex(8)              :: d

integer    :: i,n
real(8)    :: e,g
complex(8) :: f1,f2,f3

n  = size(b)

e  = 1
g  = 0.9

f1 = e*b(1)
f2 = e*(b(2)*f1 - a(1)*c(1)*e)

do i=3,n
  f3   = e*(b(i)*f2 - a(i-1)*c(i-1)*e*f1)
  f1   = f2
  f2   = f3
  if ((abs(real(f2)).gt.1e10).and.(abs(imag(f2)).gt.1e10)) then
    e  = e*g
    f1 = f1*(g**(i-1))
    f2 = f2*(g**i)
    f3 = f3*(g**i)
  elseif ((abs(real(f2)).lt.1e-10).and.(abs(imag(f2)).lt.1e-10)) then
    e  = e/g
    f1 = f1/(g**(i-1))
    f2 = f2/(g**i)
    f3 = f3/(g**i)
  endif
enddo

d = f3

endfunction

pure function sinh_dual(x) result (y)
! solves bernoulli function as y=B(x), numerically stable version
! Markus : version using dual numbers
TYPE(DUAL_NUM),intent(in)   :: x
TYPE(DUAL_NUM)              :: y

!y = (exp(x) - exp(-x))*0.5
y%x_ad_  = sinh(x%x_ad_)
y%xp_ad_ = x%xp_ad_*cosh(x%x_ad_)

endfunction

pure function sinh_dual_2(x) result (y)
! solves bernoulli function as y=B(x), numerically stable version
! Markus : version using dual numbers
TYPE(DUAL_NUM_2),intent(in)   :: x
TYPE(DUAL_NUM_2)              :: y

!y = (exp(x) - exp(-x))*0.5
y%x_ad_  = sinh(x%x_ad_)
y%xp_ad_ = x%xp_ad_*cosh(x%x_ad_)

endfunction


pure function tanh_dual(x) result (y)
! solves bernoulli function as y=B(x), numerically stable version
! Markus : version using dual numbers
TYPE(DUAL_NUM),intent(in)   :: x
TYPE(DUAL_NUM)              :: y

!y = (exp(x) - exp(-x))*0.5
y%x_ad_  = tanh(x%x_ad_)
y%xp_ad_ = x%xp_ad_*(1-tanh(x%x_ad_)**2)

endfunction

! -------------------------------------------------------------------------------------------<<<<--------BERNOULLI--------<<<
pure function bernoulli_real(x) result (y)
! solves bernoulli function as y=B(x), numerically stable version
real(8),intent(in)   :: x
real(8)              :: y

if (abs(x)<BNLI(1)) then
  y=2.0/(2.0+x+x*x/3.0)
elseif(abs(x)<BNLI(2)) then 
  y=1.0-0.5*x  
elseif(abs(x)<BNLI(3))then 
  if(x>0.0) then
    y=x/(exp(x)-1.0)
  else
    y=x*exp(-x)/(1.0-exp(-x))
  endif  
elseif(abs(x)<BNLI(4)) then  
  if(x>0.0) then  
    y=x*exp(-x)
  else
    y=-x
  endif
else
  if(x>0.0) then  
    y=0.0
  else
    y=-x
  endif
endif

!y = -y ! ????

endfunction

pure function bernoulli_dual(x) result (y)
! solves bernoulli function as y=B(x), numerically stable version
! Markus : version using dual numbers
TYPE(DUAL_NUM),intent(in)   :: x
real(8)                     :: abs_x
TYPE(DUAL_NUM)              :: y

abs_x  = abs(x%x_ad_)

if (abs_x<BNLI(1)) then
  y=2.0/(2.0+x+x*x/3.0)
elseif(abs_x<BNLI(2)) then 
  y=1.0-0.5*x  
elseif(abs_x<BNLI(3))then 
  if(x>0.0) then
    y=x/(exp(x)-1.0)
  else
    y=x*exp(-x)/(1.0-exp(-x))
  endif  
elseif(abs_x<BNLI(4)) then  
  if(x>0.0) then  
    y=x*exp(-x)
  else
    y=-x
  endif
else
  if(x>0.0) then  
    y=0.0
  else
    y=-x
  endif
endif

!y = -y ! ????

endfunction

! -------------------------------------------------------------------------------------------<<<<--------BERNOULLI--------<<<
function dbern(x) result (y)
! Numerically stable approximation of the derivative of the
! Bernoulli-function after its argument
real(8)              :: x
real(8)              :: y

real(8)              :: a,a_1

if (abs(x).lt.BNLI(1)) then
    a   = x/dble(3)
    a_1 = dble(2) + x + x*a
    y   =-dble(2)*(dble(1) + dble(2)*a)/(a_1*a_1)

elseif (abs(x).lt.BNLI(2)) then
    y=-0.5

elseif (abs(x).lt.BNLI(3)) then
    a = bernoulli(x)
    y = a/x * (dble(1) - a*exp(x))

elseif(abs(x).lt.BNLI(4)) then
  if(x.gt.dble(0)) then
    y = exp(-x)-bernoulli(x)
  else
    y = -dble(1) 
  endif

else
    if (x.gt.dble(0))  then
        y = dble(0)
    else
        y = -dble(1) 
    endif

endif

end function

! -------------------------------------------------------------------------------------------<<<<---GCD--<<<
recursive function gcd_rec(u, v) result(gcd)
integer             :: gcd
integer, intent(in) :: u, v
 
if (mod(u, v).ne.0) then
  gcd = gcd_rec(v, mod(u, v))
else
  gcd = v
endif

endfunction gcd_rec

! -------------------------------------------------------------------------------------------<<<<---GCD--<<<
function cross_product(a, b)
real(8), dimension(3) :: cross_product
real(8), dimension(3), intent(in) :: a, b
 
cross_product(1) = a(2)*b(3) - a(3)*b(2)
cross_product(2) = a(3)*b(1) - a(1)*b(3)
cross_product(3) = a(1)*b(2) - b(1)*a(2)

endfunction


! -------------------------------------------------------------------------------------------<<<<---GCD--<<<
subroutine add_point_int(xvec,nx,x,ix)

integer,dimension(:),pointer :: xvec
integer                      :: nx
integer                      :: x
integer,optional             :: ix

integer,dimension(:),pointer :: yvec
integer :: i,n

! --> check if input vector is associated
if (.not.associated(xvec)) then
  allocate(xvec(1))
  xvec(1) = x
  nx      = 1
  if (present(ix)) then
    ix = 1
  endif
  return
endif

! --> check if point is already in vector
do i=1,nx
  if (x.eq.xvec(i)) then
    if (present(ix)) then
      ix = i
    endif
    return ! point is already in vector, nothing to do
  endif
enddo

!
nullify(yvec)
allocate(yvec(nx))
do i=1,nx
  yvec(i) = xvec(i)
enddo

! --> 
deallocate(xvec)
nullify(xvec)
nx = nx+1
allocate(xvec(nx))

! --> find position of new entry
if (x.gt.yvec(nx-1)) then
  n = nx
else
  n = minloc(yvec,1,yvec.ge.x)
endif

! --> return position of new entry
if (present(ix)) then
  ix = n
endif

! --> fill vector with entries
do i=1,n-1
  xvec(i) = yvec(i)   ! before new entry
enddo

xvec(n)   = x         ! new entry

do i=n+1,nx
  xvec(i) = yvec(i-1) ! after new entry
enddo

deallocate(yvec)

endsubroutine


! -------------------------------------------------------------------------------------------<<<<---GCD--<<<
subroutine add_point(xvec,nx,x,ix,eps)

real(8),dimension(:),pointer :: xvec
integer                      :: nx
real(8)                      :: x
integer,optional             :: ix
real(8),optional             :: eps  ! change with ix

real(8),dimension(:),pointer :: yvec
real(8) :: limit
integer :: i,n

! --> check if input vector is associated
if (.not.associated(xvec)) then
  allocate(xvec(1))
  xvec(1) = x
  nx      = 1
  if (present(ix)) then
    ix = 1
  endif
  return
endif

! --> define minimum border, if difference between points is below eps, they are treated as equal
if (.not.present(eps)) then
  limit = 1e-12
else
  limit = eps
endif

! --> check if point is already in vector
do i=1,nx
  if (abs(x-xvec(i)).lt.limit) then
    if (present(ix)) then
      ix = i
    endif
    return ! point is already in vector, nothing to do
  endif
enddo

! --> 
nullify(yvec)
allocate(yvec(nx))
do i=1,nx
  yvec(i) = xvec(i)
enddo

! --> 
deallocate(xvec)
nullify(xvec)
nx = nx+1
allocate(xvec(nx))

! --> find position of new entry
if (x.gt.yvec(nx-1)) then
  n = nx
else
  n = minloc(yvec,1,yvec.ge.x)
endif

! --> return position of new entry
if (present(ix)) then
  ix = n
endif

! --> fill vector with entries
do i=1,n-1
  xvec(i) = yvec(i)   ! before new entry
enddo

xvec(n)   = x         ! new entry

do i=n+1,nx
  xvec(i) = yvec(i-1) ! after new entry
enddo

deallocate(yvec)

endsubroutine

! -------------------------------------------------------------------------------------------<<<<---WELCOME_SCREEN--<<<
subroutine total_diff(y,x,z)

real(8),dimension(:),intent(in)  :: x
real(8),dimension(:),intent(in)  :: y
real(8),dimension(:),intent(out) :: z

integer :: i,n

n = size(x)
if (n.lt.2) then
  z = y
endif

do i=1,n
  if (i.eq.1) then
    z(i) = (y(i+1)-y(i))/(x(i+1)-x(i))
  elseif (i.eq.n) then
    z(i) = (y(i)-y(i-1))/(x(i)-x(i-1))
  else
    z(i) = (y(i+1)-y(i-1))/(x(i+1)-x(i-1))
  endif
enddo

endsubroutine

! -------------------------------------------------------------------------------------------<<<<---WELCOME_SCREEN--<<<
subroutine trapz_vec(x,y,z)

real(8),dimension(:),intent(in)  :: x
real(8),dimension(:),intent(in)  :: y
real(8),dimension(:),intent(out) :: z

integer :: i,n

n = size(x)
if (n.lt.2) then
  z = y
endif

z(1) = dble(0)
do i=2,n
  z(i) = z(i-1) + (y(i-1)+y(i))/dble(2)*(x(i)-x(i-1))
enddo

endsubroutine


! -------------------------------------------------------------------------------------------<<<<---WELCOME_SCREEN--<<<
subroutine to_lower(str)

character(*), intent(in out) :: str
integer :: i
 
do i = 1, len(str)
  select case(str(i:i))
    case("A":"Z")
      str(i:i) = achar(iachar(str(i:i))+32)
  end select
enddo  

endsubroutine


! -------------------------------------------------------------------------------------------<<<<---WELCOME_SCREEN--<<<
TYPE(DUAL_NUM) function fdm1h(x)
!
! double precision rational minimax approximation of Fermi-Dirac integral of order k=-1/2
!
! Reference: Fukushima, T. (2014, submitted to App. Math. Comp.) 
!
! Author: Fukushima, T. <Toshio.Fukushima@nao.ac.jp>
!
TYPE(DUAL_NUM) x,ex,t,w,s,fd
TYPE (DUAL_NUM),PARAMETER:: factor=DUAL_NUM(2D0,0.D0)
!
!write(*,"(a20,1pe15.7)") "(fdm1h) x=",x
if(x.lt.-2.d0) then
    ex=exp(x)
    t=ex*7.38905609893065023d0
    fd=ex*(1.77245385090551603d0 &
    -ex*(40641.4537510284430d0 &
    +t*(9395.7080940846442d0 &
    +t*(649.96168315267301d0 &
    +t*(12.7972295804758967d0 &
    +t*0.00153864350767585460d0 &
    ))))/(32427.1884765292940d0 &
    +t*(11079.9205661274782d0 &
    +t*(1322.96627001478859d0 &
    +t*(63.738361029333467d0 &
    +t)))))
elseif(x.lt.0.d0) then
    s=-0.5d0*x
    t=1.d0-s
    fd=(272.770092131932696d0 &
    +t*(30.8845653844682850d0 &
    +t*(-6.43537632380366113d0 &
    +t*(14.8747473098217879d0 &
    +t*(4.86928862842142635d0 &
    +t*(-1.53265834550673654d0 &
    +t*(-1.02698898315597491d0 &
    +t*(-0.177686820928605932d0 &
    -t*0.00377141325509246441d0 &
    ))))))))/(293.075378187667857d0 &
    +s*(305.818162686270816d0 &
    +s*(299.962395449297620d0 &
    +s*(207.640834087494249d0 &
    +s*(92.0384803181851755d0 &
    +s*(37.0164914112791209d0 &
    +s*(7.88500950271420583d0 &
    +s)))))))
elseif(x.lt.2.d0) then
    t=0.5d0*x
    fd=(3531.50360568243046d0 &
    +t*(6077.5339658420037d0 &
    +t*(6199.7700433981326d0 &
    +t*(4412.78701919567594d0 &
    +t*(2252.27343092810898d0 &
    +t*(811.84098649224085d0 &
    +t*(191.836401053637121d0 &
    +t*23.2881838959183802d0 &
    )))))))/(3293.83702584796268d0 &
    +t*(1528.97474029789098d0 &
    +t*(2568.48562814986046d0 &
    +t*(925.64264653555825d0 &
    +t*(574.23248354035988d0 &
    +t*(132.803859320667262d0 &
    +t*(29.8447166552102115d0 &
    +t)))))))
elseif(x.lt.5.d0) then
    t=0.3333333333333333333d0*(x-2.d0)
    fd=(4060.70753404118265d0 &
    +t*(10812.7291333052766d0 &
    +t*(13897.5649482242583d0 &
    +t*(10628.4749852740029d0 &
    +t*(5107.70670190679021d0 &
    +t*(1540.84330126003381d0 &
    +t*(284.452720112970331d0 &
    +t*29.5214417358484151d0 &
    )))))))/(1564.58195612633534d0 &
    +t*(2825.75172277850406d0 &
    +t*(3189.16066169981562d0 &
    +t*(1955.03979069032571d0 &
    +t*(828.000333691814748d0 &
    +t*(181.498111089518376d0 &
    +t*(32.0352857794803750d0 &
    +t)))))))
elseif(x.lt.10.d0) then
    t=0.2d0*x-1.d0
    fd=(1198.41719029557508d0 &
    +t*(3263.51454554908654d0 &
    +t*(3874.97588471376487d0 &
    +t*(2623.13060317199813d0 &
    +t*(1100.41355637121217d0 &
    +t*(267.469532490503605d0 &
    +t*(25.4207671812718340d0 &
    +t*0.389887754234555773d0 &
    )))))))/(273.407957792556998d0 &
    +t*(595.918318952058643d0 &
    +t*(605.202452261660849d0 &
    +t*(343.183302735619981d0 &
    +t*(122.187622015695729d0 &
    +t*(20.9016359079855933d0 &
    +t))))))
elseif(x.lt.20.d0) then
    t=0.1d0*x-1.d0
    fd=(9446.00169435237637d0 &
    +t*(36843.4448474028632d0 &
    +t*(63710.1115419926191d0 &
    +t*(62985.2197361074768d0 &
    +t*(37634.5231395700921d0 &
    +t*(12810.9898627807754d0 &
    +t*(1981.56896138920963d0 &
    +t*81.4930171897667580d0 &
    )))))))/(1500.04697810133666d0 &
    +t*(5086.91381052794059d0 &
    +t*(7730.01593747621895d0 &
    +t*(6640.83376239360596d0 &
    +t*(3338.99590300826393d0 &
    +t*(860.499043886802984d0 &
    +t*(78.8565824186926692d0 &
    +t)))))))
elseif(x.lt.40.d0) then
    t=0.05d0*x-1.d0
    fd=(22977.9657855367223d0 &
    +t*(123416.616813887781d0 &
    +t*(261153.765172355107d0 &
    +t*(274618.894514095795d0 &
    +t*(149710.718389924860d0 &
    +t*(40129.3371700184546d0 &
    +t*(4470.46495881415076d0 &
    +t*132.684346831002976d0 &
    )))))))/(2571.68842525335676d0 &
    +t*(12521.4982290775358d0 &
    +t*(23268.1574325055341d0 &
    +t*(20477.2320119758141d0 &
    +t*(8726.52577962268114d0 &
    +t*(1647.42896896769909d0 &
    +t*(106.475275142076623d0 &
    +t)))))))
else
    w=1.d0/(x*x)
    t=1600.d0*w
    fd=sqrt(x)*factor*(1.d0 &
    -w*(0.411233516712009968d0 &
    +t*(0.00110980410034088951d0 &
    +t*(0.0000113689298990173683d0 &
    +t*(2.56931790679436797d-7 &
    +t*(9.97897786755446178d-9 &
    +t*8.67667698791108582d-10))))))
endif
fdm1h=fd/sqrt(PI)
return
endfunction

! -------------------------------------------------------------------------------------------<<<<---WELCOME_SCREEN--<<<
real*8 function fdm3h(x)
!
! double precision rational minimax approximation of Fermi-Dirac integral of order k=-3/2
!
! Reference: Fukushima, T. (2014, submitted to App. Math. Comp.) 
!
! Author: Fukushima, T. <Toshio.Fukushima@nao.ac.jp>
!
real*8 x,ex,t,w,s,fd,factor
parameter (factor=-2.d0)    ! = 1/(k+1)
!
if(x.lt.-2.d0) then
    ex=exp(x)
    t=ex*7.38905609893065023d0
    fd=ex*(-3.54490770181103205d0 &
    +ex*(82737.595643818605d0 &
    +t*(18481.5553495836940d0 &
    +t*(1272.73919064487495d0 &
    +t*(26.3420403338352574d0 &
    -t*0.00110648970639283347d0 &
    ))))/(16503.7625405383183d0 &
    +t*(6422.0552658801394d0 &
    +t*(890.85389683932154d0 &
    +t*(51.251447078851450d0 &
    +t)))))
elseif(x.lt.0.d0) then
    s=-0.5d0*x
    t=1.d0-s
    fd=-(946.638483706348559d0 &
    +t*(76.3328330396778450d0 &
    +t*(62.7809183134124193d0 &
    +t*(83.8442376534073219d0 &
    +t*(23.2285755924515097d0 &
    +t*(3.21516808559640925d0 &
    +t*(1.58754232369392539d0 &
    +t*(0.687397326417193593d0 &
    +t*0.111510355441975495d0 &
    ))))))))/(889.4123665319664d0 &
    +s*(126.7054690302768d0 &
    +s*(881.4713137175090d0 &
    +s*(108.2557767973694d0 &
    +s*(289.38131234794585d0 &
    +s*(27.75902071820822d0 &
    +s*(34.252606975067480d0 &
    +s*(1.9592981990370705d0 &
    +s))))))))
elseif(x.lt.2.d0) then
    t=0.5d0*x
    fd=-(754.61690882095729d0 &
    +t*(565.56180911009650d0 &
    +t*(494.901267018948095d0 &
    +t*(267.922900418996927d0 &
    +t*(110.418683240337860d0 &
    +t*(39.4050164908951420d0 &
    +t*(10.8654460206463482d0 &
    +t*(2.11194887477009033d0 &
    +t*0.246843599687496060d0 &
    ))))))))/(560.03894899770103d0 &
    +t*(70.007586553114572d0 &
    +t*(582.42052644718871d0 &
    +t*(56.181678606544951d0 &
    +t*(205.248662395572799d0 &
    +t*(12.5169006932790528d0 &
    +t*(27.2916998671096202d0 &
    +t*(0.53299717876883183d0 &
    +t))))))))
elseif(x.lt.5.d0) then
    t=0.3333333333333333333d0*(x-2.d0)
    fd=-(526.022770226139287d0 &
    +t*(631.116211478274904d0 &
    +t*(516.367876532501329d0 &
    +t*(267.894697896892166d0 &
    +t*(91.3331816844847913d0 &
    +t*(17.5723541971644845d0 &
    +t*(1.46434478819185576d0 &
    +t*(1.29615441010250662d0 &
    +t*0.223495452221465265d0 &
    ))))))))/(354.867400305615304d0 &
    +t*(560.931137013002977d0 &
    +t*(666.070260050472570d0 &
    +t*(363.745894096653220d0 &
    +t*(172.272943258816724d0 &
    +t*(23.7751062504377332d0 &
    +t*(12.5916012142616255d0 &
    +t*(-0.888604976123420661d0 &
    +t))))))))
elseif(x.lt.10.d0) then
    t=0.2d0*x-1.d0
    fd=-(18.0110784494455205d0 &
    +t*(36.1225408181257913d0 &
    +t*(38.4464752521373310d0 &
    +t*(24.1477896166966673d0 &
    +t*(9.27772356782901602d0 &
    +t*(2.49074754470533706d0 &
    +t*(0.163824586249464178d0 &
    -t*0.00329391807590771789d0 &
    )))))))/(18.8976860386360201d0 &
    +t*(49.3696375710309920d0 &
    +t*(60.9273314194720251d0 &
    +t*(43.6334649971575003d0 &
    +t*(20.6568810936423065d0 &
    +t*(6.11094689399482273d0 &
    +t))))))
elseif(x.lt.20.d0) then
    t=0.1d0*x-1.d0
    fd=-(4.10698092142661427d0 &
    +t*(17.1412152818912658d0 &
    +t*(32.6347877674122945d0 &
    +t*(36.6653101837618939d0 &
    +t*(25.9424894559624544d0 &
    +t*(11.2179995003884922d0 &
    +t*(2.30099511642112478d0 &
    +t*(0.0928307248942099967d0 &
    -t*0.00146397877054988411d0 &
    ))))))))/(6.40341731836622598d0 &
    +t*(30.1333068545276116d0 &
    +t*(64.0494725642004179d0 &
    +t*(80.5635003792282196d0 &
    +t*(64.9297873014508805d0 &
    +t*(33.3013900893183129d0 &
    +t*(9.61549304470339929d0 &
    +t)))))))
elseif(x.lt.40.d0) then
    t=0.05d0*x-1.d0
    fd=-(95.2141371910496454d0 &
    +t*(420.050572604265456d0 &
    +t*(797.778374374075796d0 &
    +t*(750.378359146985564d0 &
    +t*(324.818150247463736d0 &
    +t*(50.3115388695905757d0 &
    +t*(0.372431961605507103d0 &
    +t*(-0.103162211894757911d0 &
    +t*0.00191752611445211151d0 &
    ))))))))/(212.232981736099697d0 &
    +t*(1043.79079070035083d0 &
    +t*(2224.50099218470684d0 &
    +t*(2464.84669868672670d0 &
    +t*(1392.55318009810070d0 &
    +t*(346.597189642259199d0 &
    +t*(22.7314613168652593d0 &
    -t)))))))
else
    w=1.d0/(x*x)
    s=1.d0-1600.d0*w
    fd=factor/sqrt(x)*(1.d0 &
    +w*(12264.3569103180524d0 &
    +s*(3204.34872454052352d0 &
    +s*(140.119604748253961d0 &
    +s*0.523918919699235590d0 &
    )))/(9877.87829948067200d0 &
    +s*(2644.71979353906092d0 &
    +s*(128.863768007644572d0 &
    +s))))
endif
fdm3h=-fd/sqrt(PI)*0.5
return
endfunction

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! real*8 function fd1h(x)
! !
! ! double precision rational minimax approximation of Fermi-Dirac integral of order k=1/2
! !
! ! Reference: Fukushima, T. (2014, submitted to App. Math. Comp.) 
! !
! ! Author: Fukushima, T. <Toshio.Fukushima@nao.ac.jp>
! !
! real*8 x,ex,t,w,s,fd,factor
! parameter (factor=2.d0/3.d0)    ! = 1/(k+1)
! !
! !write(*,"(a20,1pe15.7)") "(fd1h) x=",x
! if(x.lt.-2.d0) then
!     ex=exp(x)
!     t=ex*7.38905609893065023d0
!     fd=ex*(0.886226925452758014d0 &
!     -ex*(19894.4553386951666d0 &
!     +t*(4509.64329955948557d0 &
!     +t*(303.461789035142376d0 &
!     +t*(5.7574879114754736d0 &
!     +t*0.00275088986849762610d0 &
!     ))))/(63493.915041308052d0 &
!     +t*(19070.1178243603945d0 &
!     +t*(1962.19362141235102d0 &
!     +t*(79.250704958640158d0 &
!     +t)))))
! elseif(x.lt.0.d0) then
!     s=-0.5d0*x
!     t=1.d0-s
!     fd=(149.462587768865243d0 &
!     +t*(22.8125889885050154d0 &
!     +t*(-0.629256395534285422d0 &
!     +t*(9.08120441515995244d0 &
!     +t*(3.35357478401835299d0 &
!     +t*(-0.473677696915555805d0 &
!     +t*(-0.467190913556185953d0 &
!     +t*(-0.0880610317272330793d0 &
!     -t*0.00262208080491572673d0 &
!     ))))))))/(269.94660938022644d0 &
!     +s*(343.6419926336247d0 &
!     +s*(323.9049470901941d0 &
!     +s*(218.89170769294024d0 &
!     +s*(102.31331350098315d0 &
!     +s*(36.319337289702664d0 &
!     +s*(8.3317401231389461d0 &
!     +s)))))))
! elseif(x.lt.2.d0) then
!     t=0.5d0*x
!     fd=(71652.717119215557d0 &
!     +t*(134954.734070223743d0 &
!     +t*(153693.833350315645d0 &
!     +t*(123247.280745703400d0 &
!     +t*(72886.293647930726d0 &
!     +t*(32081.2499422362952d0 &
!     +t*(10210.9967337762918d0 &
!     +t*(2152.71110381320778d0 &
!     +t*232.906588165205042d0 &
!     ))))))))/(105667.839854298798d0 &
!     +t*(31946.0752989314444d0 &
!     +t*(71158.788776422211d0 &
!     +t*(15650.8990138187414d0 &
!     +t*(13521.8033657783433d0 &
!     +t*(1646.98258283527892d0 &
!     +t*(618.90691969249409d0 &
!     +t*(-3.36319591755394735d0 &
!     +t))))))))
! elseif(x.lt.5.d0) then
!     t=0.3333333333333333333d0*(x-2.d0)
!     fd=(23744.8706993314289d0 &
!     +t*(68257.8589855623002d0 &
!     +t*(89327.4467683334597d0 &
!     +t*(62766.3415600442563d0 &
!     +t*(20093.6622609901994d0 &
!     +t*(-2213.89084119777949d0 &
!     +t*(-3901.66057267577389d0 &
!     -t*948.642895944858861d0 &
!     )))))))/(9488.61972919565851d0 &
!     +t*(12514.8125526953073d0 &
!     +t*(9903.44088207450946d0 &
!     +t*(2138.15420910334305d0 &
!     +t*(-528.394863730838233d0 &
!     +t*(-661.033633995449691d0 &
!     +t*(-51.4481470250962337d0 &
!     +t)))))))
! elseif(x.lt.10.d0) then
!     t=0.2d0*x-1.d0
!     fd=(311337.452661582536d0 &
!     +t*(1.11267074416648198d6 &
!     +t*(1.75638628895671735d6 &
!     +t*(1.59630855803772449d6 &
!     +t*(910818.935456183774d0 &
!     +t*(326492.733550701245d0 &
!     +t*(65507.2624972852908d0 &
!     +t*4809.45649527286889d0 &
!     )))))))/(39721.6641625089685d0 &
!     +t*(86424.7529107662431d0 &
!     +t*(88163.7255252151780d0 &
!     +t*(50615.7363511157353d0 &
!     +t*(17334.9774805008209d0 &
!     +t*(2712.13170809042550d0 &
!     +t*(82.2205828354629102d0 &
!     -t)))))))*0.999999999999999877d0
! elseif(x.lt.20.d0) then
!     t=0.1d0*x-1.d0
!     fd=(7.26870063003059784d6 &
!     +t*(2.79049734854776025d7 &
!     +t*(4.42791767759742390d7 &
!     +t*(3.63735017512363365d7 &
!     +t*(1.55766342463679795d7 &
!     +t*(2.97469357085299505d6 &
!     +t*154516.447031598403d0 &
!     ))))))/(340542.544360209743d0 &
!     +t*(805021.468647620047d0 &
!     +t*(759088.235455002605d0 &
!     +t*(304686.671371640343d0 &
!     +t*(39289.4061400542309d0 &
!     +t*(582.426138126398363d0 &
!     +t*(11.2728194581586028d0 &
!     -t)))))))
! elseif(x.lt.40.d0) then
!     t=0.05d0*x-1.d0
!     fd=(4.81449797541963104d6 &
!     +t*(1.85162850713127602d7 &
!     +t*(2.77630967522574435d7 &
!     +t*(2.03275937688070624d7 &
!     +t*(7.41578871589369361d6 &
!     +t*(1.21193113596189034d6 &
!     +t*63211.9545144644852d0 &
!     ))))))/(80492.7765975237449d0 &
!     +t*(189328.678152654840d0 &
!     +t*(151155.890651482570d0 &
!     +t*(48146.3242253837259d0 &
!     +t*(5407.08878394180588d0 &
!     +t*(112.195044410775577d0 &
!     -t))))))
! else
!     w=1.d0/(x*x)
!     s=1.d0-1600.d0*w
!     fd=x*sqrt(x)*0.666666666666666667d0*(1.d0+w &
!     *(8109.79390744477921d0 &
!     +s*(342.069867454704106d0 &
!     +s*1.07141702293504595d0)) &
!     /(6569.98472532829094d0 &
!     +s*(280.706465851683809d0 &
!     +s)))
! endif
! !write(*,"(a20,1p2e15.7)") "(fd1h) t,fd=",t,fd
! fd1h=fd
! return
! endfunction

real(8) function fd1h(x)
! wrapper for fd1h_dual below
real(8)        :: x
TYPE(DUAL_NUM) :: result

result = fd1h_dual(DUAL_NUM(x,0))
fd1h   = result%x_ad_

endfunction

real(8) function fd3h(x)
! wrapper for fd1h_dual below
real(8)        :: x
TYPE(DUAL_NUM) :: result

result = fd3h_dual(DUAL_NUM(x,0))
fd3h   = result%x_ad_

endfunction

real(8) function fd1h_inv(x)
! wrapper for fd1h_dual below
real(8)        :: x
TYPE(DUAL_NUM) :: result

result   = fd1h_inv_dual(DUAL_NUM(x,0))
fd1h_inv = result%x_ad_

endfunction

TYPE(DUAL_NUM) function fd1h_dual(x)
!
! an analytical approximation for the fermi-dirac integral F1/2
!
! Author: Aymerich
!
! TODO: maybe replace with" 
! A generalized approximation of the Fermi–Dirac integrals
! "
TYPE(DUAL_NUM) x,a,b,c,two_over_pi
!constants
a = DUAL_NUM(9.6,0)
b = DUAL_NUM(2.13,0)
c = DUAL_NUM(2.4,0)

two_over_pi = dble(2)/sqrt(pi)

if (x.le.-100) then
  fd1h_dual   = exp(x)
else
  fd1h_dual   = two_over_pi*(dble(4.24264068712)/(b+x+(abs(x-b)**c+a)**(1/c))**1.5 + exp(-x)/(0.5*sqrt(pi)) )**(-1)
endif
return
endfunction

TYPE(DUAL_NUM) function fd3h_dual(x)
!
! an analytical approximation for the fermi-dirac integral F1/2
!
! Author: Aymerich
!
! TODO: maybe replace with" 
! A generalized approximation of the Fermi–Dirac integrals
! "
TYPE(DUAL_NUM) x,a,b,c,two_over_pi
!constants
a = DUAL_NUM(14.9,0)
b = DUAL_NUM(2.64,0)
c = DUAL_NUM(dble(2.25),0)

two_over_pi = dble(4)/dble(3)/sqrt(pi)
fd3h_dual   = two_over_pi*(dble(14.1421356237)/(b+x+(abs(x-b)**c+a)**(1/c))**2.5 + exp(-x)/(0.75*sqrt(pi)) )**(-1)
return
endfunction

TYPE(DUAL_NUM) function fd1h_inv_dual(x)
!
! Nilsson, N.G. (1973), 
! An accurate approximation of the generalized einstein relation for degenerate semiconductors. phys. stat. sol. 
! (a), 19: K75-K78. https://doi.org/10.1002/pssa.2210190159
! equation (8) there
! "
TYPE(DUAL_NUM) x,mid

mid           = (dble(3)*sqrt(pi)*x/dble(4))**(dble(2)/dble(3))
fd1h_inv_dual = log(x)/(1-x**2) + mid/(1+(0.24+1.08*mid)**(-2))

return
endfunction

real(8) function dfd1h(x)
!
! an analytical approximation for the fermi-dirac integral d/dnu F1/2
!
! Author: Aymerich
!
real*8 a,b,c,two_over_pi,x
!constants
a = 9.6
b = 2.13
c = 2.4

two_over_pi = 2/sqrt(pi)
!analytical derivative of approximation for Fermi(1/2) obtained in file fermi_tests.py in 2valley project using sympy.
dfd1h = two_over_pi*(-4.24264068712*(-1.5 + 1.5*(a + abs(b - x)**c)**(1/c)*abs(b - x)**c*sign(dble(1),b - x)/((a + abs(b - x)**c)*abs(b - x)))*(b + x + (a + abs(b - x)**c)**(1/c))**(-2.5) + 2.0*exp(-x)/sqrt(pi))/(4.24264068712*(b + x + (a + abs(b - x)**c)**(1/c))**(-1.5) + 2.0*exp(-x)/sqrt(pi))**2
return
endfunction

TYPE(DUAL_NUM) function dfd1h_dual(x)
!
! an analytical approximation for the fermi-dirac integral d/dnu F1/2
!
! Author: Aymerich
!
real*8 a,b,c,two_over_pi
TYPE(DUAL_NUM) :: x
!constants
a = 9.6
b = 2.13
c = 2.4

two_over_pi = 2/sqrt(pi)
!analytical derivative of approximation for Fermi(1/2) obtained in file fermi_tests.py in 2valley project using sympy.
dfd1h_dual = two_over_pi*(-4.24264068712*(-1.5 + 1.5*(a + abs(b - x)**c)**(1/c)*abs(b - x)**c*sign(dble(1),b - x)/((a + abs(b - x)**c)*abs(b - x)))*(b + x + (a + abs(b - x)**c)**(1/c))**(-2.5) + 2.0*exp(-x)/sqrt(pi))/(4.24264068712*(b + x + (a + abs(b - x)**c)**(1/c))**(-1.5) + 2.0*exp(-x)/sqrt(pi))**2
return
endfunction

pure function integrate(x, y) result(r)
  !! Calculates the integral of an array y with respect to x using the trapezoid
  !! approximation. Note that the mesh spacing of x does not have to be uniform.
  real(8), intent(in)  :: x(:)         !! Variable x
  real(8), intent(in)  :: y(size(x))   !! Function y(x)
  real(8)              :: r            !! Integral ∫y(x)·dx
  integer              :: n

  n = size(x)

  ! Integrate using the trapezoidal rule
  r = sum((y(1+1:n-0) + y(1+0:n-1))*(x(1+1:n-0) - x(1+0:n-1)))/2
  
end function

function dintegrate_dk(x, y, k) result(dr_dk)
  !! Calculates the derivative of the integral with respect to y(k)
  real(8), intent(in)    :: x(:)         !! Variable x
  real(8), intent(inout) :: y(size(x))   !! Function y(x)
  integer, intent(in)    :: k

  real(8) :: dr_dk

  dr_dk = (( x(k+1)-x(k) ) + ( x(k) - x(k-1) ) )/2

end function

TYPE(DUAL_NUM_2)  function dual_to_quad(x)
  ! Convert dual number to quadruple precision
  TYPE(DUAL_NUM), intent(in)    :: x

  dual_to_quad%x_ad_ = x%x_ad_
  dual_to_quad%xp_ad_ = x%xp_ad_

end function

TYPE(DUAL_NUM)  function quad_to_dual(x)
  ! Convert quad dual number to double precision dual number
  TYPE(DUAL_NUM_2), intent(in)    :: x

  quad_to_dual%x_ad_ = x%x_ad_
  quad_to_dual%xp_ad_ = x%xp_ad_

end function



end module
