!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! functions for general purpose operations
module utils

implicit none

type string
    character(len=:), allocatable :: str
endtype

private

public toUpper
public clip
public CMPTRPRND
public REMAP_VEC
public string

contains

FUNCTION toUpper(s1)  RESULT (s2)
    !!!!!!!!!!!!!!!!!!!!!!!!
    ! cast string to upper
    !
    ! input
    ! -----
    ! s1 : character(*)
    !   String to cast to upper.
    !
    ! output
    ! ------
    !   String S1 casted to upper.
    !!!!!!!!!!!!!!!!!!!!!!!!
    CHARACTER(*)       :: s1
    CHARACTER(LEN(s1)) :: s2
    CHARACTER          :: ch
    INTEGER,PARAMETER  :: DUC = ICHAR('A') - ICHAR('a')
    INTEGER            :: i

    DO i = 1,LEN(s1)
        ch = s1(i:i)
        IF (ch >= 'a'.AND.ch <= 'z') ch = CHAR(ICHAR(ch)+DUC)
        s2(i:i) = ch
    END DO
END FUNCTION toUpper

pure elemental real(8) function clip(x, cut_low)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! clip to, e.g. if cut_low=True, set elements smaller than 0 to 0, 
    ! if cut_low=True set elements larger than 0 to zero.
    !
    ! input
    ! -----
    ! x : real(8)
    !   The number to clip
    ! cut_low : logical
    !   If true, set numbers smaller than 0 zo 0. Else set number larger than zero to 0.
    !
    ! output
    ! ------
    ! clip : real(8)
    !   The clipped number.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical, intent(in) :: cut_low
    real(8), intent(in)    :: x
    if (cut_low) then
        if (x.LE.0.0) then
            clip = 0
        else
            clip = x
        endif
    else
      if (x.GE.0.0) then
          clip = 0
      else
          clip = x
      endif
    endif
end function clip

DOUBLE PRECISION FUNCTION CMPTRPRND(x_p,xyz_d,rnd_s)
  ! rounding of trapezoidal material composition profile
  real(8), intent(in) :: x_p, xyz_d, rnd_s
  real(8) :: a_s, c_u, b_p, a_p
  a_s=1.0D0/xyz_d         ! slope
  c_u=a_s*rnd_s           ! function value at 2d
  b_p=(2.0D0*a_s*rnd_s-c_u)/(4.0D0*rnd_s*rnd_s)   ! quadratic coeff
  a_p=a_s-4.0D0*rnd_s*b_p                         ! linear coeff
  CMPTRPRND=(a_p+b_p*x_p)*x_p                     ! polynomial value
END

INTEGER FUNCTION REMAP_VEC(vec_1,index_1,vec_2) result(index_2)
  ! this function finds index_2, such that abs(vec_1(index_1) - vec_2(index_2)) is minimal.
  ! similar to find_nearest
  real(8),dimension(:) :: vec_1,vec_2
  integer :: index_1

  real(8),dimension(:),allocatable :: difference

  allocate(difference(size(vec_2)))
  difference = abs(vec_2-vec_1(index_1))
  index_2    = minloc(difference,1)
   
END FUNCTION REMAP_VEC

endmodule