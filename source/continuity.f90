!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of Hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



!> \brief functions for continutiy equations (DD)
!> \details here general classes that can be used to implement continuity equations are defined
module continuity

use dd_types

use crs_matrix   , only : crs_mat_cmplx          !< crs complex matrix type
use crs_matrix   , only : crs_mat_real           !< crs real matrix type
use crs_matrix   , only : dealloc_crs            !< deallocate crs matrix
use crs_matrix   , only : copy_crs               !< copy crs matrix
use crs_matrix   , only : copy_crs               !< copy crs real to complex
use crs_matrix   , only : convert_crs_ccs        !< conversion from CRS to CCS matrix

use contact      , only : TR_IT                  !< number of time step iteration
use contact      , only : TR_DT                  !< dt between time steps
use contact      , only : CON                    !< information on contact
use contact      , only : SOURCE                 !< id to source contact
use contact      , only : DRAIN                  !< id to drain contact
use contact      , only : GATE                   !< id to gate contact
use contact      , only : AC_SIM                 !< if true: AC simulation activated
use contact      , only : OP

use mUMFPACK                                     !< UMFPACK library used for solveing equation system

use dd_types, only      : dd_point_semi          !< (N_SP) allows to acces traps with TRAP_DIM conveniently
use dd_types, only      : NORM                   !> information regarding normalization

use semiconductor, only : SP                     !< contains information for semiconductor points
use semiconductor, only : SEMI_DIM               !< dimension of semiconductor
use semiconductor, only : BBT                    !< if true: band-to-band tunneling enabled
use semiconductor, only : HOLE                   !< if true: hole carriers activated
use semiconductor, only : ELEC                   !< if true: electron carriers activated
use semiconductor, only : ELEC2                  !< if true: electron second band
use semiconductor, only : TRAP_DIM               !< dimension of trap continuity equations
use semiconductor, only : CN                     !< (N_SP) electron density
use semiconductor, only : CN2                    !< (N_SP) electron density second band
use semiconductor, only : CN_TRAP                !< (N_SP) trapped electron density
use semiconductor, only : CP                     !< (N_SP) hole density
use semiconductor, only : CP_TRAP                !< (N_SP) trapped hole density
use semiconductor, only : CN_LAST                !< (N_SP) electron density from last iteration
use semiconductor, only : CN2_LAST               !< (N_SP) electron density from last iteration
use semiconductor, only : CP_LAST                !< (N_SP) hole density from last iteration
use semiconductor, only : CHARGE_DIM             !< maximum dimension of space charge
use semiconductor, only : CN_TRAP_LAST           !< (N_SP) trapped electron density from last iteration
use semiconductor, only : CP_TRAP_LAST           !< (N_SP) trapped hole density from last iteration

use profile      , only : CD                     !< (N_SP) doping charge density

use semiconductor_globals, only : N_SP           !< number of semiconductor points
use semiconductor_globals, only : TEMP_AMBIENT   !< ambient temperature of the simulation

use read_user    , only : US                     !< user input parameter

use bandstructure, only : SEMI                   !< structure containing band information
use bandstructure, only : band_pointer           !< structure containing band information

use heterostructure, only: get_valence_band_index      !return index of valence band
use heterostructure, only: get_conduction_band_index   !return index of lowest conduction band
use heterostructure, only: get_conduction_band_index2  !return index of lowest conduction band

use math_oper    , only : get_scal               !< set scaling vector for charges

use structure, only     : NXYZ                   !< total number of points
use structure, only     : NX1                    !< total number of points
use structure, only     : NY1                    !< total number of points
use structure, only     : NZ1                    !< total number of points
use structure, only     : POINT                  !< total number of points


use save_global  , only : write_vec           !< write value
use save_global  , only : save_elpa           !< write value
use utils        , only : string              !< write value


implicit none

!define an enum that is used to identify the solution variables of all continuity equations
enum, bind( C ) 
    enumerator :: psi_=1, phin_=2, phin2_=3, phip_=4, tn_=5, tl_=6, ln_=7, nvar=8 !nvar is the number of solution variables+1
end enum  

!allows other modules to call init_cont equally for all continuity equations implemented
interface
    subroutine init_cont(var)
    integer :: var
    endsubroutine init_cont
endinterface

!allows other modules to call set_cont equally for all continuity equations implemented
interface
    subroutine set_cont(simul)
    logical,intent(in) :: simul
    endsubroutine set_cont
endinterface

!allows other modules to call set_cont_ac equally for all continuity equations implemented
interface
    subroutine set_cont_ac(omega)
        real(8),intent(in) :: omega !< 2*pi*frequency
    endsubroutine set_cont_ac
endinterface

! ---> continuity equation parent type -------------------------------------------------------------------------------------------
type, abstract :: cont
    type(crs_mat_real) ,pointer,dimension(:)     :: jacobi    => null()  !< array of real jacobi matrices with respect to enum::solution_variables
    type(crs_mat_cmplx),pointer,dimension(:)     :: jacobi_ac => null()  !< array of complex jacobi matrices with respect to enum::solution_variables
    real(8),pointer,dimension(:)                 :: rhs       => null()  !< rhs of this continuity equation 
    real(8),pointer,dimension(:)                 :: delta     => null()  !< the change in the solution variable after a DC or TR newton step
    complex(8),pointer,dimension(:)              :: delta_ac  => null()  !< the change in the solution variable after a AC newton step 
    !   type(MKL_DSS_HANDLE)                         :: handle               !< mkl handle of this continuity equation
    logical                                      :: active    = .false.  !< if >=1: This continuity equation is initalized
    logical                                      :: has_converged = .false.  !< if >=1: This continuity equation has converged
    integer                                      :: valley               !< index of band valley in SEMIs
    integer                                      :: i_var                !< index to solution variable of this continuity equation in enum::solution_variables
    real(8),allocatable,dimension(:)             :: var                  !< solution variable (N_SP) of this continuity equation
    real(8),allocatable,dimension(:)             :: var_last             !< solution variable (N_SP) of this continuity equation from last operating point
    real(8)                                      :: atol                 ! absolute tolerance
    real(8)                                      :: rtol                 ! relative tolerance
    real(8)                                      :: ctol                 ! tolerance rhs
    real(8)                                      :: delta_max            ! maximum change of the solution variable
    contains
    procedure,pass                               :: close_        ! deallocate and close everything that belongs to the cont object
    !procedure,pass                               :: init_handle   ! init the intel MKL handle
    procedure,pass                               :: solve         ! solve the main diagonal jacobian with respect to i_var (successive solution)
    procedure,pass                               :: update        ! update the solution variable using delta
    procedure,pass                               :: save          ! store the solution variable into var_last
    procedure,pass                               :: restore       ! go back from current var to var_last
    procedure,pass                               :: smooth_var    ! smooth solution variable
    procedure,pass                               :: print_status  ! print information regarding this continuity equation
    procedure,pass                               :: print_debug   ! print debug information
    procedure(init_cont),nopass,deferred         :: init          ! init routine for this continuity equation
    procedure(set_cont),nopass,deferred          :: set           ! set routine for this continuity equation
    procedure(set_cont_ac),nopass,deferred       :: set_ac        ! set the AC jacobians of this continuity equation
    procedure,pass                               :: converged     ! check if this continuity equation has converged
    procedure,pass                               :: normalize     ! normalize the continuity equation
endtype

type(dd_point_semi),dimension(:),pointer :: DN  => null() !information related to carriers in first conduction band
type(dd_point_semi),dimension(:),pointer :: DN2 => null() !information related to carriers in second conduction band
type(dd_point_semi),dimension(:),pointer :: DP  => null() !information related to carriers in valence band

real(8) :: PDISSRTH !total dissipated power in device
integer :: NCCS = 0

!array that can be used to store continuity equation
type :: cont_pointer
    class(cont), pointer :: p
endtype

private

public cont_pointer
public PDISSRTH

public psi_,phin_,tn_,phin2_,phip_,tl_,ln_,nvar
public DN,DN2,DP
public cont

contains

subroutine converged(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! check if the coninuity equation "this" has converged.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equaiton to check.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont) :: this !limit for absolute potential change at point

    real(8) :: rhs_max
    logical :: atol_ok 
    logical :: rtol_ok
    integer :: n
    real(8) :: rtol, atol, ctol

    atol = this%atol
    rtol = this%rtol
    ctol = this%ctol

    ! if (this%i_var.eq.ln_) then
    !   this%has_converged = .true.
    !   return
    ! endif

    ! if (this%i_var.eq.phin2_) then
    !     this%has_converged = .true.
    !     return
    ! endif

    if (.not.this%active)  then
        this%has_converged = .true.
        return
    endif

    !if all checks off, return true
    if ((ctol.eq.0).and.(rtol.eq.0).and.(atol.eq.0)) then
        this%has_converged = .true.
        return
    endif

    !check if max value on rhs. e.g. div J_n is ok with respect to contact current
    rhs_max  = maxval(abs(this%rhs))
    if (this%ctol.ne.0) then 
        if ((rhs_max*NORM%J_N.lt.this%ctol)) then !recast to real units
            this%has_converged = .true.
            return
        endif
    endif

    !check absolute vale
    atol_ok   = all(abs(this%delta).lt.atol)
    if (.not.atol_ok) then
        this%has_converged = .false.
        return
    endif

    !check relative change
    rtol_ok   = .true.
    do n=1,size(this%delta)
        if ( this%delta(n).lt.1e-9)                           cycle !if values if very small relative error makes no sense
        if ( this%delta(n).eq.dble(0))                        cycle
        if ( this%var(n).eq.dble(0)         )                 cycle
        if ( (abs(this%delta(n)/this%var(n)) ).lt.this%rtol ) cycle
        this%has_converged = .false.
        return

    enddo

    !this point in the code is only reached if everythig is ok
    this%has_converged = .true.
endsubroutine

subroutine close_(this,var)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! deallocate everything and release the MKL handle.
    ! If Var is given, deallocate only the jacobians with respect to it.
    ! If all jacobians are not active, deallocate the MKL handle.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation whose equations shall be deallocated.
    ! var : integer,optional
    !   Index to solution variable whose jacobian shall be deallocated.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont),intent(inout) :: this
    integer,optional,intent(in) :: var
    integer :: i
    !integer :: error


    if (present(var)) then
        call dealloc_crs(this%jacobi(var))
        if (AC_SIM) then
            call dealloc_crs(this%jacobi_ac(var))
        endif

    else
        this%active=.false.
        do i=1,nvar-1
            if (this%jacobi(i)%active) then
                call dealloc_crs(this%jacobi(i))
                if (AC_SIM) then
                    call dealloc_crs(this%jacobi_ac(i))
                endif
            endif
        enddo
    endif

    ! if (.not.any(this%jacobi(:)%active)) then
    !     error = DSS_DELETE(this%handle,MKL_DSS_DEFAULTS)
    ! endif
endsubroutine

subroutine solve(this,lambda,set)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! solve the continuity equation with respect to its solution variable.
    !
    ! this : cont
    !   The continuity equation that shall be solved
    ! lambda : real
    !   Damping factor for solution
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this
    real(8)              :: lambda
    ! TYPE(MKL_DSS_HANDLE) :: handle1
    real(8), dimension(20) :: control
    real(8), dimension(90) :: info
    logical,optional :: set
    integer :: sys
    integer :: n ,i 
    integer(c_intptr_t) :: numeric, symbolic
    type(crs_mat_real)  :: ccs_mat
    type(string)                       :: jacobi_cols(100)       !columns of inqu file
    character(len=200)                 :: name
    real(8),dimension(:,:),allocatable :: jacobi_vals            ! values of inqu file
    character(len=5)                   :: n_iter_str


    if (.not.(this%active)) return

    ! set this continuity equation if "set" not given 
    ! or according to input
    if (present(set)) then
        if (set) then
            call this%set(.false.)
        endif
    else
        call this%set(.false.)
    endif

    ! --> factorise matrix
    ! handle1 = this%handle
    ! error   = DSS_FACTOR_REAL_D(handle1, MKL_DSS_DEFAULTS, this%jacobi(this%i_var)%values )
      
    ! --> solve for new QFP, and DEF using intel_DSS
    ! error   = DSS_SOLVE_REAL_D( handle1, MKL_DSS_DEFAULTS, this%rhs, 1, this%delta )

    !convert crs to ccs (but matrix is already allocated!)

    call ccs_mat%init(nr=this%jacobi(this%i_var)%nz , nc=this%jacobi(this%i_var)%nc, nz=this%jacobi(this%i_var)%nz, is_ccs=.true.)
    call convert_crs_ccs(this%jacobi(this%i_var),ccs_mat)
    n            = ccs_mat%nc

    ! 0 based indexing
    ccs_mat%row_index = ccs_mat%row_index - 1
    ccs_mat%columns   = ccs_mat%columns   - 1

    !set default parameters
    call umf4def (control)

    !print control parameters.  set control (1) to 1 to print error messages only
    control(1) = 2
    ! call umf4pcon (control)

    ! pre-order and DD_SIMUL_SYMBOLIC analysis (May be re-used if this is slow)
    call umf4sym (n, n, ccs_mat%columns, ccs_mat%row_index, ccs_mat%values, symbolic, control, info)

    !  check umf4sym error condition
    if (info (1) .lt. 0) then
        print *, 'Error occurred in umf4sym: ', info (1)
        stop
    endif

    !DD_SIMUL_NUMERIC factorization
    call umf4num (ccs_mat%columns, ccs_mat%row_index, ccs_mat%values, symbolic, numeric, control, info)

    !check umf4num error condition
    if (info (1) .lt. 0) then
        print *, 'Error occurred in umf4num: ', info (1)
        stop
    endif

    !solve Ax=b, without iterative refinement
    sys = 0
    call umf4sol (sys, this%delta, this%rhs, numeric, control, info)
    if (info (1) .lt. 0) then
        print *, 'Error occurred in umf4sol: ', info (1)
        stop

    endif

    NCCS = NCCS + 1
    if (US%dd%debug.ge.1) then 
        !prepare name of jacobian
        name = 'ccs_mat'
        write(n_iter_str,"(I3.3)") NCCS
        !change "niter" to something else
        name = trim(name)//'_niter'//trim(n_iter_str)
        name = trim(name)//'_dd_debug.elpa'
        allocate(jacobi_vals(10,size(ccs_mat%values)))
        jacobi_vals = 0

        i = 0

        i = i + 1
        jacobi_cols(i)%str = 'values'
        jacobi_vals(i,1:size(ccs_mat%values))   = ccs_mat%values

        i = i + 1
        jacobi_cols(i)%str = 'i_row'
        jacobi_vals(i,1:size(ccs_mat%row_index))   = ccs_mat%row_index
        if (any(jacobi_vals(i,:).gt.1e30)) then
            write(*,*) '***error*** problem with jacobi'
        endif

        i = i + 1
        jacobi_cols(i)%str = 'i_col'
        jacobi_vals(i,1:size(ccs_mat%columns))   = ccs_mat%columns

        i = i + 1
        jacobi_cols(i)%str = 'x'
        jacobi_vals(i,1:size(this%delta))   = this%delta

        i = i + 1
        jacobi_cols(i)%str = 'rhs'
        jacobi_vals(i,1:size(this%rhs))   = this%rhs

        ! should be converted to save_hdf
        call save_elpa(name, jacobi_cols, jacobi_vals)

    endif

    ! free the DD_SIMUL_SYMBOLIC analysis
    call umf4fsym (symbolic)

    ! free the DD_SIMUL_NUMERIC factorization
    call umf4fnum (numeric)
      
    call this%update(lambda)

    call dealloc_crs(ccs_mat)

endsubroutine

subroutine update(this,lambda)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! update the solution variable vector var of the continuity equation using the delta from the solution.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation to update.
    ! lambda : real
    !   The damping factor for the solution.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this
    real(8)              :: lambda,d=0,delta_max
    integer              :: n

    if (.not.(this%active)) return

    delta_max = this%delta_max

    do n=1,size(this%var)
        d = lambda * this%delta(n)
        if (d.gt.delta_max)    d = delta_max
        if (d.lt.(-delta_max)) d = -delta_max
        this%var(n) = this%var_last(n) + d
    enddo

endsubroutine

subroutine save(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! store the current solution variable in the last_var property of this.
    ! 
    ! input
    ! -----
    ! this : cont
    !   The continuity equation that shall save.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this

    if (.not.(this%active)) return

    this%var_last = this%var

endsubroutine

subroutine restore(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the current var to var_last.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation to restore.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this

    if (.not.(this%active)) return

    this%var = this%var_last

endsubroutine

subroutine smooth_var(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! smooth the solution variable along x-direction.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation that shall be smoothed.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this
    integer j,m,p

    if (.not.(this%active)) return

    do j=1,N_SP
        if (SP(j)%x_low_exist.and.SP(j)%x_upp_exist) then
            m = SP(j)%id_sx_low
            p = SP(j)%id_sx_upp
            if (((this%var(j)-this%var(m))*(this%var(p)-this%var(j))).lt.dble(0)) then
                this%var(j)         = (this%var(m)*SP(j)%dx_upp+this%var(p)*SP(j)%dx_low)/(SP(j)%dx_upp+SP(j)%dx_low)
            endif
        endif
    enddo

endsubroutine

subroutine normalize(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! normalize the variable of this continuity equation.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation that shall normalize itself.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this

    this%ctol = this%ctol*NORM%J_N_inv

endsubroutine

subroutine print_status(this)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! print the norm2 of this%delta to the terminal, or "ok" if the continuity equation has converged.
    !
    ! input
    ! -----
    ! this : cont
    !   The continuity equation that shall print its status
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)          :: this

    if (.not.(this%active)) return

    if (this%has_converged) then
        write(6,'(A)',advance='no') '     ok    | '
    else
        call write_vec(norm2(this%delta),.false.,'     | ')
    endif

endsubroutine

subroutine print_debug(this,n_iter)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! store the jacobians of this continuity equation at iteration n_iter to the directory where hdev has been called, for debugging.
    !
    ! input
    ! -----
    ! this : cont
    !  The continuity equation that shall store its debug information.
    ! n_iter : integer
    !  The current iteration number.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(cont)                        :: this
    type(string)                       :: jacobi_cols(100)       !columns of inqu file
    real(8),dimension(:,:),allocatable :: jacobi_vals            ! values of inqu file
    integer                            :: i,n,n_iter
    character(len=200)                 :: name
    character(len=5)                   :: n_iter_str
    integer                            :: n_vals

    if (.not.(this%active)) return

    n_vals = 0
    do n=1,size(this%jacobi)
        if (.not.this%jacobi(n)%active) cycle
        if (size(this%jacobi(n)%values).gt.n_vals) n_vals = size(this%jacobi(n)%values)
    enddo
    allocate(jacobi_vals(10,n_vals))
    jacobi_vals = 0

    do n=1,size(this%jacobi)
        if (.not.this%jacobi(n)%active) cycle

        !prepare name of jacobian
        if (this%i_var.eq.psi_) then
            name = '_f_psi'
        elseif (this%i_var.eq.phip_) then
            name = '_f_phi'
        elseif (this%i_var.eq.phin_) then
            name = '_f_phi'
        elseif (this%i_var.eq.phin2_) then
            name = '_f_phin2'
        elseif (this%i_var.eq.tl_) then
            name = '_f_tl'
        endif

        !name of derivative var
        if (n.eq.psi_) then
            name = trim(name)//'_dpsi'
        elseif (n.eq.phip_) then
            name = trim(name)//'_dphi'
        elseif (n.eq.phin_) then
            name = trim(name)//'_dphi'
        elseif (n.eq.phin2_) then
            name = trim(name)//'_dphin2'
        elseif (n.eq.tl_) then
            name = trim(name)//'_dtl'
        endif

        write(n_iter_str,"(I3.3)") n_iter
        name = trim(name)//'_niter'//trim(n_iter_str)
        name = trim(name)//'_dd_debug.elpa'

        i = 0

        i = i + 1
        jacobi_cols(i)%str = 'values'
        jacobi_vals(i,1:size(this%jacobi(n)%values))   = this%jacobi(n)%values

        i = i + 1
        jacobi_cols(i)%str = 'i_row'
        jacobi_vals(i,1:size(this%jacobi(n)%row_index))   = this%jacobi(n)%row_index
        if (any(jacobi_vals(i,:).gt.1e30)) then
            write(*,*) '***error*** problem with jacobi'
        endif

        i = i + 1
        jacobi_cols(i)%str = 'i_col'
        jacobi_vals(i,1:size(this%jacobi(n)%columns))   = this%jacobi(n)%columns

        ! should be converted to save_hdf
        call save_elpa(name, jacobi_cols, jacobi_vals)

    enddo
endsubroutine

endmodule
