!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! properties of semiconductor
module semiconductor 

use phys_const
use read_user    , only : US
use save_global  , only : DEB_ID
use save_global  , only : FILE
use save_global  , only : FORMAT_REAL
use save_global  , only : format_int
use structure

use bandstructure, only : band_pointer
use bandstructure, only : SEMI
use bandstructure, only : FE_TEMP
! use bandstructure, only : check_subband_bbt !< check if band to band tunneling is ok
use bandstructure, only : get_n_equ_band_dual
use bandstructure, only : get_fermi_equ

use profile, only: CD
use profile, only: CDTOT
use profile, only: DONATORS
use profile, only: ACCEPTORS
use profile, only: STRAIN
use profile, only: GRADING
use profile, only: GRADING2

use utils, only : CMPTRPRND
use utils, only : REMAP_VEC

use hdf_functions, only : read_col_hdf5


use semiconductor_globals, only : VT_LATTICE
use semiconductor_globals, only : TEMP_LATTICE
use semiconductor_globals, only : TEMP_AMBIENT
use semiconductor_globals, only : N_SP
use semiconductor_globals, only : UN
use semiconductor_globals, only : EC0
use semiconductor_globals, only : EV0
use semiconductor_globals, only : BGN_SEMI
use semiconductor_globals, only : MU_LF
use semiconductor_globals, only : UN2
use semiconductor_globals, only : UP
use semiconductor_globals, only : HETERO

implicit none

!Semiconductor-Point structure. This struct is available for every point in the semiconductor.
type sp_struct
  ! -- material/boundary --
  integer :: box_id           = 0          !< index to BOX struct
  integer :: cont_id          = 0          !< index to CON struct
  integer :: model_id         = 1          !< index to SEMI struct
  integer :: xline_id         = 1          !< index to x line (obsolete?)
  logical :: cont             = .false.    !< if true: is contact point
  logical :: supply           = .false.    !< if true: is supply point
  logical :: set_qfn          = .false.    !< if true : set quasi fermi potential of electrons at point
  logical :: set_qfn2         = .false.    !< if true : set quasi fermi potential of electrons at point
  logical :: set_qfp          = .false.    !< if true : set quasi fermi potential of holes at contact point
  logical :: set_qf           = .false.    !< if true: set quasi fermi potential of holes and electrons at contact point
  logical :: set_psi          = .false.    !< if true: set electrostatic potential at contact point
  logical :: set_neutrality_n = .false.    !< if true: use neutrality boundary condition for electrons
  logical :: set_neutrality_p = .false.    !< if true: use neutrality boundary condition for holes
  logical :: heterojunction   = .false.    !< if true: is a heterojunction point
  real(8) :: fac_heterojunction  =1        !< factor for heterojunction boundary condition
  ! -- index to point --
  integer :: id_p   =1                     !< index to point
  integer :: id_p_get  =0                  !< ?
  logical :: psi_offset  =.false.          !< if true: semi potential and semi charge not on same line (obsolete)
  integer,dimension(:), pointer :: id_p_get_mean => null()       !< index to point
  logical :: psi_mean   =.false.         !< if true: semi potential and semi charge not on same line
  integer :: id_x       =1        !< index to x disc vector
  integer :: id_y       =1        !< index to y disc vector
  integer :: id_z       =1        !< index to z disc vector
  integer :: id_sx      =1        !< index to semi x point
  ! -- borders --
  logical :: x_low_exist = .false.      !< if true: lower semi x point exists
  logical :: x_upp_exist = .false.      !< if true: upper semi x point exists
  logical :: y_low_exist = .false.      !< if true: lower semi y point exists
  logical :: y_upp_exist = .false.      !< if true: upper semi y point exists
  logical :: z_low_exist = .false.      !< if true: lower semi z point exists
  logical :: z_upp_exist = .false.      !< if true: upper semi z point exists
  ! -- left/upper side of semi/cont boundary --
  logical :: cont_left  = .false.     !< if true: contact region at left side
  logical :: cont_right = .false.     !< if true: contact region at right side
  logical :: cont_lower = .false.     !< if true: contact region at lower side
  logical :: cont_upper = .false.     !< if true: contact region at upper side
  ! -- neighbors --
  integer                      :: id_sx_low =0   !< index to lower semi x point
  integer                      :: id_sx_upp =0   !< index to upper semi x point
  integer                      :: id_sy_low =0   !< index to lower semi y point
  integer                      :: id_sy_upp =0   !< index to upper semi y point
  integer                      :: id_sz_low =0   !< index to lower semi z point
  integer                      :: id_sz_upp =0   !< index to upper semi z point
  ! -- discretization --
  real(8)                      :: dx_low    =0   !< distance to lower semi x point
  real(8)                      :: dx_upp    =0   !< distance to upper semi x point
  real(8)                      :: dy_low    =0   !< distance to lower semi y point
  real(8)                      :: dy_upp    =0   !< distance to upper semi y point
  real(8)                      :: dz_low    =0   !< distance to lower semi z point
  real(8)                      :: dz_upp    =0   !< distance to upper semi z point
  ! -- cylindrical coordinates -- (obsolete)
  real(8)                      :: phi             =0       !< angle
  real(8)                      :: circ            =0       !< circumference
  logical                      :: is_coax         =.false. !< if true: coaxial semiconductor structure
  ! -- id for continuity equation --
  integer                      :: id_poisson_qfn    =0 !< index to poisson matrix entry for mid qfp
  integer                      :: id_poisson_qfn2   =0 !< index to poisson matrix entry for mid qfp
  integer                      :: id_poisson_qfp    =0 !< index to poisson matrix entry for mid qfp
  integer                      :: id_poisson_psi    =0 !< index to poisson matrix entry for mid psi
  integer,dimension(:),pointer :: id_poisson_psi_mean => null()!< index to poisson matrix entry for mid psi

  integer                      :: id_cont_qfn         =0 !< index to continuity matrix entry for mid qfp
  integer                      :: id_cont_qfn_x_low    =0!< index to continuity matrix entry for lower x qfp
  integer                      :: id_cont_qfn_x_upp   =0 !< index to continuity matrix entry for upper x qfp
  integer                      :: id_cont_qfn_y_low   =0 !< index to continuity matrix entry for lower y qfp
  integer                      :: id_cont_qfn_y_upp   =0 !< index to continuity matrix entry for upper y qfp
  integer                      :: id_cont_qfn_z_low    =0!< index to continuity matrix entry for lower z qfp
  integer                      :: id_cont_qfn_z_upp    =0!< index to continuity matrix entry for upper z qfp

  integer                      :: id_cont_qfp       =0   !< index to continuity matrix entry for mid qfp
  integer                      :: id_cont_qfp_x_low  =0  !< index to continuity matrix entry for lower x qfp
  integer                      :: id_cont_qfp_x_upp  =0  !< index to continuity matrix entry for upper x qfp
  integer                      :: id_cont_qfp_y_low  =0  !< index to continuity matrix entry for lower y qfp
  integer                      :: id_cont_qfp_y_upp  =0  !< index to continuity matrix entry for upper y qfp
  integer                      :: id_cont_qfp_z_low =0   !< index to continuity matrix entry for lower z qfp
  integer                      :: id_cont_qfp_z_upp   =0 !< index to continuity matrix entry for upper z qfp

  integer                      :: id_cont_elec_psi         = 0  !< index to continuity matrix entry for mid psi
  integer                      :: id_cont_elec_psi_x_low   = 0  !< index to continuity matrix entry for mid psi
  integer                      :: id_cont_elec_psi_y_low   = 0  !< index to continuity matrix entry for mid psi
  integer                      :: id_cont_elec_psi_x_upp   = 0  !< index to continuity matrix entry for mid psi
  integer                      :: id_cont_elec_psi_y_upp   = 0  !< index to continuity matrix entry for mid psi

  integer                      :: id_cont_psi        =0  !< index to continuity matrix entry for mid psi
  integer                      :: id_cont_psi_x_tun  =0 !< index to continuity matrix entry for first tunneling point in x direction
  integer                      :: id_cont_psi_x_low  =0 !< index to continuity matrix entry for lower x psi
  integer                      :: id_cont_psi_x_upp  =0 !< index to continuity matrix entry for upper x psi
  integer                      :: id_cont_psi_y_low  =0 !< index to continuity matrix entry for lower y psi
  integer                      :: id_cont_psi_y_upp  =0 !< index to continuity matrix entry for upper y psi
  integer                      :: id_cont_psi_z_low  =0 !< index to continuity matrix entry for lower z psi
  integer                      :: id_cont_psi_z_upp  =0 !< index to continuity matrix entry for upper z psi
  integer,dimension(:),pointer :: id_cont_psi_mean  => null()       !< index to continuity matrix entry for mid psi (mean psi)
  integer,dimension(:),pointer :: id_cont_psi_x_tun_mean => null()  !< index to continuity matrix entry for first tunneling point in x direction (mean psi)
  integer,dimension(:),pointer :: id_cont_psi_x_low_mean  => null() !< index to continuity matrix entry for lower x psi (mean psi)
  integer,dimension(:),pointer :: id_cont_psi_x_upp_mean  => null() !< index to continuity matrix entry for upper x psi (mean psi)

  ! -- id to trap -- (obsolete?)
  integer                      :: n_trap    =0          !< number of different trap models
  integer,dimension(:),pointer :: trap_id     => null()        !< (n_trap) id to trap model
endtype

! ---> global module parameters ---------------------------------------------------------------------------------------
type(sp_struct),dimension(:), pointer :: SP           !< (N_SP) properties of semiconductor
real(8),dimension(:), pointer :: JouleHeat    !< (N_SP) properties of semiconductor
real(8),dimension(:), pointer :: TAUE    !< (N_SP) properties of semiconductor
real(8),dimension(:), pointer :: GPSI    !< (N_SP) properties of semiconductor

logical                               :: SEMI_EXIST   !< if true: semiconductor exists
integer                               :: N_XLINE      !< number of 1D transport equations
integer                               :: N_CNT        !< number of CNTs
integer                               :: SEMI_DIM     !< semiconductor real space dimension
integer                               :: CHARGE_DIM   !< max. dimenison of charges in the simulation domain

real(8),dimension(:),pointer          :: CN0          => null()   !< (N_SP) intrinsic electron carrier density
real(8),dimension(:),pointer          :: CN20         => null()   !< (N_SP) intrinsic electron 2 carrier density
real(8),dimension(:),pointer          :: CP0          => null()   !< (N_SP) intrinsic hole carrier density
real(8),dimension(:),pointer          :: CN           !< (N_SP) semiconductor free electron density
real(8),dimension(:),pointer          :: CN2          !< (N_SP) semiconductor free electron 2 density second band
real(8),dimension(:),pointer          :: CP           !< (N_SP) semiconductor free hole density
real(8),dimension(:),pointer          :: PDISS        !< (N_SP) dissipated power

real(8),dimension(:),pointer          :: PSI_BUILDIN  !< (N_SP) psi build-in due to doping
real(8),dimension(:),pointer          :: FX           !< (N_SP) electric field in x-direction
real(8),dimension(:),pointer          :: FY           !< (N_SP) electric field in x-direction
real(8),dimension(:),pointer          :: PSI_SEMI     !< (N_SP) electrostatic potential at CNT
real(8),dimension(:),pointer          :: L_N          !< (N_SP) bohm electron quantum potential
real(8),dimension(:),pointer          :: PSI0         !< Zero bias electrostatic potential
logical                               :: E0_ADD       !< if true: change of bandgap
real(8),dimension(:),pointer          :: E0_ADD_SEMI  !< (N_SP) change of bandgap

real(8),dimension(:,:),pointer          :: CN_TRAP      !< (N_SP,CHARGE_DIM) semiconductor electron trap density
real(8),dimension(:,:),pointer          :: CP_TRAP      !< (N_SP,CHARGE_DIM) semiconductor hole trap density
type(band_pointer),dimension(:),pointer :: TRAP         !< (N_TRAP) trap model
integer                                 :: N_TRAP       !< number of different trap models
integer                                 :: TRAP_DIM     !< dimension for trap continuity equation

real(8),dimension(:),pointer          :: CN_LAST      !< (N_SP) electron density from last iteration
real(8),dimension(:),pointer          :: CN2_LAST      !< (N_SP) electron density from last iteration
real(8),dimension(:),pointer          :: CP_LAST      !< (N_SP) hole density from last iteration
real(8),dimension(:,:),pointer        :: CN_TRAP_LAST !< (N_SP,CHARGE_DIM) trap electron trap density from last iteration
real(8),dimension(:,:),pointer        :: CP_TRAP_LAST !< (N_SP,CHARGE_DIM) trap hole trap density from last iteration

real(8),dimension(:),pointer          :: SX           !< (SNX1) x-grid semiconductor in x-direction
real(8),dimension(:),pointer          :: SDX          !< (SNX) dx-grid semiconductor in x-direction
integer                               :: SNX          !< number of sx-boxes
integer                               :: SNX1         !< number of sx-points
integer,dimension(:),pointer          :: ID_1D        !< (SNX1) index to SP
real(8),dimension(:),pointer          :: PSI_1D       !< (SNX1) electrostatic potential in x-direction
real(8),dimension(:),pointer          :: VT_1D        !< (SNX1) thermal voltage over x
real(8),dimension(:),pointer          :: E0_ADD_1D    !< (SNX1) bandgap change over x

logical,dimension(:),pointer          :: IS_SEMI      !< (NXYZ) if true: is semi point
logical                               :: MET          !< if true: is metallic CNT
logical                               :: BBT          !< if true: band to band tunneling enabled
logical                               :: ELEC         !< if true: electron carriers activated
logical                               :: ELEC2        !< if true: two bands for electrons activated
logical                               :: HOLE         !< if true: hole carriers activated

integer                               :: N_SUBBAND  =0 !< total number of subbands
integer                               :: N_BAND     =0  !< total number of bands
integer                               :: N_CB       =0!< total number of conduction bands
integer                               :: N_VB       =0 !< total number of valence bands

private

public sp_struct
public SP
public JouleHeat
public TAUE
public GPSI
public SEMI_EXIST
public N_XLINE
public N_CNT
public SEMI_DIM
public CHARGE_DIM
public CN
public CN2
public CP
public CN0
public CN20
public CP0
public PSI_BUILDIN
public FX
public FY
public PSI_SEMI
public L_N
public PDISS
public E0_ADD
public E0_ADD_SEMI
public CN_TRAP
public CP_TRAP
public TRAP_DIM
public TRAP
public N_TRAP
public CN_LAST
public CN2_LAST
public CP_LAST
public CN_TRAP_LAST
public CP_TRAP_LAST
public SX
public SDX
public SNX
public SNX1
public ID_1D
public PSI_1D
public VT_1D
public E0_ADD_1D
public IS_SEMI
public MET
public BBT
public ELEC
public ELEC2
public HOLE
public N_SUBBAND
public N_BAND
public N_CB
public N_VB
public PSI0
public init_semiconductor
public close_semiconductor
public set_thermal_voltage
public get_bandprofile_1d
public get_pos_rand
public get_x
public calc_fx
public calc_fy
public set_id_1d
public set_psi_1d
public set_dens_1d
public get_dist
public get_pos
public get_qsemi
public normalize_semiconductor
public init_psi_buildin
public set_psi_0

contains

subroutine init_semiconductor
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialization of semiconductor module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> nullify
    nullify(SP)
    nullify(CN,CN2,CP,CD)
    nullify(UN,UP,EC0,EV0)
    nullify(PSI_BUILDIN)
    nullify(FX,FY)
    nullify(PSI_SEMI)
    nullify(L_N)
    nullify(CN_TRAP,CP_TRAP,TRAP)
    nullify(CN_LAST,CN2_LAST,CP_LAST,CN_TRAP_LAST,CP_TRAP_LAST)
    nullify(SX,SDX)
    nullify(ID_1D,PSI_1D,VT_1D,E0_ADD_1D)
    nullify(IS_SEMI)

    call init_sp
    call init_semi_fields
    call set_number_of_subbands
    call init_doping
    call init_grading
    call find_heterojunctions

endsubroutine

subroutine init_sp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set parameters SP struct
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,dimension(3):: low_xyz,upp_xyz
    integer             :: i,j,n,k,i1,j1,k1,p,is,b,m,id
    real(8)             :: ri,ra,y0,z0,xx,yy,diff_phi_low,diff_phi_upp,diff_phi,phi

    allocate(IS_SEMI(NXYZ))

    ! --> count number of discretization points inside semiconductor
    N_SP=0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%semi.or.POINT(i,j,k)%semi_cont.or.POINT(i,j,k)%supply) then
                    N_SP = N_SP+1
                endif
            enddo
        enddo
    enddo

    allocate(SP(N_SP))
    allocate(JouleHeat(N_SP) )
    allocate(TAUE(N_SP) )
    allocate(GPSI(N_SP) )
    JouleHeat = 0
    TAUE = 0
    GPSI = 0

    SEMI_EXIST = N_SP.gt.0
    if (.not.SEMI_EXIST) then
        write(*,*) '***warning*** structure does not contain a semiconductor region'
        SNX1    = 1
        SNX     = 0
        allocate(SDX(SNX))
        allocate(SX(SNX1))
        SX(1)   = 0
        ELEC    = .false.
        ELEC2   = .false.
        HOLE    = .false.
        IS_SEMI = .false.
        return

    endif

    ! --> index to POINT struct
    n=0
    p=0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                p=p+1 ! total number of disc points
                if (POINT(i,j,k)%semi.or.POINT(i,j,k)%semi_cont.or.POINT(i,j,k)%supply) then
                    n                   = n+1
                    IS_SEMI(p)          = .true.
                    POINT(i,j,k)%sp_idx = n
                    SP(n)%id_p          = p
                    SP(n)%id_x          = i
                    SP(n)%id_y          = j
                    SP(n)%id_z          = k

                    if (STRUC_DIM.ge.1) then
                        if (i.gt.1) then
                            SP(n)%x_low_exist = POINT(i-1,j,k)%semi.or.POINT(i-1,j,k)%semi_cont
                        endif
                        if (i.lt.NX1) then
                            SP(n)%x_upp_exist = POINT(i+1,j,k)%semi.or.POINT(i+1,j,k)%semi_cont
                        endif
                    endif

                    if (STRUC_DIM.ge.2) then
                        if (j.gt.1) then
                            SP(n)%y_low_exist = POINT(i,j-1,k)%semi.or.POINT(i,j-1,k)%semi_cont
                        endif
                        if (j.lt.NY1) then
                            SP(n)%y_upp_exist = POINT(i,j+1,k)%semi.or.POINT(i,j+1,k)%semi_cont
                        endif
                    endif

                    if (STRUC_DIM.ge.3) then
                        if (k.gt.1) then
                            SP(n)%z_low_exist = POINT(i,j,k-1)%semi.or.POINT(i,j,k-1)%semi_cont
                        endif
                        if (k.lt.NZ1) then
                            SP(n)%z_upp_exist = POINT(i,j,k+1)%semi.or.POINT(i,j,k+1)%semi_cont
                        endif
                    endif
                  
                else
                    IS_SEMI(p) = .false.

                endif
            enddo
        enddo
    enddo

    ! --> set semiconductor parameters
    do n=1,N_SP
        i = SP(n)%id_x
        j = SP(n)%id_y
        k = SP(n)%id_z
        SP(n)%cont     = POINT(i,j,k)%semi_cont ! should be always false
        SP(n)%supply   = POINT(i,j,k)%supply ! should be always false
        SP(n)%cont_id  = POINT(i,j,k)%cont_id   ! should be always 0
        if (POINT(i,j,k)%semi.or.POINT(i,j,k)%supply) then
            SP(n)%model_id  = POINT(i,j,k)%semi_id
            SP(n)%xline_id  = POINT(i,j,k)%xline_id
            SP(n)%box_id    = POINT(i,j,k)%box_id
            ! --> in contact module:
            !if (SP(n)%cont) then
            !  SP(n)%set_qfp = (.not.CON(SP(n)%cont_id)%shky)
            !endif

        elseif (POINT(i,j,k)%semi_cont) then
            ! find neighbor semiconductor and take its properties
            if (STRUC_DIM.ge.1) then
                if (i.gt.1) then
                    if (POINT(i-1,j,k)%semi) then
                        i1=i-1
                        j1=j
                        k1=k
                    endif
                endif
                if (i.lt.NX1) then
                    if (POINT(i+1,j,k)%semi) then
                        i1=i+1
                        j1=j
                        k1=k
                    endif
                endif
            endif
            if (STRUC_DIM.ge.2) then
                if (j.gt.1) then
                    if (POINT(i,j-1,k)%semi) then
                        i1=i
                        j1=j-1
                        k1=k
                    endif
                endif
                if (j.lt.NY1) then
                    if (POINT(i,j+1,k)%semi) then
                        i1=i
                        j1=j+1
                        k1=k
                    endif
                endif
            endif
            if (STRUC_DIM.ge.3) then
                if (k.gt.1) then
                    if (POINT(i,j,k-1)%semi) then
                        i1=i
                        j1=j
                        k1=k-1
                    endif
                endif
                if (k.lt.NZ1) then
                    if (POINT(i,j,k+1)%semi) then
                        i1=i
                        j1=j
                        k1=k+1
                    endif
                endif
            endif
            SP(n)%model_id = POINT(i1,j1,k1)%semi_id
            SP(n)%xline_id = POINT(i1,j1,k1)%xline_id
            SP(n)%box_id   = POINT(i1,j1,k1)%box_id

        endif
    enddo

    ! --> look for cylindrical structures (obsolete)
    do n=1,N_SP
        j = SP(n)%id_y
        k = SP(n)%id_z
        b = SP(n)%box_id
        if ((BOX(b)%type(1:4).eq.'semi').and.(BOX(b)%shape(1:6).eq.'cyl_yz')) then
            y0 = BOX(b)%low_xyz(2)
            z0 = BOX(b)%low_xyz(3)
            ri = BOX(b)%upp_xyz(2)
            ra = BOX(b)%upp_xyz(3)
            if ((ri.eq.ra).and.(ri.gt.0)) then
                yy = Y(j)-y0
                xx = Z(k)-z0
                if ((yy.eq.0).and.(xx.eq.0)) then
                    SP(n)%phi = 0

                elseif ((yy.gt.0).and.(xx.eq.0)) then
                    SP(n)%phi = 0.5*pi

                elseif ((yy.lt.0).and.(xx.eq.0)) then
                    SP(n)%phi = 1.5*pi

                elseif ((yy.gt.0).and.(xx.gt.0)) then
                    SP(n)%phi = atan(yy/xx)

                elseif ((yy.gt.0).and.(xx.lt.0)) then
                    SP(n)%phi = pi - atan(yy/(-xx))

                elseif ((yy.le.0).and.(xx.lt.0)) then
                    SP(n)%phi = pi + atan((-yy)/(-xx))

                elseif ((yy.le.0).and.(xx.gt.0)) then
                    SP(n)%phi = 2*pi - atan((-yy)/xx)

                endif
                SP(n)%is_coax = .true.
                SP(n)%circ    = 2*PI*ra

            endif
        endif
    enddo

    do n=1,N_SP
        if (SP(n)%is_coax) then
            phi = SP(n)%phi
            diff_phi_low = -PI
            diff_phi_upp = PI
            do m=1,N_SP
                if (m.eq.n) cycle
                if ((SP(n)%id_x.eq.SP(m)%id_x).and.(SP(n)%box_id.eq.SP(m)%box_id)) then
                    ! on same circle
                    diff_phi = SP(m)%phi - phi
                    if (diff_phi.gt.pi) diff_phi = diff_phi - 2*pi
                    if (diff_phi.lt.-pi) diff_phi = diff_phi + 2*pi
                    
                    if (diff_phi.lt.0) then
                        if  (diff_phi.gt.diff_phi_low) then
                            SP(n)%id_sy_low   = m
                            SP(n)%dy_low      = -diff_phi/(2*pi)*SP(n)%circ
                            diff_phi_low      = diff_phi
                            SP(n)%y_low_exist = .true.
                        endif
                    else
                        if  (diff_phi.lt.diff_phi_upp) then
                            SP(n)%id_sy_upp   = m
                            SP(n)%dy_upp      = diff_phi/(2*pi)*SP(n)%circ
                            diff_phi_upp      = diff_phi
                            SP(n)%y_upp_exist = .true.
                        endif
                    endif
                endif
            enddo
        endif
    enddo

    ! --> set index to neighbor points
    do n=1,N_SP
        i = SP(n)%id_x
        j = SP(n)%id_y
        k = SP(n)%id_z
        if (SP(n)%x_low_exist) then
            SP(n)%id_sx_low = POINT(i-1,j,k)%sp_idx
            SP(n)%dx_low    = DX(i-1)
        endif
        if (SP(n)%x_upp_exist) then
            SP(n)%id_sx_upp = POINT(i+1,j,k)%sp_idx
            SP(n)%dx_upp    = DX(i)
        endif
        if (.not.SP(n)%is_coax) then
            if (SP(n)%y_low_exist) then
                SP(n)%id_sy_low = POINT(i,j-1,k)%sp_idx
                SP(n)%dy_low    = DY(j-1)
            endif
            if (SP(n)%y_upp_exist) then
                SP(n)%id_sy_upp = POINT(i,j+1,k)%sp_idx
                SP(n)%dy_upp    = DY(j)
            endif
            if (SP(n)%z_low_exist) then
                SP(n)%id_sz_low = POINT(i,j,k-1)%sp_idx
                SP(n)%dz_low    = DZ(k-1)
            endif
            if (SP(n)%z_upp_exist) then
                SP(n)%id_sz_upp = POINT(i,j,k+1)%sp_idx
                SP(n)%dz_upp    = DZ(k)
            endif
        endif
    enddo

    ! --> check if boundary to contact and where contact is relative to point
    do n=1,N_SP
        SP(n)%cont_left =.false.
        SP(n)%cont_right=.false.
        SP(n)%cont_lower=.false.
        SP(n)%cont_upper=.false.
    enddo
    do n=1,N_SP
        if (SP(n)%cont.and.(.not.SP(n)%supply)) then
            if (SP(n)%x_upp_exist) then
                id = SP(n)%id_sx_upp
                if (.not.SP(id)%cont) then
                    ! --> contact at left side
                    SP(n)%cont_left =.true.

                endif
            endif
            if (SP(n)%x_low_exist) then
                id = SP(n)%id_sx_low
                if (.not.SP(id)%cont) then
                    ! --> contact at right side
                    SP(n)%cont_right =.true.

                endif
            endif
            if (SP(n)%y_upp_exist) then
                id = SP(n)%id_sy_upp
                if (.not.SP(id)%cont) then
                    ! --> contact at lower side
                    SP(n)%cont_lower =.true.

                endif
            endif
            if (SP(n)%y_low_exist) then
                id = SP(n)%id_sy_low
                if (.not.SP(id)%cont) then
                    ! --> contact at upper side
                    SP(n)%cont_upper =.true.

                endif
            endif
        endif
    enddo

    ! --> semiconductor dimension
    SEMI_DIM = 0
    do n=1,N_SP
        if (SP(n)%x_low_exist.or.SP(n)%x_upp_exist) then
            SEMI_DIM = SEMI_DIM + 1
            exit

        endif
    enddo

    do n=1,N_SP
        if (SP(n)%y_low_exist.or.SP(n)%y_upp_exist.or.SP(n)%is_coax) then
            SEMI_DIM = SEMI_DIM + 1
            exit

        endif
    enddo

    ! --> max. dimension of space charges
    CHARGE_DIM = maxval(US%semi(:)%dim)

    if ((CHARGE_DIM.lt.SEMI_DIM).and.(.not.any(SP(:)%is_coax))) then
        if (SEMI_EXIST) then
            write(*,*) '***error*** check transport dimension of semiconductor, charge_dim=',CHARGE_DIM,' semi_dim=',SEMI_DIM
            stop
        endif
    endif

    !DELETED 3D LINE CHARGE CNT MODEL FROM SVEN HERE

    !was only implemented for 1D, will need to deal with this when I reimplement bbt
    MET=.false.
    BBT=.false.

    ! --> check if elec/hole transport is activated
    ELEC =.true.
    ELEC2=.true.
    HOLE =.true.
    do is=1,N_SEMI
        ELEC  = ELEC .and.SEMI(is)%elec
        ELEC2 = ELEC2.and.SEMI(is)%elec2
        HOLE  = HOLE .and.SEMI(is)%hole
    enddo

    ! --> find border grid points for semi region
    low_xyz(1) = NX1
    low_xyz(2) = NY1
    low_xyz(3) = NZ1
    upp_xyz(1) = 1
    upp_xyz(2) = 1
    upp_xyz(3) = 1
    do n=1,N_SP
        if (SP(n)%id_x.lt.low_xyz(1)) low_xyz(1) = SP(n)%id_x
        if (SP(n)%id_x.gt.upp_xyz(1)) upp_xyz(1) = SP(n)%id_x
        if (SP(n)%id_y.lt.low_xyz(2)) low_xyz(2) = SP(n)%id_y
        if (SP(n)%id_y.gt.upp_xyz(2)) upp_xyz(2) = SP(n)%id_y
        if (SP(n)%id_z.lt.low_xyz(3)) low_xyz(3) = SP(n)%id_z
        if (SP(n)%id_z.gt.upp_xyz(3)) upp_xyz(3) = SP(n)%id_z
    enddo

    ! --> x-direction
    SNX1 = upp_xyz(1)-low_xyz(1)+1
    SNX  = SNX1-1
    allocate(SDX(SNX))
    allocate(SX(SNX1))
    n = 0
    do i=low_xyz(1),upp_xyz(1)-1
        n      = n+1
        SDX(n) = DX(i)
        SX(n)  = X(i)
    enddo
    SX(n+1)  = X(upp_xyz(1))
    do i=1,N_SP
        SP(i)%id_sx = SP(i)%id_x - low_xyz(1) + 1
    enddo

    N_CNT = 0

    N_XLINE = 0
    do n=1,N_SP
        if (SP(n)%xline_id.gt.N_XLINE) then
            N_XLINE = SP(n)%xline_id
        endif
    enddo

    ! if (SEMI_DIM.eq.1) then
    !     if (N_XLINE.ne.int(dble(N_SP)/dble(SNX1))) then
    !         write(*,*) '***error*** discretization error while simulating more then one 1D-Transport equation'
    !         stop
    !     endif
    ! endif

endsubroutine
    
subroutine find_heterojunctions
    ! find all heterojunctions in the device an mark them. 
    ! heterojunctions can either be homojunctions with different doping on each side, 
    ! or heterojunctions with different semiconductor on each side.

    integer :: id_heterojunction, n_heterojunctions, n, id_left, id_right, i
    real(8),dimension(:),allocatable     :: heterojunctions
    real(8)             :: x_heterojunction_user, fac_heterojunction
    real(8)             :: dop_left, dop_right

    !find heterojunctions
    if (US%n_thermionic_emission.ge.1) then
        if (SEMI_DIM.ge.2) then
            write(*,*) '***error*** thermionic emission only implemented for 1D semiconductors.'
            stop
        endif
        ! collect all heterojunctions
        n_heterojunctions = 0
        do n=1,N_SP
            if (SP(n)%x_upp_exist) then !take only points that have a neighbour
                id_left  = SP(n)%model_id
                id_right = SP(n+1)%model_id

                dop_left = sign(dble(1),CD(n))
                dop_right = sign(dble(1),CD(n+1))

                if ((id_left.ne.id_right).or.(dop_left.ne.dop_right)) then
                    n_heterojunctions = n_heterojunctions + 1
                endif
            endif
        enddo
        allocate(heterojunctions(n_heterojunctions))

        ! find locations of heterojunctions
        n_heterojunctions = 0
        do n=1,N_SP
            if (SP(n)%x_upp_exist) then !take only points that have a neighbour
                id_left  = SP(n)%model_id
                id_right = SP(n+1)%model_id
                dop_left = sign(dble(1),CD(n))
                dop_right = sign(dble(1),CD(n+1))
                if ((id_left.ne.id_right).or.(dop_left.ne.dop_right)) then
                    n_heterojunctions                  = n_heterojunctions + 1
                    heterojunctions(n_heterojunctions) = X(n)
                endif
            endif
        enddo

        !for each user supplied heterojunction, find the index of SP that corresponds to it
        if (n_heterojunctions.ne.0 ) then
            do i=1,US%n_thermionic_emission
                x_heterojunction_user = US%thermionic_emission(i)%x
                fac_heterojunction = US%thermionic_emission(i)%fac
                id_heterojunction     = minloc(abs(heterojunctions-x_heterojunction_user),1)

                !mark the point
                n_heterojunctions     = 0
                do n=1,N_SP
                    if (SP(n)%x_upp_exist) then !take only points that have a neighbour
                        id_left  = SP(n)%model_id
                        id_right = SP(n+1)%model_id
                        dop_left = sign(dble(1),CD(n))
                        dop_right = sign(dble(1),CD(n+1))

                        if ((id_left.ne.id_right).or.(dop_left.ne.dop_right)) n_heterojunctions = n_heterojunctions + 1
                        if (n_heterojunctions.eq.id_heterojunction) then
                            SP(n)%heterojunction = .true.
                            SP(n)%fac_heterojunction = fac_heterojunction
                            write(*,*) 'thermionic field emission model: found heterojunction'
                            exit
                        endif
                    endif
                enddo
            enddo
        else
            write(*,*) 'Warning: user specified heterojunction, but none found (homogenous semiconductor). '
        endif

    endif
endsubroutine


subroutine init_semi_fields
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize semiconductor fields
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> init semiconductor fields
    allocate(ID_1D(    SNX1))
    allocate(PSI_1D(   SNX1))
    allocate(E0_ADD_1D(SNX1))
    allocate(VT_1D(    SNX1))
    allocate(PSI0(N_SP))

    allocate(UN(          N_SP))
    allocate(EC0(         N_SP))
    allocate(EV0(         N_SP))
    allocate(BGN_SEMI(    N_SP))
    allocate(MU_LF(       N_SP))
    allocate(UN2(         N_SP))
    allocate(UP(          N_SP))
    allocate(CN(          N_SP))
    allocate(CN2(         N_SP))
    allocate(CP(          N_SP))
    allocate(CD(          N_SP))
    allocate(PDISS(       N_SP))
    allocate(DONATORS(    N_SP))
    allocate(ACCEPTORS(   N_SP))
    allocate(CDTOT(       N_SP))
    allocate(GRADING(     N_SP))
    allocate(GRADING2(    N_SP))
    allocate(STRAIN(      N_SP))
    allocate(CN_LAST(     N_SP))
    allocate(CN2_LAST(    N_SP))
    allocate(CP_LAST(     N_SP))
    allocate(PSI_SEMI(    N_SP))
    allocate(L_N(    N_SP))
    allocate(E0_ADD_SEMI( N_SP))
    if (BULK) then
        allocate(FX(1))
        allocate(FY(1))

    else
        allocate(FX(N_SP))
        allocate(FY(N_SP))

    endif

    UN          = 0
    UP          = 0
    CN          = 0
    CN2         = 0
    CP          = 0
    PSI0        = 0
    CD          = 0
    DONATORS    = 0
    MU_LF       = 0
    ACCEPTORS   = 0
    GRADING     = 0
    GRADING2    = 0
    STRAIN      = 0
    CP_LAST     = 0
    CN_LAST     = 0
    CN2_LAST    = 0
    PSI_SEMI    = 0
    L_N         = 0
    E0_ADD_SEMI = 0
    VT_LATTICE  = BK*TEMP_LATTICE/Q
    PDISS       = 0
    FX          = 0
    FY          = 0
    E0_ADD_1D   = 0

endsubroutine

subroutine init_doping
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize doping density
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer              :: i,j,id,n,id_old,hdferr,id_x,id_xp,id_xm,i_x_min,i_x_max
    real(8),dimension(3) :: low_dop,upp_dop,xyz,xyz_0,a_xyz,b_xyz,sigma_xyz
    real(8)              :: d_con
    real(8),dimension(:),allocatable              :: ACC_p, DON_p
    real(8)              :: arg,vbuildin,dist
    logical              :: is_ok,set_buildin
    real(8)              :: dx1,dx2,ai,bi,ci, integral, len_actual, len_input

    !for reading in hdf5
    real(8),dimension(:),allocatable:: doping_h5, x_h5

    if (.not.SEMI_EXIST) return

    ! --> user doping
    CDTOT = 0
    do j=1,US%n_doping
        if (US%doping(j)%profile(1:3).eq.'hdf') then
            ! --> doping from hdf file
            low_dop     = US%grading(j)%low_xyz - 1e-13
            upp_dop     = US%grading(j)%upp_xyz + 1e-13
            id_old      = 0

            !read doping from hdf file
            if (US%regioninfo%hdf(1:1).ne.'-') then
                write(*,*) 'reading doping from HDF5...'

                ! find x data in HDF5 File
                call read_col_hdf5(US%regioninfo%hdf,'/'//'X',x_h5,hdferr)
                if (hdferr.eq.-1) then
                    write(*,*) ' ****error*** no X column in HDF5 file'
                    stop
                endif

                !read in doping
                call read_col_hdf5(US%regioninfo%hdf,'/doping',doping_h5,hdferr)
                if (hdferr.eq.-1) then
                    write(*,*) 'doping not found in HDF5!'
                endif

                write(*,*) '...finish reading doping from HDF5.'
            else
                write (*,*) '***error*** user specified hdf5 doping model, however no hdf file specified in REGION_INFO block or file does not exist.'
            endif

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                  
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                !step one: find  id of point
                id_x = REMAP_VEC(X,SP(i)%id_x,x_h5)
                if (id_x.eq.0) then
                    write (*,*) '***error*** internal X vector does not match HDF5 X vector'
                endif

                !take over doping from HDF5
                if (is_ok) then
                    d_con    = doping_h5(id_x)
                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i)  + abs(d_con)
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con)
                endif
            enddo

        elseif (US%doping(j)%profile(1:5).eq.'const') then
            ! --> constant doping
            low_dop     = US%doping(j)%low_xyz - 1e-13
            upp_dop     = US%doping(j)%upp_xyz + 1e-13
            d_con       = US%doping(j)%d_con
            a_xyz       = US%doping(j)%a_xyz
            vbuildin    = US%doping(j)%v_buildin
            set_buildin = abs(vbuildin).le.10
            id_old      = 0

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    id = SP(i)%model_id
                    ! if (set_buildin.and.(id.ne.id_old)) then
                    !   d_con  = get_n_equ_band_dual(vbuildin,SEMI(id)%band_pointer,.true.,BK*SEMI(id)%temp0/Q,int(0)) - get_n_equ_band_dual(vbuildin,SEMI(id)%band_pointer,.false.,BK*SEMI(id)%temp0/Q,int(0))
                    !   id_old = id
                    ! endif
                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i) + abs(d_con)
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con)
                endif
            enddo

        elseif (US%doping(j)%profile(1:5).eq.'delta') then 
            !a delta doping spike, adjust doping so that the integral over the distance given by user is the same as used in simulation 

            ! --> delta doping
            low_dop     = US%doping(j)%low_xyz - 1e-13
            upp_dop     = US%doping(j)%upp_xyz + 1e-13
            d_con       = US%doping(j)%d_con

            if (SEMI_DIM.ne.1) then
                print *, 'Error, delta doping only available for 1D simulation.'
                stop
            endif

            len_input   = US%doping(j)%upp_xyz(1) - US%doping(j)%low_xyz(1)
            integral    = len_input*d_con
            i_x_min     = N_SP
            i_x_max     = 0

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                if (is_ok) then
                    if (xyz(1).gt.X(i_x_max))  i_x_max = i
                    if (xyz(1).lt.X(i_x_min))  i_x_min = i
                endif

            enddo

            !now calculate the actual length that depends on the discretization
            len_actual = 0
            do i=1,N_SP
                if ((i.ge.i_x_min).and.(i.le.i_x_max)) then
                    len_actual = len_actual +  DX(i - 1)*0.5 + DX(i)*0.5
                endif
            enddo

            d_con = integral/len_actual

            ! now add the doping
            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i) + abs(d_con)
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con)
                endif
            enddo

        elseif (US%doping(j)%profile(1:3).eq.'exp') then
            ! --> exponential doping
            low_dop  = US%doping(j)%low_xyz - 1e-13
            upp_dop  = US%doping(j)%upp_xyz + 1e-13
            d_con    = US%doping(j)%d_con
            xyz_0    = US%doping(j)%xyz_0
            a_xyz    = US%doping(j)%a_xyz
            b_xyz    = US%doping(j)%b_xyz

            do i=1,N_SP
                xyz(1) = X(SP(i)%id_x)
                xyz(2) = Y(SP(i)%id_y)
                if ((xyz(1).lt.30e-9).and.(xyz(2).ge.55e-9)) then
                    xyz(3) = 1
                endif
            enddo

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    arg = 0
                    do n=1,SEMI_DIM
                        
                        if (US%doping(j)%profile(4:6).eq.'bte') then
                            arg = arg + abs(log(b_xyz(n)/d_con)*(xyz(n)-low_dop(n))/(upp_dop(n)-low_dop(n)))
                        else
                            arg = arg - abs((xyz(n)-xyz_0(n))/a_xyz(n))**b_xyz(n)
                        endif
                    enddo
                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i) + abs(d_con*exp(arg))
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con*exp(arg))
                endif
            enddo

        elseif (US%doping(j)%profile(1:4).eq.'erfc') then
            ! --> erfc doping
            low_dop  = US%doping(j)%low_xyz - 1e-13
            upp_dop  = US%doping(j)%upp_xyz + 1e-13
            d_con    = US%doping(j)%d_con
            xyz_0    = US%doping(j)%xyz_0
            a_xyz    = US%doping(j)%a_xyz
            b_xyz    = US%doping(j)%b_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    arg = 1
                    do n=1,SEMI_DIM
                        dist = (xyz(n)-xyz_0(n))/a_xyz(n)
                        if (b_xyz(n).EQ.0) then
                            !erfc part
                            arg = arg*ERFC(dist)
                        else
                            !exp part
                            arg = arg*exp(-abs(dist)**b_xyz(n))
                        endif
                    enddo

                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i) + abs(d_con*arg)
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con*arg)

                endif
            enddo

        elseif (US%doping(j)%profile(1:4).eq.'gaus') then
            ! --> gaussian doping profile
            low_dop  = US%doping(j)%low_xyz - 1e-13
            upp_dop  = US%doping(j)%upp_xyz + 1e-13
            d_con    = US%doping(j)%d_con
            xyz_0    = US%doping(j)%xyz_0
            sigma_xyz= US%doping(j)%sigma_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    arg = 0
                    do n=1,SEMI_DIM
                        arg = arg - (xyz(n)-xyz_0(n))**2/(2*sigma_xyz(n)**2)
                    enddo
                    if (d_con.GT.0) DONATORS(i)  = DONATORS(i) + abs(d_con*exp(arg))
                    if (d_con.LT.0) ACCEPTORS(i) = ACCEPTORS(i) + abs(d_con*exp(arg))
                endif
            enddo
        endif
    enddo

    if (any(DONATORS.lt.0)) then
        write(*,*) '***error*** problem with DONATORS.'
        stop
    endif
    if (any(ACCEPTORS.lt.0)) then
        write(*,*) '***error*** problem with ACCEPTORS.'
        stop
    endif

    ! diffusion equation in X direction
    ! https://scipython.com/book/chapter-7-matplotlib/examples/the-two-dimensional-diffusion-equation/
    if (US%dd%diffuse.gt.0) then
        allocate(DON_p(size(DONATORS)))
        allocate(ACC_p(size(ACCEPTORS)))
        DON_p = DONATORS
        ACC_p = ACCEPTORS

        do n=1,10
            do j=1,NY1
                do i=1+2,NX1-2
                    id_x = POINT(i,j,1)%sp_idx
                    id_xp = POINT(i+1,j,1)%sp_idx
                    id_xm = POINT(i-1,j,1)%sp_idx

                    ! dx = X(i+1) - X(i-1)
                    ! dx = 2e-9
                    dx1 = X(id_x) - X(id_xm)
                    dx2 = X(id_xp) - X(id_x)
                    ai  =  2.0/(dx1*(dx1+dx2))
                    bi  = -2.0/(dx1*dx2)
                    ci  =  2.0/(dx2*(dx1+dx2))

                    arg = ai*DONATORS(id_x-1) + bi*DONATORS(id_x) + ci*DONATORS(id_x+1)
                    !arg = (DONATORS(id_x+1)-2*DONATORS(id_x)+DONATORS(id_x-1))/(dx**2)
                    DON_p(id_x) = DONATORS(id_x) + US%dd%diffuse * arg

                    if (DON_p(id_x).lt.0) then
                        DON_p(id_x) = 1e-6
                    endif

                    arg = ai*ACCEPTORS(id_x-1) + bi*ACCEPTORS(id_x) + ci*ACCEPTORS(id_x+1)
                    !arg = (ACCEPTORS(id_x+1)-2*ACCEPTORS(id_x)+ACCEPTORS(id_x-1))/(dx**2)
                    ACC_p(id_x) = ACCEPTORS(id_x) + US%dd%diffuse * arg

                    if (ACC_p(id_x).lt.0) then
                        ACC_p(id_x) = 1e-6
                    endif


                enddo
            enddo
            DONATORS = DON_p
            ACCEPTORS = ACC_p

        enddo
        deallocate(DON_p)
        deallocate(ACC_p)
    endif

    if (any(DONATORS.lt.0)) then
        write(*,*) '***error*** problem with DONATORS.'
        stop
    endif
    if (any(ACCEPTORS.lt.0)) then
        write(*,*) '***error*** problem with ACCEPTORS.'
        stop
    endif

    ! calculate net doping and total doping
    CD = DONATORS - ACCEPTORS
    CDTOT = DONATORS + ACCEPTORS

endsubroutine

subroutine init_grading
    ! initialize grading
    integer              :: i,j,n,id_old,hdferr,id_x,index_low,index_upp,n_ele
    real(8),dimension(3) :: low_dop,upp_dop,xyz,xyz_0,a_xyz,sigma_xyz,b_xyz,xyz_rupp, xyz_rlow, xyz_dupp, xyz_dlow
    real(8)              :: c_max, c_min, x_p, a_low, a_upp
    real(8),dimension(3) :: su_l1, su_l2, su_u1, su_u2, sl_l1, sl_l2, s_l, s_u, sl_u1, sl_u2,arg_3
    real(8),dimension(10):: x_vec,y_vec
    real(8)              :: arg,dist
    real(8)              :: x_left,x_mid,x_right,x_sp, val, m
    logical              :: is_ok
    !real(8),dimension(:),allocatable              :: GRADING_p

    !for reading in hdf5
    real(8),dimension(:),allocatable:: grading_h5, x_h5
    real(8),dimension(:),pointer :: grading_target_vector

    if (.not.SEMI_EXIST) return

    ! --> user grading
    do j=1,US%n_grading
        if (US%grading(j)%mat_type(1:1).eq.'x') then
            grading_target_vector => GRADING
        elseif (US%grading(j)%mat_type(1:1).eq.'y') then
            grading_target_vector => GRADING2
        else
            write(*,*) '***error*** GRADING material type unknown. Possible values: x or y'
            stop
        endif
 
        if (US%grading(j)%profile(1:5).eq.'const') then
            ! --> constant grading
            low_dop     = US%grading(j)%low_xyz - 1e-13
            upp_dop     = US%grading(j)%upp_xyz + 1e-13
            c_max       = US%grading(j)%c_max
            id_old      = 0

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                ! calculate grading density for that point
                if (is_ok) then
                    grading_target_vector(i) = grading_target_vector(i) + c_max
                endif
            enddo

        elseif (US%grading(j)%profile(1:3).eq.'hdf') then
            ! --> grading from hdf file
            low_dop     = US%grading(j)%low_xyz - 1e-13
            upp_dop     = US%grading(j)%upp_xyz + 1e-13
            c_max       = US%grading(j)%c_max
            id_old      = 0

            !read grading from hdf file
            if (US%regioninfo%hdf(1:1).ne.'-') then
                write(*,*) 'reading grading from HDF5...'

                ! find x data in HDF5 File
                call read_col_hdf5(US%regioninfo%hdf,'/'//'X',x_h5,hdferr)
                if (hdferr.eq.-1) then
                    write(*,*) ' ****error*** no X column in HDF5 file'
                    stop
                endif

                !read in grading
                call read_col_hdf5(US%regioninfo%hdf,'/grading',grading_h5,hdferr)
                if (hdferr.eq.-1) then
                    write(*,*) 'grading not found in HDF5!'
                endif

                write(*,*) '...finish reading grading from HDF5.'
            else
                write (*,*) '***error*** user specified hdf5 grading model, however no hdf file specified in REGION_INFO block or file does not exist.'
            endif

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo

                !step one: find  id of point
                id_x = REMAP_VEC(X,SP(i)%id_x,x_h5)
                if (id_x.eq.0) then
                    write (*,*) '***error*** internal X vector does not match HDF5 X vector'
                endif

                if (id_x.eq.1) then 
                    cycle
                endif
                if (id_x.eq.size(x_h5)) then 
                    cycle
                endif

                !interpolate linear between grid of x_h5 and x grid of hdev
                x_left = x_h5(id_x-1)
                x_mid = x_h5(id_x)
                x_right = x_h5(id_x+1)
                x_sp = X(SP(i)%id_x)

                !linear equation y=m (x-x0) + t
                if (x_sp.ge.x_mid) then !point between i and i+1
                    m = ( grading_h5(id_x + 1) - grading_h5(id_x) ) / (x_right-x_mid)
                    val = m*(x_sp-x_mid) + grading_h5(id_x)
                else
                    m = ( grading_h5(id_x ) - grading_h5(id_x-1) ) / (x_mid-x_left)
                    val = m*(x_sp-x_left) + grading_h5(id_x-1)
                endif

                !take over grading from HDF5
                if (is_ok) then
                    grading_target_vector(i) = grading_target_vector(i) + val
                endif
            enddo
        elseif (US%grading(j)%profile(1:3).eq.'int') then !interpolation
            low_dop     = US%grading(j)%low_xyz - 1e-13
            upp_dop     = US%grading(j)%upp_xyz + 1e-13
            x_vec       = US%grading(j)%x_vec
            y_vec       = US%grading(j)%y_vec
            n_ele       = count(x_vec/=0)

            ! get x vector between low and upp dop
            index_low = 0
            index_upp = 0
            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))
                enddo
                if (is_ok.and.(index_low.eq.0)) index_low = i
                if (is_ok) index_upp = i
            enddo

            !interpolate grading vector (Will not allow summing grading this way...)
            call interp_lagrange ( 1, n_ele, x_vec(1:n_ele), y_vec(1:n_ele), index_upp-index_low &
                , X(index_low:index_upp), grading_target_vector(index_low:index_upp) )

            ! interpolation can lead to negative values => no thanks
            do i=index_low,index_upp
                if (grading_target_vector(i).lt.0) grading_target_vector(i) = 0
            enddo

        elseif (US%grading(j)%profile(1:3).eq.'exp') then
            ! --> exponential doping
            low_dop  = US%grading(j)%low_xyz - 1e-13
            upp_dop  = US%grading(j)%upp_xyz + 1e-13
            c_max    = US%grading(j)%c_max
            xyz_0    = US%grading(j)%xyz_0
            a_xyz    = US%grading(j)%a_xyz
            b_xyz    = US%grading(j)%b_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate grading density for that point
                if (is_ok) then
                    arg = 0
                    do n=1,SEMI_DIM
                        arg = arg - abs((xyz(n)-xyz_0(n))/a_xyz(n))**b_xyz(n)
                    enddo
                    grading_target_vector(i) = grading_target_vector(i) + c_max*exp(arg)

                endif
            enddo

        elseif (US%grading(j)%profile(1:3).eq.'lin') then
            ! --> exponential doping
            low_dop  = US%grading(j)%low_xyz - 1e-13
            upp_dop  = US%grading(j)%upp_xyz + 1e-13
            c_max    = US%grading(j)%c_max
            c_min    = US%grading(j)%c_min
            xyz_0    = US%grading(j)%xyz_0
            a_xyz    = US%grading(j)%a_xyz
            b_xyz    = US%grading(j)%b_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then

                    !GERADENGLEICHUNG
                    ! c = mx+t
                    ! t = y_max
                    ! m = (y_min-y_max)/(x_high-x_low)
                    grading_target_vector(i) = grading_target_vector(i) + (c_min-c_max)/(upp_dop(1)-low_dop(1)) * (xyz(1)-low_dop(1)) + c_max


                endif
            enddo

        elseif (US%grading(j)%profile(1:4).eq.'erfc') then
            ! --> erfc doping
            low_dop  = US%grading(j)%low_xyz - 1e-13
            upp_dop  = US%grading(j)%upp_xyz + 1e-13
            c_max    = US%grading(j)%c_max
            xyz_0    = US%grading(j)%xyz_0
            a_xyz    = US%grading(j)%a_xyz
            b_xyz    = US%grading(j)%b_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    arg = 1
                    do n=1,SEMI_DIM
                        dist = (xyz(n)-xyz_0(n))/a_xyz(n)
                        if (b_xyz(n).EQ.0) then
                            !erfc part
                            arg = arg*ERFC(dist)
                        else
                            !exp part
                            arg = arg*exp(-abs(dist)**b_xyz(n))
                        endif
                    enddo

                    grading_target_vector(i) = grading_target_vector(i) + c_max*arg
                endif
            enddo
        elseif (US%grading(j)%profile(1:4).eq.'gaus') then
            ! --> gaussian doping profile
            low_dop  = US%grading(j)%low_xyz - 1e-13
            upp_dop  = US%grading(j)%upp_xyz + 1e-13
            c_max    = US%grading(j)%c_max
            xyz_0    = US%grading(j)%xyz_0
            sigma_xyz= US%grading(j)%sigma_xyz

            do i=1,N_SP
                ! --> check wether point is in allowed box
                is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif
                    is_ok = is_ok.and.(low_dop(n).le.xyz(n)).and.(upp_dop(n).ge.xyz(n))

                enddo

                ! calculate doping density for that point
                if (is_ok) then
                    arg = 0
                    do n=1,SEMI_DIM
                        arg = arg - (xyz(n)-xyz_0(n))**2/(2*sigma_xyz(n)**2)
                    enddo
                    grading_target_vector(i) = grading_target_vector(i) + c_max*exp(arg)
                endif
            enddo

        elseif (US%grading(j)%profile(1:4).eq.'trap') then
            ! --> erfc doping
            low_dop  = US%grading(j)%low_xyz - 1e-13
            upp_dop  = US%grading(j)%upp_xyz + 1e-13
            xyz_dlow = US%grading(j)%xyz_dlow
            xyz_dupp = US%grading(j)%xyz_dupp
            xyz_rlow = US%grading(j)%xyz_rlow
            xyz_rupp = US%grading(j)%xyz_rupp
            c_max    = US%grading(j)%c_max

            !get boundaries
            !top part
            s_l   = low_dop+xyz_dlow
            s_u   = upp_dop-xyz_dupp

            !lowest interval
            sl_l1 = low_dop-xyz_rlow
            sl_l2 = low_dop+xyz_rlow

            !intermediate lower interval
            sl_u1 = s_l-xyz_rlow
            sl_u2 = s_l+xyz_rlow

            !intermediate upper interval
            su_l1 = s_u-xyz_rupp
            su_l2 = s_u+xyz_rupp

            !highest interval
            su_u1 = upp_dop-xyz_rupp
            su_u2 = upp_dop+xyz_rupp

            do i=1,N_SP
                ! ! --> check wether point is in allowed box
                ! is_ok = .true.
                do n=1,SEMI_DIM
                    if     (n.eq.1) then
                        xyz(n) = X(SP(i)%id_x)

                    elseif (n.eq.2) then
                        xyz(n) = Y(SP(i)%id_y)

                    elseif (n.eq.3) then
                        xyz(n) = Z(SP(i)%id_z)

                    endif

                enddo

                arg_3 = 0
                do n=1,SEMI_DIM
                    !lower slope, lower interval
                    if ( (xyz_rlow(n).GT.0) .AND. (sl_l1(n).LT.xyz(n)) .AND. (xyz(n).LE.sl_l2(n)) ) then
                        x_p=xyz(n)-sl_l1(n)
                        arg_3(n) = CMPTRPRND(x_p,xyz_dlow(n),xyz_rlow(n))
                    !lower slope
                    elseif ((xyz(n).GT.sl_l2(n)).AND.(xyz(n).LE.sl_u1(n))) then
                        a_low=1/xyz_dlow(n)
                        arg_3(n) = a_low*(xyz(n)-low_dop(n))
                    !lower slope, upper interval
                    elseif ((xyz_rlow(n).GT.0).AND.(sl_u1(n).LT.xyz(n)).AND.(xyz(n).LE.sl_u2(n))) then
                        x_p=-(xyz(n)-sl_u2(n))
                        arg_3(n) = 1-CMPTRPRND(x_p,xyz_dlow(n),xyz_rlow(n))
                    !top part
                    elseif ((sl_u2(n).LT.xyz(n)).AND.(xyz(n).LE.su_l1(n))) then
                        arg_3(n) = 1
                    !upper slope, lower interval
                    elseif ((xyz_rupp(n).GT.0).AND.(su_l1(n).LT.xyz(n)).AND.(xyz(n).LE.su_l2(n))) then
                        x_p=xyz(n)-su_l1(n)
                        arg_3(n) = 1-CMPTRPRND(x_p,xyz_dupp(n),xyz_rupp(n))
                    !upper slope
                    elseif ((su_l2(n).LT.xyz(n)).AND.(xyz(n).LE.su_u1(n))) then
                        a_upp  = 1/xyz_dupp(n)
                        arg_3(n) = 1-a_upp*(xyz(n)-s_u(n))
                    !upper slope, upper interval
                    elseif ((xyz_rupp(n)>0).AND.(su_u1(n).LT.xyz(n)).AND.(xyz(n).LE.su_u2(n))) then
                        x_p=-(xyz(n)-su_u2(n))
                        arg_3(n) = CMPTRPRND(x_p,xyz_dupp(n),xyz_rupp(n))
                    else
                        cycle
                    ! else
                    !     write(*,*) '***error*** trap grading, point in no interval.'
                    !     stop
                    endif
                enddo
                arg = 1
                do n=1,SEMI_DIM
                    arg = arg_3(n)*arg
                enddo
                grading_target_vector(i) = grading_target_vector(i) + c_max*arg
            enddo

        endif
    enddo


    ! diffusion of MOL fraction
    ! allocate(GRADING_p(size(GRADING)))
    ! GRADING_p = GRADING
    ! do n=1,10
    !     do j=1,NY1
    !         do i=1+1,NX1-1
    !             id_x = POINT(i,j,1)%sp_idx
    !             arg = 1e-29*(GRADING(id_x+1)-2*GRADING(id_x)+GRADING(id_x-1))/(X(i+2)-X(i-1))**2
    !             GRADING_p(id_x) = GRADING(id_x) + arg
    !         enddo
    !     enddo
    !     GRADING = GRADING_p
    ! enddo
    ! deallocate(GRADING_p)

endsubroutine

subroutine init_psi_buildin
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize psi build in due to doping
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer   :: i,is,hetero_id
    real(8)   :: psi_equ,dop, deg

    allocate(PSI_BUILDIN(N_SP))

    !calculate EFI_equ,EF_equ and PSI_equ, CN0,CP0
    dop = 0
    deg = 0
    hetero_id = 0
    do i=1,N_SP
        if (HETERO) hetero_id=i
        is = SP(i)%model_id
        if ((i.eq.1).or.(dop.ne.CD(i)).or.(deg.ne.(UN(i) + UP(i) ))) then
            dop       =  CD(i)
            deg       =  UN(i) + UP(i)

            psi_equ =  get_fermi_equ(is,dop,hetero_id,UN(i),UP(i),UN2(i)) !the band potentials shift the band edges!

        endif
        PSI_BUILDIN(i) = psi_equ

    enddo

endsubroutine

subroutine set_psi_0
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the zero bias electrostatic potential
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    PSI0 = PSI_SEMI
endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief get index to semiconductor model, conduction and valence band in case of btb tunneling
! !> \details
! subroutine get_band_bbt_id(ip,is, id,iv_cb,iv_vb)

! integer,intent(in ) :: ip    !< position index
! integer,intent(in ) :: is    !< subband index
! integer,intent(out) :: id    !< semiconductor model index
! integer,intent(out) :: iv_cb !< conduction band index
! integer,intent(out) :: iv_vb !< valence band index


! id    = SP(ip)%model_id
! ! iv_cb = SEMI(id)%IB_CB(is)
! ! iv_vb = SEMI(id)%IB_VB(is)

! endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief get index to semiconductor model, conduction and valence band in case of btb tunneling in x-direction
! !> \details
! subroutine get_band_bbt_id_1d(ix,is, id,iv_cb,iv_vb)

! integer,intent(in ) :: ix    !< position index in x direction
! integer,intent(in ) :: is    !< subband index
! integer,intent(out) :: id    !< semiconductor model index
! integer,intent(out) :: iv_cb !< conduction band index
! integer,intent(out) :: iv_vb !< valence band index


! call get_band_bbt_id(ID_1D(ix),is, id,iv_cb,iv_vb)

! endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief get index to semiconductor model, conduction and valence band
! !> \details
! subroutine get_band_id(ip,is,iscb, id,iv)

! integer,intent(in ) :: ip    !< position index
! integer,intent(in ) :: is    !< subband index
! logical,intent(in ) :: iscb  !< if true: conduction band, else: valence band
! integer,intent(out) :: id    !< semiconductor model index
! integer,intent(out) :: iv    !< conduction/valence band index


! id  = SP(ip)%model_id
! if (iscb) then
!   iv  = SEMI(id)%IB_CB(is)

! else
!   iv  = SEMI(id)%IB_VB(is)

! endif

! endsubroutine

subroutine set_id_1d(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set index for x-direction
    !
    ! input
    ! -----
    ! n : integer
    !   xline index of line id to set.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in) :: n

    integer :: i,j

    j=0
    do i=1,N_SP
        if (SP(i)%xline_id.eq.n) then
            j        = j+1
            ID_1D(j) = i

        endif
    enddo

endsubroutine

subroutine set_psi_1d(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set electrostatic potential for given x-line
    !
    ! input
    ! -----
    ! n : integer
    !   xline index of line id to set.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in) :: n

    integer :: i,j

    j=0
    do i=1,N_SP
        if (SP(i)%xline_id.eq.n) then
            j         = j+1
            PSI_1D(j) = PSI_SEMI(i)
        endif
    enddo

    j=0
    if (E0_ADD) then
        do i=1,N_SP
            if (SP(i)%xline_id.eq.n) then
                j            = j+1
                E0_ADD_1D(j) = E0_ADD_SEMI(i)
            endif
        enddo
    endif

endsubroutine

subroutine set_dens_1d(dens_1d,n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set carrier density for given x-line
    !
    ! input
    ! -----
    ! dens_1d : real(8),dimension(SNX1)
    !   Carrier density allong given x-line
    ! n : integer
    !   xline index of line id to set.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),dimension(SNX1),intent(in) :: dens_1d
    integer,                intent(in) :: n

    integer :: i,j

    j=0
    do i=1,N_SP
        if (SP(i)%xline_id.eq.n) then
            j       = j+1
            CN(i)   = dens_1d(j)
            if (SP(i)%is_coax) then
                CN(i) = CN(i) / SP(i)%circ
            endif
        endif
    enddo
endsubroutine

subroutine calc_fx
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate electric field in x-direction
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,m,p
    real(8) :: dx

    if (BULK) return

    do n=1,N_SP
        i  = SP(n)%id_x
        m  = SP(n)%id_sx_low
        p  = SP(n)%id_sx_upp
        dx = (SP(n)%dx_low+SP(n)%dx_upp)
        if (SP(n)%x_low_exist.and.SP(n)%x_upp_exist) then
            ! middle
            FX(n) = -(PSI_SEMI(p)-PSI_SEMI(m))/dx
          
        elseif ((.not.SP(n)%x_low_exist).and.SP(n)%x_upp_exist) then
            ! left border
            FX(n) = -(PSI_SEMI(p)-PSI_SEMI(n))/dx
          
        elseif (SP(n)%x_low_exist.and.(.not.SP(n)%x_upp_exist)) then
            ! right border
            FX(n) = -(PSI_SEMI(n)-PSI_SEMI(m))/dx
          
        else
            ! two borders
            FX(n) = 0
          
        endif
    enddo

endsubroutine

subroutine calc_fy
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate electric field in y-direction
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,m,p
    real(8) :: dy

    do n=1,N_SP
        i  = SP(n)%id_y
        m  = SP(n)%id_sy_low
        p  = SP(n)%id_sy_upp
        dy = (SP(n)%dy_low+SP(n)%dy_upp)
        if (SP(n)%y_low_exist.and.SP(n)%y_upp_exist) then
            ! middle
            FY(n) = -(PSI_SEMI(p)-PSI_SEMI(m))/dy
          
        elseif ((.not.SP(n)%y_low_exist).and.SP(n)%y_upp_exist) then
            ! left border
            FY(n) = -(PSI_SEMI(p)-PSI_SEMI(n))/dy
          
        elseif (SP(n)%y_low_exist.and.(.not.SP(n)%y_upp_exist)) then
            ! right border
            FY(n) = -(PSI_SEMI(n)-PSI_SEMI(m))/dy
          
        else
            ! two borders
            FY(n) = 0
          
        endif
    enddo

endsubroutine

subroutine set_thermal_voltage
    !!!!!!!!!!!!!!!!!!!!!
    ! set thermal voltage
    !!!!!!!!!!!!!!!!!!!!!
    VT_LATTICE = BK * TEMP_LATTICE / Q
endsubroutine

subroutine set_number_of_subbands
    !!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set number of subbands
    !!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: is

    N_CB = 9
    N_VB = 9
    do is=1,N_SEMI
        if (SEMI(is)%N_CB.lt.N_CB) then
            N_CB = SEMI(is)%N_CB
        endif
        if (SEMI(is)%N_VB.lt.N_VB) then
            N_VB = SEMI(is)%N_VB
        endif
    enddo

    N_SUBBAND = min(N_CB,N_VB)
    N_BAND    = N_CB+N_VB
endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief get largest bandgap for multiple semiconductor models
! !> \details subband given
! subroutine get_sub_bandgap(is, eg_cb,eg_vb)

! integer,intent(in ) :: is    !< subband id
! real(8),intent(out) :: eg_cb !< half bandgap conduction band
! real(8),intent(out) :: eg_vb !< half bandgap valence band

! integer :: id,iv


! eg_cb=0
! eg_vb=0
! do id=1,N_SEMI
!   iv = SEMI(id)%IB_CB(is)
!   if (SEMI(id)%band_pointer(iv)%band%e0.gt.eg_cb) then
!     eg_cb = SEMI(id)%band_pointer(iv)%band%e0

!   endif
!   iv = SEMI(id)%IB_VB(is)
!   if (SEMI(id)%band_pointer(iv)%band%e0.gt.eg_vb) then
!     eg_vb = SEMI(id)%band_pointer(iv)%band%e0

!   endif
! enddo


! endsubroutine


function get_bandprofile_1d(ib) result(eband)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get band profile for given semiconductor model and subband (todo: band potentials)
    !
    ! input
    ! -----
    ! ib : integer
    !   Index of the band to get the profile
    !
    ! output
    ! ------
    ! eband : real(8),dimension(SNX1)
    !   Band profile.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer                 :: ib    !< subband id
    real(8),dimension(SNX1) :: eband !< conduction/valence band

    logical iscb

    integer :: i,is

    iscb = SEMI(1)%band_pointer(ib)%band%iscb

    do i=1,N_SP
        is = SP(i)%model_id
        if (iscb) then !todo: UN2
            eband(i) =  SEMI(is)%band_pointer(ib)%band%e0 - PSI_SEMI(i) - UN(i)
        else
            eband(i) = -SEMI(is)%band_pointer(ib)%band%e0 - PSI_SEMI(i) - UP(i)
        endif
    enddo

endfunction

function get_dist(six,sxi,tix,txi) result(x)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get distance between to grid points
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer, intent(in) :: six !< start box index
    real(8), intent(in) :: sxi !< start rel. position in box
    integer, intent(in) :: tix !< end box index
    real(8), intent(in) :: txi !< end rel. position in box
    real(8)             :: x   !< x-position

    integer :: i

    if (six.eq.tix) then
        x = SDX(six)*abs(txi-sxi)

    elseif (six.gt.tix) then
        x = SDX(tix)*(dble(1)-txi) + SDX(six)*(sxi)
        do i=tix+1,six-1
            x = x + SDX(i)
        enddo

    else ! six.lt.tix
        x = SDX(six)*(dble(1)-sxi) + SDX(tix)*(txi)
        do i=six+1,tix-1
            x = x + SDX(i)
        enddo

    endif
  
endfunction

function get_x(ix,xi) result(x)
    !!!!!!!!!!!!!!!!!!
    ! get x position
    !!!!!!!!!!!!!!!!!!
    integer, intent(in) :: ix !< box index
    real(8), intent(in) :: xi !< rel. position in box
    real(8)             :: x  !< x-position

    x = SX(ix) + SDX(ix)*xi
endfunction

function get_qsemi(iselec) result(qsemi)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get semiconductor space charge
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,optional, intent(in) :: iselec

    real(8) :: qsemi !< semiconductor charge

    integer :: n
    real(8) :: dx,dy

    !warning: this method gives only the free charge
    qsemi=0 
    do n=1,N_SP
        dx     = (SP(n)%dx_low+SP(n)%dx_upp)*0.5 
        if (SEMI_DIM.eq.1) then
            if (CART.and.(STRUC_DIM.eq.2)) then
                !dy = CNT(SP(n)%model_id)%n_tube
            else
                dy = 1
            endif

        else
            dy   = (SP(n)%dy_low+SP(n)%dy_upp)*0.5 

        endif
        if (present(iselec)) then
            if (iselec) then
                qsemi  = qsemi - CN(n) * dx * dy
            else
                qsemi  = qsemi + CP(n) * dx * dy
            endif
        else
            qsemi  = qsemi + (CP(n)-CN(n)) * dx * dy
        endif
    enddo
    qsemi = Q*qsemi
endfunction

! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief get semiconductor trap charge
! !> \details
! function get_qtrap() result(qtrap)

! real(8),dimension(:),allocatable :: qtrap !< semiconductor charge

! integer :: n,i
! real(8) :: dx,dy

! allocate(qtrap(CHARGE_DIM))

! qtrap=0

! do i=1,CHARGE_DIM
!   do n=1,N_SP
!     dx     = (SP(n)%dx_low+SP(n)%dx_upp)*0.5 
!     if (SEMI_DIM.eq.1) then
!       if (CART.and.(STRUC_DIM.eq.2)) then
!         dy = CNT(SP(n)%model_id)%n_tube
!       else
!         dy = 1
!       endif
  
!     else
!       dy   = (SP(n)%dy_low+SP(n)%dy_upp)*0.5 
  
!     endif

!     if (i.ne.3) then
!       qtrap(i)  = qtrap(i) + (CP_TRAP(n,i)-CN_TRAP(n,i)) * dx 
!     else
!       qtrap(i)  = qtrap(i) + (CP_TRAP(n,i)-CN_TRAP(n,i)) * dx * dy
!     endif

!   enddo
! enddo
! qtrap = Q*qtrap

! endfunction

subroutine get_pos(x, ix,xi,out)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get box position for given x-value
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in ) :: x   !< x-poisition
    integer,intent(out) :: ix  !< box index
    real(8),intent(out) :: xi  !< rel. position in box
    logical,optional    :: out ! if true: outside of grid

    if     (x.lt.SX(1)) then
        ix  = 1
        xi  = dble(0)
        out = .true.

    elseif (x.eq.SX(1)) then
        ix  = 0
        xi  = dble(0)
        out = .false.

    elseif (x.eq.SX(SNX1)) then
        ix  = SNX
        xi  = dble(1)
        out = .false.

    elseif (x.gt.SX(SNX1)) then
        ix  = SNX
        xi  = dble(1)
        out = .true.

    else
        ix  = maxloc(SX,1,SX.le.x)
        xi  = (x-SX(ix))/SDX(ix)
        out = .false.

    endif

endsubroutine

subroutine get_pos_rand(ix1,xi1,ix2,xi2, ix,xi)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get box position for given x-value
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in ) :: ix1 !< left box index
    real(8),intent(in ) :: xi1 !< left rel. position in box
    integer,intent(in ) :: ix2 !< right box index
    real(8),intent(in ) :: xi2 !< right rel. position in box
    integer,intent(out) :: ix  !< random output box index
    real(8),intent(out) :: xi  !< random output rel position in box

    real(8) :: r
    real(8) :: xx1,xx2,xx

    call random_number(r)

    if (ix1.eq.ix2) then
        ix  = ix1
        xi  = xi1 + (xi2-xi1)*r
        
    else
        xx1 = SX(ix1) + xi1*SDX(ix1)
        xx2 = SX(ix2) + xi2*SDX(ix2)
        
        xx  =  xx1 + (xx2-xx1)*r

        ix  = maxloc(SX,1,SX.le.xx)
        xi  = (xx-SX(ix))/SDX(ix)

    endif

endsubroutine

subroutine close_semiconductor
    !!!!!!!!!!!!!!!!
    ! close module
    !!!!!!!!!!!!!!!!

    if (associated(SP))           deallocate(SP)
    if (associated(SP))           deallocate(SP)
    if (associated(CN))           deallocate(CN)
    if (associated(CN2))          deallocate(CN2)
    if (associated(CP))           deallocate(CP)
    if (associated(CD))           deallocate(CD)
    if (associated(CD))           deallocate(CDTOT)
    if (associated(PSI_BUILDIN))  deallocate(PSI_BUILDIN)
    if (associated(FX).and.(size(FX).gt.1)) deallocate(FX) ! todo: check
    if (associated(FY).and.(size(FY).gt.1)) deallocate(FY)
    if (associated(PSI_SEMI))     deallocate(PSI_SEMI)
    if (associated(PDISS))   deallocate(PDISS)
    if (associated(E0_ADD_SEMI))  deallocate(E0_ADD_SEMI)
    if (associated(CN_TRAP))      deallocate(CN_TRAP)
    if (associated(CP_TRAP))      deallocate(CP_TRAP)
    if (associated(TRAP))         deallocate(TRAP)
    if (associated(CN_LAST))      deallocate(CN_LAST)
    if (associated(CN2_LAST))     deallocate(CN2_LAST)
    if (associated(CP_LAST))      deallocate(CP_LAST)
    if (associated(CN_TRAP_LAST)) deallocate(CN_TRAP_LAST)
    if (associated(CP_TRAP_LAST)) deallocate(CP_TRAP_LAST)
    if (associated(SX))           deallocate(SX)
    if (associated(SDX))          deallocate(SDX)
    if (associated(ID_1D))        deallocate(ID_1D)
    if (associated(PSI_1D))       deallocate(PSI_1D)
    if (associated(VT_1D))        deallocate(VT_1D)
    if (associated(E0_ADD_1D))    deallocate(E0_ADD_1D)
    if (associated(IS_SEMI))      deallocate(IS_SEMI)
    if (associated(JouleHeat))    deallocate(JouleHeat)
    if (associated(TAUE))    deallocate(TAUE)
    if (associated(GPSI))    deallocate(GPSI)

endsubroutine

subroutine normalize_semiconductor(l_N)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !normalization of semiconductor properties
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8), intent(in) :: l_N
    SP(:)%dx_low = SP(:)%dx_low/l_N
    SP(:)%dx_upp = SP(:)%dx_upp/l_N
    SP(:)%dy_low = SP(:)%dy_low/l_N
    SP(:)%dy_upp = SP(:)%dy_upp/l_N
    SP(:)%dz_low = SP(:)%dz_low/l_N
    SP(:)%dz_upp = SP(:)%dz_upp/l_N

endsubroutine


endmodule
