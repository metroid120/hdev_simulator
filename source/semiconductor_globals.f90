!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!> \brief semiconductor variables
!> \details used to resolve circular dependencies and allow splitting the stuff in semiconductor module. Should actually contain more of the variables currently in semiconductor.
module semiconductor_globals

implicit none

real(8)                               :: VT_LATTICE           !< thermal voltage at lattice temperature
real(8)                               :: TEMP_LATTICE = 300   !< lattice temperature
real(8)                               :: TEMP_AMBIENT = 300   !< ambient temperature
real(8),dimension(:),pointer          :: UN                   !< (N_SP) semiconductor electron band potential
real(8),dimension(:),pointer          :: UN2                  !< (N_SP) semiconductor electron 2 band potential
real(8),dimension(:),pointer          :: UP                   !< (N_SP) semiconductor hole band potential
real(8),dimension(:),pointer          :: EC0                  !< (N_SP) semiconductor conduction band edge no potential no bgn
real(8),dimension(:),pointer          :: EV0                  !< (N_SP) semiconductor valence band edge no potential no bgn
real(8),dimension(:),pointer          :: BGN_SEMI             !< (N_SP) Band Gap Narrowing at point
real(8),dimension(:),pointer          :: MU_LF                !< (N_SP) Band Gap Narrowing at point
integer                               :: N_SP                 !< number of semiconductor points
logical                               :: HETERO               !< if true: this is a heterostructure
real(8),dimension(:),pointer          :: NI_SQ                !< intrinsic carrier densit squared
real(8),dimension(:),allocatable      :: NL_FACTOR            !< Non-local-Factor
logical                               :: NL_ACTIVATED=.false. !< if true: non-local factor activated


public VT_LATTICE
public TEMP_LATTICE 
public TEMP_AMBIENT
public UN
public EC0
public EV0
public BGN_SEMI
public MU_LF
public UN2
public UP
public NL_FACTOR
public N_SP
public HETERO
public NI_SQ

contains

end module
