!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Solver for drift diffusion equations.
module dd_solver

! ---> variables and functions from other modules ----------------------------------------------------------------------
use mUMFPACK
use m_strings
use heterostructure, only: init_heterostructure
use omp_lib

use phys_const
USE Dual_Num_Auto_Diff
USE Dual_Num_2_Auto_Diff
use m_strings    , only : replace

use crs_matrix

use math_oper

use read_user    , only : US                     !< user input parameter

use assert       , only : assert_isclose                     !< user input parameter
use assert       , only : isclose                     !< user input parameter

use poisson
use save_global
use hdf_functions
use mobility
use recombination
use bandstructure
use structure
use semiconductor_globals
use profile
use non_local
use semiconductor
use contact
use contact
use contact,     type_cont => cont_type               !< get bias voltage for given contact id

use utils
use continuity
use dd_types
use continuity_elec   , only : CONT_ELEC
use continuity_elec2  , only : CONT_ELEC2
use continuity_hole   , only : CONT_HOLE
use continuity_tl     , only : CONT_TL
use continuity_tn     , only : CONT_TN
use continuity_poi    , only : CONT_POI
use continuity_bohm_n , only : CONT_BOHM_N


use continuity     , only : DN
use continuity     , only : DN2
use continuity     , only : DP


implicit none

! -- all continuity equations


! --- global module parameters ----------------------------------------------------------------------------------------
logical                             :: EQUILIBRIUM = .false.       !< if true: Device is in thermal equilibrium
logical                             :: DD_ON                       !< if true: solve DD equations
logical                             :: DD_DISPL                    !< if true: calculate displacement current for transient simulations
logical                             :: DD_OSZ_CUT                  !< if true: smooth solution variables during iterations
logical                             :: DD_REMOVE_ZEROS             !< if true: remove zeros in sparse matrix (due to problem with old MKL)
logical                             :: DD_CONV                     !< if true: simulation has converged
logical                             :: DD_CONV_ALMOST              !< if true: simulation converged almost
logical                             :: DD_SIMUL_INIT               !< if true: matrices for simultaneous solution is initialized

real(8),dimension(:),pointer        :: Q_CONT       => null()   !< (N_CON) charge on contacts current iteration
real(8),dimension(:),pointer        :: Q_CONT_LAST  => null()   !< (N_CON) charge on contacts from last iteration

! -- simultaneous solution parameters --
! TYPE(MKL_DSS_HANDLE)                :: DD_SIMUL_HANDLE        !< handle for sparse matrix of complete equation system
type(crs_mat_real)                  :: DD_SIMUL               !< sparse matrix for complete equation system
type(crs_mat_real)                  :: DD_SIMUL_CCS           !< CCS Version of DD_SIMUL, needed for UMFPACK
real(8),dimension(:),pointer        :: DD_SIMUL_RHS => null() !< (N_PCC) right hand side of matrix for complete equation system
real(8),dimension(:),pointer        :: DPCC         => null() !< (N_PCC) change of solution complete equation system
real(8),dimension(:),pointer        :: DPCC_LAST    => null() !< (N_PCC) change of solution of complete equation sytem previous iteration
integer                             :: N_PCC                  !< size of solution vector for simultaneous solution
integer(c_intptr_t) DD_SIMUL_NUMERIC, DD_SIMUL_SYMBOLIC

! -- AC parameters --
! TYPE(MKL_DSS_HANDLE)                :: DD_AC_HANDLE          !< handle with dss sparse matrix for ac solution
type(crs_mat_cmplx)                 :: DD_AC                 !< matrix for ac solution
type(crs_mat_cmplx)                 :: DD_AC_CCS             !< CCS Version of DD_SIMUL, needed for UMFPACK
complex(8),dimension(:),pointer     :: DD_AC_RHS  => null()  !< rhs for ac solution
complex(8),dimension(:),pointer     :: DELTA_AC   => null()  !< (NR_AC) solution of AC equation
integer(c_intptr_t) DD_AC_NUMERIC, DD_AC_SYMBOLIC

! -- saving parameters --
logical                             :: DD_SV_NEWTON       !< if true: save internal quantities for every newton iteration step
logical,dimension(2)                :: DD_SV_TR           !< if true: (1) save transient file (2) save inqu file for transient-simulation
integer,dimension(2)                :: DD_ID              !< file_id for iv saving (1) iv file (2) transient file
character(len=10000)                :: DD_COL_INQU        !< column names for inqu file
character(len=10000)                :: DD_COL_TR          !< column names for transient file
character(len=10000)                :: DD_COL_SPECTRUM    !< column names for spectrum file (obsolete?)

type(string)                       :: iv_cols(100) ! maximum number of columns of IV file
real(8),dimension(:,:),allocatable :: iv_vals      ! values of IV file

real(8) :: fac_damp = 1 !used for damping coupling matrix




class(cont_pointer),dimension(:),pointer :: CONTS ! pointer to all continuity equation in complete equation system

private

public init_dd_solver
public dd_solve
public close_dd_solver
public write_iv
public normalize_conts

contains

subroutine init_dd_solver
    !!!!!!!!!!!!!!!!!!!!
    ! init the dd solver
    !!!!!!!!!!!!!!!!!!!!
    integer :: n,id

    ! --> check if DD solution is needed
    if ((US%dd%n_iter.ge.0).and.SEMI_EXIST) then
        DD_ON = .true.
    else
        DD_ON = .false.
        return
    endif

    ! --> global DD settings
    DD_TUNNEL       = US%tunnel%activate.gt.0
    DD_OSZ_CUT      = .false.
    DD_DISPL        = US%biasdef%displ.gt.0
    DD_SIMUL_INIT   = .false.
    DD_CONV         = .false.
    DD_ELEC         = (US%dd%elec.gt.0).and.ELEC
    DD_ELEC2        = (US%dd%elec2.gt.0).and.ELEC2
    DD_CONT_BOHM_N  = (US%dd%bohm_n.gt.0).and.ELEC
    DD_HOLE         = (US%dd%hole.gt.0).and.HOLE
    DD_TL           = (US%dd%tl.gt.0)
    DD_TN           = (US%dd%tn.gt.0)
    DD_BBT          = (US%tunnel%bbt.gt.0).and.BBT
    DD_REMOVE_ZEROS = (US%dd%remove_zeros.gt.0)

    ! --> this helper struct contains all continuty equations in correct order (see the enum in continuity.f90)
    !ORDER IS IMPORTANT
    allocate(CONTS(nvar-1))
    CONTS(1)%p => CONT_POI
    CONTS(2)%p => CONT_ELEC
    CONTS(3)%p => CONT_ELEC2
    CONTS(4)%p => CONT_HOLE
    CONTS(5)%p => CONT_TN
    CONTS(6)%p => CONT_TL
    CONTS(7)%p => CONT_BOHM_N

    ! --> set contact boundary conditions
    do n=1,N_SP
        if (SP(n)%cont) then
            if (CON(SP(n)%cont_id)%shky) then
                ! for schottky contact current boundary (landauer or velocity)
                SP(n)%set_neutrality_n = .false.
                SP(n)%set_neutrality_n = .false.
                SP(n)%set_psi          = .false.
                if (CON(SP(n)%cont_id)%v_n.eq.0) then
                    SP(n)%set_qfn          = .true.
                endif
                if (CON(SP(n)%cont_id)%v_n2.eq.0) then
                    SP(n)%set_qfn2         = .true.
                endif
                if (CON(SP(n)%cont_id)%v_p.eq.0) then
                    SP(n)%set_qfp          = .true.
                endif
                SP(n)%set_qf           =SP(n)%set_qfp.or.SP(n)%set_qfn
            elseif (CON(SP(n)%cont_id)%limiting) then
                ! for schottky contact current boundary (landauer or velocity)
                SP(n)%set_neutrality_n = .false.
                SP(n)%set_neutrality_n = .false.
                SP(n)%set_psi          = .true.
                SP(n)%set_qfn          = .false.
                SP(n)%set_qfp          = .false.
                SP(n)%set_qf           = .false.

            else
                ! ohmic contact
                if (CON(SP(n)%cont_id)%setfloat) then
                    !SP(n)%set_neutrality_n = DD_ELEC.and.DD_HOLE.and.(CD(n).ge.0)
                    SP(n)%set_neutrality_n = .true.
                    !SP(n)%set_neutrality_p = DD_ELEC.and.DD_HOLE.and.(CD(n).lt.0)
                    SP(n)%set_neutrality_p = .true.
                    SP(n)%set_qfp          = .false.
                    SP(n)%set_qfn          = .false.
                    SP(n)%set_qf           = .false.
                    SP(n)%set_psi          = .true. !not sure

                else
                    SP(n)%set_neutrality_n = .false.
                    SP(n)%set_neutrality_p = .false.
                    SP(n)%set_qfp          = .true.
                    SP(n)%set_qfn          = .true.
                    SP(n)%set_qfn2         = .true.
                    SP(n)%set_qf           = .true.
                    SP(n)%set_psi          = .true.

                endif
            endif
        endif
    enddo

    !set supply contact boundary conditions
    do n=1,N_SP
        id = SP(n)%cont_id
        if (id.GT.1) then
            if (CON(id)%supply) then
                SP(n)%set_neutrality_n                         = .false.
                SP(n)%set_neutrality_n                         = .false.

                !write this here explicitly for safety
                if (CON(SP(n)%cont_id)%supply_p) then 
                    SP(n)%set_qfp = .true.
                else
                    SP(n)%set_qfp = .false.
                endif
                if (CON(SP(n)%cont_id)%supply_n) then
                    SP(n)%set_qfn = .true.
                else
                    SP(n)%set_qfn = .false.
                    SP(n)%set_qfn2 = .false.
                endif

                SP(n)%set_qf                                   = .false.
                SP(n)%set_psi                                  = .false.

            endif
        endif
    enddo

    allocate(CN0(N_SP))
    allocate(CN20(N_SP))
    allocate(CP0(N_SP))
    CN0  = 0
    CN20 = 0
    CP0  = 0
    allocate(Q_CONT(N_CON))
    allocate(Q_CONT_LAST(N_CON))

    ! --> allocate DD point properties
    allocate(DN(N_SP))
    allocate(DN2(N_SP))
    allocate(DP(N_SP))
    if(DD_TUNNEL) then 
        do n=1,N_SP
            if (DD_ELEC) then
                allocate(DN(n)%g_tun_dpsi(N_SP))
                allocate(DN(n)%g_tun_dqf(N_SP))
                DN(n)%g_tun_dpsi = 0
                DN(n)%g_tun_dqf  = 0

            endif
            if (DD_HOLE) then
                !hole tunneling currently not implemented:
                ! allocate(DP(n)%g_tun_dpsi(N_SP))
                ! allocate(DP(n)%g_tun_dqf(N_SP))
                ! DP(n)%g_tun_dpsi = 0
                ! DP(n)%g_tun_dqf  = 0

            endif
        enddo
    endif

    ! --> allocate tunneling vector fields
    ! allocate(QFP_1D(SNX1))
    ! allocate(QFN_1D(SNX1))
    ! if (DD_TUNNEL) then
    !   allocate(JN_TUN_1D(SNX1))
    !   allocate(GN_TUN_1D(SNX1))
    !   allocate(GN_TUN_1D_DQF(3,SNX1))
    !   if (AC_SIM.or.(US%dd%tun_derive.gt.0)) allocate(GN_TUN_1D_DPSI(SNX1,SNX1))
        
    !   allocate(JP_TUN_1D(SNX1))
    !   allocate(GP_TUN_1D(SNX1))
    !   allocate(GP_TUN_1D_DQF(3,SNX1))
    !   if (AC_SIM.or.(US%dd%tun_derive.gt.0)) allocate(GP_TUN_1D_DPSI(SNX1,SNX1))
      
      ! if (DD_ELEC) then
      !   allocate(JN_TUN_1D_DQF(N_SP,SNX1))
      !   allocate(JN_TUN_1D_DPSI(N_SP,SNX1))
        
      ! endif
      
      ! if (DD_HOLE) then
      !   allocate(JP_TUN_1D_DQF(N_SP,SNX1))
      !   allocate(JP_TUN_1D_DPSI(N_SP,SNX1))
      
      ! endif
      
    ! endif

    ! --> init continuity equations (could be better if we mark active solution vars before)
    N_PCC = 0
    call CONT_POI%init(psi_)
    if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited poi psi'
    ! call CONT_POI%init_handle(psi_)
    if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited poi psi handle'
    N_PCC = NXYZ
    if (DD_ELEC) then
        call CONT_ELEC%init(phin_)
        N_PCC = N_PCC + N_SP
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited phin phin'

    endif
    if (DD_ELEC2) then
        call CONT_ELEC2%init(phin2_)
        N_PCC = N_PCC + N_SP
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited phin2 phin2'

    endif
    if (DD_HOLE) then
        call CONT_HOLE%init(phip_)
        N_PCC = N_PCC + N_SP
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited phip phip'

    endif
    if (DD_TN) then
        call CONT_TN%init(tn_)
        N_PCC = N_PCC + N_SP
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited phip phip'

    endif
    if (DD_TL) then
        call CONT_TL%init(tl_)
        N_PCC = N_PCC + 1
        CONT_TL%var(1) = TEMP_LATTICE
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited tn tn'

    endif
    if (DD_CONT_BOHM_N) then
        call CONT_BOHM_N%init(ln_)
        N_PCC = N_PCC + N_SP
        if (US%dd%debug.ge.1) write(*,*) 'dd_solver inited ln ln'

    endif

    write(*,*) 'dd_solver inited conts'

    allocate( DD_SIMUL_RHS( N_PCC ) )
    allocate( DPCC( N_PCC ) )
    allocate( DPCC_LAST( N_PCC ) ) 

    ! -->  AC simulation only if simultanious DC is activated (todo: change to simul automatically)
    if ((AC_SIM).and.(US%dd%simul.lt.1)) then
        write(*,*) '***error*** AC simulation only possible with simultaneous DC solution method'
        stop
    endif
    if (AC_SIM) then
        allocate( DELTA_AC( N_PCC ) )
        allocate( DD_AC_RHS(N_PCC ) )
        DELTA_AC = 0
        DD_AC_RHS = 0

    endif

    call init_save_dd

    if (US%output%cap_lev.ge.1) call save_capacity

endsubroutine

subroutine init_cn0_cp0
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the intrinsic carrier densities
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    CN0 = CN
    CN20 = CN2
    CP0 = CP

endsubroutine

subroutine init_dd_simul
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init coupling matrices (off diagonal). the jacobians with respect to the main variables are already inited in init_dd
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i_cont, j_cont, i_var
    logical :: init

    if (DD_SIMUL_INIT) return

    do i_cont=1,nvar-1
        do i_var=1,nvar-1

            !main diagonal already inited
            if (i_cont.eq.i_var) cycle

            !see if this continuity equation is active
            if (.not.CONTS(i_cont)%p%active) cycle

            !see if continuity equation with target var is active
            init = .false.
            do j_cont=1,nvar-1
                if (CONTS(j_cont)%p%i_var.eq.i_var) then
                    if (CONTS(j_cont)%p%active) init = .true.
                endif
            enddo

            !init coupling jacobi
            if (init) call CONTS(i_cont)%p%init(i_var)
        enddo
    enddo

    DD_SIMUL_INIT = .true.

endsubroutine

subroutine dd_solve(conv)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! solve the drift diffusion equation system
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(out) :: conv !< if true: simulation converged

    integer :: i


    conv = .true.
    if (.not.DD_ON) return

    ! --> init psi
    call set_psi_initial

    call set_psi_semi

    ! --> init temperature profile
    call update_temp

    ! --> DC solution
    if (DD_ELEC)  CN_LAST   = CN
    if (DD_ELEC2) CN2_LAST  = CN2
    if (DD_HOLE)  CP_LAST   = CP
    do i=1,nvar-1
        if (.not.CONTS(i)%p%active) cycle
        call CONTS(i)%p%save()
    enddo

    call dd_solve_dc
    conv = DD_CONV
    !if ((.not.conv).and.(.not.OP_FORCE)) then
    if ((.not.conv)) then
        do i=1,nvar-1
            if (.not.CONTS(i)%p%active) cycle
            call CONTS(i)%p%restore()
        enddo
        DD_CONV_ALMOST = .true.
        if (DD_ELEC)  CN     = CN_LAST
        if (DD_ELEC2) CN2    = CN2_LAST
        if (DD_HOLE)  CP     = CP_LAST
    endif
    write(DEB_ID,*) 'DD-solver DC convergence:',conv
    write(DEB_ID,*) ''

    if ((OP_SAVE.and.conv).or.(OP_FORCE)) then
        ! --> AC solution
        call dd_solve_ac
        ! --> transient solution
        call dd_solve_tr
    endif

endsubroutine


subroutine dd_solve_dc
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! solve the DC drift diffusion equation system
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8)          :: lambda,pdifftol
    logical          :: simul,converged=.False.
    integer          :: niter,i,numb_diver,j,l,iter_solution,jj,ll,sp_id
    real(8)          :: dpcc_norm,dpcc_norm_last,qsum,rpcc_norm
    real(8)          :: tcpu1,tcpu2,norm_solution
    integer          :: simul_start
    integer          :: update_limit
    integer          :: damp_iter=0 ,damp_iter_limit = 15
    logical          :: disc_changed

    real(8)          :: g_u_k, g_u_k_p1, t_k, K, cond, rel_change, bias_j

    character(len=250) :: output_header,output_underline

    pdifftol       = US%dd%pdiff_tol
    niter          = US%dd%n_iter
    simul_start    = US%dd%simul
    if (OP.lt.US%dd%op_simul_start) simul_start = 0 ! OP is actual operation point
    simul          = .false.
    numb_diver     = 0
    dpcc_norm_last = 0
    update_limit   = 10
    disc_changed   = .false.
    DD_CONV        = .false.

    EQUILIBRIUM=.true.
    ! get bias of one contact
    do j=1,N_CON
        if (j.eq.TAMB_ID) cycle
        bias_j = get_bias(j)
        exit
    enddo
    ! see if all contacts at same bias
    do j=2,N_CON
        if (j.eq.TAMB_ID) cycle
        EQUILIBRIUM = (get_bias(j) == bias_j).and.EQUILIBRIUM !if equilibrium==true, we already know qfn and qfp
    enddo


    if (.not.ELEC)  CN  = 0
    if (.not.ELEC2) CN2 = 0
    if (.not.HOLE)  CP  = 0

    write(*,*)
    write(*,'(A)') '----> DD SOLVER'
    write(*,*)

    if (SEMI_DIM.eq.1) then
        output_underline =   '---+---------+---------+---------+---------+--------+----------+'
        output_header      = ' # |I_D(A/m2)|I_G(A/m2)| Q(C/m2) |  t_lat  |  damp  | tcpu(s)  |'
        output_header   = replace(output_header, 'D', CON(DRAIN)%name(1:1))
        if (GATE.ne.0) output_header = replace(output_header, 'G', CON(GATE)%name(1:1))
            output_header = trim(output_header)//' POISSON    |'
            output_underline = trim(output_underline)//'------------+'
        if (DD_ELEC) then 
            output_header = trim(output_header)//' CONT_ELEC  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_ELEC2) then
            output_header = trim(output_header)//' CONT_ELEC2 |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_HOLE) then
            output_header = trim(output_header)//' CONT_HOLE  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_TN) then
            output_header = trim(output_header)//' CONT_TN    |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_TL) then
            output_header = trim(output_header)//' CONT_TL    |'
            output_underline = trim(output_underline)//'------------+'
            output_header = trim(output_header)//' PDISSRTH   |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_CONT_BOHM_N) then
            output_header = trim(output_header)//' CONT_BOHM  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        write(*,'(A)') output_header
        write(*,'(A)') output_underline

    elseif (SEMI_DIM.eq.2) then
        output_underline =   '---+---------+---------+---------+---------+--------+----------+'
        output_header      = ' # |I_D(A/m )|I_G(A/m )| Q(C/m ) |  t_lat  |  damp  | tcpu(s)  |'
        output_header   = replace(output_header, 'D', CON(DRAIN)%name(1:1))
        if (GATE.ne.0) output_header = replace(output_header, 'G', CON(GATE)%name(1:1))
            output_header = trim(output_header)//' POISSON    |'
            output_underline = trim(output_underline)//'------------+'
        if (DD_ELEC) then 
            output_header = trim(output_header)//' CONT_ELEC  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_ELEC2) then
            output_header = trim(output_header)//' CONT_ELEC2 |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_HOLE) then
            output_header = trim(output_header)//' CONT_HOLE  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_TN) then
            output_header = trim(output_header)//' CONT_TN    |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_TL) then
            output_header = trim(output_header)//' CONT_TL    |'
            output_underline = trim(output_underline)//'------------+'
            output_header = trim(output_header)//' PDISSRTH   |'
            output_underline = trim(output_underline)//'------------+'
        endif
        if (DD_CONT_BOHM_N) then
            output_header = trim(output_header)//' CONT_BOHM  |'
            output_underline = trim(output_underline)//'------------+'
        endif
        write(*,'(A)') output_header
        write(*,'(A)') output_underline

    else
        write(*,'(A)') ' # | I_d(A)  |I_g(A)   | Q(C)    |  d_psi |  alpha |  d_rhs | tcpu(s)'
        write(*,'(A)') '---+---------+---------+---------+--------+--------+--------+--------+'

    endif

    ! --> calculate electron density/current
    call set_density_and_current

    ! --> save newton
    call save_dd('newton_dc',0,dble(0),dble(0),lambda)

    ! main newton loop
    do i=1,niter
        do j=1,size(CONTS)
            if (CONTS(j)%p%active) CONTS(j)%p%var_last = CONTS(j)%p%var
        enddo

        if (i.eq.simul_start) then
            call init_dd_simul

            simul=.true.
        endif
        call cpu_time(tcpu1)

        if (i.eq.1) K = 0

        ! calculate x_k
        if (simul) then
            call solve_dd_simul(i,converged)
        else
            call solve_dd_succ(lambda,i) 
        endif

        !calculate g(u_k)
        g_u_k = 0
        do j=1,size(CONTS)
            if (CONTS(j)%p%active) then 
                g_u_k = g_u_k + norm2(CONTS(j)%p%rhs)
            endif
        enddo

        t_k = 1
        if (EQUILIBRIUM) then ! in Equilibrium only Poisson Equaion needs solution, no damping needed.
            call dd_update(t_k)

        elseif (.true.) then
          if (US%dd%damp_method(1:5).eq.'banks') then
              damp_iter = 0
              do 
                  ! damping after banks and rose:
                  ! https://www.iue.tuwien.ac.at/phd/ceric/node17.html

                  !calculate  t_k
                  t_k = 1/(1+K*g_u_k)

                  ! update solution 
                  call dd_update(t_k)

                  !calculate g(u_k_p1)
                  g_u_k_p1 = 0
                  do j=1,size(CONTS)
                      if (CONTS(j)%p%active) g_u_k_p1 = g_u_k_p1 + norm2(CONTS(j)%p%rhs)
                  enddo

                  !find new K
                  cond = (1-g_u_k_p1/g_u_k)/t_k
                  if ( cond.lt.US%dd%damp ) then 
                      if (K.eq.0) then
                          K=1
                      else
                          K=10*K
                      endif
                  else
                      K = K/10
                  endif

                  damp_iter = damp_iter + 1

                  rel_change = abs(g_u_k_p1 - g_u_k)/g_u_k
                  ! are we content with this damping?
                  if (( rel_change.gt.(1e-1) ).or.(damp_iter.gt.damp_iter_limit)) then
                      exit
                  endif

              enddo
          elseif (US%dd%damp_method(1:3).eq.'pot') then !potential damping
              norm_solution = maxval(abs(CONT_POI%var_last))
              !norm = 1
              if (norm_solution.lt.1e-12) then
                  t_k = 1
              else
                  t_k  = ( 1 + US%dd%damp*log(norm_solution)/VT_LATTICE ) / ( 1+ US%dd%damp * (norm_solution/VT_LATTICE) - 1 )
              endif
              call dd_update(t_k)
          else
              call dd_update(t_k)
          endif
        else
          !for successive solution everything should already be updated -> do nothing
        endif

        qsum = get_qsemi()

        ! --> check convergence
        converged = .true.
        do j=1,size(CONTS)
            if (.not.CONTS(j)%p%active) cycle
            call CONTS(j)%p%converged()
            converged = converged.and.CONTS(j)%p%has_converged
        enddo

        if (CRYO) then ! for CRYO simulation we just take what we have at last iteration
            if (i.eq.niter) then
                converged=.TRUE.
            endif
        endif


        ! to test:
        ! converged = converged.and.(i.gt.35)

        if (converged) then
            qsum = get_qsemi()

            if (EQUILIBRIUM) then
                call set_psi_0
                call set_ni_sq
                call init_cn0_cp0
            endif

            !calculate gradients for output , maybe this is possible smarter?
            if (SEMI_DIM.eq.1) then
                do l=1,N_SP-1
                    !GPSI(l) = ((CONT_POI%var(l+1) - PSI0(l+1)) - (CONT_POI%var(l)-PSI0(l))  )/(SP(l)%dx_low + SP(l)%dx_upp)
                    ! no psi0 version:
                    if (.not.(SP(l)%dx_upp.eq.0)) then
                        !GPSI(l) = ((CONT_POI%var(l+1) + UN(l+1) ) - (CONT_POI%var(l) + UN(l))  )/(SP(l)%dx_upp)
                        GPSI(l) = ((CONT_POI%var(l+1) ) - (CONT_POI%var(l))  )/(SP(l)%dx_upp)
                    endif
                enddo
            elseif (SEMI_DIM.eq.2) then
                do jj=2,NY1-1
                    do ll=2,NX1-1
                        sp_id = POINT(ll,jj,1)%sp_idx !index to point in semiconductor
                        if (sp_id.ne.0) then !is semiconductor_point
                            if (SP(sp_id)%dx_upp.ne.0) then
                                !GPSI(sp_id) = (((CONT_POI%var(sp_id+1) + UN(sp_id+1) ) - (CONT_POI%var(sp_id) + UN(sp_id))  )/(SP(sp_id)%dx_upp))**2
                                GPSI(sp_id) = (((CONT_POI%var(sp_id+1)  ) - (CONT_POI%var(sp_id) )  )/(SP(sp_id)%dx_upp))**2
                            endif
                            if (SP(sp_id)%dy_upp.ne.0) then
                                !GPSI(sp_id) = GPSI(sp_id) + (((CONT_POI%var(sp_id+1) + UN(sp_id+1) ) - (CONT_POI%var(sp_id) + UN(sp_id))  )/(SP(sp_id)%dy_upp))**2
                                GPSI(sp_id) = GPSI(sp_id) + (((CONT_POI%var(sp_id+1)  ) - (CONT_POI%var(sp_id) )  )/(SP(sp_id)%dy_upp))**2
                            endif
                        endif
                    enddo
                enddo
                GPSI = sqrt(GPSI)

            endif

            ! solution found
            DD_CONV        = .true.
            DD_CONV_ALMOST = .true.
            iter_solution  = i
            call cpu_time(tcpu2)

            write(6,'(A,$)') char(13)
            write(6,'(I3,A$)') i,'| ' ! 3
            
            if (DRAIN.gt.0) then
                call write_vec(CON(DRAIN)%current*NORM%J_N,.true.,' | ')
            else
                call write_vec(dble(0),.true.,' | ')
            endif
            if (GATE.gt.0) then
                call write_vec(CON(GATE)%current*NORM%J_N,.true.,' | ')
            else
                call write_vec(dble(0),.true.,' | ')
            endif
            call write_vec(qsum*NORM%l_N**SEMI_DIM*NORM%N_N,.true.,' | ')
            call write_vec(TEMP_LATTICE,.true.,' | ')
            call write_vec(t_k,.false.,' | ')
            call write_vec(dble(tcpu2-tcpu1),.false.,' ')

            if (fac_damp.ge.1) then !only exit newton if also all coupling jacobians elements are set and not damped out
                write(6,*)
                write(*,*)
                write(*,'(A)') '----> solution found!!!! :-)'
                write(*,*)
                exit 
            endif
        endif

        ! --> save newton iteration
        call save_dd('newton_dc',i,dpcc_norm,rpcc_norm,lambda) ! dc/tr/newton_dc/newton_tr

        call cpu_time(tcpu2)

        write(6,'(A,$)') char(13)
        write(6,'(I3,A$)') i,'| ' ! 3
        if (DRAIN.gt.0) then
            call write_vec(CON(DRAIN)%current*NORM%J_N,.true.,' | ')
        else
            call write_vec(dble(0),.true.,' | ')
        endif
        if (GATE.gt.0) then
            call write_vec(CON(GATE)%current*NORM%J_N,.true.,' | ')
        else
            call write_vec(dble(0),.true.,' | ')
        endif
        call write_vec(qsum*NORM%l_N**SEMI_DIM*NORM%N_N,.true.,'| ')
        call write_vec(TEMP_LATTICE,.true.,'| ')
        call write_vec(dble(t_k),.false.,' | ')
        call write_vec(dble(tcpu2-tcpu1),.false.,'     | ')
        do j=1,size(CONTS)
            if (.not.(CONTS(j)%p%active)) cycle
            call CONTS(j)%p%print_status()
        enddo
        if (CONT_TL%active) call write_vec(PDISSRTH,.false.,'     | ')
        write(6,*)
          
        if (i.eq.niter) then
            DD_CONV=.false.
            DD_CONV_ALMOST = dpcc_norm.lt.1
            write(6,*)
            write(*,*)
            write(*,'(A)') '----> max. number of iteration steps exceeded :-('
            write(*,*)
        endif

    enddo

    !if (DD_CONV.and.OP_SAVE) call save_inqu()
    if (OP_SAVE) call save_inqu()

    if (DD_CONV) then
        call store_iv(iter_solution,dpcc_norm,rpcc_norm)

    elseif (OP_FORCE) then
        call store_iv(-niter,dpcc_norm,rpcc_norm)

    endif

endsubroutine

subroutine update_temp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! update the lattice temperature and thermal voltage
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: t_last

    t_last = TEMP_AMBIENT
    TEMP_AMBIENT = get_bias(TAMB_ID)
    if (.not.CONTS(tl_)%p%active) then
        !lattice temperature is temperature at Temperature contact
        TEMP_LATTICE = TEMP_AMBIENT

    else !lattice temperature is ambient plus self heating
        TEMP_LATTICE = CONTS(tl_)%p%var(1)
    endif

    if (t_last.ne.TEMP_AMBIENT) then !re-init things that change with temperature
        call init_heterostructure !bandstructure 
        call init_energylevels  !resulting energy at contacts => boundary condition 
        call init_psi_buildin !built in potential
        call init_mu_lf !low field mobility
    endif


    VT_LATTICE   = TEMP_LATTICE*BK/Q
endsubroutine


subroutine dd_solve_ac
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! equation system AC solution !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8)                         :: omega
    integer                         :: i,r_n,n,p,k,m,j,i_col,i_p,pp,n_to
    type(crs_mat_cmplx)             :: mat_1,mat_2,mat_4
    integer                         :: nz_before
    complex(8),dimension(:,:),allocatable       :: y_para
    complex(8),dimension(:),allocatable :: curr_x, curr_y,curr_tun_x,dens_n,dens_n2,dens_p
    logical                         :: ac_init
    ! TYPE(MKL_DSS_HANDLE)            :: handle1
    logical                         :: changed_dss
    type(crs_mat_cmplx),dimension(7,7) :: jacobi_mats !this matrix will hold all jabobians

    ! slow? move allocation somewhere else.
    type(string)                       :: acinqu_cols(25)  !columns of acinqu file
    type(string)                       :: aciv_cols(20)  !columns of acinqu file
    real(8),dimension(:,:),allocatable :: acinqu_vals      ! values of acinqu file
    real(8),dimension(:,:),allocatable :: aciv_vals      ! values of acinqu file
    character(len=15)                  :: str_op_rec
    character(len=15)                  :: str_freq

    allocate(y_para(N_CON-1,N_CON-1))

    if (.not.AC_SIM) return
    ac_init = .false.

    !100xN_SP columns maximum size
    allocate(acinqu_vals(100,N_SP))
    allocate(aciv_vals(100,F_LEN))
    acinqu_vals = 0
    aciv_vals   = 0

    call init_dd_simul

    ! ----> AC output file columns ---------------------------------------------------------
    aciv_cols(1)%str  = 'FREQ'
    i = 1
    !N_CON-1 because T contact is not excited for AC analysis
    do p=1,N_CON-1
        do pp=1,N_CON-1
            i = i + 1
            aciv_cols(i)%str  = 'Y_'//toUpper(trim(CON(pp)%name))//toUpper(trim(CON(p)%name))//'|REAL'
            i = i + 1
            aciv_cols(i)%str  = 'Y_'//toUpper(trim(CON(pp)%name))//toUpper(trim(CON(p)%name))//'|IMAG'
        enddo
    enddo

    ! --> build ac matrix
    call set_density_and_current

    ! init complex jacobians
    do i=1,nvar-1 
        if (CONTS(i)%p%active) then
            call CONTS(i)%p%set(.true.)
            do n=1,nvar-1
                !is the continuity equation with this target variable active?
                if (CONTS(n)%p%active) call copy_crs_real_part(CONTS(i)%p%jacobi(n), CONTS(i)%p%jacobi_ac(n))
            enddo
        endif
    enddo

    do n=1,F_LEN
        omega = dble(2)*PI*FREQ(n)*NORM%t_N !check this
        ! set continuiy equations ac case
        do i=1,nvar-1
            if (.not.CONTS(i)%p%active) cycle
            call CONTS(i)%p%set_ac(omega)
        enddo

        ! build big jacobian by putting all individual jacobians into one big one
        do i=1,nvar-1
            do j=1,nvar-1
                if (CONTS(j)%p%active.and.CONTS(i)%p%active) then
                    jacobi_mats(i,j) = CONTS(j)%p%jacobi_ac(i)
                endif
            enddo
        enddo

        mat_2 = join_mats(jacobi_mats)
        nz_before = mat_2%nz

        !remove zeros present for tunneling
        if (DD_REMOVE_ZEROS) then
            ! --> delete Zeros
            call remove_zeros_crs_cmplx(mat_2,mat_4)
            call dealloc_crs(mat_2)
            if (n.eq.1) write(*,*) 'nonzeros: ',mat_4%nz,' / ',nz_before    

        else
            mat_4 = mat_2

        endif

        changed_dss = .false.
        if (DD_REMOVE_ZEROS.and.ac_init) then
            if (size(mat_4%columns).ne.size(DD_AC%columns)) then
                changed_dss = .true.

            elseif (size(mat_4%row_index).ne.size(DD_AC%row_index)) then
                changed_dss = .true.

            else
                do i=1,size(DD_AC%row_index)
                    if (DD_AC%row_index(i).ne.mat_4%row_index(i)) then
                        changed_dss=.true.
                        exit
                    endif
                enddo

                do i=1,size(DD_AC%columns)
                    if (DD_AC%columns(i).ne.mat_4%columns(i)) then
                        changed_dss=.true.
                        exit
                    endif
                enddo
            endif
        endif
        if (.not.ac_init) then
            call copy_crs(mat_4, DD_AC)
            ! call init_dss(DD_AC, DD_AC_HANDLE) !bug: only works with n_core=1
            call DD_AC_CCS%init(nr=DD_AC%nz , nc=DD_AC%nc, nz=DD_AC%nz, is_ccs=.true.) !CCS matrix for big jacobian
            ac_init = .true.

        elseif (changed_dss) then
            ! --> number of nonzero elements changed
            ! error   = DSS_DELETE(DD_AC_HANDLE,MKL_DSS_DEFAULTS)
            call dealloc_crs(DD_AC)
            call dealloc_crs(DD_AC_CCS)
            call copy_crs(mat_4, DD_AC)
            call DD_AC_CCS%init(nr=DD_AC%nz , nc=DD_AC%nc, nz=DD_AC%nz, is_ccs=.true.)
            ! call init_dss(DD_AC, DD_AC_HANDLE)

        endif
        DD_AC%values = mat_4%values
        call dealloc_crs(mat_4)


        ! handle1 = DD_AC_HANDLE
        ! error   = DSS_FACTOR_COMPLEX_D(handle1, MKL_DSS_DEFAULTS, DD_AC%values ) !bug: only works with n_core=1
        

        do p=1,N_CON-1 !loop over contacts
            ! zero before writing ones
            DD_AC_RHS = cmplx(dble(0),dble(0),8)
            ! --------------> RHS -----------------
            ! --> poisson
            i = 0
            do k=1,NZ1
                do j=1,NY1
                    do m=1,NX1
                        i=i+1
                        if ((POINT(m,j,k)%cont_id.eq.p)) then !.and.SP()%set_psi) then   
                            DD_AC_RHS(i) = cmplx(dble(1),dble(0),8)

                        else
                            DD_AC_RHS(i) = cmplx(dble(0),dble(0),8)

                        endif
                    enddo
                enddo
            enddo
            r_n = CONT_POI%jacobi(psi_)%nr
            ! --> elec continuity
            if (DD_ELEC) then
                do i=1,N_SP
                    if (SP(i)%set_qfn) then
                        if (SP(i)%cont_id.eq.p) then
                            DD_AC_RHS(r_n+i) = cmplx(dble(1),dble(0),8)

                        else
                            DD_AC_RHS(r_n+i) = cmplx(dble(0),dble(0),8)

                        endif
                    else
                        DD_AC_RHS(r_n+i)   = cmplx(dble(0),dble(0),8)

                    endif
                enddo
                r_n = r_n + N_SP
            endif
            ! --> elec 2 continuity
            if (DD_ELEC2) then
                do i=1,N_SP
                    if (SP(i)%set_qfn2) then
                        if (SP(i)%cont_id.eq.p) then
                            DD_AC_RHS(r_n+i) = cmplx(dble(1),dble(0),8)

                        else
                            DD_AC_RHS(r_n+i) = cmplx(dble(0),dble(0),8)

                        endif
                    else
                        DD_AC_RHS(r_n+i)   = cmplx(dble(0),dble(0),8)

                    endif
                enddo
                r_n = r_n + N_SP
            endif
            ! --> hole continuity
            if (DD_HOLE) then
                do i=1,N_SP
                    if (SP(i)%set_qfp) then
                        if (SP(i)%cont_id.eq.p) then
                            DD_AC_RHS(r_n+i) = cmplx(dble(1),dble(0),8)
                        else
                            DD_AC_RHS(r_n+i) = cmplx(dble(0),dble(0),8)
                        endif
                    else
                        DD_AC_RHS(r_n+i)   = cmplx(dble(0),dble(0),8)
                    endif
                enddo
                r_n = r_n + N_SP
            endif

            ! --> solve
            ! error    = DSS_SOLVE_COMPLEX_D( handle1, MKL_DSS_DEFAULTS, DD_AC_RHS, 1, DELTA_AC )
            call solve_ac_umfpack

            r_n               = 0
            do i=1,nvar-1
                if (.not.CONTS(i)%p%active) cycle
                n_to = r_n+CONTS(i)%p%jacobi(i)%nr
                CONTS(i)%p%delta_ac = DELTA_AC(r_n+1 : n_to)
                r_n = r_n + CONTS(i)%p%jacobi(i)%nr
            enddo
            call get_ac_inqu_current(curr_x,curr_y,curr_tun_x,dens_n,dens_n2,dens_p)
            call get_ac_current(omega)
            do i=1,N_CON-1
                y_para(i,p) = CON(i)%ac_current !*NORM%J_N*NORM%l_N
            enddo
            if (n.eq.1) then !we save only ac inqu data for first operating point.
                if (US%output%inqu_lev.gt.0) then !only save if user wants this
                    curr_x     = curr_x    *NORM%J_N
                    curr_y     = curr_y    *NORM%J_N
                    curr_tun_x = curr_tun_x*NORM%J_N

                    i_col                  = 0
                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'X'
                    do i_p=1,N_SP
                        acinqu_vals(i_col,i_p)   = X(SP(i_p)%id_x)*NORM%l_N
                    enddo

                    if (SEMI_DIM.gt.1) then
                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'Y'
                        do i_p=1,N_SP
                            acinqu_vals(i_col,i_p)   = Y(SP(i_p)%id_y)*NORM%l_N
                        enddo
                    endif

                    if (DD_ELEC) then
                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 're_qfn'
                        acinqu_vals(i_col,:)   = real(CONT_ELEC%delta_ac(:))

                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'im_qfn'
                        acinqu_vals(i_col,:)   = imag(CONT_ELEC%delta_ac(:))
                    endif

                    !irgendwie kommt nur Mist raus für die Einträge hier
                    if (DD_ELEC2) then
                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'dum'
                        acinqu_vals(i_col,:)   = real(CONT_ELEC2%delta_ac(:))

                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'dum2'
                        acinqu_vals(i_col,:)   = imag(CONT_ELEC2%delta_ac(:))
                    endif
                    if (DD_ELEC2) then
                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 're_qfn2'
                        acinqu_vals(i_col,:)   = real(CONT_ELEC2%delta_ac(:))

                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'im_qfn2'
                        acinqu_vals(i_col,:)   = imag(CONT_ELEC2%delta_ac(:))
                    endif

                    if (DD_HOLE) then
                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 're_qfp'
                        acinqu_vals(i_col,:)   = real(CONT_HOLE%delta_ac(:))

                        i_col                  = i_col +1
                        acinqu_cols(i_col)%str = 'im_qfp'
                        acinqu_vals(i_col,:)   = imag(CONT_HOLE%delta_ac(:))
                    endif

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 're_psi'
                    do i_p=1,N_SP
                        acinqu_vals(i_col,i_p)   = real(CONT_POI%delta_ac(SP(i_p)%id_p))
                    enddo

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'im_psi'
                    do i_p=1,N_SP
                        acinqu_vals(i,i_p)   = imag(CONT_POI%delta_ac(SP(i_p)%id_p))
                    enddo

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 're_curr_x'
                    acinqu_vals(i_col,:)   = real(curr_x(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'im_curr_x'
                    acinqu_vals(i_col,:)   = imag(curr_x(:))

                    ! i_col                  = i_col +1
                    ! acinqu_cols(i_col)%str = 're_curr_y'
                    ! acinqu_vals(i_col,:)   = real(curr_y(:))

                    ! i_col                  = i_col +1
                    ! acinqu_cols(i_col)%str = 'im_curr_y'
                    ! acinqu_vals(i_col,:)   = imag(curr_y(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 're_d_n_x'
                    acinqu_vals(i_col,:)   = real(dens_n(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'im_d_n_x'
                    acinqu_vals(i_col,:)   = imag(dens_n(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 're_d_n2_x'
                    acinqu_vals(i_col,:)   = real(dens_n2(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'im_d_n2_x'
                    acinqu_vals(i_col,:)   = imag(dens_n2(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 're_d_p_x'
                    acinqu_vals(i_col,:)   = real(dens_p(:))

                    i_col                  = i_col +1
                    acinqu_cols(i_col)%str = 'im_d_p_x'
                    acinqu_vals(i_col,:)   = imag(dens_p(:))


                    call value_to_string(OP_REC,str_op_rec)
                    call value_to_string(n,str_freq)
                    call save_hdf5('acinqu', 'op'//trim(str_op_rec)//'_dV'//CON(p)%name, acinqu_cols, acinqu_vals, (OP_REC.eq.1).and.(p.eq.1).and.(n.eq.1))
                    if (US%output%elpa.ge.1) then
                        call save_elpa('_acinqu_'//'op'//trim(str_op_rec)//'_dV'//CON(p)%name, acinqu_cols, acinqu_vals, .True.)
                    endif

                    deallocate(dens_n, dens_n2,dens_p,curr_x,curr_y,curr_tun_x)
                endif
            endif
        enddo

        ! store AC terminal characteristics
        i_col                = 0
        i_col                = i_col +1
        aciv_vals(i_col,n)   = FREQ(n)
        do p=1,N_CON-1
            do pp=1,N_CON-1
                i_col = i_col + 1
                aciv_vals(i_col,n)   = real(y_para(pp,p)*NORM%J_N)
                i_col = i_col + 1
                aciv_vals(i_col,n)   = imag(y_para(pp,p)*NORM%J_N)
            enddo
        enddo

    enddo

    deallocate(y_para)

    call value_to_string(OP_REC,str_op_rec)
    call save_hdf5('ac', 'op'//trim(str_op_rec), aciv_cols, aciv_vals, (OP_REC.eq.1))
    if (US%output%elpa.ge.1) then
        call save_elpa('_ac', aciv_cols, aciv_vals, .True.)
    endif

    call dealloc_crs(mat_1)
    call dealloc_crs(DD_AC_CCS)
endsubroutine


subroutine dd_solve_tr
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! transient solution of equation system
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: qsum,lambda,damp_max,dpcc_norm,dpcc_max,rpcc_norm,pdifftol
    real(8) :: phi,dpcc_max_last,mu,lambda_last,dpcc_norm_last
    real(8) :: lambda_new,lambda_psi,lambda_0
    integer :: simul_start,i,niter,j,numb_diver,r_n
    logical :: simul,converged=.false.


    if (.not.TR_ON) return

    write(*,*)
    write(*,'(A)') '----> TRANSIENT DD'
    write(*,*)
    write(*,'(A)') '  t(s)  |  is(A)  |  id(A)  |  ig(A)  |  Q(C)   | niter | d_rhs  | d_poi  '
    write(*,'(A)') '--------+---------+---------+---------+---------+-------+--------|--------'

    TR_IT         = 0
    pdifftol      = US%dd%pdiff_tol
    lambda        = 1
    damp_max      = 1
    niter         = US%dd%n_iter
    simul_start   = US%dd%simul
    if (OP.lt.US%dd%op_simul_start) simul_start = 0 ! OP is actual operation point
    dpcc_norm_last = 0

    ! --> calculate electron density/current
    call set_density_and_current
    if (DD_DISPL) then
        call calc_diff_charge_cont(Q_CONT,0)
    else
        Q_CONT = 0
    endif
    Q_CONT_LAST = Q_CONT

    ! --> save transient t=0
    call save_dd('tr')

    do TR_IT=1,TR_NT
        CN_LAST         = CN
        CP_LAST         = CP
        DD_CONV    = .false.
        numb_diver = 0
        lambda     = 1
        simul      = .false.
        Q_CONT_LAST=Q_CONT
        call save_dd('newton_tr',0 )
        do i=1,niter
            if (i.eq.simul_start) then
                if (CONT_POI%active) then
                    !init simul hier
                    call init_dd_simul ! DD_SIMUL_INIT=.true. in calc_simul
                    simul=.true.
                endif
            endif

            ! --> calc dpsi, dqfn
            if (simul) then
                call solve_dd_simul(0,converged)
            else
                call solve_dd_succ(lambda,0) 
            endif

            ! dpcc_ok      = dpcc_norm.le.ptol
            ! rpcc_ok      = rpcc_norm.le.ctol
            ! dpcc_diff_ok = (dpcc_diff.lt.pdifftol).and.(i.gt.10)
            ! dpcc_diff_ok = .false. !debug
            ! converged    = (dpcc_ok.and.rpcc_ok).or.dpcc_diff_ok

            if (converged.or.(i.eq.niter)) then
                call set_density_and_current
                if (DD_DISPL) call calc_diff_charge_cont(Q_CONT,i)
                qsum=get_qsemi()
                call get_dc_current
                ! solution found
                DD_CONV = .true.

                write(6,'(A,$)') char(13)
                call write_vec(TR_TOT(TR_IT),.true.,' | ')
                call write_vec(CON(SOURCE)%current,.true.,' | ')
                call write_vec(CON(DRAIN )%current,.true.,' | ')
                if ((GATE.lt.1).or.(GATE.gt.N_CON)) then
                    call write_vec(dble(0),.true.,' | ')
                else
                    call write_vec(CON(GATE  )%current,.true.,' | ')
                endif
                call write_vec(qsum,.true.,' | ')
                write(6,'(I5,A$)') i,' | ' ! 8
                call write_vec(rpcc_norm,.false.,' | ')
                call write_vec(dpcc_norm,.false.,' ')
                write(6,*)

                exit
            elseif (i.gt.1) then
                ! find lambda
                phi  = dpcc_max/dpcc_max_last
                mu   = 0
                do j=1,CONT_POI%jacobi(psi_)%nr
                    if (IS_SEMI(j)) then
                        mu = mu + (DPCC(j) - (1-lambda_last)*DPCC_LAST(j))**2
                    endif
                enddo
                do j=CONT_POI%jacobi(psi_)%nr+1,N_PCC
                    mu = mu + (DPCC(j) - (1-lambda_last)*DPCC_LAST(j))**2
                enddo
                mu   = sqrt(mu)
                mu   = 0.5*dpcc_norm_last/mu*lambda*lambda

                if (phi.gt.5) then
                    numb_diver = numb_diver + 1
                    if (numb_diver.gt.200) then
                        ! error
                        write(*,'(A)') 'error numb diver'
                        exit
                    endif
                endif

                if (phi.ge.(dble(1)-0.25*lambda)) then
                    lambda = min(mu,0.5*lambda)

                else
                    numb_diver = max(0,numb_diver-1)
                    lambda_new   = min(damp_max,mu)
                    if (lambda_new.ge.(dble(4)*lambda)) then ! todo: check
                        lambda     = lambda_new
                    endif
                endif

                if (lambda.lt.US%dd%damp_min) lambda = US%dd%damp_min
            endif

            ! -->  new QFN/QFP/QFT
            if (simul) then
                ! -->  new CONT_POI%var
                lambda_psi = min(lambda,0.05/maxval(abs(DPCC(1:NXYZ))))
                do j=1,NXYZ
                    CONT_POI%var(j) = CONT_POI%var(j) + lambda_psi*DPCC(j)
                enddo
                call set_psi_semi

                r_n = NXYZ
                if (DD_ELEC) then
                    lambda_0 = min(lambda,0.05/maxval(abs(DPCC(r_n+1:r_n+N_SP))))
                    do j=1,N_SP
                        CONT_ELEC%var(j) = CONT_ELEC%var(j) + lambda_0*DPCC(r_n+j)
                    enddo
                    r_n     = r_n + N_SP
                endif
                if (DD_HOLE) then
                    lambda_0 = min(lambda,0.05/maxval(abs(DPCC(r_n+1:r_n+N_SP))))
                    do j=1,N_SP
                        CONT_HOLE%var(j) = CONT_HOLE%var(j) + lambda_0*DPCC(r_n+j)
                    enddo
                    r_n     = r_n + N_SP
                endif
            else
              ! already calculated for successive solution

            endif

            ! --> calculate electron density
            call set_density_and_current
            if (DD_DISPL) call calc_diff_charge_cont(Q_CONT,i)
            qsum=get_qsemi()
            call get_dc_current
            
            ! --> save newton iteration
            call save_dd('newton_tr',i)

            !damping currently not implemented in TR solver
            ! --> save DU
            dpcc_max_last  = dpcc_max
            dpcc_norm_last = dpcc_norm
            do j=1,size(DPCC,1)
              DPCC_LAST(j) = DPCC(j)
            enddo

            write(6,'(A,$)') char(13)
            call write_vec(TR_TOT(TR_IT),.true.,' | ')
            call write_vec(CON(SOURCE)%current,.true.,' | ')
            call write_vec(CON(DRAIN )%current,.true.,' | ')
            if ((GATE.lt.1).or.(GATE.gt.N_CON)) then
                call write_vec(dble(0),.true.,' | ')
            else
                call write_vec(CON(GATE  )%current,.true.,' | ')
            endif
            call write_vec(qsum,.true.,' | ')
            write(6,'(I5,A$)') i,' | ' ! 8
            call write_vec(rpcc_norm,.false.,' | ')
            call write_vec(dpcc_norm,.false.,' ')

            if (i.eq.niter) then
                TR_NT = TR_IT
            endif
        enddo ! iteration
        
        ! --> save transient
        call save_dd('tr')

        if (.not.DD_CONV) exit

    enddo ! transient

    TR_IT=0
endsubroutine

subroutine solve_simul_umfpack() 
        real(8), dimension(20) :: control
        real(8), dimension(90) :: info
        integer :: sys
        integer :: n 

        !convert crs to ccs (but matrix is already allocated!)
        call convert_crs_ccs(DD_SIMUL,DD_SIMUL_CCS)
        n            = DD_SIMUL%nc

        ! 0 based indexing
        DD_SIMUL_CCS%row_index = DD_SIMUL_CCS%row_index - 1
        DD_SIMUL_CCS%columns   = DD_SIMUL_CCS%columns   - 1

        ! set default control parameters
        call umf4def (control)

        !control(UMFPACK_PRL) = 0 !do not print parameters
        !https://github.com/menpo/conda-suitesparse/blob/master/SuiteSparse/UMFPACK/Include/umfpack_numeric.h
        control(UMFPACK_DENSE_ROW) = 0.2
        control(UMFPACK_SCALE) = UMFPACK_SCALE_SUM !scale rows max
        call umf4pcon (control)

!       pre-order and DD_SIMUL_SYMBOLIC analysis (May be re-used if this is slow)
        call umf4sym (n, n, DD_SIMUL_CCS%columns, DD_SIMUL_CCS%row_index, DD_SIMUL_CCS%values, DD_SIMUL_SYMBOLIC, control, info)

!       check umf4sym error condition
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4sym: ', info (1)
            stop
        endif

        !DD_SIMUL_NUMERIC factorization
        call umf4num (DD_SIMUL_CCS%columns, DD_SIMUL_CCS%row_index, DD_SIMUL_CCS%values, DD_SIMUL_SYMBOLIC, DD_SIMUL_NUMERIC, control, info)

        !check umf4num error condition
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4num: ', info (1)
            stop
        endif

        !solve Ax=b, without iterative refinement
        sys = 0
        call umf4sol (sys, DPCC, DD_SIMUL_RHS, DD_SIMUL_NUMERIC, control, info)
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4sol: ', info (1)
            stop
        endif

        ! free the DD_SIMUL_SYMBOLIC analysis
        call umf4fsym (DD_SIMUL_SYMBOLIC)

        ! free the DD_SIMUL_NUMERIC factorization
        call umf4fnum (DD_SIMUL_NUMERIC)

endsubroutine

subroutine solve_ac_umfpack() 
        real(8), dimension(20) :: control
        real(8), dimension(90) :: info
        integer :: sys
        integer :: n 

        !convert crs to ccs (but matrix is already allocated!)
        call convert_crs_ccs_complex(DD_AC,DD_AC_CCS)
        n            = DD_AC%nc

        ! 0 based indexing
        DD_AC_CCS%row_index = DD_AC_CCS%row_index - 1
        DD_AC_CCS%columns   = DD_AC_CCS%columns   - 1

!       set default parameters
        call umf4cdef (control)

!       print control parameters.  set control (1) to 1 to print error messages only
        control(1) = 2
        ! call umf4pcon (control)

!       pre-order and DD_SIMUL_SYMBOLIC analysis (May be re-used if this is slow)
        call umf4csym (n, n, DD_AC_CCS%columns, DD_AC_CCS%row_index, DD_AC_CCS%values, DD_AC_SYMBOLIC, control, info)

!       check umf4sym error condition
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4csym: ', info (1)
            stop
        endif

        !DD_SIMUL_NUMERIC factorization
        call umf4cnum (DD_AC_CCS%columns, DD_AC_CCS%row_index, DD_AC_CCS%values, DD_AC_SYMBOLIC, DD_AC_NUMERIC, control, info)

        !check umf4num error condition
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4cnum: ', info (1)
            stop
        endif

        !solve Ax=b, without iterative refinement
        sys = 0
        call umf4csol (sys, DELTA_AC, DD_AC_RHS, DD_AC_NUMERIC, control, info)
        if (info (1) .lt. 0) then
            print *, 'Error occurred in umf4sol: ', info (1)
            stop
        endif

        ! free the DD_SIMUL_SYMBOLIC analysis
        call umf4fsym (DD_AC_SYMBOLIC)

        ! free the DD_SIMUL_NUMERIC factorization
        call umf4fnum (DD_AC_NUMERIC)

endsubroutine





! ---------------------------------------------------------------------------------------------------------------------
!> \brief simultaneous DD simulation
!> \details
subroutine solve_dd_simul(niter,converged)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! simultaneous solution of equation system
    !
    ! input
    ! -----
    ! niter : integer
    !   Current iteration number.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer              :: i,j,n,niter
    type(crs_mat_real)   :: mat_4
    logical              :: changed_dss,converged
    ! TYPE(MKL_DSS_HANDLE) :: handle1
    ! the number below needs to be equal to the number of possible solution variables nvar-1
    type(crs_mat_real_pointer),dimension(7,7) :: jacobi_mats !this matrix will hold all jabobians
    type(crs_mat_real_pointer),dimension(7)   :: conts_cleaned !with zeros removed
    
    if (CRYO.and.EQUILIBRIUM) then
        ! in equilbrium we can get away with just solving POISSON
        call CONT_POI%solve(dble(1.0))
        return
    endif

    ! set all continuity equation
    do i=1,nvar-1
        call CONTS(i)%p%set(.true.)
    enddo

    ! print debug information
    if (US%dd%debug.gt.0) then
        if (niter.ge.US%dd%debug) then
            !print jacobians
            do j=1,size(CONTS)
                call CONTS(j)%p%print_debug(niter)
            enddo
            !print internal quantities of interest
            call print_dd_debug(niter)
        endif
    endif

    ! simple damping algorithm that can help when poisson equation is tightly coupled to electron cont.
    if (US%dd%damp.gt.0) then
        fac_damp = 0.05*(niter-1)
        if (fac_damp.ge.1) fac_damp = 1
        if (converged) fac_damp = 1
        if (CONTS(psi_)%p%active.and.CONTS(phin_)%p%active) CONTS(psi_)%p%jacobi(phin_)%values = CONTS(psi_)%p%jacobi(phin_)%values*fac_damp
    endif

    ! put all jacobians into one big jacobian
    do i=1,nvar-1
        do n=1,nvar-1
            if (CONTS(n)%p%active.and.CONTS(i)%p%active) then
                jacobi_mats(i,n)%p => CONTS(n)%p%jacobi(i)
            endif
        enddo
    enddo

    ! here we might call a debug routine that prints the jacobians to files. not yet implemented

    !remove zeros with derivatives after psi
    if (DD_REMOVE_ZEROS) then
        do i=1,nvar-1
            if (CONTS(i)%p%active) then
                !if it is poisson, nothing required
                if (CONTS(i)%p%i_var.eq.psi_) cycle

                !else remove the zeros
                call remove_zeros_crs(CONTS(i)%p%jacobi(psi_),conts_cleaned(i)%p)
                jacobi_mats(psi_,i)%p => conts_cleaned(i)%p
            endif
        enddo
    endif

    mat_4 = join_mats(jacobi_mats)

    ! set rhs
    n = 1
    do i=1,nvar-1
        if (CONTS(i)%p%active) then
            DD_SIMUL_RHS(n:n+size(CONTS(i)%p%rhs)-1) = CONTS(i)%p%rhs
            n = n + size(CONTS(i)%p%rhs)
        endif
    enddo
    
    changed_dss = .false.
    if (DD_REMOVE_ZEROS.and.DD_SIMUL_INIT) then
        if (size(mat_4%columns).ne.size(DD_SIMUL%columns)) then
            changed_dss = .true.
      
        elseif (size(mat_4%row_index).ne.size(DD_SIMUL%row_index)) then
            changed_dss = .true.
      
        else
            do i=1,size(DD_SIMUL%row_index)
                if (DD_SIMUL%row_index(i).ne.mat_4%row_index(i)) then
                    changed_dss=.true.
                    exit
                endif
            enddo
        
            do i=1,size(DD_SIMUL%columns)
                if (DD_SIMUL%columns(i).ne.mat_4%columns(i)) then
                    changed_dss=.true.
                    exit
                endif
            enddo
        endif
    endif
    
    ! ----------------------------------------
    if (.not.DD_SIMUL_CCS%active) then
        call copy_crs(mat_4, DD_SIMUL)
        ! call init_dss(DD_SIMUL, DD_SIMUL_HANDLE)
        DD_SIMUL_INIT = .true.

        call DD_SIMUL_CCS%init(nr=DD_SIMUL%nz , nc=DD_SIMUL%nc, nz=DD_SIMUL%nz, is_ccs=.true.)
    
    elseif (changed_dss) then
        ! error   = DSS_DELETE(DD_SIMUL_HANDLE,MKL_DSS_DEFAULTS)
        call dealloc_crs(DD_SIMUL)
        call dealloc_crs(DD_SIMUL_CCS)
        call copy_crs(mat_4, DD_SIMUL)
        ! call init_dss(DD_SIMUL, DD_SIMUL_HANDLE)
        call DD_SIMUL_CCS%init(nr=DD_SIMUL%nz , nc=DD_SIMUL%nc, nz=DD_SIMUL%nz, is_ccs=.true.)
      
    endif
    
    DD_SIMUL%values = mat_4%values
    call dealloc_crs(mat_4)

    ! handle1 = DD_SIMUL_HANDLE
    ! call set_values_dss(DD_SIMUL, handle1)
    ! error   = DSS_SOLVE_REAL_D( handle1, MKL_DSS_DEFAULTS, DD_SIMUL_RHS, 1, DPCC )

    call solve_simul_umfpack

    ! --------------> set DELTA of continuity equations -----------------
    n = 1
    do i=1,nvar-1
        if (CONTS(i)%p%active) then
            CONTS(i)%p%delta = DPCC(n:n+size(CONTS(i)%p%rhs)-1)
            n = n + size(CONTS(i)%p%delta)
        endif
    enddo

endsubroutine
  
subroutine solve_dd_succ(lambda,niter)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! successive solution of equation system
    !
    ! input
    ! -----
    ! lambda : real
    !   The damping factor for the solution.
    ! niter : integer
    !   The current iteration number.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in)   :: lambda !< damping factor
    integer,intent(in)   :: niter  !< iteration number
    integer              :: i,n,dum
    logical              :: print_debug = .false.
    
    print_debug = (US%dd%debug.gt.0).and.(niter.ge.US%dd%debug)

    if (.not.EQUILIBRIUM) then
        call CONT_ELEC%solve(lambda)
        if (print_debug) call CONT_ELEC%print_debug(niter)
        call CONT_ELEC2%solve(lambda)
        if (print_debug) call CONT_ELEC2%print_debug(niter)
        call CONT_HOLE%solve(lambda)
        if (print_debug) call CONT_HOLE%print_debug(niter)
        call CONT_BOHM_N%solve(lambda)
        if (print_debug) call CONT_BOHM_N%print_debug(niter)
        call CONT_TN%solve(lambda)
        if (print_debug) call CONT_TN%print_debug(niter)
    endif

    call set_density

    call CONT_POI%solve(lambda)
    if (print_debug) call CONT_POI%print_debug(niter)

    call set_psi_semi

    call CONT_TL%solve(lambda)

    call update_temp

    if (print_debug) call print_dd_debug(niter)

    ! --------------------------- set delta vector --------------------------------------
    ! --> maybe remove this. Why is this needed here?
    n = 1
    do i=1,nvar-1
        if (.not.CONTS(i)%p%active) cycle
        dum = size(CONTS(i)%p%delta)
        DPCC(n:n+size(CONTS(i)%p%delta)-1)       = CONTS(i)%p%delta
        DD_SIMUL_RHS(n:n+size(CONTS(i)%p%rhs)-1) = CONTS(i)%p%rhs
        n = n + size(CONTS(i)%p%delta)
    enddo

endsubroutine


subroutine set_density_and_current
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all quantities that depend on the solution variables.
    ! name of the routine should be changed.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call set_density
    call set_current
    call set_recombination
    call set_tunnel_rate

endsubroutine

subroutine set_tunnel_rate
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculation of the cb->cb tunnel rate, details see dissertation of M.Mueller
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !cb -> cb tunneling with generation rate by markus
    real(8),dimension(N_SP) :: ec_1d !1d conduction band
    real(8),dimension(N_SP) :: kappa !1d conduction band
    real(8),dimension(N_SP) :: dt_dpsi !1d conduction band
    integer                 :: n,is,ib,i
    integer                 :: index_l=0, index_r=0
    real(8)                 :: e, ec_r, ec_l, ec_p, ec_m, ec_n, x_l, x_r, tunnel_integral, t, wb, f, ef_r, ef_l, supply, c
    TYPE(DUAL_NUM)          :: result
    real(8)                 :: dsupply_dpsi_l, dsupply_dpsi_r
    real(8)                 :: dsupply_dphi_l, dsupply_dphi_r
    real(8)                 :: df_dpsi_n, df_dpsi_p
    real(8)                 :: dt_dkappa_i, dkappa_dpsi_i
    logical                 :: to_left = .false., to_right = .false.
    integer                 :: index

    !for debugging
    character(len=200)                 :: name
    type(string)                       :: tunnel_cols(100)     !columns of IV file
    real(8),dimension(:,:),allocatable :: tunnel_vals      ! values of IV file

    if (.not.DD_TUNNEL) return
    c      = AM0*BK*TEMP_LATTICE/(2*PI*HBAR**3)*Q*US%tunnel%factor*US%tunnel%m

    ! zero all gtun elements here
    DN(:)%g_tun = 0 !tunneling generation rate
    do n=1,N_SP
        DN(n)%g_tun_dqf(:) = 0 !tunneling generation rate at point n derivative to qf at (j)
        DN(n)%g_tun_dpsi(:) = 0 !tunneling generation rate at point n derivative to psi at (j)
    enddo

    if (US%dd%debug.ge.1) then
        !allocate debug output
        if (.not.allocated(tunnel_vals)) allocate(tunnel_vals(10,N_SP))
        tunnel_vals = 0
        tunnel_cols(1)%str = 'i_l'   !left index of point
        tunnel_cols(2)%str = 'i_r'   !right index of point
        tunnel_cols(3)%str = 'ec_l'  !left conduction band value
        tunnel_cols(4)%str = 'ec_r'  !right conduction band value
        tunnel_cols(5)%str = 'x_l'   ! left x location
        tunnel_cols(6)%str = 'x_r'   !right x location
        tunnel_cols(7)%str = 't'     !transmission probability
        tunnel_cols(8)%str = 'f_l'   !electric field left
        tunnel_cols(9)%str = 'f_r'   !electric field right
        tunnel_cols(10)%str = 'gtun' !tunneling generation rate
    endif

    if (SEMI_DIM.ge.2) then
        write(*,*) '***error*** Tunneling only possible with spatially 1D semiconductor domain.'
        stop
    endif

    !get the 1st conduction band
    do n=1,N_SP
        is       = SP(n)%model_id
        ib       = CONT_ELEC%valley
        ec_1d(n) = SEMI(is)%band_pointer(ib)%band%e0  - PSI_SEMI(n) - UN(n)
    enddo

    !iterate over all points except first and last point
    do n=2,N_SP-1
        ec_m = ec_1d(n-1) !Ec at point to the left of n
        ec_n = ec_1d(n)   !Ec at point n
        ec_p = ec_1d(n+1) !Ec point    to the right of n

        x_l      = 0
        x_r      = 0
        index_l  = 0
        index_r  = 0
        to_left  = .false.
        to_right = .false.

        ! now, depending on the Condution band at point n, init everything for tunneling calculation
        if (ec_p.gt.ec_n) then !tunneling to the right if Ec(n+1) to the right larger than Ec(n)
            to_right = .true.
            index_l  = n
            ec_l     = ec_n
            x_l      = X(n)
            index_r  = 0
            !find end of barrier
            do i=n+1,N_SP-1
                if (ec_1d(i).le.ec_l) then
                    index_r = i
                    exit
                endif
            enddo
            if (index_r.eq.0) then
                cycle
            endif
            ec_r    = ec_1d(index_r)
            x_r     = X(index_r)

        elseif (ec_m.gt.ec_n) then !tunnel to the left
            to_left = .true.
            index_r = n
            ec_r    = ec_n
            x_r     = X(n)
            index_l = 0
            !find end of barrier
            do i=n-1,1,-1
                if (ec_1d(i).le.ec_r) then
                    index_l = i
                    exit
                endif
            enddo
            if (index_l.eq.0) then
                cycle
            endif
            ec_l  = ec_1d(index_l)
            x_l   = X(index_l)

        else ! no tunneling here
            cycle

        endif

        if ((x_l.eq.x_r).or.(index_l.eq.index_r)) then
            write(*,*) '***error*** Tunneling X makes no sense. This is probably a bug.'
            stop
        endif

        wb = x_r-x_l
        if (wb.ge.US%tunnel%max_width) cycle

        if (to_right) e = ec_l
        if (to_left)  e = ec_r

        !now we calculate t_wkb and its derivatives using trapezoidal integration
        kappa                  = 0
        dt_dpsi                = 0
        kappa(index_l:index_r) = sqrt(2*AM0*US%tunnel%m/HBAR/HBAR*(abs(ec_1d(index_l:index_r)-e))*Q)
        tunnel_integral        = integrate(X,kappa)*NORM%l_N!coordinate system normalization
        t                      = exp(-2*tunnel_integral)
        do i=index_l+1,index_r-1 
            dt_dkappa_i   = t*(-2)*dintegrate_dk(X,kappa,i) !dtunnel_integral_dkappa_i
            dkappa_dpsi_i = 1./(2.*kappa(i))*2*AM0*US%tunnel%m/HBAR/HBAR*Q !derivative Ec (here the derivative to E might be missing)
            dt_dpsi(i)    = dt_dkappa_i*dkappa_dpsi_i
        enddo
        dt_dpsi = dt_dpsi*NORM%l_N !coordinate system normalization

        !finally the field .. maybe take three points into account
        f         = (ec_p-ec_n)/DX(n)
        df_dpsi_n =  1/DX(n)
        df_dpsi_p = -1/DX(n)

        !denormalize the field
        f = f * NORM%l_N_inv

        !the supply function = f(Efl, Efr, E)
        ef_l           = -CONT_ELEC%var(index_l)
        ef_r           = -CONT_ELEC%var(index_r)
        result         = supply_fermi_dirac(DUAL_NUM(e,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(ef_l,(/0,1,0,0,0,0,0,0/)),DUAL_NUM(ef_r,(/0,0,1,0,0,0,0,0/)),VT_LATTICE, TEMP_LATTICE)
        supply         = result%x_ad_
        ! supply         = 1
        dsupply_dpsi_l = 0
        dsupply_dpsi_r = 0
        if (to_right) dsupply_dpsi_l = -result%xp_ad_(1) !ec_l propto -psi
        if (to_left)  dsupply_dpsi_r = -result%xp_ad_(1) !ec_l propto -psi
        dsupply_dphi_l = -result%xp_ad_(2) !ec_l propto -psi
        dsupply_dphi_r = -result%xp_ad_(3) !ec_l propto -psi

        !and finally the tunnel generation rate
        if (to_right) then
            DN(index_l)%g_tun = c*f*t*supply
            !derivatives with respect to phi
            DN(index_l)%g_tun_dqf(index_l) = c*f*t*dsupply_dphi_l
            DN(index_l)%g_tun_dqf(index_r) = c*f*t*dsupply_dphi_r

        else
            DN(index_r)%g_tun = c*f*t*supply
            !derivatives with respect to phi
            DN(index_r)%g_tun_dqf(index_l) = c*f*t*dsupply_dphi_l
            DN(index_r)%g_tun_dqf(index_r) = c*f*t*dsupply_dphi_r

        endif

        !derivatives with rescept to psi
        if (to_right) then
            do i=index_l,index_r
                DN(index_l)%g_tun_dpsi(i) = c*f*supply*dt_dpsi(i)
            enddo
            DN(index_l)%g_tun_dpsi(index_l) = DN(index_l)%g_tun_dpsi(index_l) - c*df_dpsi_n*supply*t
        else
            do i=index_l,index_r
                DN(index_r)%g_tun_dpsi(i) = c*f*supply*dt_dpsi(i)
            enddo
            DN(index_r)%g_tun_dpsi(index_r) = DN(index_r)%g_tun_dpsi(index_r) - c*df_dpsi_n*supply*t
        endif

        if (US%dd%debug.ge.1) then
            if (to_right) index   = index_l
            if (to_left)  index   = index_r
            tunnel_vals(1,index)  = index_l
            tunnel_vals(2,index)  = index_r
            tunnel_vals(3,index)  = ec_l
            tunnel_vals(4,index)  = ec_r
            tunnel_vals(5,index)  = x_l
            tunnel_vals(6,index)  = x_r
            tunnel_vals(7,index)  = t
            tunnel_vals(8,index)  = f
            tunnel_vals(9,index)  = f
            tunnel_vals(10,index) = DN(index)%g_tun
        endif

    enddo

    ! for debugging:
    ! zero all gtun elements here
    ! DN(:)%g_tun = 0
    ! do n=1,N_SP
    !   DN(n)%g_tun_dpsi(:) = 0
    !   DN(n)%g_tun_dqf(:)   = 0
    ! enddo

    ! DN(:)%g_tun = DN(:)%g_tun*Q
    ! do n=1,N_SP
    !   DN(n)%g_tun_dpsi(:) = DN(n)%g_tun_dpsi(:)*Q
    !   DN(n)%g_tun_dqf(:)  = DN(n)%g_tun_dqf(:)*Q
    ! enddo

    if (US%dd%debug.ge.1) then !will overwrite every time it is called.
        name               = 'dd_tunnel_internals'
        name = trim(name)//'_debug.elpa'
        !should be converted to save_hdf
        call save_elpa(name, tunnel_cols, tunnel_vals)
    endif

endsubroutine

subroutine set_density
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the carrier density within the semiconductor
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,is,ib,hetero_index
    real(8) :: deltaef,vt_i
    TYPE(DUAL_NUM)::result

    hetero_index=0

    !call omp_set_num_threads(4)

    !!$OMP PARALLEL SECTIONS PRIVATE(n,is,ib,result,deltaef)

    !!$OMP SECTION
    ! threads = omp_get_num_threads()
    ! id = omp_get_thread_num()
    ! print *, "hello from thread", id, "out of", threads

    ! --> electron density&derivative at grid points
    if (DD_ELEC) then
        if (.not.(DD_TL.or.DD_TN)) then
            do n=1,N_SP
                if (DD_CONT_BOHM_N) then
                    deltaef          = (-CONT_ELEC%var(n))                      - (-PSI_SEMI(n) - UN(n) - CONT_BOHM_N%var(n)) ! E_f-E_fi
                    !entkoppelt debug
                    !deltaef          = (-CONT_ELEC%var(n))                      - (-PSI_SEMI(n) - UN(n) ) ! E_f-E_fi
                else
                    deltaef          = (-CONT_ELEC%var(n))                      - (-PSI_SEMI(n) - UN(n)) ! E_f-E_fi
                endif
                is               = SP(n)%model_id
                ib               = CONT_ELEC%valley
                result         = get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(VT_LATTICE,(/0,0,0,0,0,0,0,0/)),n)
                CN(n)          = result%x_ad_
                DN(n)%dens_dqf =-result%xp_ad_(1) !minus since energy and potential have oppositve sign
            enddo
        else
            do n=1,N_SP
                deltaef          = (-CONT_ELEC%var(n)) - (-PSI_SEMI(n) - UN(n)) ! E_f-E_fi
                is               = SP(n)%model_id
                ib               = CONT_ELEC%valley
                if (DD_TL) then
                    vt_i             = VT_LATTICE
                else
                    !vt_i             = CONT_TN%var(n)*BK/Q
                    vt_i             = VT_LATTICE
                endif
                result           = get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(vt_i,(/0,1,0,0,0,0,0,0/)),n )
                CN(n)            = result%x_ad_
                DN(n)%dens_dqf   =-result%xp_ad_(1) !minus since energy and potential have oppositve sign
                !DN(n)%dens_dtl   = result%xp_ad_(2)*BK/Q !derivative with vt
                DN(n)%dens_dtl   = 0 !
            enddo
        endif
    endif

    ! --> electron 2 density&derivative at grid points
    if (DD_ELEC2) then
        if (.not.DD_TL) then
            do n=1,N_SP
                deltaef           = (-CONT_ELEC2%var(n)) - (-PSI_SEMI(n) - UN2(n)) ! E_f-E_fi
                is                = SP(n)%model_id
                ib                = CONT_ELEC2%valley
                result            = get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(VT_LATTICE,(/0,0,0,0,0,0,0,0/)),n)
                CN2(n)            = result%x_ad_
                DN2(n)%dens_dqf   =-result%xp_ad_(1) !minus since energy and potential have oppositve sign

            enddo
        else
            do n=1,N_SP
                deltaef           = (-CONT_ELEC2%var(n)) - (-PSI_SEMI(n) - UN2(n)) ! E_f-E_fi
                is                = SP(n)%model_id
                ib                = CONT_ELEC2%valley
                result            = get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(VT_LATTICE,(/0,1,0,0,0,0,0,0/)),n)
                CN2(n)            = result%x_ad_
                DN2(n)%dens_dqf   =-result%xp_ad_(1) !minus since energy and potential have oppositve sign
                DN2(n)%dens_dtl   = result%xp_ad_(2) !derivative with vt

            enddo
        endif
    endif

    !!$OMP SECTION
    ! --> hole density&derivative at grid points
    ! threads = omp_get_num_threads()
    ! id = omp_get_thread_num()
    ! print *, "hello from thread", id, "out of", threads
    if (DD_HOLE) then
        if (.not.DD_TL) then
            do n=1,N_SP
                deltaef        =  (-CONT_HOLE%var(n)) - (-PSI_SEMI(n) - UP(n)) ! E_f-E_fi
                is             =  SP(n)%model_id
                ib             =  CONT_HOLE%valley
                result         =  get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(VT_LATTICE,(/0,0,0,0,0,0,0,0/)),n) 
                CP(n)          =  result%x_ad_
                DP(n)%dens_dqf = -result%xp_ad_(1) !minus since energy and potential have opposite sign
            enddo
        else
            do n=1,N_SP
                deltaef        =  (-CONT_HOLE%var(n)) - (-PSI_SEMI(n) - UP(n)) ! E_f-E_fi
                is             =  SP(n)%model_id
                ib             =  CONT_HOLE%valley
                result         =  get_n_equ_band_dual(is,ib,DUAL_NUM(deltaef,(/1,0,0,0,0,0,0,0/)),DUAL_NUM(VT_LATTICE,(/0,1,0,0,0,0,0,0/)),n) 
                CP(n)          =  result%x_ad_
                DP(n)%dens_dqf = -result%xp_ad_(1) !minus since energy and potential have opposite sign
                DP(n)%dens_dtl =  result%xp_ad_(2) !derivative with vt
            enddo
        endif
    endif
    !!$OMP END PARALLEL SECTIONS

    !convert derivative with vt to t
    if (DD_TL.or.DD_TN) then
        if (DD_ELEC)  DN(:)%dens_dtl  = DN(:)%dens_dtl*BK/Q
        if (DD_HOLE)  DP(:)%dens_dtl  = DP(:)%dens_dtl*BK/Q
        if (DD_ELEC2) DN2(:)%dens_dtl = DN2(:)%dens_dtl*BK/Q
    endif

endsubroutine


subroutine set_current
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all current densities within the semiconductor.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,p,is,ib
    real(8) :: dx,dy
    real(8) :: temp
    real(8) :: v_n,v_n2,v_p
    TYPE(DUAL_NUM) :: result
    real(8), dimension(2) :: dj_dphi, dj_dtl, dj_dn, dj_dpsi
    type(type_cont),pointer :: cont => null()

    if (DD_ELEC) then
    !!$OMP PARALLEL DO DEFAULT(PRIVATE), SHARED(VT_SEMI,DN,SP,CON,N_SP,PSI_SEMI,TEMP_SEMI,CN,CN0,SOURCE,DRAIN,SEMI,E0_ADD_SEMI,SNX1,US)
        do n=1,N_SP
            is      = SP(n)%model_id
            ib      = CONT_ELEC%valley
            temp    = TEMP_LATTICE
            if (SP(n)%cont) then
                cont => CON(SP(n)%cont_id)
                ! if (EQUILIBRIUM) then
                !     DN(n)%j_con      = 0
                !     DN(n)%j_con_dqf  = 0
                !     DN(n)%j_con_dpsi = 0

                ! else
                    v_n              =  cont%v_n
                    DN(n)%j_con      =  v_n*(CN(n)-CN0(n))
                    DN(n)%j_con_dqf  =  v_n*DN(n)%dens_dqf
                    DN(n)%j_con_dtl  =  v_n*DN(n)%dens_dtl
                    DN(n)%j_con_dpsi = -v_n*DN(n)%dens_dqf
                    if (EQUILIBRIUM) DN(n)%j_con = 0

                ! endif

            else
                DN(n)%j_con      = 0
                DN(n)%j_con_dqf  = 0
                DN(n)%j_con_dtl  = 0
                DN(n)%j_con_dpsi = 0
                
            endif

            if (SP(n)%x_upp_exist) then
                dx          = SP(n)%dx_upp
                p           = SP(n)%id_sx_upp

                if (.not.(DD_TL.or.DD_TN)) then
                    result      = get_dd_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), dx )
                    DN(n)%j_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)

                    DN(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                elseif (DD_TN) then
                    result           = get_dd_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(CONT_TN%var(n),(/0,0,0,0,0,0,1,0/)), DUAL_NUM(CONT_TN%var(p),(/0,0,0,0,0,0,0,1/)), dx )
                    DN(n)%j_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl

                    result      = get_energy_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(CONT_TN%var(n),(/0,0,0,0,0,0,1,0/)), DUAL_NUM(CONT_TN%var(p),(/0,0,0,0,0,0,0,1/)), dx , result)
                    DN(n)%s_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN(n)%s_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%s_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%s_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%s_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%s_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%s_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl

                else
                    result      = get_dd_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)), dx )
                    DN(n)%j_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl
                endif

                if (DD_CONT_BOHM_N) then
                    result       = get_dg_current_density(.true., ib, n, p, DUAL_NUM(CN(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CN(p),(/0,1,0,0,0,0,0,0/)), dx )
                    DN(n)%j_xhdg = result%x_ad_
                    dj_dn(1)     = result%xp_ad_(1)
                    dj_dn(2)     = result%xp_ad_(2)

                    DN(n)%j_xhdg_dqf(1)  = + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xhdg_dqf(2)  = + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_xhdg_dpsi(1) = - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_xhdg_dpsi(2) = - dj_dn(2)*DN(p)%dens_dqf

                endif

                ! !!!! test !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ! dqf         = (CONT_ELEC%var(p) - CONT_ELEC%var(n))/dx
                ! vth_mid     = (VT_LATTICE+VT_SEMI(p))*0.5
                ! temp_mid    = (TEMP_LATTICE+TEMP_LATTICE)*0.5

                ! mob         = get_mobility(is,ib,CONT_ELEC%var(n),CONT_ELEC%var(p),dx,1,temp_mid,CN(n),DONATORS(n),ACCEPTORS(n),GRADING(n))

                ! du          = log(CN(p)/CN(n)) + dx*dqf/vth_mid
                ! b_du_p      = bernoulli( du)
                ! b_du_m      = bernoulli(-du)
                ! db_du_p     = dbern( du)*CN(p)
                ! db_du_m     = dbern(-du)*CN(n)
                ! factor      = Q*vth_mid*mob/dx


                ! DN(n)%j_xh  = factor * (b_du_p*CN(p) - b_du_m*CN(n))
                ! ! call assert_isclose(j_xh, DN(n)%j_xh, dble(1e-3), dble(1), .false.)
                
                ! result       = get_mobility_dual(is,ib,DUAL_NUM(CONT_ELEC%var(n),0),DUAL_NUM(CONT_ELEC%var(p),0),dx,1,temp_mid, DUAL_NUM(CN(n),1), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dn      = result%xp_ad_(1)
                ! result       = get_mobility_dual(is,ib,DUAL_NUM(CONT_ELEC%var(n),1),DUAL_NUM(CONT_ELEC%var(p),0),dx,1,temp_mid, DUAL_NUM(CN(n),0), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dphi_l  = result%xp_ad_(1)
                ! result       = get_mobility_dual(is,ib,DUAL_NUM(CONT_ELEC%var(n),0),DUAL_NUM(CONT_ELEC%var(p),1),dx,1,temp_mid, DUAL_NUM(CN(n),0), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dphi_r  = result%xp_ad_(1)

                ! ! dmob_dphi_l  = (get_mobility(is,1,CONT_ELEC%var(n)+1e-6,CONT_ELEC%var(p),dx,1,temp_mid,CN(n),DONATORS(n),ACCEPTORS(n),GRADING(n)) - mob)*1e6
                ! ! dmob_dphi_r  = (get_mobility(is,1,CONT_ELEC%var(n),CONT_ELEC%var(p)+1e-6,dx,1,temp_mid,CN(n),DONATORS(n),ACCEPTORS(n),GRADING(n)) - mob)*1e6
                ! ! dmob_dn      = 0

                ! dmob_dqf(1)  = dmob_dn*DN(n)%dens_dqf + dmob_dphi_l
                ! dmob_dqf(2)  = dmob_dphi_r
                ! dmob_dpsi(1) = - dmob_dn*DN(n)%dens_dqf
                ! dmob_dpsi(2) = 0

                ! ! dmob_dqf(1)  = 0
                ! ! dmob_dqf(2)  = dmob_dphi_r
                ! ! dmob_dpsi(1) = 0
                ! ! dmob_dpsi(2) = 0

                ! ! d(i+1/2)/d(i)
                ! DN(n)%j_xh_dqf(1)    =  dmob_dqf(1)*DN(n)%j_xh/mob - factor*(b_du_m*DN(n)%dens_dqf + (DN(n)%dens_dqf/CN(n)+dble(1)/vth_mid)*(db_du_p + db_du_m))
                ! ! d(i+1/2)/d(i+1)
                ! DN(n)%j_xh_dqf(2)  =  dmob_dqf(2)*DN(n)%j_xh/mob + factor*(b_du_p*DN(p)%dens_dqf + (DN(p)%dens_dqf/CN(p)+dble(1)/vth_mid)*(db_du_p + db_du_m))
                ! ! call assert_isclose(j_xh_dqf_old(1), DN(n)%j_xh_dqf(1), dble(1e-3), dble(1), .false.)
                ! ! ! d(i+1/2)/d(i)
                ! DN(n)%j_xh_dpsi(1) = dmob_dpsi(1)*DN(n)%j_xh/mob - factor*(-DN(n)%dens_dqf)/CN(n)*(b_du_m*CN(n) + db_du_m + db_du_p)
                ! ! d(i+1/2)/d(i+1)
                ! DN(n)%j_xh_dpsi(2) = dmob_dpsi(2)*DN(n)%j_xh/mob + factor*(-DN(p)%dens_dqf)/CN(p)*(b_du_p*CN(p) + db_du_p + db_du_m)


                ! !!!test!!!!!!!!!!!!!!!

            endif

            if (SP(n)%y_upp_exist) then
                dy          = SP(n)%dy_upp
                p           = SP(n)%id_sy_upp
                temp        = TEMP_LATTICE

                if (.not.(DD_TL.or.DD_TN)) then
                    result      = get_dd_current_density(.true., ib, 2, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), dy )
                    DN(n)%j_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)

                    DN(n)%j_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    ! DN(n)%j_yh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    ! DN(n)%j_yh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl
                elseif (DD_TN) then
                    result      = get_dd_current_density(.true., ib, 2, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(CONT_TN%var(n),(/0,0,0,0,0,0,1,0/)), DUAL_NUM(CONT_TN%var(p),(/0,0,0,0,0,0,0,1/)), dy )

                    DN(n)%j_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    !write (*,*) dj_dphi(1), dj_dphi(2), dj_dpsi(1), dj_dpsi(2), dj_dn(1), dj_dn(2)

                    DN(n)%j_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_yh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%j_yh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl
                
                    result      = get_energy_current_density(.true., ib, 2, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(CONT_TN%var(n),(/0,0,0,0,0,0,1,0/)), DUAL_NUM(CONT_TN%var(p),(/0,0,0,0,0,0,0,1/)), dy , result)
                    DN(n)%s_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN(n)%s_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%s_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%s_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%s_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%s_yh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%s_yh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl                                        
                else
                    result      = get_dd_current_density(.true., ib, 2, n, p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)), dy )
                    DN(n)%j_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN(n)%j_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                    DN(n)%j_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                    DN(n)%j_yh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                    DN(n)%j_yh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl                   
                endif


              endif

          enddo
      !!$OMP END PARALLEL DO 
      endif


    ! n type thermionic emission
    if (DD_ELEC.and.(US%n_thermionic_emission.ge.1)) then
        do n=1,N_SP
            if (SP(n)%heterojunction) then
                p          = n +1
                if (EQUILIBRIUM) then !somehow in equilibirum this does not work, but it should be equal to zero anyway
                    result = 0
                else
                    if (DD_TN) then
                        result      = get_thermionic_emission_current_density(.true., CONT_ELEC%valley, n , p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(CONT_TN%var(n),(/0,0,0,0,0,0,1,0/)), DUAL_NUM(CONT_TN%var(p),(/0,0,0,0,0,0,0,1/)) )
                    else
                        result      = get_thermionic_emission_current_density(.true., CONT_ELEC%valley, n , p, DUAL_NUM(CONT_ELEC%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)) )
                    endif
                endif

                DN(n)%j_xh  = result%x_ad_
                dj_dphi(1)  = result%xp_ad_(1)
                dj_dphi(2)  = result%xp_ad_(2)
                dj_dpsi(1)  = result%xp_ad_(3)
                dj_dpsi(2)  = result%xp_ad_(4)
                dj_dn(1)    = result%xp_ad_(5)
                dj_dn(2)    = result%xp_ad_(6)
                dj_dtl(1)   = result%xp_ad_(7)
                dj_dtl(2)   = result%xp_ad_(8)

                DN(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN(n)%dens_dqf
                DN(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN(p)%dens_dqf
                DN(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN(n)%dens_dqf
                DN(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN(p)%dens_dqf
                DN(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN(n)%dens_dtl
                DN(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN(p)%dens_dtl

            endif
        enddo
    endif

    if (DD_ELEC2) then
    !!$OMP PARALLEL DO DEFAULT(PRIVATE), SHARED(VT_SEMI,DN,SP,CON,N_SP,PSI_SEMI,TEMP_SEMI,CN,CN0,SOURCE,DRAIN,SEMI,E0_ADD_SEMI,SNX1,US)
        do n=1,N_SP
            is      = SP(n)%model_id
            ib      = CONT_ELEC2%valley
            temp    = TEMP_LATTICE
            if (SP(n)%cont) then
                cont => CON(SP(n)%cont_id)

                if (EQUILIBRIUM) then
                    DN2(n)%j_con      =  0
                    DN2(n)%j_con_dqf  =  0
                    DN2(n)%j_con_dtl  =  0
                    DN2(n)%j_con_dpsi =  0
                else
                    v_n2               = CON(SP(n)%cont_id)%v_n2
                    DN2(n)%j_con      =  v_n2*(CN2(n)-CN20(n))
                    DN2(n)%j_con_dqf  =  v_n2*DN2(n)%dens_dqf
                    DN2(n)%j_con_dtl  =  0
                    DN2(n)%j_con_dpsi = -v_n2*DN2(n)%dens_dqf
                endif

            else
                DN2(n)%j_con      = 0
                DN2(n)%j_con_dqf  = 0
                DN2(n)%j_con_dtl  = 0
                DN2(n)%j_con_dpsi = 0
                
            endif

            if (SP(n)%x_upp_exist) then
                dx          = SP(n)%dx_upp
                p           = SP(n)%id_sx_upp

                if (.not.DD_TL) then
                    result      = get_dd_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC2%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC2%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN2(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN2(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), dx )
                    DN2(n)%j_xh = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)

                    DN2(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN2(n)%dens_dqf
                    DN2(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN2(p)%dens_dqf
                    DN2(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN2(n)%dens_dqf
                    DN2(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN2(p)%dens_dqf
                else
                    result      = get_dd_current_density(.true., ib, 1, n, p, DUAL_NUM(CONT_ELEC2%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC2%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN2(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN2(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)), dx )
                    DN2(n)%j_xh = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DN2(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN2(n)%dens_dqf
                    DN2(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN2(p)%dens_dqf
                    DN2(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN2(n)%dens_dqf
                    DN2(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN2(p)%dens_dqf
                    DN2(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN2(n)%dens_dtl
                    DN2(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN2(p)%dens_dtl

                endif

            endif

            if (SP(n)%y_upp_exist) then
                write(*,*) '***error*** 2D simulation with two electron continuity equations not implemented.'
                stop

            endif

        enddo
    !!$OMP END PARALLEL DO 
    endif

    ! n2 thermionic emission => leads to convergence errors
    if (DD_ELEC2.and.(US%n_thermionic_emission.ge.1)) then
        do n=1,N_SP
            if (SP(n)%heterojunction) then
                p           = n +1
                if (EQUILIBRIUM) then !somehow in equilibirum this does not work, but it should be equal to zero anyway
                    result = 0
                else
                    result      = get_thermionic_emission_current_density(.true., CONT_ELEC2%valley, n , p, DUAL_NUM(CONT_ELEC2%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_ELEC2%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CN2(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CN2(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)) )
                endif

                DN2(n)%j_xh = result%x_ad_
                dj_dphi(1)  = result%xp_ad_(1)
                dj_dphi(2)  = result%xp_ad_(2)
                dj_dpsi(1)  = result%xp_ad_(3)
                dj_dpsi(2)  = result%xp_ad_(4)
                dj_dn(1)    = result%xp_ad_(5)
                dj_dn(2)    = result%xp_ad_(6)
                dj_dtl(1)   = result%xp_ad_(7)
                dj_dtl(2)   = result%xp_ad_(8)

                DN2(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DN2(n)%dens_dqf
                DN2(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DN2(p)%dens_dqf
                DN2(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DN2(n)%dens_dqf
                DN2(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DN2(p)%dens_dqf
                DN2(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DN2(n)%dens_dtl
                DN2(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DN2(p)%dens_dtl

            endif
        enddo

    endif

    if (DD_HOLE) then
    !!$OMP PARALLEL DO DEFAULT(PRIVATE), SHARED(VT_SEMI,DP,SP,CON,N_SP,PSI_SEMI,TEMP_SEMI,CP,CP0,SOURCE,SEMI,E0_ADD_SEMI)
        do n=1,N_SP
            is = SP(n)%model_id
            ib = CONT_HOLE%valley
            if (SP(n)%cont) then
                ! if (EQUILIBRIUM) then
                !     DP(n)%j_con      = 0
                !     DP(n)%j_con_dqf  = 0
                !     DP(n)%j_con_dpsi = 0
                ! else
                    v_p              = CON(SP(n)%cont_id)%v_p
                    DP(n)%j_con      = -v_p*(CP(n)-CP0(n))
                    DP(n)%j_con_dqf  = -v_p*DP(n)%dens_dqf
                    DP(n)%j_con_dtl  = 0
                    DP(n)%j_con_dpsi =  v_p*DP(n)%dens_dqf
                    if (EQUILIBRIUM) DP(n)%j_con = 0
                ! endif
            endif

            if (SP(n)%x_upp_exist) then
                dx          = SP(n)%dx_upp
                p           = SP(n)%id_sx_upp
                temp        = TEMP_LATTICE

                if (.not.DD_TL) then
                    result      = get_dd_current_density(.false., ib, 1, n, p, DUAL_NUM(CONT_HOLE%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_HOLE%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CP(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CP(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), dx )
                    DP(n)%j_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)

                    DP(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DP(p)%dens_dqf
                else
                    result      = get_dd_current_density(.false., ib, 1, n, p, DUAL_NUM(CONT_HOLE%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_HOLE%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CP(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CP(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)), dx )
                    DP(n)%j_xh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DP(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DP(n)%dens_dtl
                    DP(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DP(p)%dens_dtl
                endif

                ! !test
                ! dqf         = (CONT_HOLE%var(p) - CONT_HOLE%var(n))/dx
                ! vth_mid     = (VT_LATTICE+VT_SEMI(p))*0.5
                ! temp_mid    = (TEMP_LATTICE+TEMP_LATTICE)*0.5
                ! mob         = get_mobility(is,1,CONT_HOLE%var(n),CONT_HOLE%var(p),dx,1,temp_mid,CP(n),DONATORS(n),ACCEPTORS(n),GRADING(n))
                ! du          = -log(CP(p)/CP(n))+dx*dqf/vth_mid
                ! b_du_p      = bernoulli( du)
                ! b_du_m      = bernoulli(-du)
                ! db_du_p     = dbern( du)*CP(n)
                ! db_du_m     = dbern(-du)*CP(p)
                ! factor      = Q*vth_mid*mob/dx

                ! DP(n)%j_xh  = -factor * (b_du_m*CP(p) - b_du_p*CP(n))      

                ! result       = get_mobility_dual(is,1,DUAL_NUM(CONT_HOLE%var(n),0),DUAL_NUM(CONT_HOLE%var(p),0),dx,1,temp_mid, DUAL_NUM(CP(n),1), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dn      = result%xp_ad_(1)
                ! result       = get_mobility_dual(is,1,DUAL_NUM(CONT_HOLE%var(n),1),DUAL_NUM(CONT_HOLE%var(p),0),dx,1,temp_mid, DUAL_NUM(CP(n),0), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dphi_l  = result%xp_ad_(1)
                ! result       = get_mobility_dual(is,1,DUAL_NUM(CONT_HOLE%var(n),0),DUAL_NUM(CONT_HOLE%var(p),1),dx,1,temp_mid, DUAL_NUM(CP(n),0), DONATORS(n),ACCEPTORS(n), GRADING(n) )
                ! dmob_dphi_r  = result%xp_ad_(1)

                ! dmob_dqf(1)  = dmob_dn*DP(n)%dens_dqf + dmob_dphi_l
                ! dmob_dqf(2)  = dmob_dphi_r
                ! dmob_dpsi(1) = - dmob_dn*DP(n)%dens_dqf
                ! dmob_dpsi(2) = 0

                ! ! d(i+1/2)/d(i)
                ! DP(n)%j_xh_dqf(1)  =  dmob_dqf(1)*DP(n)%j_xh/mob + factor*(b_du_p*DP(n)%dens_dqf + (DP(n)%dens_dqf/CP(n)-dble(1)/vth_mid)*(db_du_p + db_du_m))
                ! ! d(i+1/2)/d(i+1)
                ! DP(n)%j_xh_dqf(2)  =  dmob_dqf(2)*DP(n)%j_xh/mob - factor*(b_du_m*DP(p)%dens_dqf + (DP(p)%dens_dqf/CP(p)-dble(1)/vth_mid)*(db_du_p + db_du_m))
                ! ! d(i+1/2)/d(i)
                ! DP(n)%j_xh_dpsi(1) = dmob_dpsi(1)*DP(n)%j_xh/mob + factor*(-DP(n)%dens_dqf)/CP(n)*(b_du_p*CP(n) + db_du_m + db_du_p)
                ! ! d(i+1/2)/d(i+1)
                ! DP(n)%j_xh_dpsi(2) = dmob_dpsi(2)*DP(n)%j_xh/mob - factor*(-DP(p)%dens_dqf)/CP(p)*(b_du_m*CP(p) + db_du_p + db_du_m)


                ! !!!
            endif
            if (SP(n)%y_upp_exist) then
                dy          = SP(n)%dy_upp
                p           = SP(n)%id_sy_upp
                temp        = TEMP_LATTICE

                if (.not.DD_TL) then
                    result      = get_dd_current_density(.false., ib, 2, n, p, DUAL_NUM(CONT_HOLE%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_HOLE%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CP(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CP(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), dy )
                    DP(n)%j_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)

                    DP(n)%j_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DP(p)%dens_dqf

                else
                    result      = get_dd_current_density(.false., ib, 2, n, p, DUAL_NUM(CONT_HOLE%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_HOLE%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CP(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CP(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)), dy )
                    DP(n)%j_yh  = result%x_ad_
                    dj_dphi(1)  = result%xp_ad_(1)
                    dj_dphi(2)  = result%xp_ad_(2)
                    dj_dpsi(1)  = result%xp_ad_(3)
                    dj_dpsi(2)  = result%xp_ad_(4)
                    dj_dn(1)    = result%xp_ad_(5)
                    dj_dn(2)    = result%xp_ad_(6)
                    dj_dtl(1)   = result%xp_ad_(7)
                    dj_dtl(2)   = result%xp_ad_(8)

                    DP(n)%j_yh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_yh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_yh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DP(n)%dens_dqf
                    DP(n)%j_yh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DP(p)%dens_dqf
                    DP(n)%j_yh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DP(n)%dens_dtl
                    DP(n)%j_yh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DP(p)%dens_dtl
                endif
            endif
        enddo
    !!$OMP END PARALLEL DO 
    endif

    ! p type thermionic emission
    if (DD_HOLE.and.(US%n_thermionic_emission.ge.1)) then
        do n=1,N_SP
            if (SP(n)%heterojunction) then
                p          = n +1
                if (EQUILIBRIUM) then !somehow in equilibirum this does not work, but it should be equal to zero anyway
                    result = 0
                else
                    result      = get_thermionic_emission_current_density(.false., CONT_HOLE%valley, n , p, DUAL_NUM(CONT_HOLE%var(n),(/1,0,0,0,0,0,0,0/)), DUAL_NUM(CONT_HOLE%var(p),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(n),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(PSI_SEMI(p),(/0,0,0,1,0,0,0,0/)), DUAL_NUM(CP(n),(/0,0,0,0,1,0,0,0/)), DUAL_NUM(CP(p),(/0,0,0,0,0,1,0,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,1/)) )
                endif

                DP(n)%j_xh  = result%x_ad_
                dj_dphi(1)  = result%xp_ad_(1)
                dj_dphi(2)  = result%xp_ad_(2)
                dj_dpsi(1)  = result%xp_ad_(3)
                dj_dpsi(2)  = result%xp_ad_(4)
                dj_dn(1)    = result%xp_ad_(5)
                dj_dn(2)    = result%xp_ad_(6)
                dj_dtl(1)   = result%xp_ad_(7)
                dj_dtl(2)   = result%xp_ad_(8)

                DP(n)%j_xh_dqf(1)  = dj_dphi(1) + dj_dn(1)*DP(n)%dens_dqf
                DP(n)%j_xh_dqf(2)  = dj_dphi(2) + dj_dn(2)*DP(p)%dens_dqf
                DP(n)%j_xh_dpsi(1) = dj_dpsi(1) - dj_dn(1)*DP(n)%dens_dqf
                DP(n)%j_xh_dpsi(2) = dj_dpsi(2) - dj_dn(2)*DP(p)%dens_dqf
                DP(n)%j_xh_dtl(1)  = dj_dtl(1)  + dj_dn(1)*DP(n)%dens_dtl
                DP(n)%j_xh_dtl(2)  = dj_dtl(2)  + dj_dn(2)*DP(p)%dens_dtl

            endif
        enddo
    endif

endsubroutine

function get_dg_current_density(iselec, ib, n, p, c_l, c_r, dx) result (j)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the density-gradient pseudo current density, see:
    !https://link.springer.com/content/pdf/10.1023/A:1020732515391.pdf
    !
    ! input
    ! -----
    ! iselec : logical
    !   If true, use signs for electrons, else use signs for holes
    ! ib : integer
    !   Index of the band where the carriers reside
    ! n : integer
    !   Index of the left point in the semiconductor.
    ! p : integer
    !   Index of the right point in the semiconductor.
    ! c_l : DUAL_NUM
    !   The carrier density at the left point.
    ! c_r : DUAL_NUM
    !   The carrier density at the right point.
    ! d_x : real(8)
    !   The distance between the points.
    !
    ! output
    ! ------
    ! j : DUAL_NUM
    !   The density gradient density between the points. 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in)        :: iselec! index to semi
    integer,intent(in)        :: ib    ! index to band
    integer,intent(in)        :: n     ! index to SP left point
    integer,intent(in)        :: p     ! index to SP right point
    real(8),intent(in)        :: dx    ! distance between points
    TYPE(DUAL_NUM),intent(in) :: c_l   ! left carrier density
    TYPE(DUAL_NUM),intent(in) :: c_r   ! right carrier density

    integer        :: is_left, is_right! index to semi

    integer        :: signum !holes/electrons
    TYPE(DUAL_NUM) :: j      !current density
    type(type_band),pointer :: band_left => null(), band_right => null()
    real(8)        :: m_eff_l, m_eff_r, m_eff_ave

    !difference between electrons and holes is the sign of charge
    signum                  = 1
    if (.not.iselec) signum = -1

    is_left   = SP(n)%model_id
    is_right  = SP(p)%model_id

    band_left  => SEMI(is_left)%band_pointer(ib)%band
    band_right => SEMI(is_right)%band_pointer(ib)%band

    m_eff_l   = band_left%pos_dep_m_eff(n)
    m_eff_r   = band_right%pos_dep_m_eff(p)
    m_eff_ave = m_eff_l + m_eff_r
    m_eff_ave = m_eff_ave/2

    m_eff_ave = AM0
    !Q for normalization
    j         = signum*CONST_DG/m_eff_ave*(sqrt(c_r)-sqrt(c_l))/dx

    ! simple version seems to be better for small carrier densities ... lol
    ! j         = signum*CONST_DG/m_eff_ave*(c_r*c_l)**dble(0.25)*log((c_r/c_l)**dble(0.5))/dx

endfunction get_dg_current_density

function get_dd_current_density(iselec, ib, dim, n, p, phi_l, phi_r, psi_l, psi_r, c_l, c_r, t_l, t_r, dx) result (j)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate the drift diffusion carrier density in the most general case: non-isothermal, degenerate, material non-homogenous.
    ! See markus dissertation appendix for mathematical details.
    !
    ! input
    ! -----
    ! iselec : logcial
    !   If True: Use signs for electrons, else holes.
    ! is : integer
    !   Index to the semiconductor definition => OBSOLETE?
    ! dim : integer
    !   Dimension (x=1,y=2)
    ! ib : integer
    !   Index to the band where the carriers flow.
    ! n : integer
    !   Index to the left point.
    ! p : integer
    !   Index to the right point.
    ! phi_l : DUAL_NUM
    !   Quasi Fermi level at left point.
    ! phi_r : DUAL_NUM
    !   Quasi Fermi level at right point.
    ! psi_l : DUAL_NUM
    !   Electrostatic Potential level at left point.
    ! psi_r : DUAL_NUM
    !   Electrostatic Potential right at left point.
    ! c_l : DUAL_NUM
    !   Carrier density at left point.
    ! c_r : DUAL_NUM
    !   Carrier density at right point.
    ! t_l : DUAL_NUM
    !   Lattice temperature at left point.
    ! t_r : DUAL_NUM
    !   Lattice temperature at right point.
    ! dx : real(8)
    !   Distance between points.
    !
    ! output
    ! ------
    ! j : DUAL_NUM
    !   DD current density between points.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in)        :: iselec
    integer,intent(in)        :: ib
    integer,intent(in)        :: dim
    integer,intent(in)        :: n
    integer,intent(in)        :: p
    real(8),intent(in)        :: dx
    TYPE(DUAL_NUM),intent(in) :: phi_l
    TYPE(DUAL_NUM),intent(in) :: phi_r
    TYPE(DUAL_NUM),intent(in) :: psi_l
    TYPE(DUAL_NUM),intent(in) :: psi_r
    TYPE(DUAL_NUM),intent(in) :: c_l
    TYPE(DUAL_NUM),intent(in) :: c_r
    TYPE(DUAL_NUM),intent(in) :: t_l
    TYPE(DUAL_NUM),intent(in) :: t_r

    integer        :: is_left, is_right! index to semi

    TYPE(DUAL_NUM)        :: nc_l = DUAL_NUM(0,0)  ! effective carrier density left
    TYPE(DUAL_NUM)        :: nc_r = DUAL_NUM(0,0) ! effective carrier density right

    TYPE(DUAL_NUM) :: denominator,nominator=DUAL_NUM(0,0) !diffusion enhancement factor intermediate variable
    TYPE(DUAL_NUM) :: vth!,vth_left,vth_right    !temperature voltage
    integer        :: signum !holes/electrons
    TYPE(DUAL_NUM) :: j      !current density
    TYPE(DUAL_NUM) :: g      !diffusion enhancement factor
    !TYPE(DUAL_NUM) :: gl,gr      !diffusion enhancement factor
    TYPE(DUAL_NUM) :: mob    !mobility
    TYPE(DUAL_NUM) :: nu_l, nu_r,left_sqrt, right_sqrt,c_l_aprx,c_r_aprx,tmp   !mobility
    TYPE(DUAL_NUM) :: du,b_du_m,b_du_p,factor,ec_l,ec_r,t_ave!,t_ave_inv !intermediate variables for SG approximation
    type(type_band),pointer :: band_left => null(), band_right => null()
    real(8)        :: n_eff_l, n_eff_r
    real(8)        :: moby,velo
    real(8)        :: vth_lattice,dens
    real(8)        :: deltaef
    logical :: test
    !CRYO variables with quad precision
    TYPE(DUAL_NUM_2) :: duf,du_2,j_dual,sinh_term,du_quad
    TYPE(DUAL_NUM_2) :: phi_r_quad, phi_l_quad, vth_quad, g_quad, c_l_quad, c_r_quad, factor_quad, j_quad

    is_left   = SP(n)%model_id
    is_right  = SP(p)%model_id

    band_left  => SEMI(is_left)%band_pointer(ib)%band
    band_right => SEMI(is_right)%band_pointer(ib)%band

    !difference between electrons and holes is the sign of charge
    signum                  = 1
    if (.not.iselec) signum = -1

    !isothermal case 
    ! t_ave = (t_l + t_r)*0.5
    ! if (.false.) then !non-isothermal case from Kantner paper, in current hdev implementation not required
    ! if (t_l.eq.t_r) then

    t_ave = dble(0.5)*(t_l+t_r)
    ! else

    ! endif

    vth         = t_ave*BK_Q
    vth_lattice = TEMP_LATTICE*BK_Q
    ! vth         = vth_lattice

    n_eff_l  = band_left%pos_dep_n_eff(n)*NORM%N_N_inv
    n_eff_r  = band_right%pos_dep_n_eff(p)*NORM%N_N_inv

    ! get effective DOS
    nc_l     = n_eff_l*vth_lattice**(dble(band_right%dim)/dble(2))
    nc_r     = n_eff_r*vth_lattice**(dble(band_left%dim)/dble(2))

    !find band edge, shift by electrostatic potential and band potential
    ec_l = signum*band_left%e0  - psi_l
    ec_r = signum*band_right%e0 - psi_r
    if (ib.eq.CONT_ELEC%valley) then 
        ec_l = ec_l - UN(n) 
        ec_r = ec_r - UN(p)
        !entkoppelt für debugging
        if (DD_CONT_BOHM_N) then !quantum bohm potential
            ec_l = ec_l - CONT_BOHM_N%var(n)
            ec_r = ec_r - CONT_BOHM_N%var(p)
        endif
    elseif (ib.eq.CONT_ELEC2%valley) then 
        ec_l = ec_l - UN2(n)
        ec_r = ec_r - UN2(p)
    else
        ec_l = ec_l - UP(n)
        ec_r = ec_r - UP(p)
    endif

    ! This implementation is inspired by:
    ! augmented SG ""On thermodynamic consistency of a Scharfetter–Gummel scheme based on a modified thermal voltage for
    ! drift-diffusion equations with diffusion enhancement"" 
    g  = 1
    if (band_left%fermi.or.band_right%fermi) then
        if (band_left%type_para) then
            if ((c_r%x_ad_.lt.1).or.(c_l%x_ad_.lt.1)) then
                denominator = 0
            else
                test = (c_r%x_ad_.lt.1)
                denominator = ( log(c_l/nc_l)-log(c_r/nc_r) )
            endif
            !denominator = log(c_l*nc_r/(c_r*nc_l))
            nominator   = (ec_r-ec_l) + (phi_r - phi_l)
            if ((denominator.ne.0).and.(nominator.ne.0)) then
                !nominator = (fd1h_inv(c_l/nc_l)-fd1h_inv(c_r/nc_r) )
                ! nu_L - nu_R
                g         = signum*nominator / denominator / vth
                ! gl         = c_l/nc_l / dfd1h ( signum*(- ec_l - phi_l)/vth )
                ! gr         = c_r/nc_r / dfd1h ( signum*(- ec_r - phi_r)/vth )
            else
                ! limiting case for nu_L = nu_R
                ! F_1/2 / dF_1/2
                ! c = nc*fd1h => fd1h = c/nc
                if (c_l%x_ad_.lt.1) then
                    g = 1
                else
                    g          = fd1h ( signum*(- ec_l - phi_l)/vth ) / dfd1h ( signum*(- ec_l - phi_l)/vth )
                endif

            endif
        else
            write(*,'(A)') '***error*** Degenerate carrier statistics with these band parameter currently not implemented.'
            stop
        endif
        ! if (g.le.0.9) then
        !   write(*,'(A)') '***error*** degeneracy factor unphysical.'
        !   stop
        ! endif
    endif

    ! g = 1
    mob         = get_mobility_dual(n,is_left,ib,n,phi_l,phi_r,ec_l,ec_r,t_l,t_r,dx,dim,DUAL_NUM(TEMP_LATTICE,0),c_l,DONATORS(n),ACCEPTORS(n),GRADING(n),GRADING2(n),STRAIN(n))

    !calculate also y mobility in 1D case
    if (SEMI_DIM.eq.1) then
        ! no field
        moby         = get_mobility(n,is_left,ib,n,dble(0),dble(0),ec_l%x_ad_,ec_l%x_ad_,t_l%x_ad_,t_r%x_ad_,dx,2,TEMP_LATTICE,c_l%x_ad_,DONATORS(n),ACCEPTORS(n),GRADING(n),GRADING2(n),STRAIN(n))
        if (ib.eq.CONT_ELEC%valley) then 
            DN(n)%mob(2)  = moby
        elseif (ib.eq.CONT_ELEC2%valley) then 
            DN2(n)%mob(2) = moby
        else
            DP(n)%mob(2)  = moby
        endif
    endif

    !store mobility, so we do not need to recalculate it all the time
    if (ib.eq.CONT_ELEC%valley) then 
        DN(n)%mob(dim)  = mob%x_ad_
    elseif (ib.eq.CONT_ELEC2%valley) then 
        DN2(n)%mob(dim) = mob%x_ad_
    else
        DP(n)%mob(dim)  = mob%x_ad_
    endif

    ! additionally the gradient of the effective mass, using nir=nc_l
    ! Markus is a genius
    ! if (.not.(band_left%fermi.or.band_right%fermi)) then
    ec_l = ec_l - 0 
    ec_r = ec_r - signum*g * log(nc_r/nc_l)*vth !see Markus diss. for derivation in appendix
    ! else
    !   ec_l = ec_l - 0
    !   ec_r = ec_r - signum * ( fd1h_inv(c_r/nc_l) - fd1h_inv(c_r/nc_r) )*vth_right
    ! endif

    du      =  -(ec_r-ec_l) / vth !gradient band potential and psi
    factor  = vth*mob*g/dx
    if (.not.CRYO) then
        b_du_p  = bernoulli( signum*du/g)
        b_du_m  = bernoulli(-signum*du/g)
        ! b_du_m  = b_du_p *  exp(signum*du/g) !using identitiy B(-x)=B(x)*exp(x)
        j       = signum*factor*(b_du_p*c_r-b_du_m*c_l)
    else 
        !from paper "Quasi-Fermi-Based Charge Transport Scheme for Device Simulation in Cryogenic, Wide-Band-Gap, and High-Voltage Applications"
        if (EQUILIBRIUM) then
            j = 0
        else
            ! convert the arguments to quadruple precision
            phi_r_quad  = dual_to_quad(phi_r)
            phi_l_quad  = dual_to_quad(phi_l)
            vth_quad    = dual_to_quad(vth)
            g_quad      = dual_to_quad(g)
            c_l_quad    = dual_to_quad(c_l)
            c_r_quad    = dual_to_quad(c_r)
            du_quad     = dual_to_quad(du)
            factor_quad = dual_to_quad(factor)

            duf  = - signum * (phi_r_quad-phi_l_quad) / vth_quad / g_quad
            du_2 = du_quad*0.5 / g_quad
            
            ! numerically stable implementation of sinh term (mail exchange with GTS)
            if (abs(du_2).lt.1e-6) then
                sinh_term = 1.0 - du_2/6.0
            else
                sinh_term = du_2/sinh_dual_2(du_2)
            endif

            ! current according to reference
            j_quad       = signum*2.0*factor_quad*sqrt(c_l_quad)*sqrt(c_r_quad)*sinh_dual_2(duf*0.5)*sinh_term

            !convert back to double precision
            j            = quad_to_dual(j_quad)
        endif
    endif

    !store velocity, so we do not need to recalculate it all the time
    if (ib.eq.CONT_ELEC%valley) then 
        dens = (c_r%x_ad_+c_l%x_ad_)*0.5
        if (dens.ne.0) velo      = -j%x_ad_/( dens )
        ! velo        = mob%x_ad_*(phi_l%x_ad_-phi_r%x_ad_)/dx
        DN(n)%velo  = velo
    elseif (ib.eq.CONT_ELEC2%valley) then
        dens = (c_r%x_ad_+c_l%x_ad_)*0.5
        if (dens.ne.0) velo      = -j%x_ad_/( dens )
        DN2(n)%velo  = velo
    endif

    ! if (phi_l%x_ad_.eq.phi_r%x_ad_) then
    !   if (j%x_ad_.ge.1) then !some numerical playground
    !     write(*,*) '***error*** sg disc.'
    !     stop
    !   endif
    ! endif

    ! call assert_isclose(b_du_m_old%x_ad_, b_du_m%x_ad_, dble(1e-3),dble(1e-2))
    ! call assert_isclose(b_du_p_old%x_ad_, b_du_p%x_ad_, dble(1e-3),dble(1e-2))
    ! call assert_isclose(j_old%x_ad_, j%x_ad_, dble(1e-3),dble(1e-2))

endfunction get_dd_current_density

function get_energy_current_density(iselec, ib, dim, n, p, phi_l, phi_r, psi_l, psi_r, c_l, c_r, t_l, t_r, dx, dd_j) result (j)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculate the energy flux in the non-degenerate and material non-homogenous case.
    ! See markus dissertation appendix for mathematical details.
    !
    ! input
    ! -----
    ! iselec : logcial
    !   If True: Use signs for electrons, else holes.
    ! dim : integer
    !   Dimension (x=1,y=2)
    ! ib : integer
    !   Index to the band where the carriers flow.
    ! n : integer
    !   Index to the left point.
    ! p : integer
    !   Index to the right point.
    ! phi_l : DUAL_NUM
    !   Quasi Fermi level at left point.
    ! phi_r : DUAL_NUM
    !   Quasi Fermi level at right point.
    ! psi_l : DUAL_NUM
    !   Electrostatic Potential level at left point.
    ! psi_r : DUAL_NUM
    !   Electrostatic Potential right at left point.
    ! c_l : DUAL_NUM
    !   Carrier density at left point.
    ! c_r : DUAL_NUM
    !   Carrier density at right point.
    ! t_l : DUAL_NUM
    !   Lattice temperature at left point.
    ! t_r : DUAL_NUM
    !   Lattice temperature at right point.
    ! dx : real(8)
    !   Distance between points.
    ! dd_j : DUAL_NUM
    !   DD Current density.
    !
    ! output
    ! ------
    ! j : DUAL_NUM
    !   Energy flux density between points.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in)        :: iselec
    integer,intent(in)        :: ib
    integer,intent(in)        :: dim
    integer,intent(in)        :: n
    integer,intent(in)        :: p
    real(8),intent(in)        :: dx
    TYPE(DUAL_NUM),intent(in) :: phi_l
    TYPE(DUAL_NUM),intent(in) :: phi_r
    TYPE(DUAL_NUM),intent(in) :: psi_l
    TYPE(DUAL_NUM),intent(in) :: psi_r
    TYPE(DUAL_NUM),intent(in) :: c_l
    TYPE(DUAL_NUM),intent(in) :: c_r
    TYPE(DUAL_NUM),intent(in) :: t_l
    TYPE(DUAL_NUM),intent(in) :: t_r
    TYPE(DUAL_NUM),intent(in) :: dd_j

    integer        :: is_left, is_right! index to semi

    TYPE(DUAL_NUM)        :: nc_l = DUAL_NUM(0,0)  ! effective carrier density left
    TYPE(DUAL_NUM)        :: nc_r = DUAL_NUM(0,0) ! effective carrier density right

    integer        :: signum !holes/electrons
    TYPE(DUAL_NUM) :: j      !current density
    TYPE(DUAL_NUM) :: mob    !mobility
    TYPE(DUAL_NUM) :: kappa  !thermal cond.
    TYPE(DUAL_NUM) :: bern_r,bern_l,bern_arg,ec_l,ec_r,t_ave !intermediate variables for SG approximation
    type(type_band),pointer :: band_left => null(), band_right => null()
    real(8)        :: n_eff_l, n_eff_r, vth_lattice

    is_left   = SP(n)%model_id
    is_right  = SP(p)%model_id

    band_left  => SEMI(is_left)%band_pointer(ib)%band
    band_right => SEMI(is_right)%band_pointer(ib)%band

    ! j        = 0
    ! return

    !difference between electrons and holes is the sign of charge
    signum                  = 1
    if (.not.iselec) signum = -1
    ! signum                  = -signum
    !isothermal case 
    t_ave = (t_l + t_r)*0.5
    ! if (.false.) then !non-isothermal case from Kantner paper, in current hdev implementation not required
    !   t_ave_inv = ( t_l - t_r )/log(t_l/t_r)
    !   t_ave     = 1/t_ave_inv
    ! endif

    vth_lattice = TEMP_LATTICE*BK/Q

    n_eff_l  = band_left%pos_dep_n_eff(n)*NORM%N_N_inv
    n_eff_r  = band_right%pos_dep_n_eff(p)*NORM%N_N_inv

    ! get effective DOS
    nc_l     = n_eff_l*vth_lattice**(dble(band_right%dim)/dble(2))
    nc_r     = n_eff_r*vth_lattice**(dble(band_left%dim)/dble(2))

    !find band edge, shift by electrostatic potential and band potential
    ec_l = signum*band_left%e0  - psi_l
    ec_r = signum*band_right%e0 - psi_r
    if (ib.eq.CONT_ELEC%valley) then 
        ec_l = ec_l - UN(n) 
        ec_r = ec_r - UN(p)
        if (DD_CONT_BOHM_N) then !quantum bohm potential
            ec_l = ec_l - CONT_BOHM_N%var(n)
            ec_r = ec_r - CONT_BOHM_N%var(p)
        endif
    elseif (ib.eq.CONT_ELEC2%valley) then 
        ec_l = ec_l - UN2(n)
        ec_r = ec_r - UN2(p)
    else
        ec_l = ec_l - UP(n)
        ec_r = ec_r - UP(p)
    endif

    !mob         = get_mobility_dual(is_left,ib,n,phi_l,phi_r,ec_l,ec_r,t_l,t_r,dx,dim,DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)),c_l,DONATORS(n),ACCEPTORS(n),GRADING(n),GRADING2(n),STRAIN(n))
    mob         = get_mobility_dual(n,is_left,ib,n,phi_l,phi_r,ec_l,ec_r,t_l,t_r,dx,dim,DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)),c_l,DONATORS(n),ACCEPTORS(n),GRADING(n),GRADING2(n),STRAIN(n))

    ! additionally the gradient of the effective mass, using nir=nc_l
    ! Markus is a genius
    ! if (.not.(band_left%fermi.or.band_right%fermi)) then
    ec_l = ec_l - 0 
    ec_r = ec_r - signum* log(nc_r/nc_l)*vth_lattice !see Markus diss. for derivation in appendix

    ! kappa    =  dble(2.5)*BK**2/Q*mob*t_ave*(DUAL_NUM(c_r%x_ad_,(/0,0,0,0,0,0,0,0/))+DUAL_NUM(c_l%x_ad_,(/0,0,0,0,0,0,0,0/)))*dble(0.5)
    kappa    =  (dble(2.5)+dble(0))*BK**2/Q*mob*t_ave*(c_r+c_l)*dble(0.5)
    !bern_arg =  dble(2.5)*DUAL_NUM(dd_j%x_ad_,(/0,0,0,0,0,0,0,0/))*NORM%J_N*BK/Q/kappa*dx
    bern_arg =  dble(2.5)*dd_j*NORM%J_N*BK/Q/kappa*dx
    bern_l   =  bernoulli(+bern_arg)
    bern_r   =  bernoulli(-bern_arg)
    j        =  signum*kappa/dx*(t_l*bern_l-t_r*bern_r)
    ! j        =  signum*kappa/dx*(t_l-t_r)
endfunction get_energy_current_density

function get_thermionic_emission_current_density(iselec, ib, n, p, phi_l, phi_r, psi_l, psi_r, c_l, c_r, t_l, t_r) result (j)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get the thermionic emission current density between two points. See Markus dissertation for details.
    ! Or the book: Dietmar Schroeder, "Modelling of Interface Carrier Transport for Device Simulation", 1994, Springer
    !
    ! Numerical implementation: (1) "Investigation of interface charges at the heterojunction discontinuity in HBT devices", de la Fuente
    !                           (2) "THERMIONIC EMISSION IN A HYDRODYNAMIC MODEL FOR HETEROJUNCTION STRUCTURES", Hjelmgren
    !                           (3) "Electron Transport in Rectifying Semiconductor Alloy Ramp Heterostructures" G. Tait
    !
    ! input
    ! -----
    ! iselec : logcial
    !   If True: Use signs for electrons, else holes.
    ! ib : integer
    !   Index to the band where the carriers flow.
    ! n : integer
    !   Index to the left point.
    ! p : integer
    !   Index to the right point.
    ! phi_l : DUAL_NUM
    !   QFP at left point.
    ! phi_r : DUAL_NUM
    !   QFP at right point.
    ! psi_l : DUAL_NUM
    !   Electrostatic Potential level at left point.
    ! psi_r : DUAL_NUM
    !   Electrostatic Potential right at left point.
    ! c_l : DUAL_NUM
    !   Carrier density at left point.
    ! c_r : DUAL_NUM
    !   Carrier density at right point.
    ! t_l : DUAL_NUM
    !   Lattice temperature at left point.
    ! t_r : DUAL_NUM
    !   Lattice temperature at right point.
    !
    ! output
    ! ------
    ! j : DUAL_NUM
    !   TE current density between points.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    logical,intent(in)        :: iselec! index to semi
    integer,intent(in)        :: ib    ! index to band
    integer,intent(in)        :: n     ! index to SP left point
    integer,intent(in)        :: p     ! index to SP right point
    TYPE(DUAL_NUM),intent(in) :: phi_l ! left quasi fermi potential
    TYPE(DUAL_NUM),intent(in) :: phi_r ! right quasi fermi potential
    TYPE(DUAL_NUM),intent(in) :: psi_l ! left electrostatic  potential
    TYPE(DUAL_NUM),intent(in) :: psi_r ! right electrostatic potential
    TYPE(DUAL_NUM),intent(in) :: c_l   ! left carrier density
    TYPE(DUAL_NUM),intent(in) :: c_r   ! right carrier density
    TYPE(DUAL_NUM),intent(in) :: t_l   ! left temperature
    TYPE(DUAL_NUM),intent(in) :: t_r   ! right temperature

    integer        :: is_left, is_right! index to semi

    TYPE(DUAL_NUM) :: vth    !temperature voltage
    integer        :: signum !holes/electrons
    TYPE(DUAL_NUM) :: j      !current density
    TYPE(DUAL_NUM) :: ec_l,ec_r,t_ave !intermediate variables for SG approximation
    type(type_band),pointer :: band_left => null(), band_right => null()
    real(8)        ::  m_eff_l, m_eff_r,n_eff_l,n_eff_r,nc_l,nc_r, dens, fac_heterojunction, vth_lattice
    TYPE(DUAL_NUM) :: v_r, v_l, eb, grad_phi, corr, j_corr,zcr,zcl,dummy

    is_left   = SP(n)%model_id
    is_right  = SP(p)%model_id

    fac_heterojunction = SP(n)%fac_heterojunction

    band_left  => SEMI(is_left)%band_pointer(ib)%band
    band_right => SEMI(is_right)%band_pointer(ib)%band

    !difference between electrons and holes is the sign of charge
    signum                  = 1
    if (.not.iselec) signum = -1

    !isothermal case 
    t_ave = (t_l + t_r)*0.5

    vth         = t_ave*BK/Q
    vth_lattice = TEMP_LATTICE*BK/Q

    m_eff_l  = band_left%pos_dep_m_eff(n)
    m_eff_r  = band_right%pos_dep_m_eff(p)

    n_eff_l  = band_left%pos_dep_n_eff(n)*NORM%N_N_inv
    n_eff_r  = band_right%pos_dep_n_eff(p)*NORM%N_N_inv
    nc_l     = n_eff_l*vth_lattice**(dble(band_right%dim)/dble(2))
    nc_r     = n_eff_r*vth_lattice**(dble(band_left%dim)/dble(2))

    v_l = sqrt(2*BK*t_l/pi/m_eff_l)
    v_r = sqrt(2*BK*t_r/pi/m_eff_r)

    !find band edge, shift by electrostatic potential and band potential
    ec_l = signum*band_left%e0  - psi_l
    ec_r = signum*band_right%e0 - psi_r
    if (ib.eq.CONT_ELEC%valley) then 
        ec_l = ec_l - UN(n) 
        ec_r = ec_r - UN(p)
    elseif (ib.eq.CONT_ELEC2%valley) then 
        ec_l = ec_l - UN2(n)
        ec_r = ec_r - UN2(p)
    else
        ec_l = ec_l - UP(n)
        ec_r = ec_r - UP(p)
    endif

    eb = signum* ( ec_r - ec_l ) 
    !version by Schroeder: => Does not work with Fermi Dirac since not =0 for dphi=0
    ! if (eb.gt.0) then !barrier for current from left to right
    !     j   = signum*fac_heterojunction*0.5*(                   v_r * c_r              - m_eff_r/m_eff_l * c_l * v_l * exp(-eb/vth) )
    !     dens = c_r%x_ad_
    ! else !barrier for current from right to left
    !     j   = signum*fac_heterojunction*0.5*( m_eff_l/m_eff_r * v_r * c_r* exp(eb/vth) -                   c_l * v_l                )
    !     dens = c_l%x_ad_
    ! endif

    !version by Tait Westgate 1991:
    ! if (eb.gt.0) then !barrier for current from left to right
    !     j   = signum*fac_heterojunction*0.5*v_r * c_r*(1-exp((phi_l-phi_r)/vth))
    !     ! j   = 1e-2*v_r * c_r*(1-exp((phi_l-phi_r)/vth))
    !     dens = c_r%x_ad_
    ! else !barrier for current from right to left
    !     j   = signum*fac_heterojunction*0.5*v_l * c_l*(1-exp((phi_r-phi_l)/vth))
    !     ! j   = 1e-2*v_l * c_l*(1-exp((phi_r-phi_l)/vth))
    !     dens = c_l%x_ad_
    ! endif

    !"New Physical Formulation of the Thermionic Emission Current at the Heterojunction Interface", Chang, 1993
    ! https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=225566
    ! zc defined as zc = fd1h/exp
    zcr = c_r/nc_r/exp( signum*(-phi_r - ec_r)/vth )
    zcl = c_l/nc_l/exp( signum*(-phi_l - ec_l)/vth )
    if (eb.gt.0) then !barrier for current from left to right
        corr = -(log(nc_l/nc_r) + log(zcl/zcr))*vth
        eb   = eb - corr
        j    = signum*fac_heterojunction*v_r*m_eff_l/m_eff_r/zcr*(  c_r -  c_l * exp(-eb/vth) )*NORM%l_n
        dens = c_l%x_ad_
    else !barrier for current from right to left
        corr = -(log(nc_r/nc_l) + log(zcr/zcl))*vth
        eb   = eb + corr
        j    = signum*fac_heterojunction*v_l*m_eff_r/m_eff_l/zcl*(  c_r* exp(eb/vth) -  c_l  )*NORM%l_n
        dens = c_l%x_ad_
    endif

    if (ib.eq.CONT_ELEC%valley) then 
        if (dens.ne.0) DN(n)%velo      = -j%x_ad_/( dens )
        grad_phi = (phi_r - phi_l) / DX(n)
        DN(n)%mob  = DN(n-1)%mob
    endif

endfunction get_thermionic_emission_current_density

function get_bohm_integral(cn_l, cn_m, cn_r, ln_n, dx_low, dx_upp) result(integral)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the bohm integral at point, see diss Mueller for details.
    ! (1D along x only)
    !
    ! input
    ! -----
    ! cn_l : DUAL_NUM
    !   Carrier density at left point.
    ! cn_m : DUAL_NUM
    !   Carrier density at mid point.
    ! cn_r : DUAL_NUM
    !   Carrier density at right point.
    ! ln_n : DUAL_NUM
    !   Bohm potential at point.
    ! dx_low : real(8)
    !   Distance between left and mid point.
    ! dx_upp : real(8)
    !   Distance between right and mid point.
    !
    ! output
    ! ------
    ! integral : DUAL_NUM
    !   The value of the bohm integral.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    TYPE(DUAL_NUM),intent(in) :: ln_n !bohm potential at point
    TYPE(DUAL_NUM),intent(in) :: cn_l !left carrier density
    TYPE(DUAL_NUM),intent(in) :: cn_m !middle carrier density
    TYPE(DUAL_NUM),intent(in) :: cn_r !right carrier density
    real(8),intent(in)        :: dx_low !lower h
    real(8),intent(in)        :: dx_upp !upper h
    TYPE(DUAL_NUM)            :: integral

    integral  = 0
    !first term
    if (.not.(cn_m%x_ad_.eq.cn_l%x_ad_)) then
        integral  =            sqrt(cn_m)*dx_low/log(sqrt(cn_m)/sqrt(cn_l))*(1-sqrt(sqrt(cn_l)/sqrt(cn_m)))
    endif
    if (.not.(cn_m%x_ad_.eq.cn_r%x_ad_)) then
        integral  = integral + sqrt(cn_m)*dx_upp/log(sqrt(cn_r)/sqrt(cn_m))*(sqrt(sqrt(cn_r)/sqrt(cn_m))-1)
    endif
    integral    = integral*ln_n

    ! integral    = sqrt(cn_m)*ln_n*(dx_low+dx_upp)

endfunction get_bohm_integral

subroutine set_recombination
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set all current between the bands at the same point in XYZ, i.e. what is known as recombination
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,mx,px,hetero_id
    real(8) :: rec, zeta,dx_p,dx_m,zeta_inv,tn,drec_dtn
    real(8),dimension(3) :: drec_dphin, drec_dphin2, drec_dphip, drec_dtl, drec_dpsi, zeta_dqf, zeta_inv_dqf
    real(8) :: rec_dum,drec_dn,drec_dn2
    ! real(8),dimension(3) :: drec_dphin_dum, drec_dphin2_dum, drec_dphip_dum, drec_dtl_dum, drec_dpsi_dum, zeta_dqf_dum, zeta_inv_dqf_dum
    ! real(8),dimension(3) :: drec_dphin_dumm, drec_dphin2_dumm, drec_dphip_dumm, drec_dtl_dumm, drec_dpsi_dumm
    TYPE(DUAL_NUM) :: result


    hetero_id = 0
    if (DD_ELEC) then
        DN%intervalley = 0
        DN%recomb      = 0
        DN%zeta        = 0
        do n=1,N_SP
            DN(n)%intervalley_dqf  = 0
            DN(n)%intervalley_dqf2 = 0
            DN(n)%intervalley_dpsi = 0
            DN(n)%recomb_dqf       = 0
            DN(n)%recomb_dpsi      = 0
            DN(n)%recomb_dtl       = 0
            DN(n)%recomb_dtn       = 0
        enddo
    endif

    if (DD_ELEC2) then
        DN2%intervalley  = 0
        DN2%recomb       = 0
        DN2%zeta         = 0
        do n=1,N_SP
            DN2(n)%intervalley_dqf  = 0
            DN2(n)%intervalley_dqf2 = 0
            DN2(n)%intervalley_dpsi = 0
            DN2(n)%recomb_dqf       = 0
            DN2(n)%recomb_dqf       = 0
            DN2(n)%recomb_dtl       = 0
            DN2(n)%recomb_dtn       = 0
        enddo
    endif
        
    if (DD_HOLE) then
        DP%intervalley      = 0
        DP%recomb           = 0
        do n=1,N_SP
            DP(n)%intervalley_dqf  = 0
            DP(n)%intervalley_dqf2 = 0
            DP(n)%intervalley_dpsi = 0
            DP(n)%recomb_dqf       = 0
            DP(n)%recomb_dpsi      = 0
            DP(n)%recomb_dtl       = 0
            DP(n)%recomb_dtn       = 0
        enddo
    endif

    if (DD_ELEC.and.DD_HOLE.and.(.not.EQUILIBRIUM)) then
        !!$OMP PARALLEL DO DEFAULT(PRIVATE), SHARED(DN,DP,DN_TRAP,DP_TRAP,SP,N_SP,CN,CN_TRAP,CP,CP_TRAP,VT_SEMI,TRAP,PSI_SEMI,CHARGE_DIM)
        
        do n=1,N_SP

            !at heterojunctions we don't do anything, the large QFP drop is not taken into account by the recombination models
            if (SP(n)%heterojunction) then
                cycle
            elseif (n.gt.1) then
                if (SP(n-1)%heterojunction) cycle
            elseif (n.lt.N_SP) then
                if (SP(n+1)%heterojunction) cycle
            endif

            if(HETERO) hetero_id=n

            ! current in x direction for impact ionization
            px = SP(n)%id_sx_upp
            mx = SP(n)%id_sx_low
            dx_p = SP(n)%dx_upp
            dx_m = SP(n)%dx_low

            if (DD_TN) then 
                tn = CONT_TN%var(n)
            else
                tn = TEMP_LATTICE
            endif

            !classic recombination between conduction and valence bands
            if (SP(n)%x_low_exist.and.SP(n)%x_upp_exist) then !points at both sides exist
                call get_recombination(SP(n)%model_id,n, hetero_id, CN(n), CP(n), NI_SQ(n), DONATORS(n), ACCEPTORS(n),GRADING(n),TEMP_LATTICE&
                                      &  ,DN(n)%dens_dqf, -DN(n)%dens_dqf, DN(n)%dens_dtl, DP(n)%dens_dqf, -DP(n)%dens_dqf, DP(n)%dens_dtl &
                                      &  , CONT_ELEC%var(mx),CONT_ELEC%var(n), CONT_ELEC%var(px), CONT_HOLE%var(mx), CONT_HOLE%var(n), CONT_HOLE%var(px), tn, dx_m, dx_p, .true. &
                                      &  ,rec, drec_dphin, drec_dphip, drec_dpsi, drec_dtl, drec_dtn )

            elseif (SP(n)%x_low_exist) then !only lower point exists
                call get_recombination(SP(n)%model_id,n, hetero_id, CN(n), CP(n), NI_SQ(n), DONATORS(n), ACCEPTORS(n),GRADING(n),TEMP_LATTICE&
                                      &  ,DN(n)%dens_dqf, -DN(n)%dens_dqf, DN(n)%dens_dtl, DP(n)%dens_dqf, -DP(n)%dens_dqf, DP(n)%dens_dtl &
                                      &  , CONT_ELEC%var(mx),CONT_ELEC%var(n), dble(0), CONT_HOLE%var(mx), CONT_HOLE%var(n), dble(0), tn, dx_m, dble(0), .true. &
                                      &  ,rec, drec_dphin, drec_dphip, drec_dpsi, drec_dtl, drec_dtn )    

            elseif (SP(n)%x_upp_exist) then !only lower point exists
                call get_recombination(SP(n)%model_id,n, hetero_id, CN(n), CP(n), NI_SQ(n), DONATORS(n), ACCEPTORS(n),GRADING(n),TEMP_LATTICE&
                                      &  ,DN(n)%dens_dqf, -DN(n)%dens_dqf, DN(n)%dens_dtl, DP(n)%dens_dqf, -DP(n)%dens_dqf, DP(n)%dens_dtl &
                                      &  , dble(0),CONT_ELEC%var(n), CONT_ELEC%var(px),dble(0), CONT_HOLE%var(n), CONT_HOLE%var(px), tn, dble(0), dx_p, .true. &
                                      &  ,rec, drec_dphin, drec_dphip, drec_dpsi, drec_dtl, drec_dtn )

            endif
            
            DN(n)%recomb           = DN(n)%recomb            +  rec
            DN(n)%recomb_dqf       = DN(n)%recomb_dqf        +  drec_dphin
            DN(n)%recomb_dpsi      = DN(n)%recomb_dpsi       +  drec_dpsi
            DN(n)%recomb_dtn       = DN(n)%recomb_dtn        +  drec_dtn
          
            DP(n)%recomb           = DP(n)%recomb            +  rec
            DP(n)%recomb_dqf       = DP(n)%recomb_dqf        +  drec_dphip
            DP(n)%recomb_dpsi      = DP(n)%recomb_dpsi       +  drec_dpsi

        enddo
        !!$OMP END PARALLEL DO 
    endif

    !recombination between bands
    if (DD_ELEC.and.DD_ELEC2.and.(.not.EQUILIBRIUM)) then
        !!$OMP PARALLEL DO DEFAULT(PRIVATE), SHARED(DN,DP,DN_TRAP,DP_TRAP,SP,N_SP,CN,CN_TRAP,CP,CP_TRAP,VT_SEMI,TRAP,PSI_SEMI,CHARGE_DIM)
        
        do n=2,N_SP-1
            !point at heterojunction no contributions
            if ((n.lt.N_SP).and.(n.gt.1)) then
                if (SP(n)%model_id.ne.SP(n+1)%model_id) cycle
                if (SP(n)%model_id.ne.SP(n-1)%model_id) cycle
            endif

            !if (PSI_BUILDIN(n).gt.PSI_BUILDIN(n+1)) cycle
            ! if (X(n).lt.2000e-9) cycle

            if(HETERO) hetero_id=n
            px    = SP(n)%id_sx_upp
            mx    = SP(n)%id_sx_low
            dx_p = SP(n)%dx_upp
            dx_m = SP(n)%dx_low

            ! grad (psi-psi0) 

            if (SP(n)%x_low_exist.and.SP(n)%x_upp_exist) then !points at both sides exist
                call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
                                      &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
                                      &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
                                      &, rec, drec_dphin, drec_dphin2, drec_dpsi, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            
            elseif (SP(n)%x_low_exist) then !only lower point exists
                call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
                                      &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
                                      &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, dble(0), dble(0), dble(0),dble(0) &
                                      &, rec, drec_dphin, drec_dphin2, drec_dpsi, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)

            elseif (SP(n)%x_upp_exist) then !only upper point exists
                call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
                                      &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
                                      &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), dble(0), dble(0), dble(0), dble(0), PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
                                      &, rec, drec_dphin, drec_dphin2, drec_dpsi, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            endif

            DN(n)%intervalley        = DN(n)%intervalley       -  rec
            DN(n)%intervalley_dqf    = DN(n)%intervalley_dqf   -  drec_dphin
            DN(n)%intervalley_dqf2   = DN(n)%intervalley_dqf2  -  drec_dphin2
            DN(n)%intervalley_dpsi   = DN(n)%intervalley_dpsi  -  drec_dpsi
            DN(n)%zeta               = zeta
            DN(n)%zeta_dqf           = zeta_dqf
          
            DN2(n)%intervalley       = DN2(n)%intervalley      +  rec
            DN2(n)%intervalley_dqf   = DN2(n)%intervalley_dqf  +  drec_dphin
            DN2(n)%intervalley_dqf2  = DN2(n)%intervalley_dqf2 +  drec_dphin2
            DN2(n)%intervalley_dpsi  = DN2(n)%intervalley_dpsi +  drec_dpsi
            DN2(n)%zeta              = zeta_inv
            DN2(n)%zeta_dqf          = zeta_inv_dqf

            ! if ((OP.eq.195).and.(n.ge.300)) then

            !     ! derivative cn(n)
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n)+0.01*CN(n), CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dn = (rec_dum - rec)/(CN(n)*0.01)

            !     ! derivative cn2(n)
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)+0.01*CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dn2 = (rec_dum - rec)/(CN2(n)*0.01)

            !     ! derivative phin middle
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n), CONT_ELEC%var(n)+1e-6, CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dphin_dum(2) = (rec_dum - rec)*1e6

            !     ! derivative phin left
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx)+1e-6, CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dphin_dum(1) = (rec_dum - rec)*1e6

            !     ! derivative phin right
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n), CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px)+1e-6, CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dphin_dum(3) = (rec_dum - rec)*1e6


                
            !     ! derivative psi
            !     call set_intervalley_rates(SP(n)%model_id, n, CN(n), CN2(n)&
            !                           &, DN(n)%dens_dqf, -DN(n)%dens_dqf, DN2(n)%dens_dqf, -DN2(n)%dens_dqf&
            !                           &, PSI_SEMI(n)+1e-6, CONT_ELEC%var(n), CONT_ELEC2%var(n), PSI_SEMI(mx), CONT_ELEC%var(mx), CONT_ELEC2%var(mx),dx_m, PSI_SEMI(px), CONT_ELEC%var(px), CONT_ELEC2%var(px), dx_p &
            !                           &, rec_dum, drec_dphin_dumm, drec_dphin2_dumm, drec_dpsi_dumm, zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
            !     drec_dpsi_dum(2) = (rec_dum - rec)*1e6
                
            !     ! chain derivatives
            !     drec_dphin_dum(2) = drec_dphin_dum(2) + drec_dn*DN(n)%dens_dqf
            !     drec_dpsi_dum(2) = drec_dpsi_dum(2) - drec_dn*DN(n)%dens_dqf - drec_dn2*DN2(n)%dens_dqf
            !     drec_dphin2_dum(2) = drec_dphin2_dum(2) + drec_dn2*DN2(n)%dens_dqf

            !     drec_dphin_dum(3) = 0
            ! endif

        enddo
        !!$OMP END PARALLEL DO 
    endif

    if (DD_CONT_BOHM_N) then !see Diss mueller
        do n=1,N_SP
            if ((SP(n)%x_low_exist).and.(SP(n)%x_upp_exist)) then
                px     = SP(n)%id_sx_upp
                mx     = SP(n)%id_sx_low
                result = get_bohm_integral(DUAL_NUM(CN(mx),(/1,0,0,0,0,0,0,0/)),DUAL_NUM(CN(n),(/0,1,0,0,0,0,0,0/)), DUAL_NUM(CN(px),(/0,0,1,0,0,0,0,0/)), DUAL_NUM(CONT_BOHM_N%var(n),(/0,0,0,1,0,0,0,0/)), SP(n)%dx_low, SP(n)%dx_upp)
                DN(n)%bohm_integral     = result%x_ad_

                DN(n)%bohm_integral_dqf(1)  = +result%xp_ad_(1)*DN(mx)%dens_dqf 
                DN(n)%bohm_integral_dqf(2)  = +result%xp_ad_(2)*DN(n)%dens_dqf 
                DN(n)%bohm_integral_dqf(3)  = +result%xp_ad_(3)*DN(px)%dens_dqf 

                DN(n)%bohm_integral_dpsi(1) = -result%xp_ad_(1)*DN(mx)%dens_dqf 
                DN(n)%bohm_integral_dpsi(2) = -result%xp_ad_(2)*DN(n)%dens_dqf 
                DN(n)%bohm_integral_dpsi(3) = -result%xp_ad_(3)*DN(px)%dens_dqf

                DN(n)%bohm_integral_dl(1)   = -result%xp_ad_(1)*DN(mx)%dens_dqf 
                DN(n)%bohm_integral_dl(2)   = -result%xp_ad_(2)*DN(n)%dens_dqf + result%xp_ad_(4)
                DN(n)%bohm_integral_dl(3)   = -result%xp_ad_(3)*DN(px)%dens_dqf

                DN(n)%bohm_integral_dtl     = 0 

            endif
        enddo
    endif

endsubroutine



! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief set quasi fermi level in x-direction
! !> \details
! subroutine set_qf_1d(n)

! integer,intent(in) :: n !< x-line id

! integer :: i,j


! j=0
! do i=1,N_SP
!   if (SP(i)%xline_id.eq.n) then
!     j         = j+1
!     QFN_1D(j) = CONT_ELEC%var(i)
!     QFP_1D(j) = CONT_HOLE%var(i)

!   endif
! enddo


! endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief set generation rate due to tunneling
! !> \details
! subroutine set_tunnel_generation_rate(nn,fid)

! integer,intent(in)                       :: nn  !< x-line id
! integer,dimension(:),optional,intent(in) :: fid !< file ids for saving

! logical                      :: save_on
! integer                      :: iv,nbar,rix,lix,j,i,is
! integer                      :: n_e,k,n,iv_l,iv_r,id_l,id_r
! logical                      :: iscb
! real(8)                      :: e,t,rxi,lxi
! real(8),dimension(SNX1)      :: ec,ev,em,ec_eff
! integer,dimension(SNX1)      :: iv_act,iv_cb,iv_vb,id_act
! integer,dimension(WKB_BMAX)  :: six      ! start of barriers (box)
! real(8),dimension(WKB_BMAX)  :: sxi      ! start of barriers (position in box)
! integer,dimension(WKB_BMAX)  :: eix      ! end   of barriers (box)
! real(8),dimension(WKB_BMAX)  :: exi      ! end   of barriers (position in box)
! logical                      :: fullbar,kill
! real(8),dimension(:),pointer :: e_vec,t_save,tl_save,tr_save
! real(8),dimension(:,:),pointer :: in_save
! real(8)                        :: qf_l,qf_r,vt_r,vt_l
! real(8)                        :: vs,vd,fn_l,fn_r,fp_l,fp_r,de_upp,de_low
! real(8)                        :: in,in_dqf_l,in_dqf_r
! real(8)                        :: ip,ip_dqf_l,ip_dqf_r
! logical                        :: con_s,con_d
! real(8)                        :: fn_l_dqf,fp_l_dqf,fn_r_dqf,fp_r_dqf
! real(8)                        :: arg_l,arg_r
! real(8)                        :: de,max_de,e_add
! logical                        :: must_add
! real(8)                        :: scal,in_r,dqfl,dqfr

! !for saving
! character(len=200)                 :: name
! character(len=5)                   :: n_bar_str
! character(len=5)                   :: e_bar_str
! type(string)                       :: tunnel_cols(100)     !columns of IV file
! real(8),dimension(:,:),allocatable :: tunnel_vals      ! values of IV file




! ! --> nullify
! nullify(e_vec,t_save,tr_save,tl_save,in_save)

! ! --> check wether saving is enabled
! save_on = present(fid)

! ! --> source bias
! vs = get_bias(SOURCE)

! ! --> drain bias
! vd = get_bias(DRAIN)

! JN_TUN_1D      = 0 
! GN_TUN_1D      = 0
! GN_TUN_1D_DQF  = 0

! JP_TUN_1D      = 0
! GP_TUN_1D      = 0
! GP_TUN_1D_DQF  = 0


! do iv=1,N_BAND
!   if (iv.le.N_CB) then
!     is   = iv
!     iscb = .true.

!   else
!     is   = iv-N_CB
!     iscb = .false.

!   endif
  
!   !markus: Why these two cases?
!   !This if clause creates the bandprofile
!   if (DD_BBT) then
!     if (iscb) then
!       ec = get_bandprofile_1d(CONT_ELEC%valley)
!       ev = get_bandprofile_1d(CONT_HOLE%valley)
!       em = (ec+ev)/2
      
!     else
!       cycle

!     endif

!   else
!     if (iscb) then
!       ec = get_bandprofile_1d(CONT_ELEC%valley)

      
!     else
!       ev = get_bandprofile_1d(CONT_HOLE%valley)
      
!     endif
   
    
!   endif

!   if (DD_BBT) then
!     !creates a very dense energy grid, denser than x grid
!     n_e = 2*SNX1+2*SNX+8
!     allocate(e_vec(n_e))
!     e_vec(             1:  SNX1      ) = ec
!     e_vec(  SNX1      +1:2*SNX1      ) = ev
!     e_vec(2*SNX1      +1:2*SNX1+  SNX) = (ec(1:SNX)+ec(2:SNX1))/dble(2)
!     e_vec(2*SNX1+  SNX+1:2*SNX1+2*SNX) = (ev(1:SNX)+ev(2:SNX1))/dble(2)
!     e_vec(2*SNX1+2*SNX+1             ) = ec(1)   +1e-6
!     e_vec(2*SNX1+2*SNX+2             ) = ec(1)   -1e-6
!     e_vec(2*SNX1+2*SNX+3             ) = ec(SNX1)+1e-6
!     e_vec(2*SNX1+2*SNX+4             ) = ec(SNX1)-1e-6
!     e_vec(2*SNX1+2*SNX+5             ) = ev(1)   +1e-6
!     e_vec(2*SNX1+2*SNX+6             ) = ev(1)   -1e-6
!     e_vec(2*SNX1+2*SNX+7             ) = ev(SNX1)+1e-6
!     e_vec(2*SNX1+2*SNX+8             ) = ev(SNX1)-1e-6

!   else
!     n_e = SNX1+SNX+4
!     allocate(e_vec(n_e))
!     if (iscb) then
!       e_vec(             1:  SNX1      ) = ec
!       e_vec(  SNX1      +1:  SNX1+  SNX) = (ec(1:SNX)+ec(2:SNX1))/dble(2)
!       e_vec(  SNX1+  SNX+1             ) = ec(1)   +1e-6
!       e_vec(  SNX1+  SNX+2             ) = ec(1)   -1e-6
!       e_vec(  SNX1+  SNX+3             ) = ec(SNX1)+1e-6
!       e_vec(  SNX1+  SNX+4             ) = ec(SNX1)-1e-6

!     else
!       e_vec(             1:  SNX1      ) = ev
!       e_vec(  SNX1      +1:  SNX1+  SNX) = (ev(1:SNX)+ev(2:SNX1))/dble(2)
!       e_vec(  SNX1+  SNX+1             ) = ev(1)   +1e-6
!       e_vec(  SNX1+  SNX+2             ) = ev(1)   -1e-6
!       e_vec(  SNX1+  SNX+3             ) = ev(SNX1)+1e-6
!       e_vec(  SNX1+  SNX+4             ) = ev(SNX1)-1e-6

!     endif
!   endif
!   call heapsort(e_vec) !markus: because we just want to have a nice energy grid
  
!   do j=1,DD_E_ADD
!     must_add = .false.
!     max_de   = DD_DE_MIN
!     do k=2,n_e
!       de = abs(e_vec(k)-e_vec(k-1))
!       if (de.gt.max_de) then
!         e_add    = (e_vec(k)+e_vec(k-1))*dble(0.5)
!         must_add = .true.
!         max_de   = de
!       endif
!     enddo
!     if (must_add) then
!       call add_point(e_vec,n_e,e_add)
!     else
!       exit
!     endif
!   enddo

!   !for every energy get the effective barrier shapes 
!   do k=1,n_e
!     e = e_vec(k)

!     !get the potential as a function of x
!     if (DD_BBT) then
!       ! --> set conduction/valenceband
!       if (e.gt.maxval(em)) then
!         ! take only conduction band
!         ec_eff = ec
!         iv_act = CONT_ELEC%valley

!       elseif (e.le.minval(em)) then
!         ! take only flipped valence band
!         ec_eff = 2*e-ev
!         iv_act = CONT_HOLE%valley 

!       else
!         ! mix both bands (band to band tunneling possible)
!         ! mirror band code for btb
!         do j=1,SNX1
!           if (e.gt.em(j)) then
!             ec_eff(j) = ec(j)
!             iv_act(j) = CONT_ELEC%valley

!           else
!             ec_eff(j) = 2*e-ev(j) !mirroring bands
!             iv_act(j) = CONT_HOLE%valley

!           endif
!         enddo
!       endif
!     else
!       if (iscb) then 
!         ec_eff = ec
!         iv_act = iv_cb

!       else
!         ec_eff = 2*e-ev
!         iv_act = iv_vb

!       endif
      
!     endif

!     ! this function calculates
!     ! nbar: the number of barriers that start at six and end at eix
!     ! sxi and exi are the relative positions in the boxes where the barriers start/end
!     ! fullbar indicates wheather the barrier goes through the complete 1D simulation domain
!     call get_barrier(e,ec_eff, nbar,six,sxi,eix,exi,fullbar)

!     if (save_on) then
!       allocate(t_save(nbar))
!       allocate(tr_save(nbar+1))
!       allocate(tl_save(nbar+1))
!       allocate(in_save(nbar,2))
!       in_save = 0
!     endif

!     ! for every barrier at energy e, calculate the wkb tunnelling probability
!     do n=1,nbar
!       lix = six(n)
!       lxi = sxi(n)

!       ! transmission for each barrier at this location and energy
!       if (DD_BBT) then
!         call get_twkb_bbt(e,ec_eff,is,iv_act,lix,lxi,.true.,t,rix,rxi,kill)

!       else
!         if (iscb) then
!           call get_twkb(  e,ec,    is,iscb,  lix,lxi,.true.,t,rix,rxi,kill)

!         else
!           call get_twkb(  e,ev,    is,iscb,  lix,lxi,.true.,t,rix,rxi,kill)

!         endif

!       endif

!       if (US%dd%debug.ge.1) then
!         t_save(n) = t
!       endif

!       if (t.eq.0) cycle
      
!       iv_l = iv_act(lix)
!       id_l = id_act(lix)
!       vt_l = (dble(1)-lxi)*VT_LATTICE + (lxi)*VT_LATTICE
!       iv_r = iv_act(rix+1)
!       id_r = id_act(rix+1)
!       vt_r = (dble(1)-rxi)*VT_LATTICE + (rxi)*VT_LATTICE

!       ! left Fermi potential
!       con_s = ((lix.eq.1).and.(lxi.eq.dble(0)))
      
!       if (con_s) then
!         ! from source contact !MARKUS: commented this
!         ! if (.not.get_econok(e,iv_act(1),SOURCE)) cycle
!         qf_l = vs

!       !elseif (SEMI(id_l)%band_pointer(iv_l)%band%iscb.neqv.SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!       !  qf_l = (dble(1)-lxi)*QFN_1D(lix) + (lxi)*QFN_1D(lix+1)

!       else
!         if (SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!           qf_l = (dble(1)-lxi)*QFN_1D(lix) + (lxi)*QFN_1D(lix+1)

!         else
!           qf_l   = (dble(1)-lxi)*QFP_1D(lix) + (lxi)*QFP_1D(lix+1)

!         endif
        
!       endif

!       ! right Fermi potential
!       con_d = (rix.eq.SNX) .and.(rxi.eq.dble(1))
!       if (con_d) then
!         ! from drain contact MARKUS:commented this
!         ! if (.not.get_econok(e,iv_act(SNX1),DRAIN)) cycle
!         qf_r = vd

!       elseif (SEMI(id_l)%band_pointer(iv_l)%band%iscb.neqv.SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!        qf_r = (dble(1)-rxi)*QFN_1D(rix) + (rxi)*QFN_1D(rix+1)
!       elseif (.not.con_s) then
!        qf_r = vd ! testing

!       else
!         if (SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!           qf_r  = (dble(1)-rxi)*QFN_1D(rix) + (rxi)*QFN_1D(rix+1)

!         else
!           qf_r  = (dble(1)-rxi)*QFP_1D(rix) + (rxi)*QFP_1D(rix+1)

!         endif

!       endif
!       ! -->  adapt fermi potential in channel to reduce out-tunneling
!       ! see Diss. Mothes, chapter on DD tunnelling
!       if (abs(DD_DQF_TUN).gt.dble(0)) then
!         if (SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!           dqfl = -((dble(1)-lxi)*DN(lix)%j_xh + (lxi)*DN(lix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_l+(dble(1)-lxi)*ec_eff(lix) + (lxi)*ec_eff(lix+1))/vt_l))*0.5

!         else
!           dqfl = -((dble(1)-lxi)*DP(lix)%j_xh + (lxi)*DP(lix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_l+(dble(1)-lxi)*ec_eff(lix) + (lxi)*ec_eff(lix+1))/vt_l))*0.5

!         endif

!         if (SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!           dqfr = -((dble(1)-rxi)*DN(rix)%j_xh + (rxi)*DN(rix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_r+(dble(1)-rxi)*ec_eff(rix) + (rxi)*ec_eff(rix+1))/vt_r))*0.5

!         else
!           dqfr = -((dble(1)-rxi)*DP(rix)%j_xh + (rxi)*DP(rix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_r+(dble(1)-rxi)*ec_eff(rix) + (rxi)*ec_eff(rix+1))/vt_r))*0.5

!         endif

!         if (con_s) then
!           qf_r = qf_r + dqfr
  
!         elseif (con_d) then
!           qf_l = qf_l - dqfl

!         else
!           qf_r = qf_r + dqfr
!           qf_l = qf_l - dqfl

!         endif
!       endif
      
!       !calculation of distribution functions for left and right n and p
!       arg_l    = (e + qf_l)/vt_l
!       fn_l     =  dble(1)/( dble(1)+exp( arg_l ))
!       fn_l_dqf = -dble(1)/vt_l/(dble(2)+exp(arg_l)+exp(-arg_l))
!       fp_l     =  dble(1)/( dble(1)+exp(-arg_l))
!       fp_l_dqf =  dble(1)/vt_l/(dble(2)+exp(arg_l)+exp(-arg_l))
!       if (SEMI(id_l)%band_pointer(iv_l)%band%iscb.neqv.SEMI(id_r)%band_pointer(iv_r)%band%iscb) fp_l_dqf=0


!       arg_r    = (e + qf_r)/vt_r
!       fn_r     =  dble(1)/( dble(1)+exp( arg_r ))
!       fn_r_dqf = -dble(1)/vt_r/(dble(2)+exp(arg_r)+exp(-arg_r))
!       fp_r     =  dble(1)/( dble(1)+exp(-arg_r ))
!       fp_r_dqf =  dble(1)/vt_r/(dble(2)+exp(arg_r)+exp(-arg_r))
!       if (SEMI(id_l)%band_pointer(iv_l)%band%iscb.neqv.SEMI(id_r)%band_pointer(iv_r)%band%iscb) fp_r_dqf=0

!       if (k.lt.n_e) then
!         de_upp = (e_vec(k+1)-e_vec(k  ))/dble(2)

!       else
!         de_upp = 0

!       endif

!       if (k.gt.1) then
!         de_low = (e_vec(k  )-e_vec(k-1))/dble(2)

!       else
!         de_low = 0

!       endif

!       if (SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!         in       =  t*(fn_l-fn_r)*(de_low+de_upp)
!         in_r     =  t*(fn_r     )*(de_low+de_upp)
!         in_dqf_l =  t*fn_l_dqf   *(de_low+de_upp)
!         in_dqf_r = -t*fn_r_dqf   *(de_low+de_upp)

!       else
!         ip       =  t*(fp_l-fp_r)*(de_low+de_upp)
!         ip_dqf_l =  t*fp_l_dqf   *(de_low+de_upp)
!         ip_dqf_r = -t*fp_r_dqf   *(de_low+de_upp)

!       endif
      
!       ! if (SEMI(id_r)%band_pointer(iv_r)%band%iscb.neqv.SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!       !  if (abs(in).gt.(1e-3*sqrt(NI_SQ(1))*Q*SEMI(id_r)%band_pointer(id_r)%band%vf)) then
!       !    in       =  t*1*(de_low+de_upp)
!       !    in_dqf_l =  0
!       !    in_dqf_r =  0
!       !  endif
!       !  if (abs(ip).gt.(1e-3*sqrt(NI_SQ(id_r))*Q*SEMI(id_r)%band_pointer(id_r)%band%vf)) then
!       !    ip       =  t*1*(de_low+de_upp)
!       !    ip_dqf_l =  0
!       !    ip_dqf_r =  0
!       !  endif          
!       ! endif

!       if (save_on) then
!         in_save(n,1)  =  G0*t*fn_l
!         in_save(n,2)  =  G0*t*fn_r


!       endif

!       if (con_s) then
!         if (SEMI(id_r)%band_pointer(iv_r)%band%iscb)  then
!           JN_TUN_1D(1) = JN_TUN_1D(1) - in

!         else
!           JP_TUN_1D(1) = JP_TUN_1D(1) + ip

!         endif
!       endif
     
!       if ((SEMI(id_l)%band_pointer(iv_l)%band%iscb).and.(.not.con_s)) then
!           ! left cb injection, not contact
!           GN_TUN_1D(lix  )        = GN_TUN_1D(lix  )        - in*(dble(1)-lxi)
!           GN_TUN_1D(lix+1)        = GN_TUN_1D(lix+1)        - in*(        lxi)

!           GN_TUN_1D_DQF(2,lix  ) = GN_TUN_1D_DQF(2,lix  ) - in_dqf_l*(dble(1)-lxi)*(dble(1)-lxi)
!           GN_TUN_1D_DQF(3,lix  ) = GN_TUN_1D_DQF(3,lix  ) - in_dqf_l*(        lxi)*(dble(1)-lxi)
!           GN_TUN_1D_DQF(1,lix+1) = GN_TUN_1D_DQF(1,lix+1) - in_dqf_l*(dble(1)-lxi)*(        lxi)
!           GN_TUN_1D_DQF(2,lix+1) = GN_TUN_1D_DQF(2,lix+1) - in_dqf_l*(        lxi)*(        lxi)

!       endif

!       if ((SEMI(id_r)%band_pointer(iv_r)%band%iscb).and.(.not.con_d)) then
!           ! electron generation after barrier
!           GN_TUN_1D(rix  )        = GN_TUN_1D(rix  )        + in*(dble(1)-rxi)
!           GN_TUN_1D(rix+1)        = GN_TUN_1D(rix+1)        + in*(        rxi)
          
!           GN_TUN_1D_DQF(2,rix  ) = GN_TUN_1D_DQF(2,rix  ) + in_dqf_r*(dble(1)-rxi)*(dble(1)-rxi)
!           GN_TUN_1D_DQF(3,rix  ) = GN_TUN_1D_DQF(3,rix  ) + in_dqf_r*(        rxi)*(dble(1)-rxi)
!           GN_TUN_1D_DQF(1,rix+1) = GN_TUN_1D_DQF(1,rix+1) + in_dqf_r*(dble(1)-rxi)*(        rxi)
!           GN_TUN_1D_DQF(2,rix+1) = GN_TUN_1D_DQF(2,rix+1) + in_dqf_r*(        rxi)*(        rxi)

!       endif

!       if ((.not.SEMI(id_l)%band_pointer(iv_l)%band%iscb).and.(.not.con_s)) then
!           ! hole recombination before barrier
!           GP_TUN_1D(lix  )        = GP_TUN_1D(lix  )       + ip*(dble(1)-lxi)
!           GP_TUN_1D(lix+1)        = GP_TUN_1D(lix+1)       + ip*(        lxi)
!           GP_TUN_1D_DQF(2,lix  )  = GP_TUN_1D_DQF(2,lix  ) + ip_dqf_l*(dble(1)-lxi)*(dble(1)-lxi)
!           GP_TUN_1D_DQF(3,lix  )  = GP_TUN_1D_DQF(3,lix  ) + ip_dqf_l*(        lxi)*(dble(1)-lxi)
!           GP_TUN_1D_DQF(1,lix+1)  = GP_TUN_1D_DQF(1,lix+1) + ip_dqf_l*(dble(1)-lxi)*(        lxi)
!           GP_TUN_1D_DQF(2,lix+1)  = GP_TUN_1D_DQF(2,lix+1) + ip_dqf_l*(        lxi)*(        lxi)

!       endif

!       if ((.not.SEMI(id_r)%band_pointer(iv_r)%band%iscb).and.(.not.con_d)) then
!           GP_TUN_1D(rix  )        = GP_TUN_1D(rix  )       - ip*(dble(1)-rxi)
!           GP_TUN_1D(rix+1)        = GP_TUN_1D(rix+1)       - ip*(        rxi)
!           GP_TUN_1D_DQF(2,rix  )  = GP_TUN_1D_DQF(2,rix  ) - ip_dqf_r*(dble(1)-rxi)*(dble(1)-rxi)
!           GP_TUN_1D_DQF(3,rix  )  = GP_TUN_1D_DQF(3,rix  ) - ip_dqf_r*(        rxi)*(dble(1)-rxi)
!           GP_TUN_1D_DQF(1,rix+1)  = GP_TUN_1D_DQF(1,rix+1) - ip_dqf_r*(dble(1)-rxi)*(        rxi)
!           GP_TUN_1D_DQF(2,rix+1)  = GP_TUN_1D_DQF(2,rix+1) - ip_dqf_r*(        rxi)*(        rxi)

!       endif

!     enddo ! nbar

!     if (US%dd%debug.ge.1) then
!       do n=0,nbar
!         if (DD_BBT) then
!           ! call write_elpa_value(fid(is), e)
!           ! call write_elpa_value(fid(is), dble(n))
!           ! if (n.eq.0) then
!           !   call write_elpa_value(fid(is), tr_save(nbar+1))
!           !   call write_elpa_value(fid(is), dble(0))
!           !   call write_elpa_value(fid(is), dble(0))
!           !   call write_elpa_value(fid(is), dble(0))
!           !   call write_elpa_value(fid(is), get_x(1,dble(0)))
!           !   call write_elpa_value(fid(is), get_x(SNX,dble(1)))
!           !   call write_elpa_value(fid(is), dble(0))
!           !   call write_elpa_value(fid(is), dble(0),.true.)

!           ! else
!           !   call write_elpa_value(fid(is), t_save(n))
!           !   call write_elpa_value(fid(is), -(in_save(n,1)-in_save(n,2)))
!           !   call write_elpa_value(fid(is), -in_save(n,1))
!           !   call write_elpa_value(fid(is), -in_save(n,2))
!           !   call write_elpa_value(fid(is), get_x(six(n),sxi(n)))
!           !   call write_elpa_value(fid(is), get_x(eix(n),exi(n)))
!           !   call write_elpa_value(fid(is), -qf_l)
!           !   call write_elpa_value(fid(is), -qf_r,.true.)

!           ! endif
          

!         else
!           call write_elpa_value(fid(iv), e)
!           call write_elpa_value(fid(iv), dble(n))
!           name               = 'dd_tunnel_internals'
!           write(n_bar_str,"(I3.3)") n
!           write(e_bar_str,"(I3.3)") e
!           name = trim(name)//'_bar'//trim(n_bar_str)
!           name = trim(name)//'_e'//trim(e_bar_str)
!           name = trim(name)//'_debug.elpa'

!           i                  = 0
!           if (.not.allocated(tunnel_vals)) allocate(tunnel_vals(10,1))

!           i                  = i +1
!           tunnel_cols(i)%str = 't'
!           tunnel_vals(i,OP)  = t_save(n)

!           i                  = i +1
!           tunnel_cols(i)%str = 'in_l'
!           tunnel_vals(i,OP)  = in_save(n,1)

!           i                  = i +1
!           tunnel_cols(i)%str = 'in_r'
!           tunnel_vals(i,OP)  = in_save(n,2)

!           i                  = i +1
!           tunnel_cols(i)%str = 'x_l'
!           tunnel_vals(i,OP)  = get_x(six(n),sxi(n))

!           i                  = i +1
!           tunnel_cols(i)%str = 'x_r'
!           tunnel_vals(i,OP)  = get_x(eix(n),exi(n))

!           i                  = i +1
!           tunnel_cols(i)%str = 'qf_l'
!           tunnel_vals(i,OP)  = qf_l

!           i                  = i +1
!           tunnel_cols(i)%str = 'qf_r'
!           tunnel_vals(i,OP)  = qf_r
!           call save_elpa(name, tunnel_cols, tunnel_vals)

!         endif
!       enddo
!       deallocate(t_save,tr_save,tl_save,in_save)
!       nullify(t_save,tr_save,tl_save,in_save)
!     endif


!   enddo ! integration over energy
!   deallocate(e_vec)
!   nullify(e_vec)
! enddo ! loop over bands

! !end of integration

! !divide by correct dx
! GN_TUN_1D(1)           = GN_TUN_1D(1)      /SDX(1)*dble(2)
! GN_TUN_1D_DQF(:,1)     = GN_TUN_1D_DQF(:,1)/SDX(1)*dble(2)

! GP_TUN_1D(1)           = GP_TUN_1D(1)      /SDX(1)*dble(2)
! GP_TUN_1D_DQF(:,1)     = GP_TUN_1D_DQF(:,1)/SDX(1)*dble(2)

! do i=2,SNX
!   GN_TUN_1D(i)         = GN_TUN_1D(i)      /(SDX(i)+SDX(i-1))*dble(2)
!   GN_TUN_1D_DQF(:,i)   = GN_TUN_1D_DQF(:,i)/(SDX(i)+SDX(i-1))*dble(2)

!   GP_TUN_1D(i)         = GP_TUN_1D(i)      /(SDX(i)+SDX(i-1))*dble(2)
!   GP_TUN_1D_DQF(:,i)   = GP_TUN_1D_DQF(:,i)/(SDX(i)+SDX(i-1))*dble(2)

! enddo
! GN_TUN_1D(SNX1)        = GN_TUN_1D(SNX1)      /SDX(SNX)*dble(2)
! GN_TUN_1D_DQF(:,SNX1)  = GN_TUN_1D_DQF(:,SNX1)/SDX(SNX)*dble(2)

! GP_TUN_1D(SNX1)        = GP_TUN_1D(SNX1)      /SDX(SNX)*dble(2)
! GP_TUN_1D_DQF(:,SNX1)  = GP_TUN_1D_DQF(:,SNX1)/SDX(SNX)*dble(2)


! ! --> calculate current from generation rate

! JN_TUN_1D(2)    = JN_TUN_1D(1)   + GN_TUN_1D(1  )*SDX(1     )*0.5                     + GN_TUN_1D(2)*(SDX(1)*0.5 + SDX(2)*0.5)*0.5
! JP_TUN_1D(2)    = JP_TUN_1D(1)   + GP_TUN_1D(1  )*SDX(1     )*0.5                     + GP_TUN_1D(2)*(SDX(1)*0.5 + SDX(2)*0.5)*0.5
! do i=3,SNX
!   JN_TUN_1D(i)  = JN_TUN_1D(i-1) + GN_TUN_1D(i-1)*(SDX(i-2  )*0.5 + SDX(i-1)*0.5)*0.5 + GN_TUN_1D(i)*(SDX(i-1)*0.5 + SDX(i)*0.5)*0.5
!   JP_TUN_1D(i)  = JP_TUN_1D(i-1) + GP_TUN_1D(i-1)*(SDX(i-2  )*0.5 + SDX(i-1)*0.5)*0.5 + GP_TUN_1D(i)*(SDX(i-1)*0.5 + SDX(i)*0.5)*0.5
! enddo
! JN_TUN_1D(SNX1) = JN_TUN_1D(SNX) + GN_TUN_1D(SNX)*(SDX(SNX-1)*0.5 + SDX(SNX)*0.5)*0.5 + GN_TUN_1D(SNX1)*SDX(SNX)*0.5
! JP_TUN_1D(SNX1) = JP_TUN_1D(SNX) + GP_TUN_1D(SNX)*(SDX(SNX-1)*0.5 + SDX(SNX)*0.5)*0.5 + GP_TUN_1D(SNX1)*SDX(SNX)*0.5


! JN_TUN_1D      = G0*JN_TUN_1D 
! GN_TUN_1D      = G0*GN_TUN_1D         
! GN_TUN_1D_DQF  = G0*GN_TUN_1D_DQF

! JP_TUN_1D      = G0*JP_TUN_1D 
! GP_TUN_1D      = G0*GP_TUN_1D        
! GP_TUN_1D_DQF  = G0*GP_TUN_1D_DQF

! do j=1,SNX1
!   DN(j)%g_tun_dqf  = GN_TUN_1D_DQF(:,j)*scal
!   DP(j)%g_tun_dqf  = GP_TUN_1D_DQF(:,j)*scal

! enddo
! DN(:)%j_x_tun    = JN_TUN_1D(:)
! DN(:)%g_tun      = GN_TUN_1D(:)
! DP(:)%j_x_tun    = JP_TUN_1D(:)
! DP(:)%g_tun      = GP_TUN_1D(:)

! if (US%dd%tun_derive.lt.0) then
!   do j=1,SNX1
!     i = ID_1D(j)
!     DN(i)%g_tun_dqf = 0 
!     DP(i)%g_tun_dqf = 0 
!   enddo
! endif

! endsubroutine


! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief set generation rate due to tunneling without derivative
! !> \details
! subroutine set_tunnel_generation_rate_noderivative(dvs,dvd)

! real(8),optional :: dvs
! real(8),optional :: dvd
! ! no simul

! integer :: iv,nbar,rix,lix,j,i,is
! integer :: n_e,k,n,iv_l,iv_r,id_l,id_r
! logical :: iscb
! real(8) :: e,t,rxi,lxi
! real(8),dimension(SNX1) :: ec,ev,em,ec_eff
! integer,dimension(SNX1) :: iv_act,iv_cb,iv_vb,id_act
! integer,dimension(WKB_BMAX) :: six      ! start of barriers (box)
! real(8),dimension(WKB_BMAX) :: sxi      ! start of barriers (position in box)
! integer,dimension(WKB_BMAX) :: eix      ! end   of barriers (box)
! real(8),dimension(WKB_BMAX) :: exi      ! end   of barriers (position in box)
! logical :: fullbar,kill
! real(8), dimension(:),pointer :: e_vec
! real(8) :: qf_l,qf_r,vt_r,vt_l
! real(8) :: vs,vd,fn_l,fn_r,fp_l,fp_r,de_upp,de_low
! real(8) :: in,in_dqf_l,in_dqf_r
! real(8) :: ip,ip_dqf_l,ip_dqf_r
! logical :: con_s,con_d
! real(8) :: fn_l_dqf,fp_l_dqf,fn_r_dqf,fp_r_dqf
! real(8) :: arg_l,arg_r
! real(8) :: de,max_de,e_add
! logical :: must_add
! real(8) :: dqfl,dqfr

! ! --> source
! vs = get_bias(SOURCE)

! ! --> change of source voltage (for AC)
! if (present(dvs)) then
!   vs = vs+dvs
! endif

! ! --> drain
! vd = get_bias(DRAIN)

! ! --> change of drain voltage (for AC)
! if (present(dvd)) then
!   vd = vd+dvd
! endif


! JN_TUN_1D      = 0
! GN_TUN_1D      = 0

! JP_TUN_1D      = 0
! GP_TUN_1D      = 0


! nullify(e_vec)
! do iv=1,N_BAND
!   if (iv.le.N_CB) then
!     is   = iv
!     iscb = .true.
!   else
!     is   = iv-N_CB
!     iscb = .false.
!   endif
  
!   if (DD_BBT) then
!     if (iscb) then
!       ec = get_bandprofile_1d(CONT_ELEC%valley)
!       ev = get_bandprofile_1d(CONT_HOLE%valley)
!       em = (ec+ev)/2

!     else
!       cycle

!     endif

!   else
!     if (iscb) then
!       ec = get_bandprofile_1d(CONT_ELEC%valley)
      
!     else
!       ev = get_bandprofile_1d(CONT_HOLE%valley)
      
!     endif
    
    
!   endif

    
!   if (DD_BBT) then
!     n_e = 2*SNX1+2*SNX+8
!     allocate(e_vec(n_e))
!     e_vec(           1:  SNX1      ) = ec
!     e_vec(  SNX1    +1:2*SNX1      ) = ev
!     e_vec(2*SNX1    +1:2*SNX1+  SNX) = (ec(1:SNX)+ec(2:SNX1))/dble(2)
!     e_vec(2*SNX1+SNX+1:2*SNX1+2*SNX) = (ev(1:SNX)+ev(2:SNX1))/dble(2)
!     e_vec(2*SNX1+2*SNX+1) = ec(1)   +1e-6
!     e_vec(2*SNX1+2*SNX+2) = ec(1)   -1e-6
!     e_vec(2*SNX1+2*SNX+3) = ec(SNX1)+1e-6
!     e_vec(2*SNX1+2*SNX+4) = ec(SNX1)-1e-6
!     e_vec(2*SNX1+2*SNX+5) = ev(1)   +1e-6
!     e_vec(2*SNX1+2*SNX+6) = ev(1)   -1e-6
!     e_vec(2*SNX1+2*SNX+7) = ev(SNX1)+1e-6
!     e_vec(2*SNX1+2*SNX+8) = ev(SNX1)-1e-6
!   else
!     n_e = SNX1+SNX+4
!     allocate(e_vec(n_e))
!     if (SEMI(id_act(1))%band_pointer(iv)%band%iscb) then
!       e_vec(             1:  SNX1      ) = ec
!       e_vec(  SNX1      +1:  SNX1+  SNX) = (ec(1:SNX)+ec(2:SNX1))/dble(2)
!       e_vec(  SNX1+  SNX+1) = ec(1)   +1e-6
!       e_vec(  SNX1+  SNX+2) = ec(1)   -1e-6
!       e_vec(  SNX1+  SNX+3) = ec(SNX1)+1e-6
!       e_vec(  SNX1+  SNX+4) = ec(SNX1)-1e-6
!     else
!       e_vec(           1:  SNX1      ) = ev
!       e_vec(  SNX1    +1:  SNX1+  SNX) = (ev(1:SNX)+ev(2:SNX1))/dble(2)
!       e_vec(  SNX1+  SNX+1) = ev(1)   +1e-6
!       e_vec(  SNX1+  SNX+2) = ev(1)   -1e-6
!       e_vec(  SNX1+  SNX+3) = ev(SNX1)+1e-6
!       e_vec(  SNX1+  SNX+4) = ev(SNX1)-1e-6
!     endif
!   endif
!   call heapsort(e_vec)
  
!   do j=1,DD_E_ADD
!     must_add = .false.
!     max_de   = DD_DE_MIN
!     do k=2,n_e
!       de = abs(e_vec(k)-e_vec(k-1))
!       if (de.gt.max_de) then
!         e_add    = (e_vec(k)+e_vec(k-1))*dble(0.5)
!         must_add = .true.
!         max_de   = de
!       endif
!     enddo
!     if (must_add) then
!       call add_point(e_vec,n_e,e_add)
!     else
!       exit
!     endif
!   enddo

!   do k=1,n_e
!     e = e_vec(k)
!     if (DD_BBT) then
!       ! --> set conduction/valenceband
!       if (e.gt.maxval(em)) then
!         ! take only conduction band
!         ec_eff = ec
!         iv_act = iv_cb
!       elseif (e.le.minval(em)) then
!         ! take only flipped valence band
!         ec_eff = 2*e-ev
!         iv_act = iv_vb
!       else
!         ! mix both bands (band to band tunneling possible)
!         do j=1,SNX1
!           if (e.gt.em(j)) then
!             ec_eff(j) = ec(j)
!             iv_act(j) = iv_cb(j)
!           else
!             ec_eff(j) = 2*e-ev(j)
!             iv_act(j) = iv_vb(j)
!           endif
!         enddo
!       endif
!     else
!       if (iscb) then 
!         ec_eff = ec
!         iv_act = iv_cb
!       else
!         ec_eff = 2*e-ev
!         iv_act = iv_vb
!       endif

!     endif

!     call get_barrier(e,ec_eff, nbar,six,sxi,eix,exi,fullbar)

!     do n=1,nbar
!       lix = six(n)
!       lxi = sxi(n)
!       ! transmission
!       if (DD_BBT) then
!         call get_twkb_bbt(e,ec_eff,is,iv_act,lix,lxi,.true.,t,rix,rxi,kill)
!       else
!         if (iscb) then
!           call get_twkb(e,ec,is,iscb,lix,lxi,.true.,t,rix,rxi,kill)
!         else
!           call get_twkb(e,ev,is,iscb,lix,lxi,.true.,t,rix,rxi,kill)
!         endif
!       endif
!       if (t.eq.0) cycle
!       !if (t.le.1e-4) cycle ! test

!       ! left Fermi potential
!       con_s = ((lix.eq.1).and.(lxi.eq.dble(0)))
!       if (con_s) then
!         ! from source contact
!         qf_l    = vs
!         iv_l    = iv_act(1)
!         id_l    = id_act(1)
!         vt_l    = VT_LATTICE

!       else
!         iv_l    = iv_act(lix)
!         id_l    = id_act(lix)
!         vt_l    = (dble(1)-lxi)*VT_LATTICE + (lxi)*VT_LATTICE
!         if (SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!           qf_l  = (dble(1)-lxi)*QFN_1D(lix) + (lxi)*QFN_1D(lix+1)
!         else
!           qf_l  = (dble(1)-lxi)*QFP_1D(lix) + (lxi)*QFP_1D(lix+1)
!         endif
        
!       endif

      
!       ! right Fermi potential
!       con_d = (rix.eq.SNX).and.(rxi.eq.dble(1))
!       if (con_d) then
!         ! from drain contact
!         qf_r    = vd
!         iv_r    = iv_act(SNX1)
!         id_r    = id_act(SNX1)
!         vt_r    = VT_LATTICE

!       else
!         iv_r    = iv_act(rix+1)
!         id_r    = id_act(rix+1)
!         vt_r    = (dble(1)-rxi)*VT_LATTICE + (rxi)*VT_LATTICE
!         if (SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!           qf_r  = (dble(1)-rxi)*QFN_1D(rix) + (rxi)*QFN_1D(rix+1)
!         else
!           qf_r  = (dble(1)-rxi)*QFP_1D(rix) + (rxi)*QFP_1D(rix+1)
!         endif

!       endif

!       ! -->  adapt fermi potential in channel to reduce out-tunneling
!       if (abs(DD_DQF_TUN).gt.dble(0)) then
!         if (SEMI(id_l)%band_pointer(iv_l)%band%iscb) then
!           dqfl = -((dble(1)-lxi)*DN(lix)%j_xh + (lxi)*DN(lix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_l+(dble(1)-lxi)*ec_eff(lix) + (lxi)*ec_eff(lix+1))/vt_l))*0.5

!         else
!           dqfl = -((dble(1)-lxi)*DP(lix)%j_xh + (lxi)*DP(lix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_l+(dble(1)-lxi)*ec_eff(lix) + (lxi)*ec_eff(lix+1))/vt_l))*0.5

!         endif

!         if (SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!           dqfr = -((dble(1)-rxi)*DN(rix)%j_xh + (rxi)*DN(rix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_r+(dble(1)-rxi)*ec_eff(rix) + (rxi)*ec_eff(rix+1))/vt_r))*0.5

!         else
!           dqfr = -((dble(1)-rxi)*DP(rix)%j_xh + (rxi)*DP(rix+1)%j_xh)/G0*(dble(1)+DD_DQF_TUN*exp((qf_r+(dble(1)-rxi)*ec_eff(rix) + (rxi)*ec_eff(rix+1))/vt_r))*0.5

!         endif

!         if (con_s) then
!           qf_r = qf_r + dqfr
  
!         elseif (con_d) then
!           qf_l = qf_l - dqfl

!         else
!           qf_r = qf_r + dqfr
!           qf_l = qf_l - dqfl

!         endif
!       endif

!       arg_l    = (e + qf_l)/vt_l
!       fn_l     =  dble(1)/( dble(1)+exp( arg_l ))
!       fn_l_dqf = -dble(1)/vt_l/(dble(2)+exp(arg_l)+exp(-arg_l))
!       fp_l     =  dble(1)/( dble(1)+exp(-arg_l))
!       fp_l_dqf =  dble(1)/vt_l/(dble(2)+exp(arg_l)+exp(-arg_l))


!       arg_r    = (e + qf_r)/vt_r
!       fn_r     =  dble(1)/( dble(1)+exp( arg_r ))
!       fn_r_dqf = -dble(1)/vt_r/(dble(2)+exp(arg_r)+exp(-arg_r))
!       fp_r     =  dble(1)/( dble(1)+exp(-arg_r ))
!       fp_r_dqf =  dble(1)/vt_r/(dble(2)+exp(arg_r)+exp(-arg_r))
      
!       if (k.lt.n_e) then
!         de_upp = (e_vec(k+1)-e_vec(k  ))/dble(2)
!       else
!         de_upp = 0
!       endif
!       if (k.gt.1) then
!         de_low = (e_vec(k  )-e_vec(k-1))/dble(2)
!       else
!         de_low = 0
!       endif


!         in       =  t*(fn_l-fn_r)*(de_low+de_upp)
!         in_dqf_l =  t*fn_l_dqf   *(de_low+de_upp)
!         in_dqf_r = -t*fn_r_dqf   *(de_low+de_upp)

!         ip       =  t*(fp_l-fp_r)*(de_low+de_upp)
!         ip_dqf_l =  t*fp_l_dqf*(de_low+de_upp)
!         ip_dqf_r = -t*fp_r_dqf*(de_low+de_upp)

      
!       if (con_s) then
!         if (SEMI(id_l)%band_pointer(iv_l)%band%iscb.and.SEMI(id_r)%band_pointer(iv_r)%band%iscb) then
!           JN_TUN_1D(1) = JN_TUN_1D(1) - in


!         elseif ((.not.SEMI(id_l)%band_pointer(iv_l)%band%iscb).and.(.not.SEMI(id_r)%band_pointer(iv_r)%band%iscb)) then
!           JP_TUN_1D(1) = JP_TUN_1D(1) + ip
!         endif
!       endif
     
!       if ((SEMI(id_l)%band_pointer(iv_l)%band%iscb).and.(.not.con_s)) then
!           GN_TUN_1D(lix  )        = GN_TUN_1D(lix  )        - in*(dble(1)-lxi)
!           GN_TUN_1D(lix+1)        = GN_TUN_1D(lix+1)        - in*(        lxi)
!       endif

!       if ((SEMI(id_r)%band_pointer(iv_r)%band%iscb).and.(.not.con_d)) then
!           ! electron generation after barrier
!           GN_TUN_1D(rix  )        = GN_TUN_1D(rix  )        + in*(dble(1)-rxi)
!           GN_TUN_1D(rix+1)        = GN_TUN_1D(rix+1)        + in*(        rxi)
!       endif

!       if ((.not.SEMI(id_l)%band_pointer(iv_l)%band%iscb).and.(.not.con_s)) then
!         ! hole recombination befor barrier
!           GP_TUN_1D(lix  )        = GP_TUN_1D(lix  )        + ip*(dble(1)-lxi)
!           GP_TUN_1D(lix+1)        = GP_TUN_1D(lix+1)        + ip*(        lxi)

!       endif

!       if ((.not.SEMI(id_r)%band_pointer(iv_r)%band%iscb).and.(.not.con_d)) then
!           GP_TUN_1D(rix  )        = GP_TUN_1D(rix  )        - ip*(dble(1)-rxi)
!           GP_TUN_1D(rix+1)        = GP_TUN_1D(rix+1)        - ip*(        rxi)
!       endif

!     enddo ! nbar
!   enddo ! ne
! enddo ! n_band

! GN_TUN_1D(1)           = GN_TUN_1D(1)       /SDX(1)*dble(2)
! GP_TUN_1D(1)           = GP_TUN_1D(1)       /SDX(1)*dble(2)
! do i=2,SNX
!   GN_TUN_1D(i)         = GN_TUN_1D(i)       /(SDX(i)+SDX(i-1))*dble(2)
!   GP_TUN_1D(i)         = GP_TUN_1D(i)       /(SDX(i)+SDX(i-1))*dble(2)
! enddo
! GN_TUN_1D(SNX1)        = GN_TUN_1D(SNX1)       /SDX(SNX)*dble(2)
! GP_TUN_1D(SNX1)        = GP_TUN_1D(SNX1)       /SDX(SNX)*dble(2)


! ! --> calculate current from generation rate

! JN_TUN_1D(2)    = JN_TUN_1D(1)   + GN_TUN_1D(1  )*SDX(1     )*0.5                     + GN_TUN_1D(2)*(SDX(1)*0.5 + SDX(2)*0.5)*0.5
! JP_TUN_1D(2)    = JP_TUN_1D(1)   + GP_TUN_1D(1  )*SDX(1     )*0.5                     + GP_TUN_1D(2)*(SDX(1)*0.5 + SDX(2)*0.5)*0.5
! do i=3,SNX
!   JN_TUN_1D(i)  = JN_TUN_1D(i-1) + GN_TUN_1D(i-1)*(SDX(i-2  )*0.5 + SDX(i-1)*0.5)*0.5 + GN_TUN_1D(i)*(SDX(i-1)*0.5 + SDX(i)*0.5)*0.5
!   JP_TUN_1D(i)  = JP_TUN_1D(i-1) + GP_TUN_1D(i-1)*(SDX(i-2  )*0.5 + SDX(i-1)*0.5)*0.5 + GP_TUN_1D(i)*(SDX(i-1)*0.5 + SDX(i)*0.5)*0.5
! enddo
! JN_TUN_1D(SNX1) = JN_TUN_1D(SNX) + GN_TUN_1D(SNX)*(SDX(SNX-1)*0.5 + SDX(SNX)*0.5)*0.5 + GN_TUN_1D(SNX1)*SDX(SNX)*0.5
! JP_TUN_1D(SNX1) = JP_TUN_1D(SNX) + GP_TUN_1D(SNX)*(SDX(SNX-1)*0.5 + SDX(SNX)*0.5)*0.5 + GP_TUN_1D(SNX1)*SDX(SNX)*0.5


! JN_TUN_1D      = G0*JN_TUN_1D 
! GN_TUN_1D      = G0*GN_TUN_1D        

! JP_TUN_1D      = G0*JP_TUN_1D 
! GP_TUN_1D      = G0*GP_TUN_1D        

! !GN_TUN_1D(1)    = (JN_TUN_1D(2)-JN_TUN_1D(1))/SDX(1)
! !do i=2,SNX
! !  GN_TUN_1D(i)  = (JN_TUN_1D(i+1)-JN_TUN_1D(i-1))/(SDX(i-1)+SDX(i))
! !enddo
! !GN_TUN_1D(SNX1) = (JN_TUN_1D(SNX1)-JN_TUN_1D(SNX))/SDX(SNX)

! deallocate(e_vec)
  
! endsubroutine

subroutine get_dc_current(j_n,j_n2,j_p,j_displ)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the dc current at all contacts, and its components.
    !
    ! input
    ! -----
    ! j_n     : real(8),dimension(N_CON)
    !   The electron current density. One entry for each contact.
    ! j_n2    : real(8),dimension(N_CON)
    !   The electron 2 current density. One entry for each contact.
    ! j_p     : real(8),dimension(N_CON)
    !   The hole current density. One entry for each contact.
    ! j_displ : real(8),dimension(N_CON)
    !   The displacement current density. One entry for each contact.
    !
    ! output = input -> bad programming style.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8), dimension(N_CON),optional,intent(out) :: j_n     !< electron current
    real(8), dimension(N_CON),optional,intent(out) :: j_n2    !< electron current
    real(8), dimension(N_CON),optional,intent(out) :: j_p     !< hole current
    real(8), dimension(N_CON),optional,intent(out) :: j_displ !< displacement current

    real(8), dimension(N_CON) :: curr_n
    real(8), dimension(N_CON) :: curr_n2
    real(8), dimension(N_CON) :: curr_p
    real(8), dimension(N_CON) :: curr_displ
    integer                   :: i,n,mx,my,px,py,index_left,index_right,k,l
    real(8)                   :: dx,dy = 1
    logical                   :: supply = .False.


    curr_n     = 0
    curr_n2    = 0
    curr_p     = 0
    curr_displ = 0
    do n=1,N_SP
        if (SP(n)%cont) then
            mx = SP(n)%id_sx_low
            px = SP(n)%id_sx_upp
            dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
            if (SEMI_DIM.ge.2) then
                my = SP(n)%id_sy_low
                py = SP(n)%id_sy_upp
                dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
            endif
            i  = SP(n)%cont_id

            if (SP(n)%cont_right) then
                curr_n(i)   = curr_n(i)  - DN(mx)%j_xh   * dy
                curr_n2(i)  = curr_n2(i) - DN2(mx)%j_xh  * dy
                curr_p(i)   = curr_p(i)  - DP(mx)%j_xh   * dy
                ! if (DD_TUNNEL) then
                !   curr_n(i)   = curr_n(i)  - DN(n )%j_x_tun  * dy
                !   curr_n2(i)  = curr_n2(i) - DN2(n )%j_x_tun * dy
                !   curr_p(i)   = curr_p(i)  - DP(n )%j_x_tun  * dy
                ! endif
            endif
                
            if (SP(n)%cont_left) then
                curr_n(i)   = curr_n(i)  + DN(px )%j_xh  * dy
                curr_n2(i)  = curr_n2(i) + DN2(px )%j_xh * dy
                curr_p(i)   = curr_p(i)  + DP(px )%j_xh  * dy
                ! if (DD_TUNNEL) then
                !   curr_n(i)   = curr_n(i)  + DN(n )%j_x_tun  * dy
                !   curr_n2(i)  = curr_n2(i) + DN2(n )%j_x_tun * dy
                !   curr_p(i)   = curr_p(i)  + DP(n )%j_x_tun  * dy
                ! endif
            endif
                  
            if (SP(n)%cont_upper) then
                curr_n(i)   = curr_n(i)  - DN(my)%j_yh  * dx
                curr_n2(i)  = curr_n2(i) - DN2(my)%j_yh * dx
                curr_p(i)   = curr_p(i)  - DP(my)%j_yh  * dx
            endif
                          
            if (SP(n)%cont_lower) then
                curr_n(i)   = curr_n(i)  + DN(py )%j_yh  * dx
                curr_n2(i)  = curr_n2(i) + DN2(py )%j_yh * dx
                curr_p(i)   = curr_p(i)  + DP(py )%j_yh  * dx
            endif
            
        endif
    enddo

    ! current on supply contact X direction
    do k=1,NY1
        index_left  = 0
        index_right = 0
        
        !check if there is a supply contact at all
        supply = .False.
        do l=1,NX1
            if (POINT(l,k,1)%supply) supply = .True.
        enddo

        ! find indices to points left and right from supply contact
        do l=1,NX1
            if ((index_left.eq.0).and.(POINT(l,k,1)%supply)) index_left=l-1
        enddo
        do l=NX1,1,-1
            if ((index_right.eq.0).and.(POINT(l,k,1)%supply)) index_right=l
        enddo
        if ((index_left.le.0).and.(supply)) then
            write(*,*) '***warning*** supply contact at left simulation boundary. 1D supply contact only support when surrounded by semiconductor.'
            stop
        endif
        if ((index_right.ge.NX1).and.(supply)) then
            write(*,*) '***warning*** supply contact at right simulation boundary. 1D supply contact only support when surrounded by semiconductor.'
            stop
        endif
        if ((index_left.ne.0).or.(index_right.ne.0)) then
            ! find distance of cell
            dy = 1
            if (SEMI_DIM.ge.2) then
                if (index_left.ne.0) then
                    index_left = POINT(index_left, k, 1)%sp_idx
                    dy = (SP(index_left)%dy_low + SP(index_left)%dy_upp)*dble(0.5)
                    if (index_right.ne.0) index_right = POINT(index_right,k,1)%sp_idx
                else
                    index_right = POINT(index_right,k,1)%sp_idx
                    dy = (SP(index_right)%dy_low + SP(index_right)%dy_upp)*dble(0.5)
                    if (index_left.ne.0) index_left = POINT(index_left,k,1)%sp_idx
                endif
            endif

            ! lower side
            if (index_left.ne.0) then
                if (SP(index_left)%x_low_exist) then
                    i  = SP(SP(index_left)%id_sx_upp)%cont_id
                    if (CON(i)%supply_n) then
                        curr_n(i) = curr_n(i) - ( &
                            &   (DN(index_left)%j_xh  + DN2(index_left)%j_xh) &
                            & )*dy
                    elseif (CON(i)%supply_p) then
                        curr_p(i) = curr_p(i) - ( &
                            &   DP(index_left)%j_xh  &
                            & )*dy
                    endif
                endif
            endif

            ! upper side
            if (index_right.ne.0) then
                if (SP(index_right)%x_upp_exist) then
                    i  = SP(index_right)%cont_id
                    if (CON(i)%supply_n) then
                        curr_n(i) = curr_n(i) - ( &
                            & + (DN(index_right)%j_xh  + DN2(index_right)%j_xh) &
                            & )*dy
                    elseif (CON(i)%supply_p) then
                        curr_p(i) = curr_p(i) - ( &
                            &   - DP(index_right)%j_xh &
                            & )*dy
                    endif
                endif
            endif

        endif
    enddo

    ! current on supply contact Y direction
    if (SEMI_DIM.ge.2) then
        do k=1,NX1
            index_left  = 0
            index_right = 0
            
            !check if there is a supply contact at all
            supply = .False.
            do l=1,NY1
                if (POINT(k,l,1)%supply) supply = .True.
            enddo

            ! find indices to points left and right from supply contact
            do l=1,NY1
                if ((index_left.eq.0).and.(POINT(k,l,1)%supply)) index_left=l-1
            enddo
            do l=NY1,1,-1
                if ((index_right.eq.0).and.(POINT(k,l,1)%supply)) index_right=l
            enddo
            if ((index_left.ne.0).or.(index_right.ne.0)) then
                ! find distance of cell
                if (index_left.ne.0) then
                    index_left = POINT(k, index_left, 1)%sp_idx
                    ! dx = 0
                    ! if (POINT(k-1, SP(index_left)%id_y+1, 1)%supply) then
                    !     dx = dx + SP(index_left)%dx_upp*dble(0.5)
                    ! elseif (POINT(k  , SP(index_left)%id_y+1, 1)%supply) then
                    !     dx = dx + SP(index_left)%dx_low*dble(0.5)
                    ! endif
                    dx = (SP(index_left)%dx_low + SP(index_left)%dx_upp)*dble(0.5)
                    if (index_right.ne.0) index_right = POINT(k,index_right,1)%sp_idx
                else
                    index_right = POINT(k, index_right, 1)%sp_idx
                    dx = (SP(index_right)%dx_low + SP(index_right)%dx_upp)*dble(0.5)
                    if (index_left.ne.0) index_left = POINT(k,index_left,1)%sp_idx
                endif

                ! lower side (!!!)
                if (index_left.ne.0) then
                    if (SP(index_left)%y_low_exist) then
                        i  = SP(SP(index_left)%id_sy_upp)%cont_id
                        if (CON(i)%supply_n) then
                            curr_n(i) = curr_n(i) - ( &
                                &   (DN(index_left)%j_yh  + DN2(index_left)%j_yh) &
                                & )*dx
                        elseif (CON(i)%supply_p) then
                            curr_p(i) = curr_p(i) - ( &
                                &   DP(index_left)%j_yh  &
                                & )*dx
                        endif
                    endif
                endif

                ! upper side
                if (index_right.ne.0) then
                    if (SP(index_right)%y_upp_exist) then
                        i  = SP(SP(index_right)%id_sy_low)%cont_id
                        if (CON(i)%supply_n) then
                            curr_n(i) = curr_n(i) - ( &
                                & + (DN(index_right)%j_yh  + DN2(index_right)%j_yh) &
                                & )*dx
                        elseif (CON(i)%supply_p) then
                            curr_p(i) = curr_p(i) - ( &
                                &   - DP(index_right)%j_yh &
                                & )*dx
                        endif
                    endif
                endif

            endif
        enddo
    endif


    ! --> displacement current
    if (DD_DISPL.and.(TR_IT.ge.1)) then
        do i=1,N_CON
            curr_displ(i)  = (Q_CONT(i)-Q_CONT_LAST(i))/TR_DT(TR_IT)
        enddo

    else
        if ((GATE.ne.0).and.(N_CON.ge.3)) then
            if (.not.(CON(GATE)%supply)) then
                if (SOURCE.ne.0) then
                    curr_displ(GATE) = curr_displ(GATE) -(curr_n(SOURCE) + curr_n2(SOURCE) + curr_p(SOURCE))
                endif
                if (DRAIN.ne.0) then
                    curr_displ(GATE) = curr_displ(GATE) -(curr_n(DRAIN)  + curr_n2(DRAIN)  + curr_p(DRAIN))
                endif
            endif
        endif

    endif

    do i=1,N_CON
        CON(i)%current = curr_n(i) + curr_n2(i) + curr_p(i) + curr_displ(i)
    enddo

    if (present(j_n)    ) j_n     = curr_n
    if (present(j_n2)   ) j_n2    = curr_n2
    if (present(j_p)    ) j_p     = curr_p
    if (present(j_displ)) j_displ = curr_displ

endsubroutine


subroutine get_dc_inqu_current(jn_x,jn2_x,jp_x,jn_tun_x,jn2_tun_x,jp_tun_x,jn_y,jn2_y,jp_y)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the the current density vector at each point in the semiconductor and its components.
    !
    ! input
    ! -----
    ! jn_x : real(8),dimension(:),allocatable
    !   The electron current density in x direction.
    ! jn2_x : real(8),dimension(:),allocatable
    !   The electron 2 current density in x direction.
    ! jp_x : real(8),dimension(:),allocatable
    !   The hole current density in x direction.
    ! jn_tun_x : real(8),dimension(:),allocatable
    !   The electron tunnelling current density in x direction.
    ! jn2_tun_x : real(8),dimension(:),allocatable
    !   The electron 2 tunnelling current density in x direction.
    ! jp_tun_x : real(8),dimension(:),allocatable
    !   The hole tunnelling current density in x direction.
    ! jn_y : real(8),dimension(:),allocatable
    !   The electron current density in y direction.
    ! jn2_y : real(8),dimension(:),allocatable
    !   The electron 2 current density in y direction.
    ! jp_y : real(8),dimension(:),allocatable
    !   The hole 2 current density in y direction.
    !
    ! input = ouput
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),dimension(:),allocatable :: jn_x
    real(8),dimension(:),allocatable :: jn2_x
    real(8),dimension(:),allocatable :: jp_x
    real(8),dimension(:),allocatable :: jn_tun_x
    real(8),dimension(:),allocatable :: jn2_tun_x
    real(8),dimension(:),allocatable :: jp_tun_x
    real(8),dimension(:),allocatable :: jn_y
    real(8),dimension(:),allocatable :: jn2_y
    real(8),dimension(:),allocatable :: jp_y

    integer :: n,mx,my,px,py
    real(8) :: dx,dy

    allocate(jn_x(N_SP))
    allocate(jn2_x(N_SP))
    allocate(jp_x(N_SP))
    allocate(jn_tun_x(N_SP))
    allocate(jn2_tun_x(N_SP))
    allocate(jp_tun_x(N_SP))

    jn_x      = 0
    jn2_x     = 0
    jp_x      = 0
    jn_tun_x  = 0
    jn2_tun_x = 0
    jp_tun_x  = 0

    !here was a comment from SVEN saying that the name dy is confusing...which is true

    ! --> current in x- direction
    do n=1,N_SP
        mx = SP(n)%id_sx_low
        px = SP(n)%id_sx_upp
        if (SEMI_DIM.eq.1) then
            dy = dble(1)
        else
            dy = dble(1)
        endif
        if (SP(n)%x_upp_exist) then
            jn_x(n)  = DN(n)%j_xh  *dy
            jn2_x(n) = DN2(n)%j_xh*dy
            jp_x(n)  = DP(n)%j_xh *dy
        elseif (SP(n)%x_low_exist) then
            jn_x(n)  = DN(mx)%j_xh  *dy
            jn2_x(n) = DN2(mx)%j_xh*dy
            jp_x(n)  = DP(mx)%j_xh *dy
        else
            jp_x(n)  = DP(mx)%j_xh *dy
        endif
        
        ! if (SP(n)%x_upp_exist.and.SP(n)%x_low_exist) then
        !   jn_x(n)  = (DN(mx)%j_xh + DN(n )%j_xh)*dble(0.5) * dy
        !   jn2_x(n) = (DN2(mx)%j_xh + DN2(n )%j_xh)*dble(0.5) * dy
        !   jp_x(n)  = (DP(mx)%j_xh + DP(n )%j_xh)*dble(0.5) * dy

        ! elseif (SP(n)%x_low_exist) then
        !   if (SP(n)%cont_right.and.(.not.SP(n)%set_qfn).and.(.not.SP(n)%set_neutrality_n)) then
        !     jn_x(n)  = - ( DN(n )%j_con ) * dy
        !     jn2_x(n) = - ( DN2(n )%j_con) * dy
        !   else
        !     jn_x(n) =  (DN(mx)%j_xh )  * dy
        !     jn2_x(n) =  (DN2(mx)%j_xh )  * dy
        !   endif
        !   if (SP(n)%cont_right.and.(.not.SP(n)%set_qfp).and.(.not.SP(n)%set_neutrality_p)) then
        !     jp_x(n) = -DP(n )%j_con * dy
        !   else
        !     jp_x(n) =  DP(mx)%j_xh  * dy
        !   endif

        ! elseif (SP(n)%x_upp_exist) then
        !   if (SP(n)%cont_left.and.(.not.SP(n)%set_qfn).and.(.not.SP(n)%set_neutrality_n)) then
        !     jn_x(n) =  (DN(n )%j_con ) * dy
        !     jn2_x(n) =  (DN2(n )%j_con ) * dy
        !   else
        !     jn_x(n) =  (DN(n )%j_xh )  * dy
        !     jn2_x(n) =  (DN2(n )%j_xh)  * dy
        !   endif
        !   if (SP(n)%cont_left.and.(.not.SP(n)%set_qfp).and.(.not.SP(n)%set_neutrality_p)) then
        !     jp_x(n) =  DP(n )%j_con * dy
        !   else
        !     jp_x(n) =  DP(n )%j_xh  * dy
        !   endif

        ! else
        !  ! should not happen
            
        ! endif

        if (DD_TUNNEL) then
            jn_tun_x(n) = DN(n)%j_x_tun * dy
            jn_tun_x(n) = DN2(n)%j_x_tun * dy
            jp_tun_x(n) = DP(n)%j_x_tun * dy
        endif

    enddo

    ! --> current in y- direction
    if (SEMI_DIM.ge.2) then
        allocate(jn_y(N_SP))
        allocate(jn2_y(N_SP))
        allocate(jp_y(N_SP))
        jn_y     = 0
        jn2_y     = 0
        jp_y     = 0
        do n=1,N_SP
            my = SP(n)%id_sy_low
            py = SP(n)%id_sy_upp
            dx = dble(1)

            if (SP(n)%y_upp_exist.and.SP(n)%y_low_exist) then
                jn_y(n)  = (DN(my)%j_yh + DN(n )%j_yh )*dble(0.5) * dx
                jn2_y(n) = (DN2(my)%j_yh + DN2(n )%j_yh)*dble(0.5) * dx
                jp_y(n)  = (DP(my)%j_yh + DP(n )%j_yh)*dble(0.5) * dx

            elseif (SP(n)%y_low_exist) then
                if (SP(n)%cont_upper.and.(.not.SP(n)%set_qfn).and.(.not.SP(n)%set_neutrality_n)) then
                    jn_y(n) = -(DN(n )%j_con) * dx
                    jn2_y(n) = -(DN2(n )%j_con ) * dx
                else
                    jn_y(n) =  DN(my)%j_yh  * dx
                    jn2_y(n) =  DN2(my)%j_yh  * dx
                endif
                if (SP(n)%cont_upper.and.(.not.SP(n)%set_qfp).and.(.not.SP(n)%set_neutrality_p)) then
                    jp_y(n) = -DP(n )%j_con * dx
                else
                    jp_y(n) =  DP(my)%j_yh  * dx
                endif

            elseif (SP(n)%y_upp_exist) then
                if (SP(n)%cont_lower.and.(.not.SP(n)%set_qfn).and.(.not.SP(n)%set_neutrality_n)) then
                    jn_y(n) =  (DN(n )%j_con ) * dx
                    jn2_y(n) =  (DN2(n )%j_con) * dx
                else
                    jn_y(n) =  (DN(n )%j_yh )  * dx
                    jn2_y(n) =  (DN2(n )%j_yh )  * dx
                endif
                if (SP(n)%cont_lower.and.(.not.SP(n)%set_qfp).and.(.not.SP(n)%set_neutrality_p)) then
                    jp_y(n) =  DP(n )%j_con * dx
                else
                    jp_y(n) =  DP(n )%j_yh  * dx
                endif

            else
            ! should not happen
            
            endif

        enddo
    endif

endsubroutine

subroutine get_ac_current(omega)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! get the ac current density at the device terminals.
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in) :: omega !< 2*pi*frequency

    real(8),dimension(6) :: sf_charge
    integer :: n,mx,my,px,py,id
    integer :: index_left, index_right,index_cont,i,k,l
    real(8) :: dy=1,dx = 1
    logical :: supply = .False.


    CON(:)%ac_current=0

    do n=1,N_SP
        if (SP(n)%cont.and.(.not.SP(n)%supply)) then
            mx = SP(n)%id_sx_low
            px = SP(n)%id_sx_upp
            dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
            if (SEMI_DIM.eq.2) then
                my = SP(n)%id_sy_low
                py = SP(n)%id_sy_upp
                dy   = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
            endif
            id = SP(n)%cont_id

            if (SP(n)%cont_right) then
                CON(id)%ac_current = CON(id)%ac_current - DN(mx)%j_xh_ac   * dy
                CON(id)%ac_current = CON(id)%ac_current - DN2(mx)%j_xh_ac  * dy
                CON(id)%ac_current = CON(id)%ac_current - DP(mx)%j_xh_ac   * dy
                ! if (DD_TUNNEL) then
                !   curr_n(i)   = curr_n(i)  - DN(n )%j_x_tun  * dy
                !   curr_n2(i)  = curr_n2(i) - DN2(n )%j_x_tun * dy
                !   curr_p(i)   = curr_p(i)  - DP(n )%j_x_tun  * dy
                ! endif
            endif
                
            if (SP(n)%cont_left) then
                CON(id)%ac_current = CON(id)%ac_current + DN(px )%j_xh_ac  * dy
                CON(id)%ac_current = CON(id)%ac_current + DN2(px )%j_xh_ac * dy
                CON(id)%ac_current = CON(id)%ac_current + DP(px )%j_xh_ac  * dy
                ! if (DD_TUNNEL) then
                !   curr_n(i)   = curr_n(i)  + DN(n )%j_x_tun  * dy
                !   curr_n2(i)  = curr_n2(i) + DN2(n )%j_x_tun * dy
                !   curr_p(i)   = curr_p(i)  + DP(n )%j_x_tun  * dy
                ! endif
            endif
                    
            if (SP(n)%cont_upper) then
                CON(id)%ac_current = CON(id)%ac_current - DN(my)%j_yh_ac  * dx
                CON(id)%ac_current = CON(id)%ac_current - DN2(my)%j_yh_ac * dx
                CON(id)%ac_current = CON(id)%ac_current - DP(my)%j_yh_ac  * dx
            endif
                            
            if (SP(n)%cont_lower) then
                CON(id)%ac_current = CON(id)%ac_current + DN(py )%j_yh_ac  * dx
                CON(id)%ac_current = CON(id)%ac_current + DN2(py )%j_yh_ac * dx
                CON(id)%ac_current = CON(id)%ac_current + DP(py )%j_yh_ac  * dx
            endif
        endif
    enddo

    ! current on supply contact X direction
    do k=1,NY1
        index_left  = 0
        index_right = 0
        
        !check if there is a supply contact at all
        supply = .False.
        do l=1,NX1
            if (POINT(l,k,1)%supply) supply = .True.
        enddo

        ! find indices to points left and right from supply contact
        do l=1,NX1
            if ((index_left.eq.0).and.(POINT(l,k,1)%supply)) index_left=l-1
        enddo
        do l=NX1,1,-1
            if ((index_right.eq.0).and.(POINT(l,k,1)%supply)) index_right=l
        enddo
        if ((index_left.le.0).and.(supply)) then
            write(*,*) '***warning*** supply contact at left simulation boundary. 1D supply contact only supported when surrounded by semiconductor.'
            stop
        endif
        if ((index_right.ge.NX1).and.(supply)) then
            write(*,*) '***warning*** supply contact at right simulation boundary. 1D supply contact only supported when surrounded by semiconductor.'
            stop
        endif
        if ((index_left.ne.0).or.(index_right.ne.0)) then
            ! find distance of cell
            dy = 1
            if (SEMI_DIM.ge.2) then
                if (index_left.ne.0) then !(index_left+1,k,1) ist ein supply Punkt
                    index_left = POINT(index_left, k, 1)%sp_idx

                    ! dy = 0
                    ! if (POINT(SP(index_left)%id_x+1, k-1, 1)%supply) then
                    !     dy = dy + SP(index_left)%dy_upp*dble(0.5)
                    ! endif
                    ! if (POINT(SP(index_left)%id_x+1, k, 1)%supply) then
                    !     dy = dy + SP(index_left)%dy_low*dble(0.5)
                    ! endif
                    dy = (SP(index_left)%dy_low + SP(index_left)%dy_upp)*dble(0.5)

                    if (index_right.ne.0) index_right = POINT(index_right,k,1)%sp_idx
                else
                    index_right = POINT(index_right,k,1)%sp_idx
                    dy = (SP(index_right)%dy_low + SP(index_right)%dy_upp)*dble(0.5)
                    if (index_left.ne.0) index_left = POINT(index_left,k,1)%sp_idx
                endif
            endif

            ! lower side (auskommentieren erhöt fT massiv)
            if (index_left.ne.0) then
                if (SP(index_left)%x_low_exist) then
                    i  = SP(SP(index_left)%id_sx_upp)%cont_id
                    if (CON(i)%supply_n) then
                        CON(i)%ac_current = CON(i)%ac_current - ( &
                            &   (DN(index_left)%j_xh_ac   + DN2(index_left)%j_xh_ac) &
                            & )*dy
                    elseif (CON(i)%supply_p) then
                        CON(i)%ac_current = CON(i)%ac_current - ( &
                            &   DP(index_left)%j_xh_ac  &
                            & )*dy
                    endif
                endif
            endif

            ! upper side
            if (index_right.ne.0) then
                if (SP(index_right)%x_upp_exist) then
                    i  = SP(index_right)%cont_id
                    if (CON(i)%supply_n) then
                        CON(i)%ac_current = CON(i)%ac_current - ( &
                            & + (DN(index_right)%j_xh_ac  + DN2(index_right)%j_xh_ac) &
                            & )*dy
                    elseif (CON(i)%supply_p) then
                        CON(i)%ac_current = CON(i)%ac_current - ( &
                            &   - DP(index_right)%j_xh_ac &
                            & )*dy
                    endif
                endif
            endif
        endif
    enddo

    ! current on supply contact Y direction
    if (SEMI_DIM.ge.2) then
        do k=1,NX1
            index_left  = 0
            index_right = 0
            
            !check if there is a supply contact at all
            supply = .False.
            do l=1,NY1
                if (POINT(k,l,1)%supply) supply = .True.
            enddo

            ! find indices to points left and right from supply contact
            do l=1,NY1
                if ((index_left.eq.0).and.(POINT(k,l,1)%supply)) index_left=l-1
            enddo
            do l=NY1,1,-1
                if ((index_right.eq.0).and.(POINT(k,l,1)%supply)) index_right=l
            enddo
            if ((index_left.ne.0).or.(index_right.ne.0)) then
                ! find distance of cell
                if (index_left.ne.0) then
                    index_left = POINT(k, index_left, 1)%sp_idx

                    ! dx = 0
                    ! if (POINT(k-1, SP(index_left)%id_y+1, 1)%supply) then
                    !     dx = dx + SP(index_left)%dx_upp*dble(0.5)
                    ! endif
                    ! if (POINT(k  , SP(index_left)%id_y+1, 1)%supply) then
                    !     dx = dx + SP(index_left)%dx_low*dble(0.5)
                    ! endif
                    dx = (SP(index_left)%dx_low + SP(index_left)%dx_upp)*dble(0.5)

                    if (index_right.ne.0) index_right = POINT(k,index_right,1)%sp_idx
                else
                    index_right = POINT(k, index_right, 1)%sp_idx
                    dx = (SP(index_right)%dx_low + SP(index_right)%dx_upp)*dble(0.5)
                    if (index_left.ne.0) index_left = POINT(k,index_left,1)%sp_idx
                endif

                ! lower side
                if (index_left.ne.0) then
                    if (SP(index_left)%y_low_exist) then
                        i  = SP(SP(index_left)%id_sy_upp)%cont_id
                        if (CON(i)%supply_n) then
                            CON(i)%ac_current = CON(i)%ac_current - ( &
                                &   (DN(index_left)%j_yh_ac   + DN2(index_left)%j_yh_ac) &
                                & )*dx
                        elseif (CON(i)%supply_p) then
                            CON(i)%ac_current = CON(i)%ac_current - ( &
                                &   DP(index_left)%j_yh_ac  &
                                & )*dx
                        endif
                    endif
                endif

                ! upper side
                if (index_right.ne.0) then
                    if (SP(index_right)%y_upp_exist) then
                        i  = SP(SP(index_right)%id_sy_low)%cont_id
                        if (CON(i)%supply_n) then
                            CON(i)%ac_current = CON(i)%ac_current - ( &
                                & + (DN(index_right)%j_yh_ac  + DN2(index_right)%j_yh_ac) &
                                & )*dx
                        elseif (CON(i)%supply_p) then
                            CON(i)%ac_current = CON(i)%ac_current - ( &
                                &   - DP(index_right)%j_yh_ac &
                                & )*dx
                        endif
                    endif
                endif

            endif
        enddo
    endif

    if (DD_DISPL) then
        do id=1,N_CON 
            call get_charge(id,'AC_R',sf_charge)
            if (id.eq.SOURCE) then
                CON(id)%ac_current = CON(id)%ac_current - cmplx(dble(0),sum(sf_charge)*omega,8)
            else
                CON(id)%ac_current = CON(id)%ac_current + cmplx(dble(0),sum(sf_charge)*omega,8)
            endif
            
            call get_charge(id,'AC_I',sf_charge)
            if (id.eq.SOURCE) then
                CON(id)%ac_current = CON(id)%ac_current + cmplx(sum(sf_charge)*omega,dble(0),8)
            else
                CON(id)%ac_current = CON(id)%ac_current - cmplx(sum(sf_charge)*omega,dble(0),8)
            endif
          
        enddo
    endif

endsubroutine


subroutine get_ac_inqu_current(curr_x,curr_y,curr_tun_x,dens_n,dens_n2,dens_p)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the internal AC current density and carrier density.
    !
    ! input
    ! -----
    ! curr_x : real(8),dimension(:),pointer
    !   The current density in x direction.
    ! curr_y : real(8),dimension(:),pointer
    !   The current density in y direction.
    ! curr_tun_x : real(8),dimension(:),pointer
    !   The tunnelling current density in y direction.
    ! dens_n : real(8),dimension(:),pointer
    !   The AC n type carrier density
    ! dens_n2 : real(8),dimension(:),pointer
    !   The AC n2 type carrier density
    ! dens_p : real(8),dimension(:),pointer
    !   The AC p type carrier density
    !
    ! output=input
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    complex(8),dimension(:),allocatable             :: curr_x
    complex(8),dimension(:),allocatable             :: curr_y
    complex(8),dimension(:),allocatable             :: curr_tun_x
    complex(8),dimension(:),allocatable             :: dens_n
    complex(8),dimension(:),allocatable             :: dens_n2
    complex(8),dimension(:),allocatable             :: dens_p

    integer :: n,mx,my,px,py

    if (.not.allocated(curr_x)) allocate(curr_x(N_SP))
    if (.not.allocated(curr_y)) allocate(curr_y(N_SP))
    if (.not.allocated(curr_tun_x)) allocate(curr_tun_x(N_SP))
    if (.not.allocated(dens_n)) allocate(dens_n(N_SP))
    if (.not.allocated(dens_n2)) allocate(dens_n2(N_SP))
    if (.not.allocated(dens_p)) allocate(dens_p(N_SP))

    curr_x  = 0
    curr_y  = 0
    curr_tun_x = 0
    dens_n  = 0
    dens_n2  = 0
    dens_p  = 0

    do n=1,N_SP
        ! if (SP(n)%cont) then
        !     if (SP(n)%set_qfp) then
        !         if (SP(n)%x_upp_exist) then
        !             if (DD_ELEC) then
        !                 curr_x(n) = curr_x(n)  - DN(n)%j_xh_dqf( 1)*CONT_ELEC%delta_ac(n  )&
        !                                     &- DN(n)%j_xh_dqf( 2)*CONT_ELEC%delta_ac(n+1)&
        !                                     &- DN(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n  )%id_p)&
        !                                     &- DN(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n + 1)%id_p)
                                    
        !             endif
        !             if (DD_ELEC2) then
        !                 curr_x(n) = curr_x(n)  - DN2(n)%j_xh_dqf( 1)*CONT_ELEC2%delta_ac(n  )&
        !                                     &- DN2(n)%j_xh_dqf( 2)*CONT_ELEC2%delta_ac(n+1)&
        !                                     &- DN2(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
        !                                     &- DN2(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n+1)%id_p)

        !             endif
        !             if (DD_HOLE) then
        !                 curr_x(n) = curr_x(n)  - DP(n)%j_xh_dqf(1 )*CONT_HOLE%delta_ac(n  )&
        !                                     &- DP(n)%j_xh_dqf(2 )*CONT_HOLE%delta_ac(n+1)&
        !                                     &- DP(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
        !                                     &- DP(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n+1)%id_p)
        !             endif

        !         elseif (SP(n)%x_low_exist) then
        !             if (DD_ELEC) then
        !                 curr_x(n) = curr_x(n)  - DN(n-1)%j_xh_dqf(1 )*CONT_ELEC%delta_ac(n-1)&
        !                                     &- DN(n-1)%j_xh_dqf(2 )*CONT_ELEC%delta_ac(n  )&
        !                                     &- DN(n-1)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n-1)%id_p)&
        !                                     &- DN(n-1)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n)%id_p)
        !             endif
        !             if (DD_ELEC2) then
        !                 curr_x(n) = curr_x(n)  - DN2(n-1)%j_xh_dqf(1 )*CONT_ELEC2%delta_ac(n-1)&
        !                                     &- DN2(n-1)%j_xh_dqf(2 )*CONT_ELEC2%delta_ac(n  )&
        !                                     &- DN2(n-1)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n-1)%id_p)&
        !                                     &- DN2(n-1)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n)%id_p)
        !             endif
        !             if (DD_HOLE) then
        !                 curr_x(n) = curr_x(n)  - DP(n-1)%j_xh_dqf(1 )*CONT_HOLE%delta_ac(n-1)&
        !                                     &- DP(n-1)%j_xh_dqf(2 )*CONT_HOLE%delta_ac(n  )& 
        !                                     &- DP(n-1)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n-1)%id_p)&
        !                                     &- DP(n-1)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n)%id_p)
        !             endif
        !         else
        !             ! error
        !         endif
            
        !     else
        !             if (DD_ELEC) then
        !                 curr_x(n)   = curr_x(n) - DN(n)%j_con_dqf*CONT_ELEC%delta_ac(n)&
        !                                         &- DN(n)%j_con_dpsi*CONT_POI%delta_ac(SP(n)%id_p)
        !                 curr_y(n)   = curr_y(n) - DN(n)%j_con_dqf*CONT_ELEC%delta_ac(n)&
        !                                         &- DN(n)%j_con_dpsi*CONT_POI%delta_ac(SP(n)%id_p)
        !             endif
        !             if (DD_ELEC2) then
        !                 curr_x(n)   = curr_x(n) - DN2(n)%j_con_dqf*CONT_ELEC2%delta_ac(n)&
        !                                         &- DN2(n)%j_con_dpsi*CONT_POI%delta_ac(SP(n)%id_p)
        !     endif
        !         if (DD_HOLE) then
        !             curr_x(n)   = curr_x(n) - DP(n)%j_con_dqf*CONT_HOLE%delta_ac(n)&
        !                                     &- DP(n)%j_con_dpsi*CONT_POI%delta_ac(SP(n)%id_p)
        !         endif
        !     endif

        if (SP(n)%x_upp_exist) then
            if (DD_ELEC) then
                DN(n)%j_xh_ac = + DN(n)%j_xh_dqf( 1)*CONT_ELEC%delta_ac(n  )&
                               &+ DN(n)%j_xh_dqf( 2)*CONT_ELEC%delta_ac(n+1)&
                               &+ DN(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                               &+ DN(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n+1)%id_p)
            endif
            if (DD_TN) then
                DN(n)%j_xh_ac = DN(n)%j_xh_ac + DN(n)%j_xh_dtl( 1)*CONT_TN%delta_ac(n  )&
                                             &+ DN(n)%j_xh_dtl( 2)*CONT_TN%delta_ac(n+1)
            endif
            if (DD_ELEC2) then
                DN2(n)%j_xh_ac = + DN2(n)%j_xh_dqf( 1)*CONT_ELEC2%delta_ac(n  )&
                                &+ DN2(n)%j_xh_dqf( 2)*CONT_ELEC2%delta_ac(n+1)&
                                &+ DN2(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                                &+ DN2(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n+1)%id_p)

            endif
            if (DD_HOLE) then
                DP(n)%j_xh_ac = + DP(n)%j_xh_dqf( 1)*CONT_HOLE%delta_ac(n  )&
                               &+ DP(n)%j_xh_dqf( 2)*CONT_HOLE%delta_ac(n+1)&
                               &+ DP(n)%j_xh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                               &+ DP(n)%j_xh_dpsi(2)*CONT_POI%delta_ac(SP(n+1)%id_p)
            endif
        endif
        if (STRUC_DIM.ge.2) then
            if (SP(n)%y_upp_exist) then
                py = SP(n)%id_sy_upp
                if (DD_ELEC) then
                    DN(n)%j_yh_ac = + DN(n)%j_yh_dqf( 1)*CONT_ELEC%delta_ac(n  )&
                                   &+ DN(n)%j_yh_dqf( 2)*CONT_ELEC%delta_ac(py)&
                                   &+ DN(n)%j_yh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                                   &+ DN(n)%j_yh_dpsi(2)*CONT_POI%delta_ac(SP(py)%id_p)
                endif
                if (DD_TN) then
                    DN(n)%j_yh_ac = DN(n)%j_yh_ac + DN(n)%j_yh_dtl( 1)*CONT_TN%delta_ac(n  )&
                                                 &+ DN(n)%j_yh_dtl( 2)*CONT_TN%delta_ac(py)
                endif
                if (DD_ELEC2) then
                    DN2(n)%j_yh_ac = + DN2(n)%j_yh_dqf( 1)*CONT_ELEC2%delta_ac(n  )&
                                    &+ DN2(n)%j_yh_dqf( 2)*CONT_ELEC2%delta_ac(py)&
                                    &+ DN2(n)%j_yh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                                    &+ DN2(n)%j_yh_dpsi(2)*CONT_POI%delta_ac(SP(py)%id_p)

                endif
                if (DD_HOLE) then
                    DP(n)%j_yh_ac = + DP(n)%j_yh_dqf( 1)*CONT_HOLE%delta_ac(n  )&
                                   &+ DP(n)%j_yh_dqf( 2)*CONT_HOLE%delta_ac(py)&
                                   &+ DP(n)%j_yh_dpsi(1)*CONT_POI%delta_ac(SP(n)%id_p)&
                                   &+ DP(n)%j_yh_dpsi(2)*CONT_POI%delta_ac(SP(py)%id_p)
                endif
            endif
        endif
        ! if (DD_TUNNEL) then
        !   do m=1,SNX1
        !     if (DD_ELEC) then
        !       curr_tun_x(n) = curr_tun_x(n) - JN_TUN_1D_DQF(n,m)*CONT_ELEC%delta_ac(m)&
        !                                    &- JN_TUN_1D_DPSI(n,m)*CONT_POI%delta_ac(SP(m)%id_p)
        !     endif
        !     if (DD_HOLE) then
        !       curr_tun_x(n) = curr_tun_x(n) - JP_TUN_1D_DQF(n,m)*CONT_HOLE%delta_ac(m)&
        !                                    &- JP_TUN_1D_DPSI(n,m)*CONT_POI%delta_ac(SP(m)%id_p)
        !     endif
        !   enddo
        ! endif
    enddo


    if (DD_ELEC) then
        do n=1,N_SP
            curr_x(n) = curr_x(n) + DN(n)%j_xh_ac
            curr_y(n) = curr_y(n) + DN(n)%j_yh_ac
            dens_n(n) = DN(n)%dens_dqf* (CONT_ELEC%delta_ac(n) - CONT_POI%delta_ac(SP(n)%id_p))
        enddo
    endif
    if (DD_ELEC2) then
        do n=1,N_SP
            curr_x(n)  = curr_x(n) + DN2(n)%j_xh_ac
            curr_y(n)  = curr_y(n) + DN2(n)%j_yh_ac
            dens_n2(n) = DN2(n)%dens_dqf* (CONT_ELEC2%delta_ac(n) - CONT_POI%delta_ac(SP(n)%id_p))
        enddo
    endif
    if (DD_HOLE) then
        do n=1,N_SP
            curr_x(n) = curr_x(n) + DP(n)%j_xh_ac
            curr_y(n) = curr_y(n) + DP(n)%j_yh_ac
            dens_p(n) = DP(n)%dens_dqf* (CONT_HOLE%delta_ac(n) - CONT_POI%delta_ac(SP(n)%id_p))
        enddo
    endif
 
endsubroutine

subroutine write_iv()
  !wrapper routine for save_elpa for sim_loop routine.
  !call save_elpa('_iv.elpa', iv_cols, iv_vals)
  call save_hdf5('', 'iv', iv_cols, iv_vals, .True.)
  if (US%output%elpa.ge.1) then
    call save_elpa('_iv', iv_cols, iv_vals, .False.)
  endif
endsubroutine

subroutine store_iv(niter,dpsi_norm,rpcc_norm)
    ! store the calculated DC iv characteristics in a elpa file
    integer,optional,intent(in) :: niter     !< number of iterations
    real(8),optional,intent(in) :: dpsi_norm !< norm of electrostatic and quasi fermi level potential
    real(8),optional,intent(in) :: rpcc_norm !< right hand side of dd-solution

    integer                        :: i,n
    real(8),dimension(N_CON)       :: curr_n,curr_n2,curr_p,curr_displ
    real(8)                        :: tcpu_save
    real(8)                        :: qsemi,qsemi_p,qsemi_n

    if (.not.allocated(iv_vals)) allocate(iv_vals(100,N_OP))

    ! calculate some variables for output
    call get_dc_current(curr_n,curr_n2,curr_p,curr_displ)
    curr_n  = curr_n *NORM%J_N*NORM%l_N
    curr_n2 = curr_n2*NORM%J_N*NORM%l_N
    curr_p  = curr_p *NORM%J_N*NORM%l_N

    qsemi_n = get_qsemi(.TRUE.) *NORM%l_N**SEMI_DIM*NORM%N_N
    qsemi_p = get_qsemi(.FALSE.)*NORM%l_N**SEMI_DIM*NORM%N_N
    qsemi   = qsemi_n+qsemi_p
    call calc_diff_charge_cont(Q_CONT,0)
    call cpu_time(tcpu_save)

    i = 0

    i              = i +1
    iv_cols(1)%str = 'NITER'
    iv_vals(i,OP)     = dble(niter)
    do n=1,N_CON
        if (n.eq.TAMB_ID) cycle
        i              = i +1
        iv_cols(i)%str = 'V_'//toUpper(trim(CON(n)%name))
        iv_vals(i,OP)  = CON(n)%bias(OP)
    enddo
    do n=1,N_CON
        if (n.eq.TAMB_ID) cycle
        i              = i +1
        iv_cols(i)%str = 'I_'//toUpper(trim(CON(n)%name))
        iv_vals(i,OP)  = CON(n)%current*NORM%J_N
    enddo

    do n=1,N_CON
        if (n.eq.TAMB_ID) cycle
        if (CON(n)%injection) then
            i              = i +1
            iv_cols(i)%str = 'I_'//toUpper(trim(CON(n)%name))//'|ELECTRONS'
            iv_vals(i,OP)  = curr_n(n) + curr_n2(n)
        endif
    enddo
    do n=1,N_CON
        if (n.eq.TAMB_ID) cycle
        if (CON(n)%injection) then
            i              = i +1
            iv_cols(i)%str = 'I_'//toUpper(trim(CON(n)%name))//'|HOLES'
            iv_vals(i,OP)  = curr_p(n)
        endif
    enddo

    i              = i +1
    iv_cols(i)%str = 'T_LATTICE'
    iv_vals(i,OP)  = TEMP_LATTICE

    i              = i +1
    iv_cols(i)%str = 'Q|CHANNEL'
    iv_vals(i,OP)  = qsemi

    i              = i +1
    iv_cols(i)%str = 'Q|CHANNEL|ELECTRONS'
    iv_vals(i,OP)  = qsemi_n

    i              = i +1
    iv_cols(i)%str = 'Q|CHANNEL|HOLES'
    iv_vals(i,OP)  = qsemi_p

    do n=1,N_CON
        i              = i + 1
        iv_cols(i)%str = 'Q_'//toUpper(trim(CON(n)%name))
        iv_vals(i,OP)  = Q_CONT(n)
    enddo

    i                 = i +1
    iv_cols(i)%str    = 'DPSINORM'
    iv_vals(i,OP)     = dpsi_norm

    i                 = i +1
    iv_cols(i)%str    = 'DRHSNORM'
    iv_vals(i,OP)     = rpcc_norm

    i                 = i +1
    iv_cols(i)%str    = 't_cpu'
    iv_vals(i,OP)     = tcpu_save-TCPU_OP

endsubroutine

subroutine save_inqu()
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! store the internal quantities in an elpa file
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer                            :: i,n,iv,id,k,l,sp_id
    type(string)                       :: inqu_cols(100)       !columns of inqu file
    real(8),dimension(:,:),allocatable :: inqu_vals            ! values of inqu file
    real(8),dimension(:),allocatable   :: jn_x,jp_x,jn_tun_x,jp_tun_x,jn_y,jp_y,jn2_x,jn2_y,jn2_tun_x
    real(8)                            :: grad_phi, dqf
    TYPE(DUAL_NUM)                     :: v_s,f_nonlocal
    character(len=15)                  :: str_op_rec
    type(type_band),pointer            :: band => null()

    if (.not.US%output%inqu_lev.gt.0) return !only save if user wants this

    !100xN_SP columns maximum size
    ! slow? move allocation somewhere else.
    allocate(inqu_vals(100,NXYZ))
    inqu_vals = 0

    !prepare some internal derived quantities
    call get_dc_inqu_current(jn_x,jn2_x,jp_x,jn_tun_x,jn2_tun_x,jp_tun_x,jn_y,jn2_y,jp_y)
    if (allocated(jn_x))      jn_x      = jn_x      * NORM%J_N
    if (allocated(jn2_x))     jn2_x     = jn2_x     * NORM%J_N
    if (allocated(jp_x))      jp_x      = jp_x      * NORM%J_N
    if (allocated(jn_tun_x))  jn_tun_x  = jn_tun_x  * NORM%J_N
    if (allocated(jn2_tun_x)) jn2_tun_x = jn2_tun_x * NORM%J_N
    if (allocated(jp_tun_x))  jp_tun_x  = jp_tun_x  * NORM%J_N
    if (allocated(jn_y))      jn_y      = jn_y      * NORM%J_N
    if (allocated(jn2_y))     jn2_y     = jn2_y     * NORM%J_N
    if (allocated(jp_y))      jp_y      = jp_y      * NORM%J_N
    call calc_fx
    if (SEMI_DIM.gt.1) call calc_fy

    n = 0
    do k=1,NY1
        do l=1,NX1

            sp_id = POINT(l,k,1)%sp_idx !index to point in semiconductor
            n     = n + 1 !index to point in structure
            i     = 0 !column index


            ! first all values that are defined for every point
            if (STRUC_DIM.ge.1) then
                i                = i +1
                inqu_cols(i)%str = 'X'
                inqu_vals(i,n)   = X(l)*NORM%l_N
            endif
            if (STRUC_DIM.ge.2) then
                i                = i +1
                inqu_cols(i)%str = 'Y'
                inqu_vals(i,n)   = Y(k)*NORM%l_N
            endif
            if (STRUC_DIM.ge.3) then
                write(*,*) '***error*** Not implemented.'
                stop
            endif

            i                = i +1
            inqu_cols(i)%str = 'psi_semi'
            inqu_vals(i,n)   = CONT_POI%var(n)

            i                = i +1
            inqu_cols(i)%str = 'PSI_BUILDIN'
            inqu_vals(i,n)   = PSI_BUILDIN(n)

            i                = i +1
            inqu_cols(i)%str = 'eps'
            inqu_vals(i,n)   = POINT(l,k,1)%eps

            ! only needed for debugging sometimes:
            ! i                = i +1
            ! inqu_cols(i)%str = 'box'
            ! inqu_vals(i,n)   = POINT(l,k,1)%box_id

            if (sp_id.gt.0) then
                i                = i + 1
                inqu_cols(i)%str = 'NNET'
                inqu_vals(i,n)   = CD(sp_id)*NORM%N_N
            else
                if (POINT(l,k,1)%cont) then
                    i                = i + 1
                    inqu_cols(i)%str = 'NNET'
                    inqu_vals(i,n)   = 1e20*NORM%N_N
                else !oxide
                    i                = i + 1
                    inqu_cols(i)%str = 'NNET'
                    inqu_vals(i,n)   = 0
                endif
            endif

            if (sp_id.gt.0) then

                i                = i +1
                inqu_cols(i)%str = 'semi_id'
                inqu_vals(i,n)   = SP(sp_id)%model_id

                if (DD_ELEC) then
                    i                = i +1
                    inqu_cols(i)%str = 'EC'
                    id               = SP(sp_id)%model_id
                    iv               = get_conduction_band_index(SEMI(id))
                    inqu_vals(i,n)   = -PSI_SEMI(sp_id) - UN(sp_id) + SEMI(id)%band_pointer(iv)%band%e0
                endif
                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'EC2'
                    id             = SP(sp_id)%model_id
                    iv             = get_conduction_band_index2(SEMI(id))
                    inqu_vals(i,n) = -PSI_SEMI(sp_id) - UN2(sp_id) + SEMI(id)%band_pointer(iv)%band%e0
                endif
                if (DD_HOLE) then
                    i                = i +1
                    inqu_cols(i)%str = 'EV'
                    id             = SP(sp_id)%model_id
                    iv             = get_valence_band_index(SEMI(id))
                    inqu_vals(i,n) = -PSI_SEMI(sp_id) - UP(sp_id) - SEMI(id)%band_pointer(iv)%band%e0
                endif

                if (DD_ELEC) then
                    i                = i +1
                    inqu_cols(i)%str = 'UN'
                    inqu_vals(i,n)   = UN(sp_id)

                    i                = i +1
                    inqu_cols(i)%str = 'EC0'
                    inqu_vals(i,n)   = EC0(sp_id)
                endif
                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'UN2'
                    inqu_vals(i,n)   = UN2(sp_id)
                endif
                if (DD_HOLE) then
                    i                = i +1
                    inqu_cols(i)%str = 'UP'
                    inqu_vals(i,n)   = UP(sp_id)

                    i                = i +1
                    inqu_cols(i)%str = 'EV0'
                    inqu_vals(i,n)   = EV0(sp_id)
                endif
                if (DD_ELEC) then
                    i                = i +1
                    inqu_cols(i)%str = 'EF|ELECTRONS'
                    inqu_vals(i,n)   = -CONT_ELEC%var(sp_id)
                endif  
                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'EF|ELECTRONS2'
                    inqu_vals(i,n)   = -CONT_ELEC2%var(sp_id)
                endif 
                if (DD_HOLE) then
                    i                = i +1
                    inqu_cols(i)%str = 'EF|HOLES'
                    inqu_vals(i,n)   = -CONT_HOLE%var(sp_id)
                endif  

                if (DD_CONT_BOHM_N) then
                    ! i                = i +1
                    ! inqu_cols(i)%str = 'JDGN'
                    ! inqu_vals(i,:)   = DN(:)%j_xhdg

                    i                = i +1
                    inqu_cols(i)%str = 'L_N'
                    inqu_vals(i,n)   = CONT_BOHM_N%var(sp_id)

                    ! i                = i +1
                    ! inqu_cols(i)%str = 'R_N'
                    ! inqu_vals(i,:)   = CONT_BOHM_N%rhs(:)
                endif  

                i                = i +1
                inqu_cols(i)%str = 'BGN'
                inqu_vals(i,n)   = BGN_SEMI(sp_id)

                i                = i + 1
                inqu_cols(i)%str = 'NI'
                inqu_vals(i,n)   = sqrt(NI_SQ(sp_id)*NORM%N_N*NORM%N_N)

                i                = i + 1
                inqu_cols(i)%str = 'N'
                inqu_vals(i,n)   = CN(sp_id)*NORM%N_N

                i                = i + 1
                inqu_cols(i)%str = 'N2'
                inqu_vals(i,n)   = CN2(sp_id)*NORM%N_N

                i                = i + 1
                inqu_cols(i)%str = 'P'
                inqu_vals(i,n)   = CP(sp_id)*NORM%N_N

                i                = i + 1
                inqu_cols(i)%str = 'ACC'
                inqu_vals(i,n)   = ACCEPTORS(sp_id)*NORM%N_N

                i                = i + 1
                inqu_cols(i)%str = 'DON'
                inqu_vals(i,n)   = DONATORS(sp_id)*NORM%N_N

                i                = i + 1
                inqu_cols(i)%str = 'MOL'
                inqu_vals(i,n)   = GRADING(sp_id)

                i                = i + 1
                inqu_cols(i)%str = 'MOL2'
                inqu_vals(i,n)   = GRADING2(sp_id)

                i                = i + 1
                inqu_cols(i)%str = 'STRAIN'
                inqu_vals(i,n)   = STRAIN(sp_id)

                i                = i +1
                inqu_cols(i)%str = 'J|XDIR'
                inqu_vals(i,n)   = jn_x(sp_id)  + jp_x(sp_id) + jn2_x(sp_id) + jn_tun_x(sp_id) + jn2_tun_x(sp_id) + jp_tun_x(sp_id)

                i                = i +1
                inqu_cols(i)%str = 'J|N|XDIR'
                inqu_vals(i,n)   = jn_x(sp_id)  + jn_tun_x(sp_id) 

                i                = i +1
                inqu_cols(i)%str = 'J|N2|XDIR'
                inqu_vals(i,n)   = jn2_x(sp_id) +  jn2_tun_x(sp_id)

                i                = i +1
                inqu_cols(i)%str = 'J|P|XDIR'
                inqu_vals(i,n)   = jp_x(sp_id) + jp_tun_x(sp_id)

                if (SEMI_DIM.gt.1) then
                    i                = i +1
                    inqu_cols(i)%str = 'J|YDIR'
                    inqu_vals(i,n)   = jn_y(sp_id) + jp_y(sp_id) + jn2_y(sp_id)

                    i                = i +1
                    inqu_cols(i)%str = 'J|N|YDIR'
                    inqu_vals(i,n)   = jn_y(sp_id) 

                    i                = i +1
                    inqu_cols(i)%str = 'J|N2|YDIR'
                    inqu_vals(i,n)   = jn2_y(sp_id)

                    i                = i +1
                    inqu_cols(i)%str = 'J|P|YDIR'
                    inqu_vals(i,n)   = jp_y(sp_id) 
                endif

                if (NL_ACTIVATED) then
                    i                = i +1
                    inqu_cols(i)%str = 'NL'
                    inqu_vals(i,n)   = NL_FACTOR(sp_id)

                endif

                !effective electron densiy of states
                if (DD_ELEC) then
                    i = i + 1
                    inqu_cols(i)%str = 'n_eff'
                    id             = SP(sp_id)%model_id
                    band          => SEMI(id)%band_pointer(CONT_ELEC%valley)%band
                    if (band%pos_dep_n_eff_output(sp_id).ne.0) then
                        inqu_vals(i,n) = band%pos_dep_n_eff_output(sp_id)
                    else
                        inqu_vals(i,n) = band%pos_dep_n_eff(sp_id)*VT_LATTICE**(dble(band%dim)/dble(2))
                    endif
                endif

                !effective electron densiy of states (upper band)
                if (DD_ELEC2) then
                    i = i + 1
                    inqu_cols(i)%str = 'n_eff2'
                    id             = SP(sp_id)%model_id
                    band          => SEMI(id)%band_pointer(CONT_ELEC2%valley)%band
                    if (band%pos_dep_n_eff_output(sp_id).ne.0) then
                        inqu_vals(i,n) = band%pos_dep_n_eff_output(sp_id)
                    else
                        inqu_vals(i,n) = band%pos_dep_n_eff(sp_id)*VT_LATTICE**(dble(band%dim)/dble(2))
                    endif
                endif

                !effective hole densiy of states 
                if (DD_HOLE) then
                    i = i + 1
                    inqu_cols(i)%str = 'p_eff'
                    id             = SP(sp_id)%model_id
                    band          => SEMI(id)%band_pointer(CONT_HOLE%valley)%band
                    if (band%pos_dep_n_eff_output(sp_id).ne.0) then
                        inqu_vals(i,n) = band%pos_dep_n_eff_output(sp_id)
                    else
                        inqu_vals(i,n) = band%pos_dep_n_eff(sp_id)*VT_LATTICE**(dble(band%dim)/dble(2))
                    endif
                endif

                !band degeneracy factors g for every band
                if (DD_ELEC) then
                    i = i + 1
                    inqu_cols(i)%str = 'gn'
                    id             = SP(sp_id)%model_id
                    inqu_vals(i,n) = get_degeneracy_factor(SEMI(id)%band_pointer(CONT_ELEC%valley)%band, CN(sp_id)*NORM%N_N, TEMP_LATTICE, sp_id)
                endif
                if (DD_ELEC2) then
                    i = i + 1
                    inqu_cols(i)%str = 'gn2'
                    id             = SP(sp_id)%model_id
                    inqu_vals(i,n) = get_degeneracy_factor(SEMI(id)%band_pointer(CONT_ELEC2%valley)%band, CN2(sp_id)*NORM%N_N, TEMP_LATTICE, sp_id)
                endif
                if (DD_HOLE) then
                    i = i + 1
                    inqu_cols(i)%str = 'gp'
                    id             = SP(sp_id)%model_id
                    inqu_vals(i,n) = get_degeneracy_factor(SEMI(id)%band_pointer(CONT_HOLE%valley)%band, CP(sp_id)*NORM%N_N, TEMP_LATTICE, sp_id)
                endif

                i                = i +1
                inqu_cols(i)%str = 'f_x'
                inqu_vals(i,n)   = FX(sp_id)*NORM%l_N_inv

                if (SEMI_DIM.gt.1) then
                    i                = i +1
                    inqu_cols(i)%str = 'f_y'
                    inqu_vals(i,n)   = FY(sp_id)*NORM%l_N_inv
                endif

                i                = i +1
                inqu_cols(i)%str = 'RHO'
                inqu_vals(i,n)   = Q*(CD(sp_id) - CN(sp_id) - CN2(sp_id) + CP(sp_id) )*NORM%N_N


            !   if (US%output%inqu_lev.ge.2) then
            !     do iv=1,SEMI(1)%n_band
            !       i                = i +1
            !       inqu_cols(i)%str = 'E_'//trim(SEMI(1)%band_pointer(iv)%band%name)

            !       if (SEMI(id)%band_pointer(iv)%band%iscb) then
            !         if (iv.eq.get_conduction_band_index(SEMI(id))) then
            !           do n=1,N_SP
            !             id                 = SP(n)%model_id
            !             inqu_vals(i,n)     =  -PSI_SEMI(n) - UN(n)  + SEMI(id)%band_pointer(iv)%band%e0
            !           enddo
            !         elseif (iv.eq.get_conduction_band_index2(SEMI(id))) then
            !           do n=1,N_SP
            !             id                 = SP(n)%model_id
            !             inqu_vals(i,n)     =  -PSI_SEMI(n) - UN2(n)  + SEMI(id)%band_pointer(iv)%band%e0
            !           enddo
            !         endif
            !       else
            !         do n=1,N_SP
            !           id                 = SP(n)%model_id
            !           inqu_vals(i,n)     = -PSI_SEMI(n) + UP(n) + SEMI(id)%band_pointer(iv)%band%e0 
            !         enddo
            !       endif

            !     enddo

                if (DD_ELEC) then
                    i                = i + 1
                    inqu_cols(i)%str = 'PHI|N'
                    inqu_vals(i,n)   = CONT_ELEC%var(sp_id)
                endif
                if (DD_ELEC2) then
                    i                = i + 1
                    inqu_cols(i)%str = 'PHI|N2'
                    inqu_vals(i,n)   = CONT_ELEC2%var(sp_id)
                endif
                if (DD_HOLE) then
                    i                = i + 1
                    inqu_cols(i)%str = 'PHI|P'
                    inqu_vals(i,n)   = CONT_HOLE%var(sp_id)
                endif
                if (DD_TN) then
                    i                = i + 1
                    inqu_cols(i)%str = 'TN'
                    inqu_vals(i,n)   = CONT_TN%var(sp_id)

                    i                = i +1
                    inqu_cols(i)%str = 'WN'
                    inqu_vals(i,n)   = dble(1.5)* CONT_TN%var(sp_id) * BK / Q

                    i                = i + 1
                    inqu_cols(i)%str = 'H'
                    inqu_vals(i,n)   = JouleHeat(sp_id)

                    i                = i + 1
                    inqu_cols(i)%str = 'TAUE'
                    inqu_vals(i,n)   = TAUE(sp_id)

                    i                = i + 1
                    inqu_cols(i)%str = 'SN'
                    inqu_vals(i,n)   = DN(n)%s_xh
                endif

                i                = i + 1
                inqu_cols(i)%str = 'GPSI'
                inqu_vals(i,n)   = GPSI(sp_id)

                ! gradient quasi Fermi Level
                if (DD_ELEC) then
                    i                = i + 1
                    inqu_cols(i)%str = 'GPHI|N'
                    if (SP(sp_id)%x_upp_exist.and.SP(sp_id)%x_low_exist) then
                        ! upper derivative
                        ! dqf              = CONT_ELEC%var(sp_id+1) - CONT_ELEC%var(sp_id)
                        ! grad_phi         = dqf/SP(sp_id)%dx_upp

                        ! lower derivative
                        ! dqf              = CONT_ELEC%var(sp_id) - CONT_ELEC%var(sp_id-1)
                        ! grad_phi         = 0.5 * grad_phi + 0.5 * dqf/SP(sp_id)%dx_low
                        if (SP(sp_id)%heterojunction.or.SP(sp_id + 1)%heterojunction.or.SP(sp_id - 1)%heterojunction) then
                            grad_phi = 0
                        else
                            grad_phi = (CONT_ELEC%var(sp_id + 1) - CONT_ELEC%var(sp_id-1)) / (SP(sp_id)%dx_upp + SP(sp_id)%dx_low)
                        endif

                    elseif (SP(sp_id)%x_upp_exist) then
                        dqf              = CONT_ELEC%var(sp_id+1) - CONT_ELEC%var(sp_id)
                        grad_phi         = dqf/SP(sp_id)%dx_upp
                    else
                        dqf              = CONT_ELEC%var(sp_id) - CONT_ELEC%var(sp_id-1)
                        grad_phi         = dqf/SP(sp_id)%dx_low
                    endif

                    inqu_vals(i,n)     = grad_phi*NORM%l_N_inv

                    if (NL_ACTIVATED) then
                        i                = i +1
                        inqu_cols(i)%str = 'TN'

                        if (SP(sp_id)%heterojunction) then 
                            inqu_vals(i,n)   = TEMP_LATTICE 
                        else
                            inqu_vals(i,n)   = TEMP_LATTICE + US%nonlocal%lambda * NL_FACTOR(sp_id) * grad_phi * NORM%l_N_inv * dble(2) * Q / dble(5) / BK
                        endif
                    endif

                    if (NL_ACTIVATED) then
                        i                = i +1
                        inqu_cols(i)%str = 'WN'
                        f_nonlocal       = get_nl_field(DUAL_NUM(grad_phi,0), sp_id)
                        inqu_vals(i,n)   = dble(1.5)* (TEMP_LATTICE + US%nonlocal%lambda * f_nonlocal%x_ad_ * NORM%l_N_inv * dble(2) * Q / dble(5) / BK) * BK / Q
                    endif
                endif

                if (DD_ELEC2) then
                    i                = i + 1
                    inqu_cols(i)%str = 'GPHI|N2'
                    if (SP(sp_id)%x_upp_exist) then
                        dqf              = CONT_ELEC2%var(sp_id+1) - CONT_ELEC2%var(sp_id)
                        grad_phi         = dqf/SP(sp_id)%dx_upp
                    else
                        dqf              = CONT_ELEC2%var(sp_id) - CONT_ELEC2%var(sp_id-1)
                        grad_phi         = dqf/SP(sp_id)%dx_low
                    endif
                    inqu_vals(i,n)     = grad_phi*NORM%l_N_inv
                endif

                ! !will not work like this for HBTs
                ! if (DD_ELEC) then
                !   i                = i +1
                !   inqu_cols(i)%str = 'psi_tun_n'
                !   inqu_vals(i,:)   = -DN(:)%j_xh/G0*(dble(1)+DD_DQF_TUN*exp((CONT_ELEC%var(:)-PSI_SEMI(:)+SEMI(1)%band_pointer(1)%band%e0)/VT_LATTICE))*0.5
                ! endif
                ! if (DD_HOLE) then
                !   i                = i +1
                !   inqu_cols(i)%str = 'psi_tun_p'
                !   inqu_vals(i,:)   = -DP(:)%j_xh/G0*(dble(1)+DD_DQF_TUN*exp((CONT_HOLE%var(:)-PSI_SEMI(:)-SEMI(1)%band_pointer(1)%band%e0)/VT_LATTICE))*0.5
                ! endif


            !   ! do iv=1,SEMI(1)%n_band
            !   !   if (SEMI(1)%band_pointer(iv)%band%iscb) then
            !   !     call write_colnames(DD_COL_INQU,'n_'//trim(SEMI(1)%band_pointer(iv)%band%name))
            !   !     i                = i +1
            !   !     inqu_cols(i)%str = 'n_'//trim(SEMI(1)%band_pointer(iv)%band%name)
            !   !     inqu_vals(i)     = 
            !   !   else
            !   !     call write_colnames(DD_COL_INQU,'p_'//trim(SEMI(1)%band_pointer(iv)%band%name))
            !   !     i                = i +1
            !   !     inqu_cols(i)%str = 'p_'//trim(SEMI(1)%band_pointer(iv)%band%name)
            !   !     inqu_vals(i)     = 
            !   !   endif
            !   ! enddo

                ! i                = i +1
                ! inqu_cols(i)%str = 'j_tun_x'
                ! inqu_vals(i,n)   = jn_tun_x(sp_id) + jp_tun_x(sp_id)

                ! i                = i +1
                ! inqu_cols(i)%str = 'gen_n_tun'
                ! inqu_vals(i,n)   = DN(sp_id)%g_tun*NORM%R_N

                ! i                = i +1
                ! inqu_cols(i)%str = 'gen_p_tun'
                ! inqu_vals(i,n)   = DP(sp_id)%g_tun*NORM%R_N

                i                = i +1
                inqu_cols(i)%str = 'rec'
                inqu_vals(i,n)   = DN(sp_id)%recomb*NORM%R_N

                i                = i +1
                inqu_cols(i)%str = 'interv'
                inqu_vals(i,n)   = DN(sp_id)%intervalley*NORM%R_N

                i                = i +1
                inqu_cols(i)%str = 'TEMP'
                inqu_vals(i,n)   = TEMP_LATTICE

                if (DD_ELEC) then
                    i                = i +1
                    inqu_cols(i)%str = 'VSAT|N'
                    v_s              = get_vsat(SP(sp_id)%model_id,CONT_ELEC%valley,DUAL_NUM(1,0),GRADING(sp_id),ACCEPTORS(sp_id),DONATORS(sp_id)) !not needed if field dependence is turned off
                    inqu_vals(i,n)   = v_s%x_ad_
                endif

                if (DD_HOLE) then
                    i                = i +1
                    inqu_cols(i)%str = 'VSAT|P'
                    v_s              = get_vsat(SP(sp_id)%model_id,CONT_HOLE%valley,DUAL_NUM(1,0),GRADING(sp_id),ACCEPTORS(sp_id),DONATORS(sp_id)) !not needed if field dependence is turned off
                    inqu_vals(i,n)   = v_s%x_ad_
                endif

                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'VSAT|N2'
                    v_s              = get_vsat(SP(sp_id)%model_id,CONT_ELEC2%valley,DUAL_NUM(1,0),GRADING(sp_id),ACCEPTORS(sp_id),DONATORS(sp_id)) !not needed if field dependence is turned off
                    inqu_vals(i,n)   = v_s%x_ad_
                endif

                ! --> mob_[n/p]
                i                = i +1
                inqu_cols(i)%str = 'MU|N|XDIR'
                inqu_vals(i,n)   = DN(sp_id)%mob(1)

                i                = i +1
                inqu_cols(i)%str = 'MU|N2|XDIR'
                inqu_vals(i,n)   = DN2(sp_id)%mob(1)

                i                = i +1
                inqu_cols(i)%str = 'MU|P|XDIR'
                inqu_vals(i,n)   = DP(sp_id)%mob(1)

                i                = i +1
                inqu_cols(i)%str = 'V|N|XDIR'
                inqu_vals(i,n)   = DN(sp_id)%velo
                
                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'V|N2|XDIR'
                    inqu_vals(i,n)   = DN2(sp_id)%velo
                endif

                if (SEMI_DIM.ge.1) then
                    i                = i +1
                    inqu_cols(i)%str = 'MU|N|YDIR'
                    inqu_vals(i,n)   = DN(sp_id)%mob(2)

                    i                = i +1
                    inqu_cols(i)%str = 'MU|N2|YDIR'
                    inqu_vals(i,n)   = DN2(sp_id)%mob(2)

                    i                = i +1
                    inqu_cols(i)%str = 'MU|P|YDIR'
                    inqu_vals(i,n)   = DP(sp_id)%mob(2)
                elseif (SEMI_DIM.eq.3) then
                    !todo
                endif

                if (DD_ELEC) then
                    i                = i +1
                    inqu_cols(i)%str = 'rhs_n'
                    inqu_vals(i,n)   = CONT_ELEC%rhs(sp_id)*NORM%J_N
                endif

                ! if (DD_TN) then
                !   i                = i +1
                !   inqu_cols(i)%str = 'rhs_tn'
                !   inqu_vals(i,:)   = CONT_TN%rhs
                ! endif

                if (DD_ELEC2) then
                    i                = i +1
                    inqu_cols(i)%str = 'rhs_n2'
                    inqu_vals(i,n)   = CONT_ELEC2%rhs(sp_id)*NORM%J_N
                endif

                if (DD_HOLE) then
                    i                = i + 1
                    inqu_cols(i)%str = 'rhs_p'
                    inqu_vals(i,n)   = CONT_HOLE%rhs(sp_id)*NORM%J_N
                endif

            endif
        enddo
    enddo

    call value_to_string(OP_REC,str_op_rec)
    call save_hdf5('inqu', 'op'//trim(str_op_rec), inqu_cols, inqu_vals, OP_REC.eq.1)
    if (US%output%elpa.ge.1) then 
        call save_elpa('_inqu', inqu_cols, inqu_vals, .True.)
    endif

endsubroutine

! ---------------------------------------------------------------------------------------------------------------------
!> \brief initialize saving properties -> OBSOLETE, TRANSIENT JUST NOT YET IMPLEMENTED IN NEW WAY
!> \details
subroutine init_save_dd

integer :: i


DD_SV_TR(1)  = US%output%tr_elpa_lev.gt.0
DD_SV_TR(2)  = US%output%tr_inqu_lev.gt.0
DD_SV_NEWTON = US%output%newton_lev.gt.0

! --> saving
DD_ID(1) = get_file_id() ! elpa
DD_ID(2) = get_file_id() ! tr_elpa

! ------------------------------ TRANSIENT saving -------------------------------
call write_colnames(DD_COL_TR,'t',.true.)
do i=1,N_CON
  call write_colnames(DD_COL_TR,'V_'//toUpper(trim(CON(i)%name)))
enddo
do i=1,N_CON
  call write_colnames(DD_COL_TR,'I_'//toUpper(trim(CON(i)%name)))
enddo
call write_colnames(DD_COL_TR,'Q_ch')
call write_colnames(DD_COL_TR,'Q_ch_n')
call write_colnames(DD_COL_TR,'Q_ch_p')
if (CHARGE_DIM.ge.1) call write_colnames(DD_COL_TR,'Q_t_line')
if (CHARGE_DIM.ge.2) call write_colnames(DD_COL_TR,'Q_t_surface')
if (CHARGE_DIM.ge.3) call write_colnames(DD_COL_TR,'Q_t_bulk')
do i=1,N_CON
  call write_colnames(DD_COL_TR,'Q_'//toUpper(trim(CON(i)%name)))
enddo
do i=1,N_CON
  if (CON(i)%injection) then
    call write_colnames(DD_COL_TR,'I_'//toUpper(trim(CON(i)%name))//'_n')
  endif
enddo
do i=1,N_CON
  if (CON(i)%injection) then
    call write_colnames(DD_COL_TR,'I_'//toUpper(trim(CON(i)%name))//'_p')
  endif
enddo
do i=1,N_CON
  call write_colnames(DD_COL_TR,'I_'//toUpper(trim(CON(i)%name))//'_displ')
enddo

! ------------------------------ SPECTRUM saving -------------------------------
call write_colnames(DD_COL_SPECTRUM,'e',.true.)
call write_colnames(DD_COL_SPECTRUM,'barrier_id')
call write_colnames(DD_COL_SPECTRUM,'trans')
call write_colnames(DD_COL_SPECTRUM,'j_tun_x')
call write_colnames(DD_COL_SPECTRUM,'j_tun_fw_x')
call write_colnames(DD_COL_SPECTRUM,'j_tun_bw_x')
call write_colnames(DD_COL_SPECTRUM,'x_l')
call write_colnames(DD_COL_SPECTRUM,'x_r')
call write_colnames(DD_COL_SPECTRUM,'E_qf_l')
call write_colnames(DD_COL_SPECTRUM,'E_qf_r')

endsubroutine


! ---------------------------------------------------------------------------------------------------------------------
!> \brief save output of dd solver -> OBSOLETE, JUST SOME PARTS NOT YET IMPLEMENTED IN NEW WAY
!> \details
subroutine save_dd(save_type,niter,dpsi_norm,rpcc_norm,lambda)

character(len=*),intent(in) :: save_type !< save type: 'dc','tr','newton_dc','newton_tr'
integer,optional,intent(in) :: niter     !< number of iterations
real(8),optional,intent(in) :: dpsi_norm !< norm of electrostatic and quasi fermi level potential
real(8),optional,intent(in) :: rpcc_norm !< right hand side of dd-solution
real(8),optional,intent(in) :: lambda    !< damping factor

integer                        :: io
character(len=1024)            :: fdat
character(len=12)              :: type
logical                        :: inqu_save
integer                        :: i,fid,hetero_index
real(8)                        :: qsemi,qsemi_p,qsemi_n
real(8),dimension(:),allocatable   :: qtrap
real(8),dimension(N_CON)       :: curr_n,curr_n2,curr_p,curr_displ

allocate(qtrap(CHARGE_DIM))

hetero_index = 0

if (.not.OP_SAVE) return

write(type,'(A)') trim(save_type)
qsemi_n =get_qsemi(.TRUE.)
qsemi_p =get_qsemi(.FALSE.)
qsemi=qsemi_n+qsemi_p
qtrap=0!get_qtrap() !removed traps
call get_dc_current(curr_n,curr_n2,curr_p,curr_displ)

! ------------------------------------------ TRANSIENT -----------------------------------
if ((type(1:2).eq.'tr').and.DD_SV_TR(1)) then
  if (TR_IT.eq.0) then
    ! --> open FILE ------------
    fdat = FILE
    call set_filename(fdat,'op',OP_REC)
    write(fdat,'(A,A)')  trim(fdat),'_dd_transient.elpa'
    open(DD_ID(2),file=trim(fdat),status='replace',action='write',form='formatted',iostat=io)
    ! --> first line
    call write_parameter_char(DD_ID(2), 'version',VERSION)
    call write_parameter_int( DD_ID(2), 'outp_lev',US%output%tr_elpa_lev)
    write(DD_ID(2),'(A)',advance='yes') '/'
    ! --> column names
    write(DD_ID(2),'(A)') trim(DD_COL_TR)

  endif

  ! --> write data table -----------
  if (TR_IT.eq.0) then
    call write_elpa_value(DD_ID(2), dble(0))
  else
    call write_elpa_value(DD_ID(2), TR_TOT(TR_IT))
  endif
  do i=1,N_CON
    if (TR_IT.eq.0) then
      call write_elpa_value(DD_ID(2), CON(i)%bias(OP))
    else
      call write_elpa_value(DD_ID(2), CON(i)%bias(OP)+TR_OP(i,TR_IT))
    endif
  enddo
  do i=1,N_CON
    call write_elpa_value(DD_ID(2), CON(i)%current)
  enddo
  call write_elpa_value(DD_ID(2), qsemi)
  call write_elpa_value(DD_ID(2), qsemi_n)
  call write_elpa_value(DD_ID(2), qsemi_p)
  if (CHARGE_DIM.ge.1) call write_elpa_value(DD_ID(2), qtrap(1))
  if (CHARGE_DIM.ge.2) call write_elpa_value(DD_ID(2), qtrap(2))
  if (CHARGE_DIM.ge.3) call write_elpa_value(DD_ID(2), qtrap(3))
  do i=1,N_CON
    call write_elpa_value(DD_ID(2), Q_CONT(i))
  enddo
  do i=1,N_CON
    if (CON(i)%injection) then
      call write_elpa_value(DD_ID(2), curr_n(i))
    endif
  enddo
  do i=1,N_CON
    if (CON(i)%injection) then
      call write_elpa_value(DD_ID(2), curr_p(i))
    endif
  enddo
  do i=1,N_CON
    if (TR_IT.eq.0) then
      call write_elpa_value(DD_ID(2), dble(0))

    else
      call write_elpa_value(DD_ID(2), (Q_CONT(i)-Q_CONT_LAST(i))/TR_DT(TR_IT))

    endif
  enddo
  write(DD_ID(2),'(A)',advance='yes') ''

  if (TR_IT.eq.TR_NT) then
    close(DD_ID(2))
  endif
endif


if ((type(1:9).eq.'newton_dc').and.DD_SV_NEWTON) then
  inqu_save = .true.
  ! --> open file -------
  fdat = FILE
  fid  = get_file_id()
  call set_filename(fdat,'op',OP_REC)
  call set_filename(fdat,'n',niter)
  write(fdat,'(A,A)')  trim(fdat),'_inqu.elpa'
  open(fid,file=trim(fdat),status='replace',action='write',form='formatted',iostat=io)
  ! --> first line -------
  call write_parameter_char(fid, 'version',VERSION)
  do i=1,N_CON
    call write_parameter_real(fid,'V_'//toUpper(trim(CON(i)%name)),CON(i)%bias(OP))
  enddo
  call write_parameter_real(fid,'dpsi_norm',dpsi_norm)
  call write_parameter_real(fid,'drhs_norm',rpcc_norm)
  call write_parameter_real(fid,'lambda',lambda)
  write(fid,'(A)',advance='yes')  '/'
  ! --> column names
  write(fid,'(A)') trim(DD_COL_INQU)
  

elseif ((type(1:2).eq.'tr').and.DD_SV_TR(2)) then
  inqu_save = .true.
  ! --> open file -------
  fdat = FILE
  fid  = get_file_id()
  call set_filename(fdat,'op',OP_REC)
  call set_filename(fdat,'t',TR_IT,'_dd_inqu.elpa')
  open(fid,file=trim(fdat),status='replace',action='write',form='formatted',iostat=io)
  ! --> first line -------
  call write_parameter_char(fid, 'version',VERSION)
  do i=1,N_CON
    call write_parameter_real(fid,'V_'//toUpper(trim(CON(i)%name)),CON(i)%bias(OP))
  enddo
  write(fid,'(A)') '/'
  ! --> column names
  write(fid,'(A)') trim(DD_COL_INQU)

elseif ((type(1:9).eq.'newton_tr').and.DD_SV_NEWTON) then
  inqu_save = .true.
  ! --> open file -------
  fdat = FILE
  fid  = get_file_id()
  call set_filename(fdat,'op',OP_REC)
  call set_filename(fdat,'t',TR_IT)
  call set_filename(fdat,'n',niter,'_dd_inqu.elpa')
  open(fid,file=trim(fdat),status='replace',action='write',form='formatted',iostat=io)
  ! --> first line -------
  call write_parameter_char(fid, 'version',VERSION)
  do i=1,N_CON
    call write_parameter_real(fid,'V_'//toUpper(trim(CON(i)%name)),CON(i)%bias(OP))
  enddo
  call write_parameter_real(fid,'dpsi_norm',dpsi_norm)
  call write_parameter_real(fid,'drhs_norm',rpcc_norm)
  call write_parameter_real(fid,'lambda',lambda)
  write(fid,'(A)')  '/'
  ! --> column names
  write(fid,'(A)') trim(DD_COL_INQU)

endif

! ! ------------------------------------ CONT_POI%var - TR--------------------------------
! if ((type(1:2).eq.'tr').and.(US%output%tr_psi_lev.gt.0)) then
!   fdat = ''
!   call set_filename(fdat,'t',TR_IT,'_dd')
!   call save_psi(fdat)

! endif

! ! ------------------------------------ CONT_POI%var - DC--------------------------------
! if ((type(1:2).eq.'dc').and.(US%output%psi_lev.gt.0)) then
!   call save_psi('dd')
! endif

deallocate(qtrap)

endsubroutine

subroutine dd_update(tk)
    logical equilibrium
    real(8) :: tk,gphi_before,gphi_new
    integer :: j

    !check if this operating point is a thermal equilibrium point
    equilibrium=.true.
    do j=2,N_CON
        if (j.eq.TAMB_ID) cycle
        equilibrium = (get_bias(j) == get_bias(j-1)).and.EQUILIBRIUM !if equilibrium==true, we already know qfn and qfp
    enddo
    ! equilibrium = .false. !seems to not work due to numerical noise, some qf is required.

    ! update solution variables and dependent quantities
    ! -->  Update solution variables
    do j=1,size(CONTS)
        if (.not.CONTS(j)%p%active) cycle
        call CONTS(j)%p%update(tk)
    enddo

    if (equilibrium) then
        if (DD_ELEC)  CONT_ELEC%var  = 0
        if (DD_ELEC2) CONT_ELEC2%var = 0
        if (DD_HOLE)  CONT_HOLE%var  = 0
        if (DD_TL)    CONT_TL%var    = TEMP_AMBIENT
        if (DD_TN)    CONT_TN%var    = TEMP_AMBIENT
        !if (DD_CONT_BOHM_N) CONT_BOHM_N%var  = 0
    endif

    ! 1D Damping of electron QFP for DD simulation => QFP must be monotonic increasing/decreasing depending on direction of electron current flow
    if ((SEMI_DIM.eq.1).and.(.not.equilibrium)) then
        if (DD_ELEC.and.(.not.DD_TN))  then 
            !normal damping without supply contact
            if (CONT_ELEC%var(1).lt.CONT_ELEC%var(N_SP)) then
                do j=2,N_SP-1
                    if (CONT_ELEC%var(j+1).lt.CONT_ELEC%var(j)) CONT_ELEC%var(j+1) = CONT_ELEC%var(j)
                enddo
            !endif
            elseif (CONT_ELEC%var(1).gt.CONT_ELEC%var(N_SP)) then
                do j=2,N_SP-1
                    if (CONT_ELEC%var(j+1).gt.CONT_ELEC%var(j)) CONT_ELEC%var(j+1) = CONT_ELEC%var(j)
                enddo
            endif
        endif
    endif

    ! --> smooth QFN
    ! if (DD_OSZ_CUT) then
    !     do j=1,size(CONTS)
    !     if (.not.CONTS(j)%p%active) cycle
    !     call CONTS(j)%p%smooth_var()
    !     enddo
    ! endif

    !update variables that are defined in SEMI and rest of simulation domain
    call set_psi_semi

    !update quantities that depend on solution variables
    call update_temp
    call init_energylevels !depends on T_LATTICE
    ! --> electron density
    call set_density_and_current

    ! set all continuity equation
    do j=1,nvar-1
        call CONTS(j)%p%set(.false.)
    enddo
endsubroutine

! ---------------------------------------------------------------------------------------------------------------------
!> \brief deallocate all pointers of the module
!> \details
subroutine close_dd_solver

if (associated(DN))               deallocate(DN)
if (associated(DN2))              deallocate(DN2)
if (associated(DP))               deallocate(DP)
if (associated(CN0))              deallocate(CN0)
if (associated(CP0))              deallocate(CP0)
if (associated(Q_CONT))           deallocate(Q_CONT)
if (associated(Q_CONT_LAST))      deallocate(Q_CONT_LAST)
if (associated(DD_SIMUL_RHS))     deallocate(DD_SIMUL_RHS)
if (associated(DPCC))             deallocate(DPCC)
if (associated(DPCC_LAST))        deallocate(DPCC_LAST)
if (associated(DD_AC_RHS))        deallocate(DD_AC_RHS)
if (associated(CONT_POI%delta_ac))          deallocate(CONT_POI%delta_ac)
if (associated(CONT_ELEC2%delta_ac))         deallocate(CONT_ELEC2%delta_ac)
if (associated(CONT_HOLE%delta_ac))          deallocate(CONT_HOLE%delta_ac)
! if (associated(QFN_1D))           deallocate(QFN_1D)
! if (associated(QFP_1D))           deallocate(QFP_1D)
! if (associated(JN_TUN_1D))        deallocate(JN_TUN_1D)
! if (associated(JP_TUN_1D))        deallocate(JP_TUN_1D)
! if (associated(GN_TUN_1D))        deallocate(GN_TUN_1D)
! if (associated(GP_TUN_1D))        deallocate(GP_TUN_1D)
! if (associated(GN_TUN_1D_DQF))    deallocate(GN_TUN_1D_DQF)
! if (associated(GP_TUN_1D_DQF))    deallocate(GP_TUN_1D_DQF)
! if (associated(GN_TUN_1D_DPSI))   deallocate(GN_TUN_1D_DPSI)
! if (associated(GP_TUN_1D_DPSI))   deallocate(GP_TUN_1D_DPSI)
! if (associated(JN_TUN_1D_DQF))    deallocate(JN_TUN_1D_DQF)
! if (associated(JN_TUN_1D_DPSI))   deallocate(JN_TUN_1D_DPSI)
! if (associated(JP_TUN_1D_DQF))    deallocate(JP_TUN_1D_DQF)
! if (associated(JP_TUN_1D_DPSI))   deallocate(JP_TUN_1D_DPSI)

if (CONT_ELEC%active)   call CONT_ELEC%close_
if (CONT_ELEC2%active)  call CONT_ELEC2%close_
if (CONT_HOLE%active)   call CONT_HOLE%close_
if (CONT_POI%active)    call CONT_POI%close_

endsubroutine

subroutine print_dd_debug(n_iter)

type(string)                       :: jacobi_cols(100)       !columns of inqu file
real(8),dimension(:,:),allocatable :: jacobi_vals            ! values of inqu file
integer                            :: i,n_iter
character(len=200)                 :: name
character(len=5)                   :: n_iter_str

allocate(jacobi_vals(20,N_SP))
jacobi_vals=0
name = 'dd_solver_internals'
write(n_iter_str,"(I3.3)") n_iter
name = trim(name)//'_niter'//trim(n_iter_str)
name = trim(name)//'_dd_debug.elpa'

i = 0

i = i + 1
jacobi_cols(i)%str = 'X'
jacobi_vals(i,:)   = X

i = i + 1
jacobi_cols(i)%str = 'CN'
jacobi_vals(i,:)   = CN

i = i + 1
jacobi_cols(i)%str = 'CP'
jacobi_vals(i,:)   = CP

i = i + 1
jacobi_cols(i)%str = 'CN2'
jacobi_vals(i,:)   = CN2

i = i + 1
jacobi_cols(i)%str = 'DN_dphi'
jacobi_vals(i,:)   = DN(:)%dens_dqf

i = i + 1
jacobi_cols(i)%str = 'DN2_dphi'
jacobi_vals(i,:)   = DN2(:)%dens_dqf

i = i + 1
jacobi_cols(i)%str = 'DP_dphi'
jacobi_vals(i,:)   = DP(:)%dens_dqf

i = i + 1
jacobi_cols(i)%str = 'JN_xh'
jacobi_vals(i,:)   = DN(:)%j_xh

i = i + 1
jacobi_cols(i)%str = 'JN2_xh'
jacobi_vals(i,:)   = DN2(:)%j_xh

i = i + 1
jacobi_cols(i)%str = 'JP_xh'
jacobi_vals(i,:)   = DP(:)%j_xh

i = i + 1
jacobi_cols(i)%str = 'PSI_SEMI'
jacobi_vals(i,:)   = PSI_SEMI

if (DD_ELEC) then
    i = i + 1
    jacobi_cols(i)%str = 'PHI|N'
    jacobi_vals(i,:)   = CONT_ELEC%var(:)
endif

if (DD_ELEC2) then
    i = i + 1
    jacobi_cols(i)%str = 'PHI|N2'
    jacobi_vals(i,:)   = CONT_ELEC2%var(:)
endif

if (DD_HOLE) then
    i = i + 1
    jacobi_cols(i)%str = 'PHI|P'
    jacobi_vals(i,:)   = CONT_HOLE%var(:)
endif

i = i + 1
jacobi_cols(i)%str = 'L_N'
jacobi_vals(i,:)   = L_N

call save_elpa(name, jacobi_cols, jacobi_vals)

endsubroutine

subroutine normalize_conts()
    !normalize all continuity equations.
    integer :: j

    do j=1,size(CONTS)
        call CONTS(j)%p%normalize()
    enddo

endsubroutine

endmodule
