!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  hdev simulation control initialize all modules, Bias sweep, close all modules
module simulation

use read_user    , only : US

use save_global  , only : INP_ID
use save_global  , only : TCPU_OP
use save_global  , only : init_saveglobal
use save_global  , only : set_op_rec_number
use save_global  , only : close_saveglobal
use hdf_functions  , only : close_hdf_functions
use hdf_functions  , only : init_hdf_functions

use structure    , only : init_structure
use structure    , only : close_structure
use structure    , only : BULK

use bandstructure, only : init_bandstructure
use bandstructure, only : close_bandstructure

use mobility     , only : init_mobility
use mobility     , only : init_mu_lf

use recombination, only : init_recombination

use non_local    , only : init_non_local

use semiconductor, only : init_semiconductor
use semiconductor, only : close_semiconductor
use semiconductor, only : init_psi_buildin

use semiconductor_globals, only : VT_LATTICE, TEMP_LATTICE, TEMP_AMBIENT

use heterostructure, only : init_heterostructure

use contact      , only : OP
use contact      , only : N_OP
use contact      , only : OP_FIRST
use contact      , only : OP_FORCE
use contact      , only : OP_SAVE
use contact      , only : OP_FRAC
use contact      , only : OP_ADD_OFF
use contact      , only : init_contact
use contact      , only : set_bias_diff
use contact      , only : close_contact
use contact      , only : write_op_start
use contact      , only : init_energylevels
use contact      , only : CON
use contact      , only : TAMB_ID

use phys_const , only : BK,Q

use dd_solver    , only : init_dd_solver
use dd_solver    , only : dd_solve
use dd_solver    , only : close_dd_solver
use dd_solver    , only : dd_solver_write_iv => write_iv

use normalization  , only : init_normalization

!use heterostructure, only : init_heterostructure

implicit none

private

public init_simulation
public sim_loop
public simulation_end

contains

subroutine init_simulation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize simulation parameters
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call init_saveglobal
    call init_hdf_functions


    write(*,*) 'init device structure'
    call init_structure

    write(*,*) 'init band structure'
    call init_bandstructure

    write(*,*) 'init semiconductor'
    call init_semiconductor

    write(*,*) 'init contact'
    call init_contact

    !set temperatures (maybe move to init_contact)
    TEMP_AMBIENT = CON(TAMB_ID)%bias(1)
    TEMP_LATTICE = TEMP_AMBIENT
    VT_LATTICE   = TEMP_LATTICE*BK/Q

    ! from semiconductor module, but needs temperature from contact
    call init_heterostructure

    ! from semiconductor module, but needs heterostructure
    call init_psi_buildin

    ! from contact, but depends on heterostructure
    call init_energylevels

    write(*,*) 'init recombination'
    call init_recombination

    write(*,*) 'init mobility module'
    call init_mobility

    write(*,*) 'init non-local factors'
    call init_non_local

    write(*,*) 'init drift diffusion'
    call init_dd_solver

    write(*,*) 'init normalization'
    call init_normalization

    write(*,*) 'init low field mobility'
    call init_mu_lf !low field mobility

    write(*,*) ''

endsubroutine

subroutine sim_loop
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! main hdev simulation loop
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n
    real(8) :: op_step,dv_step,op_frac_conv,op_step_min,op_step_max
    logical :: conv

    ! --> OP-LOOP ----------------------------
    OP_FIRST = .true.
    conv     = .true.
    op_step  = dble(1)
    do OP=1,N_OP
        call cpu_time(TCPU_OP)
        ! --> operation point number
        call set_op_rec_number(OP)

        ! ---> set op step size -------------------------------------
        if (OP.gt.1) then
          op_frac_conv = dv_step
          
        endif
        call set_bias_diff(dv_step)
        if (OP.gt.1) then
            if (op_frac_conv.eq.dble(0)) then
                op_step = dble(1)
              
            else
                ! Markus: What does this do?
                ! op_step = op_step*op_frac_conv/dv_step
              
            endif
            
        endif
        op_frac_conv = 0
        if (dv_step.eq.dble(0)) then
            op_step_max  = US%biasdef%dv_max
            op_step_min  = US%biasdef%dv_min
          
        else
            op_step_max  = min(dble(1),max(dble(0),US%biasdef%dv_max/dv_step))
            op_step_min  = max(dble(0),min(dble(1),US%biasdef%dv_min/dv_step))
          
        endif
        OP_FORCE     = .false.

        if (OP_ADD_OFF.and.(OP.eq.1)) then
            op_step  = dble(1)
            OP_FORCE = .true.

        elseif (US%biasdef%dv_adapt.eq.0) then
            op_step  = dble(1)
            OP_FORCE = .true.

        elseif (dv_step.eq.dble(0)) then
            op_step  = dble(1)
            OP_FORCE = .true.

        elseif (BULK) then
            op_step  = dble(1)
            OP_FORCE = .true.

        elseif (op_step_max.eq.op_step_min) then
            op_step  = op_step_max
          ! OP_FORCE = .true.

        else
            op_step  = min(op_step_max,op_step*dble(2))
            op_step  = max(op_step_min,op_step)
          
        endif
        
        if (op_step.gt.dble(0.75)) then
            op_step  = dble(1)

        elseif (op_step.gt.dble(0.5)) then
            op_step  = dble(0.5)

        endif

        ! --> set first op frac
        if ((US%biasdef%zero_bias_first.gt.0).and.(OP.eq.1).and.(dv_step.gt.dble(0))) then
            OP_FRAC = 0
            
        else
            OP_FRAC = op_step
            
        endif
        ! -----------------------------------------------------------
        n         = 0
        do 
            n       = n+1
            OP_SAVE = OP_FRAC.eq.dble(1)

            ! --> init poisson equation
            ! call init_op_poisson
            ! --> user output
            call write_op_start

            ! --> Drift diffusion solver
            call dd_solve(conv)

            ! ---> set op step size -----------------------------------
            OP_FIRST    = .false.
            
            if (conv) then
                if (OP_SAVE) then
                    exit ! success

                else
                    op_frac_conv = OP_FRAC
                    OP_FRAC      = min(dble(1),op_frac_conv + op_step)
                    if (OP_FRAC.gt.(dble(1)-op_step_min*0.5)) OP_FRAC=dble(1)

                endif

            elseif (OP_FORCE) then
                exit

            elseif (n.eq.200) then
                ! last chance
                OP_FORCE = .true.
                OP_FRAC  = dble(1)

            else
                op_step = op_step*dble(0.5)
                if (op_step_min.gt.op_step) then
                    ! --> minimum voltage step size
                    op_step      = op_step_min
                    OP_FRAC      = min(dble(1),op_frac_conv + op_step)
                    op_frac_conv = OP_FRAC
                    if (OP_FRAC.gt.(dble(1)-op_step_min*0.5)) OP_FRAC=dble(1)
                    if (OP_FRAC.eq.dble(1)) OP_FORCE = .true.

                else
                    OP_FRAC      = min(dble(1),op_frac_conv + op_step)
                    if (OP_FRAC.gt.(dble(1)-op_step_min*0.5)) OP_FRAC=dble(1)

                endif
            endif
        enddo
    enddo

    ! --> dd_solver write iv 
    call dd_solver_write_iv

    ! --> user output
    call write_sim_end

endsubroutine

subroutine write_sim_end
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! output information at simulation end on console
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> output on console --------
    write(*,'(A)') '------ simulation finished ------------'
    write(*,'(A)') ''
endsubroutine

subroutine simulation_end
    !!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! close all modules
    !!!!!!!!!!!!!!!!!!!!!!!!!!!
    call close_dd_solver
    call close_contact
    call close_semiconductor
    call close_bandstructure
    call close_structure
    call close_saveglobal
    call close_hdf_functions
endsubroutine

endmodule
