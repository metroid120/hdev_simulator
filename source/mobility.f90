!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! Mobility models for drift diffusion
module mobility

USE Dual_Num_Auto_Diff

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use phys_const                                  !< Physical constants

use math_oper    , only : init_grid            !< get grid

use read_user    , only : US                   !< all parameters from user file

use save_global  , only : DEB_ID               !< debug file id
use save_global  , only : FILE                 !< output file name
use save_global  , only : FORMAT_REAL          !< output format for real numbers
use save_global  , only : VERSION              !< hdev version
use save_global  , only : SAVE_OP              !< if true: saving of material properties enabled
use save_global  , only : get_file_id          !< get new file id
use save_global  , only : write_parameter_char !< write character parameter in first elpa line
use save_global  , only : write_parameter_real !< write real parameter in first elpa line
use save_global  , only : write_colnames       !< write column name in elpa file
use save_global  , only : write_elpa_value     !< write value in elpa table
use save_global  , only : read_parameter_real  !< read real parameter in first elpa line
use save_global  , only : read_parameter_int   !< read integer parameter in first elpa line
use save_global  , only : read_parameter_char  !< read character parameter in first elpa line

use bandstructure, only : SEMI                 !< semiconductor properties

use dd_types, only : mob_model            !< semiconductor properties
use dd_types, only : tau_model            !< semiconductor properties

use heterostructure, only: bow                 !< bowing of parameters

use structure    , only : POINT
use structure    , only : N_SEMI               !< total number of semiconductor models
use structure    , only : SEMI_MOD_NAME        !< names of semiconductor models
use structure    , only : NX1, NY1, NZ1
use structure    , only : STRUC_DIM

use semiconductor, only : SDX
use semiconductor, only : SP 

use semiconductor_globals, only: TEMP_LATTICE
use semiconductor_globals, only: MU_LF

use continuity_elec   , only : CONT_ELEC
use continuity_elec2  , only : CONT_ELEC2
use continuity_hole   , only : CONT_HOLE

use profile, only: DONATORS
use profile, only: ACCEPTORS
use profile, only: GRADING
use profile, only: GRADING2
use profile, only: STRAIN

use continuity, only: DN
use continuity, only: DP
use continuity, only: DN2

use utils, only : clip                         !< clip function from utils
use dd_types, only :NORM                  
use dd_types, only :DD_TN                  
use non_local, only :get_nl_field                  

implicit none

interface get_mobility_dual 
  module procedure get_mobility_dual, get_mobility
end interface

! ---> global module parameters ---------------------------------------------------------------------------------------

private

! ---> public subroutines/ functions ----------------------------------------------------------------------------------
public init_mobility
public init_mu_lf
public get_mobility_dual
public get_mobility
public get_vsat
public lattice_mobility
public mob_model
public get_taun_dual


contains

subroutine init_mobility
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! main initialization routine of this module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    call init_mobmodel
    call init_taumodel
endsubroutine

subroutine init_taumodel
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the tau model parameters from the user input
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i,is,ib,n_semi,n_band
    logical :: found = .false.
    character(len=64) :: valley    = ''            !< valley name
    character(len=64) :: semi_name    = ''            !< semi name

    n_semi = size(SEMI)

    !iterate over all semiconductors
    do i=1,US%n_taudef
        !find user input that matches with this band
        found = .false.
        do is=1,n_semi
            n_band = size(SEMI(is)%band_pointer)
            do ib =1,n_band
                valley = SEMI(is)%band_pointer(ib)%band%valley
                semi_name = SEMI_MOD_NAME(is)
                if ((semi_name.eq.US%taudef(i)%mod_name).and.(valley.eq.US%taudef(i)%valley)) then
                    found = .true.
                    !ids
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%semi_id      = is
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%band_id      = ib

                    write(SEMI(is)%band_pointer(ib)%band%tau_parameters%mod_name,'(A)')     trim(US%taudef(i)%mod_name)
                    write(SEMI(is)%band_pointer(ib)%band%tau_parameters%valley,'(A)')       trim(US%taudef(i)%valley)
                    write(SEMI(is)%band_pointer(ib)%band%tau_parameters%tau_type,'(A)')     trim(US%taudef(i)%tau_type)

                    !logicals
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%is_michaillat  = (US%taudef(i)%tau_type.eq.'michaillat')
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%is_default     = (US%taudef(i)%tau_type.eq.'default')

                    !throw error if no model is selected
                    if (.not.(SEMI(is)%band_pointer(ib)%band%tau_parameters%is_michaillat.or.SEMI(is)%band_pointer(ib)%band%tau_parameters%is_default)) then
                        write(*,*) '***error*** unknown tau_type parameter in &TAU_E: '//SEMI(is)%band_pointer(ib)%band%tau_parameters%tau_type
                        stop
                    endif

                    !model parameters
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%t0         = US%taudef(i)%t0
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%t1         = US%taudef(i)%t1
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%c0         = US%taudef(i)%c0
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%c1         = US%taudef(i)%c1
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%c2         = US%taudef(i)%c2
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%c3         = US%taudef(i)%c3

                    !bowing
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%t0_bow         = US%taudef(i)%t0_bow
                    SEMI(is)%band_pointer(ib)%band%tau_parameters%c0_bow         = US%taudef(i)%c0_bow

                    exit

                endif
            enddo
            ! if (.not.found) then
            !     write(*,'(A)') '***error*** tau_e model: did not find matching band.'
            !     stop
            ! endif
        enddo
    enddo

endsubroutine

subroutine init_mobmodel
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the mobility model parameters from the user input
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i,is,ib,idim
    logical :: found = .false.
    real(8),dimension(3) :: z_0
    character(len=64) :: valley    = ''            !< valley identifier

    !iterate over all semiconductors
    do is=1,size(SEMI)
        do ib =1,size(SEMI(is)%band_pointer)
            !find user input that matches with this band
            found = .false.
            do i=1,US%n_mobdef
                valley = SEMI(is)%band_pointer(ib)%band%valley
                if ((SEMI_MOD_NAME(is).eq.US%mobdef(i)%mod_name).and.(valley.eq.US%mobdef(i)%valley)) then
                    found = .true.
                    !ids
                    SEMI(is)%band_pointer(ib)%band%mob%semi_id      = is
                    SEMI(is)%band_pointer(ib)%band%mob%band_id      = ib

                    write(SEMI(is)%band_pointer(ib)%band%mob%mod_name,'(A)')     trim(US%mobdef(i)%mod_name)
                    write(SEMI(is)%band_pointer(ib)%band%mob%valley,'(A)')       trim(US%mobdef(i)%valley)
                    write(SEMI(is)%band_pointer(ib)%band%mob%l_scat_type,'(A)')  trim(US%mobdef(i)%l_scat_type)
                    write(SEMI(is)%band_pointer(ib)%band%mob%li_scat_type,'(A)') trim(US%mobdef(i)%li_scat_type)
                    write(SEMI(is)%band_pointer(ib)%band%mob%hc_scat_type,'(A)') trim(US%mobdef(i)%hc_scat_type)
                    write(SEMI(is)%band_pointer(ib)%band%mob%v_sat_type,'(A)')   trim(US%mobdef(i)%v_sat_type)
                    write(SEMI(is)%band_pointer(ib)%band%mob%bowing,'(A)')       trim(US%mobdef(i)%bowing)
                    write(SEMI(is)%band_pointer(ib)%band%mob%force,'(A)')        trim(US%mobdef(i)%force)

                    !logicals
                    SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_default         = (US%mobdef(i)%l_scat_type(1:7).eq.'default')
                    SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_temp            = (US%mobdef(i)%l_scat_type(1:4).eq.'temp')
                    SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_michaillat_n    = (US%mobdef(i)%l_scat_type.eq.'michaillatn')
                    SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_michaillat_p    = (US%mobdef(i)%l_scat_type.eq.'michaillatp')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_default        = (US%mobdef(i)%li_scat_type(1:7).eq.'default')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_caughey_thomas = (US%mobdef(i)%li_scat_type(1:7).eq.'caughey')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_device         = (US%mobdef(i)%li_scat_type(1:6).eq.'device')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_reggiani       = (US%mobdef(i)%li_scat_type(1:3).eq.'reg')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_tuwien         = (US%mobdef(i)%li_scat_type(1:6).eq.'tuwien')
                    SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_cryo           = (US%mobdef(i)%li_scat_type(1:4).eq.'cryo')
                    SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_default        = (US%mobdef(i)%hc_scat_type(1:7).eq.'default')
                    SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_sat            = (US%mobdef(i)%hc_scat_type(1:3).eq.'sat')
                    SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_ndm            = (US%mobdef(i)%hc_scat_type(1:3).eq.'ndm')
                    SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_ndm2           = (US%mobdef(i)%hc_scat_type(1:3).eq.'ddm')
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat_is_default          = (US%mobdef(i)%v_sat_type(1:7).eq.'default')
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat_is_device           = (US%mobdef(i)%v_sat_type(1:6).eq.'device')
                    SEMI(is)%band_pointer(ib)%band%mob%bowing_is_michaillat      = (US%mobdef(i)%bowing(1:10).eq.'michaillat')
                    SEMI(is)%band_pointer(ib)%band%mob%bowing_is_default         = (US%mobdef(i)%bowing(1:7).eq.'default')
                    SEMI(is)%band_pointer(ib)%band%mob%is_elec                   = SEMI(is)%band_pointer(ib)%band%iscb
                    SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_psi         = (US%mobdef(i)%force(1:3).eq.'psi')
                    SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_phi         = (US%mobdef(i)%force(1:3).eq.'phi')
                    SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_mix         = (US%mobdef(i)%force(1:3).eq.'mix')
                    SEMI(is)%band_pointer(ib)%band%mob%force_is_hd               = & 
                        ((US%dd%tn.gt.0).and.(SEMI(is)%band_pointer(ib)%band%iscb))

                    !throw error if no model is selected
                    if (.not.(SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_default.or.SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_temp.or.SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_michaillat_n.or.SEMI(is)%band_pointer(ib)%band%mob%l_scat_is_michaillat_p)) then
                        write(*,*) '***error*** unknown lattice scattering model in &MOB_DEF: '//SEMI(is)%band_pointer(ib)%band%mob%l_scat_type
                        stop
                    elseif (.not.(SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_cryo.or.SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_default.or.SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_caughey_thomas.or.SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_device.or.SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_tuwien.or.SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_reggiani)) then
                        write(*,*) '***error*** unknown impurity scattering model in &MOB_DEF'//SEMI(is)%band_pointer(ib)%band%mob%li_scat_type
                        stop
                    elseif (.not.(SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_default.or.SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_sat.or.SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_ndm.or.SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_ndm2)) then
                        write(*,*) '***error*** unknown hot carrier scattering model in &MOB_DEF'//SEMI(is)%band_pointer(ib)%band%mob%hc_scat_type
                        stop
                    elseif (.not.(SEMI(is)%band_pointer(ib)%band%mob%v_sat_is_default.or.SEMI(is)%band_pointer(ib)%band%mob%v_sat_is_device)) then
                        write(*,*) '***error*** unknown saturation velocity model in &MOB_DEF'//SEMI(is)%band_pointer(ib)%band%mob%v_sat_type
                        stop
                    elseif (.not.(SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_mix.or.SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_phi.or.SEMI(is)%band_pointer(ib)%band%mob%force_is_grad_psi)) then
                        write(*,*) '***error*** unknown force in &MOB_DEF'//SEMI(is)%band_pointer(ib)%band%mob%force
                        stop
                    elseif (SEMI(is)%is_alloy) then
                        if (.not.(SEMI(is)%band_pointer(ib)%band%mob%bowing_is_default.or.SEMI(is)%band_pointer(ib)%band%mob%bowing_is_michaillat)) then
                            write(*,*) '***error*** unknown bowing model in &MOB_DEF'//SEMI(is)%band_pointer(ib)%band%mob%bowing
                            stop
                        endif
                    endif
                    !lattice scattering parameters
                    !default
                    SEMI(is)%band_pointer(ib)%band%mob%mu_L         = US%mobdef(i)%mu_L
                    !michaillat
                    SEMI(is)%band_pointer(ib)%band%mob%mu_cs        = US%mobdef(i)%mu_cs
                    SEMI(is)%band_pointer(ib)%band%mob%beta_l       = US%mobdef(i)%beta_l
                    SEMI(is)%band_pointer(ib)%band%mob%z_0          = US%mobdef(i)%z_0
                    z_0 = SEMI(is)%band_pointer(ib)%band%mob%z_0
                    !temp
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_L      = US%mobdef(i)%gamma_L

                    SEMI(is)%band_pointer(ib)%band%mob%mu_fac       = US%mobdef(i)%mu_fac

                    !impurity scattering parameters
                    !default
                    !caughey-thomas
                    SEMI(is)%band_pointer(ib)%band%mob%mu_min       = US%mobdef(i)%mu_min
                    SEMI(is)%band_pointer(ib)%band%mob%c_ref        = US%mobdef(i)%c_ref
                    SEMI(is)%band_pointer(ib)%band%mob%alpha        = US%mobdef(i)%alpha
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_1      = US%mobdef(i)%gamma_1
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_2      = US%mobdef(i)%gamma_2
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_3      = US%mobdef(i)%gamma_3
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_4      = US%mobdef(i)%gamma_4

                    SEMI(is)%band_pointer(ib)%band%mob%gamma_1_a      = US%mobdef(i)%gamma_1_a
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_2_a      = US%mobdef(i)%gamma_2_a
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_3_a      = US%mobdef(i)%gamma_3_a
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_4_a      = US%mobdef(i)%gamma_4_a
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_1_d      = US%mobdef(i)%gamma_1_d
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_2_d      = US%mobdef(i)%gamma_2_d
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_3_d      = US%mobdef(i)%gamma_3_d
                    SEMI(is)%band_pointer(ib)%band%mob%gamma_4_d      = US%mobdef(i)%gamma_4_d
                    
                    SEMI(is)%band_pointer(ib)%band%mob%gamma        = US%mobdef(i)%gamma
                    SEMI(is)%band_pointer(ib)%band%mob%c            = US%mobdef(i)%c

                    !regianni
                    SEMI(is)%band_pointer(ib)%band%mob%c_ref_a      = US%mobdef(i)%c_ref_a
                    SEMI(is)%band_pointer(ib)%band%mob%c_ref_d      = US%mobdef(i)%c_ref_d
                    SEMI(is)%band_pointer(ib)%band%mob%mu_min_a     = US%mobdef(i)%mu_min_a
                    SEMI(is)%band_pointer(ib)%band%mob%mu_min_d     = US%mobdef(i)%mu_min_d
                    SEMI(is)%band_pointer(ib)%band%mob%alpha_a      = US%mobdef(i)%alpha_a
                    SEMI(is)%band_pointer(ib)%band%mob%alpha_d      = US%mobdef(i)%alpha_d
                    !device
                    SEMI(is)%band_pointer(ib)%band%mob%alpha_2      = US%mobdef(i)%alpha_2
                    SEMI(is)%band_pointer(ib)%band%mob%c_ref_2      = US%mobdef(i)%c_ref_2
                    SEMI(is)%band_pointer(ib)%band%mob%mu_hd        = US%mobdef(i)%mu_hd
                    SEMI(is)%band_pointer(ib)%band%mob%r            = US%mobdef(i)%r

                    !hot carrier scattering parameters
                    !default
                    !sat
                    SEMI(is)%band_pointer(ib)%band%mob%beta         = US%mobdef(i)%beta
                    !ndm
                    SEMI(is)%band_pointer(ib)%band%mob%f0           = US%mobdef(i)%f0 
                    SEMI(is)%band_pointer(ib)%band%mob%f1           = US%mobdef(i)%f1 
                    SEMI(is)%band_pointer(ib)%band%mob%beta1         = US%mobdef(i)%beta1
                    SEMI(is)%band_pointer(ib)%band%mob%beta2         = US%mobdef(i)%beta2
                    SEMI(is)%band_pointer(ib)%band%mob%beta3         = US%mobdef(i)%beta3
                    !ddm
                    SEMI(is)%band_pointer(ib)%band%mob%mu_HC        = US%mobdef(i)%mu_HC 

                    !error if beta=0 as this causes numerical overflow
                    if (SEMI(is)%band_pointer(ib)%band%mob%hc_scat_is_sat) then
                        if (SEMI(is)%band_pointer(ib)%band%mob%beta.lt.1e-5) then
                            write(*,*) '***error*** hot-carrier scattering model activated, but beta=0. Numerically not possible! '
                            stop
                        endif
                    endif

                    !saturation velocity parameters
                    !default
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat        = US%mobdef(i)%v_sat
                    SEMI(is)%band_pointer(ib)%band%mob%c_ref_vsat   = US%mobdef(i)%c_ref_vsat
                    SEMI(is)%band_pointer(ib)%band%mob%al_vsat      = US%mobdef(i)%al_vsat
                    SEMI(is)%band_pointer(ib)%band%mob%A            = US%mobdef(i)%A
                    !device
                    SEMI(is)%band_pointer(ib)%band%mob%zeta_v       = US%mobdef(i)%zeta_v
                    !driving force
                    SEMI(is)%band_pointer(ib)%band%mob%fac_driving_force       = US%mobdef(i)%fac_driving_force
                    !bowing
                    !default
                    SEMI(is)%band_pointer(ib)%band%mob%mu_bow       = US%mobdef(i)%mu_bow
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat_bow    = US%mobdef(i)%v_sat_bow
                    !michaillat
                    SEMI(is)%band_pointer(ib)%band%mob%mu_bow1      = US%mobdef(i)%mu_bow1
                    SEMI(is)%band_pointer(ib)%band%mob%mu_bow2      = US%mobdef(i)%mu_bow2
                    SEMI(is)%band_pointer(ib)%band%mob%mu_bowy      = US%mobdef(i)%mu_bowy
                    SEMI(is)%band_pointer(ib)%band%mob%mu_bowxy     = US%mobdef(i)%mu_bowxy
                    SEMI(is)%band_pointer(ib)%band%mob%chi          = US%mobdef(i)%chi
                    SEMI(is)%band_pointer(ib)%band%mob%z_1          = US%mobdef(i)%z_1
                    !device
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat_a1     = US%mobdef(i)%v_sat_a1
                    SEMI(is)%band_pointer(ib)%band%mob%v_sat_a2     = US%mobdef(i)%v_sat_a2
                    SEMI(is)%band_pointer(ib)%band%mob%mu_min_a1    = US%mobdef(i)%mu_min_a1
                    SEMI(is)%band_pointer(ib)%band%mob%mu_min_a2    = US%mobdef(i)%mu_min_a2
                    SEMI(is)%band_pointer(ib)%band%mob%mu_L_a1      = US%mobdef(i)%mu_L_a1
                    SEMI(is)%band_pointer(ib)%band%mob%mu_L_a2      = US%mobdef(i)%mu_L_a2

                    !cryo model 
                    SEMI(is)%band_pointer(ib)%band%mob%mu_max  = US%mobdef(i)%mu_max
                    SEMI(is)%band_pointer(ib)%band%mob%meff    = US%mobdef(i)%meff
                    SEMI(is)%band_pointer(ib)%band%mob%f_e     = US%mobdef(i)%f_e
                    SEMI(is)%band_pointer(ib)%band%mob%f_h     = US%mobdef(i)%f_h
                    SEMI(is)%band_pointer(ib)%band%mob%f_CW    = US%mobdef(i)%f_CW
                    SEMI(is)%band_pointer(ib)%band%mob%f_bh    = US%mobdef(i)%f_bh
                    SEMI(is)%band_pointer(ib)%band%mob%ag      = US%mobdef(i)%ag
                    SEMI(is)%band_pointer(ib)%band%mob%bg      = US%mobdef(i)%bg
                    SEMI(is)%band_pointer(ib)%band%mob%cg      = US%mobdef(i)%cg
                    SEMI(is)%band_pointer(ib)%band%mob%gammag  = US%mobdef(i)%gammag
                    SEMI(is)%band_pointer(ib)%band%mob%betag   = US%mobdef(i)%betag
                    SEMI(is)%band_pointer(ib)%band%mob%alphag  = US%mobdef(i)%alphag
                    SEMI(is)%band_pointer(ib)%band%mob%alphag2 = US%mobdef(i)%alphag2

                endif
            enddo
            if (.not.found) then
                write(*,'(A)') '***error*** mobility model not specified for all valleys.'
                stop
            endif
        enddo
    enddo

    !extend multi-dimensional input to more than one dimension
    do is=1,size(SEMI)
        do ib =1,size(SEMI(is)%band_pointer)
            if (SEMI(is)%is_alloy) then 
                cycle !mu_l from alloy is created by bowing
            endif
            if (SEMI(is)%band_pointer(ib)%band%mob%mu_l(1).eq.0) then
                write(*,'(A)') '***error*** mu_l in x direction equals zero.'
                stop
            endif
            do idim = 2,3
                if (SEMI(is)%band_pointer(ib)%band%mob%mu_cs(idim).eq.0 ) then
                    SEMI(is)%band_pointer(ib)%band%mob%mu_cs(idim)  = SEMI(is)%band_pointer(ib)%band%mob%mu_cs(1)
                endif
                if (SEMI(is)%band_pointer(ib)%band%mob%beta_l(idim).eq.0 ) then
                    SEMI(is)%band_pointer(ib)%band%mob%beta_l(idim) = SEMI(is)%band_pointer(ib)%band%mob%beta_l(1)
                endif
                ! if (SEMI(is)%band_pointer(ib)%band%mob%z_0(idim).eq.0 ) then
                !     SEMI(is)%band_pointer(ib)%band%mob%z_0(idim)    = SEMI(is)%band_pointer(ib)%band%mob%z_0(1)
                ! endif
                if (SEMI(is)%band_pointer(ib)%band%mob%mu_l(idim).eq.0 ) then
                    SEMI(is)%band_pointer(ib)%band%mob%mu_l(idim)   = SEMI(is)%band_pointer(ib)%band%mob%mu_l(1)
                endif
            enddo
       enddo
    enddo

    !check if input makes sense here
    do is=1,size(SEMI)
        do ib =1,size(SEMI(is)%band_pointer)
            !c_ref equals 0 mathematically not possible in caughey_thomas model
            !alloys use model of constituents, so there it is ok
            if ((SEMI(is)%band_pointer(ib)%band%mob%c_ref.eq.0).and.(SEMI(is)%band_pointer(ib)%band%mob%li_scat_is_caughey_thomas)) then
                if (.not.SEMI(is)%is_alloy) then
                    write(*,'(A)') '***error*** mobility uses caughey-thomas li-scattering model, but parameter c_ref equals zero.'
                    stop
                endif
            endif
        enddo
    enddo

endsubroutine


function matthiesen_rule(para_a, para_b, bowing_para, grading) result (para)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !harmonic mean used to add LI mobilities for alloys
    !
    ! input
    ! -----
    ! para_a : DUAL_NUM
    !   Parameter of material A.
    ! para_b : DUAL_NUM
    !   Parameter of material B.
    ! bowing_para : real(8)
    !   Nonlinear bowing parameter.
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! para : DUAL_NUM
    !   The harmonic mean of para_a and para_b, weighted with grading.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    TYPE(DUAL_NUM) :: para_a, para_b
    real(8)        :: grading, bowing_para
    TYPE(DUAL_NUM) :: para, para_inv
    if (bowing_para.eq.0) then
        para_inv = (1-grading)/para_A + grading/para_B !- grading*(1-grading)/bowing_para
    else
        para_inv = (1-grading)/para_A + grading/para_B + grading*(1-grading)/bowing_para
    endif
    para     = 1/para_inv

endfunction

function regianni_bowing(para_a, para_b, mob, grading, grading2, stress, dim) result (para)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !harmonic mean used to add LI mobilities for alloys
    !
    ! input
    ! -----
    ! para_a : DUAL_NUM
    !   Parameter of material A.
    ! para_b : DUAL_NUM
    !   Parameter of material B.
    ! mob : mob_model
    !   Mobility model with bowing parameters.
    ! grading : real(8)
    !   X Grading at point.
    ! grading : real(8)
    !   Y grading at point.
    ! stress : real(8)
    !   Stress at point.
    ! dim : integer
    !   Transport direction
    !
    ! output
    ! ------
    ! para : DUAL_NUM
    !   The resultant mobility
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    TYPE(DUAL_NUM) :: para_a, para_b
    TYPE(mob_model):: mob
    real(8)        :: grading, grading2, stress
    TYPE(DUAL_NUM) :: para, para_inv
    integer        :: dim

    real(8)        :: nenner_1, nenner_2, nenner_3, chi_fun, chi
    nenner_1 = mob%mu_bow + mob%mu_bow1*grading + mob%mu_bow2*grading**2
    nenner_2 = mob%mu_bowy 
    nenner_3 = mob%mu_bowxy 

    para_inv = 0
    ! first assemble big bracket
    if (nenner_1.ne.0) then
        para_inv = para_inv + (1-grading) * grading   / nenner_1
    endif
    if (nenner_2.ne.0) then
        para_inv = para_inv + (1-grading2) * grading2 / nenner_2
    endif
    if (nenner_3.ne.0) then
        para_inv = para_inv + grading*grading2        / nenner_3
    endif

    !chi function
    if (stress.eq.0) then
        chi_fun = dble(1)
    else
        if (stress.gt.0) then
            chi = mob%chi(dim)
        else 
            chi = mob%chi(dim+1)
        endif
        chi_fun = chi - ( chi - 1 ) * exp(-abs(stress)/mob%z_1)
    endif
    para_inv = para_inv*1/chi_fun

    ! add linear bowing
    para_inv = para_inv + (1-grading)/para_A + grading/para_B

    para     = 1/para_inv

endfunction


function caughey_thomas(mob,mu_L,impurity,t) result(mu_LI)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! caughey thomas mobility model
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility parameters.
    ! mu_L : DUAL_NUM
    !   Lattice mobility at point in semiconductor.
    ! impurity : real(8)
    !   Impurity concentration at point in semiconductor.
    ! t : real(8)
    !   Temperature at point in semiconductor.
    !
    ! result
    ! ------
    ! mu_LI : DUAL_NUM
    !   Caughey thomas mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob      !mobility parameters
    TYPE(DUAL_NUM)  :: mu_L     !lattice mobility
    real(8)         :: impurity !impurity concentration
    TYPE(DUAL_NUM)  :: t        !temperature in kelvin

    real(8)         :: mu_min
    real(8)         :: impurity_ref
    real(8)         :: alpha
    TYPE(DUAL_NUM)  :: mu_LI,alpha_temp,impurity_ref_temp,mu_min_t

    mu_min = mob%mu_min
    if (t.gt.200) then
        mu_min_t = mu_min*(T/300.)**mob%gamma_1
    else
        mu_min_t = mu_min*(T/200.)**mob%gamma_2  *(2./3.)**mob%gamma_1
    endif

    impurity_ref      = mob%c_ref
    impurity_ref_temp = impurity_ref*(T/300.)**mob%gamma_3

    alpha             = mob%alpha
    alpha_temp        = alpha*(t/300.)**mob%gamma_4

    impurity          = impurity + NORM%N_N_inv !very small concentration to preven 0**x
    mu_LI             = mu_min_t + (mu_L-mu_min_t)/( 1 + (impurity/impurity_ref_temp)**alpha_temp )

endfunction

function regianni(mob,mu_L,na_i,nd_i,t) result(mu_LI)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! regianni mobility dependence model
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility parameters.
    ! mu_L : DUAL_NUM
    !   Lattice mobility at point in semiconductor.
    ! na_i : real(8)
    !   Acceptor concentration at point in semiconductor.
    ! nd_i : real(8)
    !   Donor concentration at point in semiconductor.
    ! t : real(8)
    !   Temperature at point in semiconductor.
    !
    ! result
    ! ------
    ! mu_LI : DUAL_NUM
    !   regianni mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob      !mobility parameters
    TYPE(DUAL_NUM)  :: mu_L     !lattice mobility
    real(8)         :: na_i     !impurity concentration acceptors
    real(8)         :: nd_i     !donor concentration acceptors
    TYPE(DUAL_NUM)  :: t        !temperature in kelvin

    real(8)         :: alpha_a, alpha_d, c_ref_a, c_ref_d
    TYPE(DUAL_NUM)  :: mu_LI, alpha_a_temp,  alpha_d_temp, c_ref_a_temp, c_ref_d_temp, mu_L_temp, mu_min_t

    na_i              = na_i + NORM%N_N_inv !very small concentration to prevent 0**x
    nd_i              = nd_i + NORM%N_N_inv !very small concentration to prevent 0**x
    alpha_a           = mob%alpha_a
    alpha_a_temp      = alpha_a*(t/300.)**mob%gamma_4_a
    alpha_d           = mob%alpha_d
    alpha_d_temp      = alpha_d*(t/300.)**mob%gamma_4_d
    c_ref_a           = mob%c_ref_a
    c_ref_d           = mob%c_ref_d
    c_ref_a_temp      = c_ref_a*(T/300.)**mob%gamma_3_a
    c_ref_d_temp      = c_ref_d*(T/300.)**mob%gamma_3_d
    mu_L_temp         = mu_L*(T/300.)**(-mob%gamma + mob%c*(T/300.))
    !mu_min            = ( mob%mu_min_a*(T/300)**mob%gamma_1 * na_i + mob%mu_min_d*(T/300)**mob%gamma_2 * nd_i ) / ( na_i + nd_i )
    if ((mob%gamma_1_a.eq.0).and.(mob%gamma_1_d.eq.0)) then !for compatibility with previous code version
        mob%gamma_1_a = mob%gamma_1
        mob%gamma_1_d = mob%gamma_2
    endif  
    if (t.gt.200) then
        mu_min_t            = ( mob%mu_min_a*(T/300)**mob%gamma_1_a * na_i + mob%mu_min_d*(T/300)**mob%gamma_1_d* nd_i ) / ( na_i + nd_i )
    else
        mu_min_t            = ( mob%mu_min_a*(2./3.)**mob%gamma_1_a * (T/200)**mob%gamma_2_a * na_i + mob%mu_min_d*(2./3.)**mob%gamma_1_d * (T/200)**mob%gamma_2_d * nd_i ) / ( na_i + nd_i )
    endif   
    
    mu_LI             = mu_min_t + (mu_L_temp-mu_min_t)/( 1 + (na_i/c_ref_a_temp)**alpha_a_temp + (nd_i/c_ref_d_temp)**alpha_d_temp)

endfunction

function mu_LI_cryo(mob,mu_L,na_i,nd_i,t,cn_i,cp_i) result(mu_LI)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Implementation of the mobility calculation for cryo simulation according to Chenyang Yu Master thesis. 
    ! This routine is tailored for SiGe HBTs.
    ! input
    ! -----
    ! result
    ! ------
    ! mu_LI : DUAL_NUM
    !   Mobility for doping, carrier type and temperature.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob      !mobility parameters
    TYPE(DUAL_NUM)  :: mu_L     !lattice mobility
    TYPE(DUAL_NUM),optional  :: cn_i     !electron concentration
    TYPE(DUAL_NUM),optional  :: cp_i     !hole concentration
    real(8)         :: na_i     !impurity concentration acceptors
    real(8)         :: nd_i     !donor concentration acceptors
    TYPE(DUAL_NUM)  :: t        !temperature in kelvin

    real(8)         :: c_d, c_a, ND_ref, NA_ref, ND, NA, Z, m_to_m
    integer         :: ib,is
    TYPE(DUAL_NUM)  :: c,N_sc,N_sc_eff,G,PP,mu_n,mu_daeh,mu_c,F,p,n
    TYPE(DUAL_NUM)  :: mu_LI
    ! model parameters
    real(8)         :: meff,mumax, mumin, n_ref,mn_over_m0,mp_over_m0,f_e,f_h,f_CW,f_bh,alpha,ag,cg,bg,gammag,betag,alphag,alphag2

    ! init to doping if nothing else given
    p = na_i
    n = nd_i

    ! use actual densities if given
    if (present(cp_i)) p = cp_i
    if (present(cn_i)) n = cn_i

    ! copy model parameters from user input variables
    mumax      = mob%mu_max
    mumin      = mob%mu_min
    n_ref      = mob%c_ref
    meff       = mob%meff
    f_e        = mob%f_e
    f_h        = mob%f_h
    f_CW       = mob%f_CW
    f_bh       = mob%f_bh
    alpha      = mob%alpha
    ag         = mob%ag
    bg         = mob%bg
    cg         = mob%cg
    gammag     = mob%gammag
    betag      = mob%betag
    alphag     = mob%alphag
    alphag2    = mob%alphag2

    ! band and semiconductor index
    ib = mob%band_id
    is = mob%semi_id

    c = n + p

    ! carrier transport masses
    mp_over_m0 = SEMI(is)%band_pointer(CONT_HOLE%valley)%band%mob%meff
    mn_over_m0 = SEMI(is)%band_pointer(CONT_ELEC%valley)%band%mob%meff

    if (ib.eq.CONT_ELEC%valley) then !electrons
        m_to_m = mn_over_m0 / mp_over_m0
    else ! holes
        m_to_m = mp_over_m0 / mn_over_m0
    endif

    ! clustering
    if (ib.eq.CONT_ELEC%valley) then ! electrons (assume Arsenic)
        c_d = dble(0.21)
        ND_ref = 4e20*1e6
        Z = 1 + nd_i**2 / (c_d * nd_i**2 + ND_ref**2)
    else ! holes (assume Boron)
        c_a = dble(0.5)
        NA_ref = 7.2e20*1e6
        Z = 1 + na_i**2 / (c_a * na_i**2 + NA_ref**2) 
    endif

    ND = nd_i * Z
    NA = na_i * Z

    if (ib.eq.CONT_ELEC%valley) then !electrons
        N_sc = ND + NA + p
    else ! holes
        N_sc = NA + ND + n
    endif

    ! screening parameter
    PP = (f_CW / (dble(3.97e13) * N_sc ** (-dble(2)/dble(3))) + f_BH * c / dble(1.36e20) * (1/meff)) ** -1 * (t / dble(300)) ** dble(2)

    ! minority impurity scattering analytic function
    G = (1 - ag * (bg + PP * ((1 / (meff) * (t / dble(300))) ** alphag)) ** (-betag) &
        & + cg * (PP * ((meff) * (dble(300) / t)) ** alphag2) ** (-gammag))

    ! electron-hole scattering analytic function
    F = (dble(0.7643) * PP ** dble(0.6478) + dble(2.2999) + dble(6.5502) * m_to_m) &
        & / (PP ** dble(0.6478) + dble(2.3670) - dble(0.8552) * m_to_m)

    ! All scattering partners
    if (ib.eq.CONT_ELEC%valley) then !electrons
        N_sc_eff = ND + G * NA + f_e * (p / F)
    else !holes
        N_sc_eff = NA + G * ND + f_h * (n / F)
    endif

    ! calculate mu_daeh
    mu_N = (mumax ** dble(2.0) / (mumax - mumin)) * (t / dble(300)) ** (dble(3) * alpha - dble(1.5))
    mu_c = (mumax * mumin / (mumax - mumin)) * (dble(300) / t) ** dble(0.5)
    mu_daeh = mu_N * (N_sc / N_sc_eff) * (n_ref / N_sc) ** alpha + mu_c * c / N_sc_eff

    ! The total mobility using Matthiessen's rule
    mu_LI = 1 / (1 / mu_L + 1 / mu_daeh)
endfunction

! function impurity_tu_wien(mob,mu_L,don,acc,t_t0) result(mu_LI)
!   ! electron mobility model from TU WIEN
!   ! ONLY USE WITH ELECTRONS
!   type(mob_model) :: mob      !mobility parameters
!   real(8)         :: mu_L     !lattice mobility
!   real(8)         :: don      !donator density
!   real(8)         :: acc      !acceptor density
!   real(8)         :: impurity !impurity concentration
!   real(8)         :: t_t0     !temperature in kelvin

!   real(8)         :: mu_min,mu_min_t
!   real(8)         :: impurity_ref,impurity_ref_temp
!   real(8)         :: alpha
!   real(8)         :: mu_LI
!   real(8)         :: mu_1,mu_2,beta,C_1,C_2

!   if (don.gt.acc) then !use majority parameters
!     mu_1  = mob%mob_maj     *t_t0**mob%gamma_1
!     mu_2  = mob%mob_maj_2   *t_t0**mob%gamma_2
!     alpha = mob%mob_alph    *t_t0**mob%gamma_3
!     beta  = mob%mob_beta_maj*t_t0**mob%gamma_4
!     C_1   = mob%C_1         *t_t0**mob%gamma_5
!     C_2   = mob%C_2_maj     *t_t0**mob%gamma_6
!   else
!     mu_1  = mob%mob_min     *t_t0**mob%gamma_7
!     mu_2  = mob%mob_min_2   *t_t0**mob%gamma_8
!     alpha = mob%mob_alph    *t_t0**mob%gamma_3
!     beta  = mob%mob_beta_min*t_t0**mob%gamma_9
!     C_1   = mob%C_1         *t_t0**mob%gamma_5
!     C_2   = mob%C_2_min     *t_t0**mob%gamma_10
!   endif

!   mu_LI             = (mu_L-mu_1-mu_2)/(1+(impurity/C_1)**alpha) + mu_1/(1+(impurity/C_2)**mob%mob_beth) + mu_2

! endfunction

function lattice_mobility(mob,dim,T_T0) result(mu_L)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! simple lattice mobility model, see manual.
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! dim : integer
    !   Obsolete -> transport dimensions.
    ! T_T0 : DUAL_NUM
    !   Ratio of T/T0 at point.
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! mu_L : DUAL_NUM
    !   The lattice mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob
    integer         :: dim
    type(DUAL_NUM)  :: T_T0
    type(DUAL_NUM)  :: mu_L

    mu_L = mob%mu_L(dim)
    mu_L = mu_L*T_T0**mob%gamma_L

endfunction lattice_mobility

function michaillat_n(mob,dim,T_T0,stress) result(mu_L)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! simple lattice mobility model, see manual.
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! dim : integer
    !   Obsolete -> transport dimensions.
    ! T_T0 : DUAL_NUM
    !   Ratio of T/T0 at point.
    ! stress : real(8)
    !   Stress z at point.
    !
    ! output
    ! ------
    ! mu_L : DUAL_NUM
    !   The lattice mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob
    integer         :: dim
    type(DUAL_NUM)  :: T_T0
    real(8)         :: stress
    type(DUAL_NUM)  :: mu_L

    mu_L = mob%mu_L(dim) + ( mob%mu_cs(dim) - mob%mu_L(dim) ) /( 1+ exp( (stress-mob%z_0(dim)) * mob%beta_l(dim) ) )
    mu_L = mu_L * T_T0**mob%gamma_l

endfunction michaillat_n

function michaillat_p(mob,dim,T_T0,stress) result(mu_L)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! simple lattice mobility model, see manual.
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! dim : integer
    !   Obsolete -> transport dimensions.
    ! T_T0 : DUAL_NUM
    !   Ratio of T/T0 at point.
    ! stress : real(8)
    !   Stress z at point.
    !
    ! output
    ! ------
    ! mu_L : DUAL_NUM
    !   The lattice mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob
    integer         :: dim
    type(DUAL_NUM)  :: T_T0
    real(8)         :: stress
    type(DUAL_NUM)  :: mu_L

    mu_L = mob%mu_L(dim) + (stress/mob%z_0(dim))**2 *1e-4
    mu_L = mu_L * T_T0**mob%gamma_l

endfunction michaillat_p

function get_lattice_mobility(mob,dim,T_T0,stress) result(mu_L)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! wrapper for calculation of lattice mobility
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! dim : integer
    !   Obsolete -> transport dimensions.
    ! T_T0 : DUAL_NUM
    !   Ratio of T/T0 at point.
    ! grading : real(8)
    !   x grading at point.
    ! stress : real(8)
    !   Stress z at point.
    !
    ! output
    ! ------
    ! mu_L : DUAL_NUM
    !   The lattice mobility.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model) :: mob
    integer         :: dim
    type(DUAL_NUM)  :: T_T0
    real(8)         :: stress
    type(DUAL_NUM)  :: mu_L
    if (mob%l_scat_is_default) then
        mu_L   = mob%mu_L(dim)
    elseif (mob%l_scat_is_temp) then
        mu_L   = lattice_mobility(mob,dim,T_T0)
    elseif (mob%l_scat_is_michaillat_n) then
        mu_L   = michaillat_n(mob,dim,T_T0,stress)
    elseif (mob%l_scat_is_michaillat_p) then
        mu_L   = michaillat_p(mob,dim,T_T0,stress)
    endif
endfunction get_lattice_mobility

function get_mobility_dual(ip,is,ib,isp,phi_l,phi_r,ec_l,ec_r,tn_l,tn_r,dx,dim,t,n,don,acc,grading,grading2,stress) result(mu)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Main function used by other modules to calculate the mobility at grid points in the semiconductor.
    ! Dual valued arguments allow easy generation of derivatives. The mobility is always defined between
    ! two points in the semiconductor with indices is (left point index) and isp (right point index).
    !
    ! input
    ! -----
    ! ip : integer
    !   Index of the point in the structure.
    ! is : integer
    !   Index of the semiconductor model, left point.
    ! ib : integer
    !   Index of the band. Is the same for all semiconductors, thus only one input.
    ! isp : integer
    !   Index of the semiconductor model, right point.
    ! phi_l : DUAL_NUM
    !   Quasi-Fermi potential at left point.
    ! phi_r : DUAL_NUM
    !   Quasi-Fermi potential at right point.
    ! ec_l : DUAL_NUM
    !   Conduction band edge at left point.
    ! ec_r : DUAL_NUM
    !   Conduction band edge at right point.
    ! tn_l : DUAL_NUM
    !   Left carrier gas temperature.
    ! tn_r : DUAL_NUM
    !   Right carrier gas temperature.
    ! dx : real(8)
    !   Distance between points.
    ! dim : integer
    !   Transport direction (maybe obsolete, was needed for direction dependent mobility.)
    ! t : DUAL_NUM
    !   (Average) temperature between points.
    ! n : DUAL_NUM
    !   (Average) carrier density between points.
    ! don : real(8)
    !   (Average) donor concentration between points.
    ! acc : real(8)
    !   (Average) acceptor concentration between points.
    ! grading : real(8)
    !   (Average) x grading between points.
    ! grading2 : real(8)
    !   (Average) y  grading between points.
    ! stress : real(8)
    !   (Average) stress at point.
    !
    ! output
    ! ------
    ! mu : DUAL_NUM
    !   The mobility at the semiconductor grid point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in)              :: ip
    integer,intent(in)              :: is
    integer,intent(in)              :: ib
    integer,intent(in)              :: isp
    TYPE(DUAL_NUM),intent(in)       :: phi_l
    TYPE(DUAL_NUM),intent(in)       :: phi_r
    TYPE(DUAL_NUM),intent(in)       :: ec_l
    TYPE(DUAL_NUM),intent(in)       :: ec_r
    TYPE(DUAL_NUM),intent(in)       :: tn_l
    TYPE(DUAL_NUM),intent(in)       :: tn_r
    TYPE(DUAL_NUM),intent(in)       :: t
    real(8),intent(in)              :: dx
    real(8),intent(in)              :: stress


    integer                        :: dim
    TYPE(DUAL_NUM),optional,value  :: n
    TYPE(DUAL_NUM)                 :: f       !< [V/m] electric field
    real(8),optional,value         :: don
    real(8),optional,value         :: acc
    real(8),optional,value         :: grading
    real(8),optional,value         :: grading2
    TYPE(DUAL_NUM)                 :: mu

    integer :: id_materialA, id_materialB
    TYPE(DUAL_NUM) :: mu_LI
    TYPE(DUAL_NUM) :: f_n,f_e,T_T0
    TYPE(DUAL_NUM) :: vs
    TYPE(DUAL_NUM) :: tau
    !TYPE(DUAL_NUM) :: wn,wo
    !TYPE(DUAL_NUM) :: alpha
    !real(8) :: f_crit,mu_df,mu_val
    type(mob_model),pointer :: mob,mob_A,mob_B
    logical :: is_alloy = .false.

    real(8) :: beta, beta1, beta2, beta3
    real(8) :: fac_driving_force


    ! not used anymore:
    ! n   = n  *NORM%N_N 

    mob  => SEMI(is)%band_pointer(ib)%band%mob
    if (SEMI(is)%is_alloy) then
        id_materialA = SEMI(is)%id_materialA
        id_materialB = SEMI(is)%id_materialB
        mob_A        => SEMI(id_materialA)%band_pointer(ib)%band%mob
        mob_B        => SEMI(id_materialB)%band_pointer(ib)%band%mob
    endif
    is_alloy = SEMI(is)%is_alloy

    ! load mu_LF if does not depend on solution variable
    if  (.not.(mob%li_scat_is_cryo)) then
        if (ib.eq.CONT_ELEC%valley) then 
            mu_LI = DN(ip)%mu_LF(dim) 
        elseif (ib.eq.CONT_ELEC2%valley) then 
            mu_LI = DN2(ip)%mu_LF(dim)
        else 
            mu_LI = DP(ip)%mu_LF(dim)
        endif
    else
        if (ib.eq.CONT_ELEC%valley) then 
            mu_LI = get_mu_LF(is,ib,dim,T_T0,acc,don,grading,grading2,stress,n,DUAL_NUM(acc,0))
        else
            mu_LI = get_mu_LF(is,ib,dim,T_T0,acc,don,grading,grading2,stress,DUAL_NUM(don,0),n)
        endif
    endif

    ! step 2: field dependence if beta>0
    if (mob%hc_scat_is_default) then
        mu = mu_LI
    else !field dependent F = force*J/Je
        f = 1
        if (mob%force_is_hd) then 
            tau = get_taun_dual(is,ib,tn_l,tn_r,t,grading)

            fac_driving_force = mob%fac_driving_force

            ! calculate v_s
            T_T0 = t/SEMI(is)%temp0
            vs = get_vsat(is,ib,T_T0,grading,acc,don) !not needed if field dependence is turned off

            ! DA Wedel
            f   = dble(1.5)*fac_driving_force*BK/Q/tau*( (tn_l + tn_r)*dble(0.5) - t )/vs

            ! Minimos: haensch model
            ! f  = dble(.5)*BK*mu_LI/Q/tau/vs/vs
            ! mu = mu_LI/(1+f*( (tn_l + tn_r)*dble(0.5) - t ))
            ! return

            ! "Hydrodynamic simulations for advanced SiGe HBTs"
            ! wn    = dble(1.5)*BK*(tn_l + tn_r)*dble(0.5)
            ! wo    = dble(1.5)*BK*t
            ! alpha = dble(0.5)*(mu_LI/Q/tau/vs/vs)**(mob%beta*dble(0.5))
            ! mu    = mu_LI/(  alpha*(wn-wo)**(mob%beta*dble(0.5)) + sqrt(alpha**dble(2)*(wn-wo)**mob%beta + 1 )  )**(dble(2)/mob%beta)
            ! return
        elseif (mob%force_is_grad_psi) then 
            f = abs(ec_r - ec_l) / dx 
        elseif (mob%force_is_grad_phi) then
            ! f = (phi_r - phi_l)/ dx 
            ! if ( .not.(f.eq.0) ) f = sqrt(f**dble(2))
            f = abs(phi_r - phi_l) / dx 
            !f = abs(phi_l - phi_r) / dx 
            ! f_abs = abs(phi_r - phi_l) / dx 
            ! if (f.lt.-1e3) then !wenn phi_l>phi_r dreht abs() das Vorzeichen der Ableitung um
            !     f_e = 0
            ! endif
            ! if (f.ge.250e5 - 5e5) then !smooth damping like in Ngspice after 200 kV/cm
            !      f = 250e5 - exp(250e5-f-5e5)
            ! endif

        elseif (mob%force_is_grad_mix) then
            f_e = (ec_r  - ec_l) / dx 
            f_n = (phi_r - phi_l)/ dx 
            if ( .not.(f_e.eq.0) ) f_e = sqrt(abs(f_e))
            if ( .not.(f_n.eq.0) ) f_n = sqrt(abs(f_n))
            f = f_n*f_E
        else
            write(*,*) '***error*** hc_scat_type specifies field dependent mobiltiy model, however force was not correctly specified.'
            stop
        endif

        !denormalize f, so we can keep the parameters for the field
        f = f * NORM%l_N_inv

        ! nonlocal field in xdir for first valley
        if (dim.eq.1) then 
            f = get_nl_field(f,isp)
            if (ib.eq.CONT_ELEC%valley) then
                f = f*US%nonlocal%af !nonlocal field tuning parameter af
                mu_LI = mu_LI *US%nonlocal%af !af used here else the non-local v(E) does not converge to the local one for pre-factor=1
            endif
        endif

        if (f.le.1e3) then !small fields, just LF mobility
            mu = mu_LI
            return
        else
            !calculate beta
            if (.not.is_alloy) then
                beta   = mob%beta
            else !beta always bowed linearly between constituents
                beta   = mob_A%beta*(1-grading) + mob_B%beta * grading !linear bowing
            endif

            ! calculate v_s
            T_T0 = t/SEMI(is)%temp0
            vs = get_vsat(is,ib,T_T0,grading,acc,don) !not needed if field dependence is turned off

            ! actual velocity saturation model
            if (mob%hc_scat_is_sat) then

                ! device version
                ! mu = (dble(1) + (f*mu_LI/vs)**mob%beta)**(dble(1)/mob%beta)

                ! hdev version (old hdev implicitly assumes signs?)
                !mu = mu_LI/(dble(1) + (f*mu_LI/vs)**beta)**(dble(1)/beta)
                mu = mu_LI/(dble(1) + (f*mu_LI/vs)**beta)**(dble(1)/beta) 
                !mu = mu_LI/sqrt(dble(1) + (f*mu_LI/vs)*(f*mu_LI/vs))


                ! numerical version
                ! mu = mu_LI/exp(log(dble(1)+exp(mob%beta*log(f*mu_LI/vs)))/mob%beta)
                ! mu = mu_LI/sqrt(dble(1) + (f*mu_LI/vs)**dble(2))
                !slope to aid convergence
                ! f_crit = 50e5
                ! if (f.gt.(f_crit)) then
                !     mu = mu*(1+(f-f_crit)*1e-4*30e-4)
                ! endif
                ! mu_val       = mu_LI%x_ad_/((1 + ((f%x_ad_+0.0001e2)*mu_LI%x_ad_/vs%x_ad_)**mob%beta)**(dble(1)/mob%beta))
                ! mu_df        = mu_val - mu_LI%x_ad_/((1 + (f%x_ad_*mu_LI%x_ad_/vs%x_ad_)**mob%beta)**(dble(1)/mob%beta))
                ! mu_df        = mu_df/0.0001e2
                ! mu%x_ad_     = mu_val
                ! mu%xp_ad_(1) = mu_df*f%xp_ad_(1) !d phil
                ! mu%xp_ad_(2) = mu_df*f%xp_ad_(2) !d phil
            elseif (mob%hc_scat_is_ndm) then
                ! mu = ( mu_LI + vs/f*(f/mob%f0)**beta ) / ( 1 + (f/mob%f0)**beta )
                beta1  = mob%beta1
                beta2  = mob%beta2
                beta3  = mob%beta3
                if ((beta1.eq.0).and.(beta2.eq.0).and.(beta3.eq.0)) then
                    beta1 = beta
                endif
                !mu = ( mu_LI + vs/f*(f/mob%f0)**beta1 ) / ( 1 + beta3*(f/mob%f0)**beta2 + (f/mob%f0)**beta1)
                ! mu = ( mu_LI + vs/f*(f/mob%f1)**beta3  + mob%mu_HC*(f/mob%f0)**beta1) / ( 1 + beta2*(f/mob%f0)**beta1 + (f/mob%f1)**beta3)
                mu = ( mu_LI + vs/f*(f/mob%f0)**beta1  + mob%mu_HC*(f/mob%f1)**beta2) / ( 1 + (f/mob%f0)**beta1 + beta3*(f/mob%f1)**beta2)!!
                
            elseif (mob%hc_scat_is_ndm2) then
                ! mu = ( mu_LI + mob%mu_HC*(f/mob%f0)**beta ) / ( 1 + (f/mob%f0)**beta )
                write(*,*) '***error*** ndm2 model not implemented anymore.'
                stop
            else 
                write(*,*) '***error*** hc scat mobility model not set correctly. Possible values: default, sat, ndm'
                stop
            endif
        endif
    endif

    ! for debugging:
    ! if (mu%x_ad_.eq.dble(0)) then
    !     write(*,*) '***error*** mobility equals zero. Something is definately wrong.'
    !     stop
    ! endif
endfunction get_mobility_dual

function get_mobility(ip,is,ib,isp,phi_l,phi_r,ec_l,ec_r,tn_l,tn_r,dx,dim,t,n,don,acc,grading,grading2,stress) result(mu)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! warpper for get_mobility_dual, such that it can be called with real valued arguments. See the doc of that routine (above)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer              :: ip            !< point id
    integer              :: is            !< semiconductor model id
    integer              :: ib            !< index to band
    integer              :: isp            !< index to semiconductor point id
    real(8)              :: phi_l             !< [V/m] electric field
    real(8)              :: phi_r             !< [V/m] electric field
    real(8)              :: ec_l             !< [V/m] electric field
    real(8)              :: ec_r             !< [V/m] electric field
    real(8)              :: tn_l             !< [V/m] electric field
    real(8)              :: tn_r             !< [V/m] electric fiel
    real(8)              :: dx             !< [V/m] electric field
    real(8)              :: stress             !< stress

    integer                   :: dim           !< dimension
    real(8)                   :: t             !< [K] temperatur
    real(8),optional,value    :: n       !< [1/m] carrier density
    real(8),optional,value    :: don     !< [1/m] donator density
    real(8),optional,value    :: acc     !< [1/m] acceptor density
    real(8),optional,value    :: grading !< [1/m] grading x density
    real(8),optional,value    :: grading2 !< [1/m] grading y density
    TYPE(DUAL_NUM)            :: result   
    real(8)                   :: mu            !< [m^2/Vs] mobility

    result = get_mobility_dual(ip,is,ib,isp,DUAL_NUM(phi_l,0),DUAL_NUM(phi_r,0),DUAL_NUM(ec_l,0),DUAL_NUM(ec_r,0),DUAL_NUM(tn_l,0),DUAL_NUM(tn_r,0),dx,dim,DUAL_NUM(t,0),DUAL_NUM(n,0),don,acc,grading,grading2,stress)
    mu     = result%x_ad_
endfunction get_mobility

function vs_device(mob_paras,t_t0) result(v_s)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! velocity saturation model from device. See manual.
    !
    ! input
    ! -----
    ! mob_paras : mob_model
    !   Mobility model parameters.
    ! t_t0 : DUAL_NUM
    !   Ratio of lattice to reference temperature.
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! v_s : DUAL_NUM
    !   Saturaiton velocity as a function of T and grading.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model)         :: mob_paras !< semiconductor model id
    TYPE(DUAL_NUM)          :: t_t0      !< [K] temperatur
    TYPE(DUAL_NUM)          :: v_s

    v_s = mob_paras%v_sat
    v_s = v_s*t_t0**mob_paras%zeta_v

endfunction 

function get_mu_LI(mob,mu_L,t,acc,don,cn_i,cp_i) result(mu_LI)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Get lattice and impurity scattering limited mobility of pure semiconductor
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! t   : DUAL_NUM
    !   Tattice temperature
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! v_s : DUAL_NUM
    !   Saturation velocity.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model)         :: mob
    TYPE(DUAL_NUM)          :: mu_L,t
    real(8),value           :: don,acc
    TYPE(DUAL_NUM)          :: mu_LI
    TYPE(DUAL_NUM)          :: cn_i,cp_i

    if (mob%li_scat_is_default) then
        mu_LI = mu_L
    elseif (mob%li_scat_is_caughey_thomas) then
        mu_LI = caughey_thomas(mob, mu_L, abs(don)+abs(acc), t)
    elseif (mob%li_scat_is_device) then
        write(*,*) '***error*** mu_LI model from DEVICE not anymore supported'
        stop
    elseif (mob%li_scat_is_reggiani) then
        mu_LI = regianni(mob, mu_L, acc, don, t)
    elseif (mob%li_scat_is_cryo) then
        mu_LI = mu_LI_cryo(mob,mu_L,acc,don,t,cn_i,cp_i)
    else
        write(*,*) '***error*** li mobility model not set correctly.'
        stop
    endif

endfunction

function vs_default(mob,t_t0,acc,don) result(v_s)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! default saturation velocity model. See manual.
    !
    ! input
    ! -----
    ! mob : mob_model
    !   Mobility model parameters.
    ! t_t0 : DUAL_NUM
    !   Ratio of lattice temperature to reference temperature.
    !
    ! output
    ! ------
    ! v_s : DUAL_NUM
    !   Saturation velocity.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(mob_model)         :: mob
    TYPE(DUAL_NUM)          :: t_t0
    real(8),optional,value  :: don,acc
    TYPE(DUAL_NUM)          :: v_s

    v_s = mob%v_sat
    ! Doping change
    if (mob%c_ref_vsat.ne.0) then
        !v_s = v_s*(abs(don-acc)*NORM%N_N/mob%c_ref_vsat)**mob%al_vsat
        v_s = v_s/( 1 + (abs(don-acc)*NORM%N_N/mob%c_ref_vsat)**mob%al_vsat )
    endif
    ! Change
    v_s = v_s/(1-mob%A+mob%A*t_t0)
endfunction 

function get_vsat(is,ib,T_T0,grading,acc,don) result(vs)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! wrapper routine for the saturation velocity models.
    !
    ! input
    ! -----
    ! is : integer
    !   Index of semiconductor model
    ! ib : integer
    !   Index of band.
    ! T_T0 : DUAL_NUM
    !   Ratio of lattice to reference temperature
    ! grading : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! vs : DUAL_NUM
    !   Saturation velocity as a function of grading and temperature.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer                 :: is,ib
    type(DUAL_NUM)          :: T_T0
    real(8)                 :: grading,acc,don
    type(mob_model),pointer :: mob,mob_A,mob_B
    integer                 :: id_materialA
    integer                 :: id_materialB
    type(DUAL_NUM)          :: vs,vs_A,vs_B

    mob  => SEMI(is)%band_pointer(ib)%band%mob
    vs   = 0
    if (SEMI(is)%is_alloy) then
        id_materialA = SEMI(is)%id_materialA
        id_materialB = SEMI(is)%id_materialB
        mob_A        => SEMI(id_materialA)%band_pointer(ib)%band%mob
        mob_B        => SEMI(id_materialB)%band_pointer(ib)%band%mob
    endif

    if (mob%v_sat_is_default) then
        if (.not.SEMI(is)%is_alloy) then
            vs   = vs_default(mob,T_T0,acc,don)
        elseif (mob%bowing_is_default.or.mob%bowing_is_michaillat) then
            vs_A = vs_default(mob_A,T_T0,acc,don)
            vs_B = vs_default(mob_B,T_T0,acc,don)
            vs   = bow(vs_A, vs_B, mob%v_sat_bow, grading)
        endif
    elseif (mob%v_sat_is_device) then
        if (.not.SEMI(is)%is_alloy) then
            vs   = vs_device(mob,T_T0)
        elseif (mob%bowing_is_default.or.mob%bowing_is_michaillat) then
            vs_A = vs_device(mob_A,T_T0)
            vs_B = vs_device(mob_B,T_T0)
            vs   = bow(vs_A, vs_B, mob%v_sat_bow, grading)
        endif
    endif

endfunction

function get_taun_dual(is,ib,tn_l,tn_r,t,grading) result(tau_e)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! main function used by other modules to calculate the energy relaxation time at at grid points in the semiconductor.
    ! Dual valued arguments allow easy generation of derivatives. The mobility is always defined between
    ! two points in the semiconductor with indices is and isp.
    !
    ! input
    ! -----
    ! is : integer
    !   Index of the semiconductor model, left point.
    ! ib : integer
    !   Index of the band. Is the same for all semiconductors, thus only one input.
    ! tn_l : DUAL_NUM
    !   Electron temperature at left point.
    ! tn_r : DUAL_NUM
    !   Electron temperature at right point.
    ! grading : real(8)
    !   (Average) x grading between points.
    !
    ! output
    ! ------
    ! tau : DUAL_NUM
    !   The energy relaxation time at the semiconductor grid point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in)              :: is
    integer,intent(in)              :: ib
    TYPE(DUAL_NUM),intent(in)       :: tn_l
    TYPE(DUAL_NUM),intent(in)       :: tn_r
    TYPE(DUAL_NUM),intent(in)       :: t

    real(8),optional,value         :: grading
    TYPE(DUAL_NUM)                 :: tau_e

    integer :: id_materialA, id_materialB
    TYPE(DUAL_NUM)                 :: tn
    type(tau_model),pointer :: tau_parameters,tau_parameters_A,tau_parameters_B

    real(8) :: t0,t1,c0,c1,c2,c3
    real(8) :: t0_A,t1_A,c0_A,c1_A,c2_A,c3_A
    real(8) :: t0_B,t1_B,c0_B,c1_B,c2_B,c3_B
    real(8) :: t0_bow,c0_bow
    real(8) :: one_min_x,fac

    tn   = (tn_r + tn_l)*0.5
    tau_parameters  => SEMI(is)%band_pointer(ib)%band%tau_parameters
    if (SEMI(is)%is_alloy) then
        id_materialA = SEMI(is)%id_materialA
        id_materialB = SEMI(is)%id_materialB
        tau_parameters_A => SEMI(id_materialA)%band_pointer(ib)%band%tau_parameters
        tau_parameters_B => SEMI(id_materialB)%band_pointer(ib)%band%tau_parameters
    endif

    if (SEMI(is)%is_alloy.and.tau_parameters%is_michaillat) then
        tau_e = (tau_parameters%t0+tau_parameters%t1*grading)*( 1 - exp( -tau_parameters%c0*tn/t ) + dble(1)/(tn/t) )
    elseif (tau_parameters%is_default) then
        if (.not.SEMI(is)%is_alloy) then
            t0   = tau_parameters%t0
            t1   = tau_parameters%t1
            c0   = tau_parameters%c0
            c1   = tau_parameters%c1
            c2   = tau_parameters%c2
            c3   = tau_parameters%c3
        else
            !bow parameters according to Diss. Palanovski
            !mat A
            t0_A   = tau_parameters_A%t0
            t1_A   = tau_parameters_A%t1
            c0_A   = tau_parameters_A%c0
            c1_A   = tau_parameters_A%c1
            c2_A   = tau_parameters_A%c2
            c3_A   = tau_parameters_A%c3
            !mat B
            t0_B   = tau_parameters_B%t0
            t1_B   = tau_parameters_B%t1
            c0_B   = tau_parameters_B%c0
            c1_B   = tau_parameters_B%c1
            c2_B   = tau_parameters_B%c2
            c3_B   = tau_parameters_B%c3
            !bow A_(1-x)B_x
            one_min_x = (1-grading)
            fac = one_min_x*grading
            t0_bow  = tau_parameters%t0_bow
            c0_bow  = tau_parameters%c0_bow
            t0 = t0_A * one_min_x + t0_B * grading + t0_bow*fac
            t1 = t1_A * one_min_x + t1_B * grading 
            c0 = c0_A * one_min_x + c0_B * grading + c0_bow*fac
            c1 = c1_A * one_min_x + c1_B * grading 
            c2 = c2_A * one_min_x + c2_B * grading 
            c3 = c3_A * one_min_x + c3_B * grading 

        endif
        tau_e   = t0 + t1*exp( c1*(tn/dble(300)+c0)**2 + c2*(tn/dble(300)+c0) + c3*(TEMP_LATTICE/dble(300)) )
    else
        write(*,'(A)') '***error*** tau_e model error.'
        stop
    endif

endfunction get_taun_dual

subroutine init_mu_lf()
    integer sp_id
    real(8) don, acc,strai,grad,grad2
    TYPE(DUAL_NUM) :: mu_LI, T_T0
    integer i,j,k,ib,is
    integer :: dim

    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%semi_id.GT.0) then
                    sp_id   = POINT(i,j,k)%sp_idx
                    don = DONATORS(sp_id)*NORM%N_N
                    acc = ACCEPTORS(sp_id)*NORM%N_N
                    grad = GRADING(sp_id)
                    grad2 = GRADING2(sp_id)
                    strai = STRAIN(sp_id)
                    is = POINT(i,j,k)%semi_id
                    T_T0 = TEMP_LATTICE/SEMI(is)%temp0

                    do dim=1,2 !iterate over transport dimension
                        do ib=1,SEMI(is)%n_band !iterate over all bands of that semiconductor

                            mu_LI = get_mu_LF(is,ib,dim,T_T0,acc,don,grad,grad2,strai,DUAL_NUM(don,0),DUAL_NUM(acc,0))

                            if (ib.eq.CONT_ELEC%valley) then 
                                DN(sp_id)%mu_LF(dim) = mu_LI%x_ad_
                            elseif (ib.eq.CONT_ELEC2%valley) then 
                                DN2(sp_id)%mu_LF(dim) = mu_LI%x_ad_
                            else 
                                DP(sp_id)%mu_LF(dim) = mu_LI%x_ad_
                            endif

                        enddo
                    enddo
                endif
            enddo
        enddo
    enddo

endsubroutine

function get_mu_LF(is,ib,dim,T_T0,acc,don,grad,grad2,strai,cn_i,cp_i) result(mu_LI)
    integer is,ib,dim
    integer :: id_materialA, id_materialB
    logical :: is_alloy = .False.
    real(8) :: acc,don,grad,grad2,strai
    TYPE(DUAL_NUM) :: mu_LI
    TYPE(DUAL_NUM) :: cn_i, cp_i

    TYPE(DUAL_NUM) :: mu_LI_A, mu_LI_B, mu_L, mu_L_A, mu_L_B, T_T0
    type(mob_model),pointer :: mob,mob_A,mob_B

    id_materialA = SEMI(is)%id_materialA
    id_materialB = SEMI(is)%id_materialB

    mob  => SEMI(is)%band_pointer(ib)%band%mob
    if (SEMI(is)%is_alloy) then
        mob_A        => SEMI(id_materialA)%band_pointer(ib)%band%mob
        mob_B        => SEMI(id_materialB)%band_pointer(ib)%band%mob
    endif
    is_alloy = SEMI(is)%is_alloy

    !step 1: calculate lattice mobility and scale it with temperature
    !step 2: calculate lattice+impurity mobility using caughey and thomas equation
    if (.not.is_alloy) then
        mu_L = get_lattice_mobility(mob,dim,T_T0,strai)
        mu_LI = get_mu_LI(mob,mu_L,DUAL_NUM(TEMP_LATTICE,0),acc,don,cn_i,cp_i)
    else
        mu_L_A = get_lattice_mobility(mob_A,dim,T_T0,strai)
        mu_L_B = get_lattice_mobility(mob_B,dim,T_T0,strai)
        mu_LI_A = get_mu_LI(mob_A,mu_L_A,DUAL_NUM(TEMP_LATTICE,0),acc,don,cn_i,cp_i)
        mu_LI_B = get_mu_LI(mob_B,mu_L_B,DUAL_NUM(TEMP_LATTICE,0),acc,don,cn_i,cp_i)
        if (mob%bowing_is_default) then
            ! mu_LI   = matthiesen_rule(mu_LI_A, mu_LI_B, mob%mu_bow, grading)
            mu_LI   = bow(mu_LI_A, mu_LI_B, mob%mu_bow, grad)
        elseif (mob%bowing_is_michaillat) then
            mu_LI   = regianni_bowing(mu_LI_A, mu_LI_B, mob, grad, grad2, strai, dim)
        endif
    endif

    ! multiply with adjustement factor
    mu_LI = mu_LI*mob%mu_fac

    if (mu_LI%x_ad_.eq.dble(0)) then
        write(*,*) '***error*** LI mobility equals zero. Something is definately wrong.'
        stop
    endif

endfunction get_mu_lf

endmodule