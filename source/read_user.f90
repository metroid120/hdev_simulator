!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! Input file parser. 
! In this module the input file is analyzed and its parameters are transfered into the internal data structure of hdev. Also an output file with all input parameters is generated.
module read_user

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use math_oper, only : to_lower !< convert all upper to lower characters

implicit none

! define one struct for all possible blocks below!

!----------------------------------------------------------------------------------------------------------------------
!> \brief &REGION_INFO \details
type u_regioninfo
    integer                 :: spat_dim        !< spatial dimension (0 is bulk, 1,2,3-dimensional)
    character(len=64)       :: hdf             !< name of hdf file to be readin by hdev for region definition
    integer                 :: pnts_max        !< maximum allowed number of discretization points
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &NON_LOCAL \details
type u_nonlocal
    character(len=20)       :: type        !< identifier for model
    real(8)                 :: xj_l        !< left junction
    real(8)                 :: xj_r        !< right junction
    real(8)                 :: ymax        !< for 2D simulation: force only used where y<ymax
    real(8)                 :: nl_base     !< factor for base of HBT
    real(8)                 :: lambda      !< characteristic energy relaxation length
    real(8)                 :: fmax        !< maximum field
    real(8)                 :: beta        !< transition parameter from low to high field
    real(8)                 :: af          !< pre-factor for driving force
    real(8)                 :: ai          !< pre-factor for intervalley transfer
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &REGION_INFO \details
type u_thermionic_emission
    real(8)                 :: x !< point near the hetero interface, where the boundary condition shall be applied
    real(8)                 :: fac !< factor for the boundary condition
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &REGION_DEF \details
type u_regiondef
    character(len=64)       :: reg_mat         !< region name
    character(len=64)       :: mod_name        !< corresponding physical model name
    character(len=64)       :: cont_name       !< contact name
    real(8),dimension(3)    :: low_xyz         !< corner closest to the origin of the coordinate system
    real(8),dimension(3)    :: upp_xyz         !< corresponding diagonal corner on the foreside of the origin
    integer                 :: layer           !< layer id
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &RANGE_GRID \details 
type u_rangegrid
    character(len=64)       :: disc_dir        !< discretization direction
    character(len=64)       :: disc_set        !< how to read intv_pnts
    real(8),dimension(20)   :: intv_pnts       !< definition of grid points
    real(8),dimension(20)   :: intv_diff       !< distance between grid points
    integer,dimension(20)   :: n_pnts          !< number of grid points
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &AREA_GRID \details 
type u_areagrid
    character(len=64)       :: disc_dir        !< discretization direction
    real(8),dimension(20)   :: intv_pnts       !< definition of grid points
    real(8),dimension(3,20) :: intv_diff       !< definition of grid points
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &SUPPLY \details The SUPPLY subblock can be used to define physical models for contacts.
type u_supply
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: supply_type     !< either "n" or "p" supply contact 
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &DOPING \details The DOPING parameter block can be used to define doping profiles.
type u_doping
    character(len=64)       :: profile         !< doping profile 'file','const','exp'
    real(8)                 :: d_con           !< doping concentration
    real(8)                 :: v_buildin       !< build in voltage
    real(8),dimension(3)    :: xyz_0           !< parameter
    real(8),dimension(3)    :: a_xyz           !< parameter
    real(8),dimension(3)    :: b_xyz           !< exponantial decay factor
    real(8),dimension(3)    :: sigma_xyz       !< standard deviation for gaussian profile
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
endtype u_doping

!----------------------------------------------------------------------------------------------------------------------
!> \brief &GRADING \details The DOPING parameter block can be used to define doping profiles.
type u_grading
    character(len=64)       :: profile         !< doping profile 'file','const','exp'
    character(len=64)       :: mat_type        !< x or y GRADING
    real(8)                 :: c_max           !< doping concentration
    real(8)                 :: c_min           !< doping concentration
    real(8),dimension(3)    :: xyz_0           !< parameter
    real(8),dimension(3)    :: a_xyz           !< parameter
    real(8),dimension(3)    :: b_xyz           !< exponantial decay factor
    !real(8),dimension(3)    :: sigma_xyz      !< standard deviation for gaussian profile
    !character(len=1024)     :: file           !< file with doping information
    !integer                 :: f_col          !< column number
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: sigma_xyz       !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
    real(8),dimension(3)    :: xyz_dlow        !< lower corner
    real(8),dimension(3)    :: xyz_dupp        !< upper corner
    real(8),dimension(3)    :: xyz_rlow        !< lower corner
    real(8),dimension(3)    :: xyz_rupp        !< upper corner

    real(8),dimension(10)    :: x_vec        !< upper corner
    real(8),dimension(10)    :: y_vec        !< upper corner
endtype u_grading

!----------------------------------------------------------------------------------------------------------------------
!> \brief &STRAIN \details The DOPING parameter block can be used to define doping profiles.
type u_strain
    real(8)                 :: a1=0
    real(8)                 :: a2=0
    real(8)                 :: a3=0
endtype u_strain

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TRAP \details The TRAP parameter block can be used to define trap densities.
type u_trap
    character(len=64)       :: region          !< region of traps
    character(len=64)       :: profile         !< trap profile 
    character(len=64)       :: type            !< energy distribution of traps
    character(len=64)       :: band            !< type of trap band, electrons ('e'/'cb') or holes ('p'/'vb')
    real(8)                 :: d_con           !< trap concentration
    real(8)                 :: e_0             !< [eV] energy offset parameter
    real(8)                 :: sigma           !< parameter for gaussian dos
    real(8)                 :: n_dos           !< parameter for gaussian dos
    integer                 :: fermi           !< if not 0: Fermi distr. else: boltzmann distr.
    character(len=64)       :: dimension       !< dimension
    character(len=64)       :: shape           !< rectangular or cylindric
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
    real(8),dimension(3)    :: xyz_0           !< parameter
    real(8),dimension(3)    :: a_xyz           !< parameter
    real(8),dimension(3)    :: b_xyz           !< exponantial decay factor
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &SEMI \details
type u_semi
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: materialA       !< model name
    character(len=64)       :: materialB       !< model name
    character(len=64)       :: bgn_model       !< model name
    character(len=64)       :: strain          !< model name

    integer                 :: elec            !< if not 0: conduction bands & electron carrier density activated
    integer                 :: elec2           !< if not 0: conduction bands & electron carrier density activated
    integer                 :: hole            !< if not 0: valence bands & hole carrier density activated
    integer                 :: fermi           !< if not 0: Fermi distr. else: boltzmann distr.
    integer                 :: bgn_apparent    !< if not 0: Apparent BGN  else: BGN
    integer                 :: dim             !< transport dimension

    real(8)                 :: eps             !< relative permittivity
    real(8)                 :: eps_c           !< bowing of permittivity

    !bgn model
    !default
    real(8)                 :: bgap_gamhd      !< bandgap narrowing model parameter
    !classic
    real(8)                 :: v_hd0
    real(8)                 :: c_ref
    real(8)                 :: c_hd
    real(8)                 :: bgn_alpha
    !jain
    real(8),dimension(2)    :: bgn_a
    real(8),dimension(2)    :: bgn_b
    real(8),dimension(2)    :: bgn_c

    real(8)                 :: eoff            !< offset of valence band edge
    real(8)                 :: eoff_c          !< bowing of offset

    real(8)                 :: temp0           !< temperature of semiconductor
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CNT \details
type u_cnt
    character(len=64)       :: mod_name        !< model name
    integer                 :: n_1             !< chiral index n1
    integer                 :: n_2             !< chiral index n2
    real(8)                 :: d_cnt           !< [m] diameter of CNT  (is read only if n_1=0 and n_2=0)
    real(8)                 :: a_cc            !< [m] carbon-carbon distance
    real(8)                 :: t_cc            !< [eV] tight binding energy
    real(8)                 :: strain          !< strain
    real(8)                 :: p_r             !< Poisson's ratio
    real(8)                 :: n_cnt           !< number of tubes per meter (for 2D simulations)
    character(len=64)       :: fc_model        !< force constant model parameters
    real(8),dimension(4,3)  :: fc_val          !< force constant values (4 nearest neighbor rings)
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &BAND_DEF \details
type u_banddef
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: valley          !< identifier for the valley
    character(len=64)       :: band            !< conduction of valence band
    character(len=64)       :: type            !< valley model type

    real(8)                 :: e0             !< [eV] half of bandgap
    real(8)                 :: e0_c             !< [eV] half of bandgap
    real(8)                 :: deg             !< degeneracy factor
    real(8)                 :: e0_alpha             !< [eV] half of bandgap
    real(8)                 :: e0_beta             !< [eV] half of bandgap

    real(8)                 :: m_eff           !< effective mass for sphere valleys
    real(8)                 :: m_eff_c         !< bowing of effective mass
    real(8)                 :: al              !< bowing of effective mass
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &SCAT_DEF \details
type u_scatdef
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: scat_name       !< name of scattering process
    character(len=64)       :: type            !< scattering model
    real(8)                 :: mfp             !< scattering rate mean free path
    real(8)                 :: e_ph            !< phonon energy
    real(8)                 :: d_pot           !< interaction potential for phonon scattering
    real(8)                 :: v_s             !< [m/s] sound velocity
    real(8)                 :: eps_0           !< low freq. permittivity
    real(8)                 :: eps_inf         !< high freq. permittivity
    real(8)                 :: p_ii            !< parameter for impact ionization
    real(8)                 :: beta_ii         !< parameter for impact ionization
    character(len=64)       :: abs_ems         !< phonon absorption, emission or both
    character(len=64)       :: fs_bs           !< forward, backward scattering or both
    character(len=64)       :: initial_band    !< initial valley or subband for scattering
    character(len=64)       :: final_band      !< final valley or subband for scattering
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TAU_E \details

type u_taudef
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: valley
    character(len=64)       :: tau_type

    !sige model
    real(8)    :: t0
    real(8)    :: t1
    real(8)    :: c0
    !default model
    real(8)    :: c1
    real(8)    :: c2
    real(8)    :: c3

    !bowing
    real(8)    :: t0_bow
    real(8)    :: c0_bow
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &MOB_DEF \details
type u_mobdef
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: valley
    character(len=64)       :: l_scat_type
    character(len=64)       :: li_scat_type
    character(len=64)       :: hc_scat_type
    character(len=64)       :: v_sat_type
    character(len=64)       :: bowing
    character(len=64)       :: force

    !lattice scattering parameters
    !default
    real(8),dimension(3)    :: mu_l
    !michaillat_n
    real(8),dimension(3)    :: mu_cs
    real(8),dimension(3)    :: z_0
    real(8),dimension(3)    :: beta_l
    !temp
    real(8)                 :: gamma_l

    !impurity scattering
    !caughey thomas
    real(8)                 :: mu_min
    real(8)                 :: c_ref
    real(8)                 :: alpha
    real(8)                 :: gamma_1
    real(8)                 :: gamma_2
    real(8)                 :: gamma_3
    real(8)                 :: gamma_4
    real(8)                 :: gamma_1_a
    real(8)                 :: gamma_2_a
    real(8)                 :: gamma_1_d
    real(8)                 :: gamma_2_d
    real(8)                 :: gamma_3_a
    real(8)                 :: gamma_4_a
    real(8)                 :: gamma_3_d
    real(8)                 :: gamma_4_d
    !reggiani
    real(8)                 :: c_ref_a
    real(8)                 :: c_ref_d
    real(8)                 :: mu_min_a
    real(8)                 :: mu_min_d
    real(8)                 :: alpha_a
    real(8)                 :: alpha_d
    real(8)                 :: gamma
    real(8)                 :: c
    !device
    real(8)                 :: alpha_2
    real(8)                 :: c_ref_2
    real(8)                 :: mu_hd
    real(8)                 :: r

    !to adjust mobility
    real(8)                 :: mu_fac

    !hot carrier scattering
    !default
    !sat
    real(8)                 :: beta
    !ndm
    real(8)                 :: f0
    real(8)                 :: f1
    real(8)                 :: beta1
    real(8)                 :: beta2
    real(8)                 :: beta3
    !ndm
    real(8)                 :: mu_hc

    !saturation velocity
    !default
    real(8)                 :: v_sat
    real(8)                 :: a
    real(8)                 :: c_ref_vsat
    real(8)                 :: al_vsat
    !device
    real(8)                 :: zeta_v
    !driving force
    real(8)                 :: fac_driving_force
    !bowing
    !default
    real(8)              :: mu_bow
    real(8)              :: v_sat_bow
    !michaillat
    real(8)              :: mu_bow1
    real(8)              :: mu_bow2
    real(8)              :: mu_bowy
    real(8)              :: mu_bowxy
    real(8),dimension(4) :: chi
    real(8)              :: z_1
    !device
    real(8)              :: v_sat_a1
    real(8)              :: v_sat_a2
    real(8)              :: mu_min_a1
    real(8)              :: mu_min_a2
    real(8)              :: mu_L_a1
    real(8)              :: mu_L_a2

    real(8) :: mu_max
    real(8) :: meff
    real(8) :: f_e
    real(8) :: f_h
    real(8) :: f_CW
    real(8) :: f_bh
    real(8) :: ag
    real(8) :: bg
    real(8) :: cg
    real(8) :: gammag
    real(8) :: betag
    real(8) :: alphag
    real(8) :: alphag2

endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &RECOMB_DEF \details
type u_recombdef
    character(len=64) :: mod_name = '' !< model name
    character(len=64) :: type = '' !< model name
    character(len=64) :: force = '' !< model name

    !langevin
    real(8)           :: beta = 0

    !srh
    character(len=64) :: lifetime     = '' !< model name
    real(8)           :: e_trap       = 0
    real(8)           :: n_t          = 0
    real(8)           :: beta_n       = 0
    real(8)           :: beta_p       = 0
    real(8)           :: c_ref_n      = 0
    real(8)           :: c_ref_p      = 0
    real(8)           :: tau_n_min    = 0
    real(8)           :: tau_p_min    = 0
    real(8)           :: tau_n_max    = 0
    real(8)           :: tau_p_max    = 0
    real(8)           :: tau_n_min_a1 = 0
    real(8)           :: tau_n_min_a2 = 0
    real(8)           :: tau_p_min_a1 = 0
    real(8)           :: tau_p_min_a2 = 0
    real(8)           :: tau_n_max_a1 = 0
    real(8)           :: tau_n_max_a2 = 0
    real(8)           :: tau_p_max_a1 = 0
    real(8)           :: tau_p_max_a2 = 0
    real(8)           :: sigma_n      = 0
    real(8)           :: sigma_p      = 0
    real(8)           :: zeta_n       = 0
    real(8)           :: zeta_p       = 0

    !auger
    real(8)           :: c_n    = 0
    real(8)           :: c_p    = 0
    real(8)           :: c_n_a1 = 0
    real(8)           :: c_n_a2 = 0
    real(8)           :: c_p_a1 = 0
    real(8)           :: c_p_a2 = 0
    !zetas from srh def

    !avalanche
    real(8)           :: alpha_n  = 0
    real(8)           :: alpha_p  = 0
    real(8)           :: e_crit_n = 0
    real(8)           :: e_crit_p = 0

    !intervalley transfer
    real(8)                 :: v_21    
    real(8)                 :: dv_21    
    real(8)                 :: v21off
    real(8)                 :: dop_crit    
    real(8)                 :: f0
    real(8)                 :: f1
    real(8)                 :: a
    real(8)                 :: af
    real(8)                 :: zeta_dop
    real(8)                 :: xl
    real(8)                 :: xr
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &DEFECT_DEF \details
type u_defectdef
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: type            !< defect model type
    real(8)                 :: x_0             !< position
    real(8)                 :: std             !< width
    real(8)                 :: amp             !< energy
    character(len=1024)     :: file            !< file with defect information
    integer                 :: f_col           !< column
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONTACT \details The CONTACT subblock can be used to define physical models for contacts.
type u_contact
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: con_type        !< contact type \details 'schottky'... Schottky contact <br> 'ohmic'... ohmic contact
    character(len=64)       :: sb_type         !< Schottky barrier type \details 'elec'... electron Schottky barrier <br> 'hole'... hole Schottky barrier <br> 'mid'... midgap <br> 'fermi_diff'... define difference between contact Fermi level and intrinsic semiconductor Fermi level
    real(8)                 :: phi_sb          !< [eV] Schottky barrier height \details depends on setting of 'sb_type'
    character(len=64)       :: ohmic_bc        !< Ohmic boundary condition type
    character(len=64)       :: schottky_bc     !< Schottky boundary condition type
    real(8)                 :: r_con           !< [Ohm] contact resistance
    real(8)                 :: kappa           !< [W/m/K] thermal conductivity of contact
    real(8)                 :: v_n             !< [m/s] recombination velocity for electrons in case of schottky contact
    real(8)                 :: v_n2            !< [m/s] recombination velocity for electrons2 in case of schottky contact
    real(8)                 :: v_p             !< [m/s] recombination velocity for holes in case of schottky contact
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &OXIDE \details The OXIDE subblock can be used to define physical models for oxides.
type u_oxide
    character(len=64)       :: mod_name        !< model name
    real(8)                 :: eps             !< relative permittivity \f$\eps_r\f$
    real(8)                 :: kappa           !< [W/m/K] thermal conductivity
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TBC \details 
type u_tbc
    real(8)                 :: g               !< thermal boundary
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
endtype u_tbc

!----------------------------------------------------------------------------------------------------------------------
!> \brief &HEAT_SOURCE \details 
type u_heatsource
    character(len=64)       :: dimension       !< 0D/1D_x/1D_y/1D_z/2D_xy/2D_xz/2D_yz/3D
    character(len=64)       :: shape           !< rectangular or calindric
    real(8)                 :: p_diss          !< heat generation rate
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
endtype u_heatsource

!----------------------------------------------------------------------------------------------------------------------
!> \brief &HEAT_SINK \details 
type u_heatsink
    real(8)                 :: temp            !< heat generation rate
    real(8),dimension(3)    :: low_xyz         !< lower corner
    real(8),dimension(3)    :: upp_xyz         !< upper corner
endtype u_heatsink

!----------------------------------------------------------------------------------------------------------------------
!> \brief &EFM \details 
type u_efm
    integer                 :: n_iter          !< max iterations for newton
    real(8)                 :: d_tol           !< rel tolerance for adaptive integration
    real(8)                 :: p_tol           !< error limit for electrostatic potential
    real(8)                 :: damp_init       !< initial damping factor
    real(8)                 :: damp_min        !< minimum damping factor
    real(8)                 :: x_min           !< minimum energy width in adaptive integration (eV)
    integer                 :: n_max           !< maximum function evaluations in adaptive integration
    integer                 :: n_pml           !< number of eigenvalues for pml
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &DD \details 
type u_dd
    integer                 :: debug           !< at iteration corresponding to debug, the jacobians and other usefull elpa files are printed out
    integer                 :: n_iter          !< max iterations for newton
    integer                 :: simul           !< if 1: simultaneous solution of poisson,cont_n,cont_p
    real(8)                 :: rth             !< temperature resistance
    real(8)                 :: diffuse         !< if >0: apply diffusion equation to doping in X direction with "diffuse" as diffusion constant
    real(8)                 :: pdiff_tol       !< limit of difference of dpsi_norm to last iteration
    integer                 :: op_simul_start  !< set simul=1 if this op is reached
    character(64)           :: init_qf         !< initialization method for quasi fermi potential
    integer                 :: tun_derive      !< if 1: num. calc. of derivation of tunneling currents added to jacobi matrix
    integer                 :: osz_cut         !< if 1: smooth qusi fermi level during iterations
    integer                 :: elec            !< if 1: electron continuity equation activated
    integer                 :: tl              !< if 1: lattice heat calculated
    integer                 :: tn              !< if 1: electron temperature calculated
    integer                 :: elec2           !< if 1: electron continuity equation activated for second electron band
    integer                 :: hole            !< if 1: hole continuity equation activated
    integer                 :: bohm_n          !< if 1: hole continuity equation activated
    integer                 :: remove_zeros    !< if 1: remove zeros in sparse matrix
    real(8)                 :: rc              !< contact resistance
    real(8)                 :: dqf_dens        !< lower fermi level for carrier density calculation
    integer                 :: take_prev       !< if 1: take quasi fermi level from previous iteration
    character(64)           :: damp_method     !< damping method
    real(8)                 :: damp            !< damping number
    real(8)                 :: damp_min        !< damping number
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &NORMALIZATION \details 
type u_norm
    integer                 :: x_norm         
    character(64)           :: dens_norm
endtype



!----------------------------------------------------------------------------------------------------------------------
!> \brief &BALLBTE \details 
type u_ballbte
    integer                 :: n_iter          !< max iterations for newton
    real(8)                 :: d_tol           !< rel tolerance for adaptive integration
    real(8)                 :: p_tol           !< error limit for electrostatic potential
    real(8)                 :: damp_init       !< initial damping factor
    real(8)                 :: damp_min        !< minimum damping factor
    real(8)                 :: x_min           !< minimum energy width in adaptive integration (eV)
    integer                 :: n_max           !< maximum function evaluations in adaptive integration
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &MCBTE \details 
type u_mcbte
    integer                 :: solve           !< solve MC-BTE if >0
    integer                 :: show            !< show transient output on console MC-BTE if >0
    real(8)                 :: particle_charge !< fraction of charge of a simulation electron particle
    integer                 :: n_particle      !< number of electron particles at start
    integer                 :: n_max           !< maximum number of particles during simulation
    integer                 :: n_min           !< minimum number of particles
    integer                 :: n_target        !< target number of particles
    integer                 :: seed            !< seed for random numbers
    integer                 :: nt_poi          !< number of timesteps between poisson update
    integer                 :: nt_par          !< number of timesteps between particle fraction update
    integer                 :: nt_pauli        !< number of timesteps used for actual distribution estimate
    integer                 :: init_subband    !< initial subband for particle(s)
    real(8)                 :: init_energy     !< initial energy for particle(s)
    character(len=64)       :: init_distr      !< method to define initial distribution
    character(len=1024)     :: file            !< read distribution from file
    integer                 :: take_prev       !< take distribution function from last operation point as initial distribution
    integer                 :: scat_table      !< if ~0 calculate scattering rates before simulation and store them
    integer                 :: scat_ne         !< distance of energy points for scattering rate table
    real(8)                 :: scat_emax       !< maximum energy for scattering rate table
    real(8)                 :: scatrate_max    !< maximum value for scattering rate
    real(8)                 :: scatrate_min    !< minimum value for scattering rate
    integer                 :: pauli           !< if 1: pauli blocking active
    integer                 :: scat_virtual    !< if 1: virtual scattering mechanisms activated for pauli blocking
    real(8)                 :: pauli_dk        !< dk for pauli blocking
    real(8)                 :: thermionic_dk   !< dk for integration for thermionic injection
    real(8)                 :: tunnel_de       !< de for tunneling
    real(8)                 :: tunnel_emax     !< emax for tunneling
    real(8)                 :: e_cut           !< energy intervall for metallic tubes
    integer                 :: adapt_dt        !< change dt for bulk simulations
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &DETBTE \details 
type u_detbte
    integer                 :: solve           !< solve DET-BTE if >0
    real(8)                 :: kmax            !< maximum k-value
    integer                 :: nk              !< number of k-values
    integer                 :: check_dt        !< if 1: adapt time step, if f<0
    character(len=64)       :: init_distr      !< method to define initial distribution
    integer                 :: adapt_dt        !< change dt for bulk simulations
    integer                 :: take_prev       !< take distribution function from last operation point as initial distribution
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &POISSON \details 
type u_poisson
    integer                 :: x_periodic      !< if ~=0 periodic in x-direction
    integer                 :: y_periodic      !< if ~=0 periodic in y-direction
    integer                 :: z_periodic      !< if ~=0 periodic in z-direction
    character(len=64)       :: init_psi        !< initialization method for electrostatic potential
    real(8)                 :: lambda          !< parameter for given electrostatic potential
    real(8)                 :: atol
    real(8)                 :: rtol
    real(8)                 :: delta_max
    real(8)                 :: ctol
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TL \details 
type u_cont_tl
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONT_ELEC \details 
type u_cont_elec
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONT_TN \details 
type u_cont_tn
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype
!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONT_BOHM_N \details 
type u_cont_bohm_n
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONT_ELEC2 \details 
type u_cont_elec2
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &CONT_ELEC \details 
type u_cont_hole
    real(8)                 :: atol       !< absolute tolerance lattice temperature
    real(8)                 :: rtol       !< relative tolerance lattice temperature
    real(8)                 :: ctol       !< tolerance rhs
    real(8)                 :: delta_max       !< tolerance rhs
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TUNNEL \details 
type u_tunnel
    character(len=64)       :: model           !< if 'on': tunneling enabled
    real(8)                 :: max_width       !< T=0 for longer barriers then xbmax
    integer                 :: n_barrier       !< maximum number of barriers
    integer                 :: bbt             !< if 1: band to band tunneling active
    real(8)                 :: de_min          !< T=0 for longer barriers then xbmax
    integer                 :: e_add           !< T=0 for longer barriers then xbmax
    real(8)                 :: factor          !< scaling of transmission probability
    real(8)                 :: dqf_tun         !< lowing of fermi level for tunneling
    real(8)                 :: x_start         !< lowing of fermi level for tunneling
    real(8)                 :: x_end           !< lowing of fermi level for tunneling
    real(8)                 :: activate        !< if >0 : tunneling activated
    real(8)                 :: m               !< tunneling effective mass
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &HEATFLOW \details 
type u_heatflow
    real(8)                 :: d_t             !< time step size
    integer                 :: dim             !< dimension
    real(8)                 :: g               !< thermal conductance substrate
    real(8)                 :: g_con           !< thermal conductance substrate
    real(8)                 :: g_inf           !< thermal conductance substrate
    real(8)                 :: damp            !< damping factor
    integer                 :: save
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &BIAS_DEF \details
type u_biasdef
    integer                 :: list            !< how to interprete the BIAS_INFO blocks
    character(len=64)       :: random          !< add noise to bias in bulk simulations
    real(8)                 :: dv_max          !< maximum change of bias voltage between two operation points
    real(8)                 :: dv_min          !< minimum change of bias voltage between two operation points
    integer                 :: dv_adapt        !< if 1: adaptive voltage step on
    integer                 :: zero_bias_first !< if 1: computer solution with zero bias first
    real(8)                 :: d_t             !< size of tie step for transient simulations
    character(len=64)       :: t_func_type     !< interval method for time
    real(8),dimension(3,20) :: t_val           !< parameter used for logarithmic time stepping between 1e(tval(1)) ro 1e(tval(3)) similar to frequency
    integer                 :: n_t             !< total number of time steps
    integer                 :: displ           !< if 1: consider displacement currents
    character(len=64)       :: ssd_type        !< steady state detection method
    integer                 :: n_cross         !< max number of mean crossings
    real(8)                 :: batch_size      !< batch size
    integer                 :: n_batch         !< number of past batches for mean test
    real(8)                 :: rel_error       !< relative error border
    real(8)                 :: rel_initial     !< fraction of initial particles
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &BIAS_INFO \details
type u_biasinfo
    character(len=64)       :: cont_name       !< contact name
    character(len=64)       :: bias_fun        !< interval method
    real(8),dimension(3,20) :: bias_val        !< bias values
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &TR_INFO \details
type u_trinfo
    character(len=64)       :: cont_name       !< contact name
    character(len=64)       :: func_type       !< interval method
    real(8),dimension(20)   :: v_max           !< amplitude
    real(8),dimension(20)   :: T               !< periode
    real(8),dimension(20)   :: phase           !< phase
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &AC_INFO \details
type u_acinfo
    character(len=64)       :: port1           !< contact name port 1
    character(len=64)       :: port2           !< contact name port 2
    character(len=64)       :: sweep_type      !< interval method
    real(8),dimension(3,20) :: freq_val        !< bias values
endtype

!----------------------------------------------------------------------------------------------------------------------
!> \brief &OUTPUT \details
type u_output
    character(len=1024)     :: name            !< \brief Output file name \details This string is used for all output files
    character(len=1024)     :: path            !< output path
    integer                 :: digits          !< number of digits for output
    integer                 :: elpa            !< If >=1: Write elpa output
    integer                 :: inqu_lev        !< internal quantities
    integer                 :: psi_lev         !< saving of electrostatic potential
    integer                 :: tr_elpa_lev     !< electrical parameters for transient simulation
    integer                 :: tr_inqu_lev     !< internal quantities for transient simulation
    integer                 :: cap_lev         !< save external capacitances
    integer                 :: newton_lev      !< save iterations in newton iteration
endtype


!----------------------------------------------------------------------------------------------------------------------
!> \brief user parameter > \details 
type user 
    type(u_regioninfo)                :: regioninfo    !< &REGION_INFO parameters
    type(u_regiondef), dimension(100) :: regiondef     !< &REGION_DEF parameters
    integer                           :: n_regiondef =0!< number of REGION_DEF blocks

    type(u_nonlocal)                  :: nonlocal     !< &NON_LOCAL parameters
    
    type(u_rangegrid), dimension(100) :: rangegrid       !< &RANGE_GRID parameters
    integer                           :: n_rangegrid  =0 !< number of RANGE_GRID blocks
    type(u_areagrid),  dimension(100) :: areagrid        !< &AREA_GRID parameters
    integer                           :: n_areagrid   =0 !< number of AREA_GRID blocks

    type(u_strain),    dimension(100) :: strain        !< &DOPING parameters
    integer                           :: n_strain   =0 !< number of &DOPING blocks
    
    type(u_doping),    dimension(100) :: doping        !< &DOPING parameters
    integer                           :: n_doping   =0 !< number of &DOPING blocks
    type(u_grading),   dimension(100) :: grading       !< &GRADING parameters
    integer                           :: n_grading  =0 !< number of &GRADING blocks
    type(u_trap),      dimension(100) :: trap          !< &TRAP parameters
    integer                           :: n_trap     =0 !< number of &TRAP blocks

    type(u_semi),      dimension(100) :: semi            !< &SEMI parameters
    integer                           :: n_semi    = 0   !< number of &SEMI blocks
    type(u_cnt),       dimension(100) :: cnt             !< &CNT parameters
    integer                           :: n_cnt     = 0   !< number of &CNT blocks
    type(u_banddef),   dimension(100) :: banddef         !< &BAND_DEF parameters
    integer                           :: n_banddef = 0   !< number of &BAND_DEF blocks
    type(u_scatdef),   dimension(100) :: scatdef         !< &SCAT_DEF parameters
    integer                           :: n_scatdef = 0   !< number of &SCAT_DEF blocks
    type(u_mobdef),    dimension(100) :: mobdef          !< &MOB_DEF parameters
    integer                           :: n_mobdef  = 0   !< number of &MOB_DEF blocks
    type(u_taudef),    dimension(100) :: taudef          !< &TAU_E parameters
    integer                           :: n_taudef  = 0   !< number of &TAU_E blocks
    type(u_recombdef), dimension(100) :: recombdef       !< &RECOMB_DEF parameters
    integer                           :: n_recombdef = 0 !< number of &RECOMB_DEF blocks
    type(u_defectdef), dimension(100) :: defectdef       !< &DEFECT_DEF parameters
    integer                           :: n_defectdef =0  !< number of &DEFECT_DEF blocks
    
    type(u_oxide),     dimension(100) :: oxide           !< &OXIDE parameters
    integer                           :: n_oxide    =0   !< number of &OXIDE blocks
    type(u_supply),    dimension(100) :: supply          !< &SUPPLY parameters
    integer                           :: n_supply    =0  !< number of &SUPPLY blocks        
    
    type(u_contact),   dimension(100) :: contact         !< &CONTACT parameters
    integer                           :: n_contact   =0  !< number of &CONTACT blocks
    type(u_biasdef)                   :: biasdef         !< &BIAS_DEF parameters
    type(u_biasinfo),  dimension(100) :: biasinfo        !< BIAS_INFO parameters
    integer                           :: n_biasinfo  =0  !< number of &BIAS_INFO blocks
    type(u_trinfo)  ,  dimension(100) :: trinfo          !< &TR_INFO parameters
    integer                           :: n_trinfo    =0  !< number of &TR_INFO blocks
    type(u_acinfo)                    :: acinfo          !< &AC_INFO parameters
    
    type(u_thermionic_emission),dimension(100)       :: thermionic_emission       !< &POISSON parameters for Poisson equation
    integer                           :: n_thermionic_emission  =0    !< number of &TR_INFO blocks
    type(u_cont_tl)                   :: cont_tl       !< &POISSON parameters for Poisson equation
    type(u_cont_elec)                 :: cont_elec     !< &POISSON parameters for Poisson equation
    type(u_cont_tn)                   :: cont_tn       !< &POISSON parameters for Poisson equation
    type(u_cont_bohm_n)               :: cont_bohm_n   !< &POISSON parameters for Poisson equation
    type(u_cont_hole)                 :: cont_hole     !< &POISSON parameters for Poisson equation
    type(u_cont_elec2)                :: cont_elec2    !< &POISSON parameters for Poisson equation
    type(u_poisson)                   :: poisson       !< &POISSON parameters for Poisson equation
    type(u_dd)                        :: dd            !< &DD parameters for drift diffusion solver
    type(u_norm)                      :: norm          !< &DD parameters for drift diffusion solver
    type(u_efm)                       :: efm           !< &EFM parameters for effective mass SE solver
    type(u_ballbte)                   :: ballbte       !< &BALL_BTE parameters for deterministic BTE solver
    type(u_mcbte)                     :: mcbte         !< &MC_BTE parameters for monte carlo BTE solver
    type(u_detbte)                    :: detbte        !< &DET_BTE parameters for deterministic BTE solver
    type(u_tunnel)                    :: tunnel        !< &TUNNEL parameters for Transmission calculation with WKB method
    
    type(u_heatflow)                  :: heatflow      !< &HEAT_FLOW parameters for heatflow equation
    type(u_tbc),       dimension(100) :: tbc           !< &TBC parameters
    integer                           :: n_tbc        =0   !< number of &TBC blocks
    type(u_heatsource),dimension(100) :: heatsource    !< &HEAT_SOURCE parameters
    integer                           :: n_heatsource =0 !< number of &HEAT_SOURCE blocks
    type(u_heatsink),  dimension(100) :: heatsink      !< &HEAT_SINK parameters
    integer                           :: n_heatsink   =0 !< number of &HEAT_SINK blocks
    
    type(u_output)                    :: output        !< &OUTPUT parameters
endtype

! --- global module parameters ----------------------------------------------------------------------------------------
type(user) :: US  !< all user parameters are stored in this variable

contains

subroutine init_user(path)
    !!!!!!!!!!!!!!!!!!!!!!
    ! init the US variable
    !!!!!!!!!!!!!!!!!!!!!!
    character(len=1024),dimension(128) :: arg
    character(len=1024)                :: line
    character(len=1024),optional       :: path !direct path to file
    integer                            :: i,io
    integer                            :: inp_id

    ! --> file id for input file
    inp_id = 1 

    if (present(path)) then
        ! --> file name from direct argument
        i = 1
        arg(1) = trim(path)

    else
        ! --> file name from get command line ----------------------
        i=0
        do
            i=i+1
            call get_command_argument(i,arg(i));
            if (len_trim(arg(i))==0) then
                i=i-1
                exit
            else
                if (i.eq.1) then
                    ! file found
                endif
            endif
        enddo

        ! ---> try debug mode  -----------------------
        if (i.lt.1) then ! no input file in command line
            open(inp_id,file=trim('hdev.debug'),status='old',action='read',iostat=io);
            if (io.eq.0) then
                write(*,*) 'debug mode'
                read(inp_id,'(A)') arg(1) ! set arg(1) by hand
                i = 1
                close(inp_id)
            endif
        endif

    endif

    ! ---> stop if no input file defined --------------------
    if (i.lt.1) then
        write(*,*) '***error***  no inputfile'
        stop
    endif

    ! ---> open input file -------------------
    write(*,'(A,A)') ' User input file: ',trim(arg(i))
    open(inp_id,file=trim(arg(1)),status='old',action='read',iostat=io);
    if (io.ne.0) then
        write(*,*) '***error*** ',trim(arg(1)), ' does not exist'
        stop
    endif

    ! ---> set default parameters
    call user_default

    ! --> read parameters for every block
    do
        read(inp_id,'(A)',iostat=io) line
        write(line,'(A)') trim(adjustl(line))
        if ((io.eq.0).and.(len_trim(line).ge.3)) then
            ! --> line is ok
            if     (line(1:12).eq.'&REGION_INFO') then
                call read_regioninfo(US%regioninfo,inp_id)
            elseif     (line(1:10).eq.'&NON_LOCAL') then
                call read_nonlocal(US%nonlocal,inp_id)

            elseif (line(1:11).eq.'&REGION_DEF') then
                US%n_regiondef  = US%n_regiondef + 1
                call read_regiondef(US%regiondef(US%n_regiondef),inp_id)
              
            elseif (line(1:11).eq.'&RANGE_GRID') then
                US%n_rangegrid  = US%n_rangegrid + 1
                call read_rangegrid(US%rangegrid(US%n_rangegrid),inp_id)

            elseif (line(1:10).eq.'&AREA_GRID') then
                US%n_areagrid  = US%n_areagrid + 1
                call read_areagrid(US%areagrid(US%n_areagrid),inp_id)

            elseif (line(1:8 ).eq.'&CONTACT') then
                US%n_contact  = US%n_contact + 1
                call read_contact(US%contact(US%n_contact),inp_id)

            elseif (line(1:7 ).eq.'&SUPPLY') then
                US%n_supply  = US%n_supply + 1
                call read_supply(US%supply(US%n_supply),inp_id)
              
            elseif (line(1:6 ).eq.'&OXIDE') then
                US%n_oxide  = US%n_oxide + 1
                call read_oxide(US%oxide(US%n_oxide),inp_id)

            elseif (line(1:20).eq.'&THERMIONIC_EMISSION') then
                US%n_thermionic_emission  = US%n_thermionic_emission + 1
                call read_thermionic_emission(US%thermionic_emission(US%n_thermionic_emission),inp_id)
            
            elseif (line(1:5 ).eq.'&SEMI') then
                US%n_semi  = US%n_semi + 1
                call read_semi(US%semi(US%n_semi),inp_id)
            
            ! elseif (line(1:4 ).eq.'&CNT') then
            !   US%n_cnt  = US%n_cnt + 1
            !   call read_cnt(US%cnt(US%n_cnt),inp_id)

            elseif (line(1:9 ).eq.'&BAND_DEF') then
                US%n_banddef  = US%n_banddef + 1
                call read_banddef(US%banddef(US%n_banddef),inp_id)

            ! elseif (line(1:9 ).eq.'&SCAT_DEF') then
            !   US%n_scatdef  = US%n_scatdef + 1
            !   call read_scatdef(US%scatdef(US%n_scatdef),inp_id)

            elseif (line(1:8 ).eq.'&MOB_DEF') then
                US%n_mobdef  = US%n_mobdef + 1
                call read_mobdef(US%mobdef(US%n_mobdef),inp_id)
            elseif (line(1:6 ).eq.'&TAU_E') then
                US%n_taudef  = US%n_taudef + 1
                call read_taudef(US%taudef(US%n_taudef),inp_id)

            elseif (line(1:11).eq.'&RECOMB_DEF') then
                US%n_recombdef  = US%n_recombdef + 1
                call read_recombdef(US%recombdef(US%n_recombdef),inp_id)

            ! elseif (line(1:11).eq.'&DEFECT_DEF') then
            !   US%n_defectdef  = US%n_defectdef + 1
            !   call read_defectdef(US%defectdef(US%n_defectdef),inp_id)

            elseif (line(1:7 ).eq.'&DOPING') then
                US%n_doping  = US%n_doping + 1
                call read_doping(US%doping(US%n_doping),inp_id)

            elseif (line(1:7 ).eq.'&STRAIN') then
                US%n_strain  = US%n_strain + 1
                call read_strain(US%strain(US%n_strain),inp_id)

            elseif (line(1:8 ).eq.'&GRADING') then
                US%n_grading = US%n_grading + 1
                call read_grading(US%grading(US%n_grading),inp_id)
        
            ! elseif (line(1:5 ).eq.'&TRAP') then
            !   US%n_trap  = US%n_trap + 1
            !   call read_trap(US%trap(US%n_trap),inp_id)

            elseif (line(1:4 ).eq.'&TBC') then
                US%n_tbc  = US%n_tbc + 1
                call read_tbc(US%tbc(US%n_tbc),inp_id)

            elseif (line(1:12).eq.'&HEAT_SOURCE') then
                US%n_heatsource  = US%n_heatsource + 1
                call read_heatsource(US%heatsource(US%n_heatsource),inp_id)

            elseif (line(1:10).eq.'&HEAT_SINK') then
                US%n_heatsink  = US%n_heatsink + 1
                call read_heatsink(US%heatsink(US%n_heatsink),inp_id)

            ! elseif (line(1:4 ).eq.'&EFM') then
            !   call read_efm(US%efm,inp_id)
            
            elseif (line(1:6 ).eq.'&SOLVE') then
                call read_dd(US%dd,.true.,inp_id)
            elseif (line(1:3 ).eq.'&DD') then
                call read_dd(US%dd, .false., inp_id)
            elseif (line(1:14).eq.'&NORMALIZATION') then
                call read_norm(US%norm,inp_id)
            ! elseif (line(1:9 ).eq.'&BALL_BTE') then
            !   call read_ballbte(US%ballbte,inp_id)

            elseif (line(1:8 ).eq.'&POISSON') then
                call read_poisson(US%poisson,inp_id)
            elseif (line(1:8 ).eq.'&CONT_TL') then
                call read_cont_tl(US%cont_tl,inp_id)
            elseif (line(1:10).eq.'&CONT_HOLE') then
                call read_cont_hole(US%cont_hole,inp_id)
            elseif (line(1:11).eq.'&CONT_ELEC ') then
                call read_cont_elec(US%cont_elec,inp_id)
            elseif (line(1:8).eq.'&CONT_TN') then
                call read_cont_tn(US%cont_tn,inp_id)
            elseif (line(1:13).eq.'&CONT_BOHM_N ') then
                call read_cont_bohm_n(US%cont_bohm_n,inp_id)
            elseif (line(1:11 ).eq.'&CONT_ELEC2') then
                call read_cont_elec2(US%cont_elec2,inp_id)
            ! elseif (line(1:8 ).eq.'&DET_BTE') then
            !   call read_detbte(US%detbte,inp_id)

            elseif (line(1:7 ).eq.'&MC_BTE') then
                call read_mcbte(US%mcbte,inp_id)
              
            elseif (line(1:7 ).eq.'&TUNNEL') then
                call read_tunnel(US%tunnel,inp_id)

            elseif (line(1:9 ).eq.'&HEATFLOW') then
                call read_heatflow(US%heatflow,inp_id)

            elseif (line(1:9 ).eq.'&BIAS_DEF') then
                call read_biasdef(US%biasdef,inp_id)

            elseif (line(1:10).eq.'&BIAS_INFO') then
                US%n_biasinfo  = US%n_biasinfo + 1
                call read_biasinfo(US%biasinfo(US%n_biasinfo),inp_id)

            elseif (line(1:8 ).eq.'&TR_INFO') then
                US%n_trinfo  = US%n_trinfo + 1
                call read_trinfo(US%trinfo(US%n_trinfo),inp_id)

            elseif (line(1:8 ).eq.'&AC_INFO') then
                call read_acinfo(US%acinfo,inp_id)

            elseif (line(1:7 ).eq.'&OUTPUT') then
                call read_output(US%output,inp_id)

            else
                ! skip line

            endif


        elseif (io.gt.0) then
            write(*     ,*) 'cannot read input file line: ',line
            exit

        elseif (io.lt.0) then
            ! end of file reached
            exit

        endif
    enddo

    close(inp_id)

endsubroutine


subroutine user_default
    ! init the default parameters in the US struct
    integer :: i,n

    ! --> REGION_INFO block
    call read_regioninfo(US%regioninfo)

    ! --> NON_LOCAL block
    call read_nonlocal(US%nonlocal)

    ! --> REGION_DEF block
    do i=1,size(US%regiondef)
        call read_regiondef(US%regiondef(i))
    enddo
    US%n_regiondef = 0

    ! --> RANGE_GRID block
    do i=1,size(US%rangegrid)
        call read_rangegrid(US%rangegrid(i))
    enddo
    US%n_rangegrid = 0

    ! --> AREA_GRID block
    do i=1,size(US%areagrid)
        call read_areagrid(US%areagrid(i))
    enddo
    US%n_areagrid = 0

    ! --> CONTACT block
    do i=1,size(US%contact)
        call read_contact(US%contact(i))
    enddo
    n = 0
    n = n+1
    US%contact(n)%mod_name    = 'default'
    US%contact(n)%con_type    = 'schottky'
    US%contact(n)%sb_type     = 'elec'
    US%contact(n)%phi_sb      = 0.1
    US%contact(n)%ohmic_bc    = 'setfloat'
    US%contact(n)%schottky_bc = 'velocity'
    US%contact(n)%r_con       = 0
    US%contact(n)%kappa       = 80
    n = n+1
    US%contact(n)%mod_name    = 'gate'
    US%contact(n)%con_type    = 'schottky'
    US%contact(n)%sb_type     = 'mid'
    US%contact(n)%phi_sb      = 0
    US%contact(n)%r_con       = 0
    US%contact(n)%kappa       = 80

    US%n_contact = n

    ! --> OXIDE block
    do i=1,size(US%oxide)
        call read_oxide(US%oxide(i))
    enddo
    n = 0
    n = n+1
    US%oxide(n)%mod_name = 'sio2'
    US%oxide(n)%eps      = 3.9
    US%oxide(n)%kappa    = 1.3
    n = n+1
    US%oxide(n)%mod_name = 'hfo2'
    US%oxide(n)%eps      = 16
    US%oxide(n)%kappa    = 10
    n = n+1
    US%oxide(n)%mod_name = 'al2o3'
    US%oxide(n)%eps      = 9
    US%oxide(n)%kappa    = 10
    n = n+1
    US%oxide(n)%mod_name = 'quartz'
    US%oxide(n)%eps      = 3.8
    US%oxide(n)%kappa    = 10
    n = n+1
    US%oxide(n)%mod_name = 'si'
    US%oxide(n)%eps      = 11.68
    US%oxide(n)%kappa    = 150
    n = n+1
    US%oxide(n)%mod_name = 'air'
    US%oxide(n)%eps      = 1
    US%oxide(n)%kappa    = 0.026
    n = n+1
    US%oxide(n)%mod_name = 'si3n4'
    US%oxide(n)%eps      = 7.5
    US%oxide(n)%kappa    = 0.026
    n = n+1

    US%n_oxide = n

    ! ! --> SEMI block
    ! do i=1,size(US%semi)
    !   call read_semi(US%semi(i))
    ! enddo
    ! US%n_semi = 1

    ! --> CNT block
    do i=1,size(US%cnt)
        call read_cnt(US%cnt(i))
    enddo
    US%n_cnt = 0

    ! --> BAND_DEF block
    do i=1,size(US%banddef)
        call read_banddef(US%banddef(i))
    enddo
    US%n_banddef = 0

    ! --> SCAT_DEF block
    do i=1,size(US%scatdef)
        call read_scatdef(US%scatdef(i))
    enddo
    US%n_scatdef = 0

    ! Markus: no more defaults in Fortran. Use Python if this feature is wanted!
    ! ! --> MOB_DEF block
    ! do i=1,size(US%mobdef)
    !   call read_mobdef(US%mobdef(i))
    ! enddo
    ! US%n_mobdef = 1

    ! --> RECOMB_DEF block
    do i=1,size(US%recombdef)
        call read_recombdef(US%recombdef(i))
    enddo
    US%n_recombdef = 0

    ! --> DEFECT_DEF block
    do i=1,size(US%defectdef)
        call read_defectdef(US%defectdef(i))
    enddo
    US%n_defectdef = 0

    ! --> DOPING block
    do i=1,size(US%doping)
        call read_doping(US%doping(i))
    enddo
    US%n_doping = 0

    ! --> STRAIN block
    do i=1,size(US%strain)
        call read_strain(US%strain(i))
    enddo
    US%n_strain = 0

    ! --> DOPING block
    do i=1,size(US%grading)
        call read_grading(US%grading(i))
    enddo
    US%n_grading = 0

    ! --> TRAP block
    do i=1,size(US%trap)
        call read_trap(US%trap(i))
    enddo
    US%n_trap = 0

    ! --> TBC block
    do i=1,size(US%tbc)
        call read_tbc(US%tbc(i))
    enddo
    US%n_tbc = 0

    ! --> HEAT_SOURCE block
    do i=1,size(US%heatsource)
        call read_heatsource(US%heatsource(i))
    enddo
    US%n_heatsource = 0

    ! --> HEAT_SINK block
    do i=1,size(US%heatsink)
        call read_heatsink(US%heatsink(i))
    enddo
    US%n_heatsink = 0

    ! --> EFM block
    call read_efm(US%efm)

    ! --> DD block
    call read_dd(US%dd,.true.)

    ! --> NORMALIZATION block
    call read_norm(US%norm)

    ! --> BALL_BTE block
    call read_ballbte(US%ballbte)

    ! --> MC_BTE block
    call read_mcbte(US%mcbte)

    ! --> POISSON block
    call read_poisson(US%poisson)

    ! --> POISSON block
    call read_cont_hole(US%cont_hole)
    call read_cont_elec(US%cont_elec)
    call read_cont_bohm_n(US%cont_bohm_n)
    call read_cont_elec2(US%cont_elec2)
    call read_cont_tl(US%cont_tl)
    call read_cont_tn(US%cont_tn)

    ! --> TUNNEL block
    call read_tunnel(US%tunnel)

    ! --> HEATFLOW block
    call read_heatflow(US%heatflow)

    ! --> BIAS_DEF block
    call read_biasdef(US%biasdef)

    ! --> BIAS_INFO block
    do i=1,size(US%biasinfo)
        call read_biasinfo(US%biasinfo(i))
    enddo
    US%n_biasinfo = 0

    ! --> TR_INFO block
    do i=1,size(US%trinfo)
        call read_trinfo(US%trinfo(i))
    enddo
    US%n_trinfo = 0

    ! --> AC_INFO block
    call read_acinfo(US%acinfo)

    ! --> OUTPUT block
    call read_output(US%output)

endsubroutine

subroutine read_nonlocal(nonlocal,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &NON_LOCAL parameters.
    ! 
    ! input
    ! -----
    ! nonlocal : u_nonlocal
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_nonlocal),intent(out)          :: nonlocal !< struct containing user inputs
    integer,           intent(in ),optional :: inp_id     !< file id for user input file

    integer          :: io

    character(len=20) :: type
    real(8)          :: xj_l,xj_r,af,ai,ymax,nl_base,lambda,fmax,beta ! integer
    NAMELIST /NON_LOCAL/type,xj_l,af,ai,ymax,xj_r,nl_base,lambda,fmax,beta


    ! --> default parameter
    type     = 'none'
    xj_l     = 0
    af       = dble(1)
    ai       = dble(1)
    xj_r     = 0
    ymax     = 1
    nl_base  = 0
    lambda   = 0
    fmax     = 0
    beta     = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,NON_LOCAL,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &NON_LOCAL parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(type)

    ! --> save parameters in struct
    nonlocal%type = type
    nonlocal%xj_l = xj_l
    nonlocal%xj_r = xj_r
    nonlocal%nl_base = nl_base
    nonlocal%lambda = lambda
    nonlocal%fmax   = fmax
    nonlocal%beta   = beta
    nonlocal%ymax   = ymax
    nonlocal%af     = af
    nonlocal%ai     = ai

endsubroutine

subroutine read_regioninfo(regioninfo,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &REGION_INFO parameters.
    ! 
    ! input
    ! -----
    ! regiononfo : u_regioninfo
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_regioninfo),intent(out)          :: regioninfo !< struct containing user inputs
    integer,           intent(in ),optional :: inp_id     !< file id for user input file

    integer          :: io

    character(len=64):: hdf
    real(8)          :: spat_dim,pnts_max ! integer
    NAMELIST /REGION_INFO/spat_dim,pnts_max,hdf


    ! --> default parameter
    spat_dim = 0
    hdf      = '-'
    pnts_max = 200000

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,REGION_INFO,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &REGION_INFO parameters'
            stop
        endif
    endif

    ! --> convert to lower characters

    ! --> save parameters in struct
    regioninfo%spat_dim = int(spat_dim)
    ! coor sys removed as not needed for HBTs => always cart
    ! regioninfo%coor_sys = coor_sys
    regioninfo%hdf      = hdf
    regioninfo%pnts_max = int(pnts_max)

endsubroutine


subroutine read_regiondef(regiondefs,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &REGION_DEF parameters.
    ! 
    ! input
    ! -----
    ! regiondefs : u_regiondef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_regiondef),intent(out)          :: regiondefs !< struct containing user inputs
    integer,          intent(in ),optional :: inp_id     !< file id for input file

    integer              :: io

    character(len=64)    :: mod_name,reg_mat,cont_name
    real(8),dimension(3) :: low_xyz,upp_xyz
    real(8)              :: layer ! integer
    NAMELIST /REGION_DEF/reg_mat,mod_name,low_xyz,upp_xyz,cont_name,layer

    ! --> default parameter
    reg_mat   = ''
    mod_name  = 'default'
    low_xyz   = 0
    upp_xyz   = 0
    cont_name = ''
    layer     = 0

    if (present(inp_id)) then
        ! --> read user parameter
        backspace(inp_id)
        read(inp_id,REGION_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &REGION_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(reg_mat)
    call to_lower(mod_name)
    call to_lower(cont_name)

    ! --> save parameters in struct
    regiondefs%reg_mat   = reg_mat
    regiondefs%mod_name  = mod_name
    regiondefs%cont_name = cont_name
    regiondefs%low_xyz   = low_xyz
    regiondefs%upp_xyz   = upp_xyz
    regiondefs%layer     = int(layer)
endsubroutine

subroutine read_rangegrid(rangegrid,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &RANGE_GRID parameters.
    ! 
    ! input
    ! -----
    ! rangegrid : u_rangegrid
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_rangegrid),intent(out)          :: rangegrid !< struct containing user inputs
    integer,          intent(in ),optional :: inp_id    !< file id for user input file

    integer               :: io

    character(len=64)     :: disc_dir,disc_set
    real(8),dimension(20) :: intv_pnts,intv_diff
    real(8),dimension(20) :: n_pnts              ! integer
    NAMELIST /RANGE_GRID/disc_dir,disc_set,intv_pnts,intv_diff,n_pnts

    ! --> default parameter
    disc_dir  = 'x'
    disc_set  = 'diff'
    intv_pnts = -9.9D90
    intv_diff = -9.9D90
    n_pnts    = -1

    if (present(inp_id)) then
    ! --> read user parameter
      backspace(inp_id)
      read(inp_id,RANGE_GRID,iostat=io)
      if (io.ne.0) then
        write(*,*) '***error*** reading &RANGE_GRID parameters'
        stop
      endif
    endif

    ! --> convert to lower characters
    call to_lower(disc_dir)
    call to_lower(disc_set)

    ! --> save parameters in struct
    rangegrid%disc_dir  = disc_dir
    rangegrid%disc_set  = disc_set
    rangegrid%intv_pnts = intv_pnts
    rangegrid%intv_diff = intv_diff
    rangegrid%n_pnts    = int(n_pnts)

endsubroutine


subroutine read_areagrid(areagrid,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &AREA_GRID parameters.
    ! 
    ! input
    ! -----
    ! areagrid : u_areagrid
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_areagrid),intent(out)          :: areagrid !< struct containing user inputs
    integer,         intent(in ),optional :: inp_id   !< file id for user input file

    integer                 :: io

    character(len=64)       :: disc_dir
    real(8),dimension(20)   :: intv_pnts
    real(8),dimension(3,20) :: intv_diff
    NAMELIST /AREA_GRID/disc_dir,intv_pnts,intv_diff

    ! --> default parameter
    disc_dir  = 'x'
    intv_pnts = -9.9D90
    intv_diff = -9.9D90

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,AREA_GRID,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &AREA_GRID parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(disc_dir)

    ! --> save parameters in struct
    areagrid%disc_dir  = disc_dir
    areagrid%intv_pnts = intv_pnts
    areagrid%intv_diff = intv_diff
endsubroutine


subroutine read_doping(dopings,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &DOPING parameters.
    ! 
    ! input
    ! -----
    ! dopings : u_doping
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_doping),intent(out)          :: dopings !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer              :: io

    character(len=64)    :: profile
    real(8)              :: d_con
    real(8),dimension(3) :: low_xyz,upp_xyz,xyz_0,a_xyz,b_xyz,sigma_xyz
    NAMELIST /DOPING/profile,d_con,xyz_0,a_xyz,b_xyz,sigma_xyz,low_xyz,upp_xyz

    ! --> default parameter
    profile   = 'const'
    d_con     = 0
    xyz_0     = 0
    a_xyz     = 1
    b_xyz     = 1
    sigma_xyz = 1
    low_xyz   = -1
    upp_xyz   = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,DOPING,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &DOPING parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(profile)

    ! --> save parameters in struct
    dopings%profile   = profile
    dopings%d_con     = d_con
    dopings%xyz_0     = xyz_0
    dopings%a_xyz     = a_xyz
    dopings%b_xyz     = b_xyz
    dopings%sigma_xyz = sigma_xyz
    dopings%low_xyz   = low_xyz
    dopings%upp_xyz   = upp_xyz

endsubroutine

subroutine read_grading(gradings,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &GRADING parameters.
    ! 
    ! input
    ! -----
    ! gradings : u_grading
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_grading),intent(out)         :: gradings!< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer              :: io

    character(len=64)    :: profile
    character(len=64)    :: mat_type
    real(8)              :: c_max,c_min
    real(8),dimension(10) :: x_vec, y_vec
    real(8),dimension(3) :: low_xyz,sigma_xyz,upp_xyz,xyz_dlow,xyz_dupp,xyz_0,xyz_rlow,xyz_rupp,a_xyz,b_xyz
    NAMELIST /GRADING/profile,mat_type,x_vec,y_vec,c_max,c_min,xyz_0,a_xyz,b_xyz,sigma_xyz,low_xyz,upp_xyz,xyz_dlow,xyz_dupp,xyz_rlow,xyz_rupp

    ! --> default parameter
    profile   = 'const'
    mat_type  = 'x'
    c_max     = 0
    c_min     = 0
    xyz_0     = 0
    a_xyz     = 1
    b_xyz     = 1
    sigma_xyz = 1
    low_xyz   = -1
    upp_xyz   = 1
    xyz_dlow  = 0
    xyz_dupp  = 0
    xyz_rlow  = 0
    xyz_rupp  = 0
    x_vec     = 0
    y_vec     = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,GRADING,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &GRADING parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(profile)
    call to_lower(mat_type)

    ! --> save parameters in struct
    gradings%profile   = profile
    gradings%mat_type  = mat_type
    gradings%c_max     = c_max
    gradings%c_min     = c_min
    gradings%xyz_0     = xyz_0
    gradings%a_xyz     = a_xyz
    gradings%b_xyz     = b_xyz
    gradings%low_xyz   = low_xyz
    gradings%sigma_xyz = sigma_xyz
    gradings%upp_xyz   = upp_xyz
    gradings%xyz_dlow  = xyz_dlow
    gradings%xyz_dupp  = xyz_dupp
    gradings%xyz_rlow  = xyz_rlow
    gradings%xyz_rupp  = xyz_rupp
    gradings%x_vec     = x_vec
    gradings%y_vec     = y_vec
endsubroutine

subroutine read_strain(strains,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &GRADING parameters.
    ! 
    ! input
    ! -----
    ! gradings : u_grading
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_strain),intent(out)         :: strains!< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer              :: io

    real(8)              :: a1,a2,a3
    NAMELIST /STRAIN/a1,a2,a3

    ! --> default parameter
    a1     = 0
    a2     = 0
    a3     = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,STRAIN,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &STRAIN parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    strains%a1   = a1
    strains%a2   = a2
    strains%a3   = a3
endsubroutine

subroutine read_trap(traps,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &TRAP parameters.
    ! 
    ! input
    ! -----
    ! traps : u_trap
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_trap),intent(out)          :: traps  !< struct containing user inputs
    integer,     intent(in ),optional :: inp_id !< file id for user input file

    integer              :: io

    character(len=64)    :: region,profile,kind,dimension,shape,band
    real(8)              :: fermi ! integer
    real(8)              :: d_con,e_0,sigma,n_dos
    real(8),dimension(3) :: low_xyz,upp_xyz,xyz_0,a_xyz,b_xyz
    NAMELIST /TRAP/region,band,profile,kind,fermi,d_con,e_0,sigma,n_dos,dimension,shape,low_xyz,upp_xyz,xyz_0,a_xyz,b_xyz

    !changed dimension to dim 
    !changed type to kind 

    ! --> default parameter
    region    = 'oxide'
    profile   = 'const'
    kind      = 'fixed_charge'
    band      = ''
    fermi     = 1
    d_con     = 0
    e_0       = 0
    sigma     = 0.1
    n_dos     = 1e21
    dimension = '3D'
    shape     = 'rect'
    low_xyz   = -1
    upp_xyz   = 1
    xyz_0     = 0
    a_xyz     = 1
    b_xyz     = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,TRAP,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &TRAP parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(region)
    call to_lower(profile)
    call to_lower(kind)
    call to_lower(band)
    call to_lower(dimension)
    call to_lower(shape)

    ! --> save parameters in struct
    traps%region    = region
    traps%profile   = profile
    traps%type      = kind
    traps%band      = band
    traps%fermi     = int(fermi)
    traps%d_con     = d_con
    traps%e_0       = e_0
    traps%sigma     = sigma
    traps%n_dos     = n_dos
    traps%dimension = dimension
    traps%shape     = shape
    traps%low_xyz   = low_xyz
    traps%upp_xyz   = upp_xyz
    traps%xyz_0     = xyz_0
    traps%a_xyz     = a_xyz
    traps%b_xyz     = b_xyz

endsubroutine

subroutine read_contact(contacts,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONTACT parameters.
    ! 
    ! input
    ! -----
    ! contacts : u_contact
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_contact),intent(out)          :: contacts !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for input file

    integer           :: io

    character(len=64) :: mod_name,con_type,sb_type,ohmic_bc,schottky_bc
    real(8)           :: phi_sb,r_con,kappa,v_n,v_n2,v_p
    NAMELIST /CONTACT/mod_name,con_type,sb_type,phi_sb,ohmic_bc,schottky_bc,r_con,kappa,v_n,v_n2,v_p

    ! --> default parameter
    mod_name        = 'default'
    con_type        = 'schottky'
    sb_type         = 'elec'
    phi_sb          = 0.1
    ohmic_bc        = 'dirichlet'
    schottky_bc     = 'velocity'
    r_con           = 0
    kappa           = 80
    v_n             = 4.7e6
    v_n2            = 4.7e6
    v_p             = 3.6e4



    if (present(inp_id)) then
    ! --> read user parameter
      backspace(inp_id)
      read(inp_id,CONTACT,iostat=io)
      if (io.ne.0) then
        write(*,*) '***error*** reading &CONTACT parameters'
        stop
      endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(con_type)
    call to_lower(sb_type)
    call to_lower(ohmic_bc)
    call to_lower(schottky_bc)

    ! --> save parameters in struct
    contacts%mod_name        = mod_name
    contacts%con_type        = con_type
    contacts%sb_type         = sb_type
    contacts%phi_sb          = phi_sb
    contacts%ohmic_bc        = ohmic_bc
    contacts%schottky_bc     = schottky_bc
    contacts%r_con           = r_con
    contacts%kappa           = kappa
    contacts%v_n             = v_n
    contacts%v_n2            = v_n2
    contacts%v_p             = v_p

endsubroutine

subroutine read_supply(supplies,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! write the &SUPPLY parameters.
    ! 
    ! input
    ! -----
    ! supplies : u_supply
    !   Struct that shall contain the parameters of the block.
    ! out_id : integer
    !   File id of the output file to write to.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_supply),intent(out)          :: supplies !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for input file

    integer           :: io

    character(len=64) :: mod_name,supply_type
    NAMELIST /SUPPLY/mod_name,supply_type

    ! --> default parameter
    supply_type    = 'p'
    mod_name       = ''

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,SUPPLY,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &SUPPLY parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(supply_type)

    ! --> save parameters in struct
    supplies%mod_name = mod_name
    supplies%supply_type    = supply_type

endsubroutine

subroutine read_semi(semis,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &SEMI parameters.
    ! 
    ! input
    ! -----
    ! semis : u_semi
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_semi),intent(out)          :: semis  !< struct containing user inputs
    integer,     intent(in ),optional :: inp_id !< file id for input file

    integer             :: io

    ! neufangang
    character(len=64)   :: mod_name,materialA,materialB,bgn_model,strain
    real(8)             :: elec,elec2,hole,fermi,dim,bgn_apparent
    real(8)             :: eps,eps_c
    real(8),dimension(2):: bgn_a, bgn_b, bgn_c
    real(8)             :: bgap_gamhd,v_hd0,c_ref,c_hd, bgn_alpha 
    real(8)             :: eoff,eoff_c
    real(8)             :: temp0
    NAMELIST /SEMI/mod_name,strain,materialA,materialB,bgn_model,elec,elec2,hole,fermi,bgn_apparent,dim,eps,eps_c,bgap_gamhd,v_hd0,c_ref,c_hd,bgn_a,bgn_b,bgn_c,bgn_alpha,eoff,eoff_c,temp0

    ! --> default parameter
    mod_name   = ''
    materialA  = ''
    materialB  = ''
    bgn_model  = ''
    strain     = 'none'
    elec       = 0
    elec2      = 0
    hole       = 0
    fermi      = 0
    bgn_apparent = 1
    dim        = 0
    eps        = 0
    eps_c      = 0
    bgap_gamhd = 0
    v_hd0      = 0
    c_ref      = 0
    c_hd       = 0
    bgn_alpha  = 0
    bgn_a      = 0
    bgn_b      = 0
    bgn_c      = 0
    eoff       = 0
    eoff_c     = 0
    temp0      = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,SEMI,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &SEMI parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(materialA)
    call to_lower(materialB)
    call to_lower(bgn_model)
    call to_lower(strain)

    ! --> save parameters in struct
    semis%mod_name  = mod_name
    semis%materialA = materialA
    semis%materialB = materialB
    semis%bgn_model = bgn_model
    semis%strain    = strain

    semis%elec       = int(elec)
    semis%elec2      = int(elec2)
    semis%hole       = int(hole)
    semis%fermi      = int(fermi)
    semis%bgn_apparent = int(bgn_apparent)
    semis%dim        = int(dim)
    semis%eps        = eps
    semis%eps_c      = eps_c
    semis%bgap_gamhd = bgap_gamhd
    semis%v_hd0      = v_hd0
    semis%c_ref      = c_ref
    semis%c_hd       = c_hd
    semis%bgn_alpha  = bgn_alpha
    semis%bgn_a      = bgn_a
    semis%bgn_b      = bgn_b
    semis%bgn_c      = bgn_c
    semis%eoff       = eoff
    semis%eoff_c     = eoff_c
    semis%temp0      = temp0
    semis%eoff       = eoff

endsubroutine

subroutine read_cnt(cnts,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CNT parameters.
    ! 
    ! input
    ! -----
    ! cnts : u_cnt
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cnt),intent(out)          :: cnts   !< struct containing user inputs
    integer,    intent(in ),optional :: inp_id !< file id for input file

    integer                :: io

    character(len=64)      :: mod_name,fc_model
    real(8)                :: a_cc,t_cc,d_cnt,strain,n_cnt,p_r
    real(8),dimension(4,3) :: fc_val
    real(8)                :: n_1,n_2 ! integer
    NAMELIST /CNT/mod_name,n_1,n_2,d_cnt,a_cc,t_cc,strain,p_r,n_cnt,fc_model,fc_val

    ! --> default parameter
    mod_name = 'default'
    n_1      = 0
    n_2      = 0
    d_cnt    = 1.4e-9
    a_cc     = 0.142e-9
    t_cc     = 3.033
    strain   = 0
    p_r      = 0.2
    n_cnt    = 1
    fc_model = 'hasan'
    fc_val   = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CNT,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CNT parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(fc_model)

    ! --> save parameters in struct
    cnts%mod_name = mod_name
    cnts%n_1      = int(n_1)
    cnts%n_2      = int(n_2)
    cnts%d_cnt    = d_cnt
    cnts%a_cc     = a_cc
    cnts%t_cc     = t_cc
    cnts%strain   = strain
    cnts%p_r      = p_r
    cnts%n_cnt    = n_cnt
    cnts%fc_model = fc_model
    cnts%fc_val   = fc_val

endsubroutine

subroutine read_banddef(banddef,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &BAND_DEF parameters.
    ! 
    ! input
    ! -----
    ! banddef : u_banddef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_banddef),intent(out)          :: banddef !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id  !< file id for user input file

    integer             :: io

    character(len=64)   :: mod_name,valley,band,type
    real(8)             :: e0,e0_c,deg,e0_alpha,e0_beta,m_eff,m_eff_c,al

    NAMELIST /BAND_DEF/mod_name,valley,band,type,e0,e0_c,deg,e0_alpha,e0_beta,m_eff,m_eff_c,al

    ! --> default parameter
    mod_name = ''
    valley   = ''
    band     = ''
    type     = ''

    e0       = 0
    e0_c     = 0
    deg      = 0
    e0_alpha = 0
    e0_beta  = 0

    m_eff   = 0
    m_eff_c = 0
    al      = 0

    if (present(inp_id)) then
    ! --> read user parameter
      backspace(inp_id)
      read(inp_id,BAND_DEF,iostat=io)
      if (io.ne.0) then
        write(*,*) '***error*** reading &BAND_DEF parameters'
        stop
      endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(type)
    call to_lower(band)
    call to_lower(valley)

    banddef%mod_name = mod_name
    banddef%valley   = valley
    banddef%band     = band
    banddef%type     = type
    banddef%e0       = e0
    banddef%e0_c     = e0_c
    banddef%deg      = deg
    banddef%e0_alpha = e0_alpha
    banddef%e0_beta  = e0_beta
    banddef%m_eff    = m_eff
    banddef%m_eff_c  = m_eff_c
    banddef%al       = al

endsubroutine

subroutine read_scatdef(scatdefs,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &SCAT_DEF parameters.
    ! 
    ! input
    ! -----
    ! scatdefs : u_scatdef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_scatdef),intent(out)          :: scatdefs !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer           :: io

    character(len=64) :: mod_name,scat_name,type,abs_ems,fs_bs,initial_band,final_band
    real(8)           :: mfp,e_ph,d_pot,v_s,eps_0,eps_inf,p_ii,beta_ii
    NAMELIST /SCAT_DEF/mod_name,scat_name,type,mfp,e_ph,d_pot,v_s,eps_0,eps_inf,p_ii,beta_ii,abs_ems,fs_bs,&
                      &initial_band,final_band

    ! --> default parameter
    mod_name     = 'default'
    scat_name    = ''
    type         = ''
    mfp          = 1e-7
    e_ph         = 0.16
    d_pot        = 1e11
    v_s          = 18e3
    eps_0        = 3.9
    eps_inf      = 2.5
    p_ii         = 1000
    beta_ii      = 2
    abs_ems      = 'both'
    fs_bs        = 'both'
    initial_band = 'all'
    final_band   = 'all'

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,SCAT_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &SCAT_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(type)
    call to_lower(abs_ems)
    call to_lower(fs_bs)
    call to_lower(initial_band)
    call to_lower(final_band)

    ! --> save parameters in struct
    scatdefs%mod_name     = mod_name
    scatdefs%scat_name    = scat_name
    scatdefs%type         = type
    scatdefs%mfp          = mfp
    scatdefs%e_ph         = e_ph
    scatdefs%d_pot        = d_pot
    scatdefs%v_s          = v_s
    scatdefs%eps_0        = eps_0
    scatdefs%eps_inf      = eps_inf
    scatdefs%p_ii         = p_ii
    scatdefs%beta_ii      = beta_ii
    scatdefs%abs_ems      = abs_ems
    scatdefs%fs_bs        = fs_bs
    scatdefs%initial_band = initial_band
    scatdefs%final_band   = final_band

endsubroutine

subroutine read_taudef(taudef,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &MOB_DEF parameters.
    ! 
    ! input
    ! -----
    ! taudef : u_taudef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_taudef),intent(out)          :: taudef !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id !< file id for user input file

    integer              :: io

    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: valley
    character(len=64)       :: tau_type

    real(8) :: t0
    real(8) :: t1
    real(8) :: c0
    real(8) :: c1
    real(8) :: c2
    real(8) :: c3

    real(8) :: t0_bow
    real(8) :: c0_bow

    NAMELIST /TAU_E/mod_name,valley,tau_type,t0,t1,c0,c1,c2,c3,t0_bow,c0_bow

    ! --> default parameter
    mod_name     = ''    !< model name
    valley       = ''
    tau_type     = 'default'

    t0  = 0
    t1  = 0
    c0  = 0
    c1  = 0
    c2  = 0
    c3  = 0
    t0_bow  = 0
    c0_bow  = 0
      
    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,TAU_E,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &TAU_E parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(valley)
    call to_lower(tau_type)

    ! --> default parameter
    taudef%mod_name     = mod_name 
    taudef%valley       = valley 
    taudef%tau_type     = tau_type 

    taudef%t0 = t0 
    taudef%t1 = t1 
    taudef%c0 = c0 
    taudef%c1 = c1 
    taudef%c2 = c2 
    taudef%c3 = c3 
    taudef%t0_bow = t0_bow 
    taudef%c0_bow = c0_bow 

endsubroutine

subroutine read_mobdef(mobdef,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &MOB_DEF parameters.
    ! 
    ! input
    ! -----
    ! mobdef : u_mobdef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_mobdef),intent(out)          :: mobdef !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id !< file id for user input file

    integer              :: io

    !strings
    character(len=64)       :: mod_name        !< model name
    character(len=64)       :: valley
    character(len=64)       :: l_scat_type
    character(len=64)       :: li_scat_type
    character(len=64)       :: hc_scat_type
    character(len=64)       :: v_sat_type
    character(len=64)       :: force
    character(len=64)       :: bowing


    !lattice scattering parameters
    !default
    real(8),dimension(3)    :: mu_l
    !michaillat
    real(8),dimension(3)    :: mu_cs
    real(8),dimension(3)    :: z_0
    real(8),dimension(3)    :: beta_l
    !temp
    real(8)                 :: gamma_l

    !impurity scattering
    !caughey thomas
    real(8)                 :: mu_min
    real(8)                 :: c_ref
    real(8)                 :: alpha
    real(8)                 :: gamma_1
    real(8)                 :: gamma_2
    real(8)                 :: gamma_3
    real(8)                 :: gamma_4
    real(8)                 :: gamma_1_a
    real(8)                 :: gamma_2_a
    real(8)                 :: gamma_1_d
    real(8)                 :: gamma_2_d
    real(8)                 :: gamma_3_a
    real(8)                 :: gamma_4_a
    real(8)                 :: gamma_3_d
    real(8)                 :: gamma_4_d

    !reggiani
    real(8)                 :: c_ref_a
    real(8)                 :: c_ref_d
    real(8)                 :: mu_min_a
    real(8)                 :: mu_min_d
    real(8)                 :: alpha_a
    real(8)                 :: alpha_d
    real(8)                 :: gamma
    real(8)                 :: c
    !device
    real(8)                 :: alpha_2
    real(8)                 :: c_ref_2
    real(8)                 :: mu_hd
    real(8)                 :: r

    !hot carrier scattering
    !default
    !sat
    real(8)                 :: beta
    !ndm
    real(8)                 :: f0
    real(8)                 :: f1
    real(8)                 :: beta1
    real(8)                 :: beta2
    real(8)                 :: beta3
    real(8)                 :: mu_hc

    !saturation velocity
    !default
    real(8)                 :: v_sat
    real(8)                 :: a
    real(8)                 :: c_ref_vsat
    real(8)                 :: al_vsat
    !device
    real(8)                 :: zeta_v

    real(8)              :: mu_fac

    !driving force
    real(8)                 :: fac_driving_force
    !bowing
    !default
    real(8)              :: mu_bow
    real(8)              :: v_sat_bow
    !michaillat
    real(8)              :: mu_bow1
    real(8)              :: mu_bow2
    real(8)              :: mu_bowy
    real(8)              :: mu_bowxy
    real(8),dimension(4) :: chi
    real(8)              :: z_1
    !device
    real(8)              :: v_sat_a1
    real(8)              :: v_sat_a2
    real(8)              :: mu_min_a1
    real(8)              :: mu_min_a2
    real(8)              :: mu_l_a1
    real(8)              :: mu_l_a2

    ! cryo
    real(8) :: mu_max
    real(8) :: meff
    real(8) :: f_e
    real(8) :: f_h
    real(8) :: f_CW
    real(8) :: f_bh
    real(8) :: ag
    real(8) :: bg
    real(8) :: cg
    real(8) :: gammag
    real(8) :: betag
    real(8) :: alphag
    real(8) :: alphag2

    NAMELIST /MOB_DEF/mod_name,valley,mu_hc,mu_fac,force,mu_cs,z_0,beta_l,l_scat_type,li_scat_type,hc_scat_type,v_sat_type,bowing,mu_l,gamma_l,mu_min,c_ref,alpha,gamma_1,gamma_2,gamma_1_a,gamma_2_a,gamma_1_d,gamma_2_d,gamma_3,gamma_4,gamma_3_a,gamma_4_a, gamma_3_d,gamma_4_d, alpha_2,c_ref_2,mu_hd,r,beta, beta1, beta2, beta3, f0, f1,v_sat,c_ref_vsat,al_vsat,a,zeta_v, fac_driving_force,mu_bow,v_sat_bow,v_sat_a1,v_sat_a2,mu_min_a1,mu_min_a2,mu_l_a1,mu_l_a2,mu_min_a,mu_min_d,alpha_a,alpha_d,c_ref_a,c_ref_d,mu_bow1,mu_bow2,mu_bowy,mu_bowxy,chi,z_1, gamma, c,mu_max,meff,f_e,f_h,f_CW,f_bh,ag,bg,cg,gammag,betag,alphag,alphag2

    ! --> default parameter
    mod_name     = ''    !< model name
    valley       = ''
    l_scat_type  = 'default'
    li_scat_type = 'default'
    hc_scat_type = 'default'
    v_sat_type   = 'default'
    bowing       = 'default'
    force        = 'default'

      !lattice scattering parameters
      !default
    mu_l = 0
      !michaillat
    mu_cs  = 0
    z_0    = 0
    beta_l = 0
      !temp
    gamma_l = 0

      !impurity scattering
      !caughey thomas
    mu_min  = 0
    c_ref   = 0
    alpha   = 0
    gamma_1 = 0
    gamma_2 = 0
    gamma_3 = 0
    gamma_4 = 0
    gamma_1_a = 0
    gamma_2_a = 0
    gamma_1_d = 0
    gamma_2_d = 0
    gamma_3_a = 0
    gamma_4_a = 0
    gamma_3_d = 0
    gamma_4_d = 0
      !device
    alpha_2 = 0
    c_ref_2 = 0
    mu_hd   = 0
    r       = 0
        !reggiani
    mu_min_a = 0
    mu_min_d = 0
    alpha_a  = 0
    alpha_d  = 0
    gamma    = 0
    c        = 0

    mu_fac   = 1

      !hot carrier scattering
      !default
      !sat
    beta = 0
      !ndm
    f0 = 1
    f1 = 1
    beta1 = 0
    beta2 = 0
    beta3 = 0    
      !ndm22
    mu_hc = 0

      !saturation velocity
      !default
    v_sat = 0
    a     = 0
    c_ref_vsat = 0
    al_vsat = 0
      !device
    zeta_v = 0
      !driving force
    fac_driving_force         = 1
      !bowing
      !default
    mu_bow    = 0
    v_sat_bow = 0
      !michaillat
    mu_bow1  = 0
    mu_bow2  = 0
    mu_bowy  = 0
    mu_bowxy = 0
    chi      = 0
    z_1      = 0
      !device
    v_sat_a1  = 0
    v_sat_a2  = 0
    mu_min_a1 = 0
    mu_min_a2 = 0
    mu_l_a1   = 0
    mu_l_a2   = 0

    !cryo
    mu_max = 0
    meff = 0
    f_e = 0
    f_h = 0
    f_CW = 0
    f_bh = 0
    ag = 0
    bg = 0
    cg = 0
    gammag = 0
    betag = 0
    alphag = 0
    alphag2 = 0

      
    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,MOB_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &MOB_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(valley)
    call to_lower(l_scat_type)
    call to_lower(li_scat_type)
    call to_lower(hc_scat_type)
    call to_lower(v_sat_type)
    call to_lower(force)
    call to_lower(bowing)

    ! --> default parameter
    mobdef%mod_name     = mod_name 
    mobdef%valley       = valley 
    mobdef%l_scat_type  = l_scat_type 
    mobdef%li_scat_type = li_scat_type 
    mobdef%hc_scat_type = hc_scat_type 
    mobdef%v_sat_type   = v_sat_type 
    mobdef%bowing       = bowing 
    mobdef%force        = force 

      !lattice scattering parameters
      !default
    mobdef%mu_l = mu_l 
      !michaillat
    mobdef%mu_cs   = mu_cs
    mobdef%z_0     = z_0
    mobdef%beta_l  = beta_l
      !temp
    mobdef%gamma_l = gamma_l 

    mobdef%mu_fac = mu_fac 

      !impurity scattering
      !caughey thomas
    mobdef%mu_min  = mu_min 
    mobdef%c_ref   = c_ref 
    mobdef%alpha   = alpha 
    mobdef%gamma_1 = gamma_1 
    mobdef%gamma_2= gamma_2
    mobdef%gamma_1_a = gamma_1_a 
    mobdef%gamma_2_a = gamma_2_a
    mobdef%gamma_1_d = gamma_1_d  
    mobdef%gamma_2_d = gamma_2_d 
    mobdef%gamma_3 = gamma_3 
    mobdef%gamma_4 = gamma_4 
    mobdef%gamma_3_a = gamma_3_a 
    mobdef%gamma_4_a = gamma_4_a 
    mobdef%gamma_3_d = gamma_3_d
    mobdef%gamma_4_d = gamma_4_d 
      !regianni
    mobdef%c_ref_a  = c_ref_a
    mobdef%c_ref_d  = c_ref_d
    mobdef%mu_min_a = mu_min_a
    mobdef%mu_min_d = mu_min_d
    mobdef%alpha_a  = alpha_a
    mobdef%alpha_d  = alpha_d
    mobdef%gamma    = gamma
    mobdef%c        = c
      !device
    mobdef%alpha_2 = alpha_2 
    mobdef%c_ref_2 = c_ref_2 
    mobdef%mu_hd   = mu_hd 
    mobdef%r       = r 

      !hot carrier scattering
      !default
      !sat
    mobdef%beta = beta 
      !ndm2
    mobdef%f0 = f0 
    mobdef%f1 = f1
    mobdef%mu_hc = mu_hc 
    mobdef%beta1 = beta1 
    mobdef%beta2 = beta2 
    mobdef%beta3 = beta3 
      !saturation velocity
      !default
    mobdef%v_sat     = v_sat
    mobdef%a         = a 
    mobdef%c_ref_vsat = c_ref_vsat
    mobdef%al_vsat = al_vsat
      !device
    mobdef%zeta_v    = zeta_v 
      !driving force
    mobdef%fac_driving_force = fac_driving_force   
      !bowing
      !default
    mobdef%mu_bow    = mu_bow
    mobdef%v_sat_bow = v_sat_bow 
      !michaillat
    mobdef%mu_bow1  = mu_bow1
    mobdef%mu_bow2  = mu_bow2
    mobdef%mu_bowy  = mu_bowy
    mobdef%mu_bowxy = mu_bowxy
    mobdef%chi      = chi
    mobdef%z_1      = z_1
      !device
    mobdef%v_sat_a1  = v_sat_a1
    mobdef%v_sat_a2  = v_sat_a2 
    mobdef%mu_min_a1 = mu_min_a1 
    mobdef%mu_min_a2 = mu_min_a2 
    mobdef%mu_l_a1   = mu_l_a1 
    mobdef%mu_l_a2   = mu_l_a2 
    
    !cryo
    mobdef%mu_max = mu_max
    mobdef%meff = meff
    mobdef%f_e = f_e
    mobdef%f_h = f_h
    mobdef%f_CW = f_CW
    mobdef%f_bh = f_bh
    mobdef%ag = ag
    mobdef%bg = bg
    mobdef%cg = cg
    mobdef%gammag = gammag
    mobdef%betag = betag
    mobdef%alphag = alphag
    mobdef%alphag2 = alphag2


endsubroutine

subroutine read_recombdef(recombdef,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &RECOMB_DEF parameters.
    ! 
    ! input
    ! -----
    ! recombdef : u_recombdef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_recombdef),intent(out)          :: recombdef !< struct containing user inputs
    integer,          intent(in ),optional :: inp_id    !< file id for user input file

    integer           :: io

    character(len=64) :: mod_name='',type='',lifetime='',force=''

    !langevin
    real(8)           :: beta = 0

    !srh
    real(8)           :: e_trap       =0
    real(8)           :: n_t          =0
    real(8)           :: beta_n       =0
    real(8)           :: beta_p       =0
    real(8)           :: c_ref_n      =0
    real(8)           :: c_ref_p     =0
    real(8)           :: tau_n_min   =0
    real(8)           :: tau_p_min   =0
    real(8)           :: tau_n_max   =0
    real(8)           :: tau_p_max   =0
    real(8)           :: tau_n_min_a1=0
    real(8)           :: tau_n_min_a2=0
    real(8)           :: tau_p_min_a1=0
    real(8)           :: tau_p_min_a2=0
    real(8)           :: tau_n_max_a1=0
    real(8)           :: tau_n_max_a2=0
    real(8)           :: tau_p_max_a1=0
    real(8)           :: tau_p_max_a2=0
    real(8)           :: sigma_n     =0
    real(8)           :: sigma_p     =0
    real(8)           :: zeta_n      =0
    real(8)           :: zeta_p      =0

    !auger
    real(8)           :: c_n    =0
    real(8)           :: c_p    =0
    real(8)           :: c_n_a1 =0
    real(8)           :: c_n_a2 =0
    real(8)           :: c_p_a1 =0
    real(8)           :: c_p_a2 =0
    !zetas from srh def

    !avalanche
    real(8)           :: alpha_n  =0
    real(8)           :: alpha_p  =0
    real(8)           :: e_crit_n =0
    real(8)           :: e_crit_p =0

    !intervalley transfer
    real(8)           :: v_21    =0
    real(8)           :: af      =1
    real(8)           :: dv_21   =0
    real(8)           :: xl      =-1e9
    real(8)           :: xr      = 1e9
    real(8)           :: v21off  =0
    real(8)           :: dop_crit=0
    real(8)           :: f0=0
    real(8)           :: f1=0
    real(8)           :: a=0
    real(8)           :: zeta_dop=0
    real(8)           :: b=0

    NAMELIST /RECOMB_DEF/mod_name,type,force,lifetime,beta,e_trap,n_t,beta_n,beta_p,c_ref_n,c_ref_p,tau_n_min,tau_p_min, tau_n_max, tau_p_max, tau_n_min_a1, tau_n_min_a2, tau_p_min_a1, tau_p_min_a2, tau_n_max_a1, tau_n_max_a2, tau_p_max_a1, tau_p_max_a2, sigma_n, sigma_p, zeta_n, zeta_p, c_n, c_p, c_n_a1, c_n_a2, c_p_a1, c_p_a2, alpha_n, alpha_p, e_crit_n, e_crit_p, v_21,af, dv_21, v21off, xl,xr, dop_crit, f0, f1, a,b, zeta_dop

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,RECOMB_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &RECOMB_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(type)
    call to_lower(lifetime)
    call to_lower(force)

    ! --> save parameters in struct
    recombdef%force        = force 
    recombdef%mod_name     = mod_name
    recombdef%type         = type
    recombdef%lifetime     = lifetime

    !langevin
    recombdef%beta = beta

    !srh
    recombdef%e_trap       = e_trap
    recombdef%n_t          = n_t
    recombdef%beta_n       = beta_n
    recombdef%beta_p       = beta_p
    recombdef%c_ref_n      = c_ref_n
    recombdef%c_ref_p      = c_ref_p
    recombdef%tau_n_min    = tau_n_min
    recombdef%tau_p_min    = tau_p_min
    recombdef%tau_n_max    = tau_n_max
    recombdef%tau_p_max    = tau_p_max
    recombdef%tau_n_min_a1 = tau_n_min_a1
    recombdef%tau_n_min_a2 = tau_n_min_a2
    recombdef%tau_p_min_a1 = tau_p_min_a1
    recombdef%tau_p_min_a2 = tau_p_min_a2
    recombdef%tau_n_max_a1 = tau_n_max_a1
    recombdef%tau_n_max_a2 = tau_n_max_a2
    recombdef%tau_p_max_a1 = tau_p_max_a1
    recombdef%tau_p_max_a2 = tau_p_max_a2
    recombdef%sigma_n      = sigma_n
    recombdef%sigma_p      = sigma_p
    recombdef%zeta_n       = zeta_n
    recombdef%zeta_p       = zeta_p

    !auger
    recombdef%c_n    = c_n
    recombdef%c_p    = c_p
    recombdef%c_n_a1 = c_n_a1
    recombdef%c_n_a2 = c_n_a2
    recombdef%c_p_a1 = c_p_a1
    recombdef%c_p_a2 = c_p_a2

    !avalanche
    recombdef%alpha_n  = alpha_n
    recombdef%alpha_p  = alpha_p
    recombdef%e_crit_n = e_crit_n
    recombdef%e_crit_p = e_crit_p

    !intervalley transfer
    recombdef%v_21     = v_21
    recombdef%af       = af
    recombdef%dv_21    = dv_21
    recombdef%v21off   = v21off
    recombdef%xl       = xl
    recombdef%xr       = xr
    recombdef%f0       = f0
    recombdef%f1       = f1
    recombdef%a        = a
    recombdef%dop_crit = dop_crit
    recombdef%zeta_dop = zeta_dop

endsubroutine

subroutine read_defectdef(defectdef,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &DEFECT_DEF parameters.
    ! 
    ! input
    ! -----
    ! defectdef : u_defectdef
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_defectdef),intent(out)          :: defectdef !< struct containing user inputs
    integer,          intent(in ),optional :: inp_id    !< file id for user input file

    integer             :: io

    character(len=64)   :: mod_name,type
    character(len=1024) :: file
    real(8)             :: x_0,std,amp
    real(8)             :: f_col ! integer 
    NAMELIST /DEFECT_DEF/mod_name,type,x_0,std,amp,file,f_col

    ! --> default parameter
    mod_name = 'default'
    type     = 'eg'
    x_0      = 0
    std      = 1e-9
    amp      = 0
    file     = ''
    f_col    = 2

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,DEFECT_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &DEFECT_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)
    call to_lower(type)

    ! --> save parameters in struct
    defectdef%mod_name = mod_name
    defectdef%type     = type
    defectdef%x_0      = x_0
    defectdef%std      = std
    defectdef%amp      = amp
    defectdef%file     = file
    defectdef%f_col    = int(f_col)

endsubroutine

subroutine read_oxide(oxides,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &OXIDE parameters.
    ! 
    ! input
    ! -----
    ! oxides : u_oxide
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_oxide),intent(out)          :: oxides !< struct containing user inputs
    integer,      intent(in ),optional :: inp_id !< file id for input file

    integer           :: io

    character(len=64) :: mod_name
    real(8)           :: eps
    real(8)           :: kappa
    NAMELIST /OXIDE/mod_name,eps,kappa

    ! --> default parameter
    mod_name = 'default'
    eps      = 1
    kappa    = 0.026

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,OXIDE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &OXIDE parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(mod_name)

    ! --> save parameters in struct
    oxides%mod_name =  mod_name
    oxides%eps      =  eps
    oxides%kappa    =  kappa

endsubroutine

subroutine read_thermionic_emission(thermionic_emissions,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &THERMIONIC_EMISSION parameters.
    ! 
    ! input
    ! -----
    ! thermionic_emissions : u_thermionic_emission
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_thermionic_emission),intent(out)          :: thermionic_emissions !< struct containing user inputs
    integer,      intent(in ),optional :: inp_id !< file id for input file

    integer           :: io

    real(8)           :: x
    real(8)           :: fac
    NAMELIST /THERMIONIC_EMISSION/x, fac

    ! --> default parameter
    x      = 0
    fac    = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,THERMIONIC_EMISSION,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &THERMIONIC_EMISSION parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    thermionic_emissions%x   =  x
    thermionic_emissions%fac =  fac

endsubroutine

subroutine read_tbc(tbcs,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &TBC parameters.
    ! 
    ! input
    ! -----
    ! tbcs : u_tbc
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_tbc),intent(out)          :: tbcs   !< struct containing user inputs
    integer,    intent(in ),optional :: inp_id !< file id for user input file

    integer              :: io

    real(8)              :: g
    real(8),dimension(3) :: low_xyz,upp_xyz
    NAMELIST /TBC/g,low_xyz,upp_xyz

    ! --> default parameter
    g         = 0
    low_xyz   = 0
    upp_xyz   = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,TBC,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &TBC parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    tbcs%g       = g
    tbcs%low_xyz = low_xyz
    tbcs%upp_xyz = upp_xyz

endsubroutine

subroutine read_heatsource(heatsources,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &HEATSOURCE parameters.
    ! 
    ! input
    ! -----
    ! heatsources : u_heatsource
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_heatsource),intent(out)          :: heatsources !< struct containing user inputs
    integer,           intent(in ),optional :: inp_id      !< file id for user input file

    integer              :: io

    character(len=64)    :: dimension,shape
    real(8)              :: p_diss
    real(8),dimension(3) :: low_xyz,upp_xyz
    NAMELIST /HEAT_SOURCE/dimension,shape,p_diss,low_xyz,upp_xyz

    ! --> default parameter
    dimension = '1d_x'
    shape     = 'rect'
    p_diss    = 0
    low_xyz   = 0
    upp_xyz   = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,HEAT_SOURCE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &HEAT_SOURCE parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(dimension)
    call to_lower(shape)

    ! --> save parameters in struct
    heatsources%dimension = dimension
    heatsources%shape     = shape
    heatsources%p_diss    = p_diss
    heatsources%low_xyz   = low_xyz
    heatsources%upp_xyz   = upp_xyz

endsubroutine

subroutine read_heatsink(heatsinks,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &HEAT_SINK parameters.
    ! 
    ! input
    ! -----
    ! heatsinks : u_heatsink
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_heatsink),intent(out)          :: heatsinks !< struct containing user inputs
    integer,         intent(in ),optional :: inp_id    !< file id for user input file

    integer              :: io

    real(8)              :: temp
    real(8),dimension(3) :: low_xyz,upp_xyz
    NAMELIST /HEAT_SINK/temp,low_xyz,upp_xyz

    ! --> default parameter
    temp    = 300
    low_xyz = 0
    upp_xyz = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,HEAT_SINK,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &HEAT_SINK parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    heatsinks%temp    = temp
    heatsinks%low_xyz = low_xyz
    heatsinks%upp_xyz = upp_xyz

endsubroutine

subroutine read_efm(efms,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &EFM parameters.
    ! 
    ! input
    ! -----
    ! efms : u_nonlocal
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_efm),intent(out)          :: efms   !< struct containing user inputs
    integer,    intent(in ),optional :: inp_id !< file id for user input file

    integer :: io

    real(8) :: n_iter,n_max,n_pml ! integer
    real(8) :: d_tol,x_min,p_tol,damp_init,damp_min
    NAMELIST/EFM/n_iter,d_tol,p_tol,damp_init,damp_min,x_min,n_max,n_pml

    ! --> default parameter
    n_iter    = -1
    d_tol     = 1e-4
    p_tol     = 1e-6
    damp_init = 1e-1
    damp_min  = 1e-3
    x_min     = 1e-5
    n_max     = 10000
    n_pml     = 20

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,EFM,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &EFM parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    efms%n_iter    = int(n_iter)
    efms%d_tol     = d_tol
    efms%p_tol     = p_tol
    efms%damp_init = damp_init
    efms%damp_min  = damp_min
    efms%x_min     = x_min
    efms%n_max     = int(n_max)
    efms%n_pml     = int(n_pml)

endsubroutine

subroutine read_dd(dds,use_solve,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &DD parameters.
    ! 
    ! input
    ! -----
    ! dds : u_dd
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_dd),intent(out)          :: dds    !< struct containing user inputs
    logical,   intent(in )          :: use_solve !< if true: use SOLVE namelist, else DD
    integer,   intent(in ),optional :: inp_id !< file id for user input file

    integer             :: io

    character(len=64)   :: init_qf, damp_method
    real(8)             :: rth,pdiff_tol,damp_init,damp_min,rc,dqf_dens,debug,damp
    real(8)             :: n_iter,simul,tun_derive,osz_cut,diffuse,elec,elec2,tl,tn,hole,op_simul_start,remove_zeros,take_prev,bohm_n ! integer
    !legacy name DD:
    NAMELIST/DD/n_iter,simul,damp_method,damp,rth,pdiff_tol,damp_init,damp_min,init_qf,&
              &tun_derive,osz_cut,elec,elec2,bohm_n,hole,tl,tn,diffuse,op_simul_start,remove_zeros,rc,dqf_dens,take_prev,debug

    !these namelists need to match exactly
    NAMELIST/SOLVE/n_iter,simul,damp_method,damp,rth,pdiff_tol,damp_init,damp_min,init_qf,&
              &tun_derive,osz_cut,elec,elec2,bohm_n,hole,tl,tn,diffuse,op_simul_start,remove_zeros,rc,dqf_dens,take_prev,debug

    ! --> default parameter
    n_iter         = -1
    simul          = 1
    pdiff_tol      = 0
    init_qf        = 'default'
    damp_method    = 'none'
    damp           = 1
    tun_derive     = 0
    osz_cut        = 1
    elec           = 1
    elec2          = 0
    bohm_n         = 0
    rth            = 0
    damp_min       = 1
    tl             = 0
    tn             = 0
    hole           = 1
    op_simul_start = 1
    debug          = 0
    remove_zeros   = -1
    rc             = 0
    dqf_dens       = 0
    take_prev      = 1
    diffuse        = 0

    if (present(inp_id)) then
        ! --> read user parameter
        if (use_solve) then
            backspace(inp_id)
            read(inp_id,SOLVE,iostat=io)
            if (io.ne.0) then
                write(*,*) '***error*** reading &SOLVE parameters'
                stop
            endif
        else
            backspace(inp_id)
            read(inp_id,DD,iostat=io)
            if (io.ne.0) then
                write(*,*) '***error*** reading &DD parameters'
                stop
            endif
 
        endif
    endif


    ! --> convert to lower characters
    call to_lower(init_qf)
    call to_lower(damp_method)

    ! --> save parameters in struct
    dds%n_iter         = int(n_iter)
    dds%simul          = int(simul)
    dds%damp_method    = damp_method
    dds%damp           = damp
    dds%simul          = int(simul)
    dds%pdiff_tol      = pdiff_tol
    dds%rth            = rth
    dds%debug          = int(debug)
    dds%diffuse        = diffuse
    dds%init_qf        = init_qf 
    dds%tun_derive     = int(tun_derive)
    dds%osz_cut        = int(osz_cut)
    dds%elec           = int(elec)
    dds%elec2          = int(elec2)
    dds%bohm_n         = int(bohm_n)
    dds%tl             = int(tl)
    dds%damp_min       = damp_min
    dds%tn             = int(tn)
    dds%hole           = int(hole)
    dds%op_simul_start = int(op_simul_start)
    dds%remove_zeros   = int(remove_zeros)
    dds%rc             = rc
    dds%dqf_dens       = dqf_dens
    dds%take_prev      = int(take_prev)

endsubroutine

subroutine read_norm(norms,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &NORM parameters.
    ! 
    ! input
    ! -----
    ! norms : u_norm
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_norm),intent(out)          :: norms  !< struct containing user inputs
    integer,   intent(in ),optional :: inp_id !< file id for user input file

    integer             :: io

    character(len=64)   :: dens_norm
    real(8)             :: x_norm
    NAMELIST/NORMALIZATION/dens_norm,x_norm

    ! --> default parameter
    dens_norm      = 'none'
    x_norm         = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,NORMALIZATION,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &NORMALIZATION parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(dens_norm)

    ! --> save parameters in struct
    norms%dens_norm      = dens_norm
    norms%x_norm         = int(x_norm)
endsubroutine

subroutine read_ballbte(ballbtes,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &BALL_BTE parameters.
    ! 
    ! input
    ! -----
    ! ballbtes : u_ballbte
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_ballbte),intent(out)          :: ballbtes !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer :: io

    real(8) :: n_iter,n_max ! integer
    real(8) :: d_tol,p_tol,damp_init,damp_min,x_min
    NAMELIST/BALL_BTE/n_iter,d_tol,p_tol,damp_init,damp_min,x_min,n_max


    ! --> default parameter
    n_iter    = -1
    d_tol     = 1e-4
    p_tol     = 1e-6
    damp_init = 1e-1
    damp_min  = 1e-3
    x_min     = 1e-5
    n_max     = 10000

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,BALL_BTE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &BALL_BTE parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    ballbtes%n_iter    = int(n_iter)
    ballbtes%d_tol     = d_tol
    ballbtes%p_tol     = p_tol
    ballbtes%damp_init = damp_init
    ballbtes%damp_min  = damp_min
    ballbtes%x_min     = x_min
    ballbtes%n_max     = int(n_max)

endsubroutine

subroutine read_mcbte(mcbtes,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &MCBTE parameters.
    ! 
    ! input
    ! -----
    ! mcbtes : u_mcbte
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_mcbte),intent(out)          :: mcbtes !< struct containing user inputs
    integer,      intent(in ),optional :: inp_id !< file id for user input file

    integer             :: io

    character(len=1024) :: file     
    character(len=64)   :: init_distr
    real(8)             :: solve,show,init_subband,n_particle,n_max,n_min,n_target,seed,nt_poi,nt_par,nt_pauli,take_prev,scat_table,scat_ne,pauli,scat_virtual,adapt_dt ! integer
    real(8)             :: particle_charge,init_energy,scat_emax,scatrate_max,scatrate_min,pauli_dk,thermionic_dk
    real(8)             :: tunnel_de,tunnel_emax,e_cut
    NAMELIST /MC_BTE/solve,show,particle_charge,n_particle,n_max,n_min,n_target,seed,nt_poi,nt_par,nt_pauli,init_subband,init_energy,init_distr,file,take_prev,&
                    &scat_table,scat_ne,scat_emax,scatrate_max,scatrate_min,pauli,scat_virtual,pauli_dk,thermionic_dk,&
                    &tunnel_de,tunnel_emax,e_cut,adapt_dt

    ! --> default parameter
    solve           = 0
    show            = 0
    particle_charge = 1e-3
    n_particle      = 1
    n_max           = 1e6
    n_min           = 1e3
    n_target        = 0
    seed            = 0
    nt_poi          = 1
    nt_par          = 10
    nt_pauli        = 1
    init_subband    = 0
    init_energy     = -1
    init_distr      = 'intrinsic'
    file            = ''
    take_prev       = 1
    scat_table      = 1
    scat_ne         = 1e4
    scat_emax       = 1.5
    scatrate_max    = 1e15
    scatrate_min    = 1e12
    pauli           = 1
    scat_virtual    = 1
    pauli_dk        = 1e7
    thermionic_dk   = 1e6
    tunnel_de       = 1e-3
    tunnel_emax     = 0.8
    e_cut           = 0.4
    adapt_dt        = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,MC_BTE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &MC_BTE parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(init_distr)

    ! --> save parameters in struct
    mcbtes%solve           = int(solve)
    mcbtes%show            = int(show)
    mcbtes%particle_charge = particle_charge
    mcbtes%n_particle      = int(n_particle)
    mcbtes%n_max           = int(n_max)
    mcbtes%n_min           = int(n_min)
    mcbtes%n_target        = int(n_target)
    mcbtes%seed            = int(seed)
    mcbtes%nt_poi          = int(nt_poi)
    mcbtes%nt_par          = int(nt_par)
    mcbtes%nt_pauli        = int(nt_pauli)
    mcbtes%init_subband    = int(init_subband)
    mcbtes%init_energy     = init_energy
    mcbtes%init_distr      = init_distr
    mcbtes%file            = file
    mcbtes%take_prev       = int(take_prev)
    mcbtes%scat_table      = int(scat_table)
    mcbtes%scat_ne         = int(scat_ne)
    mcbtes%scat_emax       = scat_emax
    mcbtes%scatrate_max    = scatrate_max
    mcbtes%scatrate_min    = scatrate_min
    mcbtes%pauli           = int(pauli)
    mcbtes%scat_virtual    = int(scat_virtual)
    mcbtes%pauli_dk        = pauli_dk
    mcbtes%thermionic_dk   = thermionic_dk
    mcbtes%tunnel_de       = tunnel_de
    mcbtes%tunnel_emax     = tunnel_emax
    mcbtes%e_cut           = e_cut
    mcbtes%adapt_dt        = int(adapt_dt)

endsubroutine

subroutine read_detbte(detbtes,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &DET_BTE parameters.
    ! 
    ! input
    ! -----
    ! detbtes : u_detbte
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_detbte),intent(out)          :: detbtes !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id !< file id for user input file

    integer           :: io

    real(8)           :: solve,nk,check_dt,adapt_dt,take_prev
    real(8)           :: kmax
    character(len=64) :: init_distr
    NAMELIST /DET_BTE/solve,kmax,nk,check_dt,init_distr,adapt_dt,take_prev


    ! --> default parameter
    solve      = 0
    kmax       = 1e9
    nk         = 501
    check_dt   = 1
    init_distr = 'ball'
    adapt_dt   = 1
    take_prev  = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,DET_BTE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &DET_BTE parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(init_distr)

    ! --> save parameters in struct
    detbtes%solve      = int(solve)
    detbtes%kmax       = kmax
    detbtes%nk         = int(nk)
    detbtes%check_dt   = int(check_dt)
    detbtes%init_distr = init_distr
    detbtes%adapt_dt   = int(adapt_dt)
    detbtes%take_prev  = int(take_prev)

endsubroutine

subroutine read_tunnel(tunnels,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &TUNNEL parameters.
    ! 
    ! input
    ! -----
    ! tunnels : u_tunnel
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_tunnel),intent(out)          :: tunnels !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer                           :: io

    real(8)           :: bbt,e_add,n_barrier ! integer
    real(8)           :: max_width,de_min,factor,dqf_tun
    real(8)           :: x_start,x_end,activate,m
    character(len=64) :: model
    NAMELIST /TUNNEL/model,max_width,bbt,e_add,de_min,factor,n_barrier,dqf_tun, x_start, x_end,activate,m

    ! --> default parameter
    model     = 'wkb'
    max_width = 300e-9
    bbt       = 1
    e_add     = 0
    de_min    = 1e-5
    factor    = 1
    m         = 1
    n_barrier = 20
    x_start   = 0
    x_end     = 1 !1m should always be large enough
    dqf_tun   = dble(0)
    activate  = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,TUNNEL,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &TUNNEL parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(model)

    ! --> save parameters in struct
    tunnels%activate  = int(activate)
    tunnels%model     = model
    tunnels%max_width = max_width
    tunnels%x_start   = x_start
    tunnels%x_end     = x_end
    tunnels%bbt       = int(bbt)
    tunnels%e_add     = int(e_add)
    tunnels%de_min    = de_min
    tunnels%factor    = factor
    tunnels%n_barrier = int(n_barrier)
    tunnels%dqf_tun   = dqf_tun
    tunnels%m         = m

endsubroutine

subroutine read_heatflow(heatflows,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &REGION_DEF parameters.
    ! 
    ! input
    ! -----
    ! heatflows : u_heatflow
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_heatflow),intent(out)          :: heatflows !< struct containing user inputs
    integer,         intent(in ),optional :: inp_id    !< file id for user input file

    integer :: io

    real(8) :: d_t,g,damp,g_con,g_inf
    real(8) :: dim,save ! integer
    NAMELIST /HEATFLOW/d_t,dim,g,damp,g_con,g_inf,save

    ! --> default parameter
    dim   = 0
    d_t   = 1e-14
    g     = 0.17
    g_con = 0.1
    g_inf = 1e6
    damp  = 0.1
    save  = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,HEATFLOW,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &HEATFLOW parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    heatflows%d_t   = d_t
    heatflows%dim   = int(dim)
    heatflows%g     = g
    heatflows%g_con = g_con
    heatflows%g_inf = g_inf
    heatflows%damp  = damp
    heatflows%save  = int(save)

endsubroutine

subroutine read_poisson(poissons,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &POISSON parameters.
    ! 
    ! input
    ! -----
    ! poissons : u_poisson
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_poisson),intent(out)          :: poissons !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io

    character(len=64)   :: init_psi
    real(8)             :: atol,rtol,ctol,delta_max
    NAMELIST /POISSON/init_psi,atol,rtol,ctol,delta_max

    ! --> default parameter
    init_psi     = 'buildin'
    delta_max    = 25e-3
    atol         = 1e-6
    rtol         = 1e-3
    ctol         = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,POISSON,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &POISSON parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(init_psi)

    ! --> save parameters in struct
    poissons%init_psi     = init_psi    
    poissons%atol         = atol
    poissons%rtol         = rtol
    poissons%ctol         = ctol
    poissons%delta_max    = delta_max

endsubroutine

subroutine read_cont_tl(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_TL parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_tl
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_tl),intent(out)          :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_TL/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_TL,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_TL parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%ctol   = ctol
    cont%delta_max   = delta_max

endsubroutine

subroutine read_cont_tn(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_TN parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_tn
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_tn),intent(out)        :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_TN/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1000

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_TN,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_TN parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%ctol   = ctol
    cont%delta_max   = delta_max

endsubroutine

subroutine read_cont_elec(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_ELEC parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_elec
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_elec),intent(out)        :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_ELEC/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_ELEC,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_ELEC parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%ctol   = ctol
    cont%delta_max   = delta_max

endsubroutine

subroutine read_cont_bohm_n(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_BOHM_N parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_bohm_n
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_bohm_n),intent(out)        :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_BOHM_N/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_BOHM_N,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_BOHM_N parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%ctol   = ctol
    cont%delta_max   = delta_max

endsubroutine

subroutine read_cont_elec2(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_ELEC2 parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_elec2
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_elec2),intent(out)        :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_ELEC2/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_ELEC2,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_ELEC2 parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%ctol   = ctol
    cont%delta_max   = delta_max

endsubroutine

subroutine read_cont_hole(cont,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &CONT_HOLE parameters.
    ! 
    ! input
    ! -----
    ! cont : u_cont_hole
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_cont_hole),intent(out)        :: cont !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer             :: io
    real(8)             :: atol, rtol, ctol, delta_max
    NAMELIST /CONT_HOLE/atol,rtol,ctol, delta_max

    ! --> default parameter
    atol      = 1e-3
    rtol      = 1e-3
    ctol      = 0
    delta_max = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,CONT_HOLE,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &CONT_HOLE parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    cont%atol   = atol
    cont%rtol   = rtol
    cont%delta_max   = delta_max
    cont%ctol   = ctol

endsubroutine

subroutine read_biasdef(biasdefs,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &BIAS_DEF parameters.
    ! 
    ! input
    ! -----
    ! nonlocal : u_nonlocal
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_biasdef),intent(out)          :: biasdefs !< struct containing user inputs
    integer,        intent(in ),optional :: inp_id   !< file id for user input file

    integer                 :: io

    character(len=64)       :: ssd_type,t_func_type
    real(8),dimension(3,20) :: t_val
    real(8)                 :: list,n_t,dv_adapt,zero_bias_first,displ,n_cross,n_batch ! integer
    real(8)                 :: d_t,dv_max,dv_min,batch_size,rel_error,rel_initial
    NAMELIST /BIAS_DEF/list,dv_adapt,dv_max,dv_min,zero_bias_first,d_t,t_func_type,t_val,n_t,displ&
                    &,ssd_type,n_cross,batch_size,n_batch,rel_error,rel_initial

    ! --> default parameter
    list            = 0
    dv_adapt        = 1
    dv_max          = 1
    dv_min          = 0.01
    zero_bias_first = 0
    d_t             = 1e-15
    t_func_type     = '' 
    n_t             = dble(0)
    displ           = 1
    ssd_type        = 'batch_mean'
    n_cross         = 3
    t_val           = 0
    batch_size      = 1e-13
    n_batch         = 4
    rel_error       = 0.05
    rel_initial     = 1

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,BIAS_DEF,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &BIAS_DEF parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(ssd_type)
    call to_lower(t_func_type)

    ! --> save parameters in struct
    biasdefs%list            = int(list)
    biasdefs%dv_adapt        = int(dv_adapt)
    biasdefs%dv_max          = dv_max
    biasdefs%dv_min          = dv_min
    biasdefs%zero_bias_first = int(zero_bias_first)
    biasdefs%d_t             = d_t
    biasdefs%t_func_type     = t_func_type
    biasdefs%n_t             = int(n_t)
    biasdefs%displ           = int(displ)
    biasdefs%ssd_type        = ssd_type
    biasdefs%n_cross         = int(n_cross)
    biasdefs%batch_size      = batch_size
    biasdefs%n_batch         = int(n_batch)
    biasdefs%rel_error       = rel_error
    biasdefs%t_val           = t_val
    biasdefs%rel_initial     = rel_initial

endsubroutine

subroutine read_biasinfo(biasinfos,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &BIAS_INFO parameters.
    ! 
    ! input
    ! -----
    ! biasinfos : u_biasinfo
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_biasinfo),intent(out)          :: biasinfos !< struct containing user inputs
    integer,         intent(in ),optional :: inp_id    !< file id for user input file

    integer                 :: io

    character(len=64)       :: cont_name,bias_fun
    real(8),dimension(3,20) :: bias_val
    NAMELIST /BIAS_INFO/cont_name,bias_fun,bias_val

    ! --> default parameter
    cont_name = ''
    bias_fun  = 'TAB'
    bias_val  = -9.9D90

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,BIAS_INFO,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &BIAS_INFO parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(cont_name)
    call to_lower(bias_fun)

    ! --> save parameters in struct
    biasinfos%cont_name = cont_name
    biasinfos%bias_fun  = bias_fun
    biasinfos%bias_val  = bias_val

endsubroutine

subroutine read_trinfo(trinfos,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &TR_INFO parameters.
    ! 
    ! input
    ! -----
    ! trinfos : u_trinfo
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_trinfo),intent(out)          :: trinfos !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer               :: io

    character(len=64)     :: cont_name,func_type
    real(8),dimension(20) :: v_max,T,phase
    NAMELIST /TR_INFO/cont_name,func_type,v_max,T,phase

    ! --> default parameter
    cont_name = ''
    func_type = ''
    v_max     = 0
    T         = 0
    phase     = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,TR_INFO,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &TR_INFO parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(cont_name)
    call to_lower(func_type)

    ! --> save parameters in struct
    trinfos%cont_name = cont_name
    trinfos%func_type = func_type
    trinfos%v_max     = v_max
    trinfos%T         = T
    trinfos%phase     = phase

endsubroutine

subroutine read_acinfo(acinfos,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &AC_INFO parameters.
    ! 
    ! input
    ! -----
    ! acinfos : u_acinfo
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_acinfo),intent(out)          :: acinfos !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer                 :: io

    character(len=64)       :: sweep_type,port1,port2
    real(8),dimension(3,20) :: freq_val
    NAMELIST /AC_INFO/sweep_type,freq_val,port1,port2

    ! --> default parameter
    sweep_type    = 'tab'
    port1         = 'g'
    port2         = 'd'
    freq_val      = -9.9D90
    freq_val(1,1) = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,AC_INFO,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &AC_INFO parameters'
            stop
        endif
    endif

    ! --> convert to lower characters
    call to_lower(sweep_type)
    call to_lower(port1)
    call to_lower(port2)

    ! --> save parameters in struct
    acinfos%port1      = port1
    acinfos%port2      = port2
    acinfos%sweep_type = sweep_type
    acinfos%freq_val   = freq_val

endsubroutine


subroutine read_output(outputs,inp_id)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! read the &OUTPUT parameters.
    ! 
    ! input
    ! -----
    ! outputs : u_output
    !   Struct that shall contain the parameters of the block.
    ! inp_id : integer
    !   File id of the input file to read from.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(u_output),intent(out)          :: outputs !< struct containing user inputs
    integer,       intent(in ),optional :: inp_id  !< file id for user input file

    integer                 :: io

    character(len=256)      :: name,path
    real(8)                 :: digits ! integer
    real(8)                 :: inqu_lev,psi_lev,cap_lev  ! integer
    real(8)                 :: tr_elpa_lev,tr_inqu_lev  ! integer
    real(8)                 :: newton_lev  ! integer
    real(8)                 :: elpa  ! integer

    NAMELIST /OUTPUT/name,path,digits,&
                      &inqu_lev,psi_lev,elpa,cap_lev,&
                      &tr_elpa_lev,tr_inqu_lev,&
                      &newton_lev

    ! --> default parameter
    name              = ''
    path              = ''
    digits            = 9
    inqu_lev          = 2
    elpa              = 0
    psi_lev           = 0
    cap_lev           = 0
    tr_elpa_lev       = 2
    tr_inqu_lev       = 0
    newton_lev        = 0

    if (present(inp_id)) then
    ! --> read user parameter
        backspace(inp_id)
        read(inp_id,OUTPUT,iostat=io)
        if (io.ne.0) then
            write(*,*) '***error*** reading &OUTPUT parameters'
            stop
        endif
    endif

    ! --> save parameters in struct
    outputs%name            = name
    outputs%path            = path
    outputs%digits          = int(digits)
    outputs%elpa            = int(elpa)
    outputs%inqu_lev        = int(inqu_lev)
    outputs%psi_lev         = int(psi_lev)
    outputs%cap_lev         = int(cap_lev)
    outputs%tr_elpa_lev     = int(tr_elpa_lev)
    outputs%tr_inqu_lev     = int(tr_inqu_lev)
    outputs%newton_lev      = int(newton_lev)

endsubroutine

endmodule