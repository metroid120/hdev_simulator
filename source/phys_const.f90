!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!physical  constants
module phys_const

implicit none

real(8) :: PI,BK,Q,BK_Q,HBAR,EP0,AM0,G0,QH,QHB,HBQ,HHQ,CONST_DG

parameter( PI   = 3.14159e-00 )  ! PI
parameter( BK   = 1.38066e-23 )  ! bolzmann constant
parameter( Q    = 1.60219e-19 )  ! electron charge
parameter( BK_Q = BK/Q )  ! bolzmann constant
parameter( HBAR = 1.05459e-34 )  ! planksches Wirkungsquantum /2pi
parameter( EP0  = 8.85419e-12 )  ! 
parameter( AM0  = 9.10953e-31 )  ! m0
parameter( G0   = dble(4)*Q*Q/HBAR/dble(2)/PI ) ! 4q^2/h
parameter( QH   = Q/HBAR/dble(2)/PI ) ! q/h
parameter( QHB  = Q/HBAR )            ! q/hbar
parameter( HBQ  = HBAR/Q )            ! hbar/q
parameter( HHQ  = HBAR*HBAR/Q )       ! h*h/q
parameter( CONST_DG = HBAR*HBAR/dble(6)/Q)       ! prefactor for density gradient equation

endmodule