!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! band structure related calculations
module bandstructure

USE Dual_Num_Auto_Diff


! ---> variables and functions from other moduls ----------------------------------------------------------------------
use phys_const

use math_oper  , only : add_point
use math_oper  , only : gcd
use math_oper  , only : total_diff
use math_oper  , only : trapz_vec
use math_oper  , only : init_grid
use math_oper  , only : fdm1h
use math_oper  , only : fd1h
use math_oper  , only : fd3h
use math_oper  , only : fd1h_inv
use math_oper  , only : dfd1h
use math_oper  , only : fdm3h
use math_oper  , only : tanh_dual

use read_user  , only : US
use read_user  , only : u_banddef

use save_global, only : DEB_ID
use save_global, only : FILE
use save_global, only : VERSION
use save_global, only : FORMAT_REAL
use save_global, only : SAVE_OP
use save_global, only : format_int
use save_global, only : format_int_vec
use save_global, only : get_file_id
use save_global, only : write_parameter_char
use save_global, only : write_parameter_real
use save_global, only : write_parameter_int
use save_global, only : write_colnames
use save_global, only : write_elpa_value

use structure  , only : N_SEMI
use structure  , only : SEMI_MOD_NAME

use semiconductor_globals  , only : N_SP, TEMP_LATTICE
use profile  , only : CD
use dd_types, only  : mob_model
use dd_types, only  : rec_model
use dd_types, only  : type_band
use dd_types, only  : NORM
use dd_types, only  : CRYO


implicit none

!interface so that method can be called with arguments of dual and real values based on argument type
interface get_n_equ_dual
    module procedure get_n_equ_dual, get_n_equ
end interface

!interface so that method can be called with arguments of dual and real values based on argument type
interface get_n_equ_band_dual
    module procedure get_n_equ_band_dual, get_n_equ_band
end interface

! fuck Fortran. This cheat allows to have different types in one array of pointers
! https://stackoverflow.com/questions/49616410/intrinsic-assignment-and-polymorphism-in-fortran-2003
type band_pointer
    class(type_band), allocatable :: band
    contains
end type band_pointer


! semiconductor properties (maybe move to dd_types.f90 when circular dependency problems occur)
type semi_prop
    integer :: n_band = 0                                     !number of bands
    integer :: n_cb   = 0                                     !number of conduction bands
    integer :: n_vb   = 0                                     !number of valence bands

    !strings    
    character(len=64)               :: mod_name  = ''         !name of semiconductor, e.g. "Si" or "InP"
    character(len=64)               :: materialA = ''         !if alloy: constituent A
    character(len=64)               :: materialB = ''         !if alloy: constituent B
    character(len=64)               :: bgn_model = ''         !name of band-gap-narrowing model
    character(len=64)               :: strain    = ''         !name of strain model

    !logicals
    logical :: elec           = .false.                       !if .true.: calculate electron density in semiconductor
    logical :: elec2          = .false.                       !if .true.: calculate electron density 2 in semiconductor
    logical :: hole           = .false.                       !if .true.: calculate hole density in semiconductor
    logical :: fermi          = .false.                       !if .true.: use fermi-statistics
    logical :: bgn_apparent   = .true.                        !if .true.: apparent bgn model parameters
    logical :: is_alloy       = .false.                       !if .true.: this is an alloy semiconductor
    logical :: bgn_is_default = .false.                       !if .true.: use default bgn model
    logical :: bgn_is_classic = .false.                       !if .true.: use classic bgn model
    logical :: bgn_is_jain    = .false.                       !if .true.: use jain bgn model
    logical :: strain_is_sige = .false.                       !if .true.: use strain model for effective mass
    logical :: strain_is_DOS = .false.                        !if .true.: use strain model for DOS

    class(band_pointer),allocatable,dimension(:) :: band_pointer  !array of pointers to the bands of the semiconductor
    type(rec_model),allocatable,dimension(:)     :: rec_models    !array of pointers to the recombination models of the semiconductor

    integer                         :: dim=0          !< transport dimension
    real(8)                         :: temp0=0        !< reference temperature of models

    !eps model
    real(8)                         :: eps =0         !< permitivity of semiconductor
    real(8)                         :: eps_c  =0      !< bowing of permittivity

    !energy offset model
    real(8) :: eoff         !< offset energy of valence band edge with respect to a reference material.
    real(8) :: eoff_c       !< offset energy bowing

    !bgn model parameters
    !default
    real(8)                         :: bgap_gamhd  =0 
    !classic
    real(8)                         :: v_hd0=0
    real(8)                         :: c_ref=0
    real(8)                         :: c_hd=0
    real(8)                         :: bgn_alpha=0
    !jain
    real(8),dimension(2)            :: bgn_a=0
    real(8),dimension(2)            :: bgn_b=0
    real(8),dimension(2)            :: bgn_c=0

    !if alloy: id to constituents
    integer :: id_materialA =0  !< id to alloy material A
    integer :: id_materialB =0  !< id to alloy material B

endtype




!allows other modules to call init of bands equally for all types of bands
abstract interface
subroutine init(this, banddef)
    import u_banddef
    import type_band
    class(type_band),intent(inout) :: this
    type(u_banddef)     :: banddef
endsubroutine init
end interface

! child class ot "type_band" for parabolic bands
type, extends(type_band) :: type_para_band
    contains
endtype

! array of bands for contacts (is this used currently?->if no, delete)
type bss
    class(band_pointer),dimension(:),allocatable :: band_pointer         !< (n_band) bandstructure parameters
endtype

! --> global module parameters ----------------------------------------------------------------------------------------
type(semi_prop),dimension(:),pointer :: SEMI      !< (N_SEMI) properties of semiconductors that are specified
type(bss),dimension(2)               :: BS        !< bandstructure parameters for source and drain contact
real(8)                              :: FE_TEMP   !< Fermi temperature
real(8)                              :: FE_DK     !< Fermi dk
real(8)                              :: E_CUT     !< cut off energy
real(8)                              :: EMIN_ELEC !< minimum energy electrons (for metallic tubes)
real(8)                              :: EMAX_ELEC !< maximum energy electrons (for metallic tubes)
integer                              :: NUM_GAUSS !< number of nodes for numeric integration
real(8), dimension(:), pointer       :: W         !< Weights for gauss hermite integration
real(8), dimension(:), pointer       :: X_        !< abscissas for gauss hermite integration


private

! --> public types ----------------------------------------------------------------------------------------------------
public semi_prop
public get_degeneracy_factor
public type_band
public supply_fermi_dirac

! --> variables -------------------------------------------------------------------------------------------------------
public band_pointer
public SEMI
public BS
public FE_TEMP
public E_CUT
public EMIN_ELEC
public EMAX_ELEC
public NUM_GAUSS
public X_
public W

! ---> subroutines/ functions -----------------------------------------------------------------------------------------
public init_bandstructure
public get_n_equ_dual
public get_n_equ_band_dual
public get_fermi_equ
public get_fermi_e
public get_boltz_e
public get_j_equ
public close_bandstructure
public get_conduction_band_index
public get_conduction_band_index2
public get_valence_band_index

contains

subroutine copy_band(to, from)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! subroutine used for copying bands with = operator
    !
    ! input
    ! -----
    ! to : type_band
    !   The band that is left of the = operator.
    ! from : type_band
    !   The band that is right of the = operator.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(type_band), allocatable, intent(out) :: to
    class(type_band), intent(in) :: from

    !allocate other
    select type(tb => from)
        type is (type_band)
            allocate(type_band::to)
        type is (type_para_band)
            allocate(type_para_band::to)
    end select

    ! write(*,*) '***error*** copy band called'
    !copy all variables (->this is very error prone!)
    to%name         = from%name
    to%valley       = from%valley
    to%type         = from%type
    to%type_para    = from%type_para
    to%iscb         = from%iscb
    to%fermi        = from%fermi
    to%dim          = from%dim
    to%e0           = from%e0
    to%e0_alpha     = from%e0_alpha
    to%e0_beta      = from%e0_beta
    to%n_eff        = from%n_eff
    to%al           = from%al
    to%deg          = from%deg
    to%e0_c         = from%e0_C

    if (allocated(from%pos_dep_n_eff)) then
        allocate(to%pos_dep_n_eff(size(from%pos_dep_n_eff)))
        to%pos_dep_n_eff = from%pos_dep_n_eff
    endif
    if (allocated(from%pos_dep_n_eff_output)) then
        allocate(to%pos_dep_n_eff_output(size(from%pos_dep_n_eff_output)))
        to%pos_dep_n_eff_output = from%pos_dep_n_eff_output
    endif
    if (allocated(from%pos_dep_al)) then
        allocate(to%pos_dep_al(size(from%pos_dep_al)))
        to%pos_dep_al = from%pos_dep_al
    endif
    if (allocated(from%pos_dep_m_eff)) then
        allocate(to%pos_dep_m_eff(size(from%pos_dep_m_eff)))
        to%pos_dep_m_eff = from%pos_dep_m_eff
    endif

    to%me     = from%me
    to%me_c   = from%me_c
    to%n_eff  = from%n_eff

end subroutine

subroutine init_bandstructure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize the bandstructure objects
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: is,i,is2,n
    integer :: id_materialA
    integer :: id_materialB
    integer :: dim_A, dim_B
    logical :: fermi_A, fermi_B
    character(len=64) :: valley_A,valley_B,valley_alloy

    ! ---> nullify
    nullify(SEMI)
    nullify(W,X_)  !weights and steps for gauss hermite quadrature

    ! ---> set parameters
    FE_DK     = 1e6                  ! todo: where can this parameter be set?
    FE_TEMP   = 300
    E_CUT     = dble(0)*US%mcbte%e_cut  ! todo: useful parameter?
    E_CUT     = max(E_CUT, BK*300/Q*log(0.99/0.01)) ! todo: check
    EMIN_ELEC = -E_CUT
    EMAX_ELEC =  E_CUT
    NUM_GAUSS = 25

    ! --> initialize gauss quadratur
    allocate(W(NUM_GAUSS))
    allocate(X_(NUM_GAUSS))

    ! --> init Semiconductor properties
    allocate(SEMI(N_SEMI))
    do is=1,size(SEMI)
        do i=1,US%n_semi
            if ((SEMI_MOD_NAME(is).eq.US%semi(i)%mod_name).or.(US%semi(i)%mod_name(1:7).eq.'default')) then
                write(SEMI(is)%mod_name,'(A)')       trim(US%semi(i)%mod_name)
                write(SEMI(is)%materialA,'(A)')       trim(US%semi(i)%materialA)
                write(SEMI(is)%materialB,'(A)')       trim(US%semi(i)%materialB)
                write(SEMI(is)%bgn_model,'(A)')       trim(US%semi(i)%bgn_model)
                write(SEMI(is)%strain,'(A)')         trim(US%semi(i)%strain)


                SEMI(is)%elec           = US%semi(i)%elec.ne.0
                SEMI(is)%elec2          = US%semi(i)%elec2.ne.0
                SEMI(is)%hole           = US%semi(i)%hole.ne.0
                SEMI(is)%fermi          = US%semi(i)%fermi.ne.0
                SEMI(is)%bgn_apparent   = US%semi(i)%bgn_apparent.ne.0
                SEMI(is)%bgn_is_classic = (SEMI(is)%bgn_model(1:4).eq.'clas')
                SEMI(is)%bgn_is_jain    = (SEMI(is)%bgn_model(1:4).eq.'jain')
                SEMI(is)%bgn_is_default = (SEMI(is)%bgn_model(1:4).eq.'defa')
                SEMI(is)%strain_is_sige = (SEMI(is)%strain(1:4).eq.'sige')
                SEMI(is)%strain_is_DOS = (SEMI(is)%strain(1:3).eq.'dos')

                SEMI(is)%dim    = US%semi(i)%dim
                SEMI(is)%temp0  = US%semi(i)%temp0

                SEMI(is)%eps   = US%semi(i)%eps
                SEMI(is)%eps_c = US%semi(i)%eps_c

                SEMI(is)%eoff   = US%semi(i)%eoff
                SEMI(is)%eoff_c = US%semi(i)%eoff_c


                SEMI(is)%bgap_gamhd = US%semi(i)%bgap_gamhd
                SEMI(is)%v_hd0      = US%semi(i)%v_hd0
                SEMI(is)%c_ref      = US%semi(i)%c_ref
                SEMI(is)%c_hd       = US%semi(i)%c_hd
                SEMI(is)%bgn_alpha  = US%semi(i)%bgn_alpha
                SEMI(is)%bgn_a      = US%semi(i)%bgn_a
                SEMI(is)%bgn_b      = US%semi(i)%bgn_b
                SEMI(is)%bgn_c      = US%semi(i)%bgn_c

            endif
        enddo
    enddo

    !set indices of alloy constituents for alloy semiconductors
    do is=1,size(SEMI)
        do i=1,US%n_semi
            if ((SEMI_MOD_NAME(is).eq.US%semi(i)%mod_name).or.(US%semi(i)%mod_name(1:7).eq.'default')) then
                !now we have is and i matching
                if ((US%semi(i)%materialA(1:2).eq.'  ').and.(US%semi(i)%materialB(1:4).eq.'  ')) then
                    cycle !this is not an alloy
                endif
                SEMI(is)%is_alloy = .true.
                !find material A
                do is2=1,size(SEMI)
                    if (SEMI_MOD_NAME(is2).eq.US%semi(i)%materialA) then
                        SEMI(is)%id_materialA = is2
                        exit
                    endif
                enddo
                !find material B
                do is2=1,size(SEMI)
                    if (SEMI_MOD_NAME(is2).eq.US%semi(i)%materialB) then
                        SEMI(is)%id_materialB = is2
                        exit
                    endif
                enddo

                if (US%semi(i)%materialA(1:2).ne.'  '.and.SEMI(is)%id_materialA.eq.0) then
                    write(*,'(A)') '***error*** did not find materialA.'
                    stop
                endif
                if (US%semi(i)%materialB(1:2).ne.'  '.and.SEMI(is)%id_materialB.eq.0) then
                    write(*,'(A)') '***error*** did not find materialB.'
                    stop
                endif

            endif
        enddo
    enddo

    ! --> init band structure
    do is=1,size(SEMI)
        call init_band(is)
    enddo

    ! set band indices, such that they match over alloy and its constitutents, e.g.
    ! alloy%band_pointer(1) -> gamma valley => semi_A%band_pointer(1) -> gamma valley
    do is=1,size(SEMI)
        if (SEMI(is)%is_alloy) then
            id_materialA = SEMI(is)%id_materialA
            id_materialB = SEMI(is)%id_materialB

            !0: write common band parameters that are not affected by bowing
            do n=1,size(SEMI(is)%band_pointer)
                !valley
                valley_A    = SEMI(id_materialA)%band_pointer(n)%band%valley
                valley_B    = SEMI(id_materialB)%band_pointer(n)%band%valley
                valley_alloy = SEMI(is)%band_pointer(n)%band%valley
                if (valley_alloy.ne.valley_A) then
                    write(*,'(A)') '***error*** alloy bandstructure does not match with constitutent A.'
                    stop
                endif
                if (valley_alloy.ne.valley_B) then
                    write(*,'(A)') '***error*** alloy bandstructure does not match with constitutent B.'
                    stop
                endif

                !dim
                dim_A = SEMI(id_materialA)%band_pointer(n)%band%dim
                dim_B = SEMI(id_materialB)%band_pointer(n)%band%dim
                if (dim_A.ne.dim_B) then
                    write(*,*) '***error*** Band dimensions between alloy constituents do not match.'
                    stop
                endif
                SEMI(is)%band_pointer(n)%band%dim = dim_A
                !fermi
                fermi_A = SEMI(id_materialA)%band_pointer(n)%band%fermi
                fermi_B = SEMI(id_materialB)%band_pointer(n)%band%fermi
                if (fermi_A.neqv.fermi_B) then
                    write(*,*) '***error*** Band fermi between alloy constituents do not match.'
                    stop
                endif
                SEMI(is)%band_pointer(n)%band%fermi = fermi_A

            enddo
        endif
    enddo

endsubroutine

subroutine init_parabolic_band(this, banddef)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init band properties special to parabolic bands
    !
    ! input
    ! -----
    ! this: type_band
    !   band object whose properties are set from the user defined information.
    ! this: u_banddef
    !   structure that contains user set band properties from the input.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(type_band),intent(inout)    :: this
    type(u_banddef)     :: banddef

    this%type_para=.True.

    if (banddef%band(1:2).eq.'vb') then
        this%e0   = -banddef%e0
    else
        this%e0   = banddef%e0
    endif
    this%e0_alpha = banddef%e0_alpha
    this%e0_beta  = banddef%e0_beta
    this%e0_c     = banddef%e0_c
    this%me       = banddef%m_eff*AM0
    this%me_c     = banddef%m_eff_c
    this%al       = banddef%al
    this%n_eff    = this%deg*2*(this%me*Q/HBAR/HBAR/dble(2)/PI)**(this%dim/dble(2))
    if (isnan(this%me)) then
        stop '"n_eff" is NaN'
    endif

endsubroutine

subroutine init_band(is)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bandstucture properties common to all band structure types
    !
    ! input
    ! -----
    ! is: integer
    !    The id of the semiconductor in the SEMI structure whose bands shall be inited.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer,intent(in) :: is !< semiconductor id

    integer                      :: i,iv,iv_cb,iv_vb,k

    class(type_band), allocatable :: band

    ! --> set number of subbands
    SEMI(is)%n_cb = 0
    SEMI(is)%n_vb = 0

    ! --> semiconducting band
    do i=1,US%n_banddef
        if ((US%banddef(i)%mod_name.eq.SEMI_MOD_NAME(is)).or.(US%banddef(i)%mod_name(1:7).eq.'default')) then
            if (((SEMI(is)%elec).or.(SEMI(is)%elec2)).and.(US%banddef(i)%band(1:2).ne.'vb')) then
                SEMI(is)%n_cb = SEMI(is)%n_cb + 1

            endif
            if ((SEMI(is)%hole).and.(US%banddef(i)%band(1:2).ne.'cb')) then
                SEMI(is)%n_vb = SEMI(is)%n_vb + 1

            endif
        endif
    enddo

    if (US%n_banddef.eq.0) then
        if (SEMI(is)%elec) SEMI(is)%n_cb = 1
        if (SEMI(is)%hole) SEMI(is)%n_vb = 1
    endif

    ! --> total number of subbands
    SEMI(is)%n_band = SEMI(is)%n_cb + SEMI(is)%n_vb

    ! --> init struct with channel band structure
    allocate(SEMI(is)%band_pointer(SEMI(is)%n_band))

    iv    = 0
    iv_cb = 0
    iv_vb = 0
    do i=1,US%n_banddef
        if ((US%banddef(i)%mod_name.eq.SEMI_MOD_NAME(is)).or.(US%banddef(i)%mod_name(1:7).eq.'default')) then
            do k=1,2 ! cb/vb

                !only init a band when everything is ok here
                if (k.eq.1) then
                    if (.not.((SEMI(is)%elec.or.SEMI(is)%elec2).and.(US%banddef(i)%band(1:2).ne.'vb'))) then
                        cycle
                    endif
                else
                    if (.not.((SEMI(is)%hole).and.(US%banddef(i)%band(1:2).ne.'cb'))) then
                        cycle
                    endif
                endif

                ! ------------------------------------------------PARABOLIC BAND-----
                if (US%banddef(i)%type.eq.'para') then
                    allocate(type_para_band::band)
                endif

                ! take over parameters common to all models
                if (k.eq.1) then
                    ! new conduction band
                    iv    = iv    + 1
                    iv_cb = iv_cb + 1
                    band%iscb   = .true.
                    write(band%name,'(A2,I1)') 'cb',iv_cb

                else
                    ! new valence band
                    iv    = iv    + 1
                    iv_vb = iv_vb + 1
                    band%iscb   = .false.
                    write(band%name,'(A2,I1)') 'vb',iv_vb

                endif

                write(band%type,'(A2)') US%banddef(i)%type 
                write(band%valley,'(A2)') US%banddef(i)%valley 

                call init_parabolic_band(band,US%banddef(i))

                band%is_gamma = (band%valley(1:1).eq.'g')
                band%is_l     = (band%valley(1:1).eq.'l')
                band%is_x     = (band%valley(1:1).eq.'x')
                band%is_hole  = (band%valley(1:1).eq.'h')
                if (.not.(band%is_gamma.or.band%is_l.or.band%is_x.or.band%is_hole)) then
                    write(*,'(A)') '***error*** valley of band not recognized. available: G, L, X, H'
                    stop
                endif


                band%fermi  = SEMI(is)%fermi
                band%dim    = SEMI(is)%dim
                band%type   = US%banddef(i)%type
                band%deg    = US%banddef(i)%deg

                !write band to semiconductor
                call copy_band(SEMI(is)%band_pointer(iv)%band, band)
                deallocate(band)

            enddo ! cb/vb
        endif ! mod_name ok
    enddo ! n_subband

    if (size(SEMI(is)%band_pointer).eq.0) then
        write(*,'(A)') '***error*** band_pointer of SEMI empty.'
        stop
    endif

endsubroutine

function get_fermi_equ(is,dop,hetero,degn,degp,degn2) result(ef)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculates the Fermi level for a given doping concentration
    ! using newton algorithm.
    !
    ! input
    ! -----
    ! is : integer
    !   The id of the semiconductor description that shall be used for the calculation.
    ! dop : real
    !   The ionized impurity density [1/m^3] that shall be assumed.
    ! hetero : integer
    !   If >0: Use the effective mass at position hetero in the bandstructure.
    ! degn : real,optional
    !   Condution band offset [eV] of conduction band, e.g. due to BGN.
    ! degn : real,optional
    !   Condution band offset [eV] of second conduction band, e.g. due to BGN.
    ! degp : real,optional
    !   Valence band offset [eV] of valence band, e.g. due to BGN.
    !
    ! output
    ! ------
    ! ef : real
    !   Equilibirum Fermi level [eV].
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer          :: is
    real(8)          :: dop
    integer          :: hetero
    real(8),optional :: degn
    real(8),optional :: degn2
    real(8),optional :: degp
    real(8)          :: ef

    real(8) :: vt,err,def,den,den2,dep
    real(8) :: n,p,f,dn,dp
    integer :: i,ib,nmax,iv
    logical :: is_elec,is_hole,is_elec2

    TYPE(DUAL_NUM) :: result=DUAL_NUM(0,0)

    if (present(degn)) then
        den = degn
    else
        den = dble(0)
    endif

    if (present(degn2)) then
        den2 = degn2
    else
        den2 = dble(0)
    endif

    if (present(degp)) then
        dep = degp
    else
        dep = dble(0)
    endif

    vt       = BK*TEMP_LATTICE/Q
    nmax     = 1000
    err      = 1e-6
    ef       = 0
    is_elec  = SEMI(is)%elec
    is_elec2 = SEMI(is)%elec2
    is_hole  = SEMI(is)%hole

    if     ((.not.is_elec).and.(.not.is_elec2)) then
        n  = 0
        dn = 0
          
    elseif (.not.is_hole) then
        p  = 0
        dp = 0
      
    endif

    ! initial guess for Fermi level
    if (dop.eq.0) then
        ef  = 0

    elseif (dop.gt.0) then
        if (.not.(SEMI(is)%elec.or.SEMI(is)%elec2)) then
            write(*,'(A)') '***error*** donator doping without electron transport'
            stop
        endif
        iv  = get_conduction_band_index(SEMI(is))
        ef  = max(dble(0),vt*log(+dop  /( SEMI(is)%band_pointer(iv)%band%pos_dep_n_eff(hetero)*vt**(SEMI(is)%band_pointer(iv)%band%dim/dble(2)) ))+SEMI(is)%band_pointer(iv)%band%e0+den) ! todo: check that (vt not in neff)

    else
        if (.not.SEMI(is)%hole) then
            write(*,'(A)') '***error*** acceptor doping without hole transport'
            stop
        endif
        iv  = get_valence_band_index(SEMI(is))
        ef  = -max(dble(0),vt*log(-dop  /( SEMI(is)%band_pointer(iv)%band%pos_dep_n_eff(hetero)*vt**(SEMI(is)%band_pointer(iv)%band%dim/dble(2)) ))+SEMI(is)%band_pointer(iv)%band%e0+dep)
    endif

    ! --> newton
    do i=1,nmax
        if (is_elec) then
            ib      = get_conduction_band_index(SEMI(is))
            result  = get_n_equ_band_dual(is,ib,DUAL_NUM(ef+den,1),DUAL_NUM(vt,0),hetero)
            n       = result%x_ad_
            dn      = result%xp_ad_(1)
          
        endif
        if (is_elec2) then
            ib      = get_conduction_band_index2(SEMI(is))
            result  = get_n_equ_band_dual(is,ib,DUAL_NUM(ef+den2,1),DUAL_NUM(vt,0),hetero)
            n       = n + result%x_ad_
            dn      = dn + result%xp_ad_(1)
          
        endif
        if (is_hole) then
            ib      = get_valence_band_index(SEMI(is))
            result  = get_n_equ_band_dual(is,ib,DUAL_NUM(ef+dep,1),DUAL_NUM(vt,0),hetero)
            p       = result%x_ad_
            dp      = result%xp_ad_(1)
          
        endif
        
        if (dop.NE.0) then
            f   = n - p - dop
            def = -f/(dn-dp)
              
        elseif (dop.eq.0) then
            if ((n.ne.p).and.(abs(log(n)-log(p)).gt.10)) then
                ! big difference in n and p
                f   = log(abs(n - p ))
                def = -f/(n-p-dop)*(dn-dp)
            
            else
                f   = n - p - dop
                def = -f/(dn-dp)
              
            endif
        endif
        
        
        if (def.gt.0.1)     def = 0.1
        if (def.lt.(-0.1))  def = -0.1
          
        ef = ef + def
        
        if ((abs(def).le.(err))) then
            exit
        endif
        if (i.eq.nmax) then
            write(*,*) 'Newton error, ef could not be determined'
            stop
        endif
    enddo

endfunction

function get_n_equ_dual(is,ef,iselec,vt,hetero,deg) result(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculates the total carrier density of given type "iselec" for given fermi level using dual numbers
    !
    ! input
    ! -----
    ! is : integer
    !   Index of semiconductor in SEMI struct whose properties shall be used for the calculation.
    ! ef : dual_num
    !   Fermi level [eV] that is assumed for the calculation.
    ! iselec : logical
    !   If true: calculate the total electron density, else: calculate the total hole density.
    ! vt : dual_num
    !   The thermal voltage [V] that is assumed for the calculation
    ! hetero : integer
    !   If >0: Use the effective mass with index hetero in the band's position dependent effective mass.
    ! deg : dual_num,optional
    !   Possible bandgap offset [eV] used in the calculation.
    !
    ! output
    ! ------
    ! n : dual_num
    !   The total carrier density [1/m^d].
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer                 :: is
    TYPE(DUAL_NUM)          :: ef
    logical                 :: iselec
    TYPE(DUAL_NUM)          :: vt
    integer                 :: hetero
    TYPE(DUAL_NUM),optional :: deg

    TYPE(DUAL_NUM)          :: n

    integer :: iv

    n = 0
    do iv=1,size(SEMI(is)%band_pointer) ! iterate over bands
        if (SEMI(is)%band_pointer(iv)%band%iscb.neqv.iselec) cycle ! take bands of correct type
        if (present(deg)) then
            n = n + get_n_equ_band_dual(is,iv,ef,vt,hetero)

        else
            n = n + get_n_equ_band_dual(is,iv,ef,vt,hetero)

        endif
    enddo

endfunction

function get_n_equ(is,ef,iselec,vt,hetero,deg) result(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculates the total carrier density of given type "iselec" for given fermi level using real numbers.
    !
    ! input
    ! -----
    ! is : integer
    !   Index of semiconductor in SEMI struct whose properties shall be used for the calculation.
    ! ef : real
    !   Fermi level [eV] that is assumed for the calculation.
    ! iselec : logical
    !   If true: calculate the total electron density, else: calculate the total hole density.
    ! vt : real
    !   The thermal voltage [V] that is assumed for the calculation
    ! hetero : integer
    !   If >0: Use the effective mass with index hetero in the band's position dependent effective mass.
    ! deg : real,optional
    !   Possible bandgap offset [eV] used in the calculation.
    !
    ! output
    ! ------
    ! n : dual_num
    !   The total carrier density [1/m^d].
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer          :: is
    real(8)          :: ef
    logical          :: iselec
    real(8)          :: vt
    integer          :: hetero
    real(8),optional :: deg

    real(8)          :: n

    TYPE(DUAL_NUM) :: result !intermediate variable

    integer :: iv

    n = 0
    do iv=1,size(SEMI(is)%band_pointer) ! iterate over bands
        if (SEMI(is)%band_pointer(iv)%band%iscb.neqv.iselec) cycle ! take bands of correct type
        if (present(deg)) then
            result = get_n_equ_band(is,iv,ef,vt,hetero)
            n = n + result%x_ad_

        else
            result = get_n_equ_band(is,iv,ef,vt,hetero)
            n = n + result%x_ad_

        endif
    enddo

endfunction

function get_n_equ_band(is,ib,ef,vt,hetero) result(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculates the total carrier density of given type "iselec" for given fermi level and band using real numbers
    ! ->basically a wrapper for get_n_equ_band_dual
    !
    ! input
    ! -----
    ! is : integer
    !   Index of semiconductor in SEMI struct whose properties shall be used for the calculation.
    ! ef : real
    !   Fermi level [eV] that is assumed for the calculation.
    ! iselec : logical
    !   If true: calculate the total electron density, else: calculate the total hole density.
    ! vt : real
    !   The thermal voltage [V] that is assumed for the calculation
    ! hetero : integer
    !   If >0: Use the effective mass with index hetero in the band's position dependent effective mass.
    ! deg : real,optional
    !   Possible bandgap offset [eV] used in the calculation.
    !
    ! output
    ! ------
    ! n : real
    !   The carrier density for given fermi level and thermal voltage in band ib of semiconductor is.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer             :: is     !< index to semiconductor
    integer             :: ib     !< index to band in semiconductor
    real(8)             :: ef     !< [eV] difference to intrinsic fermi level
    real(8)             :: vt     !< [V] thermal voltage
    integer             :: hetero !< if >0: use pos dependent n_eff at index hetero
    real(8)             :: n      !< [1/m^d] electron/hole density
    TYPE(DUAL_NUM) :: result

    result = get_n_equ_band_dual(is,ib,DUAL_NUM(ef,0),DUAL_NUM(vt,0),hetero)
    n      = result%x_ad_
endfunction

function get_n_equ_band_dual(is,ib,ef,vt,hetero) result(n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Calculates the total carrier density of given type "iselec" for given fermi level and band using dual numbers
    !
    ! input
    ! -----
    ! is : integer
    !   Index of semiconductor in SEMI struct whose properties shall be used for the calculation.
    ! is : integer
    !   Index of band in SEMI struct whose properties shall be used for the calculation.
    ! ef : dual_num
    !   Fermi level [eV] that is assumed for the calculation.
    ! iselec : logical
    !   If true: calculate the total electron density, else: calculate the total hole density.
    ! vt : dual_num
    !   The thermal voltage [V] that is assumed for the calculation
    ! hetero : integer
    !   If >0: Use the effective mass with index hetero in the band's position dependent effective mass.
    ! deg : real,optional
    !   Possible bandgap offset [eV] used in the calculation.
    !
    ! output
    ! ------
    ! n : dual_num
    !   The carrier density for given fermi level and thermal voltage in band ib of semiconductor is
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer          :: is
    integer          :: ib
    TYPE(DUAL_NUM)   :: ef
    TYPE(DUAL_NUM)   :: vt
    integer          :: hetero
    TYPE(DUAL_NUM)   :: n

    TYPE(DUAL_NUM)       :: ef0,n_eff,nu,val,nc
    type(band_pointer),pointer  :: bs     !< pointer to band
    real(8) :: al,a

    bs => SEMI(is)%band_pointer(ib) 

    if (bs%band%iscb) then !difference between n type and p type carriers is just the sign!
        ef0 = ef
    else
        ef0 = -ef
    endif

    if (hetero.GT.0) then !take correct effective mass/n_eff
        n_eff = bs%band%pos_dep_n_eff(hetero)
        al = bs%band%pos_dep_al(hetero)
    else
        al = bs%band%al
        n_eff = bs%band%n_eff
    endif

    n = 0

    nu = (ef0-bs%band%e0)/vt ! (ec- ef) / vt

    if (CRYO) then !limit nu to prevent underflow. Paper: "Robust Simulation of Poisson’s Equation in a P-N Diode Down to 1 µK"
        a = 270 !limits nu and therefore density, empirically set for now
        nu = a*log(10.0)*tanh_dual(nu/a/log(10.0))
    endif

    nc = n_eff*vt**(bs%band%dim/dble(2))

    if (bs%band%fermi) then
        if (bs%band%dim.eq.1) then
            ! 1D
            n     = fdm1h(nu)*nc

        elseif (bs%band%dim.eq.2) then
            ! 2D
            if (abs(ef0-bs%band%e0).le.0.5) then
                n = n_eff*vt*( log(exp(-nu)+dble(1))+(nu) ) ! in 1/m^2

            else ! the log function has too large numerical error at small arguments
                n = n_eff*vt*( exp(nu) )
                if (n.eq.dble(0)) n=1e-3

            endif

        elseif (bs%band%dim.eq.3) then 
            if (al.eq.0) then !parabolic
                n     = fd1h(nu)*nc
            else  !nonparabolic from "FERMI-DIRAC INTEGRALS AND FERMI ENERGY FOR DEGENERATE NARROW-GAP SEMICONDUCTORS", C VAN, 1982
                val   = fd1h(nu)
                n     = (val + fd3h(nu)*dble(15)/dble(4)*al*vt)*nc
                ! write Nc for output
                bs%band%pos_dep_n_eff_output(hetero) = n%x_ad_/val%x_ad_
            endif
       endif

    else ! boltzmann
        ! valid for 1D,2D,3D semiconductors
        n  = nc * exp(nu) 
    endif

    n = n/NORM%N_N !normalize result

endfunction

! ---------------------------------------------------------------------------------------------------------------------
!> \brief ballistic current ... obsolete?
!> \details
function get_j_equ(ef,bs,iscb,vt) result(j)

real(8)                 :: ef    !< [eV] fermi energy
class(band_pointer),dimension(:) :: bs    !< band structure parameter
logical                 :: iscb  !< if true: conduction band
real(8),optional        :: vt    !< [V] thermal voltage
real(8)                 :: j     !< current

integer :: iv


j = 0
do iv=1,size(bs) ! subband
  if (bs(iv)%band%iscb.neqv.iscb) cycle ! only bands with elec or hole
  if (present(vt)) then
    j = j + get_j_equ_iv(ef,bs(iv),vt)

  else
    j = j + get_j_equ_iv(ef,bs(iv))

  endif

enddo

endfunction


! ---------------------------------------------------------------------------------------------------------------------
!> \brief ballistic current for single subband ... obsolete?
!> \details 
function get_j_equ_iv(ef,bs,vt) result(j)

real(8)          :: ef    !< [eV] fermi energy
class(band_pointer)       :: bs    !< band structure parameter
real(8),optional :: vt    !< [V] thermal voltage
real(8)          :: j     !< ballistic current

real(8) :: ef0,vt0


if (bs%band%iscb) then
  ef0 =  ef
else
  ef0 = -ef
endif

if (present(vt)) then
  vt0 = vt
else
  vt0 = BK*FE_TEMP/Q
endif

! thermionic
! semiconducting
j = bs%band%deg/PI*vt0*Q/HBAR*log(dble(1)+exp((ef0-bs%band%e0)/vt0))
  

endfunction

! ---------------------------------------------------------------------------------------------------------------------
!> \brief Fermi distr. function for a given Fermi level and energy
!> \details e=ef --> f=0.5,  e>ef --> f<0.5,  e<ef --> f>0.5
function get_fermi_e(ef,e,vt) result(f)

real(8) :: ef !< [eV] fermi energy
real(8) :: e  !< [eV] energy
real(8) :: vt !< [V] thermal voltage
real(8) :: f  !< boltzmann distr. function

real(8) :: arg


arg = (e-ef)/vt
f   = dble(1)/(dble(1)+exp(arg))

endfunction


! ---------------------------------------------------------------------------------------------------------------------
!> \brief boltzmann distr. function for a given Fermi level and energy
!> \details e=ef --> f=1,  e>ef --> f<1,  e<ef --> f>1
function get_boltz_e(ef,e,vt) result(f)

real(8) :: ef !< [eV] fermi energy
real(8) :: e  !< [eV] energy
real(8) :: vt !< [V] thermal voltage
real(8) :: f  !< boltzmann distr. function

real(8) :: arg


arg = (e-ef)/vt
f   = exp(-arg)

endfunction

subroutine close_bandstructure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !deallocate everything form this module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer ::n,i
    do n=1,size(SEMI)
      do i=1,size(SEMI(n)%band_pointer)
        if (allocated(SEMI(n)%band_pointer(i)%band)) deallocate(SEMI(n)%band_pointer(i)%band)
      enddo
      if (allocated(SEMI(n)%band_pointer)) deallocate(SEMI(n)%band_pointer)
      if (allocated(SEMI(n)%rec_models)) deallocate(SEMI(n)%rec_models)
    enddo

    if (associated(SEMI)) deallocate(SEMI) !todo: check if all internal pointer have to be deallocated first
    if (associated(SEMI)) deallocate(SEMI) !todo: check if all internal pointer have to be deallocated first
    if (associated(W))    deallocate(W)
    if (associated(X_))   deallocate(X_)

    endsubroutine

function get_valence_band_index(semi) result(index)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! return the index of the valence band for semiconductor semi
    ! if no band is found, return 0.
    !
    ! input
    ! -----
    ! semi : semi_prop
    !   The semiconductor whose bands shall be searched.
    !
    ! output
    ! ------
    ! index : integer
    !   the index of the valence band.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: index        !the chosen index
    type(semi_prop) :: semi !the semiconductor

    integer :: n=0

    index = 0
    do n=1,size(semi%band_pointer)
        if (semi%band_pointer(n)%band%iscb) then 
            cycle
        endif
        !found valence band
        index = n
    enddo

endfunction

function get_conduction_band_index(semi) result(index)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! return the index of the lowest conduction band for semiconductor semi
    ! if no band is found, return 0
    !
    ! input
    ! -----
    ! semi : semi_prop
    !   The semiconductor whose bands shall be searched.
    !
    ! output
    ! ------
    ! index : integer
    !   the index of the first conduction band.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(semi_prop) :: semi !semiconductor structure
    integer :: index !index of the conduction band

    real(8) :: ec    !conduction band edge as defined by user
    integer :: n=0

    index = 0
    ec    = 100
    do n=1,size(semi%band_pointer)
        if (.not.semi%band_pointer(n)%band%iscb) then 
            cycle
        endif
        !found conduction band
        if (semi%band_pointer(n)%band%e0.lt.ec) then
            ec    = semi%band_pointer(n)%band%e0
            index = n
        endif
    enddo

endfunction

function get_conduction_band_index2(semi) result(index)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! return the index of the highest conduction band for semiconductor semi
    ! if no band is found, return 0.
    !
    ! input
    ! -----
    ! semi : semi_prop
    !   The semiconductor whose bands shall be searched.
    !
    ! output
    ! ------
    ! index : integer
    !   the index of the second conduction band.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(semi_prop) :: semi
    integer :: index
    integer :: n=0
    real(8) :: ec, ec_n

    index = 0
    ec    = 0
    do n=1,size(semi%band_pointer)
        if (.not.semi%band_pointer(n)%band%iscb) then 
            cycle
        endif
        !found conduction band
        ec_n = semi%band_pointer(n)%band%e0
        if (ec_n.gt.ec) then
            ec = ec_n
            index = n
        endif
    enddo

endfunction

function get_degeneracy_factor(band, dens, t, n) result(g)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !return the diffusion enhancenemtn factor as defined in Kantner paper.
    !
    ! input
    ! -----
    ! t : real
    !   The temperature [K] for which the calculation is performed.
    ! dens : real  
    !   The carrier density [1/m^d] that shall be used for the calculation.
    ! band : type_band
    !   The band properties used for the calculation.
    ! n : integer
    !   Index to the point in the SP structure.
    !
    ! output
    ! ------
    ! g : real
    !   The degeneracy factor.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: t    ! temperature
    real(8) :: g    ! diff. enhancement factor
    integer :: n    ! index to SP
    type(type_band) :: band    ! band for which the factor shall be calculated
    real(8) :: dens ! carrier density at point n
    real(8) :: vth  ! thermal voltage at point n
    real(8) :: n_eff! DOS prefactor
    real(8) :: nu   ! chemical potential
    g = 1
    if (band%fermi) then
        if (band%type_para) then
            vth   = t*BK/Q
            n_eff = band%pos_dep_n_eff(n)*vth**(band%dim/dble(2))
            if (dens.lt.n_eff/1e20) then
                g = 1
            else
                nu    = fd1h_inv(dens/n_eff)
                g     = dens/n_eff/dfd1h(nu)
            endif
        endif
    endif
endfunction

TYPE(DUAL_NUM) function supply_fermi_dirac(e,ef_l,ef_r,vt,t)
    ! supply function assuming fermi-dirac statistics
    ! e : energy at which particles fly
    ! ef_l : left fermi level
    ! ef_r : right fermi level
    TYPE(DUAL_NUM) :: e,ef_l,ef_r
    real(8)        :: vt,t

    !if (ef_l.eq.ef_r) then
    !supply_fermi_dirac = 0
    !else
    supply_fermi_dirac = BK*t*log( ( 1+exp(-(e-ef_l)/vt) )  /  ( 1+exp(-(e-ef_r)/vt) )  )
    !endif

endfunction

endmodule
