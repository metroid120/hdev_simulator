!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! functions for crs (compressed row storage format) matrices
module crs_matrix

! ---> variables and functions from other modules ----------------------------------------------------------------------
! use mkl_dss, only : MKL_DSS_HANDLE
! use mkl_dss, only : DSS_FACTOR_REAL_D
! use mkl_dss, only : MKL_DSS_NON_SYMMETRIC
! use mkl_dss, only : DSS_DEFINE_STRUCTURE
! use mkl_dss, only : MKL_DSS_DEFAULTS
! use mkl_dss, only : DSS_CREATE
! use mkl_dss, only : MKL_DSS_AUTO_ORDER
! use mkl_dss, only : DSS_REORDER


implicit none


! ---> crs real matrix type -------------------------------------------------------------------------------------------
type, abstract :: crs_mat
    integer                       :: nr        = 0       !< number of rows
    integer                       :: nc        = 0       !< number of columns
    integer                       :: nz        = 0       !< number of nonzero elements
    integer,dimension(:),allocatable  :: row_index       !< (nc+1) row index  (CRS matrix format)
    integer,dimension(:),allocatable  :: columns         !< (nz  ) column index  (CRS matrix format)
    logical                       :: active    = .False. !if 1: the matrix is initialized
    contains
    procedure,pass :: init  ! allocate space and handle
    procedure,pass :: check ! check if matrix properties are consistent
endtype

! ---> crs real matrix type ----------------------------------------------------------------------------------------
type, extends(crs_mat) :: crs_mat_real
    real(8),dimension(:),allocatable  :: values     !< (nz  ) nonzero elements in matrix
    contains
endtype

! ---> crs complex matrix type ----------------------------------------------------------------------------------------
type, extends(crs_mat) :: crs_mat_cmplx
    complex(8),dimension(:),allocatable  :: values  !< (nz  ) nonzero elements in matrix
    contains
endtype

!this allows to define arrays of pointers to crs_mats
type crs_mat_real_pointer
    type(crs_mat_real), pointer :: p => null()
endtype 

type(crs_mat_real_pointer),dimension(:),pointer :: last_rows_real
type(crs_mat_real) :: last_out_real

interface join_mats 
    module procedure join_mats_real, join_mats_cmplx
end interface

interface add_crs_col 
    module procedure add_crs_col_real, add_crs_col_cmplx
end interface

!todo
! interface add_crs_cols !allows to call add_crs_col with complex and real valued matrix, correct procedure assigned based on argument type
!   module procedure add_crs_cols_real, add_crs_cols_cmplx
! end interface

interface add_crs_row 
    module procedure add_crs_row_real, add_crs_row_cmplx
end interface

!todo
! interface add_crs_rows !allows to call add_crs_row with complex and real valued matrix, correct procedure assigned based on argument type
!   module procedure add_crs_rows_real, add_crs_rows_cmplx
! end interface

contains

subroutine init(mat,nr,nc,nz,is_ccs)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! allocate space for the matrix.
    !
    ! input
    ! -----
    ! mat : crs_mat
    !   The matrix object.
    ! nr : integer
    !   The number of rows of the matrix.
    ! nc : integer
    !   The number of columns of the matrix.
    ! nz : integer
    !   The number of nonzeros of the matrix.
    ! is_ccs : logical
    !   if True: this is a CCS matrix, else CRS.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(crs_mat),intent(inout) :: mat !< crs matrix
    integer,intent(in) :: nr
    integer,intent(in) :: nc
    integer,intent(in) :: nz 
    logical,optional :: is_ccs 
    logical :: build_ccs = .false.

    mat%nr=nr
    mat%nz=nz
    mat%nc=nc

    build_ccs = .false.

    if (present(is_ccs)) then
        build_ccs = is_ccs
    endif

    ! --> nullify
    !nullify(mat%row_index)
    !nullify(mat%columns)
    select type (mat)
      class is  (crs_mat_real)
        !nullify(mat%values)
      class is (crs_mat_cmplx)
        !nullify(mat%values)
    end select

    ! --> allocate
    if (.not.build_ccs) then
        allocate(mat%row_index(mat%nr+1))
        allocate(mat%columns(mat%nz))
    else !ccs matrix
        allocate(mat%row_index(mat%nz))
        allocate(mat%columns(mat%nc+1))
    endif

    select type (mat)
        class is (crs_mat_real)
            allocate(mat%values(mat%nz))
        class is (crs_mat_cmplx)
            allocate(mat%values(mat%nz))
    end select

    ! --> set to zero
    mat%columns(:)   = 0
    mat%row_index(:) = 0
    select type (mat)
        class is (crs_mat_real)
            mat%values(:)    = dble(0)
        class is (crs_mat_cmplx)
            mat%values(:)    = dble(0)
    end select

    mat%active = .True.

endsubroutine

subroutine dealloc_crs(mat)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! deallocate the propertis of the matrix.
    !
    ! input
    ! -----
    ! mat: crs_mat
    !  The matrix to deallocate.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    class(crs_mat),intent(inout) :: mat !< crs matrix

    ! --> deallocate
    if (allocated(mat%row_index)) deallocate(mat%row_index)
    if (allocated(mat%columns))   deallocate(mat%columns)
    select type (mat)
        class is (crs_mat_cmplx)
            if (allocated(mat%values)) deallocate(mat%values)
        class is (crs_mat_real)
            if (allocated(mat%values)) deallocate(mat%values)
    end select

    ! --> nullify
    !nullify(mat%row_index)
    !nullify(mat%columns)
    select type (mat)
        class is (crs_mat_cmplx)
            !nullify(mat%values)
        class is (crs_mat_real)
            !nullify(mat%values)
    end select

    !put counters back to 0
    mat%nz = 0
    mat%nr = 0
    mat%nc = 0

    mat%active = .False.

endsubroutine


subroutine copy_crs(from,to)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Copy structure of "from" crs matrix to "to" crs matrix.
    !
    ! input
    ! -----
    ! from : crs_mat
    !   The "from" matrix.
    ! to : crs_mat
    !   The "to" matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(crs_mat),intent(in ) :: from !< crs matrix real
    class(crs_mat),intent(out) :: to !< crs matrix real

    call to%init(nr=from%nr, nc=from%nc, nz=from%nz)
    to%row_index = from%row_index
    to%columns   = from%columns

endsubroutine

! ---------------------------------------------------------------------------------------------------------------------
!> \brief copy real values to real part of complex matrix
!> \details
subroutine copy_crs_real_part(mat_r,mat_c)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Copy real values to real part of complex matrix.
    !
    ! input
    ! -----
    ! mat_r : crs_mat_real
    !   The real valued matrix.
    ! mat_c : crs_mat_cmplx
    !   The complex valued matrix.
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> take over real part
    class(crs_mat_real), intent(in ) :: mat_r !< crs matrix real
    class(crs_mat_cmplx),intent(inout) :: mat_c !< crs matrix complex

    integer :: n

    ! --> take over real part
    do n=1,mat_r%nz
        mat_c%values(n) = cmplx(mat_r%values(n),dble(0),8)
    enddo

endsubroutine

function add_crs_col_real(mat_1,mat_2) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating two crs matrices [A,B] that have the same number of rows.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   left matrix.
    ! mat_2 : crs_mat_real
    !   right matrix.
    ! mat_3 : crs_mat_real
    !   resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                      
    type(crs_mat_real),intent(in)  :: mat_1
    type(crs_mat_real),intent(in)  :: mat_2
    type(crs_mat_real) :: mat_3

    integer :: i,j,n

    if (.not.(mat_1%active.or.mat_2%active)) then
        return
    elseif (.not.(mat_1%active)) then
        call copy_crs(mat_2, mat_3)
        mat_3%values = mat_2%values
        return
    elseif (.not.(mat_2%active)) then
        call copy_crs(mat_1, mat_3)
        mat_3%values = mat_1%values
        return
    endif

    if (mat_1%nr.ne.mat_2%nr) then
        write(*,*) '***error*** concatenating matrices, not same number of rows'
        stop

    endif

    call mat_3%init(nr=mat_1%nr, nc=mat_1%nc + mat_2%nc ,nz=mat_1%nz + mat_2%nz)

    if (mat_2%nz.eq.0) then
        ! add matrix with zeros
        mat_3%row_index(1:mat_1%nr+1) = mat_1%row_index(1:mat_1%nr+1)
        mat_3%columns(1:mat_1%nz)     = mat_1%columns(1:mat_1%nz)
        mat_3%values(1:mat_1%nz)      = mat_1%values(1:mat_1%nz)
        return

    endif

    do i=1,mat_1%nr
        mat_3%row_index(i)  = mat_1%row_index(i)-1 + mat_2%row_index(i)-1 + 1
    enddo
    mat_3%row_index(mat_3%nr+1) = mat_3%nz+1

    n=0
    do i=1,mat_1%nr
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            n=n+1
            mat_3%columns(n) = mat_1%columns(j)
            mat_3%values(n)  = mat_1%values(j)
        enddo
        do j=mat_2%row_index(i),mat_2%row_index(i+1)-1
            n=n+1
            mat_3%columns(n) = mat_1%nc + mat_2%columns(j)
            mat_3%values(n)  = mat_2%values(j)
        enddo
    enddo

endfunction

function add_crs_cols_real(mats, last_col) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating a vector of crs matrices from left to right that have the same number of rows.
    !
    ! input
    ! -----
    ! mats : crs_mat_real_pointer
    !   array to pointers of  matrixes to concatenate.
    ! last_col : crs_mat_real
    !   pointer to last result of this operation. If the result is still allocated, it will be overwritten. Saves allocate time.
    !
    ! output
    ! -----
    ! mat_3 : crs_mat_real
    !   pointer to resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                      
    type(crs_mat_real_pointer),dimension(:),intent(in)  :: mats !< vector of crs matrixes
    type(crs_mat_real_pointer) :: mat_3 !< resulting crs matrix
    type(crs_mat_real_pointer),optional :: last_col !< resulting crs matrix
    type(crs_mat_real_pointer),dimension(:),allocatable :: active_mats

    integer :: i,j,k,n,nr,nz,nc
    integer :: n_active


    n_active = 0
    do i=1,size(mats)
        if (associated(mats(i)%p)) then
            if (mats(i)%p%active) n_active = n_active + 1
        endif
    enddo

    if (n_active.eq.0) then
        return
    endif

    ! pointer to only active matrices
    ! maybe make this global
    allocate(active_mats(n_active))
    n_active = 0
    do i=1,size(mats)
        if (.not.associated(mats(i)%p)) cycle
        if (mats(i)%p%active) then
            n_active = n_active + 1
            active_mats(n_active)%p => mats(i)%p
            nr = active_mats(n_active)%p%nr
            if (nr.eq.0) then
                write(*,*) '***error*** active matrix with zero rows.'
                stop
            endif
        endif
    enddo

    nr=0
    nr = active_mats(1)%p%nr
    do i=2,n_active
        if (active_mats(i)%p%nr.ne.nr) then
            write(*,*) '***error*** concatenating matrices, not same number of rows'
            stop
        endif
    enddo

    nc = 0
    nz = 0
    do i=1,n_active
        nc = nc + active_mats(i)%p%nc
        nz = nz + active_mats(i)%p%nz
    enddo

    if (present(last_col)) then
        !previous result given, assume shape has not changed
        !todo: detect if shape changed, e.g. TUNNELING
        n=size(last_col%p%values)
        mat_3 = last_col
        !only copy values, rows, and cols unchanged
        n=1
        do i=1,nr
            do j=1,n_active
                do k=active_mats(j)%p%row_index(i),active_mats(j)%p%row_index(i+1)-1 
                    mat_3%p%values(n)  = active_mats(j)%p%values(k)
                    n                  = n  + 1
                enddo
                ! mat_3%p%values(n:n + (active_mats(j)%p%row_index(i+1)-1-active_mats(j)%p%row_index(i))) &
                ! & = active_mats(j)%p%values(active_mats(j)%p%row_index(i):active_mats(j)%p%row_index(i+1)-1 )
                ! n = n  + (active_mats(j)%p%row_index(i+1)-1-active_mats(j)%p%row_index(i))
            enddo
        enddo

    else
        allocate(mat_3%p)
        call mat_3%p%init(nr=nr, nc=nc, nz=nz)

        ! if (mat_2%nz.eq.0) then
        !   ! add matrix with zeros
        !   mat_3%row_index(1:mat_1%nr+1) = mat_1%row_index(1:mat_1%nr+1)
        !   mat_3%columns(1:mat_1%nz)     = mat_1%columns(1:mat_1%nz)
        !   mat_3%values(1:mat_1%nz)      = mat_1%values(1:mat_1%nz)
        !   return

        ! endif

        do i=1,mat_3%p%nr
            mat_3%p%row_index(i) = 0
            do j=1,n_active
                mat_3%p%row_index(i)  = mat_3%p%row_index(i) + active_mats(j)%p%row_index(i)-1
            enddo
            mat_3%p%row_index(i)  = mat_3%p%row_index(i) + 1
        enddo
        mat_3%p%row_index(mat_3%p%nr+1) = mat_3%p%nz + 1

        !set columns
        n=1
        do i=1,mat_3%p%nr
            nc = 0
            do j=1,n_active
                do k=active_mats(j)%p%row_index(i),active_mats(j)%p%row_index(i+1)-1 
                    mat_3%p%columns(n) = nc + active_mats(j)%p%columns(k)
                    mat_3%p%values(n)  = active_mats(j)%p%values(k)
                    n                = n  + 1
                enddo
                nc               = nc + active_mats(j)%p%nc
            enddo
        enddo
    endif

endfunction

subroutine convert_crs_ccs(mat_1, mat_3) 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Convert crs to ccs matrix. 
    ! https://stackoverflow.com/questions/26576984/how-to-convert-from-a-compressed-row-storage-to-a-compressed-column-storage-of-a
    ! Quite complicated! Need to understand theory thoroughly to understand routine!
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   crs matrix.
    !
    ! output
    ! ------
    ! mat_3 : ccs_mat_real
    !   ccs matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_real),intent(in)  :: mat_1
    type(crs_mat_real),intent(inout) :: mat_3
    integer,allocatable,dimension(:) :: counter

    integer :: k,j,n,i,index,nz,nc,nr

    nc = mat_1%nc
    nr = mat_1%nr
    if (nc.ne.nr) then
        write(*,*) '***error*** not implemented for nc != nr .'
        stop

    endif

    n  = mat_1%nc
    nz = mat_1%nz
    allocate(counter(n))
    counter = 0

    k = mat_1%nz
    k = size(mat_1%values)

    do i=1,mat_1%nz !Anzahl Elemente pro Spalte
        k          = mat_1%columns(i)
        counter(k) = counter(k) + 1
    enddo
    mat_3%columns(1) = 1
    do j=1,n !für jede Spalte
        mat_3%columns(j+1) = mat_3%columns(j) + counter(j)
    enddo

    counter = 0
    do i=1,n !für jede Reihe
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1 !Für jedes Element in Reihe
            k                         = mat_1%columns(j)
            index                     = mat_3%columns(k)    !Index erstes Element in Spalte
            index                     = index + counter(k)  !Anzahl Elemente IN Aktueller spalte
            mat_3%values(index)       = mat_1%values(j)
            mat_3%row_index(index)    = i
            counter(k)                = counter(k) + 1
        enddo
    enddo

endsubroutine

subroutine convert_crs_ccs_complex(mat_1, mat_3) 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Convert crs to ccs matrix. 
    ! https://stackoverflow.com/questions/26576984/how-to-convert-from-a-compressed-row-storage-to-a-compressed-column-storage-of-a
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   crs matrix.
    !
    ! output
    ! ------
    ! mat_3 : ccs_mat_real
    !   ccs matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_cmplx),intent(in)  :: mat_1
    type(crs_mat_cmplx),intent(inout) :: mat_3
    integer,allocatable,dimension(:) :: counter

    integer :: k,j,n,i,index,nz

    if (mat_1%nc.ne.mat_1%nr) then
        write(*,*) '***error*** not implemented for nc.ne.nr .'
        stop

    endif

    ! call mat_3%init(nr=mat_1%nz , nc=mat_1%nc, nz=mat_1%nz, is_ccs=.true.)

    n  = mat_1%nc
    nz = mat_1%nz
    allocate(counter(n))
    counter = 0

    do i=1,mat_1%nz !Anzahl Elemente pro Spalte
        counter(mat_1%columns(i)) =counter(mat_1%columns(i)) + 1
    enddo
    mat_3%columns(1) = 1
    do j=1,n !für jede Spalte
        mat_3%columns(j+1) = mat_3%columns(j) + counter(j)
    enddo

    counter = 0
    do i=1,n !für jede Reihe
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1 !Für jedes Element in Reihe
            k                         = mat_1%columns(j)
            index                     = mat_3%columns(k)    !Index erstes Element in Spalte
            index                     = index + counter(k)  !Anzahl Elemente IN Aktueller spalte
            mat_3%values(index)       = mat_1%values(j)
            mat_3%row_index(index)    = i
            counter(k)                = counter(k) + 1
        enddo
    enddo

endsubroutine


! ---------------------------------------------------------------------------------------------------------------------
!> \brief concatenating two crs matrices [A;B]
!> \details must have same number of columns
function add_crs_row_real(mat_1,mat_2) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating two crs matrices [A;B] that have the same number of columns.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   top matrix.
    ! mat_2 : crs_mat_real
    !   bottom matrix.
    !
    ! output
    ! ------
    ! mat_3 : crs_mat_real
    !   resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_real),intent(in)  :: mat_1
    type(crs_mat_real),intent(in)  :: mat_2
    type(crs_mat_real) :: mat_3


    integer :: i

    if (.not.(mat_1%active.or.mat_2%active)) then
        return
    elseif (.not.(mat_1%active)) then
        call copy_crs(mat_2, mat_3)
        mat_3%values = mat_2%values
        return
    elseif (.not.(mat_2%active)) then
        call copy_crs(mat_1, mat_3)
        mat_3%values = mat_1%values
        return
    endif

    if (mat_1%nc.ne.mat_2%nc) then
        write(*,*) '***error*** concatenating matrices, not same number of columns'
        stop

    endif

    call mat_3%init(nr=mat_1%nr + mat_2%nr, nc=mat_1%nc, nz=mat_1%nz + mat_2%nz)

    if (mat_2%nz.eq.0) then
        do i=1,mat_1%nr
            mat_3%row_index(i)     = mat_1%row_index(i)
        enddo
        do i=1,mat_2%nr+1
            mat_3%row_index(mat_1%nr+i) = mat_1%nz+1
        enddo
        do i=1,mat_1%nz
            mat_3%columns(i) = mat_1%columns(i)
            mat_3%values(i)  = mat_1%values(i)
        enddo
        return
        
    endif

    do i=1,mat_1%nr
        mat_3%row_index(i)          = mat_1%row_index(i)
    enddo
    do i=1,mat_2%nr
        mat_3%row_index(mat_1%nr+i) = mat_1%nz+mat_2%row_index(i)
    enddo
    mat_3%row_index(mat_3%nr+1)   = mat_3%nz+1

    do i=1,mat_1%nz
        mat_3%columns(i)            = mat_1%columns(i)
        mat_3%values(i)             = mat_1%values(i)
    enddo
    do i=1,mat_2%nz
        mat_3%columns(mat_1%nz+i)   = mat_2%columns(i)
        mat_3%values(mat_1%nz+i)    = mat_2%values(i)
    enddo

endfunction

function add_crs_rows_real(mats,last_mat) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating a vector of crs matrices from top to bottom that have the same number of columns.
    !
    ! input
    ! -----
    ! mats : crs_mat_real_pointer
    !   array to pointers of  matrixes to concatenate.
    ! last_col : crs_mat_real
    !   pointer to last result of this operation. If the result is still allocated, it will be overwritten. Saves allocate time.
    !
    ! output
    ! -----
    ! mat_3 : crs_mat_real
    !   pointer to resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                      
    type(crs_mat_real_pointer),dimension(:),intent(in)  :: mats !< upper crs matrix
    type(crs_mat_real) :: mat_3 !< resulting crs matrix
    type(crs_mat_real),optional :: last_mat !< resulting crs matrix
    integer :: n_active
    type(crs_mat_real_pointer),dimension(:),allocatable :: active_mats

    integer :: i,j,nr,nc,nz,n

    n_active = 0
    do i=1,size(mats)
        if (associated(mats(i)%p)) then
            if (mats(i)%p%active) n_active = n_active + 1
        endif
    enddo

    if (n_active.eq.0) then
        return
    endif

    ! pointer to only active matrices
    ! maybe make this global
    allocate(active_mats(n_active))
    n_active = 0
    do i=1,size(mats)
        if (.not.associated(mats(i)%p)) cycle
        if (mats(i)%p%active) then
            n_active = n_active + 1
            active_mats(n_active)%p => mats(i)%p
        endif
    enddo

    ! elseif (.not.(mat_1%active)) then
    !   call copy_crs(mat_2, mat_3)
    !   mat_3%values = mat_2%values
    !   return
    ! elseif (.not.(mat_2%active)) then
    !   call copy_crs(mat_1, mat_3)
    !   mat_3%values = mat_1%values
    !   return
    ! endif
    nc=0
    do i=1,size(active_mats)
        nc = active_mats(i)%p%nc
    enddo
    do i=1,size(active_mats)
        if (active_mats(i)%p%nc.ne.nc) then
            write(*,*) '***error*** concatenating matrices, not same number of columns'
            stop
        endif
    enddo

    nr = 0
    nz = 0
    do i=1,size(active_mats)
        nr = nr + active_mats(i)%p%nr
        nz = nz + active_mats(i)%p%nz
    enddo

    if (present(last_mat)) then
        mat_3 = last_mat
        nz = 0
        do i=1,size(active_mats)
            do j=1,active_mats(i)%p%nz
                mat_3%values(j  + nz) = active_mats(i)%p%values(j)
            enddo
            nz = nz + active_mats(i)%p%nz
        enddo

    else
        call mat_3%init(nr=nr, nc=nc, nz=nz)

        ! if (mat_2%nz.eq.0) then
        !   do i=1,mat_1%nr
        !     mat_3%row_index(i)     = mat_1%row_index(i)
        !   enddo
        !   do i=1,mat_2%nr+1
        !     mat_3%row_index(mat_1%nr+i) = mat_1%nz+1
        !   enddo
        !   do i=1,mat_1%nz
        !     mat_3%columns(i) = mat_1%columns(i)
        !     mat_3%values(i)  = mat_1%values(i)
        !   enddo
        !   return
            
        ! endif

        nz = 0
        n  = 1
        do i=1,size(active_mats)
            do j=1,active_mats(i)%p%nr
                mat_3%row_index(n) = nz + active_mats(i)%p%row_index(j)
                n                  = n  + 1
            enddo
            nz = nz + active_mats(i)%p%nz
        enddo
        mat_3%row_index(mat_3%nr+1)   = nz+1

        nz = 0
        do i=1,size(active_mats)
            do j=1,active_mats(i)%p%nz
                mat_3%columns(j + nz) = active_mats(i)%p%columns(j)
                mat_3%values(j  + nz) = active_mats(i)%p%values(j)
            enddo
            nz = nz + active_mats(i)%p%nz
        enddo
    endif

endfunction


subroutine remove_zeros_crs(mat_1,mat_2,lim)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! remove entries that are zero or very close to it from mat_1 and return mat_2.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   The matrix whose zeros shall be removed.
    ! lim : real,optional
    !   Numbers with a norm of less than lim are removed. If not given, lim=0.
    !
    ! output
    ! ------
    ! mat_2 : crs_mat_real
    !   The matrix without zeros.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    class(crs_mat_real),intent(in)    :: mat_1 !< input crs matrix
    class(crs_mat_real),intent(out)   :: mat_2 !< output crs matrix
    real(8),intent(in),optional :: lim   !< limitation value

    real(8) :: limit
    integer :: i,j,n


    if (present(lim)) then
        limit=lim

    else
        limit=dble(0)

    endif

    n=0
    do i=1,mat_1%nr
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            if (abs(mat_1%values(j)).gt.limit) then
                n = n+1
            endif
        enddo
    enddo

    call mat_2%init(nr=mat_1%nr, nc=mat_1%nc, nz=n)

    n=0
    do i=1,mat_1%nr
        mat_2%row_index(i) = n+1
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            if (abs(mat_1%values(j)).gt.limit) then
                n = n+1
                mat_2%columns(n) = mat_1%columns(j)
                mat_2%values(n)  = mat_1%values(j)
            endif
        enddo
    enddo
    mat_2%row_index(mat_2%nr+1) = n+1
endsubroutine


function add_crs_col_cmplx(mat_1,mat_2) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating two crs matrices [A,B] that have the same number of rows.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   left matrix.
    ! mat_2 : crs_mat_real
    !   right matrix.
    !
    ! output
    ! ------
    ! mat_3 : crs_mat_real
    !   resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_cmplx),intent(in)  :: mat_1 !< left crs matrix complex
    type(crs_mat_cmplx),intent(in)  :: mat_2 !< right crs matrix complex
    type(crs_mat_cmplx) :: mat_3 !< resulting crs matrix complex

    integer :: n,i,j

    if (.not.(mat_1%active.or.mat_2%active)) then
        return
    elseif (.not.(mat_1%active)) then
        call copy_crs(mat_2, mat_3)
        mat_3%values = mat_2%values
        return
    elseif (.not.(mat_2%active)) then
        call copy_crs(mat_1, mat_3)
        mat_3%values = mat_1%values
        return
    endif

    if (mat_1%nr.ne.mat_2%nr) then
        write(*,*) '***error*** concatenating matrices, not same number of rows'
        stop

    endif

    call mat_3%init(nr=mat_1%nr, nc=mat_1%nc + mat_2%nc, nz=mat_1%nz + mat_2%nz, is_ccs=.False.)

    if (mat_2%nz.eq.0) then
        ! add matrix with zeros
        do i=1,mat_1%nr+1
            mat_3%row_index(i) = mat_1%row_index(i)
        enddo
        do i=1,mat_1%nz
            mat_3%columns(i)  = mat_1%columns(i)
            mat_3%values(i)   = mat_1%values(i)
        enddo
        return

    endif

    do i=1,mat_1%nr
        mat_3%row_index(i)  = mat_1%row_index(i)-1 + mat_2%row_index(i)-1 + 1
    enddo
    mat_3%row_index(mat_3%nr+1) = mat_3%nz+1

    n=0
    do i=1,mat_1%nr
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            n=n+1
            mat_3%columns(n) = mat_1%columns(j)
            mat_3%values(n)  = mat_1%values(j)
        enddo
        do j=mat_2%row_index(i),mat_2%row_index(i+1)-1
            n=n+1
            mat_3%columns(n) = mat_1%nc + mat_2%columns(j)
            mat_3%values(n)  = mat_2%values(j)
        enddo
    enddo
endfunction


function add_crs_row_cmplx(mat_1,mat_2) result(mat_3)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Concatenating two crs matrices [A;B] that have the same number of columns.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   top matrix.
    ! mat_2 : crs_mat_real
    !   bottom matrix.
    !
    ! output
    ! ------
    ! mat_3 : crs_mat_real
    !   resulting matrix.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_cmplx),intent(in)  :: mat_1 !< upper crs matrix
    type(crs_mat_cmplx),intent(in)  :: mat_2 !< lower crs matrix
    type(crs_mat_cmplx) :: mat_3 !< resulting crs matrix

    integer :: i

    if (.not.(mat_1%active.or.mat_2%active)) then
        return
    elseif (.not.(mat_1%active)) then
        call copy_crs(mat_2, mat_3)
        mat_3%values = mat_2%values
        return
    elseif (.not.(mat_2%active)) then
        call copy_crs(mat_1, mat_3)
        mat_3%values = mat_1%values
        return
    endif

    if (mat_1%nc.ne.mat_2%nc) then
        write(*,*) '***error*** concatenating matrices, not same number of columns'
        stop

    endif

    call mat_3%init(nr=mat_1%nr + mat_2%nr, nc=mat_1%nc, nz=mat_1%nz + mat_2%nz)

    if (mat_2%nz.eq.0) then
        do i=1,mat_1%nr
            mat_3%row_index(i)     = mat_1%row_index(i)
        enddo
        do i=1,mat_2%nr+1
            mat_3%row_index(mat_1%nr+i) = mat_1%nz+1
        enddo
        do i=1,mat_1%nz
            mat_3%columns(i) = mat_1%columns(i)
            mat_3%values(i)  = mat_1%values(i)
        enddo
        return
        
    endif


    do i=1,mat_1%nr
        mat_3%row_index(i)     = mat_1%row_index(i)
    enddo
    do i=1,mat_2%nr
        mat_3%row_index(mat_1%nr+i) = mat_1%nz+mat_2%row_index(i)
    enddo
    mat_3%row_index(mat_3%nr+1)   = mat_3%nz+1

    do i=1,mat_1%nz
        mat_3%columns(i)      = mat_1%columns(i)
        mat_3%values(i)       = mat_1%values(i)
    enddo
    do i=1,mat_2%nz
        mat_3%columns(mat_1%nz+i)  = mat_2%columns(i)
        mat_3%values(mat_1%nz+i)   = mat_2%values(i)
    enddo

endfunction

subroutine remove_zeros_crs_cmplx(mat_1,mat_2)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! remove entries that are zero or very close to it from mat_1 and return mat_2.
    !
    ! input
    ! -----
    ! mat_1 : crs_mat_real
    !   The matrix whose zeros shall be removed.
    ! lim : real,optional (TODO: ONLY IN REAL VERSION OF THIS FUNCTION)
    !   Numbers with a norm of less than lim are removed. If not given, lim=0.
    !
    ! output
    ! ------
    ! mat_2 : crs_mat_real
    !   The matrix without zeros.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_cmplx),intent(in)    :: mat_1 !< input crs matrix
    type(crs_mat_cmplx),intent(out)   :: mat_2 !< output crs matrix

    real(8) :: dummy
    integer :: i,j,n

    n = 0
    do i=1,mat_1%nr
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            if ((abs(real(mat_1%values(j))).gt.(1e0*epsilon(dummy))).or.(abs(imag(mat_1%values(j))).gt.(1e0*epsilon(dummy))))  then
                n = n+1
            endif
        enddo
    enddo
    call mat_2%init(nr=mat_1%nr, nc=mat_1%nc, nz=n)

    n=0
    do i=1,mat_1%nr
        mat_2%row_index(i) = n+1
        do j=mat_1%row_index(i),mat_1%row_index(i+1)-1
            if ((abs(real(mat_1%values(j))).gt.(1e0*epsilon(dummy))).or.(abs(imag(mat_1%values(j))).gt.(1e0*epsilon(dummy))))  then
                n = n+1
                mat_2%columns(n) = mat_1%columns(j)
                mat_2%values(n)  = mat_1%values(j)
            endif
        enddo
    enddo
    mat_2%row_index(mat_2%nr+1) = n+1

endsubroutine

! subroutine init_dss(mat,handle)
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!
!     ! build real sparse matrix.
!     !
!     ! input
!     ! -----
!     ! mat : crs_mat
!     !  crs matrix real
!     ! handle : MKL_DSS_HANDLE
!     !  dss matrix handle
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!
!     class(crs_mat),intent(inout)     :: mat
!     type(MKL_DSS_HANDLE),intent(out) :: handle 

!     integer,dimension(1) :: perm
!     integer              :: error = 0

!     ! --> build SPARSE matrix ----------------------
!     error   = DSS_CREATE( handle, MKL_DSS_DEFAULTS )
!     if (error.gt.0) then
!         write(*,*) '***error*** DSS_CREATE'
!         stop
!     endif

!     error   = DSS_DEFINE_STRUCTURE( handle, MKL_DSS_NON_SYMMETRIC, mat%row_index, mat%nr, &
!                                     & mat%nc, mat%columns, mat%nz )
!     if (error.gt.0) then
!         write(*,*) '***error*** DSS_DEFINE_STRUCTURE'
!         stop
!     endif

!     perm(1) = 0
!     error   = DSS_REORDER( handle, MKL_DSS_AUTO_ORDER, perm )
!     if (error.gt.0) then
!         write(*,*) '***error*** DSS_REORDER, error'
!         stop
!     endif

! endsubroutine

! subroutine set_values_dss(mat,handle)
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     ! insert values to sparse matrix
!     !
!     ! input
!     ! -----
!     ! mat : crs_mat_real
!     !   the matrix whose values shall be set.
!     ! handle : MKL_DSS_HANDLE
!     !   dss matrix handle.
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!     class(crs_mat_real),intent(inout)      :: mat    !< crs matrix real
!     type(MKL_DSS_HANDLE),intent(out) :: handle !< dss matrix handle

!     integer              :: error

!     error = DSS_FACTOR_REAL_D( handle, MKL_DSS_DEFAULTS, mat%values )
!     if (error.gt.0) then
!         write(*,*) '***error*** DSS_FACTOR_REAL'
!         stop
!     endif

! endsubroutine

subroutine check(mat)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! check structure of real crs matrix
    !
    ! mat : crs_mat
    !   the matrix that shall be checkd.
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class(crs_mat),intent(in ) :: mat !< crs matrix real

    integer :: i,j

    do i=1,mat%nr
        do j=mat%row_index(i),mat%row_index(i+1)-2
            if (mat%columns(j).gt.mat%columns(j+1)) then
                write(*,*) 'crs problem'
                stop
            endif
        enddo
    enddo
endsubroutine

function join_mats_real(mat) result(out)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Given a matrix of crs_matrixes, e.g. each element is a pointer to a crs matrix.
    ! Join all these sub-matrices to one big sparse matrix.
    !
    ! input
    ! -----
    ! mat : crs_mat_real_poiter(:,:)
    !   2d array of pointers to crs matrices.
    !
    ! output
    ! ------
    ! out : crs_mat_real
    !   The result of the join operation.
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_real_pointer),dimension(:,:),intent(in) :: mat !< crs matrix real
    type(crs_mat_real) :: out

    integer :: n,i

    !check if shape of mat ok
    if (size(mat,1).NE.size(mat,2)) then
        write(*,*) '***error*** matrix needs to be rectangular for joining.' 
        stop
    endif
    n = size(mat,1)
    !check if number of active mats ok

    if (.not.associated(last_rows_real)) then
        allocate(last_rows_real(n))
    endif

    !memory leak here?: row gets allocated each loop newly, old reference does not get dealloced
    do i=1,n
        if (associated(last_rows_real(i)%p)) then
            last_rows_real(i) = add_crs_cols_real(mat(:,i), last_rows_real(i))
        else
            last_rows_real(i) = add_crs_cols_real(mat(:,i))
        endif
    enddo

    if (.not.last_out_real%active) then
        last_out_real = add_crs_rows_real(last_rows_real(:))
    else
        last_out_real = add_crs_rows_real(last_rows_real(:),last_out_real)
    endif

    out = last_out_real

endfunction

function join_mats_cmplx(mat) result(out)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Given a matrix of crs_matrixes, e.g. each element is a pointer to a crs matrix.
    ! Join all these sub-matrices to one big sparse matrix.
    !
    ! input
    ! -----
    ! mat : crs_mat_real_poiter(:,:)
    !   2d array of pointers to crs matrices.
    !
    ! output
    ! ------
    ! out : crs_mat_real
    !   The result of the join operation.
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(crs_mat_cmplx),dimension(:,:),intent(in) :: mat !< crs matrix real
    type(crs_mat_cmplx) :: row
    type(crs_mat_cmplx) :: out

    integer :: n,i,j
    logical :: active

    if (size(mat,1).NE.size(mat,2)) then
        write(*,*) '***error*** matrix needs to be rectangular for joining.' 
        stop
    endif

    n = size(mat,1)

    do i=1,n
        do j=1,n
            active = mat(j,i)%active
            if (active) row = add_crs_col_cmplx(row, mat(j,i))
        enddo
        out = add_crs_row_cmplx(out,row)
        call dealloc_crs(row)
    enddo

endfunction

endmodule
