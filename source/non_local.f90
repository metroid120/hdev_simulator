!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



! Non local prefactors for energy fields in drift diffusion simulation
module non_local

use semiconductor_globals, only: NL_FACTOR, NL_ACTIVATED
use semiconductor, only: SP
use read_user            , only : US
use semiconductor_globals, only : N_SP                  !< number of semiconductor points
use structure            , only : X,Y
use structure            , only : STRUC_DIM
use math_oper            , only :integrate
USE Dual_Num_Auto_Diff

implicit none



private

public init_non_local
public get_nl_field

contains

subroutine init_non_local
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init the non local prefactor (only dependent on 1D structure)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8) :: xj_l, xj_r,ymax, lambda, integral, vsn, taue, fac, delta_x, nl_base, id_x
    integer :: n,index_r,index_l

    if (US%nonlocal%type(1:4).eq.'none') then 
        write(*,*) 'non-local factors not activated'
        return
    endif

    ! if (STRUC_DIM.gt.1) then
    !     write(*,*) '***error*** non-local factors only allowed in 1D simulation. Stop.'
    !     stop
    ! endif
    
    lambda  = US%nonlocal%lambda
    ymax    = US%nonlocal%ymax 
    xj_l    = US%nonlocal%xj_l
    xj_r    = US%nonlocal%xj_r
    nl_base = US%nonlocal%nl_base
    index_l = minloc(abs(X(:)-xj_l),1)
    index_r = minloc(abs(X(:)-xj_r),1)

    if (US%nonlocal%type(1:5).eq.'npnnp') then 
        write(*,*) 'non-local factors for n+-n-n+ structure activated'
        allocate(NL_FACTOR(N_SP))
        NL_FACTOR = 0
        do n=1,N_SP
            id_x = SP(n)%id_x
            if ((id_x.gt.index_l).and.(id_x.lt.index_r)) then !maybe keep energy after index_r
                ! integral     = integrate( X(index_l:n), exp(( X(index_l:n) -X(n) )/lambda )  )
                integral     = lambda-lambda*exp((X(index_l)-X(id_x))/lambda)
                NL_FACTOR(n) = integral/lambda
            elseif (id_x.ge.index_r) then
                ! integral     = integrate( X(index_l:index_r), exp(( X(index_l:index_r) -X(n) )/lambda )  )
                integral     = lambda*exp(-X(id_x)/lambda)*(exp(X(index_r)/lambda)-exp(X(index_l)/lambda))
                NL_FACTOR(n) = integral/lambda
            else
                cycle
            endif
        enddo
        NL_ACTIVATED = .true.

    elseif (US%nonlocal%type(1:7).eq.'iiivhbt') then 
        write(*,*) 'non-local factors for III-V HBT structure activated'
        allocate(NL_FACTOR(N_SP))
        NL_FACTOR = 0
        do n=1,N_SP
            id_x = SP(n)%id_x
            if (id_x.lt.index_l) then !to the left of BE junction assume carriers are thermalized
                NL_FACTOR(n) = dble(1)
                NL_FACTOR(n) = dble(0)
                ! NL_FACTOR(n) = nl_base
            elseif (id_x.lt.index_r) then !in the base, set non_local factor to user defined input
                NL_FACTOR(n) = nl_base
            elseif (id_x.ge.index_r) then ! in the collector, let the field rise with lambda from base_factor to 1
                delta_x      =  (X(index_r)-X(id_x))
                NL_FACTOR(n) = nl_base + (dble(1)-exp(delta_x/lambda))*(dble(1)-nl_base)
                ! NL_FACTOR(n) = dble(1)-exp(delta_x/lambda)
            else
                cycle
            endif
        enddo
        NL_ACTIVATED = .true.

    elseif (US%nonlocal%type(1:6).eq.'simple') then 
        write(*,*) 'non-local factors for simple structure activated'
        allocate(NL_FACTOR(N_SP))
        NL_FACTOR = 0

        !for SiGe some approximate values
        vsn          = 0.8e5
        taue         = 0.23e-12
        fac          = dble(3)/dble(5)/vsn/taue
        fac          = fac*2
        fac          = 1/lambda

        do n=1,N_SP
            id_x = SP(n)%id_x
            if (id_x.gt.index_l) then !maybe keep energy after index_r
                ! integral   = integrate( X(index_l:n), exp(( X(index_l:n) -X(n) )/lambda )  )
                delta_x      =  (X(index_l)-X(id_x))
                integral     = lambda-lambda*exp(delta_x/lambda)
                NL_FACTOR(n) = fac*integral
            else
                NL_FACTOR(n) = dble(0)
            endif
        enddo
        NL_ACTIVATED = .true.

    else
        write(*,*) '***error*** non-local type not recognized. Stop.'
        stop

    endif

endsubroutine

TYPE(DUAL_NUM) function get_nl_field(local_field, n) result(nl_field)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! given the local field, return the non-local one using the position dependent prefactor.
    !
    ! input
    ! -----
    ! local_field : DUAL_NUM
    !   The local field used for energy estimation.
    ! n : integer
    !   The index of the poin in the semiconductor.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer        :: n,id_y           ! index to semiconductor
    TYPE(DUAL_NUM) :: local_field !local field
    real(8)        :: fmax, beta

    if (.not.NL_ACTIVATED) then
        nl_field = local_field
    elseif (Y(SP(n)%id_y).gt.US%nonlocal%ymax) then
        nl_field = local_field
    else
        nl_field = NL_FACTOR(n)*local_field !factor from integral, see HE Script from Schroeter
        if (US%nonlocal%fmax.gt.0) then !clipping of field (empirical extension)
            fmax     = US%nonlocal%fmax
            beta     = US%nonlocal%beta
            nl_field = nl_field/(dble(1)+(nl_field/US%nonlocal%fmax)**(US%nonlocal%beta))**(dble(1)/US%nonlocal%beta) !smooth transistion to fmax
        endif
    endif
endfunction

endmodule
