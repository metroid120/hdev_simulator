!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! Poisson module
module poisson

use m_strings
use utils, only : string, toUpper
use save_global
use phys_const                                    !< Physical constants
use save_global  , only : get_col_id             !< get column index of specific column name
use profile, only : DONATORS                     !< (N_SP) semiconductor doping density
use profile, only : ACCEPTORS                     !< (N_SP) semiconductor doping densit
use semiconductor, only : CN                     !< (N_SP) semiconductor electron density
use semiconductor, only : CN2                    !< (N_SP) semiconductor electron density
use semiconductor, only : CP                     !< (N_SP) semiconductor hole density
use structure    , only : X,Y,Z                      !< (NY1) y-grid
use structure    , only : NX,NY,NZ                      !< (NY1) y-grid
use structure    , only : NX1,NY1,NZ1                      !< (NY1) y-grid
use structure    , only : NXYZ                     !< (NY1) y-grid
use structure    , only : NY1                     !< (NY1) y-grid
use structure    , only : NY ,NX1                    !< (NY1) y-grid
use structure    , only : DX,DY,DZ                     !< (NY1) y-grid
use continuity_poi,  only: CONT_POI
use continuity, only : DN,DN2,DP
use contact      , only : OP_FIRST               !< if true: first operation point
use semiconductor_globals, only : N_SP                   !< number of semiconductor points
use structure    , only : STRUC_DIM              !< dimension for poisson equation
use structure    , only : CART                   !< if true: cartesian coordinate system
use structure    , only : POINT                   !< if true: cartesian coordinate system
use semiconductor, only : SEMI_EXIST             !< if true: semiconductor exists
use semiconductor, only : SP                     !< (N_SP) properties of semiconductor
use semiconductor, only : PSI_BUILDIN                    !< number of sx-boxes
use semiconductor, only : PSI_SEMI               !< (N_SP) electrostatic potential at CNT
use read_user    , only : US                     !< all parameters from user input file
use save_global  , only : read_colnames          !< read column names from elpa file
use save_global  , only : write_elpa_value       !< write value in elpa table
use contact      , only : N_CON                  !< number of contacts
use contact      , only : TR_IT                  !< actual time step
use contact      , only : TR_OP                  !< transient operation points
use contact      , only : CON                  !< transient operation points
use contact      , only : TAMB_ID                  !< transient operation points
use dd_types, only  : NORM
use hdf_functions
! ---> variables and functions from other moduls ----------------------------------------------------------------------

! use mkl_dss                                      !< MKL solver


! use crs_matrix   , only : crs_mat_real           !< type for crs matrix
! use crs_matrix   , only : set_values_dss         !< initialize dss solver
! !use crs_matrix   , only : check_crs              !< check column entries

  
! use save_global  , only : DEB_ID                 !< debug file id
! use save_global  , only : OP_REC                 !< operation point record number
! use save_global  , only : FILE                   !< output file name
! use save_global  , only : VERSION                !< hdev version
! use save_global  , only : FORMAT_REAL            !< output format for real numbers
! use save_global  , only : get_file_id            !< get new file id
! use save_global  , only : set_filename           !< set file name
! use save_global  , only : get_col_id             !< get column index of specific column name
! use save_global  , only : write_colnames         !< write column names for elpa file
! use save_global  , only : write_parameter_int    !< write integer parameter in first elpa line
! use save_global  , only : write_parameter_char   !< write character parameter in first elpa line

! use cnt_prop     , only : CNT                    !< cnt properties

! use bandstructure, only : SEMI                   !< semiconductor properties
! use bandstructure, only : get_fermi_equ          !< get fermi level for given carrier density

! use structure    , only : BOX                    !< properties of boxes in device structure
! use structure    , only : N_BOX                  !< total number of boxes
! use structure    , only : POINT                  !< (NX1,NY1,NZ1) parameter for each disc. point
! use structure    , only : BULK                   !< if true: bulk simulation
! use structure    , only : X                      !< (NX1) x-grid
! use structure    , only : DX                     !< (NX) dx-grid
! use structure    , only : NX                     !< number of x-boxes
! use structure    , only : NX1                    !< number of x-points
! use structure    , only : DY                     !< (NY) dy-grid
! use structure    , only : NY                     !< number of y-boxes
! use structure    , only : NY1                    !< number of y-points
! use structure    , only : Z                      !< (NZ1) z-grid
! use structure    , only : DZ                     !< (NZ) dz-grid
! use structure    , only : NZ                     !< number of z-boxes
! use structure    , only : NZ1                    !< number of z-points
! use structure    , only : NXYZ                   !< total number of grid points
! use structure    , only : X_PERIODIC             !< if true: periodic BC in x-direction
! use structure    , only : Y_PERIODIC             !< if true: periodic BC in y-direction
! use structure    , only : Z_PERIODIC             !< if true: periodic BC in z-direction

! use semiconductor_globals, only : HETERO                   !< number of semiconductor points
! use semiconductor, only : SEMI_DIM               !< semiconductor real space dimension
! use semiconductor, only : CHARGE_DIM             !< maximum dimension of space charge
! use semiconductor, only : MET                    !< if true: is metallic CNT
! use semiconductor, only : SNX                    !< number of sx-boxes
! use semiconductor, only : SNX1                   !< number of sx-points
! use semiconductor, only : SX                     !< (SNX1) x-grid semiconductor in x-direction
! use semiconductor, only : SDX                    !< (SNX) dx-grid semiconductor in x-direction
! use semiconductor, only : FX                     !< (N_SP) electric field in x-direction
! use semiconductor_globals, only : TEMP_SEMI              !< (N_SP) semiconductor temperature

! use profile, only : CD                     !< (N_SP) semiconductor doping density
! use profile, only : DONATORS                     !< (N_SP) semiconductor doping density
! use profile, only : ACCEPTORS                     !< (N_SP) semiconductor doping density
! use semiconductor, only : CN_TRAP                !< (N_SP,CHARGE_DIM) semiconductor electron trap density
! use semiconductor, only : CP_TRAP                !< (N_SP,CHARGE_DIM) semiconductor hole trap density
! use semiconductor, only : calc_fx                !< calculate electric field in x direction

! use contact      , only : OP                     !< actual operation point
! use contact      , only : FX_RAND                !< if true: random bulk electron field
! use contact      , only : FX_ABS                 !< max. random field
! use contact      , only : CON                    !< contact properties
! use contact      , only : SOURCE                 !< id to source contact
! use contact      , only : DRAIN                  !< id to drain contact
! use contact      , only : GATE                   !< id to gate contact
! use contact      , only : TR_ON                  !< if true: transient simulation activated
! use contact      , only : write_op_start                     !< hdev output for new operation point
! use contact      , only : get_bias                           !< get bias value for contact

implicit none


public set_psi_initial
public set_psi_semi
public calc_diff_charge_cont
public get_charge
public save_capacity

private

contains
 

! ! --> calculate extern capacities
! if ((US%output%cap_lev.ge.1).and.PCALC) then
!   call save_capacity

! endif

! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief Poisson equation in 1-dimension
! !> \details
! subroutine init_poisson1D

! integer :: i,n,v,ii,id
! real(8) :: x_p,x_m

! ! --> count nonzero matrix entries
! n=0
! do i=1,NX1
!   if (POINT(i,1,1)%diri) then
!     n = n + 1

!   elseif (X_PERIODIC.and.(i.eq.1)) then
!     n = n + 2 ! periodic boundary in x direction

!   else  
!     n = n + 3
!     if (i.eq.1)   n = n - 1
!     if ((.not.X_PERIODIC).and.(i.eq.NX1)) n = n - 1 

!   endif
! enddo

! ! --> set Poisson Matrix ----------------
! call POI%init(nr=NXYZ, nc=NXYZ, nz=n)

! n = 0
! v = 0
! do i=1,NX1
!   n  = n+1
!   ! --> CONTACT -------------------------
!   if (POINT(i,1,1)%diri) then
!     v                = v+1
!     POI%values(v)    = 1
!     POI%columns(v)   = n
!     POI%row_index(n) = v
!     ! --> main diagonal CNT
!     if (POINT(i,1,1)%semi) then
!       id                    = POINT(i,1,1)%sp_idx
!       SP(id)%id_poisson_psi = v
!     endif
    
!   elseif (X_PERIODIC.and.(i.eq.1)) then
!     ! periodic boundary condition at zmin
!     POI%row_index(n) = v+1

!     v                = v+1
!     POI%values(v)    = 1
!     POI%columns(v)   = n
!     v                = v+1
!     POI%values(v)    = -1
!     POI%columns(v)   = n + (NX1-1)
  
!   else
   
!     ii=i
!     if (X_PERIODIC.and.(i.eq.NX1)) ii=1

!     ! --> x+
!     x_p   = dble(0)
!     if (i.lt.NX1) then
!       x_p = POINT(i,1,1)%eps/DX(i)

!     elseif (ii.lt.NX1) then
!       x_p = POINT(ii,1,1)%eps/DX(ii)

!     endif

!     ! --> x-
!     x_m   = dble(0)
!     if (i.gt.1) then
!       x_m = POINT(i-1,1,1)%eps/DX(i-1)

!     endif

!     ! ----------------> FILL MATRIX -------------------------
!     POI%row_index(n) = v+1

!     ! -->  x-periodic phi_i+1,j
!     if (X_PERIODIC.and.(i.eq.NX1)) then
!       v              = v+1
!       POI%values(v)  = x_p
!       POI%columns(v) = n - (NX1-2)
!     endif

!     ! --> phi_i-1
!     if (i.gt.1) then
!       v              = v+1
!       POI%values(v)  = x_m
!       POI%columns(v) = n-1
!     endif

!     ! --> phi_i
!     v                = v+1
!     POI%values(v)    = -x_m-x_p
!     POI%columns(v)   = n

!     ! --> main diagonal CNT
!     if (POINT(i,1,1)%semi.or.POINT(i,1,1)%supply) then
!       id                    = POINT(i,1,1)%sp_idx
!       SP(id)%id_poisson_psi = v
!     endif

!     ! --> phi_i+1
!     if (i.lt.NX1) then
!       v              = v+1
!       POI%values(v)  = x_p
!       POI%columns(v) = n+1
!     endif

!     ! ----------------------------POI_RHS ------------------------------------
!     ! [POI_RHS] = V/m
!     ! --> CNT region ----
!     !beware, this is different in my DA, will only work if hole simulation region is semiconductor
!     !if (POINT(i,1,1)%semi) then ! todo: no only cnt
!     if (.not.POINT(i,1,1)%diri) then ! todo: no only cnt
!       id = POINT(i,1,1)%sp_idx
!       ! line charge at CNT
!       if (CHARGE_DIM.ge.1) then
!         if (i.gt.1) then
!           if (CNT(SP(id)%model_id)%ar.ne.0) then
!             POI_RHS_SEMI(id,1) =                    - DX(i-1)*dble(0.5)/EP0*Q/CNT(SP(id)%model_id)%ar !*densitiy  in 1/m
!           else
!             POI_RHS_SEMI(id,1) =                    - 0 !todo
!           endif
!         endif
!         if (i.lt.NX1) then
!           if (CNT(SP(id)%model_id)%ar.ne.0) then
!             POI_RHS_SEMI(id,1) = POI_RHS_SEMI(id,1) - DX(i  )*dble(0.5)/EP0*Q/CNT(SP(id)%model_id)%ar
!           else
!             POI_RHS_SEMI(id,1) =                    - 0 !todo
!           endif        
!         endif
!       endif
!       !todo: 2D charge
!       if (CHARGE_DIM.ge.3) then !Charge with 1/m^2 TODO
!         ! ---> west box
!         if (i.gt.1) then
!           POI_RHS_SEMI(id,3) =                    - DX(i-1)*dble(0.5)/EP0*Q
!         endif
!         ! ---> west box
!         if (i.lt.NX1) then
!           POI_RHS_SEMI(id,3) = POI_RHS_SEMI(id,3) - DX(i  )*dble(0.5)/EP0*Q
!         endif
!       endif
!     else
!       POI_RHS(n) = POI_RHS(n) - CHARGE(n)/EP0*Q ! [CHARGE] = 1/m^2
      
!     endif
!   endif
! enddo
! POI%row_index(POI%nr+1) = POI%nz+1

! endsubroutine

 
! ! ---------------------------------------------------------------------------------------------------------------------
! !> \brief Poisson equation in 2-dimensions, cartesian
! !> \details
! subroutine init_poisson2Dcart

! integer :: i,j,n,v,ii,jj,id
! real(8) :: x_pp,x_pm,x_mp,x_mm,y_pp,y_pm,y_mp,y_mm


! ! --> count nonzero matrix entries
! n=0
! do j=1,NY1
!   do i=1,NX1
!     if (POINT(i,j,1)%diri) then
!       n = n + 1

!     elseif (X_PERIODIC.and.(i.eq.1)) then
!       n = n + 2 ! periodic boundary in x direction

!     elseif (Y_PERIODIC.and.(j.eq.1)) then
!       n = n + 2 ! periodic boundary in y direction

!     else
!       n = n + 5
!       if (i.eq.1)   n = n - 1 
!       if ((.not.X_PERIODIC).and.(i.eq.NX1)) n = n - 1 
!       if (j.eq.1)   n = n - 1 
!       if ((.not.Y_PERIODIC).and.(j.eq.NY1)) n = n - 1

!     endif
!   enddo
! enddo

! ! --> set Poisson Matrix
! call POI%init(nr=NXYZ, nc=NXYZ, nz=n)

! n = 0
! v = 0
! do j=1,NY1
!   do i=1,NX1
!     n = n+1
!     ! -----> CONTACT -------------------------
!     if (POINT(i,j,1)%diri) then
!       v                    = v+1
!       POI%values(v)    = 1
!       POI%columns(v)   = n
!       POI%row_index(n) = v

!       ! --> main diagonal CNT
!       if (POINT(i,j,1)%semi) then
!         id                    = POINT(i,j,1)%sp_idx
!         SP(id)%id_poisson_psi = v

!       endif
    
!     elseif (X_PERIODIC.and.(i.eq.1)) then
!       ! periodic boundary condition at zmin
!       POI%row_index(n) = v+1

!       v                    = v+1
!       POI%values(v)    = 1
!       POI%columns(v)   = n
!       v                    = v+1
!       POI%values(v)    = -1
!       POI%columns(v)   = n + (NX1-1)

!     elseif (Y_PERIODIC.and.(j.eq.1)) then
!       ! periodic boundary condition at zmin
!       POI%row_index(n) = v+1

!       v                    = v+1
!       POI%values(v)    = 1
!       POI%columns(v)   = n
!       v                    = v+1
!       POI%values(v)    = -1
!       POI%columns(v)   = n+(NY1-1)*NX1

!     ! -------> OXID/SEMI  -------------------------
!     else
!       jj=j
!       ii=i
!       if (Y_PERIODIC.and.(j.eq.NY1)) jj=1
!       if (X_PERIODIC.and.(i.eq.NX1)) ii=1

!       ! ---> north-east box (x+,y+) ---
!       x_pp = 0
!       y_pp = 0
!       if ((ii.lt.NX1).and.(jj.lt.NY1)) then
!         x_pp = POINT(ii,jj,1)%eps*DY(jj)/DX(ii)*0.5
!         y_pp = POINT(ii,jj,1)%eps*DX(ii)/DY(jj)*0.5

!       endif

!       ! ---> north-west box (x-,y+) ----
!       x_mp = 0
!       y_mp = 0
!       if ((i.gt.1).and.(jj.lt.NY1)) then
!         x_mp = POINT(i-1,jj,1)%eps*DY(jj )/DX(i-1)*0.5
!         y_mp = POINT(i-1,jj,1)%eps*DX(i-1)/DY(jj )*0.5

!       endif

!       ! ---> south-east box (x+,y-) ----
!       x_pm = 0
!       y_pm = 0
!       if ((ii.lt.NX1).and.(j.gt.1)) then
!         x_pm = POINT(ii,j-1,1)%eps*DY(j-1)/DX(ii )*0.5
!         y_pm = POINT(ii,j-1,1)%eps*DX(ii )/DY(j-1)*0.5

!       endif

!       ! ---> south-west box (x-,y-) ----
!       x_mm = 0
!       y_mm = 0
!       if ((i.gt.1).and.(j.gt.1)) then
!         x_mm = POINT(i-1,j-1,1)%eps*DY(j-1)/DX(i-1)*0.5
!         y_mm = POINT(i-1,j-1,1)%eps*DX(i-1)/DY(j-1)*0.5

!       endif
      
!       ! ----------------> FILL MATRIX -------------------------
!       POI%row_index(n) = v+1

!       ! -->  y-periodic phi_i,j+1
!       if (Y_PERIODIC.and.(j.eq.NY1)) then
!         v              = v+1
!         POI%values(v)  = y_pp + y_mp
!         POI%columns(v) = n - (NY1-2)*NX1

!       endif

!       ! --> phi_i,j-1
!       if (j.gt.1) then
!         v              = v+1
!         POI%values(v)  = y_pm + y_mm
!         POI%columns(v) = n-NX1

!       endif

!       ! -->  x-periodic phi_i+1,j
!       if (X_PERIODIC.and.(i.eq.NX1)) then
!         v              = v+1
!         POI%values(v)  = x_pp + x_pm
!         POI%columns(v) = n - (NX1-2)

!       endif

!       ! --> phi_i-1,j
!       if (i.gt.1) then
!         v              = v+1
!         POI%values(v)  = x_mp + x_mm
!         POI%columns(v) = n-1

!       endif

!       ! --> phi_i,j
!       v                = v+1
!       POI%values(v)    = -x_pp - x_pm - x_mp - x_mm - y_pp - y_pm - y_mp - y_mm
!       POI%columns(v)   = n

!       ! --> main diagonal CNT
!       if (POINT(i,j,1)%semi) then
!         id                    = POINT(i,j,1)%sp_idx
!         SP(id)%id_poisson_psi = v

!       endif

!       ! --> phi_i+1,j
!       if (i.lt.NX1) then
!         v              = v+1
!         POI%values(v)  = x_pp + x_pm
!         POI%columns(v) = n+1

!       endif

!       ! --> phi_i,j+1
!       if (j.lt.NY1) then
!         v              = v+1
!         POI%values(v)  = y_pp + y_mp
!         POI%columns(v) = n+NX1
!       endif
      
!       ! ----------------------------POI_RHS ------------------------------------
!       ! [POI_RHS] = V
!       if (POINT(i,j,1)%semi) then
!         id = POINT(i,j,1)%sp_idx
!         ! line charge at CNT
!         if (i.gt.1) then
!           POI_RHS_SEMI(id,1) =                  - DX(i-1)*0.5/EP0*Q/CNT(SP(id)%model_id)%dcnt   ! [CHARGE] = 1/m
!         endif
!         if (i.lt.NX1) then
!           POI_RHS_SEMI(id,1) = POI_RHS_SEMI(id,1) - DX(i  )*0.5/EP0*Q/CNT(SP(id)%model_id)%dcnt   ! /DCNT --> densely packed CNT array
!         endif

!         if (CHARGE_DIM.ge.3) then
!           ! ---> north-east box (x+,y+) ---
!           if ((i.lt.NX1).and.(j.lt.NY1)) then
!             if (SP(id)%x_upp_exist.and.SP(id)%y_upp_exist) then
!               POI_RHS_SEMI(id,3) = POI_RHS_SEMI(id,3)-DX(i  )*DY(j  )*0.25/EP0*Q ! [CHARGE] = 1/m^3

!             endif
!           endif
!           ! ---> north-west box (x-,y+) ----
!           if ((i.gt.1).and.(j.lt.NY1)) then
!             if (SP(id)%x_low_exist.and.SP(id)%y_upp_exist) then
!               POI_RHS_SEMI(id,3) = POI_RHS_SEMI(id,3)-DX(i-1)*DY(j  )*0.25/EP0*Q

!             endif
!           endif
!           ! ---> south-east box (x+,y-) ----
!           if ((i.lt.NX1).and.(j.gt.1)) then
!             if (SP(id)%x_upp_exist.and.SP(id)%y_low_exist) then
!               POI_RHS_SEMI(id,3) = POI_RHS_SEMI(id,3)-DX(i  )*DY(j-1)*0.25/EP0*Q

!             endif
!           endif
!           ! ---> south-west box (x-,y-) ----
!           if ((i.gt.1).and.(j.gt.1)) then
!             if (SP(id)%x_low_exist.and.SP(id)%y_low_exist) then
!               POI_RHS_SEMI(id,3) = POI_RHS_SEMI(id,3)-DX(i-1)*DY(j-1)*0.25/EP0*Q

!             endif
!           endif
!         endif

!         if (CHARGE_DIM.ge.2) then
!           ! ---> east box (x+) ---
!           if (j.lt.NY1) then
!             if (SP(id)%x_upp_exist) then
!               POI_RHS_SEMI(id,2) = POI_RHS_SEMI(id,2)-DX(i  )*0.5/EP0*Q ! [CHARGE] = 1/m^2

!             endif
!           endif
!           ! ---> west box (x-) ----
!           if (i.gt.1) then
!             if (SP(id)%x_low_exist) then
!               POI_RHS_SEMI(id,2) = POI_RHS_SEMI(id,2)-DX(i-1)*0.5/EP0*Q

!             endif
!           endif
!         endif

!       else
!         ! --> fixed charge in OXIDE region ----
!         POI_RHS(n) = POI_RHS(n)-CHARGE(n)/EP0*Q ! [CHARGE] = 1/m

!       endif

!     endif
!   enddo
! enddo
! POI%row_index(POI%nr+1) = POI%nz+1

! endsubroutine


function calc_capacity(con_fix) result(q_con)
    ! get capacitance for all contacts with respect to con_fix
    integer, intent(in) :: con_fix !< contact id that is at 1V
    integer :: id,j,i,n,k,l
    real(8),dimension(N_CON) :: vcon_all
    real(8),dimension(N_CON)                  :: q_con
    real(8),dimension(6)     :: sf_charge ! x,y,z side

    type(string)     :: poi_cols(5)       !columns of inqu file
    real(8),dimension(:,:),allocatable :: poi_vals            ! values of inqu file
    
    ! set jacobian
    CONT_POI%var = 0 !psi equal zero everywhere
    ! no charge, no derivative
    ! DN(:)%dens_dqf = 0
    ! DP(:)%dens_dqf = 0
    ! DN2(:)%dens_dqf = 0
    call CONT_POI%set(.false.)

    CONT_POI%rhs = 0 ! no charges

    ! --> set rhs for contact region
    vcon_all = 0
    vcon_all(con_fix) = dble(1)
    n = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                n=n+1
                if (POINT(i,j,k)%cont) then
                    CONT_POI%rhs(n) = vcon_all(POINT(i,j,k)%cont_id)
                endif
            enddo
        enddo
    enddo   

    call CONT_POI%solve(dble(1),.false.)

    !write output
    if (.not.allocated(poi_vals)) allocate(poi_vals(5,NXYZ))
    n = 0
    do k=1,NY1
        do l=1,NX1
            n     = n + 1 !index to point in structure
            i     = 0 !column index


            ! first all values that are defined for every point
            if (STRUC_DIM.ge.1) then
                i                = i +1
                poi_cols(i)%str = 'X'
                poi_vals(i,n)   = X(l)*NORM%l_N
            endif
            if (STRUC_DIM.ge.2) then
                i                = i +1
                poi_cols(i)%str = 'Y'
                poi_vals(i,n)   = Y(k)*NORM%l_N
            endif
            if (STRUC_DIM.ge.3) then
                write(*,*) '***error*** Not implemented.'
                stop
            endif

            i                = i +1
            poi_cols(i)%str = 'psi'
            poi_vals(i,n)   = CONT_POI%var(n)

            i                = i +1
            poi_cols(i)%str = 'eps'
            poi_vals(i,n)   = POINT(l,k,1)%eps
        enddo
    enddo

    call save_hdf5('laplace', trim(CON(con_fix)%name), poi_cols, poi_vals, con_fix.eq.1)

    deallocate(poi_vals)

    !get charges
    do id=1,N_CON
        if (id.eq.TAMB_ID) cycle
        call get_charge(id,'POI',sf_charge)
        q_con(id) = sum(sf_charge)
    enddo 

endfunction


subroutine save_capacity
    integer                  :: i,n,k
    real(8),dimension(N_CON) :: q_con
    !for storing
    type(string)                       :: cap_cols(100) ! maximum number of columns of IV file
    real(8),dimension(:,:),allocatable :: cap_vals      ! values of IV file

    if (.not.allocated(cap_vals)) allocate(cap_vals(100,1))

    ! no contacts ?
    if (N_CON.eq.0) return

    k = 0
    do i=1,N_CON
        if (i.eq.TAMB_ID) cycle
        q_con          = calc_capacity(i) !get charge on all contacts when contact(i)=1V
        do n=1,N_CON 
            if (n.eq.TAMB_ID) cycle
            k = k +1
            cap_cols(k)%str = 'C_'//trim(CON(i)%name)//trim(CON(n)%name)
            cap_vals(k,1)   = q_con(n)
        enddo
    enddo

    call save_hdf5('', 'cap', cap_cols, cap_vals, .True.)
    deallocate(cap_vals)

endsubroutine

subroutine set_psi_initial
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set the initial electrostatic potential
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer                                :: i
    character(len=32),dimension(:),pointer :: cols

    nullify(cols)

    !if (.not.SEMI_EXIST)         return ! no semiconductor
    if (.not.OP_FIRST) return ! only first operation point

    do i=1,N_SP
        CN(i) = DONATORS(i)
        CP(i) = ACCEPTORS(i)
    enddo

    if     (US%poisson%init_psi(1:7).eq.'laplace') then
        call CONT_POI%set(.false.) ! get initial CONT_POI%var/PSI_SEMI
        call CONT_POI%solve(dble(1))

    elseif     (US%poisson%init_psi(1:7).eq.'buildin') then
        do i=1,N_SP
            CONT_POI%var(SP(i)%id_p) = PSI_BUILDIN(i)
        enddo

    else
        write(*,*) '***error*** init_psi keyword in inp file not valid'
        stop
    endif

endsubroutine

subroutine set_psi_semi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! from the electrostatic potential in the whole structure, set only that inside the semiconductor (has its own index)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i

    if (.not.SEMI_EXIST) return

    ! --> get CNT electrostatic potential ----
    do i=1,N_SP
        PSI_SEMI(i) = CONT_POI%var(SP(i)%id_p)
    enddo
endsubroutine

subroutine get_charge_2dcart(cont_id,psi_type,sf_charge)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the surface charge on a contacts in the 2D case.
    !
    ! input
    ! -----
    ! cont_id : integer
    !   Id of the contact to integrate along.
    ! psi_type : character
    !   The type of potential to integrate. Possible values: LAPL (default),AC_R ,AC_I. 
    !
    ! output
    ! ------
    ! sf_charge : real(8),dimension(6)
    !   The surface charge. Index (1) is the y_line at x=x_low
    !   Index (2) is the y_line at x=x_upp.
    !   Index (3) is the x_line at y=y_upp.
    !   Index (4) is the x_line at y=y_low.
    !   Other indexes not used.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,             intent(in)  :: cont_id   !< contact id
    character(len=*),    intent(in)  :: psi_type  !< type of psi
    real(8),dimension(6),intent(out) :: sf_charge !< surface charge
    real(8),dimension(:,:),pointer :: eps,psir !epsilon and potential for every point
    real(8)                        :: e,factor
    integer                        :: i,j
    character(len=8)               :: ptype


    nullify(eps,psir)
    allocate(eps(NX1,NY1))
    allocate(psir(NX1,NY1))
    psir      = dble(0)
    eps       = dble(1) ! air
    sf_charge = dble(0)
    write(ptype,'(A)') psi_type

    ! --> for convenience
    do j=1,NY1
        do i=1,NX1
            if     (ptype(1:4).eq.'LAPL') then
                psir(i,j)  = CONT_POI%var(i+NX1*(j-1))

            elseif (ptype(1:4).eq.'AC_R') then
                psir(i,j)  = real(CONT_POI%delta_ac(i+NX1*(j-1)))

            elseif (ptype(1:4).eq.'AC_I') then
                psir(i,j)  = imag(CONT_POI%delta_ac(i+NX1*(j-1)))

            else
                psir(i,j)  = CONT_POI%var(     i+NX1*(j-1))

            endif

            eps(i,j)   = POINT(i,j,1)%eps

        enddo
    enddo

    ! eps = 10

    ! --> calculate charges
    sf_charge = dble(0)
    do j = 1,NY1
        do i = 1,NX1
            if (POINT(i,j,1)%cont_id.ne.cont_id) cycle
            ! ------------------ y-line (x==low_x) ------------------------
            if (POINT(i,j,1)%cont_b_xlow) then
                e            = -((psir(i-1,j) - psir(i,j))/DX(i-1))
                factor       = 0
                if ((j.lt.NY1)) then ! (x-,y+)
                    factor     = factor + eps(i-1,j  )*DY(j  )*0.5
                endif
                if ((j.gt.1  )) then ! (x-,y-)
                    factor     = factor + eps(i-1,j-1)*DY(j-1)*0.5
                endif
                sf_charge(1) = sf_charge(1) + e*factor
                ! e            = -((psir(i-1,j) - psir(i,j))/DX(i-1))
                ! factor       = 0
                ! if ((j.lt.NY1)) then ! (x-,y+)
                !     factor     = factor + eps(i,j  )*DY(j  )*0.5
                ! endif
                ! if ((j.gt.1  )) then ! (x-,y-)
                !     factor     = factor + eps(i,j)  *DY(j-1)*0.5
                ! endif
                ! sf_charge(1) = sf_charge(1) + e*factor
            endif

            ! ------------- y-line (x==upp_x) ----------------------------
            if (POINT(i,j,1)%cont_b_xupp) then
                e            = -((psir(i+1,j) - psir(i,j))/DX(i))
                factor       = 0
                if ((j.lt.NY1)) then ! (x+,y+)
                    factor     = factor + eps(i  ,j  )*DY(j  )*0.5
                endif
                if ((j.gt.1  )) then ! (x+,y-)
                    factor     = factor + eps(i  ,j-1)*DY(j-1)*0.5
                endif
                sf_charge(2) = sf_charge(2) + e*factor
                ! e            = -((psir(i+1,j) - psir(i,j))/DX(i))
                ! factor       = 0
                ! if ((j.lt.NY1)) then ! (x+,y+)
                !     factor     = factor + eps(i  ,j  )*DY(j  )*0.5
                ! endif
                ! if ((j.gt.1  )) then ! (x+,y-)
                !     factor     = factor + eps(i  ,j)*DY(j-1)*0.5
                ! endif
                ! sf_charge(2) = sf_charge(2) + e*factor
                
            endif

            ! --------------- x-line (y==y_low) ----------------------
            if (POINT(i,j,1)%cont_b_ylow) then
                e            = -((psir(i,j-1) - psir(i,j))/DY(j-1))
                factor       = 0
                if ((i.lt.NX1)) then ! (x+,y-)
                    factor     = factor + eps(i  ,j-1)*DX(i  )*0.5
                endif
                if ((i.gt.1  )) then ! (x-,y-)
                    factor     = factor + eps(i-1,j-1)*DX(i-1)*0.5
                endif
                sf_charge(3) = sf_charge(3) + e*factor
                ! e            = -((psir(i,j-1) - psir(i,j))/DY(j-1))
                ! factor       = 0
                ! if ((i.lt.NX1)) then ! (x+,y-)
                !     factor     = factor + eps(i  ,j)*DX(i  )*0.5
                ! endif
                ! if ((i.gt.1  )) then ! (x-,y-)
                !     factor     = factor + eps(i,j)*DX(i-1)*0.5
                ! endif
                ! sf_charge(3) = sf_charge(3) + e*factor
                
                
            endif

            ! --------------- x-line (y==y_upp) ----------------------
            if (POINT(i,j,1)%cont_b_yupp) then
                e            = -((psir(i,j+1) - psir(i,j))/DY(j)) 
                factor       = 0
                if ((i.lt.NX1)) then ! (x+,y+)
                    factor     = factor + eps(i  ,j  )*DX(i  )*0.5
                endif
                if ((i.gt.1  )) then ! (x-,y+)
                    factor     = factor + eps(i-1,j  )*DX(i-1)*0.5
                endif
                sf_charge(4) = sf_charge(4) + e*factor
                ! e            = -((psir(i,j+1) - psir(i,j))/DY(j)) 
                ! factor       = 0
                ! if ((i.lt.NX1)) then ! (x+,y+)
                !     factor     = factor + eps(i  ,j  )*DX(i  )*0.5
                ! endif
                ! if ((i.gt.1  )) then ! (x-,y+)
                !     factor     = factor + eps(i,j  )*DX(i-1)*0.5
                ! endif
                ! sf_charge(4) = sf_charge(4) + e*factor
                
            endif
        enddo
    enddo

    sf_charge = sf_charge*EP0

    if (associated(psir))       deallocate(psir)
    if (associated(eps))        deallocate(eps)

endsubroutine

subroutine get_charge_3dcart(cont_id,psi_type,sf_charge)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the surface charge on a contacts in the 3D case.
    !
    ! input
    ! -----
    ! cont_id : integer
    !   Id of the contact to integrate along.
    ! psi_type : character
    !   The type of potential to integrate. Possible values: LAPL (default),AC_R ,AC_I. 
    ! fid : integer
    !   output file id for contact output files.
    ! cont_fix : integer
    !   identifier in output file for contact.
    !
    ! output
    ! ------
    ! sf_charge : real(8),dimension(6)
    !   The surface charge. Index (1) 
    !   Index (2) 
    !   Index (3) 
    !   Index (4) 
    !   Index (5) 
    !   Index (6) 
    !   TODO: Write which index is which surface.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,             intent(in)  :: cont_id   !< contact id
    character(len=*),    intent(in)  :: psi_type  !< type of psi
    real(8),dimension(6),intent(out) :: sf_charge !< surface charge

    real(8),dimension(:,:,:),pointer :: eps,psir
    real(8)                          :: e,factor
    integer                          :: i,j,k
    character(len=8)                 :: ptype


    nullify(eps,psir)
    allocate(eps(NX,NY,NZ))
    allocate(psir(NX1,NY1,NZ1))
    psir      = dble(0)
    eps       = dble(1)  ! air
    sf_charge = dble(0)
    write(ptype,'(A)') psi_type

    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if     (ptype(1:4).eq.'LAPL') then
                    psir(i,j,k)  = CONT_POI%var(i+NX1*(j-1)+NX1*NY1*(k-1))

                elseif (ptype(1:4).eq.'AC_R') then
                    psir(i,j,k)  = real(CONT_POI%delta_ac(i+NX1*(j-1)+NX1*NY1*(k-1)))

                elseif (ptype(1:4).eq.'AC_I') then
                    psir(i,j,k)  = imag(CONT_POI%delta_ac(i+NX1*(j-1)+NX1*NY1*(k-1)))

                else
                    psir(i,j,k)  = CONT_POI%var(     i+NX1*(j-1)+NX1*NY1*(k-1))

                endif
                
                if ((i.lt.NX1).and.(j.lt.NY1).and.(k.lt.NZ1)) then
                    eps(i,j,k)   = POINT(i,j,k)%eps ! for speed a local variable
                endif
            enddo
        enddo
    enddo

    ! --> calculate charges
    sf_charge = dble(0)
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%cont_id.ne.cont_id) cycle

                ! 1 ------------------ y-z-plane (x==low_x) ------------------------
                if (POINT(i,j,k)%cont_b_xlow) then
                    e            = -((psir(i-1,j,k) - psir(i,j,k))/DX(i-1))
                    factor       = 0
                    if ((j.lt.NY1).and.(k.lt.NZ1)) then ! (x-,y+,z+)
                      factor     = factor + eps(i-1,j  ,k  )*DY(j  )*DZ(k  )*0.25
                    endif
                    if ((j.gt.1  ).and.(k.lt.NZ1)) then ! (x-,y-,z+)
                      factor     = factor + eps(i-1,j-1,k  )*DY(j-1)*DZ(k  )*0.25
                    endif
                    if ((j.lt.NY1).and.(k.gt.1  )) then ! (x-,y+,z-)
                      factor     = factor + eps(i-1,j  ,k-1)*DY(j  )*DZ(k-1)*0.25
                    endif
                    if ((j.gt.1  ).and.(k.gt.1  )) then ! (x-,y-,z-)
                      factor     = factor + eps(i-1,j-1,k-1)*DY(j-1)*DZ(k-1)*0.25
                    endif

                    sf_charge(1) = sf_charge(1) + e*factor

                endif

                ! 2 ------------------ y-z-plane (x==upp_x) ------------------------
                if (POINT(i,j,k)%cont_b_xupp) then
                    e            = -((psir(i+1,j,k) - psir(i,j,k))/DX(i))
                    factor       = 0
                    if ((j.lt.NY1).and.(k.lt.NZ1)) then ! (x+,y+,z+)
                        factor     = factor + eps(i  ,j  ,k  )*DY(j  )*DZ(k  )*0.25
                    endif
                    if ((j.gt.1  ).and.(k.lt.NZ1)) then ! (x+,y-,z+)
                        factor     = factor + eps(i  ,j-1,k  )*DY(j-1)*DZ(k  )*0.25
                    endif
                    if ((j.lt.NY1).and.(k.gt.1  )) then ! (x+,y+,z-)
                        factor     = factor + eps(i  ,j  ,k-1)*DY(j  )*DZ(k-1)*0.25
                    endif
                    if ((j.gt.1  ).and.(k.gt.1  )) then ! (x+,y-,z-)
                        factor     = factor + eps(i  ,j-1,k-1)*DY(j-1)*DZ(k-1)*0.25
                    endif

                    sf_charge(2) = sf_charge(2) + e*factor

                endif

                ! 3 ------------------ x-z-plane (y==low_y) ------------------------
                if (POINT(i,j,k)%cont_b_ylow) then
                    e            = -((psir(i,j-1,k) - psir(i,j,k))/DY(j-1)) 
                    factor       = 0
                    if ((i.lt.NX1).and.(k.lt.NZ1)) then ! (x+,y-,z+)
                        factor     = factor + eps(i  ,j-1,k  )*DX(i  )*DZ(k  )*0.25
                    endif
                    if ((i.gt.1  ).and.(k.lt.NZ1)) then ! (x-,y-,z+)
                        factor     = factor + eps(i-1,j-1,k  )*DX(i-1)*DZ(k  )*0.25
                    endif
                    if ((i.lt.NX1).and.(k.gt.1  )) then ! (x+,y-,z-)
                        factor     = factor + eps(i  ,j-1,k-1)*DX(i  )*DZ(k-1)*0.25
                    endif
                    if ((i.gt.1  ).and.(k.gt.1  )) then ! (x-,y-,z-)
                        factor     = factor + eps(i-1,j-1,k-1)*DX(i-1)*DZ(k-1)*0.25
                    endif

                    sf_charge(3) = sf_charge(3) + e*factor

                endif

                ! 4 ------------------ x-z-plane (y==upp_y) ------------------------
                if (POINT(i,j,k)%cont_b_yupp) then
                    e            = -((psir(i,j+1,k) - psir(i,j,k))/DY(j))
                    factor       = 0
                    if ((i.lt.NX1).and.(k.lt.NZ1)) then ! (x+,y+,z+)
                        factor     = factor + eps(i  ,j  ,k  )*DX(i  )*DZ(k  )*0.25
                    endif
                    if ((i.gt.1  ).and.(k.lt.NZ1)) then ! (x-,y+,z+)
                        factor     = factor + eps(i-1,j  ,k  )*DX(i-1)*DZ(k  )*0.25
                    endif
                    if ((i.lt.NX1).and.(k.gt.1  )) then ! (x+,y+,z-)
                        factor     = factor + eps(i  ,j  ,k-1)*DX(i  )*DZ(k-1)*0.25
                    endif
                    if ((i.gt.1  ).and.(k.gt.1  )) then ! (x-,y+,z-)
                        factor     = factor + eps(i-1,j  ,k-1)*DX(i-1)*DZ(k-1)*0.25
                    endif

                    sf_charge(4) = sf_charge(4) + e*factor
                  
                endif

                ! 5 ------------------ x-y-plane (z==low_z) ------------------------
                if (POINT(i,j,k)%cont_b_zlow) then
                    e            = -((psir(i,j,k-1) - psir(i,j,k))/DZ(k-1)) 
                    factor       = 0
                    if ((i.lt.NX1).and.(j.lt.NY1)) then ! (x+,y+,z-)
                        factor     = factor + eps(i  ,j  ,k-1)*DX(i  )*DY(j  )*0.25
                    endif
                    if ((i.gt.1  ).and.(j.lt.NY1)) then ! (x-,y+,z-)
                        factor     = factor + eps(i-1,j  ,k-1)*DX(i-1)*DY(j  )*0.25
                    endif
                    if ((i.lt.NX1).and.(j.gt.1  )) then ! (x+,y-,z-)
                        factor     = factor + eps(i  ,j-1,k-1)*DX(i  )*DY(j-1)*0.25
                    endif
                    if ((i.gt.1  ).and.(j.gt.1  )) then ! (x-,y-,z-)
                        factor     = factor + eps(i-1,j-1,k-1)*DX(i-1)*DY(j-1)*0.25
                    endif

                    sf_charge(5) = sf_charge(5) + e*factor
                  
                endif

                ! 6 ------------------ x-y-plane (z==upp_z) ------------------------
                if (POINT(i,j,k)%cont_b_zupp) then
                    e            = -((psir(i,j,k+1) - psir(i,j,k))/DZ(k)) 
                    factor       = 0
                    if ((i.lt.NX1).and.(j.lt.NY1)) then ! (x+,y+,z+)
                        factor     = factor + eps(i  ,j  ,k )*DX(i  )*DY(j  )*0.25
                    endif
                    if ((i.gt.1  ).and.(j.lt.NY1)) then ! (x-,y+,z+)
                        factor     = factor + eps(i-1,j  ,k )*DX(i-1)*DY(j  )*0.25
                    endif
                    if ((i.lt.NX1).and.(j.gt.1  )) then ! (x+,y-,z+)
                        factor     = factor + eps(i  ,j-1,k )*DX(i  )*DY(j-1)*0.25
                    endif
                    if ((i.gt.1  ).and.(j.gt.1  )) then ! (x-,y-,z+)
                        factor     = factor + eps(i-1,j-1,k )*DX(i-1)*DY(j-1)*0.25
                    endif

                    sf_charge(6) = sf_charge(6) + e*factor
                  
                endif
            enddo
        enddo
    enddo

    sf_charge = sf_charge*EP0

    if (associated(psir))       deallocate(psir)
    if (associated(eps))        deallocate(eps)

endsubroutine

! ---------------------------------------------------------------------------------------------------------------------
!> \brief calculate difference of charge in contact
!> \details difference to laplace solution
subroutine calc_diff_charge_cont(q_cont,n_iter)

real(8),dimension(N_CON),intent(out) :: q_cont !< charge on contacts
integer,                 intent(in)  :: n_iter !< iteration number

real(8),dimension(N_CON) :: q_cont2
integer                  :: id
real(8),dimension(6)     :: sf_charge ! x,y,z side
logical                  :: solve_lapl
real(8)                  :: qcon


do id=1,N_CON
  call get_charge(id,'POI',sf_charge)
  q_cont(id) = sum(sf_charge)
enddo

solve_lapl = TR_IT.le.1
if (TR_IT.gt.1) then
  do id=1,N_CON
    if (abs(TR_OP(id,TR_IT)-TR_OP(id,TR_IT-1)).gt.0) then
      solve_lapl = .true.
    endif
  enddo
endif
if (n_iter.ge.2) solve_lapl = .false.

! if (solve_lapl) then
!   call calc_poisson_nocharge
! endif

do id=1,N_CON
  call get_charge(id,'LAPL',sf_charge)
  q_cont2(id) = sum(sf_charge)

enddo

q_cont  = q_cont-q_cont2
qcon   = sum(q_cont)

endsubroutine

! ---------------------------------------------------------------------------------------------------------------------
!> \brief calculate charge on contact
!> \details
subroutine get_charge(cont_id,psi_type,sf_charge,fid,cont_fix)
    integer,             intent(in) :: cont_id   !< contact id
    character(len=*),    intent(in) :: psi_type  !< type of psi
    real(8),dimension(6),intent(out):: sf_charge !< surface charge
    integer,optional,    intent(in) :: fid       !< file id for output
    integer,optional,    intent(in) :: cont_fix  !< contact id at 1V
    integer :: c,id

    if (present(cont_fix)) then
        c = cont_fix
    else
        c = 0
    endif

    if (present(fid)) then
        id = fid
    else
        id = -1
    endif

    if (STRUC_DIM.eq.3) then
        call get_charge_3dcart(  cont_id,psi_type,sf_charge)

    elseif (STRUC_DIM.eq.2) then
        call get_charge_2dcart(cont_id,psi_type,sf_charge)

    else
        sf_charge = 0
        !write(*,'(A)') '***error*** dimension not implemented for surface charge calculation.'
        !stop
    endif

endsubroutine






endmodule
