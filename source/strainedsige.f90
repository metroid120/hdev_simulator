!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! This module implements the models from the Paper:
! "Si/SiGe heterostructure parameters for device simulations" Lianfeng Yang
! It has been implemented by the student Victor Kanzantsev in his Oberseminar

module strainedsige

    
use phys_const

use semiconductor_globals, only: N_SP, TEMP_LATTICE, VT_LATTICE
implicit none
private
public get_mcond
public get_deg
public get_meff_valence
public get_strained_DOS_cond, get_strained_DOS_val

contains

real(8) function get_mcond(x) result(m_cond)
    real(8), dimension(3) :: W11, W21, W31, W12, W22, W32, m
    real(8), intent (in) :: x
        !!Wii = (/'m_l',    'm_t1',    'm_t2'/)
    W11 = (/0.91793, 0.1974800, 0.19744/)
    W21 = (/0.015733, 0.0049877, 0.014813/)
    W31 = (/0.0005307,-0.0007336, -0.00125/)
    W12 = (/0.018794,0.0028421, 0.0028605/)
    W22 = (/-0.0080541,-0.0034829, -0.0034666/)
    W32 = (/-0.0008887,0.0001538, 0.0001288/)


    m = (W11 + x*W12) + (W21 + x*W22)*x + (W31 + x*W32)*x**2
    
    m_cond = (0.5*(  m(1)**(-1) + m(3)**(-1)  ) )**(-1)
    
end function get_mcond

real(8) function get_deg(x)
    real(8) :: a, b, k, T, m_e, a_0_Si, a_0_Ge, a_0_x, a_0_y
    real(8) :: c_11_Si, c_11_Ge, c_12_Si, c_12_Ge, c_11, c_12
    real(8) :: a_ll, a_per, eps_ll, eps_per, Xi, delE_c_001, delE_c_100_010
    real(8), intent (in) :: x
    T = TEMP_LATTICE !K
    a = 4.
    b = 2.
    k = BK !!
    m_e = AM0
    a_0_Si = 5.43  
    a_0_Ge = 5.65 !A
    a_0_x = a_0_Si + 0.200326 * x * (1 - x) + (a_0_Ge - a_0_Si) * x**2
    a_0_y = a_0_Si 
    c_11_Si = 1.675! # Si, Mbar
    c_11_Ge = 1.315! # Ge, Mbar
    c_12_Si = 0.65! # Si, Mbar
    c_12_Ge = 0.494! # Ge, Mbar
    
    c_11 = c_11_Si + (c_11_Ge - c_11_Si) * x
    c_12 = c_12_Si + (c_12_Ge - c_12_Si) * x
    
    ! (6)
    a_ll = a_0_y
    a_per = a_0_x * (1. - 2. * (c_12 / c_11) * ((a_ll - a_0_x) / a_0_x)) 
    
    !(7)
    eps_per = a_per/a_0_x-1.
    eps_ll = a_ll/a_0_x-1.


    Xi = 9.16 + (9.42-9.16)*x! #[10]eV, Si 9.42 ev for Ge
    delE_c_001 = 2./3.*Xi *( eps_per - eps_ll) !(10)
    delE_c_100_010 = -1./3.*Xi *( eps_per - eps_ll) !(11)
    

    get_deg = (a+b*exp(-abs(delE_c_001 - delE_c_100_010)/VT_LATTICE))
    
end function get_deg

real(8) function get_meff_valence(x,dimens)
    real(8) :: a, b, k, T, m_e, a_0_Si, a_0_Ge, a_0_x, a_0_y
    real(8) :: c_11_Si, c_11_Ge, c_12_Si, c_12_Ge, c_11, c_12
    real(8) :: a_ll, a_per, eps_ll, eps_per, b_eV, Del0, delE, DelEv1, DelEv3, DelEv2
    real(8) :: m_SSiGe_v2, m_SSiGe_v1, m_U, m_M
    real(8), intent (in) :: x
    integer(4), intent (in) :: dimens
    T = TEMP_LATTICE !K
    a = 4.
    b = 2.
    k = BK !!
    m_e = AM0
    a_0_Si = 5.43  
    a_0_Ge = 5.65 !A
    a_0_x = a_0_Si + 0.200326 * x * (1 - x) + (a_0_Ge - a_0_Si) * x**2
    a_0_y = a_0_Si 
    c_11_Si = 1.675! # Si, Mbar
    c_11_Ge = 1.315! # Ge, Mbar
    c_12_Si = 0.65! # Si, Mbar
    c_12_Ge = 0.494! # Ge, Mbar
    
    c_11 = c_11_Si + (c_11_Ge - c_11_Si) * x
    c_12 = c_12_Si + (c_12_Ge - c_12_Si) * x
    
    a_ll = a_0_y
    a_per = a_0_x * (1. - 2. * (c_12 / c_11) * ((a_ll - a_0_x) / a_0_x)) 
    
    eps_per = a_per/a_0_x-1
    eps_ll = a_ll/a_0_x-1

    b_eV = (-1.5 + (-2.2 + 1.5) * x) 
    Del0 = (0.044 + (0.296-0.044)*x)
    delE = (2. * (b_eV) * (eps_per - eps_ll))
    
    DelEv1 = -(1./6.)*Del0+0.25*delE+0.5*(Del0**2.+Del0*delE +9./4.*delE**2)**0.5
    DelEv3 = -(1./6.)*Del0+0.25*delE-0.5*(Del0**2.+Del0*delE +9./4.*delE**2)**0.5
    DelEv2 = (1./3.)*Del0-0.5*delE
    
    m_SSiGe_v2 =  -2.8369 * x**3. + 4.6844 * x**2. - 2.87 * x + 0.8956
    m_SSiGe_v1 =  -0.1432 * x**3. + 0.3618 * x**2. - 0.3669 * x + 0.2534
    
    m_U = m_SSiGe_v2 
    m_M = m_SSiGe_v1 
    
    get_meff_valence = (m_U**(3./2.) + m_M**(3./2.)*exp(-abs(DelEv2- DelEv1)*Q/(k*T)))**(dble(2)/dimens)
    
end function get_meff_valence
    

real(8) function get_strained_DOS_cond(x, neff_si)
    real(8) ::  k, T, m_e
    real(8) :: DelEc, DelEv1, DelEv2, DelEv3, neff
    real(8), intent (in) :: x, neff_si
    DelEc = -0.6
    DelEv1 = -0.31
    DelEv2 = -0.315
    DelEv3 = -0.044

    T = TEMP_LATTICE !K
    k = BK !!
    m_e = AM0


    get_strained_DOS_cond = neff_si * (4. + 2.*exp((DelEc*x)*Q/(k*T)))/6.
    
end function get_strained_DOS_cond

real(8) function get_strained_DOS_val(x, neff_si)
    real(8) ::  k, T, m_e
    real(8) :: DelEc, DelEv1, DelEv2, DelEv3, neff
    real(8), intent (in) :: x, neff_si
    DelEc = -0.6
    DelEv1 = -0.31
    DelEv2 = -0.315
    DelEv3 = -0.044

    T = TEMP_LATTICE !K
    k = BK !!
    m_e = AM0


    get_strained_DOS_val = neff_si * (1. + exp((DelEv1*x)*Q/(k*T)) + exp((DelEv2*x)*Q/(k*T)))/(2.+ exp((DelEv3*x)*Q/(k*T)))
    
end function get_strained_DOS_val

end module strainedsige