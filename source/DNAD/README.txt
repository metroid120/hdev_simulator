DNAD: a Simple Tool for Automatic Differentiation of Fortran Codes Using Dual Numbers


The Dual_Num_Auto_Diff module consists of the file named "DNAD.f90", the file named "DNADHeaders.f90".

The DNAD distribution file contains these two code files plus two example programs, CircleArea.f90 and CheckSpeed.f90 and this README file.


To install it : 
---------------
Save the file DNAD.f90 and DNADHeaders.f90 in a directory, if necessary with a different 
name or suffix in order to be accepted as fortran 95 code by your compiler


To run the CircleArea test :
----------------------------
Make sure that line 113 of DNAD.f90 reads
INTEGER(2), PUBLIC,PARAMETER:: NDV_AD=1 ! number of design variables
and then compile and link DNAD with the CircleArea code e.g.

gfortran -O3 -c DNAD.f90
gfortran -O3 -c CircleArea.f90
gfortran -O3 -static -o dnad.exe *.o

Run 
./dnad.exe
and enter
5, 1

This should produce the results 

Area=78.5398163397448, 31.4159265358979 

To run the CheckSpeed test :
----------------------------

Change line 113 of the DNAD.f90 file to 
INTEGER(2), PUBLIC,PARAMETER:: NDV_AD=2 ! number of design variables

Remove the CicleArea.o file if present.

gfortran -O3 -c DNAD.f90
gfortran -O3 -c CheckSpeed.f90
gfortran -O3 -static -o dnad.exe *.o
Run ./dnad.exe 
This should show the running times recorded. Note the running time may be different
from what is shown in the paper as it depends on  both the machine and the compiler. 

To use it :
-----------
Modify the source codes you want to differentiate using DNAD. The only changes you 
need to make to DNAD.f90 is to change NDV_AD to the number of design variables you
have. In very rare situations some functions/operations in the the source codes you 
want to differentiate are not defined yet. You can easily insert the definitions 
into the module similar to what has been done for many other functions and operations. 
The instruction for this modification were described in the paper and  the beginning 
comment portion of DNAD.f90. Compile DNAD.f90 and  the compile your source codes and 
link your source codes with DNAD.O



To take full advantage of the compiler optimization, it seems,that combining DNAD.f90
and CheckSpeed.f90 into one file makes a signficant difference in computing speed. 
To test this and see the difference, one just need to use the following:

gfortran -O3 -static -o dnad.exe test.f90


