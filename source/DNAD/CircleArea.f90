PROGRAM CircleArea
        
		USE Dual_Num_Auto_Diff
        TYPE (DUAL_NUM),PARAMETER:: PI=DUAL_NUM(3.141592653589793D0,0.D0)
        TYPE (DUAL_NUM):: radius,area
        
		READ(*,*) radius
        
		Area=PI*radius**2
        WRITE(*,*) "AREA=",Area

END PROGRAM CircleArea