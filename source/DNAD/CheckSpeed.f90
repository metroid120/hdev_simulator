PROGRAM CheckSpeed
USE Dual_Num_Auto_Diff
IMPLICIT NONE
INTEGER::i
TYPE(DUAL_NUM)::x,y,z,ftot,f,u,v
!real(8)::x,y,z,ftot,f
INTEGER::nloops=50000000
REAL:: start_time, end_time

call CPU_TIME(start_time)
x=DUAL_NUM(1.0D0,(/1.0D0,0.0D0,0.0D0/)); 
y=DUAL_NUM(2.0D0,(/0.0D0,1.0D0,0.0D0/)); z=DUAL_NUM(3.0D0,(/0.0D0,0.0D0,1.0D0/))

ftot=0.0D0
DO i=1,nloops
	f=x*y-x*sin(y)*log(i*z)
    ftot= (ftot- f)/exp(z)
ENDDO

write(*,*) ftot

CALL CPU_TIME(end_time)
WRITE(*,*) end_time-start_time,"seconds"
END PROGRAM CheckSpeed