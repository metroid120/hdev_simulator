!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Recombination models for drift diffusion
module recombination

use Dual_Num_Auto_Diff

! ---> variables and functions from other moduls ----------------------------------------------------------------------
use phys_const
use read_user    , only : US
use save_global
use structure
use bandstructure
use dd_types, only : rec_model

use semiconductor, only : SP
use semiconductor, only : PSI_BUILDIN
use semiconductor, only : PSI_SEMI
use semiconductor, only : CPP => CP
use semiconductor, only : CNN => CN
use semiconductor, only : CNN2 => CN2
use semiconductor, only : CNN0 => CN0
use semiconductor, only : CNN20 => CN20

use continuity_elec   , only : CONT_ELEC
use continuity_elec2   , only : CONT_ELEC2

use heterostructure

use mobility

use profile
use semiconductor_globals
use structure
use dd_types
use math_oper , only :integrate
use non_local , only : get_nl_field

implicit none

private

! ---> public variables -----------------------------------------------------------------------------------------------

! ---> public subroutines/ functions ----------------------------------------------------------------------------------
public init_recombination
public get_recombination
public set_intervalley_rates
public set_intervalley_paras
public set_ni_sq
public get_intervalley_rates


contains

subroutine init_recombination
    !!!!!!!!!!!!!!!!!!
    ! init this module
    !!!!!!!!!!!!!!!!!!

    ! --> set eigenleitungsdichte
    allocate(NI_SQ(N_SP))
    NI_SQ = 0
    !call set_ni_sq =>later in DD solver after equilibrium solution

    ! --> set recombination parameters
    call init_recomb_parameter

    ! ! --> initiallize intervalley model
    ! -> shifted after equilibrium solution
    ! call set_intervalley_paras

endsubroutine

subroutine set_ni_sq
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the intrinsic carrier density squared.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: is,n,hetero_id, ib
    TYPE(DUAL_NUM) :: result

    hetero_id = 0

    ! --> eigenleitungsdichte
    do n=1,N_SP
        if (HETERO) hetero_id = n
        is = POINT( SP(n)%id_x,SP(n)%id_y,SP(n)%id_z )%semi_id
        if ((SEMI(is)%elec).and.(.not.SEMI(is)%hole)) then
            ! n*n
            ib      = get_conduction_band_index(SEMI(is))
            result  = get_n_equ_band_dual(is,ib,DUAL_NUM(PSI_BUILDIN(n)+UN(n),0D0),DUAL_NUM(VT_LATTICE,0),hetero_id)
            NI_SQ(n) = result%x_ad_**2

        elseif ((SEMI(is)%hole).and.(.not.SEMI(is)%elec)) then
            ! p*p
            ib      = get_valence_band_index(SEMI(is))
            result  = get_n_equ_band_dual(is,ib,DUAL_NUM(PSI_BUILDIN(n)+UP(n),0D0),DUAL_NUM(VT_LATTICE,0),hetero_id)
            NI_SQ(n) = result%x_ad_**2

        else
            NI_SQ(n) = CNN(n)*CPP(n)
            ! n*p
            ! ib      = get_conduction_band_index(SEMI(is))
            ! result  = get_n_equ_band_dual(is,ib,DUAL_NUM(PSI_BUILDIN(n)+UN(n),0D0),DUAL_NUM(VT_LATTICE,0),hetero_id)
            ! NI_SQ(n) =  result%x_ad_

            ! ib      = get_valence_band_index(SEMI(is))
            ! result  = get_n_equ_band_dual(is,ib,DUAL_NUM(PSI_BUILDIN(n)+UP(n),0D0),DUAL_NUM(VT_LATTICE,0),hetero_id)
            ! NI_SQ(n)=  NI_SQ(n)*result%x_ad_

        endif
    enddo
endsubroutine

subroutine set_intervalley_paras
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate intervalley low energy transition time from principle of detailed balance.
    ! See paper by Markus.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer        :: is,ir,n,index_intervalley_model
    real(8)        :: cn1 = 0, cn2 = 0

    TYPE(DUAL_NUM)  :: zero = DUAL_NUM(0,0)
    TYPE(DUAL_NUM)  :: r = DUAL_NUM(0,0), psi_i = DUAL_NUM(0,0)

    do is=1,size(SEMI)
        !find intervalley recombination model
        index_intervalley_model = 0
        do ir=1,size(SEMI(is)%rec_models)
            if (SEMI(is)%rec_models(ir)%is_intervalley) then
                index_intervalley_model = ir
                exit
            endif
        enddo
        if (index_intervalley_model.eq.0) then
            cycle  !did not find intervalley model for this semiconductor
        endif

        ! --> apply principle of detailed balance here to set intervalley parameters
        ! --> tau_12 is position dependent due to ni position dependence
        do n=1,N_SP
            if (.not.(SP(n)%model_id.eq.is)) then
                cycle ! only find points that belong to this semiconductor
            endif
            if (.not.(SEMI(is)%elec.and.SEMI(is)%elec2)) then
                write(*,*) '***error*** intervalley specified for semiconductor, however less than two electron bands activated.'
                stop
            endif

            !get psi buildin
            cn1    = CNN(n)
            cn2    = CNN2(n)

            SEMI(is)%rec_models(index_intervalley_model)%v_12(n) = 0

            psi_i = DUAL_NUM(PSI_SEMI(n),0)

            r = get_intervalley_rates(n, is,SEMI(is)%rec_models(index_intervalley_model), DUAL_NUM(cn1,0), DUAL_NUM(cn2,0), psi_i, zero, zero, zero, zero, zero, dble(1e-9), zero, zero, zero, dble(1e-9))

            ! r0 = - f12*cn + f21*cn2 
            ! 0 = - (f12+d)*cn + f21*cn2 
            ! 0 = - d*cn + r0
            ! d = r0/cn
            SEMI(is)%rec_models(index_intervalley_model)%v_12(n) = r%x_ad_/cn1

            r = get_intervalley_rates(n, is,SEMI(is)%rec_models(index_intervalley_model), DUAL_NUM(cn1,0), DUAL_NUM(cn2,0), psi_i, zero, zero, zero, zero, zero, dble(1e-9), zero, zero, zero, dble(1e-9))

        enddo
    enddo

endsubroutine

subroutine init_recomb_parameter
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialization recombination model parameters
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: is,i,n,ir, index_intervalley_model

    ! --> first count how many recombination models we have per semiconductor
    do is=1,N_SEMI
        n=0
        do i=1,US%n_recombdef
            if ((SEMI_MOD_NAME(is).eq.US%recombdef(i)%mod_name).or.(US%recombdef(i)%mod_name(1:7).eq.'default')) then
                n=n+1
            endif
        enddo
        allocate(SEMI(is)%rec_models(n))
    enddo

    ! --> allocate recombination models

    ! --> set recombination parameters from user input
    do is=1,N_SEMI
        n=0
        do i=1,US%n_recombdef
            if ((SEMI_MOD_NAME(is).eq.US%recombdef(i)%mod_name).or.(US%recombdef(i)%mod_name(1:7).eq.'default')) then
                n = n +1
                write(SEMI(is)%rec_models(n)%model,'(A)')  trim(US%recombdef(i)%type)
                write(SEMI(is)%rec_models(n)%lifetime,'(A)')  trim(US%recombdef(i)%lifetime)
                SEMI(is)%rec_models(n)%semi_id  = is

                !logicals
                SEMI(is)%rec_models(n)%is_srh              = (US%recombdef(i)%type(1:3).eq.'srh')
                SEMI(is)%rec_models(n)%lifetime_is_default = (US%recombdef(i)%lifetime(1:3).eq.'def')
                SEMI(is)%rec_models(n)%lifetime_is_device  = (US%recombdef(i)%lifetime(1:3).eq.'dev')
                SEMI(is)%rec_models(n)%is_auger            = (US%recombdef(i)%type(1:3).eq.'aug')
                SEMI(is)%rec_models(n)%is_avalanche        = (US%recombdef(i)%type(1:3).eq.'ava')
                SEMI(is)%rec_models(n)%is_langevin         = (US%recombdef(i)%type(1:3).eq.'lan')
                SEMI(is)%rec_models(n)%is_intervalley      = (US%recombdef(i)%type(1:3).eq.'int')
                SEMI(is)%rec_models(n)%force_is_phi        = (US%recombdef(i)%force(1:3).eq.'phi')
                SEMI(is)%rec_models(n)%force_is_psi        = (US%recombdef(i)%force(1:3).eq.'psi')
                SEMI(is)%rec_models(n)%force_is_mix        = (US%recombdef(i)%force(1:3).eq.'mix')

                !langevin
                SEMI(is)%rec_models(n)%beta = US%recombdef(i)%beta

                !srh
                SEMI(is)%rec_models(n)%e_trap       = US%recombdef(i)%e_trap       
                SEMI(is)%rec_models(n)%n_t          = US%recombdef(i)%n_t          
                SEMI(is)%rec_models(n)%beta_n       = US%recombdef(i)%beta_n       
                SEMI(is)%rec_models(n)%beta_p       = US%recombdef(i)%beta_p       
                SEMI(is)%rec_models(n)%c_ref_n      = US%recombdef(i)%c_ref_n      
                SEMI(is)%rec_models(n)%c_ref_p      = US%recombdef(i)%c_ref_p      
                SEMI(is)%rec_models(n)%tau_n_min    = US%recombdef(i)%tau_n_min    
                SEMI(is)%rec_models(n)%tau_p_min    = US%recombdef(i)%tau_p_min    
                SEMI(is)%rec_models(n)%tau_n_max    = US%recombdef(i)%tau_n_max    
                SEMI(is)%rec_models(n)%tau_p_max    = US%recombdef(i)%tau_p_max    
                SEMI(is)%rec_models(n)%tau_n_min_a1 = US%recombdef(i)%tau_n_min_a1
                SEMI(is)%rec_models(n)%tau_n_min_a2 = US%recombdef(i)%tau_n_min_a2
                SEMI(is)%rec_models(n)%tau_p_min_a1 = US%recombdef(i)%tau_p_min_a1
                SEMI(is)%rec_models(n)%tau_p_min_a2 = US%recombdef(i)%tau_p_min_a2
                SEMI(is)%rec_models(n)%tau_n_max_a1 = US%recombdef(i)%tau_n_max_a1
                SEMI(is)%rec_models(n)%tau_n_max_a2 = US%recombdef(i)%tau_n_max_a2
                SEMI(is)%rec_models(n)%tau_p_max_a1 = US%recombdef(i)%tau_p_max_a1
                SEMI(is)%rec_models(n)%tau_p_max_a2 = US%recombdef(i)%tau_p_max_a2
                SEMI(is)%rec_models(n)%sigma_n      = US%recombdef(i)%sigma_n      
                SEMI(is)%rec_models(n)%sigma_p      = US%recombdef(i)%sigma_p      
                SEMI(is)%rec_models(n)%zeta_n       = US%recombdef(i)%zeta_n       
                SEMI(is)%rec_models(n)%zeta_p       = US%recombdef(i)%zeta_p       

                !auger
                SEMI(is)%rec_models(n)%c_n    = US%recombdef(i)%c_n    
                SEMI(is)%rec_models(n)%c_p    = US%recombdef(i)%c_p    
                SEMI(is)%rec_models(n)%c_n_a1 = US%recombdef(i)%c_n_a1
                SEMI(is)%rec_models(n)%c_n_a2 = US%recombdef(i)%c_n_a2
                SEMI(is)%rec_models(n)%c_p_a1 = US%recombdef(i)%c_p_a1
                SEMI(is)%rec_models(n)%c_p_a2 = US%recombdef(i)%c_p_a2
                !zetas from srh def

                !avalanche
                SEMI(is)%rec_models(n)%alpha_n  = US%recombdef(i)%alpha_n  
                SEMI(is)%rec_models(n)%alpha_p  = US%recombdef(i)%alpha_p  
                SEMI(is)%rec_models(n)%e_crit_n = US%recombdef(i)%e_crit_n
                SEMI(is)%rec_models(n)%e_crit_p = US%recombdef(i)%e_crit_p

                ! intervalley recombination
                ! M. Müller, P. Dollfus and M. Schröter, 
                ! "1-D Drift-Diffusion Simulation of Two-Valley Semiconductors and Devices," 
                ! in IEEE Transactions on Electron Devices, vol. 68, no. 3, pp. 1221-1227, March 2021, doi: 10.1109/TED.2021.3051552.
                SEMI(is)%rec_models(n)%v_21     = US%recombdef(i)%v_21
                SEMI(is)%rec_models(n)%dv_21    = US%recombdef(i)%dv_21
                SEMI(is)%rec_models(n)%v21off   = US%recombdef(i)%v21off.eq.dble(1)
                SEMI(is)%rec_models(n)%xl       = US%recombdef(i)%xl
                SEMI(is)%rec_models(n)%xr       = US%recombdef(i)%xr
                SEMI(is)%rec_models(n)%a        = US%recombdef(i)%a
                SEMI(is)%rec_models(n)%af       = US%recombdef(i)%af
                SEMI(is)%rec_models(n)%f0       = US%recombdef(i)%f0
                SEMI(is)%rec_models(n)%zeta_dop = US%recombdef(i)%zeta_dop
                SEMI(is)%rec_models(n)%dop_crit = US%recombdef(i)%dop_crit

            endif
        enddo
    enddo

    !init to zero intervalley offset
    do is=1,size(SEMI)
        !find intervalley recombination model
        index_intervalley_model = 0
        do ir=1,size(SEMI(is)%rec_models)
            if (SEMI(is)%rec_models(ir)%is_intervalley) then
                index_intervalley_model = ir
                exit
            endif
        enddo
        if (index_intervalley_model.eq.0) then
            cycle  !did not find intervalley model for this semiconductor
        endif
        allocate(SEMI(is)%rec_models(index_intervalley_model)%v_12(N_SP))
        SEMI(is)%rec_models(index_intervalley_model)%v_12(N_SP) = 0
    enddo
endsubroutine

subroutine get_recombination(is,sp_id, hetero_id,n,p,ni_sq_i,don,acc,graded,temp&
                           &, dn_dphin, dn_dpsi, dn_dtl, dp_dphip, dp_dpsi, dp_dtl&
                           &, phin_m,phin, phin_p, phip_m, phip, phip_p, tn, dx_m, dx_p, cb_vb&
                           &, r, dr_dphin, dr_dphip, dr_dpsi, dr_dtl, dr_dtn)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the recombination rate in the semiconductor. FIeld dependent effects only along 
    ! x direction.
    !
    ! input
    ! -----
    ! is : integer
    !   Semiconductor model id.
    ! sp_id : integer
    !   Index to point in semiconductor.
    ! hetero_id :integer
    !   Index to point in semiconductor, if effective mass non-constant.
    ! n : real(8)
    !   Electron carrier density.
    ! p : real(8)
    !   Hole carrier density.
    ! ni_sq_i : real(8)
    !   Squared intrinsic carrier density at point.
    ! don : real(8)
    !   Donator density at point.
    ! acc : real(8)
    !   Acceptor density at point.
    ! graded : real(8)
    !   Grading at point.
    ! temp : real(8)
    !   Lattice temperature at point.
    ! dn_dphin : real(8)
    !   Derivative electron density with quasi Fermi potential.
    ! dn_dpsi : real(8)
    !   Derivative electron density with electrostatic potential.
    ! dn_dtl : real(8)
    !   Derivative electron density with lattice temperature.
    ! dp_dphip : real(8)
    !   Derivative hole density with quasi Fermi potential.
    ! dp_dpsi : real(8)
    !   Derivative hole density with electrostatic potential.
    ! dp_dtl : real(8)
    !   Derivative hole density with lattice temperature.
    ! psi : real(8)
    !   Electrostatic potential at point.
    ! phin_m : real(8)
    !   Quasi Fermi potential electrons at left point.
    ! phin : real(8)
    !   Quasi Fermi potential electrons at point.
    ! phin_p : real(8)
    !   Quasi Fermi potential electrons at right point.
    ! phip_m : real(8)
    !   Quasi Fermi potential holes at left point.
    ! phip : real(8)
    !   Quasi Fermi potential holes at point.
    ! phip_p : real(8)
    !   Quasi Fermi potential holes at right point.
    ! tn : real(8)
    !   Electron temperature at point.
    ! dx_m : real(8)
    !   Distance between left and mid point.
    ! dx_p : real(8)
    !   Distance between right and mid point.
    ! cb_vb : logical
    !   Obsolete.
    !
    ! output
    ! ------
    ! r : real(8)
    !   Recombination rate at point.
    ! dr_dphin : real(8) ,dimension(3)
    !   Recombination rate derivative with phin at left (1) mid (2) and right (3) point.
    ! dr_dphip  : real(8) ,dimension(3)
    !   Recombination rate derivative with phip at left (1) mid (2) and right (3) point.
    ! dr_dpsi  : real(8) ,dimension(3)
    !   Recombination rate derivative with psi at left (1) mid (2) and right (3) point.
    ! dr_dtl : real(8) ,dimension(3)
    !   Recombination rate derivative with tl at left (1) mid (2) and right (3) point.
    ! dr_dtn : real(8) ,dimension(3)
    !   Recombination rate derivative with tn at mid point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,                intent(in )   :: is
    integer,                intent(in )   :: sp_id
    integer,                intent(in )   :: hetero_id
    real(8),                value         :: n
    real(8),                value         :: p
    real(8),                intent(in )   :: ni_sq_i
    real(8),                value         :: acc
    real(8),                value         :: don
    real(8),                intent(in )   :: graded
    real(8),                intent(in )   :: temp
    real(8),                intent(in )   :: dn_dphin
    real(8),                intent(in )   :: dn_dpsi
    real(8),                intent(in )   :: dp_dphip
    real(8),                intent(in )   :: dp_dpsi
    real(8),                intent(in )   :: dn_dtl
    real(8),                intent(in )   :: dp_dtl
    real(8),                value         :: dx_m
    real(8),                value         :: dx_p
    logical,                intent(in)    :: cb_vb
    real(8),                intent(in)    :: phin_m
    real(8),                intent(in)    :: phin
    real(8),                intent(in)    :: tn
    real(8),                intent(in)    :: phin_p
    real(8),                intent(in)    :: phip_m
    real(8),                intent(in)    :: phip
    real(8),                intent(in)    :: phip_p
    real(8),                intent(out)   :: r
    real(8),dimension(3),   intent(out)   :: dr_dphin
    real(8),dimension(3),   intent(out)   :: dr_dphip
    real(8),dimension(3),   intent(out)   :: dr_dpsi
    real(8),dimension(3),   intent(out)   :: dr_dtl
    real(8),                intent(out)   :: dr_dtn

    real(8) :: np_min_ni
    integer :: k
    TYPE(DUAL_NUM) :: result

    type(rec_model),pointer :: rec

    r         = 0
    dr_dphin  = 0
    dr_dphip  = 0
    dr_dpsi   = 0
    dr_dtl    = 0
    dr_dtn    = 0

    ! if (abs(n*p/ni_sq_i-dble(1)).lt.(1e-3)) then
    !   np_min_ni = 0

    ! else
    np_min_ni = (n*p-ni_sq_i)*NORM%N_N**2 !denormalize
    n         = n   *NORM%N_N
    p         = p   *NORM%N_N
    don       = don *NORM%N_N
    acc       = acc *NORM%N_N
    dx_m      = dx_m*NORM%l_N
    dx_p      = dx_p*NORM%l_N

    ! endif

    do k=1,size(SEMI(is)%rec_models)
        rec => SEMI(is)%rec_models(k)

        if (rec%is_srh.and.cb_vb) then
            result      = get_srh_rate(sp_id,is,ni_sq_i,don,acc,graded,hetero_id,rec, DUAL_NUM(n,(/1,0,0,0,0,0,0,0/)), DUAL_NUM(p,(/0,1,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,1,0,0,0,0,0/)))
            r           = r + result%x_ad_
            dr_dphin(2) = dr_dphin(2) + result%xp_ad_(1)*dn_dphin
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(1)*dn_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(1)*dn_dtl
            dr_dphip(2) = dr_dphip(2) + result%xp_ad_(2)*dp_dphip
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(2)*dp_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(2)*dp_dtl
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(3)

        elseif (rec%is_auger.and.cb_vb) then
            result      = get_auger_rate(rec, ni_sq_i, DUAL_NUM(n,(/1,0,0,0,0,0,0,0/)), DUAL_NUM(p,(/0,1,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,1,0,0,0,0,0/)), graded)
            r           = r + result%x_ad_
            dr_dphin(2) = dr_dphin(2) + result%xp_ad_(1)*dn_dphin
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(1)*dn_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(1)*dn_dtl
            dr_dphip(2) = dr_dphip(2) + result%xp_ad_(2)*dp_dphip
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(2)*dp_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(2)*dp_dtl
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(3)

        elseif (rec%is_avalanche.and.cb_vb) then
            result      = get_avalanche_rate(sp_id,is,rec, DUAL_NUM(n,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(p,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phin,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(tn,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phin_m,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip_m,(/0,0,0,0,0,0,0,0/)), dx_m, DUAL_NUM(phin_p,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip_p,(/0,0,0,0,0,0,0,0/)), dx_p)
            r           = r + result%x_ad_

            ! derivatives directly after solution variables
            result      = get_avalanche_rate(sp_id,is,rec, DUAL_NUM(n,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(p,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,1,0/)), DUAL_NUM(phin,(/0,1,0,0,0,0,0,0/)), DUAL_NUM(phip,(/0,0,0,0,1,0,0,0/)), DUAL_NUM(tn,(/0,0,0,0,0,0,0,1/)), DUAL_NUM(phin_m,(/1,0,0,0,0,0,0,0/)), DUAL_NUM(phip_m,(/0,0,0,1,0,0,0,0/)), dx_m, DUAL_NUM(phin_p,(/0,0,1,0,0,0,0,0/)), DUAL_NUM(phip_p,(/0,0,0,0,0,1,0,0/)), dx_p)
            dr_dphin(1) = dr_dphin(1) + result%xp_ad_(1)
            dr_dphin(2) = dr_dphin(2) + result%xp_ad_(2)
            dr_dphin(3) = dr_dphin(3) + result%xp_ad_(3)
            dr_dphip(1) = dr_dphip(1) + result%xp_ad_(4)
            dr_dphip(2) = dr_dphip(2) + result%xp_ad_(5)
            dr_dphip(3) = dr_dphip(3) + result%xp_ad_(6)
            dr_dtl(2)   = result%xp_ad_(7)
            dr_dtn      = result%xp_ad_(8)


            !derivatives after carrier densities at point
            result      = get_avalanche_rate(sp_id,is,rec, DUAL_NUM(n,(/1,0,0,0,0,0,0,0/)), DUAL_NUM(p,(/0,1,0,0,0,0,0,0/)), DUAL_NUM(temp,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phin,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(tn,(/0,0,0,0,0,0,0,1/)), DUAL_NUM(phin_m,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip_m,(/0,0,0,0,0,0,0,0/)), dx_m, DUAL_NUM(phin_p,(/0,0,0,0,0,0,0,0/)), DUAL_NUM(phip_p,(/0,0,0,0,0,0,0,0/)), dx_p)
            dr_dphin(2) = dr_dphin(2) + result%xp_ad_(1)*dn_dphin
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(1)*dn_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(1)*dn_dtl
            dr_dtn      = dr_dtn      + result%xp_ad_(1)*dn_dtl
            dr_dphip(2) = dr_dphip(2) + result%xp_ad_(2)*dp_dphip
            dr_dpsi(2)  = dr_dpsi(2)  + result%xp_ad_(2)*dp_dpsi
            dr_dtl(2)   = dr_dtl(2)   + result%xp_ad_(2)*dp_dtl

        elseif (rec%is_langevin.and.cb_vb) then
            r           = r           + (np_min_ni             ) * rec%beta
            dr_dphin(2) = dr_dphin(2) + (p*dn_dphin            ) * rec%beta
            dr_dphip(2) = dr_dphip(2) + (dp_dphip*n            ) * rec%beta
            dr_dpsi(2)  = dr_dpsi(2)  + (dp_dpsi*n + dn_dpsi*p ) * rec%beta
            dr_dtl(2)   = dr_dtl(2)   + (dp_dtl*n + dn_dtl*p   ) * rec%beta !not correct for tl

        endif

        ! if (abs(n*p/ni_sq_i-dble(1)).lt.(0.01)) then
        !   r = 0
        ! endif

    enddo
    ! take into account normalization
    r         = r        * NORM%R_N_inv
    dr_dphin  = dr_dphin * NORM%R_N_inv
    dr_dphip  = dr_dphip * NORM%R_N_inv
    dr_dpsi   = dr_dpsi  * NORM%R_N_inv
    dr_dtl    = dr_dtl   * NORM%R_N_inv
endsubroutine

subroutine set_intervalley_rates(is, index_sp, n, n2&
                           &,dn_dphin, dn_dpsi, dn2_dphin2, dn2_dpsi&
                           &,psi,phin,phin2, psi_m,phin_m,phin2_m,dx_m, psi_p,phin_p,phin2_p,dx_p &
                           &,r, dr_dphin, dr_dphin2, dr_dpsi&
                           &,zeta, zeta_dqf, zeta_inv, zeta_inv_dqf)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set intervalley recombination rates. Energy dependence only due to field in x direction.
    ! tau_12 has been calculated before using principle of detailed balance. Only for Multi Valley DD simulations.
    ! 
    ! input
    ! -----
    ! is        : integer
    !   Index to the semiconductor model.
    ! index_sp  : integer
    !   Index to the point in the semiconductor.
    ! n         : real(8)
    !   Electron carrier density.
    ! n2        : real(8)
    !   Electron 2 carrier density.
    ! dn_dphin  : real(8)
    !   Electron carrier density derivative with phin.
    ! dn_dpsi   : real(8)
    !   Electron carrier density derivative with psi.
    ! dn2_dphin2: real(8)
    !   Electron 2 carrier density derivative with phin2.
    ! dn2_dpsi  : real(8)
    !   Electron 2 carrier density derivative with psi.
    ! psi       : real(8)
    !   Electrostatic potential at point.
    ! phin      : real(8)
    !   Quasi Fermi potential electrons at point.
    ! phin2     : real(8)
    !   Quasi Fermi potential electrons 2 at point.
    ! psi_m     : real(8)
    !   Electrostatic potential at point left.
    ! phin_m    : real(8)
    !   Quasi Fermi potential electrons potential at point left.
    ! phin2_m   : real(8)
    !   Quasi Fermi potential electons 2 potential at point left.
    ! dx_m      : real(8)
    !   Distance mid to left point.
    ! psi_p     : real(8)
    !   Electrostatic potential at point right.
    ! phin_p    : real(8)
    !   Quasi Fermi potential electrons potential at point right.
    ! phin2_p   : real(8)
    !   Quasi Fermi potential electons 2 potential at point right.
    ! dx_p      : real(8)
    !   Distance mid to right point.
    !
    ! output
    ! ------
    ! r         : real(8)
    !   Intervalley transer rate.
    ! dr_dphin  : real(8),dimension(3)
    !   Intervalley transer rate derivative with phin at left (1) mid (2) and right (3) point.
    ! dr_dphin2 : real(8),dimension(3)
    !   Intervalley transer rate derivative with phin2 at left (1) mid (2) and right (3) point.
    ! dr_dpsi   : real(8),dimension(3)
    !   Intervalley transer rate derivative with psi at left (1) mid (2) and right (3) point.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in)                   :: is,index_sp
    real(8),intent(in)                   :: n,n2,dn_dphin,dn_dpsi,dn2_dphin2,dn2_dpsi,phin,phin2,phin_p,phin2_p,dx_p,phin_m,phin2_m,dx_m,psi,psi_m,psi_p
    real(8),intent(out)                  :: r       
    real(8),dimension(3),   intent(out)  :: dr_dpsi
    real(8),dimension(3),   intent(out)  :: dr_dphin
    real(8),dimension(3),   intent(out)  :: dr_dphin2
    real(8),dimension(3),   intent(out)  :: zeta_dqf,zeta_inv_dqf
    real(8),intent(out) :: zeta,zeta_inv

    integer :: k
    real(8) :: zeta_df

    real(8) :: dr_dn, dr_dn2

    TYPE(DUAL_NUM) :: result
    type(rec_model),pointer :: rec, rec_A, rec_B
    integer         :: id_materialA = 0
    integer         :: id_materialB = 0

    r            = 0
    dr_dphin     = 0
    dr_dphin2    = 0
    dr_dpsi      = 0
    zeta         = 0
    zeta_inv     = 0
    zeta_dqf     = 0
    zeta_inv_dqf = 0

    do k=1,size(SEMI(is)%rec_models)
        rec => SEMI(is)%rec_models(k)
        !take correct model for given semiconductor
        if (.not.rec%is_intervalley) then
            cycle
        endif

        !bowing
        if (SEMI(is)%is_alloy) then
            id_materialA     = SEMI(is)%id_materialA
            id_materialB     = SEMI(is)%id_materialB
            rec_A            => SEMI(id_materialA)%rec_models(k)
            rec_B            => SEMI(id_materialB)%rec_models(k)
            ! bowing parameters for alloy not yet implemented => take them as zero
            rec%f0       = bow(rec_A%f0      , rec_B%f0      , dble(0), GRADING(index_sp))
            rec%zeta_dop = bow(rec_A%zeta_dop, rec_B%zeta_dop, dble(0), GRADING(index_sp))
            rec%dop_crit = bow(rec_A%dop_crit, rec_B%dop_crit, dble(0), GRADING(index_sp))
            rec%a        = bow(rec_A%a       , rec_B%a       , dble(0), GRADING(index_sp))
            rec%af       = bow(rec_A%af      , rec_B%af      , dble(0), GRADING(index_sp))
            rec%v_21     = bow(rec_A%v_21    , rec_B%v_21    , dble(0), GRADING(index_sp))
            rec%dv_21    = bow(rec_A%dv_21   , rec_B%dv_21   , dble(0), GRADING(index_sp))

        endif

        result       = get_intervalley_rates( &
            & index_sp, &
            & is, &
            & rec, &
            & DUAL_NUM(n,(/1,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(n2,(/0,1,0,0,0,0,0,0/)), &
            & DUAL_NUM(psi,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin,(/0,0,0,1,0,0,0,0/)), &
            & DUAL_NUM(phin2,(/0,0,0,0,0,0,1,0/)), &
            & DUAL_NUM(psi_m,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin_m,(/0,0,1,0,0,0,0,0/)), &
            & DUAL_NUM(phin2_m,(/0,0,0,0,0,1,0,0/)), &
            & dx_m, &
            & DUAL_NUM(psi_p,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin_p,(/0,0,0,0,1,0,0,0/)), &
            & DUAL_NUM(phin2_p,(/0,0,0,0,0,0,0,1/)), &
            & dx_p &
        & )
        r            = result%x_ad_
        dr_dn        = result%xp_ad_(1)
        dr_dn2       = result%xp_ad_(2)
        dr_dphin(1)  = result%xp_ad_(3)
        dr_dphin(2)  = result%xp_ad_(4) + dr_dn*dn_dphin
        dr_dphin(3)  = result%xp_ad_(5)
        dr_dphin2(1) = result%xp_ad_(6)
        dr_dphin2(2) = result%xp_ad_(7) + dr_dn2*dn2_dphin2
        dr_dphin2(3) = result%xp_ad_(8)

        result       = get_intervalley_rates( &
            & index_sp,  &
            & is, &
            & rec,  &
            & DUAL_NUM(n,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(n2,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(psi,(/0,1,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin2,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(psi_m,(/1,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin_m,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin2_m,(/0,0,0,0,0,0,0,0/)), &
            & dx_m, &
            & DUAL_NUM(psi_p,(/0,0,1,0,0,0,0,0/)), &
            & DUAL_NUM(phin_p,(/0,0,0,0,0,0,0,0/)), &
            & DUAL_NUM(phin2_p,(/0,0,0,0,0,0,0,0/)), &
            & dx_p &
        & )
        dr_dpsi(1)   = result%xp_ad_(1)
        dr_dpsi(2)   = result%xp_ad_(2) + dr_dn*dn_dpsi + dr_dn2*dn2_dpsi
        dr_dpsi(3)   = result%xp_ad_(3)

        !dr_dpsi(1)  = 0
        !dr_dpsi(2)  = 0
        !dr_dpsi(3)  = 0
        ! dr_dpsi  = -dr_dpsi

        ! dr_dphin2(1) = 0
        ! ! dr_dphin2(2) = dr_dn2*dn2_dphin2
        ! dr_dphin2(3) = 0

        ! zeta berechnungen 
        zeta         = 0
        zeta_df      = 0
        !zeta_df      = 0
        zeta_dqf(1)  = 0!zeta_df*df_dphi_left
        zeta_dqf(2)  = 0!zeta_df*df_dphi_mid
        zeta_dqf(3)  = 0!zeta_df*df_dphi_right
        ! zeta invers für anderes band
        zeta_inv_dqf(1)  = 0!zeta_inv_df*df_dphi_left
        zeta_inv_dqf(2)  = 0!zeta_inv_df*df_dphi_mid
        zeta_inv_dqf(3)  = 0!zeta_inv_df*df_dphi_right
    enddo

    ! take care of norm
    r         = r        * NORM%R_N_inv
    dr_dphin  = dr_dphin * NORM%R_N_inv
    dr_dphin2 = dr_dphin2* NORM%R_N_inv
    dr_dpsi   = dr_dpsi  * NORM%R_N_inv

endsubroutine

function get_intervalley_rates(index_sp, is,rec, n, n2, psi, phin, phin2, psi_m, phin_m, phin2_m, dx_m, psi_p, phin_p, phin2_p, dx_p) result(r)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the energy dependent intervalley rate, see paper by Markus for details.
    ! 
    ! input 
    ! ------
    ! index_sp: integer
    !   Index to point in semiconductor.
    ! rec     : rec_model
    !   Recombination model parameters.
    ! n       : DUAL_NUM
    !   Carrier density at point mid.
    ! n2      : DUAL_NUM
    !   Carrier 2 density at point mid.
    ! psi     : DUAL_NUM
    !   Electrostatic potential at point mid.
    ! phin    : DUAL_NUM
    !   Quasi Fermi potential electrons at point mid.
    ! phin2   : DUAL_NUM
    !   Quasi Fermi potential electrons 2 at point mid.
    ! psi_m   : DUAL_NUM
    !   Electrostatic potential at point left.
    ! phin_m  : DUAL_NUM
    !   Quasi Fermi potential electrons at point left.
    ! phin2_m : DUAL_NUM
    !   Quasi Fermi potential electrons 2 at point left.
    ! dx_m    : real(8)
    !   Distance left to mid point.
    ! psi_p   : DUAL_NUM
    !   Electrostatic potential at point right.
    ! phin_p  : DUAL_NUM
    !   Quasi Fermi potential electrons at point right.
    ! phin2_p : DUAL_NUM
    !   Quasi Fermi potential electrons 2 at point right.
    ! dx_p    : real(8)
    !   Distance mid to right point.
    ! 
    ! output 
    ! ------
    ! r : DUAL_NUM
    !   Intervalley recombination rate.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer index_sp, is, ib
    TYPE(DUAL_NUM)::n,n2,phin,phin2,phin_m,phin2_m,phin_p,phin2_p,r,psi,psi_m,psi_p
    real(8)::dx_m,dx_p
    TYPE(DUAL_NUM)  :: f_right,f_left = DUAL_NUM(0,0)
    TYPE(DUAL_NUM)  :: f_n
    TYPE(DUAL_NUM)  :: f_n2
    TYPE(DUAL_NUM)  :: f_E
    TYPE(DUAL_NUM)  :: v_12
    TYPE(DUAL_NUM)  :: arg
    real(8)         :: f_crit
    logical         :: in_region,left,right
    type(rec_model) :: rec
    TYPE(DUAL_NUM)  :: n1_equ, n2_equ, v_12_equ, deltaef

    left = (X(index_sp).lt.rec%xl)
    right = (X(index_sp).gt.rec%xr)
    in_region = .not.(left.or.right)

    if (left) then !left of region no rec
        r = 0
        return
    endif

    !field phiG
    if (dx_p.gt.0) then
        f_right             =  abs(  phin_p - phin        )/dx_p
    endif
    if (dx_m.gt.0) then
        f_left             =  abs(  phin        - phin_m  )/dx_m
    endif
    f_n = (f_right * dx_p + f_left * dx_m) / (dx_p + dx_m)
    f_n = f_right

    !field phiL
    if (dx_p.gt.0) then
        f_right             =  abs(  phin2_p - phin2        )/dx_p
    endif
    if (dx_m.gt.0) then
        f_left             =  abs(  phin2    - phin2_m  )/dx_m
    endif
    f_n2 = (f_right * dx_p + f_left * dx_m) / (dx_p + dx_m)

    !electric field
    if (dx_p.gt.0) then
        f_right            =  abs(  psi_p - psi        )/dx_p
    endif
    if (dx_m.gt.0) then
        f_left             =  abs(  psi        - psi_m  )/dx_m
    endif
    f_E = (f_right * dx_p + f_left * dx_m) / (dx_p + dx_m)

    if (rec%force_is_phi) then
        f_n  = abs(f_n)
        f_n2 = abs(f_n2)
    elseif (rec%force_is_psi) then
        f_n  = abs(f_E)
        f_n2 = 0
    elseif (rec%force_is_mix) then
        if ((f_E.eq.0).or.(f_n.eq.0)) then
            f_n = 0
        else
            f_n  = sqrt(abs(f_n*f_E))
        endif
        if ((f_E.eq.0).or.(f_n2.eq.0)) then
            f_n2 = 0
        else
            f_n2  = sqrt(abs(f_n2*f_E))
        endif
    endif

    f_n  = f_n*NORM%l_N_inv
    f_n2 = f_n2*NORM%l_N_inv

    ! model without electron temperature
    f_crit = rec%f0*( 1+ (CDTOT(index_sp)*NORM%N_N/rec%dop_crit)**rec%zeta_dop )

    f_n  = get_nl_field(f_n,index_sp)
    f_n  = f_n*US%nonlocal%ai

    !can be used to aid convergence
    f_n2  = get_nl_field(f_n2,index_sp)
    ! f_n2 = 0 

    ! calculate equilibrium G to L rate
    ! for boltzmann n1/n2 is independent of PSI and always equal for Ef1=Ef2
    ! not for Fermi! Here n1/n2 is a function of psi and Ef1-Ef2
    ! r=0 for Ef1=Ef2 for all Psi
    ! => offset must be dependent on psi and Ef1=Ef2=EfG
    ib          = CONT_ELEC%valley
    deltaef     = - phin- (-psi - UN(index_sp)) ! E_f-E_fi
    ! if (.not.rec%v21off) then 
        n1_equ      = get_n_equ_band_dual(is,ib,deltaef,DUAL_NUM(VT_LATTICE,(/0,0,0,0,0,0,0,0/)),index_sp)
    ! else
    !     n1_equ      = CNN0(index_sp)
    ! endif
    ib          = CONT_ELEC2%valley
    deltaef     = - phin - (-psi - UN2(index_sp)) ! E_f-E_fi
    ! if (.not.rec%v21off) then 
        n2_equ      = get_n_equ_band_dual(is,ib,deltaef,DUAL_NUM(VT_LATTICE,(/0,0,0,0,0,0,0,0/)),index_sp)
    ! else
    !     n2_equ      = CNN20(index_sp)
    ! endif
    v_12_equ = n2_equ/n1_equ * rec%v_21

    ! zero the rate
    r = 0

    ! G to L valley:
    !softplus function
    arg         = (f_n-f_crit)/1e5/rec%af
    v_12        = v_12_equ + rec%a* ( log( 1+exp(-abs(arg))) + max(arg,0) ) !numerically stable softplus implementation
    !to ensure zero at zero field, substract bias dependent term for field=0
    arg         = (0-f_crit)/1e5/rec%af
    v_12        = v_12 - rec%a* ( log( 1+exp(-abs(arg))) + max(arg,0) ) !numerically stable softplus implementation
    r           = -n*v_12

    ! ! G to L valley:
    ! !to ensure zero at zero field, substract bias dependent term
    ! arg         = (0-f_crit)/1e5/rec%af
    ! v_120       = rec%a* ( log( 1+exp(-abs(arg))) + max(arg,0) ) !numerically stable softplus implementation
    ! deltaef     = v_12_equ/v_120
    ! !softplus function
    ! arg         = (f_n-f_crit)/1e5/rec%af
    ! v_12        = deltaef*rec%a* ( log( 1+exp(-abs(arg))) + max(arg,0) ) !numerically stable softplus implementation
    ! r   = (-n)*v_12

    ! L to G valley:
    if (in_region.and.rec%v21off) then
        r = r
    else 
        r = r + n2* ( rec%v_21+rec%dv_21*f_n2/1e5 )
    endif

    !normalize
    r   = r*NORM%N_N
endfunction

function get_auger_rate(rec,ni_sq_i, n, p,temp, graded ) result(r)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the auger recombination rate.
    ! 
    ! input
    ! -----
    ! rec    : rec_model
    !   Recombination model parameters.
    ! ni_sq_i: DUAL_NUM
    !   Squared intrinsic carrier density at point.
    ! n      : DUAL_NUM
    !   Electron carrier density at point.
    ! p      : DUAL_NUM
    !   Hole carrier density at point.
    ! temp   : DUAL_NUM
    !   Lattice temperature at point.
    ! graded : real(8)
    !   Grading at point.
    !
    ! output
    ! ------
    ! r : DUAL_NUM
    !   Auger recombination rate.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(rec_model) :: rec
    TYPE(DUAL_NUM)  :: n,p
    TYPE(DUAL_NUM)  :: r
    TYPE(DUAL_NUM)  :: np_min_ni,temp,c_n,c_p
    real(8)         :: graded,ni_sq_i

    call auger_coefficients(rec, graded, temp, c_n, c_p)
    np_min_ni = n*p-ni_sq_i
    r         = np_min_ni*(c_n*n+c_p*p)
endfunction

function get_avalanche_rate(isp,is,rec, n, p, temp, phin, phip, tn, phin_m, phip_m, dx_m, phin_p, phip_p, dx_p) result(r)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the avalanche recombination rate.
    ! 
    ! input
    ! -----
    ! isp   : integer
    !   Index to point in semiconductor.
    ! is    : integer
    !   Index to semiconductor model.
    ! rec   : rec_model
    !   Recombination model parameters.
    ! n     : DUAL_NUM
    !   Electron carrier density.
    ! p     : DUAL_NUM
    !   Hole carrier density.
    ! temp  : DUAL_NUM
    !   Lattice temperature at point.
    ! phin  : DUAL_NUM
    !   Quasi Fermi potential electrons at point.
    ! phip  : DUAL_NUM
    !   Quasi Fermi potential holes at point.
    ! tn  : DUAL_NUM
    !   Electron temperature at poin.
    ! phin_m: DUAL_NUM
    !   Quasi Fermi potential electrons at left point.
    ! phip_m: DUAL_NUM
    !   Quasi Fermi potential holes at left point.
    ! dx_m  : real(8)
    !   Distance left to mid point.
    ! phin_p: DUAL_NUM
    !   Quasi Fermi potential electrons at right point.
    ! phip_p: DUAL_NUM
    !   Quasi Fermi potential holes at right point.
    ! dx_p  : real(8)
    !   Distance mid to right point.
    !
    ! output
    ! ------
    ! r  : DUAL_NUM
    !   Avalanche recombination rate.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer           is
    integer           isp
    integer           ib
    TYPE(DUAL_NUM)  :: n,p,phin,phip,phin_m,phip_m,phin_p,phip_p,tn
    TYPE(DUAL_NUM)  :: r
    TYPE(DUAL_NUM)  :: temp
    real(8)         :: dx_m, dx_p
    TYPE(DUAL_NUM)  :: vsn, vsp
    TYPE(DUAL_NUM)  :: aln= DUAL_NUM(0,(/0,0,0,0,0,0,0,0/)), alp = DUAL_NUM(0,(/0,0,0,0,0,0,0,0/))
    TYPE(DUAL_NUM)  :: fn_right= DUAL_NUM(0,(/0,0,0,0,0,0,0,0/)),fn_left = DUAL_NUM(0,0)
    TYPE(DUAL_NUM)  :: fp_right= DUAL_NUM(0,(/0,0,0,0,0,0,0,0/)),fp_left = DUAL_NUM(0,0)
    TYPE(DUAL_NUM)  :: f_n,f_p,tau
    type(rec_model) :: rec

    !calculate local fields left and right from point
    if ((dx_p.eq.dble(0)).and.(dx_m.eq.dble(0))) then
        write(*,'(A)') '***error*** both lengths equal zero.'
        stop
    endif
    if (dx_p.gt.0) then
        fn_right            =  abs(  phin_p - phin        )/dx_p
        fp_right            =  abs(  phip_p - phip        )/dx_p
    endif
    if (dx_m.gt.0) then
        fn_left             =  abs(  phin        - phin_m  )/dx_m
        fp_left             =  abs(  phip        - phip_m  )/dx_m
    endif

    !saturation velocities
    ib       = get_conduction_band_index(SEMI(is))
    vsn      = get_vsat(is,ib,temp/SEMI(is)%temp0,GRADING(isp),ACCEPTORS(is),DONATORS(is))
    ib       = get_valence_band_index(SEMI(is))
    vsp      = get_vsat(is,ib,temp/SEMI(is)%temp0,GRADING(isp),ACCEPTORS(is),DONATORS(is))

    ! electron field for avalache calculation
    if (DD_TN) then
        ib  = get_conduction_band_index(SEMI(is))
        tau = get_taun_dual(is, ib, tn, tn, temp, GRADING(isp))
        f_n = dble(1.5)*BK/Q/tau*( tn - temp )/vsn
    else
        ! using average field
        f_n = (fn_right             * dx_p + fn_left            *dx_m) / (dx_p + dx_m)
        ! normalization
        f_n = f_n*NORM%l_N_inv
        ! non local field
        f_n = get_nl_field(f_n,isp)
    endif

    ! hole field for avalache calculation using average field
    f_p = (fp_right             * dx_p + fp_left            *dx_m) / (dx_p + dx_m)
    ! normalization
    f_p = f_p*NORM%l_N_inv
    !f_p = 0

    !actual model
    aln = 0
    if (.not.(f_n.EQ.dble(0))) then
        aln = rec%alpha_n*exp(-(rec%e_crit_n/abs(f_n))**rec%beta_n)
    endif

    alp = 0
    if (.not.(f_p.EQ.dble(0))) then
        alp = rec%alpha_p*exp(-(rec%e_crit_p/abs(f_p))**rec%beta_p)
    endif

    !calculation of recombination rate
    r      =  -(aln*n*vsn + alp*p*vsp)

endfunction

function get_srh_rate(isp,is,ni_sq_i,don,acc,graded,hetero_id,rec, n, p, temp) result(r)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate the Shockley-Read-Hall recombination rate.
    ! 
    ! input
    ! -----
    ! isp: integer
    !   Index to point in semiconductor.
    ! is: integer
    !   Index to semiconductor model.
    ! ni_sq_i: real(8)
    !   Squared intrinsic carrier density.
    ! don: real(8)
    !   Donor density.
    ! acc: real(8)
    !   Acc density.
    ! graded: real(8)
    !   Grading at point.
    ! hetero_id: real(8)
    ! If effective mass poisiton dependent this is the same as isp, else = 0.
    ! rec: rec_model
    !   Recombination model parameters.
    ! n: DUAL_NUM
    !   Electron carrier density.
    ! p: DUAL_NUM
    !   Hole carrier density.
    ! temp: DUAL_NUM
    !   Lattice temperature at point.
    !
    ! output
    ! ------
    ! r  : DUAL_NUM
    !   SRH recombination rate.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer           is  !index to semi
    integer           isp !index to sp
    integer           ib !index to sp
    integer           hetero_id !index to sp
    TYPE(DUAL_NUM)  :: n,p
    TYPE(DUAL_NUM)  :: r
    TYPE(DUAL_NUM)  :: np_min_ni,temp,tau_n,tau_p
    real(8)         :: don,acc,graded,nt,pt,ni_sq_i
    type(rec_model) :: rec

    !lifetimes
    if (rec%lifetime_is_device) then
        call lifetimes_device(rec, don, acc, graded, temp, tau_n, tau_p)
    elseif (rec%lifetime_is_default) then
        call lifetimes_default(rec, isp, temp, tau_n, tau_p)
    else
        write(*,*) '***error*** srh lifetime model not recognized'
        stop
    endif

    !calculate trap density
    ib = get_conduction_band_index(SEMI(is))
    nt = get_n_equ_band_dual(is,ib,rec%e_trap,BK*TEMP_LATTICE/Q,hetero_id)
    ib = get_valence_band_index(SEMI(is))
    pt = get_n_equ_band_dual(is,ib,rec%e_trap,BK*TEMP_LATTICE/Q,hetero_id)
    ! nt = 1e15
    ! pt = 1e15

    np_min_ni = n*p-ni_sq_i

    !rate
    r  = np_min_ni / (tau_n* (p+pt) +tau_p*(n + nt))

endfunction

subroutine lifetimes_device(rec, don, acc, graded, temp, tau_n , tau_p)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! CALCULATE SRH volume recombination lifetimes using Device function.
  !
  ! input
  ! -----
  ! rec   : rec_model
  !   Recombination model parameters.
  ! don   : real(8)
  !   Donor density at point.
  ! acc   : real(8)
  !   Acceptor density at point.
  ! graded: real(8)
  !   Grading at point.
  ! temp  : DUAL_NUM
  !   Temperature at point.
  ! tau_n : DUAL_NUM
  !   Electron lifetime at point.
  ! tau_p : DUAL_NUM
  !   Electron lifetime at point.
  !
  ! output
  ! ------
  ! tau_n, tau_p
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real(8), intent(in)    :: don,acc,graded
  type(DUAL_NUM), intent(inout) :: tau_n,tau_p
  type(rec_model)        :: rec
  type(DUAL_NUM)         :: temp,T_Tmat

  real(8) :: t_CD_n,t_CD_p,a_2,n_tot
  real(8) :: tau_n_max, tau_p_max
  real(8) :: tau_n_min, tau_p_min

  T_Tmat = temp/SEMI(rec%semi_id)%temp0

  tau_n_min = rec%tau_n_min
  tau_p_min = rec%tau_p_min
  tau_n_max = rec%tau_n_max
  tau_p_max = rec%tau_p_max
  if (graded.GT.1.0e-5) then
      a_2       = Graded*Graded
      tau_n_min = tau_n_min + rec%tau_n_min_a1*Graded + rec%tau_n_min_a2*a_2
      tau_p_min = tau_p_min + rec%tau_p_min_a1*Graded + rec%tau_p_min_a2*a_2
      tau_n_max = tau_n_max + rec%tau_n_max_a1*Graded + rec%tau_n_max_a2*a_2
      tau_p_max = tau_p_max + rec%tau_p_max_a1*Graded + rec%tau_p_max_a2*a_2
  endif
    
  !## doping dependence
  n_tot  = abs(acc) + abs(don)
  t_CD_n = tau_n_min+(tau_n_max-tau_n_min)/(1.0+(n_tot/rec%c_ref_n)**rec%beta_n)
  t_CD_p = tau_p_min+(tau_p_max-tau_p_min)/(1.0+(n_tot/rec%c_ref_p)**rec%beta_p)

  !## temperature dependence
  tau_n = t_CD_n*T_Tmat**rec%zeta_n
  tau_p = t_CD_p*T_Tmat**rec%zeta_p

endsubroutine

subroutine lifetimes_default(rec, sp_id, temp, tau_n , tau_p)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! CALCULATE SRH volume recombination lifetimes using default model
    ! 
    ! input 
    ! -----
    ! rec   : rec_model
    !   Recombination model parameters.
    ! sp_id : integer
    !   Id if point in semiconductor.
    ! temp  : DUAL_NUM
    !   Lattice temperature at point.
    !
    ! output
    ! ------
    ! tau_n,  tau_p: DUAL_NUM
    !   Electron and hole lifetimes.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(DUAL_NUM), intent(inout) :: tau_n,tau_p
    type(rec_model)        :: rec
    integer                :: sp_id
    type(DUAL_NUM)         :: temp

    integer :: index
    real(8) :: v_n, v_p
    real(8) :: m_n, m_p
    real(8) :: tau_n_300, tau_p_300
    type(DUAL_NUM) :: T_Tmat

    T_Tmat = temp/SEMI(rec%semi_id)%temp0

    index     = get_conduction_band_index(SEMI(rec%semi_id))
    m_n       = SEMI(rec%semi_id)%band_pointer(index)%band%pos_dep_m_eff(sp_id)
    index     = get_valence_band_index(SEMI(rec%semi_id))
    m_p       = SEMI(rec%semi_id)%band_pointer(index)%band%pos_dep_m_eff(sp_id)
    v_n       = sqrt(3*300*BK/m_n)
    v_p       = sqrt(3*300*BK/m_p)

    tau_n_300 = 1/(rec%sigma_n*rec%n_t*v_n)
    tau_p_300 = 1/(rec%sigma_p*rec%n_t*v_p)

    tau_n     = tau_n_300*T_Tmat**rec%zeta_n
    tau_p     = tau_p_300*T_Tmat**rec%zeta_p

endsubroutine

subroutine auger_coefficients(rec, graded, temp, c_n , c_p)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! calculate the auger coefficients.
  !
  ! input
  ! -----
  ! rec   : rec_model
  !   Recombination model parameters.
  ! don   : real(8)
  !   Donor density at point.
  ! acc   : real(8)
  !   Acceptor density at point.
  ! graded: real(8)
  !   Grading at point.
  ! temp  : DUAL_NUM
  !   Temperature at point.
  !
  ! output
  ! ------
  ! c_n, c_p : DUAL_NUM
  !   Electron and hole auger coefficients.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real(8), intent(in)    :: graded
  type(DUAL_NUM)         :: temp
  type(DUAL_NUM), intent(inout) :: c_n,c_p
  type(rec_model)        :: rec

  type(DUAL_NUM) :: T_Tmat,d,a_2

  T_Tmat = temp/SEMI(rec%semi_id)%temp0

  c_n = rec%c_n
  c_p = rec%c_p
  if (graded.GT.1.0e-5) then
      a_2 = Graded*Graded
      c_n = c_n    + rec%c_n_a1  *Graded + rec%c_n_a2  * a_2
      c_p = c_p    + rec%c_p_a1  *Graded + rec%c_p_a2  * a_2
  endif
    
  !## temperature dependence
  d     = log(T_Tmat)

  c_n = c_n*exp(rec%zeta_n*d)
  c_p = c_p*exp(rec%zeta_p*d)

endsubroutine

endmodule
