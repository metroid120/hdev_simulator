!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! main hdev routine
program hdev

use read_user  , only : init_user         !< read user parameter

use save_global, only : TCPU_START        !< time at start of simulation
use save_global, only : welcome_screen    !< welcome screen

use simulation , only : init_simulation   !< init simulation module
use simulation , only : sim_loop          !< simulation loop
use simulation , only : simulation_end    !< exit simulation module


implicit none


real(8) :: tcpu2=0 
integer :: count1=0
integer :: count2=0
integer :: count_rate=0


! --> track CPU time
call cpu_time(TCPU_START)
call system_clock(count1,count_rate)

! --> welcome screen
call welcome_screen

! --> init user parameters
write(*,*) 'read user input'
call init_user

! --> initialize simulation parameters
call init_simulation

! --> perform operation point & time loop
call sim_loop

! --> deallocate fields
call simulation_end

! --> track CPU time
call cpu_time(tcpu2)
call system_clock(count2,count_rate)

! --> output CPU time
write(*,*) 'total    cpu time: ',tcpu2-TCPU_START,'s'
write(*,*) 'total system time: ',dble(count2-count1)/dble(count_rate),'s'

call exit

endprogram
