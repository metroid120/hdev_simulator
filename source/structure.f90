!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! definition of device structure
module structure

use phys_const
use math_oper  , only : init_grid
use math_oper  , only : range_grid
use math_oper  , only : area_grid
use math_oper  , only : add_point
use math_oper  , only : add_point_int

use read_user  , only : US

use save_global, only : DEB_ID
use save_global, only : FILE
use save_global, only : FORMAT_REAL
use save_global, only : SAVE_OP
use save_global, only : get_file_id
use save_global, only : format_int
use hdf_functions, only : read_col_hdf5

use utils, only : REMAP_VEC

implicit none

! ---> BOX structure type ---------------------------------------------------------------------------------------------
type struc_box
  character(len=64)    :: mod_name   !< model name
  character(len=64)    :: type       !< boxtype
  real(8)              :: eps        !< permittivity
  real(8)              :: kappa      !< thermal conductivity
  real(8),dimension(3) :: low_xyz    !< lower box corner
  real(8),dimension(3) :: upp_xyz    !< upper box corner
  character(len=64)    :: cont_name  !< contact name
  character(len=64)    :: shape      !< shape
  logical              :: diri       !< if true: dirichlet contact
  integer              :: layer      !< layer_number
  integer              :: semi_id    !< semiconductor id
  integer              :: supply_id  !< supply_contact id
  integer              :: oxid_id    !< oxide id
  integer              :: cont_id    !< contact id
  integer              :: model_id   !< model id
endtype


! ---> POINT structure type -------------------------------------------------------------------------------------------
type struc_point
  integer :: box_id      = 1        !< index to BOX
  integer :: sp_idx      = 0        !< index to SP
  integer :: id          = 0        !< index to number of this POINT
  real   (8) :: eps      = 0        !< permittivity
  real   (8) :: kappa    = 0        !< thermal conductance
  logical :: diri        = .false.  !< if true: dirichlet contact
  logical :: cont        = .false.  !< if true: is contact
  integer :: cont_id     = 0        !< index to contact model CON
  logical :: cont_b      = .false.  !< if true: is contact boundary
  logical :: cont_b_xlow = .false.  !< if true: is contact lower x boundary
  logical :: cont_b_xupp = .false.  !< if true: is contact upper x boundary
  logical :: cont_b_ylow = .false.  !< if true: is contact lower y boundary
  logical :: cont_b_yupp = .false.  !< if true: is contact upper y boundary
  logical :: cont_b_zlow = .false.  !< if true: is contact lower z boundary
  logical :: cont_b_zupp = .false.  !< if true: is contact upper z boundary
  logical :: oxid        = .false.  !< if true: is oxide
  logical :: semi        = .false.  !< if true: is semiconductor
  integer :: semi_id     = 0        !< index to semiconductor model SEMI
  integer :: xline_id    = 0        !<
  logical :: semi_cont   = .false.  !< semi-contact boundary
  integer :: semi_box    = 0        !< index to semi box
  logical :: supply      = .false.  !< if true: is supply contact boundary
  integer :: supply_id   = 0        !< if true: is supply contact with id supp_id
endtype

! --- global module parameters ----------------------------------------------------------------------------------------
type(struc_box),dimension(:),pointer        :: BOX           !< properties of boxes
integer                                     :: N_BOX         !< number of boxes
type(struc_point),dimension(:,:,:), pointer :: POINT => null() !< (NX1,NY1,NZ1) parameter for each disc. point
integer                                     :: STRUC_DIM     !< dimension for poisson equation
logical                                     :: CART          !< if true: cartesian coordinate system
logical                                     :: BULK          !< if true: bulk simulation
real(8),dimension(:),pointer                :: X             !< (NX1) x-grid
real(8),dimension(:),pointer                :: Y             !< (NY1) y-grid
real(8),dimension(:),pointer                :: Z             !< (NZ1) z-grid
real(8),dimension(:),pointer                :: DX            !< (NX) dx-grid
real(8),dimension(:),pointer                :: DY            !< (NY) dy-grid
real(8),dimension(:),pointer                :: DZ            !< (NZ) dz-grid
integer                                     :: NX            !< number of x-boxes
integer                                     :: NX1           !< number of x-points
integer                                     :: NY            !< number of y-boxes
integer                                     :: NY1           !< number of y-points
integer                                     :: NZ            !< number of z-boxes
integer                                     :: NZ1           !< number of z-points
integer                                     :: NXYZ          !< total number of grid points
character(len=64),dimension(:), pointer     :: SEMI_MOD_NAME !< model name for semiconductors
integer                                     :: N_SEMI        !< number of semiconductor models
integer                                     :: N_CON         !< number of contacts


private

public struc_box
public struc_point

public BOX
public N_BOX
public POINT
public STRUC_DIM
public CART
public BULK
public X
public Y
public Z
public DX
public DY
public DZ
public NX1
public NY1
public NZ1
public NXYZ
public NX
public NY
public NZ
public SEMI_MOD_NAME
public N_SEMI

public init_structure
public normalize_structure
public close_structure
public update_disc

contains

subroutine init_structure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialization of structure module
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j,k

    ! --> nullify pointers
    nullify(X,Y,Z,DX,DY,DZ)
    nullify(BOX)
    nullify(POINT) 
    nullify(SEMI_MOD_NAME)

    N_CON = 0

    STRUC_DIM = US%regioninfo%spat_dim
    if (STRUC_DIM.eq.0) then
        write(*,*) '***error*** struc_dim equals zero. Not possible. Check &REGION_INFO spat_dim.'
        stop
    endif
    CART      = .TRUE.
    call init_device

    ! --> bulk simulation/device test 
    BULK = (STRUC_DIM.eq.0)            
    if (.not.BULK) call check_device

    ! --> user grid (add points to grid, if already automatic grid)
    if (STRUC_DIM.ge.1) then 
        call get_disc('x') ! --> get x-grid points
    endif
    if (STRUC_DIM.ge.2) then 
        call get_disc('y') ! --> get y-grid points
    endif
    if (STRUC_DIM.ge.3) then 
        call get_disc('z') ! --> get z-grid points
    endif

    ! --> add box borders to grid
    ! call add_box_border_to_grid

    ! --> check for errors
    call finish_disc

    ! --> output number of disc points
    write(*,*) ''
    write(*,*) 'x-points: ',NX1
    write(*,*) 'y-points: ',NY1
    write(*,*) 'z-points: ',NZ1
    write(*,*) 'total:    ',NXYZ
    write(*,*) ''

    ! --> limit maximum discretization points
    ! if (NXYZ.gt.US%regioninfo%pnts_max) then
    !   write(*,*) '***error*** too many discretization points (larger then ',US%regioninfo%pnts_max,')'
    !   stop
    ! endif

    ! --> calculate difference vector
    call diff_disc

    ! --> test discretization
    if (.not.BULK) call check_disc

    ! --> init point structure from box information
    call init_point_struc


    ! --> calculate number of suppply points
    n = 0
    do i=1,NX1
        do j=1,NY1
            do k=1,NZ1
                if (POINT(i,j,k)%supply) n = n +1
            enddo
        enddo
    enddo

    write(*,*) ''
    write(*,*) 'supply-points: ',n
    write(*,*) ''

    ! --> find alle semiconductor models needed for simulation
    call set_semi_model

endsubroutine

subroutine update_disc(add_x_pnts)
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! update discretization
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real(8),dimension(:),intent(in) :: add_x_pnts

  integer :: i

  ! --> add points to X-vector
  do i=1,size(add_x_pnts)
      call add_point(X,NX1,add_x_pnts(i))
  enddo

  ! --> new diff x vector
  deallocate(DX)
  nullify(DX)
  NX      = NX1 - 1
  allocate(DX(NX))
  do i=1,NX
      DX(i) = X(i+1)-X(i)
  enddo

  ! --> new point struct
  deallocate(POINT)
  nullify(POINT)
  call init_point_struc
endsubroutine

subroutine init_box
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialization of box structure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i

    ! --> allocate BOX data structure
    if (N_BOX.ge.0) then
      allocate(BOX(N_BOX))

    else
      write(*,*) '***error*** N_BOX negative'
      stop

    endif

    ! --> set default values of BOX data structure
    do i=1,N_BOX
        BOX(i)%mod_name   = ''
        BOX(i)%type       = ''
        BOX(i)%eps        = dble(1)
        BOX(i)%kappa      = dble(1)
        BOX(i)%low_xyz    = dble(0)
        BOX(i)%upp_xyz    = dble(0)
        BOX(i)%cont_name  = ''
        BOX(i)%shape      = 'rect'
        BOX(i)%diri       = .false.
        BOX(i)%layer      = 0
        BOX(i)%semi_id    = 0
        BOX(i)%oxid_id    = 0
        BOX(i)%cont_id    = 0
        BOX(i)%model_id   = 0
        BOX(i)%supply_id  = 0
    enddo

endsubroutine

subroutine init_device
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialization of internal device structure from user input
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer           :: n,n_cont,n_oxid,n_semi,j,i,n_supply
    logical           :: model_found
    character(len=64) :: mod_name

    ! --> init BOX structure
    N_BOX = US%n_regiondef + 1
    call init_box

    ! --> read in boxes
    n        = 0 ! total number of boxes
    n_oxid   = 0 ! number of oxide boxes
    n_cont   = 0 ! number of contact boxes
    n_semi   = 0 ! number of semiconductor boxes
    n_supply = 0 ! number of semiconductor boxes

    ! --> AIR background box
    mod_name    = 'air'
    model_found = .false.
    do j=US%n_oxide,1,-1
        if (US%oxide(j)%mod_name.eq.mod_name) then
            model_found      = .true.
            n                = n      + 1
            n_oxid           = n_oxid + 1
            BOX(n)%mod_name  = mod_name
            BOX(n)%type      = 'oxid'
            BOX(n)%eps       = US%oxide(j)%eps
            BOX(n)%kappa     = US%oxide(j)%kappa
            BOX(n)%shape     = 'rect'
            BOX(n)%low_xyz   = 0
            BOX(n)%upp_xyz   = 0
            BOX(n)%cont_name = ''
            BOX(n)%diri      = .false.
            BOX(n)%oxid_id   = n_oxid
            BOX(n)%model_id  = j
            BOX(n)%layer     = 0
            exit
        endif
    enddo
    if (.not.model_found) then
        write(*,*) '***error*** OXIDE model ',trim(mod_name),' not found'
        stop
    endif

    ! --> other boxes
    do i=1,US%n_regiondef
        ! --> OXIDE -------------------
        call add_oxide_box(i,n,n_oxid)

        ! --> SEMICONDUCTOR -----------
        call add_semi_box(i,n,n_semi)

        ! --> CONTACT -----------------
        call add_contact_box(i,n)

        ! --> SUPPLY -----------------
        call add_supply_box(i,n,n_supply)
    enddo

    ! --> check if size of BOX is correct
    if (N_BOX.ne.n) then
        write(*,*) '***error*** initialization of structure failed, some &REGION_DEF lines are invalid'
        stop
    endif

endsubroutine

subroutine add_contact_box(i,n)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! add contact box to device structure
    !
    ! input
    ! -----
    ! i : integer
    !   index to regiondef of contact regiondef.
    !
    ! output
    ! ------
    ! n : integer  
    !   number of boxes (?)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in   ) :: i
    integer,intent(inout) :: n

    integer           :: j,k,id_existing=0
    character(len=64) :: mod_name
    logical           :: model_found,new

    if (US%regiondef(i)%reg_mat(1:4).eq.'cont') then
        if (len_trim(US%regiondef(i)%mod_name).eq.0) then
            mod_name  = 'default'

        else
            mod_name  = US%regiondef(i)%mod_name

        endif
        model_found = .false.
        do j=US%n_contact,1,-1
            if ((US%contact(j)%mod_name.eq.mod_name).or.(US%contact(j)%mod_name(1:7).eq.'default')) then
                model_found       = .true.  
                n                 = n    + 1
                BOX(n)%mod_name   = mod_name
                BOX(n)%eps        = 0
                BOX(n)%kappa      = US%contact(j)%kappa
                BOX(n)%type       = 'cont'
                BOX(n)%shape      = 'rect'
                BOX(n)%low_xyz    = US%regiondef(i)%low_xyz
                BOX(n)%upp_xyz    = US%regiondef(i)%upp_xyz
                BOX(n)%cont_name  = US%regiondef(i)%cont_name
                BOX(n)%layer      = US%regiondef(i)%layer
                if (US%contact(j)%con_type(1:8).eq.'schottky') then
                    BOX(n)%diri     = .true.  ! dirichlet boundary
                else  
                    if (US%contact(j)%ohmic_bc(1:5).eq.'float') then
                        BOX(n)%diri   = .false. ! neumann boundary
                    else
                        BOX(n)%diri   = .true.  ! dirichlet boundary
                    endif
                endif
                BOX(n)%model_id   = j

                ! new contact box?
                new               = .true.
                do k=1,n-1
                    if (BOX(k)%cont_name.eq.BOX(n)%cont_name) then
                        new           = .false.
                        id_existing   = BOX(k)%cont_id
                        if (BOX(n)%model_id.ne.BOX(k)%model_id) then
                            write(*,*) '***error*** two different models (',trim(mod_name),') for CONTACT ',trim(BOX(n)%cont_name),' defined'
                            stop
                        endif
                    endif
                enddo
                if (new) then
                    N_CON          = N_CON + 1
                    BOX(n)%cont_id  = N_CON
                else
                    BOX(n)%cont_id  = id_existing
                endif
                exit
            endif
        enddo
        if (.not.model_found) then
            write(*,*) '***error*** CONT model ',trim(mod_name),' not found'
            stop
        endif
    endif

endsubroutine

subroutine add_supply_box(i,n,n_supply)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! add supply box to device structure
    !
    ! input
    ! -----
    ! i : integer
    !   index to regiondef of contact regiondef.
    !
    ! output
    ! ------
    ! n : integer  
    !   number of boxes (?)
    ! n_cont : integer
    !   number of supply boxes (?)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in   ) :: i
    integer,intent(inout) :: n
    integer,intent(inout) :: n_supply

    integer           :: j,id_existing = 0
    character(len=64) :: mod_name
    logical           :: model_found,new


    if (US%regiondef(i)%reg_mat(1:4).eq.'supp') then
        if (len_trim(US%regiondef(i)%mod_name).eq.0) then
            mod_name  = 'default'

        else
            mod_name  = US%regiondef(i)%mod_name

        endif
        model_found = .false.
        do j=US%n_supply,1,-1
            if ((US%supply(j)%mod_name.eq.mod_name).or.(US%supply(j)%mod_name(1:7).eq.'default')) then
                model_found       = .true.  
                n                 = n    + 1
                BOX(n)%mod_name   = mod_name
                BOX(n)%cont_name  = US%regiondef(i)%cont_name
                BOX(n)%eps        = 0
                BOX(n)%kappa      = US%contact(j)%kappa
                BOX(n)%type       = 'supply'
                BOX(n)%shape      = 'rect'
                BOX(n)%low_xyz    = US%regiondef(i)%low_xyz
                BOX(n)%upp_xyz    = US%regiondef(i)%upp_xyz
                BOX(n)%layer      = US%regiondef(i)%layer
                BOX(n)%diri       = .false.  ! base bundary contact does not set psi
                BOX(n)%model_id   = j

                ! !go through all semi boxes and check if this supply contact is in them
                ! !need to add semi box before supply!
                ! semi_id = 0
                ! do m=n,1,-1
                !   if (BOX(m)%semi_id.GT.0) then
                !     !will only work for 1D...
                !     if ((BOX(m)%low_xyz(1).LT.BOX(n)%low_xyz(1)).AND.(BOX(m)%upp_xyz(1).GT.BOX(n)%upp_xyz(1))) then
                !       semi_id = BOX(m)%semi_id
                !     endif
                !   endif
                ! enddo
                ! if (semi_id.EQ.0) then
                !   write(*,*) '***error*** Could not find semiconductor region for supply contact. Add semiconductor before supply.'
                ! endif
                ! BOX(n)%semi_id = semi_id


                ! new contact box?
                new               = .true.
                !do k=1,n-1
                !  if (BOX(k)%supp_name.eq.BOX(n)%supp_name) then
                !    new           = .false.
                !    id_existing   = BOX(k)%supply_id
                    !if (BOX(n)%model_id.ne.BOX(k)%model_id) then
                    !  write(*,*) '***error*** two different models (',trim(mod_name),') for SUPPLY ',trim(BOX(n)%cont_name),' defined'
                    !  stop
                    !endif
                !  endif
                !enddo
                if (new) then
                    n_supply          = n_supply + 1
                    BOX(n)%supply_id  = n_supply
                else
                    BOX(n)%supply_id  = id_existing
                endif

                !set contact id
                !will only work for single supply box.
                N_CON          = N_CON + 1
                BOX(n)%cont_id = N_CON

                exit
            endif
        enddo
        if (.not.model_found) then
            write(*,*) '***error*** CONT model ',trim(mod_name),' not found'
            stop
        endif
    endif

endsubroutine

subroutine add_oxide_box(i,n,n_oxid)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! add oxide box to device structure
    !
    ! input
    ! -----
    ! i : integer
    !   index to regiondef of contact regiondef.
    !
    ! output
    ! ------
    ! n : integer  
    !   number of boxes (?)
    ! n_cont : integer
    !   number of supply boxes (?)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in   ) :: i
    integer,intent(inout) :: n
    integer,intent(inout) :: n_oxid

    integer           :: j
    character(len=64) :: mod_name
    logical           :: model_found

    if (US%regiondef(i)%reg_mat(1:4).eq.'oxid') then
        if (len_trim(US%regiondef(i)%mod_name).eq.0) then
            mod_name  = 'sio2'

        else
            mod_name  = US%regiondef(i)%mod_name

        endif
        model_found = .false.
        do j=US%n_oxide,1,-1
            if ((US%oxide(j)%mod_name.eq.mod_name).or.(US%oxide(j)%mod_name(1:7).eq.'default')) then
                model_found      = .true.
                n                = n+1
                n_oxid           = n_oxid + 1
                BOX(n)%mod_name  = mod_name
                BOX(n)%eps       = US%oxide(j)%eps
                BOX(n)%kappa     = US%oxide(j)%kappa
                BOX(n)%type      = 'oxid'
                BOX(n)%shape     = 'rect'
                BOX(n)%low_xyz   = US%regiondef(i)%low_xyz
                BOX(n)%upp_xyz   = US%regiondef(i)%upp_xyz
                BOX(n)%cont_name = ''
                BOX(n)%layer     = US%regiondef(i)%layer
                BOX(n)%diri      = .false.
                BOX(n)%oxid_id   = n_oxid
                BOX(n)%model_id  = j
                exit
            endif
        enddo
        if (.not.model_found) then
            write(*,*) '***error*** OXIDE model ',trim(mod_name),' not found'
            stop
        endif
    endif

endsubroutine

subroutine add_semi_box(i,n,n_semi)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! add semi box to device structure
    !
    ! input
    ! -----
    ! i : integer
    !   index to regiondef of contact regiondef.
    !
    ! output
    ! ------
    ! n : integer  
    !   number of boxes (?)
    ! n_cont : integer
    !   number of semi boxes (?)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer,intent(in   ) :: i
    integer,intent(inout) :: n
    integer,intent(inout) :: n_semi

    !in semiconductor boxes, we set mod_id, eps, and so on later since we may have a heterostructure
    if (US%regiondef(i)%reg_mat(1:4).eq.'semi') then
        n                = n      + 1
        n_semi           = n_semi + 1
        BOX(n)%mod_name  = US%regiondef(i)%mod_name(1:6)
        BOX(n)%eps       = 0
        BOX(n)%kappa     = 0
        BOX(n)%type      = 'semi'
        BOX(n)%shape     = 'rect'
        BOX(n)%low_xyz   = US%regiondef(i)%low_xyz
        BOX(n)%upp_xyz   = US%regiondef(i)%upp_xyz
        BOX(n)%diri      = .false.
        BOX(n)%layer     = US%regiondef(i)%layer
        BOX(n)%model_id  = 0
    endif

endsubroutine

subroutine check_device
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! check device structure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),dimension(3) :: upp1, low1, upp2, low2
    logical,dimension(3) :: touch
    integer              :: i, j

    ! --> test if low & upp is defined correctly
    do i = 1,N_BOX
        if (BOX(i)%shape(1:4).ne.'rect') cycle ! todo
        upp1 = BOX(i)%upp_xyz
        low1 = BOX(i)%low_xyz
        if (upp1(1).lt.low1(1)) then
            write(*,*) '***error*** structure definition (x_low>x_upp)'
            write(*,*) 'box: ',trim(BOX(i)%type),' low_x=',low1(1), ' upp_x=',upp1(1)
            stop

        endif
        if (upp1(2).lt.low1(2)) then
            write(*,*) '***error*** structure definition (y_low>y_upp)'
            write(*,*) 'box: ',trim(BOX(i)%type),' low_y=',low1(2), ' upp_y=',upp1(2)
            stop

        endif
        if (upp1(3).lt.low1(3)) then
            write(*,*) '***error*** structure definition (z_low>z_upp)'
            write(*,*) 'box: ',trim(BOX(i)%type),' low_z=',low1(3), ' upp_z=',upp1(3)
            stop

        endif
    enddo

    ! --> test if boxes overlap
    do i=1,N_BOX-1
        if (BOX(i)%shape(1:4).ne.'rect') cycle
        upp1   = BOX(i)%upp_xyz
        low1   = BOX(i)%low_xyz
        do j=i+1,N_BOX
            if (BOX(j)%shape(1:4).ne.'rect') cycle
            upp2 = BOX(j)%upp_xyz
            low2 = BOX(j)%low_xyz

            touch(1) = .not.((low1(1).gt.upp2(1)).or.(upp1(1).lt.low2(1)))
            touch(2) = .not.((low1(2).gt.upp2(2)).or.(upp1(2).lt.low2(2)))
            touch(3) = .not.((low1(3).gt.upp2(3)).or.(upp1(3).lt.low2(3)))

            if (STRUC_DIM.eq.1) then
                if (touch(1)) then
                    ! error if different contact box
                    if (BOX(i)%type(1:4).eq.'cont') then
                        if (BOX(j)%type(1:4).eq.'cont') then
                            if (BOX(i)%cont_name.ne.BOX(j)%cont_name) then
                                write(*,*) '***error*** overlapping box ', trim(BOX(i)%cont_name),' and ',trim(BOX(j)%cont_name),&
                                          &' (',touch(1),')'
                                stop

                            endif
                        endif
                    endif
                endif

            elseif (STRUC_DIM.eq.2) then
                if (touch(1).and.touch(2)) then
                    ! error if different contact box
                    if (BOX(i)%type(1:4).eq.'cont') then
                        if (BOX(j)%type(1:4).eq.'cont') then
                            if (BOX(i)%cont_name.ne.BOX(j)%cont_name) then
                                write(*,*) '***error*** overlapping box ', trim(BOX(i)%cont_name),' and ',trim(BOX(j)%cont_name),&
                                          &' (',touch(1),touch(2),')'
                                stop

                            endif
                        endif
                    endif
                endif

            elseif (STRUC_DIM.eq.3) then
                if (touch(1).and.touch(2).and.touch(3)) then
                    ! error if different contact box
                    if (BOX(i)%type(1:4).eq.'cont') then
                        if (BOX(j)%type(1:4).eq.'cont') then
                            if (BOX(i)%cont_name.ne.BOX(j)%cont_name) then
                                write(*,*) '***error*** overlapping box ', trim(BOX(i)%cont_name),' and ',trim(BOX(j)%cont_name),&
                                          &' (',touch(1),touch(2),touch(3),')'
                                stop

                            endif
                        endif
                    endif
                endif
            endif

        enddo
    enddo

endsubroutine

subroutine null_point_struc
    !!!!!!!!!!!!!!!!!!!!!!!!
    ! null point structure
    !!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i,j,k

    ! null only values, that could not be inited in type def of struc_point
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                POINT(i,j,k)%eps         = BOX(1)%eps
                POINT(i,j,k)%kappa       = BOX(1)%kappa
                POINT(i,j,k)%diri        = BOX(1)%diri
                POINT(i,j,k)%cont_id     = BOX(1)%cont_id
            enddo
        enddo
    enddo
endsubroutine

subroutine init_point_struc
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init point structure
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),dimension(3)         :: low_xyz,upp_xyz
    integer                      :: b,b1,i,j,k,xline_id,l,semi_id,hdferr!,id_x
    logical                      :: z_top,y_top,x_top,z_low,y_low,x_low,x_border,y_border,z_border,border,new
    real(8)                      :: x0,y0,z0,ri,ra,rr,rr1,rr2,rr3,xx,yy,zz,xx1,yy1,zz1
    logical                      :: ok1,ok2,ok3
    logical                      :: semi_cont
    integer,dimension(:),pointer :: layers
    integer                      :: nl,p
    character(len=64)            :: mod_name
    real(8),dimension(:), allocatable:: x_h5, prof_h5
    real(8),dimension(:,:), allocatable:: profiles_h5

    nullify(layers)

    ! ----> init point struct
    allocate(POINT(NX1,NY1,NZ1))
    call null_point_struc

    ! --> get number of layers
    nl=0
    do b=1,N_BOX
        call add_point_int(layers,nl,BOX(b)%layer)
    enddo

    ! --> set POINT struct
    do l=1,size(layers)
        do b=1,N_BOX
            if (BOX(b)%layer.ne.layers(l)) cycle
            low_xyz    = BOX(b)%low_xyz-1e-13
            upp_xyz    = BOX(b)%upp_xyz+1e-13
            if ((.not.CART).or.(STRUC_DIM.le.1).or.(BOX(b)%shape(1:4).eq.'rect')) then
                do k=1,NZ1
                    if ((Z(k).ge.low_xyz(3)).and.(Z(k).le.upp_xyz(3))) then
                        z_top    = abs(Z(k)-upp_xyz(3)).lt.1e-12
                        z_low    = abs(Z(k)-low_xyz(3)).lt.1e-12
                        z_border = (z_top.or.z_low).and.(STRUC_DIM.ge.3)
                        do j=1,NY1
                            if ((Y(j).ge.low_xyz(2)).and.(Y(j).le.upp_xyz(2))) then
                                y_top    = abs(Y(j)-upp_xyz(2)).lt.1e-12
                                y_low    = abs(Y(j)-low_xyz(2)).lt.1e-12
                                y_border = (y_top.or.y_low).and.(STRUC_DIM.ge.2)
                                do i=1,NX1
                                    if ((X(i).ge.low_xyz(1)).and.(X(i).le.upp_xyz(1))) then
                                        ! --> rectangular box
                                        x_top    = abs(X(i)-upp_xyz(1)).lt.1e-12
                                        x_low    = abs(X(i)-low_xyz(1)).lt.1e-12
                                        x_border = (x_top.or.x_low).and.(STRUC_DIM.ge.1)
                                        border   = x_border.or.y_border.or.z_border
                                        
                                        if (POINT(i,j,k)%box_id.eq.0) then
                                          POINT(i,j,k)%box_id = b ! new point

                                        ! elseif (border) then
                                        else
                                          b1 = POINT(i,j,k)%box_id
                                          if     ((BOX(b1)%cont_id.gt.0).and.(BOX(b)%cont_id.eq.0)) then
                                            ! contact cannot be overwritten if new point is not contact
                                          else
                                            POINT(i,j,k)%box_id  = b ! overwrite point
                                            
                                          endif
                                      
                                        ! else
                                        !     POINT(i,j,k)%box_id    = b ! overwrite point
                                          
                                        endif

                                        !this point is touched by a semiconductor box
                                        !if ((BOX(b)%type(1:4).eq.'semi').or.(POINT(i,j,k)%semi_box.gt.0)) then
                                        ! if (BOX(b)%type(1:4).eq.'semi') then
                                        !     POINT(i,j,k)%semi_box = b
                                        ! endif

                                        ! --> define eps/kappa ---------
                                        if (STRUC_DIM.eq.1) then
                                            ! if (.not.x_top) then
                                                POINT(i,j,k)%eps   = BOX(b)%eps
                                                POINT(i,j,k)%kappa = BOX(b)%kappa
                                            ! endif
                                        elseif (STRUC_DIM.eq.2) then
                                            ! if ((.not.x_top).and.(.not.y_top)) then
                                                POINT(i,j,k)%eps   = BOX(b)%eps
                                                POINT(i,j,k)%kappa = BOX(b)%kappa
                                            ! endif
                                        elseif (STRUC_DIM.eq.3) then
                                            if ((.not.x_top).and.(.not.y_top).and.(.not.z_top)) then
                                                POINT(i,j,k)%eps   = BOX(b)%eps
                                                POINT(i,j,k)%kappa = BOX(b)%kappa
                                            endif
                                        endif

                                    endif
                                enddo
                            endif
                        enddo
                    endif
                enddo

            elseif (BOX(b)%shape(1:6).eq.'cyl_xy') then !obsolete
              x0 = BOX(b)%low_xyz(1)
              y0 = BOX(b)%low_xyz(2)
              ri = BOX(b)%upp_xyz(1)*0.9999
              ra = BOX(b)%upp_xyz(2)*1.0001
              do k=1,NZ1
                if ((Z(k).ge.low_xyz(3)).and.(Z(k).le.upp_xyz(3))) then
                  z_top = abs(Z(k)-upp_xyz(3)).lt.1e-12
                  do j=1,NY1 ! y-discretization points
                    yy = abs(y0-Y(j))
                    do i=1,NX1 ! x-discretization points
                      xx = abs(x0-X(i))
                      rr = sqrt(xx*xx+yy*yy) 
                      if ((rr.ge.ri).and.(rr.le.ra)) then
                        POINT(i,j,k)%box_id   = b
                        if ((BOX(b)%type.eq.'semi').or.(POINT(i,j,k)%semi_box.gt.0)) then
                          POINT(i,j,k)%semi_box = b
                        endif
                        ! ---> epsilon/kappa
                        if (j.lt.NY1) then
                          yy1 = abs(y0-Y(j+1))
                        else
                          cycle
                        endif
                        if (i.lt.NX1) then
                          xx1 = abs(x0-X(i+1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(xx*xx+yy1*yy1) 
                        rr2 = sqrt(xx1*xx1+yy*yy) 
                        rr3 = sqrt(xx1*xx1+yy1*yy1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if ((STRUC_DIM.eq.2).or.(.not.z_top)) then
                            POINT(i,j,k)%eps   = BOX(b)%eps
                            POINT(i,j,k)%kappa = BOX(b)%kappa
                          endif
                        endif
                      endif
                    enddo
                  enddo
                endif
              enddo

            elseif (BOX(b)%shape(1:6).eq.'cyl_xz') then
              if (STRUC_DIM.lt.3) cycle
              x0 = BOX(b)%low_xyz(1)
              z0 = BOX(b)%low_xyz(3)
              ri = BOX(b)%upp_xyz(1)*0.9999
              ra = BOX(b)%upp_xyz(3)*1.0001
              do k=1,NZ1 ! z-discretization points
                zz = abs(z0-Z(k))
                do j=1,NY1
                  if ((Y(j).ge.low_xyz(2)).and.(Y(j).le.upp_xyz(2))) then
                    y_top = abs(Y(j)-upp_xyz(2)).lt.1e-12
                    do i=1,NX1 ! x-discretization points
                      xx = abs(x0-X(i))
                      rr = sqrt(xx*xx+zz*zz) 
                      if ((rr.ge.ri).and.(rr.le.ra)) then
                        POINT(i,j,k)%box_id   = b
                        if ((BOX(b)%semi_id.gt.0).or.(POINT(i,j,k)%semi_box.gt.0)) then
                          POINT(i,j,k)%semi_box = b
                        endif
                        ! ---> epsilon/kappa
                        if (k.lt.NZ1) then
                          zz1 = abs(z0-Z(k+1))
                        else
                          cycle
                        endif
                        if (i.lt.NX1) then
                          xx1 = abs(x0-X(i+1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(xx*xx+zz1*zz1) 
                        rr2 = sqrt(xx1*xx1+zz*zz) 
                        rr3 = sqrt(xx1*xx1+zz1*zz1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if (.not.y_top) then
                            POINT(i,j,k)%eps   = BOX(b)%eps
                            POINT(i,j,k)%kappa = BOX(b)%kappa
                          endif
                        endif
                      endif
                    enddo
                  endif
                enddo
              enddo

            elseif (BOX(b)%shape(1:6).eq.'cyl_yz') then
              if (STRUC_DIM.lt.3) cycle
              y0 = BOX(b)%low_xyz(2)
              z0 = BOX(b)%low_xyz(3)
              ri = BOX(b)%upp_xyz(2)*0.9999
              ra = BOX(b)%upp_xyz(3)*1.0001
              
              do k=1,NZ1 ! y-discretization points
                zz = abs(z0-Z(k))
                do j=1,NY1 ! x-discretization points
                  yy = abs(y0-Y(j))
                  rr = sqrt(yy*yy+zz*zz)
                  if ((rr.ge.ri).and.(rr.le.ra)) then
                    do i=1,NX1
                      if ((X(i).ge.low_xyz(1)).and.(X(i).le.upp_xyz(1))) then
                        x_top = abs(X(i)-upp_xyz(1)).lt.1e-12
          
                        POINT(i,j,k)%box_id   = b
                        if ((BOX(b)%type.eq.'semi').or.(POINT(i,j,k)%semi_box.gt.0)) then
                          POINT(i,j,k)%semi_box = b
                        endif
                        if (BOX(b)%semi_id.gt.0) then
                          POINT(i,j,k)%semi_id  = BOX(b)%semi_id
                        endif

                        ! ---> epsilon/kappa
                        ! --> check k+1 & j+1 point if >ri and <ra
                        if (k.lt.NZ1) then
                          zz1 = abs(z0-Z(k+1))
                        else
                          cycle
                        endif
                        if (j.lt.NY1) then
                          yy1 = abs(y0-Y(j+1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(yy*yy+zz1*zz1) 
                        rr2 = sqrt(yy1*yy1+zz*zz) 
                        rr3 = sqrt(yy1*yy1+zz1*zz1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if (.not.x_top) then
                            POINT(i,j,k)%eps   = BOX(b)%eps
                            POINT(i,j,k)%kappa = BOX(b)%kappa
                          endif
                        endif
                        
                        ! --> check k-1 & j-1 point if >ri and <ra
                        if (k.gt.1) then
                          zz1 = abs(z0-Z(k-1))
                        else
                          cycle
                        endif
                        if (j.gt.1) then
                          yy1 = abs(y0-Y(j-1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(yy*yy+zz1*zz1) 
                        rr2 = sqrt(yy1*yy1+zz*zz) 
                        rr3 = sqrt(yy1*yy1+zz1*zz1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if (.not.x_top) then
                            POINT(i,j-1,k-1)%eps   = BOX(b)%eps
                            POINT(i,j-1,k-1)%kappa = BOX(b)%kappa
                          endif
                        endif
                        
                        ! --> check k+1 & j-1 point if >ri and <ra
                        if (k.lt.NZ1) then
                          zz1 = abs(z0-Z(k+1))
                        else
                          cycle
                        endif
                        if (j.gt.1) then
                          yy1 = abs(y0-Y(j-1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(yy*yy+zz1*zz1) 
                        rr2 = sqrt(yy1*yy1+zz*zz) 
                        rr3 = sqrt(yy1*yy1+zz1*zz1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if (.not.x_top) then
                            POINT(i,j-1,k)%eps   = BOX(b)%eps
                            POINT(i,j-1,k)%kappa = BOX(b)%kappa
                          endif
                        endif
                        
                        ! --> check k-1 & j+1 point if >ri and <ra
                        if (k.gt.1) then
                          zz1 = abs(z0-Z(k-1))
                        else
                          cycle
                        endif
                        if (j.lt.NY1) then
                          yy1 = abs(y0-Y(j+1))
                        else
                          cycle
                        endif
                        rr1 = sqrt(yy*yy+zz1*zz1) 
                        rr2 = sqrt(yy1*yy1+zz*zz) 
                        rr3 = sqrt(yy1*yy1+zz1*zz1) 
                        ok1 = ((rr1.ge.ri).and.(rr1.le.ra))
                        ok2 = ((rr2.ge.ri).and.(rr2.le.ra))
                        ok3 = ((rr3.ge.ri).and.(rr3.le.ra))

                        if ((ok1.and.ok2).or.(ok1.and.ok3).or.(ok2.and.ok3)) then
                          if (.not.x_top) then
                            POINT(i,j,k-1)%eps   = BOX(b)%eps
                            POINT(i,j,k-1)%kappa = BOX(b)%kappa
                          endif
                        endif
                        
                      endif
                    enddo
                  endif
                enddo
              enddo
            endif
        enddo
    enddo

    do b=1,N_BOX
        mod_name = BOX(b)%mod_name
    enddo

    ! set supply box ID
    do i=1,NX1
        do j=1,NY1
            do k=1,NZ1
                b = POINT(i,j,k)%box_id
                if (BOX(b)%supply_id.gt.0) then
                    POINT(i,j,k)%supply_id  = BOX(b)%supply_id 
                    POINT(i,j,k)%cont_id    = BOX(b)%cont_id
                endif
            enddo
        enddo
    enddo

    ! read profiles from hdf5
    if (US%regioninfo%hdf(1:1).ne.'-') then
        write(*,*) 'reading profile from HDF5...'

        ! find x data in HDF5 File
        call read_col_hdf5(US%regioninfo%hdf,'/'//'X',x_h5,hdferr)
        if (hdferr.eq.-1) then
            write(*,*) ' ****error*** no X column in HDF5 file'
            stop
        endif

        !allocate array that holds info
        allocate(profiles_h5(US%n_semi,size(x_h5)))
        profiles_h5 = 0

        !try to find semi columns in HDF5 File
        do semi_id=1,US%n_semi
            mod_name = US%semi(semi_id)%mod_name
            call read_col_hdf5(US%regioninfo%hdf,'/'//trim(mod_name),prof_h5,hdferr)
            if (hdferr.eq.-1) then
                write(*,*) '- semiconductor model ',trim(mod_name),' not found in HDF5!'
            else
                profiles_h5(semi_id,:) = prof_h5
                write(*,*) '- semiconductor model ',trim(mod_name),' found in HDF5!'
            endif

        enddo
        ! write(*,*) '...finish reading semiconductor profile from HDF5.'

        ! write(*,*) 'assign table model to points in grid ...'
        ! do i=1,NX1
        !     !step one: find  id of point
        !     id_x = REMAP_VEC(X,i,x_h5)
        !     if (id_x.eq.0) then
        !         write (*,*) '***error*** internal X vector does not match HDF5 X vector'
        !     endif

        !     !step2:see which semiconductor is in there
        !     do n=1,US%n_semi
        !         if (profiles_h5(n,id_x).eq.1) then
        !             POINT(i,1,1)%semi_id = n
        !             exit
        !         endif
        !     enddo

        ! enddo
        write(*,*) '... finish assign table model to points in grid.'
    endif

    ! set semiconductor box ID
    ! todo: might be smarter to use POINT%box_id instead of semi_box, 
    !       however for semi_box POINTS we would then need to find 
    !       neighbour for semi_id...will keep this dirty implementation
    !       until better solution is required (hopefully never)
    ! hint: for hdf input grid this should already be done
    do i=1,NX1
        do j=1,NY1
            do k=1,NZ1

                b = POINT(i,j,k)%box_id
                if ( .not.((BOX(b)%type(1:4).eq.'semi').or.(BOX(b)%type(1:4).eq.'cont').or.(BOX(b)%type(1:6).eq.'supply'))) then
                    cycle
                endif
                if (BOX(b)%type(1:6).eq.'supply') then
                    POINT(i,j,k)%semi_id   = POINT(i-1,j,k)%semi_id
                endif

                if (b.gt.0) then !point in a semiconductor box
                    mod_name              = BOX(b)%mod_name !try to find box's model
                    if (mod_name(1:3).eq.'hdf') then
                        ! !step one: find  id of point
                        ! id_x = REMAP_VEC(X,i,x_h5)
                        ! if (id_x.eq.0) then
                        !     write (*,*) '***error*** internal X vector does not match HDF5 X vector'
                        ! endif

                        ! !step2:see which semiconductor is in there
                        ! do n=1,US%n_semi
                        !     if (profiles_h5(n,id_x).eq.1) then
                        !         POINT(i,j,k)%semi_id = n
                        !         exit
                        !     endif
                        ! enddo

                    else
                        !find semiconductor model next to point
                        if (BOX(b)%type(1:4).eq.'cont') then
                            mod_name = 'none'
                            if (i.gt.1) then !left point
                                if (BOX( POINT(i-1,j,k)%box_id )%type.eq.'semi') mod_name = BOX( POINT(i-1,j,k)%box_id )%mod_name 
                            endif 
                            if (i.lt.NX1) then !right point
                                if (BOX( POINT(i+1,j,k)%box_id )%type.eq.'semi') mod_name = BOX( POINT(i+1,j,k)%box_id )%mod_name 
                            endif
                            if (j.gt.1) then !lower point
                                if (BOX( POINT(i,j-1,k)%box_id )%type.eq.'semi') mod_name = BOX( POINT(i,j-1,k)%box_id )%mod_name 
                            endif 
                            if (j.lt.NY1) then !right point
                                if (BOX( POINT(i,j+1,k)%box_id )%type.eq.'semi') mod_name = BOX( POINT(i,j+1,k)%box_id )%mod_name 
                            endif
                            if (mod_name.eq.'none') then !this contact point is not next to a semiconductor
                                cycle
                            endif
                        endif

                        !scan through already read in semi models
                        do semi_id=1,US%n_semi
                            if ((US%semi(semi_id)%mod_name(1:6).eq.mod_name(1:6))) then
                                POINT(i,j,k)%semi_id   = semi_id
                            endif
                        enddo
                        if (POINT(i,j,k)%semi_id.eq.0) then
                            write (*,*) '***error*** error during setting of semi_id. Did not find semiconductor model specified in REGION_DEF. Box mod_name:' // BOX(POINT(i,j,k)%box_id)%mod_name
                            stop
                        endif

                    endif

                endif
                if ((b.gt.0).and.(POINT(i,j,k)%semi_id.eq.0)) then
                    write (*,*) '***error*** error during setting of semi_id.'
                    stop
                endif
            enddo
        enddo
    enddo


    !dirty fix by Markus to get eps right
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX
                if (POINT(i,j,k)%semi_id.gt.0) then
                    do b=1,N_BOX
                        if (BOX(b)%semi_id.eq.POINT(i,j,k)%semi_id) POINT(i,j,k)%eps = BOX(b)%eps
                    enddo
                endif

            enddo
        enddo
    enddo


    ! --> set semi/oxid/cont -------
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                b = POINT(i,j,k)%box_id
                POINT(i,j,k)%diri    = BOX(b)%diri
                POINT(i,j,k)%cont_id = BOX(b)%cont_id
                if (POINT(i,j,k)%semi_id.gt.0) then
                    POINT(i,j,k)%semi  = .true.
                endif
                if (BOX(b)%oxid_id.gt.0) then
                    POINT(i,j,k)%oxid  = .true.
                endif
                if (POINT(i,j,k)%cont_id.gt.0) then
                    POINT(i,j,k)%cont  = .true.
                endif
                if (POINT(i,j,k)%supply_id.gt.0) then
                    POINT(i,j,k)%supply = .true.
                endif
                if ((POINT(i,j,k)%supply_id.gt.0).and.(POINT(i,j,k)%cont_id.eq.0)) then
                    write(*,'(A)') '***error*** supply and contact id make no sense.'
                    stop
                endif
            enddo
        enddo
    enddo

    ! --> set lower/upper boundarys for contacts
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%cont) then
                    ! contact
                    if (STRUC_DIM.ge.1) then
                        if (i.gt.1) then
                            POINT(i,j,k)%cont_b_xlow = .not.POINT(i-1,j,k)%cont
                        endif
                        if (i.lt.NX1) then
                            POINT(i,j,k)%cont_b_xupp = .not.POINT(i+1,j,k)%cont
                        endif
                    endif

                    if (STRUC_DIM.ge.2) then
                        if (j.gt.1) then
                            POINT(i,j,k)%cont_b_ylow = .not.POINT(i,j-1,k)%cont
                        endif
                        if (j.lt.NY1) then
                            POINT(i,j,k)%cont_b_yupp = .not.POINT(i,j+1,k)%cont
                        endif
                    endif

                    if (STRUC_DIM.ge.3) then
                        if (k.gt.1) then
                            POINT(i,j,k)%cont_b_zlow = .not.POINT(i,j,k-1)%cont
                        endif
                        if (k.lt.NZ1) then
                            POINT(i,j,k)%cont_b_zupp = .not.POINT(i,j,k+1)%cont
                        endif
                    endif
                    POINT(i,j,k)%cont_b = ( POINT(i,j,k)%cont_b_zlow.or.POINT(i,j,k)%cont_b_zupp.or.POINT(i,j,k)%cont_b_ylow.or.POINT(i,j,k)%cont_b_yupp.or. POINT(i,j,k)%cont_b_xlow.or.POINT(i,j,k)%cont_b_xupp)
                endif
            enddo
        enddo
    enddo

    ! --> mark contacts points with semiconductor neighbour
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                if (POINT(i,j,k)%cont.and.(POINT(i,j,k)%semi_id.gt.0)) then
                    ! semiconductor/contact boundary points
                    semi_cont = .false.

                    if (STRUC_DIM.ge.1) then
                        if (i.gt.1) then
                            semi_cont = semi_cont.or.POINT(i-1,j,k)%semi
                        endif
                        if (i.lt.NX1) then
                            semi_cont = semi_cont.or.POINT(i+1,j,k)%semi
                        endif
                    endif

                    if (STRUC_DIM.ge.2) then
                        if (j.gt.1) then
                            semi_cont = semi_cont.or.POINT(i,j-1,k)%semi
                        endif
                        if (j.lt.NY1) then
                            semi_cont = semi_cont.or.POINT(i,j+1,k)%semi
                        endif
                    endif

                    if (STRUC_DIM.ge.3) then
                        if (k.gt.1) then
                            semi_cont = semi_cont.or.POINT(i,j,k-1)%semi
                        endif
                        if (k.lt.NZ1) then
                            semi_cont = semi_cont.or.POINT(i,j,k+1)%semi
                        endif
                    endif

                    POINT(i,j,k)%semi_cont = semi_cont
                endif
            enddo
        enddo
    enddo

    ! --> xline id semiconductor
    xline_id=0
    do k=1,NZ1
        do j=1,NY1
            new=.true.
            do i=1,NX1
                if (POINT(i,j,k)%semi.or.POINT(i,j,k)%semi_cont.or.POINT(i,j,k)%supply) then
                    if (new) then
                        xline_id = xline_id + 1
                        new      = .false.
                    endif
                    POINT(i,j,k)%xline_id = xline_id
                endif
            enddo
        enddo
    enddo

    ! set index of POINT struct
    p = 0
    POINT(:,:,:)%id = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                p=p+1 ! total number of disc points
                POINT(i,j,k)%id = p
            enddo
        enddo
    enddo

    deallocate(layers)

endsubroutine

subroutine testi
    write(*,*) 'test'
endsubroutine

subroutine get_disc(c)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialize discretization, conversion from user input to internal data
    !
    ! input
    ! -----
    ! c : character(len=1)
    !   'x' or 'y' or 'z' - dircetion
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=1),intent(in) :: c  !< 

    real(8),dimension(:),pointer :: p,v
    integer                      :: i,j,np1,nv1,n
    logical                      :: error   ! error

    ! --> nullify
    nullify(p)
    nullify(v)

    np1 = 1
    n   = 0


    ! --> get range_grid points ---
    do i=1,US%n_rangegrid
        if (US%rangegrid(i)%disc_dir(1:1).eq.c) then
            call range_grid(US%rangegrid(i)%intv_pnts, US%rangegrid(i)%intv_diff, &
                          &US%rangegrid(i)%n_pnts, (US%rangegrid(i)%disc_set(1:4).eq.'diff'), v, nv1, error)
            n = n+1
            if (n.eq.1) then
                allocate(p(nv1))
                p = v
                np1 = nv1

            else
                do j=1,nv1
                    call add_point(p,np1,v(j))
                enddo

            endif
            if (associated(v)) then
                deallocate(v)
                nullify(v)

            endif
        endif
    enddo

    ! --> get area_grid points ---
    do i=1,US%n_areagrid
        if (US%areagrid(i)%disc_dir(1:1).eq.c) then
            call area_grid(US%areagrid(i)%intv_pnts, US%areagrid(i)%intv_diff, v, nv1)
            n = n+1
            if (n.eq.1) then
                allocate(p(nv1))
                p = v
                np1 = nv1

            else
                do j=1,nv1
                    call add_point(p,np1,v(j))
                enddo

            endif
            if (associated(v)) then
                deallocate(v)
                nullify(v)

            endif
        endif
    enddo

    ! ---> save in poi structure ----
    if (c.eq.'x') then
        if (.not.associated(X)) then
            if (associated(p)) then
                allocate(X(np1))
                X   = p
                NX1 = np1

            endif

        else
            if (associated(p)) then
                do i=1,NX1
                    call add_point(p,np1,X(i))
                enddo
                deallocate(X)
                nullify(X)
                allocate(X(np1))
                X   = p
                NX1 = np1

            endif

        endif

    elseif (c.eq.'y') then
        if (.not.associated(Y)) then
            if (associated(p)) then
                allocate(Y(np1))
                Y   = p
                NY1 = np1

            endif

        else
            if (associated(p)) then
                do i=1,NY1
                    call add_point(p,np1,Y(i))
                enddo
                deallocate(Y)
                nullify(Y)
                allocate(Y(np1))
                Y   = p
                NY1 = np1

            endif

        endif

    elseif (c.eq.'z') then
        if (.not.associated(Z)) then
            if (associated(p)) then
                allocate(Z(np1))
                Z   = p
                NZ1 = np1

            endif

        else
            if (associated(p)) then
                do i=1,NZ1
                    call add_point(p,np1,Z(i))
                enddo
                deallocate(Z)
                nullify(Z)
                allocate(Z(np1))
                Z   = p
                NZ1 = np1

            endif
        endif
    endif

    ! --> deallocate data
    if (associated(p)) deallocate(p)
endsubroutine


subroutine diff_disc
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! calculate distance between grid points
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i

    NX      = NX1 - 1     ! number of boxes = number of gridpoints - 1
    allocate(DX(NX))
    do i=1,NX
      DX(i) = X(i+1)-X(i) ! width of boxes
    enddo

    NY      = NY1 - 1     ! number of boxes = number of gridpoints - 1
    allocate(DY(NY))
    do i=1,NY
      DY(i) = Y(i+1)-Y(i) ! width of boxes
    enddo

    NZ      = NZ1 - 1     ! number of boxes = number of gridpoints - 1
    allocate(DZ(NZ))
    do i=1,NZ
      DZ(i) = Z(i+1)-Z(i) ! width of boxes
    enddo
endsubroutine


! ---------------------------------------------------------------------------------------------------------------------
!> \brief add box borders to disctretization
!> \details
subroutine add_box_border_to_grid

integer :: n,ix


do n=1,N_BOX
  if (BOX(n)%shape(1:4).ne.'rect') cycle ! todo: add also circle points?
  if (STRUC_DIM.ge.1) then
    call add_point(X,NX1,BOX(n)%low_xyz(1),ix,dble(1e-13))
    call add_point(X,NX1,BOX(n)%upp_xyz(1),ix,dble(1e-13))

  endif

  if (STRUC_DIM.ge.2) then
    call add_point(Y,NY1,BOX(n)%low_xyz(2),ix,dble(1e-13))
    call add_point(Y,NY1,BOX(n)%upp_xyz(2),ix,dble(1e-13))

  endif

  if (STRUC_DIM.ge.3) then
    call add_point(Z,NZ1,BOX(n)%low_xyz(3),ix,dble(1e-13))
    call add_point(Z,NZ1,BOX(n)%upp_xyz(3),ix,dble(1e-13))

  endif
       
enddo

endsubroutine

subroutine finish_disc
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! finish discretization, calculate total number of disc. points
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! --> x-direction -----
    if (.not.associated(X)) then
        allocate(X(1))
        X   = 0
        NX1 = 1

    endif

    ! --> y-direction -----
    if (.not.associated(Y)) then
        allocate(Y(1))
        Y   = 0
        NY1 = 1

    endif

    ! --> z-direction -----
    if (.not.associated(Z)) then
        allocate(Z(1))
        Z   = 0
        NZ1 = 1

    endif

    ! --> total number of discretization points ---
    NXYZ = NX1*NY1*NZ1

    ! --> set boundarys of background air box
    if (N_BOX.ge.1) then
        BOX(1)%low_xyz(1) = X(1)
        BOX(1)%low_xyz(2) = Y(1)
        BOX(1)%low_xyz(3) = Z(1)
        BOX(1)%upp_xyz(1) = X(NX1)
        BOX(1)%upp_xyz(2) = Y(NY1)
        BOX(1)%upp_xyz(3) = Z(NZ1)

    endif

endsubroutine

subroutine check_disc
    !!!!!!!!!!!!!!!!!!!!!!!
    ! check discretization
    !!!!!!!!!!!!!!!!!!!!!!!
    integer                  :: i,j,k,n
    logical,dimension(N_BOX) :: active,sf_xlow,sf_xupp,sf_ylow,sf_yupp,sf_zlow,sf_zupp
    real(8),dimension(N_BOX) :: low_x,upp_x,low_y,upp_y,low_z,upp_z

    ! --> check discretization 
    if (STRUC_DIM.ge.1) then
        do i=1,NX
            if (DX(i).le.dble(0)) then
                write(*,*) '***error*** x(',i+1,')-x(',i,') is ',DX(i)
                stop
            endif
        enddo
    endif

    if (STRUC_DIM.ge.2) then
        do i=1,NY
            if (DY(i).le.dble(0)) then
                write(*,*) '***error*** y(',i+1,')-y(',i,') is ',DY(i)
                stop
            endif
        enddo
    endif

    if (STRUC_DIM.ge.3) then
        do i=1,NZ
            if (DZ(i).le.dble(0)) then
                write(*,*) '***error*** z(',i+1,')-z(',i,') is ',DZ(i)
                stop
            endif
        enddo
    endif

    ! --> get box borders
    do n=1,N_BOX
        if (BOX(n)%shape(1:4).eq.'rect') then
            low_x(n) = BOX(n)%low_xyz(1)-1e-13
            upp_x(n) = BOX(n)%upp_xyz(1)+1e-13
            low_y(n) = BOX(n)%low_xyz(2)-1e-13
            upp_y(n) = BOX(n)%upp_xyz(2)+1e-13
            low_z(n) = BOX(n)%low_xyz(3)-1e-13
            upp_z(n) = BOX(n)%upp_xyz(3)+1e-13

        elseif (BOX(n)%shape(1:6).eq.'cyl_yz') then
            low_x(n) = BOX(n)%low_xyz(1)-1e-13
            upp_x(n) = BOX(n)%upp_xyz(1)+1e-13
            low_y(n) = BOX(n)%low_xyz(2)- BOX(n)%upp_xyz(3)-1e-13
            upp_y(n) = BOX(n)%low_xyz(2)+ BOX(n)%upp_xyz(3)+1e-13
            low_z(n) = BOX(n)%low_xyz(3)- BOX(n)%upp_xyz(3)-1e-13
            upp_z(n) = BOX(n)%low_xyz(3)+ BOX(n)%upp_xyz(3)+1e-13

        elseif (BOX(n)%shape(1:6).eq.'cyl_xz') then
            low_x(n) = BOX(n)%low_xyz(1)- BOX(n)%upp_xyz(3)-1e-13
            upp_x(n) = BOX(n)%low_xyz(1)+ BOX(n)%upp_xyz(3)+1e-13
            low_y(n) = BOX(n)%low_xyz(2)-1e-13
            upp_y(n) = BOX(n)%upp_xyz(2)+1e-13
            low_z(n) = BOX(n)%low_xyz(3)- BOX(n)%upp_xyz(3)-1e-13
            upp_z(n) = BOX(n)%low_xyz(3)+ BOX(n)%upp_xyz(3)+1e-13

        elseif (BOX(n)%shape(1:6).eq.'cyl_xy') then
            low_x(n) = BOX(n)%low_xyz(1)- BOX(n)%upp_xyz(2)-1e-13
            upp_x(n) = BOX(n)%low_xyz(1)+ BOX(n)%upp_xyz(2)+1e-13
            low_y(n) = BOX(n)%low_xyz(2)- BOX(n)%upp_xyz(2)-1e-13
            upp_y(n) = BOX(n)%low_xyz(2)+ BOX(n)%upp_xyz(2)+1e-13
            low_z(n) = BOX(n)%low_xyz(3)-1e-13
            upp_z(n) = BOX(n)%upp_xyz(3)+1e-13
          
        endif
    enddo

    ! --> check borders of x-discretization
    ! if (STRUC_DIM.ge.1) then
    !     if (abs(minval(low_x)-X(1)).gt.1e-12) then
    !         write(*,*) '***error*** min(x) discretization does not match with lowest x-value of boxes'
    !         stop
    !     endif
    !     if (abs(maxval(upp_x)-X(NX1)).gt.1e-12) then
    !         write(*,*) '***error*** max(x) discretization does not match with highest x-value of boxes'
    !         stop
    !     endif
    ! endif

    ! --> check borders of y-discretization
    ! if (STRUC_DIM.ge.2) then
    !     if (abs(minval(low_y)-Y(1)).gt.1e-12) then
    !         write(*,*) '***error*** min(y) discretization does not match with lowest y-value of boxes'
    !         stop
    !     endif
    !     if (abs(maxval(upp_y)-Y(NY1)).gt.1e-12) then
    !         write(*,*) '***error*** max(y) discretization does not match with highest y-value of boxes'
    !         stop
    !     endif
    ! endif

    ! ! --> check borders of z-discretization
    ! if (STRUC_DIM.ge.3) then
    !     if (abs(minval(low_z)-Z(1)).gt.1e-12) then
    !         write(*,*) '***error*** min(z) discretization does not match with lowest z-value of boxes'
    !         stop
    !     endif
    !     if (abs(maxval(upp_z)-Z(NZ1)).gt.1e-12) then
    !         write(*,*) '***error*** max(z) discretization does not match with highest z-value of boxes'
    !         stop
    !     endif
    ! endif


    ! --> go through points and check if all boxes are covert with disc points
    sf_xlow = .false.
    sf_xupp = .false.
    sf_ylow = .false.
    sf_yupp = .false.
    sf_zlow = .false.
    sf_zupp = .false.
    do i = 1,NX1
        do j = 1,NY1
            do k = 1,NZ1
                do n=1,N_BOX
                    ! grid point in box
                    active(n)  = ((X(i).ge.low_x(n)) .and. (X(i).le.upp_x(n)) .and. &
                                &(Y(j).ge.low_y(n)) .and. (Y(j).le.upp_y(n)) .and. &
                                &(Z(k).ge.low_z(n)) .and. (Z(k).le.upp_z(n)))
                    
                    ! (x==low_x)
                    sf_xlow(n) = sf_xlow(n).or.(abs(X(i)-low_x(n)).lt.1e-12)

                    ! (x==upp_x)
                    sf_xupp(n) = sf_xupp(n).or.(abs(X(i)-upp_x(n)).lt.1e-12)

                    ! (y==low_y)
                    sf_ylow(n) = sf_ylow(n).or.(abs(Y(j)-low_y(n)).lt.1e-12)

                    ! (y==upp_y)
                    sf_yupp(n) = sf_yupp(n).or.(abs(Y(j)-upp_y(n)).lt.1e-12)

                    ! (z==low_z)
                    sf_zlow(n) = sf_zlow(n).or.(abs(Z(k)-low_z(n)).lt.1e-12)

                    ! (z==upp_z)
                    sf_zupp(n) = sf_zupp(n).or.(abs(Z(k)-upp_z(n)).lt.1e-12)
                enddo
            enddo
        enddo
    enddo


    ! do n=1,N_BOX
    !     if (BOX(n)%shape(1:4).ne.'rect') cycle ! todo: check also circle
    !     if (STRUC_DIM.ge.1) then
    !         if (.not.sf_xlow(n)) then ! --> X-coordinate high
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  low_x=',BOX(n)%low_xyz(1),' missing in disc'
    !             stop
    !         endif

    !         if (.not.sf_xupp(n)) then ! --> X-coordinate high
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  upp_x=',BOX(n)%upp_xyz(1),' missing in disc'
    !             stop
    !         endif
    !     endif

    !     if (STRUC_DIM.ge.2) then
    !         if (.not.sf_ylow(n)) then ! --> Y-coordinate low
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  low_y=',BOX(n)%low_xyz(2),' missing in disc'
    !             stop
    !         endif

    !         if (.not.sf_yupp(n)) then ! --> Y-coordinate high
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  upp_y=',BOX(n)%upp_xyz(2),' missing in disc'
    !             stop
    !         endif
    !     endif

    !     if (STRUC_DIM.ge.3) then
    !         if (.not.sf_zlow(n)) then ! --> Z-coordinate low
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  low_z=',BOX(n)%low_xyz(3),' missing in disc'
    !             stop
    !         endif

    !         if (.not.sf_zupp(n)) then ! --> Z-coordinate high
    !             write(*,*) '***error*** box: ',trim(BOX(n)%type), '  upp_z=',BOX(n)%upp_xyz(3),' missing in disc'
    !             stop
    !         endif
    !     endif
    ! enddo
endsubroutine

subroutine set_semi_model
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set semiconductor models
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j,k,semi_id
    logical :: new_semi
    character(len=64),dimension(100) :: semi_models
    character(len=64)            :: mod_name

    !find semiconductors that are inside the simulation domain
    N_SEMI = 0
    do i=1,NX1
        do j=1,NY1
            do k=1,NZ1
                if (POINT(i,j,k)%semi_id.gt.0) then
                    !find pure/alloy semiconductors
                    new_semi = .true.
                    semi_id  = POINT(i,j,k)%semi_id
                    mod_name = US%semi(semi_id)%mod_name
                    do n=1,N_SEMI
                        if (mod_name.eq.semi_models(n)) then
                            new_semi = .false.
                            exit

                        endif
                    enddo
                    if (new_semi) then
                        N_SEMI=N_SEMI + 1
                        if (N_SEMI.gt.size(semi_models)) then
                            write(*,*) '***error*** too many semiconductor models'
                            stop

                        endif
                        semi_models(N_SEMI) = mod_name

                    endif

                    !find alloy materials
                    if ((US%semi(semi_id)%materialA(1:2).eq.'  ').and.(US%semi(semi_id)%materialB(1:2).eq.'  ')) then
                        cycle !this is not an alloy
                    endif
                    !check if only one material is given, which makes no sense
                    if ((US%semi(semi_id)%materialA(1:2).eq.'  ').and.(US%semi(semi_id)%materialA(1:2).ne.'  ')) then
                        write(*,'(A)') '***error*** only material B given for alloy'
                        stop
                    endif
                    if ((US%semi(semi_id)%materialB(1:2).eq.'  ').and.(US%semi(semi_id)%materialB(1:2).ne.'  ')) then
                        write(*,'(A)') '***error*** only material A given for alloy'
                        stop
                    endif
                    !add materialA
                    new_semi = .true.
                    mod_name = US%semi(semi_id)%materialA
                    do n=1,N_SEMI
                        if (mod_name.eq.semi_models(n)) then
                            new_semi = .false.
                            exit

                        endif
                    enddo
                    if (new_semi) then
                        N_SEMI=N_SEMI + 1
                        if (N_SEMI.gt.size(semi_models)) then
                            write(*,*) '***error*** too many semiconductor models'
                            stop

                        endif
                        semi_models(N_SEMI) = mod_name

                    endif
                    !add materialB
                    new_semi = .true.
                    mod_name = US%semi(semi_id)%materialB
                    do n=1,N_SEMI
                        if (mod_name.eq.semi_models(n)) then
                            new_semi = .false.
                            exit

                        endif
                    enddo
                    if (new_semi) then
                        N_SEMI=N_SEMI + 1
                        if (N_SEMI.gt.size(semi_models)) then
                            write(*,*) '***error*** too many semiconductor models'
                            stop

                        endif
                        semi_models(N_SEMI) = mod_name
                    endif
                endif
            enddo
        enddo
    enddo

    allocate(SEMI_MOD_NAME(N_SEMI))
    do n=1,N_SEMI
        SEMI_MOD_NAME(n) = semi_models(n)
    enddo

    !remap semi_id in point struct
    do i=1,NX1
        do j=1,NY1
            do k=1,NZ1
                if (POINT(i,j,k)%semi_id.gt.0) then
                    semi_id  = POINT(i,j,k)%semi_id
                    mod_name = US%semi(semi_id)%mod_name
                    do n=1,N_SEMI
                        if (mod_name.eq.semi_models(n)) then
                            POINT(i,j,k)%semi_id = n

                        endif
                    enddo

                endif
            enddo
        enddo
    enddo

    ! do n=1,N_BOX
    !   if (BOX(n)%type(1:4).eq.'semi') then
    !     do i=1,N_SEMI
    !       if (BOX(n)%mod_name.eq.SEMI_MOD_NAME(i)) then
    !         BOX(n)%model_id = i

    !       endif
    !     enddo
    !   endif
    ! enddo

endsubroutine

subroutine normalize_structure(l_norm)
real(8), intent(in) :: l_norm
  if (l_norm.ne.1) then
    write(*,*) '***info*** normalizing structure.'

    X = X/l_norm
    Y = Y/l_norm
    Z = Z/l_norm

    DX = DX/l_norm
    DY = DY/l_norm
    DZ = DZ/l_norm
  endif

endsubroutine


! ---------------------------------------------------------------------------------------------------------------------
!> \brief close module
!> \details deallocate all pointers
subroutine close_structure

if (associated(POINT))         deallocate(POINT)
if (associated(BOX))           deallocate(BOX)
if (associated(X))             deallocate(X)
if (associated(Y))             deallocate(Y)
if (associated(Z))             deallocate(Z)
if (associated(DX))            deallocate(DX)
if (associated(DY))            deallocate(DY)
if (associated(DZ))            deallocate(DZ)
if (associated(SEMI_MOD_NAME)) deallocate(SEMI_MOD_NAME)

endsubroutine


endmodule