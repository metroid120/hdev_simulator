!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! continuity equation of holes
module continuity_hole

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

implicit none

type, extends(cont) :: cont_hole_ !hole continuity equation of DD equ. system
  contains
  !these methods of the parent class are overwritten
  procedure,nopass   :: init   => init_cont_hole
  procedure,nopass   :: set    => set_cont_hole
  procedure,nopass   :: set_ac => set_cont_hole_ac
  procedure,nopass   :: init_cont_hole_qfn
  procedure,nopass   :: init_cont_hole_qfn2
  procedure,nopass   :: init_cont_hole_qfp
  procedure,nopass   :: init_cont_hole_psi
  procedure,nopass   :: init_cont_hole_tl
  procedure,nopass   :: init_cont_hole_tn
  procedure,nopass   :: init_cont_hole_ln
endtype

type(cont_hole_),target             :: CONT_HOLE          !< singleton continuity equation of electrons object

public CONT_HOLE

contains

subroutine init_cont_hole(var)
integer :: var,n
    if (.not.CONT_HOLE%active) then
      allocate(CONT_HOLE%jacobi(nvar-1))
      allocate(CONT_HOLE%jacobi_ac(nvar-1))
      allocate(CONT_HOLE%rhs(N_SP))
      allocate(CONT_HOLE%delta(N_SP))
      allocate(CONT_HOLE%var(N_SP))
      allocate(CONT_HOLE%var_last(N_SP))
      CONT_HOLE%var                 = 0
      CONT_HOLE%var_last            = 0
      CONT_HOLE%delta               = 0
      CONT_HOLE%rhs                 = 0
      CONT_HOLE%active              =.true.
      CONT_HOLE%jacobi(:)%active    =.false.
      CONT_HOLE%jacobi_ac(:)%active =.false.
      CONT_HOLE%i_var               = phip_

      CONT_HOLE%atol        = US%cont_hole%atol
      CONT_HOLE%rtol        = US%cont_hole%rtol
      CONT_HOLE%ctol        = US%cont_hole%ctol
      CONT_HOLE%delta_max   = US%cont_hole%delta_max


      !obtain valence band index
      do n=1,size(SEMI(1)%band_pointer)
        if (.not.(SEMI(1)%band_pointer(n)%band%iscb)) then
          CONT_HOLE%valley = get_valence_band_index(SEMI(1))
          exit
        endif
      enddo

    endif
    if (var.eq.phin_) then
        call CONT_HOLE%init_cont_hole_qfn
    elseif (var.eq.phin2_) then
        call CONT_HOLE%init_cont_hole_qfn2
    elseif (var.eq.phip_) then
        call CONT_HOLE%init_cont_hole_qfp
    elseif (var.eq.psi_) then
        call CONT_HOLE%init_cont_hole_psi
    elseif (var.eq.tl_) then
        call CONT_HOLE%init_cont_hole_tl
    elseif (var.eq.tn_) then
        call CONT_HOLE%init_cont_hole_tn
    elseif (var.eq.ln_) then
        call CONT_HOLE%init_cont_hole_ln
    else
        write(*,*) '***error*** initialization routine of CONT_HOLE not implemented for this case.'
        stop
    endif
endsubroutine

! init jacobian with respect to bohm potential of electrons
subroutine init_cont_hole_ln

integer :: i

! --> get size
call CONT_HOLE%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
CONT_HOLE%jacobi(ln_)%active=.true.

! --> set row_index/columns (not needed actually) -> remove for less memory
do i=1,N_SP
  CONT_HOLE%jacobi(ln_)%row_index(i)  = i
  CONT_HOLE%jacobi(ln_)%columns(i)    = i
enddo
CONT_HOLE%jacobi(ln_)%row_index(N_SP+1) = N_SP+1

! --> complex matrix for AC
if (AC_SIM) then
  call copy_crs(CONT_HOLE%jacobi(ln_), CONT_HOLE%jacobi_ac(ln_))
  CONT_HOLE%jacobi_ac(ln_)%active =.true.
endif

endsubroutine

!why not just 0 ?
subroutine init_cont_hole_tn

integer :: i

! --> get size
call CONT_HOLE%jacobi(tn_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
CONT_HOLE%jacobi(tn_)%active=.true.

! --> set row_index/columns (not needed actually) -> remove for less memory
do i=1,N_SP
  CONT_HOLE%jacobi(tn_)%row_index(i)  = i
  CONT_HOLE%jacobi(tn_)%columns(i)    = i
enddo
CONT_HOLE%jacobi(tn_)%row_index(N_SP+1) = N_SP+1

! --> complex matrix for AC
if (AC_SIM) then
  call copy_crs(CONT_HOLE%jacobi(tn_), CONT_HOLE%jacobi_ac(tn_))
  CONT_HOLE%jacobi_ac(tn_)%active =.true.
endif

endsubroutine

! init jacobian with respect to bohm potential of holes
subroutine init_cont_hole_qfp

integer :: n,i


! --> number of nonzeros
n = 0
do i=1,N_SP
  if (SP(i)%set_qfp) then
    n = n+1

  else
    n = n+1
    if (SP(i)%x_low_exist) n = n+1
    if (SP(i)%x_upp_exist) n = n+1
    if (SP(i)%y_low_exist) n = n+1
    if (SP(i)%y_upp_exist) n = n+1

  endif
enddo

! --> get size
call CONT_HOLE%jacobi(phip_)%init(nr=N_SP, nc=N_SP, nz=n)
CONT_HOLE%jacobi(phip_)%active=.true.

! --> set row_index/columns
n=0
do i=1,N_SP
  CONT_HOLE%jacobi(phip_)%row_index(i) = n+1
  if (SP(i)%set_qfp) then
    n = n+1
    CONT_HOLE%jacobi(phip_)%columns(n) = i
    SP(i)%id_cont_qfp         = n

  else
    if (SP(i)%y_low_exist) then
      n = n+1
      CONT_HOLE%jacobi(phip_)%columns(n) = SP(i)%id_sy_low
      SP(i)%id_cont_qfp_y_low   = n
    endif
    if (SP(i)%x_low_exist) then
      n = n+1
      CONT_HOLE%jacobi(phip_)%columns(n) = SP(i)%id_sx_low
      SP(i)%id_cont_qfp_x_low   = n
    endif
    n = n+1
    CONT_HOLE%jacobi(phip_)%columns(n) = i
    SP(i)%id_cont_qfp                  = n
    if (SP(i)%x_upp_exist) then
      n = n+1
      CONT_HOLE%jacobi(phip_)%columns(n) = SP(i)%id_sx_upp
      SP(i)%id_cont_qfp_x_upp   = n
    endif
    if (SP(i)%y_upp_exist) then
      n = n+1
      CONT_HOLE%jacobi(phip_)%columns(n) = SP(i)%id_sy_upp
      SP(i)%id_cont_qfp_y_upp   = n
    endif
    ! do j=1,N_SP
    !   if     (j.eq.i) then
    !     n = n+1
    !     CONT_HOLE%jacobi(phip_)%columns(n) = j
    !     SP(i)%id_cont_qfp         = n

    !   elseif (j.eq.SP(i)%id_sx_low) then
    !     n = n+1
    !     CONT_HOLE%jacobi(phip_)%columns(n) = j
    !     SP(i)%id_cont_qfp_x_low   = n

    !   elseif (j.eq.SP(i)%id_sx_upp) then
    !     n = n+1
    !     CONT_HOLE%jacobi(phip_)%columns(n) = j
    !     SP(i)%id_cont_qfp_x_upp   = n

    !   elseif (j.eq.SP(i)%id_sy_low) then
    !     n = n+1
    !     CONT_HOLE%jacobi(phip_)%columns(n) = j
    !     SP(i)%id_cont_qfp_y_low   = n

    !   elseif (j.eq.SP(i)%id_sy_upp) then
    !     n = n+1
    !     CONT_HOLE%jacobi(phip_)%columns(n) = j
    !     SP(i)%id_cont_qfp_y_upp   = n

    !   endif
    ! enddo
  endif
enddo
CONT_HOLE%jacobi(phip_)%row_index(N_SP+1) = n+1

! --> complex matrix for AC
if (AC_SIM) then !todo
  call copy_crs(CONT_HOLE%jacobi(phip_), CONT_HOLE%jacobi_ac(phip_))
  CONT_HOLE%jacobi_ac(phip_)%active =.true.
  allocate(CONT_HOLE%delta_ac(size(CONT_HOLE%delta)))
endif

endsubroutine

! init jacobian with respect to lattice temperature
subroutine init_cont_hole_tl

integer :: n

! --> get size
call CONT_HOLE%jacobi(tl_)%init(nr=N_SP, nc=1, nz=N_SP)
CONT_HOLE%jacobi(tl_)%active=.true.

! --> set row_index/columns
CONT_HOLE%jacobi(tl_)%columns     = 1 !all values in first column
do n=1,N_SP !every value new row
  CONT_HOLE%jacobi(tl_)%row_index(n)   = n
enddo
CONT_HOLE%jacobi(tl_)%row_index(N_SP+1)   = N_SP + 1

! --> complex matrix for AC
if (AC_SIM) then !todo
  call copy_crs(CONT_HOLE%jacobi(tl_), CONT_HOLE%jacobi_ac(tl_))
  CONT_HOLE%jacobi_ac(tl_)%active =.true.
endif

endsubroutine

! init jacobian with respect to electron quasi Fermi potential first conduction band
subroutine init_cont_hole_qfn

integer :: i

! --> get size
call CONT_HOLE%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
CONT_HOLE%jacobi(phin_)%active=.true.

! --> set row_index/columns
do i=1,N_SP
  CONT_HOLE%jacobi(phin_)%row_index(i)  = i
  CONT_HOLE%jacobi(phin_)%columns(i)    = i
enddo
CONT_HOLE%jacobi(phin_)%row_index(N_SP+1) = N_SP+1

! --> complex matrix for AC
if (AC_SIM) then !todo
  call copy_crs(CONT_HOLE%jacobi(phin_), CONT_HOLE%jacobi_ac(phin_))
  CONT_HOLE%jacobi_ac(phin_)%active =.true.
endif

endsubroutine

! init jacobian with respect to electron quasi Fermi potential second conduction band
subroutine init_cont_hole_qfn2

integer :: i

! --> get size
call CONT_HOLE%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
CONT_HOLE%jacobi(phin2_)%active=.true.

! --> set row_index/columns
do i=1,N_SP
  CONT_HOLE%jacobi(phin2_)%row_index(i)  = i
  CONT_HOLE%jacobi(phin2_)%columns(i)    = i
enddo
CONT_HOLE%jacobi(phin2_)%row_index(N_SP+1) = N_SP+1

! --> complex matrix for AC
if (AC_SIM) then !todo
  call copy_crs(CONT_HOLE%jacobi(phin2_), CONT_HOLE%jacobi_ac(phin2_))
  CONT_HOLE%jacobi_ac(phin2_)%active =.true.
endif

endsubroutine



! init jacobian with respect to electrostatic potential
subroutine init_cont_hole_psi

integer :: n,i,j,m
logical :: first


!if (.not.DD_TUNNEL) then
if (.true.) then !tunneling curently not implemented for holes

  ! --> without tunneling -----------------------------------------------------
  ! --> get size (same as electrons)
  n = 0
  do i=1,N_SP
      n = n + 1
      if (SP(i)%x_low_exist) then
          n = n + 1
      endif
      if (SP(i)%x_upp_exist) then
          n = n + 1
      endif
      if (SP(i)%y_low_exist) then
          n = n + 1
      endif
      if (SP(i)%y_upp_exist) then
          n = n + 1
      endif
  enddo

  call CONT_HOLE%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
  CONT_HOLE%jacobi(psi_)%active=.true.


  ! --> set row_index/columns
  n=0
  do i=1,N_SP
    CONT_HOLE%jacobi(psi_)%row_index(i) = n+1
      if (SP(i)%y_low_exist) then
        n = n+1
        CONT_HOLE%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_low)%id_p
        SP(i)%id_cont_psi_y_low   = n
      endif
      if (SP(i)%x_low_exist) then
        n = n+1
        CONT_HOLE%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_low)%id_p
        SP(i)%id_cont_psi_x_low   = n
      endif
      n = n+1
      CONT_HOLE%jacobi(psi_)%columns(n)  = SP(i)%id_p
      SP(i)%id_cont_psi                  = n
      if (SP(i)%x_upp_exist) then
        n = n+1
        CONT_HOLE%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_upp)%id_p
        SP(i)%id_cont_psi_x_upp   = n
      endif
      if (SP(i)%y_upp_exist) then
        n = n+1
        CONT_HOLE%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_upp)%id_p
        SP(i)%id_cont_psi_y_upp   = n
      endif
      ! do j=1,N_SP
      !   if     (j.eq.i) then
      !     n = n+1
      !     if (SP(j)%psi_offset) then
      !     CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
      !     else
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
      !     endif
      !     SP(i)%id_cont_psi        = n
    
      !   elseif (j.eq.SP(i)%id_sx_low) then
      !     n = n+1
      !     if (SP(j)%psi_offset) then
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
      !     else
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
      !   endif
      !     SP(i)%id_cont_psi_x_low  = n

      !   elseif (j.eq.SP(i)%id_sx_upp) then
      !     n = n + 1
      !     if (SP(j)%psi_offset) then
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
      !     else
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
      !     endif
      !     SP(i)%id_cont_psi_x_upp  = n

      !   elseif (j.eq.SP(i)%id_sy_low) then
      !     if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
      !       n = n + 1
      !       CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
      !       SP(i)%id_cont_psi_y_low  = n
      !     endif

      !   elseif (j.eq.SP(i)%id_sy_upp) then
      !     if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
      !       n = n+1
      !       CONT_HOLE%jacobi(psi_)%columns(n)  = SP(j)%id_p
      !       SP(i)%id_cont_psi_y_upp   = n
      !     endif

      !   endif
      ! enddo
  enddo
  CONT_HOLE%jacobi(psi_)%row_index(N_SP+1) = n+1

else
  ! --> with tunneling --------------------------------------------------------
  ! --> get size (same as elec)
  n = 0
  do i=1,N_SP
    if (SP(i)%set_qfp) then
      n = n + 1

    else
      do j=1,N_SP
        if     (j.eq.i) then
          if (SP(i)%psi_mean) then
            n = n + size(SP(i)%id_p_get_mean)
          else
            n = n + 1
          endif

        elseif (j.eq.SP(i)%id_sx_low) then
          if (SP(i)%psi_mean) then
            n = n + size(SP(i)%id_p_get_mean)
          else
            n = n + 1
          endif

        elseif (j.eq.SP(i)%id_sx_upp) then
          if (SP(i)%psi_mean) then
            n = n + size(SP(i)%id_p_get_mean)
          else
            n = n + 1
          endif

        elseif (j.eq.SP(i)%id_sy_low) then
          if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset).and.(.not.SP(i)%psi_mean)) then
            n = n + 1
          endif

        elseif (j.eq.SP(i)%id_sy_upp) then
          if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset).and.(.not.SP(i)%psi_mean)) then
            n = n + 1
          endif

        elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
          if (SP(i)%psi_mean) then
            n = n + size(SP(i)%id_p_get_mean)
          else
            n = n + 1
          endif

        endif
      enddo

    endif
  enddo

  call CONT_HOLE%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
  CONT_HOLE%jacobi(psi_)%active=.true.

  ! --> set row_index/columns
  n=0
  do i=1,N_SP
    CONT_HOLE%jacobi(psi_)%row_index(i) = n+1
    if (SP(i)%set_qfp) then
      n = n+1
      CONT_HOLE%jacobi(psi_)%columns(n) = SP(i)%id_p
      SP(i)%id_cont_psi        = n

    else
      if (SP(i)%psi_mean) then
        if (.not.DD_ELEC) then
          allocate(SP(i)%id_cont_psi_mean(      size(SP(i)%id_p_get_mean)))
          allocate(SP(i)%id_cont_psi_x_low_mean(size(SP(i)%id_p_get_mean)))
          allocate(SP(i)%id_cont_psi_x_upp_mean(size(SP(i)%id_p_get_mean)))
          allocate(SP(i)%id_cont_psi_x_tun_mean(size(SP(i)%id_p_get_mean)))
        endif
        do m=1,size(SP(i)%id_p_get_mean)
          first = .true.
          do j=1,N_SP
            if     (j.eq.i) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
              SP(i)%id_cont_psi_mean(m) = n
              if (first) then
                SP(i)%id_cont_psi_x_tun_mean(m) = n
                first = .false.
              endif
          
            elseif (j.eq.SP(i)%id_sx_low) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n)   = SP(j)%id_p_get_mean(m)
              SP(i)%id_cont_psi_x_low_mean(m) = n
              if (first) then
                SP(i)%id_cont_psi_x_tun_mean(m) = n
                first = .false.
              endif

            elseif (j.eq.SP(i)%id_sx_upp) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
              SP(i)%id_cont_psi_x_upp_mean(m)   = n

            elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
              if (first) then
                SP(i)%id_cont_psi_x_tun_mean(m) = n
                first = .false.
              endif

            endif

          enddo
        enddo

      else
        first = .true.
        do j=1,N_SP
          if     (j.eq.i) then
            n = n+1
            if (SP(i)%psi_offset) then
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
            else
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
            endif
            SP(i)%id_cont_psi         = n
            if (first) then
              SP(i)%id_cont_psi_x_tun = n
              first = .false.
            endif
          
          elseif (j.eq.SP(i)%id_sx_low) then
            n = n+1
            if (SP(j)%psi_offset) then
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
            else
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
            endif
            SP(i)%id_cont_psi_x_low   = n
            if (first) then
              SP(i)%id_cont_psi_x_tun = n
              first = .false.
            endif

          elseif (j.eq.SP(i)%id_sx_upp) then
            n = n+1
            if (SP(j)%psi_offset) then
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
            else
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
            endif
            SP(i)%id_cont_psi_x_upp   = n

          elseif (j.eq.SP(i)%id_sy_low) then
            if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n)  = SP(j)%id_p
              SP(i)%id_cont_psi_y_low   = n
            endif

          elseif (j.eq.SP(i)%id_sy_upp) then
            if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
              n = n+1
              CONT_HOLE%jacobi(psi_)%columns(n)  = SP(j)%id_p
              SP(i)%id_cont_psi_y_upp   = n
            endif

          elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
            n = n+1
            if (SP(j)%psi_offset) then
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p_get
            else
              CONT_HOLE%jacobi(psi_)%columns(n) = SP(j)%id_p
            endif
            if (first) then
              SP(i)%id_cont_psi_x_tun = n
              first = .false.
            endif

          endif

        enddo
      endif
    endif
  enddo
  CONT_HOLE%jacobi(psi_)%row_index(N_SP+1) = n+1
endif

! --> check crs
call CONT_HOLE%jacobi(psi_)%check

! --> complex matrix for AC
if (AC_SIM) then !todo
    call copy_crs(CONT_HOLE%jacobi(psi_), CONT_HOLE%jacobi_ac(psi_))
    CONT_HOLE%jacobi_ac(psi_)%active =.true.
endif
  
endsubroutine

! set values of hole continuity equation
subroutine set_cont_hole(simul)

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx=0,my=0,n=0,m=0,px=0,py=0 
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_px=0,j_px_dqfm=0,j_px_dqfp=0,j_px_dpsim=0,j_px_dpsip=0,j_px_dtlp=0,j_px_dtlm=0
    real(8) :: j_mx=0,j_mx_dqfm=0,j_mx_dqfp=0,j_mx_dpsim=0,j_mx_dpsip=0,j_mx_dtlp=0,j_mx_dtlm=0
    real(8) :: j_py=0,j_py_dqfm=0,j_py_dqfp=0,j_py_dpsim=0,j_py_dpsip=0
    real(8) :: j_my=0,j_my_dqfm=0,j_my_dqfp=0,j_my_dpsim=0,j_my_dpsip=0
    ! real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension
    
    if (.not.(CONT_HOLE%active)) return

    ! allocate(scal(CHARGE_DIM))
    
    ! ---> nullify data fields
    CONT_HOLE%rhs                     = 0 ! right hand side
    CONT_HOLE%jacobi(phip_)%values    = 0 ! nonzero elements in matrix
    if (simul) then
      CONT_HOLE%jacobi(psi_)%values   = 0
    endif
    if (DD_ELEC.and.simul) then
      CONT_HOLE%jacobi(phin_)%values  = 0
    endif
    if (DD_TN.and.simul) then
      CONT_HOLE%jacobi(tn_)%values  = 0
    endif
    if (DD_ELEC2.and.simul) then
      CONT_HOLE%jacobi(phin2_)%values  = 0
    endif
    if (DD_TL.and.simul) then
      CONT_HOLE%jacobi(tl_)%values = 0
    endif
    if (DD_CONT_BOHM_N.and.simul) then
      CONT_HOLE%jacobi(ln_)%values = 0
    endif

    is_2d      = SEMI_DIM.ge.2
    dy         = 1

    ! --> loop over position
    do n=1,N_SP
      mx = SP(n)%id_sx_low
      px = SP(n)%id_sx_upp
      dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
      if (is_2d) then
        my = SP(n)%id_sy_low
        py = SP(n)%id_sy_upp
        dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
      endif
      
      ! scal=get_scal(dx,dy,CHARGE_DIM)
    
      if (SP(n)%set_qfp) then
        ! ohmic contact, qfp=psi
        CONT_HOLE%rhs(n)    = CONT_HOLE%var(n) - get_bias(SP(n)%cont_id)
        
        ! A(i,i  ) ---------------------------------------------
        CONT_HOLE%jacobi(phip_)%values(SP(n)%id_cont_qfp) = dble(1)
    
        ! This should not be here. Discuss with Sven
        ! if (simul) then
        !   CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) = -dble(1)
        ! endif
    
      elseif (SP(n)%set_neutrality_p) then
        ! holes are majorities
        CONT_HOLE%rhs(n) = (CD(n) - CN(n) + CP(n))*Q
        ! A(i,i  ) ---------------------------------------------
        CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp ) = DP(n)%dens_dqf*Q
        if (simul) then
          CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) =(DN(n)%dens_dqf - DP(n)%dens_dqf)*Q
        endif
        if (DD_ELEC.and.simul) then
          CONT_HOLE%jacobi(phin_)%values(n)                 = -DN(n)%dens_dqf*Q
        endif
    
      else
        ! current boundary
        j_px       = DP(n)%j_xh           ! (i+1/2)
        j_px_dqfm  = DP(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
        j_px_dpsim = DP(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
        j_px_dqfp  = DP(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
        j_px_dpsip = DP(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
        
        if (SP(n)%x_low_exist) then
          j_mx       = DP(mx)%j_xh          ! (i-1/2)
          j_mx_dqfm  = DP(mx)%j_xh_dqf(1)   ! (i-1/2)/(i-1)
          j_mx_dpsim = DP(mx)%j_xh_dpsi(1)  ! (i-1/2)/(i-1)
          j_mx_dqfp  = DP(mx)%j_xh_dqf(2)   ! (i-1/2)/(i)
          j_mx_dpsip = DP(mx)%j_xh_dpsi(2)  ! (i-1/2)/(i)
          
        else
          j_mx       = 0  ! (i-1/2)
          j_mx_dqfm  = 0  ! (i-1/2)/(i-1)
          j_mx_dpsim = 0  ! (i-1/2)/(i-1)
          j_mx_dqfp  = 0  ! (i-1/2)/(i)
          j_mx_dpsip = 0  ! (i-1/2)/(i)
          
        endif
    
        if (is_2d) then
          j_py       = DP(n)%j_yh           ! (i+1/2)
          j_py_dqfm  = DP(n)%j_yh_dqf(1)    ! (i+1/2)/(i)
          j_py_dpsim = DP(n)%j_yh_dpsi(1)   ! (i+1/2)/(i)
          j_py_dqfp  = DP(n)%j_yh_dqf(2)    ! (i+1/2)/(i+1)
          j_py_dpsip = DP(n)%j_yh_dpsi(2)   ! (i+1/2)/(i+1)
        
          if (SP(n)%y_low_exist) then
            j_my       = DP(my)%j_yh          ! (i-1/2)
            j_my_dqfm  = DP(my)%j_yh_dqf(1)   ! (i-1/2)/(i-1)
            j_my_dpsim = DP(my)%j_yh_dpsi(1)  ! (i-1/2)/(i-1)
            j_my_dqfp  = DP(my)%j_yh_dqf(2)   ! (i-1/2)/(i)
            j_my_dpsip = DP(my)%j_yh_dpsi(2)  ! (i-1/2)/(i)
          
          else
            j_my       = 0  ! (i-1/2)
            j_my_dqfm  = 0  ! (i-1/2)/(i-1)
            j_my_dpsim = 0  ! (i-1/2)/(i-1)
            j_my_dqfp  = 0  ! (i-1/2)/(i)
            j_my_dpsip = 0  ! (i-1/2)/(i)
          
          endif
        endif
    
        ! --> schottky contact point
        if (SP(n)%cont_left) then
          ! --> schottky contact at left side
          j_mx       = DP(n)%j_con       ! contact (i)
          j_mx_dqfm  = 0
          j_mx_dqfp  = DP(n)%j_con_dqf   ! contact (i)/(i)
          j_mx_dpsim = 0                 
          j_mx_dpsip = DP(n)%j_con_dpsi  ! contact (i)/(i)
    
        endif
     
        if (SP(n)%cont_right) then
          ! --> drain contact at right side
          j_px       = -DP(n)%j_con       ! contact (i)
          j_px_dqfm  = -DP(n)%j_con_dqf   ! contact (i)/(i)
          j_px_dqfp  = 0
          j_px_dpsim = -DP(n)%j_con_dpsi  ! contact (i)/(i)
          j_px_dpsip = 0  
    
        endif
        
        if (is_2d) then
          if (SP(n)%cont_lower) then
            ! --> contact at lower side
            j_my       = DP(n)%j_con       ! contact (i)
            j_my_dqfm  = 0
            j_my_dqfp  = DP(n)%j_con_dqf   ! contact (i)/(i)
            j_my_dpsim = 0  
            j_my_dpsip = DP(n)%j_con_dpsi  ! contact (i)/(i)
          endif
    
          if (SP(n)%cont_upper) then
            ! --> contact at upper side
            j_py       = -DP(n)%j_con       ! contact (i)
            j_py_dqfm  = -DP(n)%j_con_dqf   ! contact (i)/(i)
            j_py_dqfp  = 0
            j_py_dpsim = -DP(n)%j_con_dpsi
            j_py_dpsip = 0
          endif
    
        endif

        ! --> RHS -------------------------------------------------
        CONT_HOLE%rhs(n) = (j_px - j_mx)*dy + (j_py - j_my)*dx + dx*dy*(DP(n)%recomb  + DP(n)%g_tun)
    
        ! --> A(i,i-NX) ---------------------------------------------
        if (SP(n)%y_low_exist) then
          CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp_y_low ) = -j_my_dqfm *dx
          if (simul) then
            if (SP(n)%psi_mean) then ! is_2d=fake
            elseif (SP(n)%psi_offset.or.SP(my)%psi_offset) then ! is_2d=fake
              CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) - ( - j_my_dpsip)*dx ! no y-dependence (sub from main)
            else
              CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_y_low) = -j_my_dpsim*dx
            endif
          endif
        endif
    
        ! --> A(i,i-1) ---------------------------------------------
        if (SP(n)%x_low_exist) then
          CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp_x_low ) = -j_mx_dqfm *dy + dx*dy*(DP(n)%recomb_dqf(1) )!+ DP(n)%g_tun_dqf(1))
          if (simul) then
            CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_low) = (-j_mx_dpsim + dx*(DP(n)%recomb_dpsi(1)))*dy
          endif
          if (simul.and.DD_TL) CONT_HOLE%jacobi(tl_)%values(n) = CONT_HOLE%jacobi(tl_)%values(n) - j_mx_dtlm*dy
        endif
    
        ! --> A(i,i  ) ---------------------------------------------
        CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp ) = (j_px_dqfm  - j_mx_dqfp )*dy + (j_py_dqfm  - j_my_dqfp )*dx + dx*dy*(DP(n)%recomb_dqf(2)  )!+ DP(n)%g_tun_dqf(2))
        if (simul) then
          CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) = (j_px_dpsim - j_mx_dpsip)*dy + (j_py_dpsim - j_my_dpsip)*dx + dx*dy*(DP(n)%recomb_dpsi(2))
        endif
       !if (DD_ELEC.and.simul.and.(.not.SP(n)%set_qfn)) then
        if (DD_ELEC.and.simul) then
          CONT_HOLE%jacobi(phin_)%values(n)                 =  dx*dy*DN(n)%recomb_dqf(2) ! other derivates ignored
        endif
        if (simul.and.DD_TL) then 
          CONT_HOLE%jacobi(tl_)%values(n) =  CONT_HOLE%jacobi(tl_)%values(n) + (j_px_dtlm - j_mx_dtlp)*dy + (0 - 0)*dx + dx*dy*(DP(n)%recomb_dtl(2))
        endif
    
    
        ! --> A(i,i+1) ---------------------------------------------
        if (SP(n)%x_upp_exist) then
          CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp_x_upp ) =  j_px_dqfp *dy  + dx*dy*(DP(n)%recomb_dqf(3)  )!+ DP(n)%g_tun_dqf(3))
          if (simul) then
            CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_upp) =  j_px_dpsip*dy + dx*dy*(DP(n)%recomb_dpsi(3))
          endif
          if (simul.and.DD_TL) CONT_HOLE%jacobi(tl_)%values(n) = CONT_HOLE%jacobi(tl_)%values(n) + j_px_dtlp*dy
        endif
    
        ! --> A(i,i+NX) ---------------------------------------------
        if (SP(n)%y_upp_exist) then
          CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp_y_upp ) =  j_py_dqfp *dx
          if (simul) then
            CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_y_upp) =  j_py_dpsip*dx
          endif
        endif
    
        ! --> TRANSIENT ---------------------------------
        if (TR_IT.ge.1) then
          ! backward euler:
          CONT_HOLE%rhs(n) = CONT_HOLE%rhs(n) + dx*dy*Q*(CP(n) - CP_LAST(n) )/TR_DT(TR_IT)!+ CP_TRAP(n,CHARGE_DIM) - CP_TRAP_LAST(n,CHARGE_DIM)
          ! CONT_HOLE%rhs(n) = CONT_HOLE%rhs(n) + dot_product(scal, CP_TRAP(n,:) - CP_TRAP_LAST(n,:) )/TR_DT(TR_IT)    !all traps                       
    
          CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp ) = CONT_HOLE%jacobi(phip_)%values(SP(n)%id_cont_qfp ) + dx*dy*Q*DP(n)%dens_dqf/TR_DT(TR_IT)
          ! CONT_HOLE%jacobi(phip_)%values(  SP(n)%id_cont_qfp ) = CONT_HOLE%jacobi(phip_)%values(SP(n)%id_cont_qfp ) + dot_product(scal, DP_TRAP(n,:)%dens_dqf * DD_TRAP_FILTER )/TR_DT(TR_IT) !all but TRAP_DIM                      )/TR_DT(TR_IT)
    
          if (simul) then
            if (SP(n)%psi_mean) then ! is_2d=.false.
              do m=1,size(SP(n)%id_p_get_mean)
                CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) - dx*dy*Q*DP(n)%dens_dqf/TR_DT(TR_IT)/size(SP(n)%id_p_get_mean)
                ! CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) - dot_product(scal, DP_TRAP(n,:)%dens_dqf )/TR_DT(TR_IT)/size(SP(n)%id_p_get_mean) !all traps 
              enddo
            else
              CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) - dx*dy*Q*DP(n)%dens_dqf/TR_DT(TR_IT)
              ! CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi) - dot_product(scal, DP_TRAP(n,:)%dens_dqf )/TR_DT(TR_IT) !all traps
            endif
    
            ! if (CONT_TRAP_HOLE%active) then
            !   CONT_HOLE%jacobi(phipt_)%values(  n            ) = CONT_HOLE%jacobi(phipt_)%values(n                ) + scal(TRAP_DIM)*DP_TRAP(n,TRAP_DIM)%dens_dqf/TR_DT(TR_IT) !only TRAP_DIM
    
            ! endif
          endif
        endif
    
        ! ! --> Tunneling simul to psi ---------------
        ! if (simul) then
        !   if (DD_TUNNEL) then
        !     if (SP(n)%psi_mean) then ! is_2d=.false.
        !       do m=1,size(SP(n)%id_p_get_mean)
        !         do j=1,size(DN(n)%g_tun_dpsi)
        !           CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun_mean(m)-1+j) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun_mean(m)-1+j) + dx*dy*DP(n)%g_tun_dpsi(j)/size(SP(n)%id_p_get_mean)
        !         enddo
        !       enddo
        !     else
        !       do j=1,size(DP(n)%g_tun_dpsi)
        !         CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun-1+j) = CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun-1+j) + dx*dy*DP(n)%g_tun_dpsi(j)
        !       enddo
        !     endif
        !   endif
        ! endif
    
      endif
    
    enddo
    
    CONT_HOLE%rhs  = -CONT_HOLE%rhs
    
    ! deallocate(scal)
    
endsubroutine

! set ac values of hole continuity equation
subroutine set_cont_hole_ac(omega)

real(8),intent(in) :: omega !< 2*pi*frequency

integer :: n
real(8) :: dx,dy
logical :: is_2d
real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension

if (.not.(CONT_HOLE%active)) return

allocate(scal(CHARGE_DIM))

is_2d = SEMI_DIM.ge.2
dy    = 1

do n=1,N_SP
              dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
  if (is_2d)  dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)

  scal=get_scal(dx,dy,CHARGE_DIM)

  if (.not.(SP(n)%set_qfp.or.SP(n)%set_neutrality_p)) then
    !all carriers
    CONT_HOLE%jacobi_ac(phip_)%values(SP(n)%id_cont_qfp ) = cmplx(CONT_HOLE%jacobi(phip_)%values(SP(n)%id_cont_qfp ),omega*dx*dy*( DP(n)%dens_dqf ),8)

    !all carriers
    CONT_HOLE%jacobi_ac(psi_)%values(SP(n)%id_cont_psi) = cmplx(CONT_HOLE%jacobi(psi_)%values(SP(n)%id_cont_psi),omega*dx*dy*(-DP(n)%dens_dqf),8)

  endif
enddo


deallocate(scal)

endsubroutine


endmodule
