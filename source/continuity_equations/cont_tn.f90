!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!continuity equation of electrons
module continuity_tn

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use continuity_elec   , only : CONT_ELEC
use continuity_poi   , only : CONT_POI
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types
use mobility, only: get_taun_dual

implicit none

type, extends(cont) :: cont_tn_ 
  contains
  !these methods of the parent class are overwritten
  procedure,nopass   :: init   => init_cont_tn
  procedure,nopass   :: set    => set_cont_tn
  procedure,nopass   :: set_ac => set_cont_tn_ac
  procedure,nopass   :: init_cont_tn_tn
  procedure,nopass   :: init_cont_tn_qfn
  procedure,nopass   :: init_cont_tn_qfn2
  procedure,nopass   :: init_cont_tn_qfp
  procedure,nopass   :: init_cont_tn_psi
  procedure,nopass   :: init_cont_tn_tl
  procedure,nopass   :: init_cont_tn_ln
endtype

type(cont_tn_),target             :: CONT_TN

public CONT_TN

contains

subroutine init_cont_tn(var)
integer :: var,n
    if (.not.CONT_TN%active) then
      allocate(CONT_TN%jacobi(nvar-1))
      allocate(CONT_TN%jacobi_ac(nvar-1))
      allocate(CONT_TN%rhs(N_SP))
      allocate(CONT_TN%delta(N_SP))
      allocate(CONT_TN%var(N_SP))
      allocate(CONT_TN%var_last(N_SP))
      CONT_TN%var                 = TEMP_LATTICE !initial solution is just lattice temperature
      CONT_TN%var_last            = 0
      CONT_TN%rhs                 = 0
      CONT_TN%delta               = 0
      CONT_TN%active              = .true.
      CONT_TN%jacobi(:)%active    = .false.
      CONT_TN%jacobi_ac(:)%active = .false.
      CONT_TN%i_var               = tn_

      CONT_TN%atol        = US%cont_tn%atol
      CONT_TN%rtol        = US%cont_tn%rtol
      CONT_TN%ctol        = US%cont_tn%ctol
      CONT_TN%delta_max   = US%cont_tn%delta_max


      !obtain band for electron continuity equation
      do n=1,size(SEMI(1)%band_pointer)
        if (SEMI(1)%band_pointer(n)%band%iscb) then
          CONT_TN%valley = get_conduction_band_index(SEMI(1))
          exit
        endif
      enddo

    endif
    if (var.eq.tn_) then
        call CONT_TN%init_cont_tn_tn
    elseif (var.eq.phin_) then
        call CONT_TN%init_cont_tn_qfn
    elseif (var.eq.phin2_) then
        call CONT_TN%init_cont_tn_qfn2
    elseif (var.eq.phip_) then
        call CONT_TN%init_cont_tn_qfp
    elseif (var.eq.psi_) then
        call CONT_TN%init_cont_tn_psi
    elseif (var.eq.tl_) then
        call CONT_TN%init_cont_tn_tl
    elseif (var.eq.ln_) then
        call CONT_TN%init_cont_tn_ln
    else
        write(*,*) '***error*** initialization routine of CONT_TN not implemented for this case.'
        stop
    endif
endsubroutine

subroutine init_cont_tn_ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_TN%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_TN%jacobi(ln_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_TN%jacobi(ln_)%row_index(i) = n+1

            if (SP(i)%y_low_exist) then
                n = n+1
                CONT_TN%jacobi(ln_)%columns(n) = SP(i)%id_sy_low
            endif
            if (SP(i)%x_low_exist) then
                n = n+1
                CONT_TN%jacobi(ln_)%columns(n) = SP(i)%id_sx_low
            endif
            n = n+1
            CONT_TN%jacobi(ln_)%columns(n) = i
            if (SP(i)%x_upp_exist) then
                n = n+1
                CONT_TN%jacobi(ln_)%columns(n) = SP(i)%id_sx_upp
            endif
            if (SP(i)%y_upp_exist) then
                n = n+1
                CONT_TN%jacobi(ln_)%columns(n) = SP(i)%id_sy_upp
            endif

    enddo
    CONT_TN%jacobi(ln_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(ln_), CONT_TN%jacobi_ac(ln_))
        CONT_TN%jacobi_ac(ln_)%active=.true.
    endif
endsubroutine

! 1D Version, outdated:
! subroutine init_cont_tn_tn
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     ! init electron continuituity jacobi with respect to phip
!     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     integer :: i

!     ! --> get size
!     call CONT_TN%jacobi(tn_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
!     CONT_TN%jacobi(tn_)%active=.true.

!     ! --> set row_index/columns
!     do i=1,N_SP
!         CONT_TN%jacobi(tn_)%row_index(i)  = i
!         CONT_TN%jacobi(tn_)%columns(i)    = i
!     enddo
!     CONT_TN%jacobi(tn_)%row_index(N_SP+1) = N_SP+1

! endsubroutine

subroutine init_cont_tn_tn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to phin
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i


    if (.not.DD_TUNNEL) then
        ! --> number of nonzeros
        n = 0
        do i=1,N_SP

                n = n+1
                if (SP(i)%x_low_exist) n = n+1
                if (SP(i)%x_upp_exist) n = n+1
                if (SP(i)%y_low_exist) n = n+1
                if (SP(i)%y_upp_exist) n = n+1

        enddo

        ! --> get size
        call CONT_TN%jacobi(tn_)%init(nr=N_SP, nc=N_SP, nz=n)
        CONT_TN%jacobi(tn_)%active=.true.

        ! --> set row_index/columns
        ! --> very slow for large structure ... maybe use POINT struct instead of SP?
        n=0
        do i=1,N_SP
            CONT_TN%jacobi(tn_)%row_index(i) = n+1
                if (SP(i)%y_low_exist) then
                    n = n+1
                    CONT_TN%jacobi(tn_)%columns(n) = SP(i)%id_sy_low
                endif
                if (SP(i)%x_low_exist) then
                    n = n+1
                    CONT_TN%jacobi(tn_)%columns(n) = SP(i)%id_sx_low
                endif
                n = n+1
                CONT_TN%jacobi(tn_)%columns(n) = i
                if (SP(i)%x_upp_exist) then
                    n = n+1
                    CONT_TN%jacobi(tn_)%columns(n) = SP(i)%id_sx_upp
                endif
                if (SP(i)%y_upp_exist) then
                    n = n+1
                    CONT_TN%jacobi(tn_)%columns(n) = SP(i)%id_sy_upp
                endif
        enddo
        CONT_TN%jacobi(tn_)%row_index(N_SP+1) = n+1
    else !with tunneling
        write(*,*) '***error*** cont_tn with tunnelling not implemented '
        stop

    endif

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(tn_), CONT_TN%jacobi_ac(tn_))
        CONT_TN%jacobi_ac(tn_)%active=.true.
        allocate(CONT_TN%delta_ac(size(CONT_TN%delta)))
    endif

endsubroutine

subroutine init_cont_tn_qfn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to phin
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i


    if (.not.DD_TUNNEL) then
        ! --> number of nonzeros
        n = 0
        do i=1,N_SP
                n = n+1
                if (SP(i)%x_low_exist) n = n+1
                if (SP(i)%x_upp_exist) n = n+1
                if (SP(i)%y_low_exist) n = n+1
                if (SP(i)%y_upp_exist) n = n+1
        enddo

        ! --> get size
        call CONT_TN%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=n)
        CONT_TN%jacobi(phin_)%active=.true.

        ! --> set row_index/columns
        ! --> very slow for large structure ... maybe use POINT struct instead of SP?
        n=0
        do i=1,N_SP
            CONT_TN%jacobi(phin_)%row_index(i) = n+1
                if (SP(i)%y_low_exist) then
                    n = n+1
                    CONT_TN%jacobi(phin_)%columns(n) = SP(i)%id_sy_low
                endif
                if (SP(i)%x_low_exist) then
                    n = n+1
                    CONT_TN%jacobi(phin_)%columns(n) = SP(i)%id_sx_low
                endif
                n = n+1
                CONT_TN%jacobi(phin_)%columns(n) = i
                if (SP(i)%x_upp_exist) then
                    n = n+1
                    CONT_TN%jacobi(phin_)%columns(n) = SP(i)%id_sx_upp
                endif
                if (SP(i)%y_upp_exist) then
                    n = n+1
                    CONT_TN%jacobi(phin_)%columns(n) = SP(i)%id_sy_upp
                endif
        enddo
        CONT_TN%jacobi(phin_)%row_index(N_SP+1) = n+1
    else !with tunneling
        write(*,*) '***error*** cont_tn with tunnelling not implemented '
        stop
    endif

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(phin_), CONT_TN%jacobi_ac(phin_))
        CONT_TN%jacobi_ac(phin_)%active=.true.
    endif

endsubroutine

subroutine init_cont_tn_qfp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Jacobian w.r.t. to qfp
    ! actually, this is nothing... TRY LATER
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i

    ! --> get size
    call CONT_TN%jacobi(phip_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_TN%jacobi(phip_)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
        CONT_TN%jacobi(phip_)%row_index(i)  = i
        CONT_TN%jacobi(phip_)%columns(i)    = i
    enddo
    CONT_TN%jacobi(phip_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(phip_), CONT_TN%jacobi_ac(phip_))
        CONT_TN%jacobi_ac(phip_)%active =.true.
    endif

endsubroutine

subroutine init_cont_tn_psi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! electron continuity equation coupling to electrostatic potential
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i,j

    if (.not.DD_TUNNEL) then ! for testing
        !if (.false.) then 
        ! --> get size
        n = 0
        do i=1,N_SP
            n = n + 1
            if (SP(i)%x_low_exist) then
                n = n + 1
            endif

            if (SP(i)%x_upp_exist) then
                n = n + 1
            endif

            if (SP(i)%y_low_exist) then
                n = n + 1
            endif

            if (SP(i)%y_upp_exist) then
                n = n + 1
            endif

        enddo

        call CONT_TN%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_TN%jacobi(psi_)%active =.true.

        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_TN%jacobi(psi_)%row_index(i) = n+1
            if (SP(i)%y_low_exist) then
                n = n+1
                CONT_TN%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_low)%id_p
                ! SP(i)%id_cont_elec_psi_y_low   = n
            endif
            if (SP(i)%x_low_exist) then
                n = n+1
                CONT_TN%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_low)%id_p
                ! SP(i)%id_cont_elec_psi_x_low   = n
            endif
            n = n+1
            CONT_TN%jacobi(psi_)%columns(n)  = SP(i)%id_p
            ! SP(i)%id_cont_elec_psi                  = n
            if (SP(i)%x_upp_exist) then
                n = n+1
                CONT_TN%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_upp)%id_p
                ! SP(i)%id_cont_elec_psi_x_upp   = n
            endif
            if (SP(i)%y_upp_exist) then
                n = n+1
                CONT_TN%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_upp)%id_p
                ! SP(i)%id_cont_elec_psi_y_upp   = n
            endif
        enddo
        CONT_TN%jacobi(psi_)%row_index(N_SP+1) = n+1
            
    else 
        ! --> get size
        n = 0
        do i=1,N_SP
            n = n + N_SP !depends on every point ...

        enddo

        call CONT_TN%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_TN%jacobi(psi_)%active =.true.

        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_TN%jacobi(psi_)%row_index(i) = n+1
            do j=1,N_SP
                n                                  = n+1
                CONT_TN%jacobi(psi_)%columns(n)  = SP(j)%id_p
                if (j.eq.i) then
                    SP(i)%id_cont_elec_psi         = n

                elseif (j.eq.SP(i)%id_sx_low) then
                    SP(i)%id_cont_elec_psi_x_low   = n

                elseif (j.eq.SP(i)%id_sx_upp) then
                    SP(i)%id_cont_elec_psi_x_upp   = n
                
                endif
            enddo
        enddo
        CONT_TN%jacobi(psi_)%row_index(N_SP+1) = n+1
        
    endif

    ! --> check crs
    call CONT_TN%jacobi(psi_)%check

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(psi_), CONT_TN%jacobi_ac(psi_))
        CONT_TN%jacobi_ac(psi_)%active =.true.
    endif

endsubroutine

subroutine init_cont_tn_tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init tn continuituity jacobi with respect to tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n

    write(*,*) '***error*** cont_tn with cont_tl not implemented '
    stop

    ! --> get size
    call CONT_TN%jacobi(tl_)%init(nr=N_SP, nc=1, nz=N_SP)
    CONT_TN%jacobi(tl_)%active=.true.

    ! --> set row_index/columns
    CONT_TN%jacobi(tl_)%columns     = 1 !all values in first column
    do n=1,N_SP !every value new row
        CONT_TN%jacobi(tl_)%row_index(n)   = n
    enddo
    CONT_TN%jacobi(tl_)%row_index(N_SP+1)   = N_SP + 1

    ! --> complex matrix for AC
    if (AC_SIM) then !todo
        call copy_crs(CONT_TN%jacobi(tl_), CONT_TN%jacobi_ac(tl_))
        CONT_TN%jacobi_ac(tl_)%active =.true.
    endif
endsubroutine

subroutine init_cont_tn_qfn2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init tn jacobi with respect to phin2. Not needed? Check!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
            n = n+1
            if (SP(i)%x_low_exist) n = n+1
            if (SP(i)%x_upp_exist) n = n+1
            if (SP(i)%y_low_exist) n = n+1
            if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_TN%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_TN%jacobi(phin2_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
    CONT_TN%jacobi(phin2_)%row_index(i) = n+1

        do j=1,N_SP
            if     (j.eq.i) then
                n = n+1
                CONT_TN%jacobi(phin2_)%columns(n) = i
            elseif (j.eq.SP(i)%id_sx_low) then
                n = n+1
                CONT_TN%jacobi(phin2_)%columns(n) = SP(i)%id_sx_low
            elseif (j.eq.SP(i)%id_sx_upp) then
                n = n+1
                CONT_TN%jacobi(phin2_)%columns(n) = SP(i)%id_sx_upp
            elseif (j.eq.SP(i)%id_sy_low) then
                n = n+1
                CONT_TN%jacobi(phin2_)%columns(n) = SP(i)%id_sy_low
            elseif (j.eq.SP(i)%id_sy_upp) then
                n = n+1
                CONT_TN%jacobi(phin2_)%columns(n) = SP(i)%id_sy_upp
            endif
        enddo
    enddo
    CONT_TN%jacobi(phin2_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_TN%jacobi(phin2_), CONT_TN%jacobi_ac(phin2_))
        CONT_TN%jacobi_ac(phin2_)%active=.true.
    endif
endsubroutine

subroutine set_cont_tn(simul)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set matrix values for solution of electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx=0,px=0,my=0,py=0,n=0,is,ib,is_left,is_right
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_px=0,j_px_dqfm=0,j_px_dqfp=0,j_px_dpsim=0,j_px_dpsip=0,j_px_dtnm=0, j_px_dtnp=0
    real(8) :: j_mx=0,j_mx_dqfm=0,j_mx_dqfp=0,j_mx_dpsim=0,j_mx_dpsip=0,j_mx_dtnm=0, j_mx_dtnp=0
    real(8) :: s_px=0, s_mx=0
    real(8) :: s_px_dtnm=0 , s_px_dtnp=0 , s_mx_dtnm=0 , s_mx_dtnp=0
    real(8) :: s_px_dpsim=0, s_px_dpsip=0, s_mx_dpsim=0, s_mx_dpsip=0
    real(8) :: s_px_dqfm=0 , s_px_dqfp=0 , s_mx_dqfm=0 , s_mx_dqfp=0
    real(8) :: phi_px=0,phi_mx=0
    real(8) :: phi_px_dphip=0,phi_px_dphim=0
    real(8) :: phi_mx_dphim=0,phi_mx_dphip=0
    real(8) :: phi_px_dpsip=0,phi_px_dpsim=0
    real(8) :: phi_mx_dpsim=0,phi_mx_dpsip=0


    real(8) :: j_py=0,j_py_dqfm=0,j_py_dqfp=0,j_py_dpsim=0,j_py_dpsip=0,j_py_dtnm=0, j_py_dtnp=0
    real(8) :: j_my=0,j_my_dqfm=0,j_my_dqfp=0,j_my_dpsim=0,j_my_dpsip=0,j_my_dtnm=0, j_my_dtnp=0
    real(8) :: s_py=0, s_my=0
    real(8) :: s_py_dtnm=0 , s_py_dtnp=0 , s_my_dtnm=0 , s_my_dtnp=0
    real(8) :: s_py_dpsim=0, s_py_dpsip=0, s_my_dpsim=0, s_my_dpsip=0
    real(8) :: s_py_dqfm=0 , s_py_dqfp=0 , s_my_dqfm=0 , s_my_dqfp=0
    real(8) :: phi_py=0,phi_my=0
    real(8) :: phi_py_dphip=0,phi_py_dphim=0
    real(8) :: phi_my_dphim=0,phi_my_dphip=0
    real(8) :: phi_py_dpsip=0,phi_py_dpsim=0
    real(8) :: phi_my_dpsim=0,phi_my_dpsip=0
    real(8) :: ec_mx, ec_n, ec_px


    real(8) :: rhs
    real(8) :: tau,fac,tau_dtn
    logical :: use_grad_phi = .False.
    type(DUAL_NUM) :: result
    ! real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension

    if (.not.(CONT_TN%active)) return

    use_grad_phi = .True.
    
    ! allocate(scal(CHARGE_DIM))
    
    ! ---> nullify data fields
    CONT_TN%rhs                     = 0 ! right hand side
    CONT_TN%jacobi(tn_)%values      = 0 ! nonzero elements in matrix
    if (simul) then
      CONT_TN%jacobi(psi_)%values   = 0
    endif
    if (DD_HOLE.and.simul) then
      CONT_TN%jacobi(phip_)%values  = 0
    endif
    if (DD_ELEC.and.simul) then
      CONT_TN%jacobi(phin_)%values = 0
    endif
    if (DD_ELEC2.and.simul) then
      CONT_TN%jacobi(phin2_)%values = 0
    endif
    if (DD_TL.and.simul) then
      CONT_TN%jacobi(tl_)%values = 0
    endif
    if (DD_CONT_BOHM_N.and.simul) then
      CONT_TN%jacobi(ln_)%values = 0
    endif
    
    is_2d      = SEMI_DIM.ge.2
    ! if (is_2d) then
    !     write(*,*) '***error*** electron energy equation not implemented for 2D !'
    !     stop
    ! endif
    dy         = 1 

    ! --> loop over position
    do n=1,N_SP
      mx = SP(n)%id_sx_low
      px = SP(n)%id_sx_upp
      dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
      if (is_2d) then
        my = SP(n)%id_sy_low
        py = SP(n)%id_sy_upp
        dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
      endif

      
      ! scal=get_scal(dx,dy,CHARGE_DIM)
      
      if (SP(n)%cont) then
            CONT_TN%rhs(n) = CONT_TN%var(n) - TEMP_LATTICE
            
            ! A(i,i  ) ---------------------------------------------
            CONT_TN%jacobi(tn_)%values(SP(n)%id_cont_qfn) = dble(1)
      else

        is           = SP(n)%model_id
        is_right     = SP(px)%model_id
        is_left      = SP(mx)%model_id
        ib           = get_conduction_band_index(SEMI(is))

        ! current boundary
        j_px       = DN(n)%j_xh*NORM%J_N           ! (i+1/2)
        j_px_dqfm  = DN(n)%j_xh_dqf(1)*NORM%J_N    ! (i+1/2)/(i)
        j_px_dpsim = DN(n)%j_xh_dpsi(1)*NORM%J_N   ! (i+1/2)/(i)
        j_px_dtnm  = DN(n)%j_xh_dtl(1)*NORM%J_N    ! (i+1/2)/(i)
        j_px_dqfp  = DN(n)%j_xh_dqf(2)*NORM%J_N    ! (i+1/2)/(i+1)
        j_px_dpsip = DN(n)%j_xh_dpsi(2)*NORM%J_N   ! (i+1/2)/(i+1)
        j_px_dtnp  = DN(n)%j_xh_dtl(2)*NORM%J_N    ! (i+1/2)/(i+1)

        s_px       = DN(n)%s_xh           ! (i-1/2)
        s_px_dqfm  = DN(n)%s_xh_dqf(1)    ! (i-1/2)/(i-1)
        s_px_dpsim = DN(n)%s_xh_dpsi(1)   ! (i-1/2)/(i-1)
        s_px_dtnm  = DN(n)%s_xh_dtl(1)    ! (i-1/2)/(i-1)
        s_px_dqfp  = DN(n)%s_xh_dqf(2)    ! (i-1/2)/(i)
        s_px_dpsip = DN(n)%s_xh_dpsi(2)   ! (i-1/2)/(i)
        s_px_dtnp  = DN(n)%s_xh_dtl(2)    ! (i-1/2)/(i)

        if (use_grad_phi) then
            phi_px       = (CONT_ELEC%var(px)  + CONT_ELEC%var(n)   )*dble(0.5)
            phi_px_dphip = +dble(0.5)
            phi_px_dphim = +dble(0.5)
            phi_px_dpsip = +dble(0)
            phi_px_dpsim = +dble(0)
        else
            !phi_px       = (CONT_POI%var(px)-PSI0(px)  + CONT_POI%var(n)-PSI0(n)   )*dble(0.5)
            ec_px        = SEMI(is_right)%band_pointer(ib)%band%e0-UN(px)-CONT_POI%var(px)
            ec_n         = SEMI(is)%band_pointer(ib)%band%e0-UN(n)-CONT_POI%var(n)
            phi_px       = -(ec_px +  ec_n)*dble(0.5)
            phi_px_dphip = +dble(0)
            phi_px_dphim = +dble(0)
            phi_px_dpsip = +dble(0.5)
            phi_px_dpsim = +dble(0.5)
        endif
 
       
        if (SP(n)%x_low_exist) then
            j_mx       = DN(mx)%j_xh*NORM%J_N          ! (i-1/2)
            j_mx_dqfm  = DN(mx)%j_xh_dqf(1)*NORM%J_N   ! (i-1/2)/(i-1)
            j_mx_dpsim = DN(mx)%j_xh_dpsi(1)*NORM%J_N  ! (i-1/2)/(i-1)
            j_mx_dtnm  = DN(mx)%j_xh_dtl(1)*NORM%J_N  ! (i-1/2)/(i-1)
            j_mx_dqfp  = DN(mx)%j_xh_dqf(2)*NORM%J_N   ! (i-1/2)/(i)
            j_mx_dpsip = DN(mx)%j_xh_dpsi(2)*NORM%J_N  ! (i-1/2)/(i)
            j_mx_dtnp  = DN(mx)%j_xh_dtl(2)*NORM%J_N  ! (i-1/2)/(i)

            s_mx       = DN(mx)%s_xh          ! (i-1/2)
            s_mx_dqfm  = DN(mx)%s_xh_dqf(1)   ! (i-1/2)/(i-1)
            s_mx_dpsim = DN(mx)%s_xh_dpsi(1)  ! (i-1/2)/(i-1)
            s_mx_dtnm  = DN(mx)%s_xh_dtl(1)  ! (i-1/2)/(i-1)
            s_mx_dqfp  = DN(mx)%s_xh_dqf(2)   ! (i-1/2)/(i)
            s_mx_dpsip = DN(mx)%s_xh_dpsi(2)  ! (i-1/2)/(i)
            s_mx_dtnp  = DN(mx)%s_xh_dtl(2)  ! (i-1/2)/(i)

            if ( use_grad_phi ) then
                phi_mx       = (CONT_ELEC%var(n) + CONT_ELEC%var(mx) )*dble(0.5)
                phi_mx_dphip = +dble(0.5)
                phi_mx_dphim = +dble(0.5)
                phi_mx_dpsip = +dble(0)
                phi_mx_dpsim = +dble(0)
            else
                !phi_mx       = (CONT_POI%var(n)-PSI0(n) + CONT_POI%var(mx)-PSI0(mx) )*dble(0.5)
                ec_n         = SEMI(is)%band_pointer(ib)%band%e0-UN(n)-CONT_POI%var(n)
                ec_mx        = SEMI(is_left)%band_pointer(ib)%band%e0-UN(mx)-CONT_POI%var(mx)
                phi_mx       = -(ec_n+ ec_mx)*dble(0.5)
                phi_mx_dpsip = +dble(0.5)
                phi_mx_dpsim = +dble(0.5)
                phi_mx_dphip = +dble(0)
                phi_mx_dphim = +dble(0)
            endif
          
        else
            j_mx       = 0  ! (i-1/2)
            j_mx_dqfm  = 0  ! (i-1/2)/(i-1)
            j_mx_dpsim = 0  ! (i-1/2)/(i-1)
            j_mx_dtnm  = 0  ! (i-1/2)/(i)
            j_mx_dqfp  = 0  ! (i-1/2)/(i)
            j_mx_dpsip = 0  ! (i-1/2)/(i)
            j_mx_dtnp = 0  ! (i-1/2)/(i)

            s_mx       = 0
            s_mx_dqfm  = 0
            s_mx_dpsim = 0
            s_mx_dtnm  = 0
            s_mx_dqfp  = 0
            s_mx_dpsip = 0
            s_mx_dtnp  = 0

            phi_mx       = 0
            phi_mx_dphip = 0
            phi_mx_dphim = 0
            phi_mx_dpsip = 0
            phi_mx_dpsim = 0
            phi_mx       = 0
            phi_mx_dpsip = 0
            phi_mx_dpsim = 0
            phi_mx_dphip = 0
            phi_mx_dphim = 0
          
        endif

        if (is_2d) then

            ! current boundary
            j_py       = DN(n)%j_yh*NORM%J_N           ! (i+1/2)
            j_py_dqfm  = DN(n)%j_yh_dqf(1)*NORM%J_N    ! (i+1/2)/(i)
            j_py_dpsim = DN(n)%j_yh_dpsi(1)*NORM%J_N   ! (i+1/2)/(i)
            j_py_dtnm  = DN(n)%j_yh_dtl(1)*NORM%J_N    ! (i+1/2)/(i)
            j_py_dqfp  = DN(n)%j_yh_dqf(2)*NORM%J_N    ! (i+1/2)/(i+1)
            j_py_dpsip = DN(n)%j_yh_dpsi(2)*NORM%J_N   ! (i+1/2)/(i+1)
            j_py_dtnp  = DN(n)%j_yh_dtl(2)*NORM%J_N    ! (i+1/2)/(i+1)
    
            s_py       = DN(n)%s_yh           ! (i-1/2)
            s_py_dqfm  = DN(n)%s_yh_dqf(1)    ! (i-1/2)/(i-1)
            s_py_dpsim = DN(n)%s_yh_dpsi(1)   ! (i-1/2)/(i-1)
            s_py_dtnm  = DN(n)%s_yh_dtl(1)    ! (i-1/2)/(i-1)
            s_py_dqfp  = DN(n)%s_yh_dqf(2)    ! (i-1/2)/(i)
            s_py_dpsip = DN(n)%s_yh_dpsi(2)   ! (i-1/2)/(i)
            s_py_dtnp  = DN(n)%s_yh_dtl(2)    ! (i-1/2)/(i)
    
            if (use_grad_phi) then
                phi_py       = (CONT_ELEC%var(py)  + CONT_ELEC%var(n)   )*dble(0.5)
                phi_py_dphip = +dble(0.5)
                phi_py_dphim = +dble(0.5)
                phi_py_dpsip = +dble(0)
                phi_py_dpsim = +dble(0)
            else
                phi_py       = (CONT_POI%var(py)-PSI0(py)  + CONT_POI%var(n)-PSI0(n)   )*dble(0.5)
                phi_py_dphip = +dble(0)
                phi_py_dphim = +dble(0)
                phi_py_dpsip = +dble(0.5)
                phi_py_dpsim = +dble(0.5)
            endif
     
           
            if (SP(n)%y_low_exist) then
                j_my       = DN(my)%j_yh*NORM%J_N          ! (i-1/2)
                j_my_dqfm  = DN(my)%j_yh_dqf(1)*NORM%J_N   ! (i-1/2)/(i-1)
                j_my_dpsim = DN(my)%j_yh_dpsi(1)*NORM%J_N  ! (i-1/2)/(i-1)
                j_my_dtnm  = DN(my)%j_yh_dtl(1)*NORM%J_N  ! (i-1/2)/(i-1)
                j_my_dqfp  = DN(my)%j_yh_dqf(2)*NORM%J_N   ! (i-1/2)/(i)
                j_my_dpsip = DN(my)%j_yh_dpsi(2)*NORM%J_N  ! (i-1/2)/(i)
                j_my_dtnp  = DN(my)%j_yh_dtl(2)*NORM%J_N  ! (i-1/2)/(i)
    
                s_my       = DN(my)%s_yh          ! (i-1/2)
                s_my_dqfm  = DN(my)%s_yh_dqf(1)   ! (i-1/2)/(i-1)
                s_my_dpsim = DN(my)%s_yh_dpsi(1)  ! (i-1/2)/(i-1)
                s_my_dtnm  = DN(my)%s_yh_dtl(1)  ! (i-1/2)/(i-1)
                s_my_dqfp  = DN(my)%s_yh_dqf(2)   ! (i-1/2)/(i)
                s_my_dpsip = DN(my)%s_yh_dpsi(2)  ! (i-1/2)/(i)
                s_my_dtnp  = DN(my)%s_yh_dtl(2)  ! (i-1/2)/(i)
    
                if ( use_grad_phi ) then
                    phi_my       = (CONT_ELEC%var(n) + CONT_ELEC%var(my) )*dble(0.5)
                    phi_my_dphip = +dble(0.5)
                    phi_my_dphim = +dble(0.5)
                    phi_my_dpsip = +dble(0)
                    phi_my_dpsim = +dble(0)
                else
                    phi_my       = (CONT_POI%var(n)-PSI0(n) + CONT_POI%var(my)-PSI0(my) )*dble(0.5)
                    phi_my_dpsip = +dble(0.5)
                    phi_my_dpsim = +dble(0.5)
                    phi_my_dphip = +dble(0)
                    phi_my_dphim = +dble(0)
                endif
              
            else
                j_my       = 0  ! (i-1/2)
                j_my_dqfm  = 0  ! (i-1/2)/(i-1)
                j_my_dpsim = 0  ! (i-1/2)/(i-1)
                j_my_dtnm  = 0  ! (i-1/2)/(i-1)
                j_my_dqfp  = 0  ! (i-1/2)/(i)
                j_my_dpsip = 0  ! (i-1/2)/(i)
                j_my_dtnp  = 0  ! (i-1/2)/(i)
    
                s_my       = 0
                s_my_dqfm  = 0
                s_my_dpsim = 0
                s_my_dtnm  = 0
                s_my_dqfp  = 0
                s_my_dpsip = 0
                s_my_dtnp  = 0
    
                phi_my       = 0
                phi_my_dphip = 0
                phi_my_dphim = 0
                phi_my_dpsip = 0
                phi_my_dpsim = 0
                phi_my       = 0
                phi_my_dpsip = 0
                phi_my_dpsim = 0
                phi_my_dphip = 0
                phi_my_dphim = 0
              
            endif
        endif

        fac                           = dble(1.5)*BK*dx*dy

        !from material model later
        is      = SP(n)%model_id
        ib      = CONT_TN%valley
        result                        = get_taun_dual(is,ib,&
            DUAL_NUM(CONT_TN%var(n),(/1,0,0,0,0,0,0,0/)), &
            DUAL_NUM(CONT_TN%var(n),(/1,0,0,0,0,0,0,0/)), &
            DUAL_NUM(TEMP_LATTICE,(/0,0,0,0,0,0,0,0/)), &
            GRADING(n) &
        )
        tau                           = result%x_ad_
        tau_dtn                       = result%xp_ad_(1)
        ! tau                           = 1e-12
        ! tau_dtn                       = 0

        ! For user output
        !0.5 because the dx we use here is between the point centers
        ! TODO: JouleHeat not correct for 2D case
        ! GPSI(n)                       = ((CONT_POI%var(px) + UN(px)) - (CONT_POI%var(mx)+UN(mx))  )/dx
        JouleHeat(n)                  = -j_px*GPSI(n)*dy -j_py*GPSI(n)*dx
        TAUE(n)                       = tau

        rhs                           = -(j_px*phi_px - j_mx*phi_mx + s_px - s_mx )*dy - fac * CN(n) *( CONT_TN%var(n)-TEMP_LATTICE )/tau
        rhs                           = -(j_py*phi_py - j_my*phi_my + s_py - s_my )*dx + rhs   
        CONT_TN%rhs(n)                = rhs
        ! --> A(i,i-NX) ---------------------------------------------
        if (SP(n)%y_low_exist) then
            CONT_TN%jacobi(tn_)%values(  SP(n)%id_cont_qfn_y_low ) = &
            - (-j_my_dtnm*phi_my-s_my_dtnm ) *dx 
            if (simul) then
                CONT_TN%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_y_low) = &
                - (- j_my_dpsim*phi_my - j_my*phi_my_dpsim - s_my_dpsim )*dx
                if (DD_ELEC) CONT_TN%jacobi(phin_)%values(SP(n)%id_cont_qfn_y_low) = & 
                - (- j_my_dqfm*phi_my - j_my*phi_my_dphim - s_my_dqfm )*dx                
            endif
        endif
        ! --> A(i,i-1) ---------------------------------------------
        if (SP(n)%x_low_exist) then
          CONT_TN%jacobi(tn_)%values(  SP(n)%id_cont_qfn_x_low ) = &
          - (-j_mx_dtnm*phi_mx-s_mx_dtnm ) *dy 
          if (simul) then
            CONT_TN%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_low) = &
            - (- j_mx_dpsim*phi_mx - j_mx*phi_mx_dpsim - s_mx_dpsim )*dy 
            if (DD_ELEC) CONT_TN%jacobi(phin_)%values(SP(n)%id_cont_qfn_x_low) = & 
            - (- j_mx_dqfm*phi_mx - j_mx*phi_mx_dphim - s_mx_dqfm )*dy 
          endif
        endif

        ! --> A(i,i  ) ---------------------------------------------
        CONT_TN%jacobi(tn_)%values(  SP(n)%id_cont_qfn ) = &
        -(j_px_dtnm*phi_px - j_mx_dtnp*phi_mx + s_px_dtnm - s_mx_dtnp )*dy &
        -(j_py_dtnm*phi_py - j_my_dtnp*phi_my + s_py_dtnm - s_my_dtnp )*dx &
        - fac * ( DN(n)%dens_dtl * ( CONT_TN%var(n)-TEMP_LATTICE )/tau + CN(n) * ( 1/tau - ( CONT_TN%var(n)-TEMP_LATTICE )/tau/tau*tau_dtn ))
        if (simul) then
          CONT_TN%jacobi(psi_)%values(SP(n)%id_cont_elec_psi) = &
            -(j_px_dpsim*phi_px + j_px*phi_px_dpsim - j_mx_dpsip*phi_mx - j_mx*phi_mx_dpsip + s_px_dpsim - s_mx_dpsip )*dy & 
            -(j_py_dpsim*phi_py + j_py*phi_py_dpsim - j_my_dpsip*phi_my - j_my*phi_my_dpsip + s_py_dpsim - s_my_dpsip )*dx &  
            - fac * (-DN(n)%dens_dqf) *( CONT_TN%var(n)-TEMP_LATTICE )/tau
          if (DD_ELEC) then
            CONT_TN%jacobi(phin_)%values(SP(n)%id_cont_qfn)   = & 
            -(j_px_dqfm*phi_px + j_px*phi_px_dphim  - j_mx_dqfp*phi_mx - j_mx*phi_mx_dphip + s_px_dqfm - s_mx_dqfp )*dy & 
            -(j_py_dqfm*phi_py + j_py*phi_py_dphim  - j_my_dqfp*phi_my - j_my*phi_my_dphip + s_py_dqfm - s_my_dqfp )*dx & 
            - fac * DN(n)%dens_dqf *( CONT_TN%var(n)-TEMP_LATTICE )/tau
          endif
        endif

        ! --> A(i,i+1) ---------------------------------------------
        if (SP(n)%x_upp_exist) then
          CONT_TN%jacobi(tn_)%values(  SP(n)%id_cont_qfn_x_upp ) = - (j_px_dtnp*phi_px + s_px_dtnp ) *dy 
          if (simul) then
            CONT_TN%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_upp) =  &
            -(j_px_dpsip*phi_px + j_px*phi_px_dpsip + s_px_dpsip )*dy 
            if (DD_ELEC) CONT_TN%jacobi(phin_)%values(SP(n)%id_cont_qfn_x_upp)  = &
              -(j_px_dqfp*phi_px + j_px*phi_px_dphip + s_px_dqfp )*dy 
          endif
        endif

        ! --> A(i,i+NX) ---------------------------------------------
        if (SP(n)%y_upp_exist) then
            CONT_TN%jacobi(tn_)%values(  SP(n)%id_cont_qfn_y_upp ) =  - (j_py_dtnp*phi_py+s_py_dtnp ) *dx 
            if (simul) then
                CONT_TN%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_y_upp) =  &
                - (j_py_dpsip*phi_py + j_py*phi_py_dpsip + s_py_dpsip )*dx
                if (DD_ELEC) CONT_TN%jacobi(phin_)%values(SP(n)%id_cont_qfn_y_upp)  = &
                  -(j_py_dqfp*phi_py + j_py*phi_py_dphip + s_py_dqfp )*dx  
            endif
        endif
    endif
    
    enddo

    CONT_TN%rhs  = -CONT_TN%rhs

endsubroutine

subroutine set_cont_tn_ac(omega)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set matrix values for ac solution of electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in) :: omega !< 2*pi*frequency
    real(8) :: dummy

    if (.not.(CONT_TN%active)) return

    dummy = omega
endsubroutine

endmodule
