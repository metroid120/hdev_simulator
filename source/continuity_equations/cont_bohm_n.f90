!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!> continuity equation for bohm potential electrons
module continuity_bohm_n

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use continuity_elec
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

implicit none

type, extends(cont) :: cont_bohm_n_ !bohm electron equation of DD equ. system
  contains
  procedure,nopass   :: init   => init_cont_bohm_n
  procedure,nopass   :: set    => set_cont_bohm_n
  procedure,nopass   :: set_ac => set_cont_bohm_n_ac
  procedure,nopass   :: init_cont_bohm_n_ln
  procedure,nopass   :: init_cont_bohm_n_qfn
  procedure,nopass   :: init_cont_bohm_n_qfn2
  procedure,nopass   :: init_cont_bohm_n_qfp
  procedure,nopass   :: init_cont_bohm_n_psi
  procedure,nopass   :: init_cont_bohm_n_tl
endtype

type(cont_bohm_n_),target             :: CONT_BOHM_N

public CONT_BOHM_N

contains

subroutine init_cont_bohm_n(var)
    integer :: var
    if (.not.CONT_BOHM_N%active) then
      allocate(CONT_BOHM_N%jacobi(nvar-1))
      allocate(CONT_BOHM_N%jacobi_ac(nvar-1))
      allocate(CONT_BOHM_N%rhs(N_SP))
      allocate(CONT_BOHM_N%delta(N_SP))
      allocate(CONT_BOHM_N%var(N_SP))
      allocate(CONT_BOHM_N%var_last(N_SP))
      CONT_BOHM_N%var                 = 0
      CONT_BOHM_N%var_last            = 0
      CONT_BOHM_N%delta               = 0
      CONT_BOHM_N%rhs                 = 0
      CONT_BOHM_N%active              =.true.
      CONT_BOHM_N%jacobi(:)%active    =.false.
      CONT_BOHM_N%jacobi_ac(:)%active =.false.
      CONT_BOHM_N%i_var               = ln_

      CONT_BOHM_N%atol      = US%cont_bohm_n%atol
      CONT_BOHM_N%rtol      = US%cont_bohm_n%rtol
      CONT_BOHM_N%ctol      = US%cont_bohm_n%ctol
      CONT_BOHM_N%delta_max = US%cont_bohm_n%delta_max

      !use band of electron continuity equation
      CONT_BOHM_N%valley    = CONT_ELEC%valley

    endif
    if (var.eq.phin_) then
        call CONT_BOHM_N%init_cont_bohm_n_qfn
    elseif (var.eq.phin2_) then
        call CONT_BOHM_N%init_cont_bohm_n_qfn2
    elseif (var.eq.phip_) then
        call CONT_BOHM_N%init_cont_bohm_n_qfp
    elseif (var.eq.psi_) then
        call CONT_BOHM_N%init_cont_bohm_n_psi
    elseif (var.eq.tl_) then
        call CONT_BOHM_N%init_cont_bohm_n_tl
    elseif (var.eq.ln_) then
        call CONT_BOHM_N%init_cont_bohm_n_ln
    else
        write(*,*) '***error*** initialization routine of CONT_BOHM_N not implemented for this case.'
        stop
    endif
endsubroutine


subroutine init_cont_bohm_n_ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_BOHM_N%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_BOHM_N%jacobi(ln_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_BOHM_N%jacobi(ln_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(ln_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(ln_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_BOHM_N%jacobi(ln_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(ln_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(ln_)%columns(n) = SP(i)%id_sy_upp
        endif
    enddo
    CONT_BOHM_N%jacobi(ln_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_BOHM_N%jacobi(ln_), CONT_BOHM_N%jacobi_ac(ln_))
        CONT_BOHM_N%jacobi_ac(ln_)%active=.true.
        allocate(CONT_BOHM_N%delta_ac(size(CONT_BOHM_N%delta)))
    endif

endsubroutine

subroutine init_cont_bohm_n_qfn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to qfn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_BOHM_N%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_BOHM_N%jacobi(phin_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_BOHM_N%jacobi(phin_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(phin_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(phin_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_BOHM_N%jacobi(phin_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(phin_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(phin_)%columns(n) = SP(i)%id_sy_upp
        endif
    enddo
    CONT_BOHM_N%jacobi(phin_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
    call copy_crs(CONT_BOHM_N%jacobi(phin_), CONT_BOHM_N%jacobi_ac(phin_))
    CONT_BOHM_N%jacobi_ac(phin_)%active=.true.
    endif

endsubroutine

subroutine init_cont_bohm_n_tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_BOHM_N%jacobi(tl_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_BOHM_N%jacobi(tl_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_BOHM_N%jacobi(tl_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(tl_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(tl_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_BOHM_N%jacobi(tl_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(tl_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(tl_)%columns(n) = SP(i)%id_sy_upp
        endif
    enddo
    CONT_BOHM_N%jacobi(tl_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_BOHM_N%jacobi(tl_), CONT_BOHM_N%jacobi_ac(tl_))
        CONT_BOHM_N%jacobi_ac(tl_)%active=.true.
    endif

endsubroutine

subroutine init_cont_bohm_n_psi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to psi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i

    ! --> get size
    n = 0
    do i=1,N_SP
        n = n + 1
        if (SP(i)%x_low_exist) then
            n = n + 1
        endif

        if (SP(i)%x_upp_exist) then
            n = n + 1
        endif

        if (SP(i)%y_low_exist) then
            n = n + 1
        endif

        if (SP(i)%y_upp_exist) then
            n = n + 1
        endif

    enddo

    call CONT_BOHM_N%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
    CONT_BOHM_N%jacobi(psi_)%active =.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_BOHM_N%jacobi(psi_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_low)%id_p
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_low)%id_p
        endif
        n = n+1
        CONT_BOHM_N%jacobi(psi_)%columns(n)  = SP(i)%id_p
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_upp)%id_p
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_BOHM_N%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_upp)%id_p
        endif
    enddo
    CONT_BOHM_N%jacobi(psi_)%row_index(N_SP+1) = n+1

    ! --> check crs
    call CONT_BOHM_N%jacobi(psi_)%check

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_BOHM_N%jacobi(psi_), CONT_BOHM_N%jacobi_ac(psi_))
        CONT_BOHM_N%jacobi_ac(psi_)%active =.true.
    endif

endsubroutine

subroutine init_cont_bohm_n_qfp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to quasi Fermi potential holes
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i

    ! --> get size
    call CONT_BOHM_N%jacobi(phip_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_BOHM_N%jacobi(phip_)%active=.true.

    ! --> set row_index/columns (not needed actually) -> remove for less memory
    do i=1,N_SP
        CONT_BOHM_N%jacobi(phip_)%row_index(i)  = i
        CONT_BOHM_N%jacobi(phip_)%columns(i)    = i
    enddo
    CONT_BOHM_N%jacobi(phip_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_BOHM_N%jacobi(phip_), CONT_BOHM_N%jacobi_ac(phip_))
        CONT_BOHM_N%jacobi_ac(phip_)%active =.true.
    endif
endsubroutine

subroutine init_cont_bohm_n_qfn2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to quasi Fermi potential electrons 2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i

    ! --> get size
    call CONT_BOHM_N%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_BOHM_N%jacobi(phin2_)%active=.true.

    ! --> set row_index/columns (not needed actually) -> remove for less memory
    do i=1,N_SP
        CONT_BOHM_N%jacobi(phin2_)%row_index(i)  = i
        CONT_BOHM_N%jacobi(phin2_)%columns(i)    = i
    enddo
    CONT_BOHM_N%jacobi(phin2_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_BOHM_N%jacobi(phin2_), CONT_BOHM_N%jacobi_ac(phin2_))
        CONT_BOHM_N%jacobi_ac(phin2_)%active =.true.
    endif
endsubroutine

subroutine set_cont_bohm_n(simul)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set values of bohm_n continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx=0,my=0,n=0,px=0,py=0 
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_px=0,j_px_dqfm=0,j_px_dqfp=0,j_px_dpsim=0,j_px_dpsip=0
    real(8) :: j_mx=0,j_mx_dqfm=0,j_mx_dqfp=0,j_mx_dpsim=0,j_mx_dpsip=0
    real(8) :: j_py=0,j_py_dqfm=0,j_py_dqfp=0,j_py_dpsim=0,j_py_dpsip=0
    real(8) :: j_my=0,j_my_dqfm=0,j_my_dqfp=0,j_my_dpsim=0,j_my_dpsip=0
    ! real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension
    
    if (.not.(CONT_BOHM_N%active)) return

    ! ---> nullify data fields
    CONT_BOHM_N%rhs                   = 0 ! right hand side
    CONT_BOHM_N%jacobi(ln_)%values    = 0 ! nonzero elements in matrix
    if (simul) then
      CONT_BOHM_N%jacobi(psi_)%values   = 0
    endif
    if (DD_ELEC.and.simul) then
      CONT_BOHM_N%jacobi(phin_)%values  = 0
    endif
    if (DD_TN.and.simul) then
      CONT_BOHM_N%jacobi(tn_)%values  = 0
    endif
    if (DD_HOLE.and.simul) then
      CONT_BOHM_N%jacobi(phip_)%values  = 0
    endif
    if (DD_ELEC2.and.simul) then
      CONT_BOHM_N%jacobi(phin2_)%values  = 0
    endif
    if (DD_TL.and.simul) then
      CONT_BOHM_N%jacobi(tl_)%values = 0
    endif

    is_2d      = SEMI_DIM.ge.2
    dy         = 1

    ! --> loop over position
    do n=1,N_SP
        mx = SP(n)%id_sx_low
        px = SP(n)%id_sx_upp
        dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
        if (is_2d) then
            my = SP(n)%id_sy_low
            py = SP(n)%id_sy_upp
            dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
        endif
        
        ! scal=get_scal(dx,dy,CHARGE_DIM)
        
        if (SP(n)%set_qfn.or.SP(n)%set_psi) then
            ! contact, ln_ = 0
            CONT_BOHM_N%rhs(n)    = CONT_BOHM_N%var(n)
            
            ! A(i,i  ) ---------------------------------------------
            CONT_BOHM_N%jacobi(ln_)%values(SP(n)%id_cont_qfn) = 1.0
        
        else
            ! current boundary
            j_px       = DN(n)%j_xhdg           ! (i+1/2)
            j_px_dqfm  = DN(n)%j_xhdg_dqf(1)    ! (i+1/2)/(i)
            j_px_dpsim = DN(n)%j_xhdg_dpsi(1)   ! (i+1/2)/(i)
            j_px_dqfp  = DN(n)%j_xhdg_dqf(2)    ! (i+1/2)/(i+1)
            j_px_dpsip = DN(n)%j_xhdg_dpsi(2)   ! (i+1/2)/(i+1)
    
            
            if (SP(n)%x_low_exist) then
                j_mx       = DN(mx)%j_xhdg          ! (i-1/2)
                j_mx_dqfm  = DN(mx)%j_xhdg_dqf(1)   ! (i-1/2)/(i-1)
                j_mx_dpsim = DN(mx)%j_xhdg_dpsi(1)  ! (i-1/2)/(i-1)
                j_mx_dqfp  = DN(mx)%j_xhdg_dqf(2)   ! (i-1/2)/(i)
                j_mx_dpsip = DN(mx)%j_xhdg_dpsi(2)  ! (i-1/2)/(i)
            
            else
                j_mx       = 0  ! (i-1/2)
                j_mx_dqfm  = 0  ! (i-1/2)/(i-1)
                j_mx_dpsim = 0  ! (i-1/2)/(i-1)
                j_mx_dqfp  = 0  ! (i-1/2)/(i)
                j_mx_dpsip = 0  ! (i-1/2)/(i)
            
            endif
        
            if (is_2d) then
                j_py       = DN(n)%j_yhdg           ! (i+1/2)
                j_py_dqfm  = DN(n)%j_yhdg_dqf(1)    ! (i+1/2)/(i)
                j_py_dpsim = DN(n)%j_yhdg_dpsi(1)   ! (i+1/2)/(i)
                j_py_dqfp  = DN(n)%j_yhdg_dqf(2)    ! (i+1/2)/(i+1)
                j_py_dpsip = DN(n)%j_yhdg_dpsi(2)   ! (i+1/2)/(i+1)
                
                if (SP(n)%y_low_exist) then
                    j_my       = DN(my)%j_yhdg          ! (i-1/2)
                    j_my_dqfm  = DN(my)%j_yhdg_dqf(1)   ! (i-1/2)/(i-1)
                    j_my_dpsim = DN(my)%j_yhdg_dpsi(1)  ! (i-1/2)/(i-1)
                    j_my_dqfp  = DN(my)%j_yhdg_dqf(2)   ! (i-1/2)/(i)
                    j_my_dpsip = DN(my)%j_yhdg_dpsi(2)  ! (i-1/2)/(i)
                
                else
                    j_my       = 0  ! (i-1/2)
                    j_my_dqfm  = 0  ! (i-1/2)/(i-1)
                    j_my_dpsim = 0  ! (i-1/2)/(i-1)
                    j_my_dqfp  = 0  ! (i-1/2)/(i)
                    j_my_dpsip = 0  ! (i-1/2)/(i)
                
                endif
            endif
        
            !Schottky contact removed.

            ! --> RHS -------------------------------------------------
            CONT_BOHM_N%rhs(n) = (j_px - j_mx)*dy + (j_py - j_my)*dx - DN(n)%bohm_integral
        
            !only 1D case no self-heating

            ! --> A(i,i-1) ---------------------------------------------
            if (SP(n)%x_low_exist) then
                CONT_BOHM_N%jacobi(ln_)%values(  SP(n)%id_cont_qfn_x_low )                  = -j_mx_dpsim *dy - DN(n)%bohm_integral_dl(1)
                if (simul) then
                    CONT_BOHM_N%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_low)             = -j_mx_dpsim *dy - DN(n)%bohm_integral_dpsi(1)
                    if (DD_ELEC) CONT_BOHM_N%jacobi(phin_)%values(  SP(n)%id_cont_qfn_x_low ) = -j_mx_dqfm *dy  - DN(n)%bohm_integral_dqf(1)
                endif
            endif
        
            ! --> A(i,i  ) ---------------------------------------------
            CONT_BOHM_N%jacobi(ln_)%values(  SP(n)%id_cont_qfn )                          = (j_px_dpsim  - j_mx_dpsip )*dy + (j_py_dpsim - j_my_dpsip )*dx - DN(n)%bohm_integral_dl(2)
            if (simul) then
                CONT_BOHM_N%jacobi(psi_)%values(SP(n)%id_cont_elec_psi)                   = (j_px_dpsim  - j_mx_dpsip )*dy + (j_py_dpsim - j_my_dpsip )*dx - DN(n)%bohm_integral_dpsi(2)
                if (DD_ELEC) CONT_BOHM_N%jacobi(phin_)%values(SP(n)%id_cont_qfn)          = (j_px_dqfm   - j_mx_dqfp  )*dy + (j_py_dqfm  - j_my_dqfp  )*dx - DN(n)%bohm_integral_dqf(2)
            endif
        
            ! --> A(i,i+1) ---------------------------------------------
            if (SP(n)%x_upp_exist) then
                CONT_BOHM_N%jacobi(ln_)%values(  SP(n)%id_cont_qfn_x_upp )                  =  j_px_dpsip *dy  - DN(n)%bohm_integral_dl(3)
                if (simul) then
                    CONT_BOHM_N%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_upp)           =  j_px_dpsip*dy   - DN(n)%bohm_integral_dpsi(3)
                    if (DD_ELEC) CONT_BOHM_N%jacobi(phin_)%values(SP(n)%id_cont_qfn_x_upp)  =  j_px_dqfp *dy   - DN(n)%bohm_integral_dqf(3)
                endif
            endif
        
        endif
    
    enddo

    CONT_BOHM_N%rhs  = -CONT_BOHM_N%rhs
    
endsubroutine

subroutine set_cont_bohm_n_ac(omega)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set ac values of bohm_n continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in) :: omega
    real(8) :: dummy
    dummy = omega
endsubroutine

endmodule
