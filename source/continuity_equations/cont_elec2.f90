!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! continuity equation for electrons in second conduction band (III-V semiconductors)
module continuity_elec2

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

implicit none

type, extends(cont) :: cont_elec2_ !electron continuity equation of DD equ. system
  contains
  procedure,nopass   :: init   => init_cont_elec2
  procedure,nopass   :: set    => set_cont_elec2
  procedure,nopass   :: set_ac => set_cont_elec2_ac
  procedure,nopass   :: init_cont_elec2_qfn2
  procedure,nopass   :: init_cont_elec2_qfn
  procedure,nopass   :: init_cont_elec2_qfp
  procedure,nopass   :: init_cont_elec2_psi
  procedure,nopass   :: init_cont_elec2_tl
  procedure,nopass   :: init_cont_elec2_ln
endtype

type(cont_elec2_),target             :: CONT_ELEC2

public CONT_ELEC2

contains

subroutine init_cont_elec2(var)
integer :: var,n
    if (.not.CONT_ELEC2%active) then
      allocate(CONT_ELEC2%jacobi(nvar-1))
      allocate(CONT_ELEC2%jacobi_ac(nvar-1))
      allocate(CONT_ELEC2%rhs(N_SP))
      allocate(CONT_ELEC2%delta(N_SP))
      allocate(CONT_ELEC2%var(N_SP))
      allocate(CONT_ELEC2%var_last(N_SP))
      CONT_ELEC2%var                 = 0
      CONT_ELEC2%var_last            = 0
      CONT_ELEC2%delta               = 0
      CONT_ELEC2%rhs                 = 0
      CONT_ELEC2%active              = .true.
      CONT_ELEC2%jacobi(:)%active    = .false.
      CONT_ELEC2%jacobi_ac(:)%active = .false.
      CONT_ELEC2%i_var               = phin2_

      CONT_ELEC2%atol        = US%cont_elec2%atol
      CONT_ELEC2%rtol        = US%cont_elec2%rtol
      CONT_ELEC2%ctol        = US%cont_elec2%ctol
      CONT_ELEC2%delta_max   = US%cont_elec2%delta_max

      !obtain band of second conduction band valley (iterate reverse)
      do n=size(SEMI(1)%band_pointer),1,-1
        if (SEMI(1)%band_pointer(n)%band%iscb) then
          CONT_ELEC2%valley = get_conduction_band_index2(SEMI(1))
          exit
        endif
      enddo

    endif
    if (var.eq.phin2_) then
        call CONT_ELEC2%init_cont_elec2_qfn2
    elseif (var.eq.phin_) then
        call CONT_ELEC2%init_cont_elec2_qfn
    elseif (var.eq.phip_) then
        call CONT_ELEC2%init_cont_elec2_qfp
    elseif (var.eq.psi_) then
        call CONT_ELEC2%init_cont_elec2_psi
    elseif (var.eq.tl_) then
        call CONT_ELEC2%init_cont_elec2_tl
    elseif (var.eq.ln_) then
        call CONT_ELEC2%init_cont_elec2_ln
    else
        write(*,*) '***error*** initialization routine of CONT_ELEC2 not implemented for this case.'
        stop
    endif

endsubroutine


subroutine init_cont_elec2_ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobi with respect to bohm potential of first electron continuity equation 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i

    ! --> get size
    call CONT_ELEC2%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_ELEC2%jacobi(ln_)%active=.true.

    ! --> set row_index/columns (not needed actually) -> remove for less memory
    do i=1,N_SP
        CONT_ELEC2%jacobi(ln_)%row_index(i)  = i
        CONT_ELEC2%jacobi(ln_)%columns(i)    = i
    enddo
    CONT_ELEC2%jacobi(ln_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC2%jacobi(ln_), CONT_ELEC2%jacobi_ac(ln_))
        CONT_ELEC2%jacobi_ac(ln_)%active =.true.
    endif
endsubroutine

subroutine init_cont_elec2_qfn2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobi with respect to quasi Fermi potential of second electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i


    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1

    enddo

    ! --> get size
    call CONT_ELEC2%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_ELEC2%jacobi(phin2_)%active=.true.

    ! --> set row_index/columns
    ! --> very slow for large structure ... maybe use POINT struct instead of SP?
    n=0
    do i=1,N_SP
        CONT_ELEC2%jacobi(phin2_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_ELEC2%jacobi(phin2_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_ELEC2%jacobi(phin2_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_ELEC2%jacobi(phin2_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_ELEC2%jacobi(phin2_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_ELEC2%jacobi(phin2_)%columns(n) = SP(i)%id_sy_upp
        endif

    enddo
    CONT_ELEC2%jacobi(phin2_)%row_index(N_SP+1) = n+1    ! --> complex matrix for AC

    if (AC_SIM) then
        call copy_crs(CONT_ELEC2%jacobi(phin2_), CONT_ELEC2%jacobi_ac(phin2_))
        CONT_ELEC2%jacobi_ac(phin2_)%active=.true.
        allocate(CONT_ELEC2%delta_ac(size(CONT_ELEC2%delta)))
    endif

endsubroutine

subroutine init_cont_elec2_qfn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobi with respect to quasi Fermi potential of first electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i,j

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1

    enddo

    ! --> get size
    call CONT_ELEC2%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_ELEC2%jacobi(phin_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_ELEC2%jacobi(phin_)%row_index(i) = n+1
          do j=1,N_SP
              if     (j.eq.i) then
                  n = n+1
                  CONT_ELEC2%jacobi(phin_)%columns(n) = i
              elseif (j.eq.SP(i)%id_sx_low) then
                  n = n+1
                  CONT_ELEC2%jacobi(phin_)%columns(n) = SP(i)%id_sx_low
              elseif (j.eq.SP(i)%id_sx_upp) then
                  n = n+1
                  CONT_ELEC2%jacobi(phin_)%columns(n) = SP(i)%id_sx_upp
              elseif (j.eq.SP(i)%id_sy_low) then
                  n = n+1
                  CONT_ELEC2%jacobi(phin_)%columns(n) = SP(i)%id_sy_low
              elseif (j.eq.SP(i)%id_sy_upp) then
                  n = n+1
                  CONT_ELEC2%jacobi(phin_)%columns(n) = SP(i)%id_sy_upp
              endif
          enddo
    enddo
    CONT_ELEC2%jacobi(phin_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC2%jacobi(phin_), CONT_ELEC2%jacobi_ac(phin_))
        CONT_ELEC2%jacobi_ac(phin_)%active=.true.
    endif

endsubroutine

subroutine init_cont_elec2_qfp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobi with respect to quasi Fermi potential of holes
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i

    ! --> get size
    call CONT_ELEC2%jacobi(phip_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_ELEC2%jacobi(phip_)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
        CONT_ELEC2%jacobi(phip_)%row_index(i)  = i
        CONT_ELEC2%jacobi(phip_)%columns(i)    = i
    enddo
    CONT_ELEC2%jacobi(phip_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC2%jacobi(phip_), CONT_ELEC2%jacobi_ac(phip_))
        CONT_ELEC2%jacobi_ac(phip_)%active =.true.
    endif

endsubroutine

subroutine init_cont_elec2_psi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobi with respect to electrostatic Potential
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j,m
    logical :: first


    !if (.not.DD_TUNNEL) then !currently tunneling not available for cont_elec2
    if (.true.) then
        ! --> without tunneling -----------------------------------------------------
        ! --> get size
        n = 0
        do i=1,N_SP
            if (SP(i)%psi_mean) then
                n = n + size(SP(i)%id_p_get_mean)
            else
                n = n + 1
            endif

            if (SP(i)%x_low_exist) then
                if (SP(SP(i)%id_sx_low)%psi_mean) then
                    n = n + size(SP(SP(i)%id_sx_low)%id_p_get_mean)
                else
                        n = n + 1
                endif
            endif

            if (SP(i)%x_upp_exist) then
                if (SP(SP(i)%id_sx_upp)%psi_mean) then
                    n = n + size(SP(SP(i)%id_sx_upp)%id_p_get_mean)
                else
                    n = n + 1
                endif
            endif

            if (SP(i)%y_low_exist) then
                if ((.not.SP(i)%psi_offset).and.(.not.SP(SP(i)%id_sy_low)%psi_offset).and.(.not.SP(i)%psi_mean)) then
                    n = n + 1
                endif
            endif

            if (SP(i)%y_upp_exist) then
                if ((.not.SP(i)%psi_offset).and.(.not.SP(SP(i)%id_sy_upp)%psi_offset).and.(.not.SP(i)%psi_mean)) then
                    n = n + 1
                endif
            endif

        enddo

        call CONT_ELEC2%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_ELEC2%jacobi(psi_)%active =.true.


        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_ELEC2%jacobi(psi_)%row_index(i) = n+1
            if (SP(i)%psi_mean) then
                allocate(SP(i)%id_cont_psi_mean(      size(SP(i)%id_p_get_mean)))
                allocate(SP(i)%id_cont_psi_x_low_mean(size(SP(i)%id_p_get_mean)))
                allocate(SP(i)%id_cont_psi_x_upp_mean(size(SP(i)%id_p_get_mean)))
                !allocate(SP(i)%id_cont_psi_x_tun_mean(size(SP(i)%id_p_get_mean)))
                do m=1,size(SP(i)%id_p_get_mean)
                    do j=1,N_SP
                        if     (j.eq.i) then
                            n = n+1
                            CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
                            SP(i)%id_cont_psi_mean(m) = n
                        
                        elseif (j.eq.SP(i)%id_sx_low) then
                            n = n+1
                            CONT_ELEC2%jacobi(psi_)%columns(n)   = SP(j)%id_p_get_mean(m)
                            SP(i)%id_cont_psi_x_low_mean(m) = n
                
                        elseif (j.eq.SP(i)%id_sx_upp) then
                            n = n+1
                            CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
                            SP(i)%id_cont_psi_x_upp_mean(m)   = n
                
                        endif

                    enddo
                enddo
                
                else
                    do j=1,N_SP
                        if     (j.eq.i) then
                            n = n+1
                            if (SP(j)%psi_offset) then
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                            else
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                            endif
                            SP(i)%id_cont_psi        = n
                    
                        elseif (j.eq.SP(i)%id_sx_low) then
                            n = n+1
                            if (SP(j)%psi_offset) then
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                            else
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                            endif
                            SP(i)%id_cont_psi_x_low  = n
                
                        elseif (j.eq.SP(i)%id_sx_upp) then
                            n = n + 1
                            if (SP(j)%psi_offset) then
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                            else
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                            endif
                            SP(i)%id_cont_psi_x_upp  = n
                
                        elseif (j.eq.SP(i)%id_sy_low) then
                            if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
                                n = n + 1
                                CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                                SP(i)%id_cont_psi_y_low  = n
                            endif

                        elseif (j.eq.SP(i)%id_sy_upp) then
                            if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
                                n = n+1
                                CONT_ELEC2%jacobi(psi_)%columns(n)  = SP(j)%id_p
                                SP(i)%id_cont_psi_y_upp   = n
                            endif
                    
                        endif
                    enddo
                endif
            enddo
        CONT_ELEC2%jacobi(psi_)%row_index(N_SP+1) = n+1
        
    else
        ! --> with tunneling --------------------------------------------------------
        ! --> get size
        n = 0
        do i=1,N_SP
            if (SP(i)%set_qfp) then
              n = n + 1

            else
            do j=1,N_SP
                if     (j.eq.i) then
                if (SP(i)%psi_mean) then
                    n = n + size(SP(i)%id_p_get_mean)
                else
                    n = n + 1
                    endif

                elseif (j.eq.SP(i)%id_sx_low) then
                if (SP(i)%psi_mean) then
                    n = n + size(SP(i)%id_p_get_mean)
                else
                    n = n + 1
                endif

                elseif (j.eq.SP(i)%id_sx_upp) then
                    if (SP(i)%psi_mean) then
                    n = n + size(SP(i)%id_p_get_mean)
                else
                    n = n + 1
            endif
            
                elseif (j.eq.SP(i)%id_sy_low) then
                if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset).and.(.not.SP(i)%psi_mean)) then
                    n = n + 1
                endif

                elseif (j.eq.SP(i)%id_sy_upp) then
                if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset).and.(.not.SP(i)%psi_mean)) then
                    n = n + 1
                endif

                elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
                if (SP(i)%psi_mean) then
                    n = n + size(SP(i)%id_p_get_mean)
                else
                    n = n + 1
                endif

                endif
            enddo

            endif
        enddo
        call CONT_ELEC2%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_ELEC2%jacobi(psi_)%active =.true.

        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_ELEC2%jacobi(psi_)%row_index(i) = n+1
            if (SP(i)%set_qfp) then
            n = n+1
            CONT_ELEC2%jacobi(psi_)%columns(n) = SP(i)%id_p
            SP(i)%id_cont_psi        = n

            else
            if (SP(i)%psi_mean) then
                allocate(SP(i)%id_cont_psi_mean(      size(SP(i)%id_p_get_mean)))
                allocate(SP(i)%id_cont_psi_x_low_mean(size(SP(i)%id_p_get_mean)))
                allocate(SP(i)%id_cont_psi_x_upp_mean(size(SP(i)%id_p_get_mean)))
                    allocate(SP(i)%id_cont_psi_x_tun_mean(size(SP(i)%id_p_get_mean)))
                do m=1,size(SP(i)%id_p_get_mean)
                first = .true.
                do j=1,N_SP
                    if     (j.eq.i) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
                    SP(i)%id_cont_psi_mean(m) = n
                    if (first) then
                        SP(i)%id_cont_psi_x_tun_mean(m) = n
                        first = .false.
                    endif
                
                    elseif (j.eq.SP(i)%id_sx_low) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n)   = SP(j)%id_p_get_mean(m)
                    SP(i)%id_cont_psi_x_low_mean(m) = n
                    if (first) then
                        SP(i)%id_cont_psi_x_tun_mean(m) = n
                            first = .false.
                    endif

                    elseif (j.eq.SP(i)%id_sx_upp) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
                    SP(i)%id_cont_psi_x_upp_mean(m)   = n

                    elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get_mean(m)
                    if (first) then
                        SP(i)%id_cont_psi_x_tun_mean(m) = n
                        first = .false.
                    endif

                    endif

                enddo
                enddo

            else
                first = .true.
                do j=1,N_SP
                if     (j.eq.i) then
                        n = n+1
                    if (SP(i)%psi_offset) then
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                    else
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                    endif
                    SP(i)%id_cont_psi         = n
                    if (first) then
                        SP(i)%id_cont_psi_x_tun = n
                    first = .false.
                    endif
                
                elseif (j.eq.SP(i)%id_sx_low) then
                    n = n+1
                if (SP(j)%psi_offset) then
                        CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                    else
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                    endif
                    SP(i)%id_cont_psi_x_low   = n
                    if (first) then
                    SP(i)%id_cont_psi_x_tun = n
                    first = .false.
                    endif
            
                elseif (j.eq.SP(i)%id_sx_upp) then
                    n = n+1
                    if (SP(j)%psi_offset) then
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                    else
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                    endif
                    SP(i)%id_cont_psi_x_upp   = n

                elseif (j.eq.SP(i)%id_sy_low) then
                    if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n)  = SP(j)%id_p
                    SP(i)%id_cont_psi_y_low   = n
                        endif

                elseif (j.eq.SP(i)%id_sy_upp) then
                    if ((.not.SP(i)%psi_offset).and.(.not.SP(j)%psi_offset)) then
                    n = n+1
                    CONT_ELEC2%jacobi(psi_)%columns(n)  = SP(j)%id_p
                    SP(i)%id_cont_psi_y_upp   = n
                    endif

                elseif ((SP(i)%id_y.eq.SP(j)%id_y).and.(SP(i)%id_z.eq.SP(j)%id_z)) then
                    n = n+1
                    if (SP(j)%psi_offset) then
                        CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p_get
                    else
                    CONT_ELEC2%jacobi(psi_)%columns(n) = SP(j)%id_p
                    endif
                    if (first) then
                    SP(i)%id_cont_psi_x_tun = n
                    first = .false.
                    endif

                endif

                enddo
            endif
            endif
        enddo
        CONT_ELEC2%jacobi(psi_)%row_index(N_SP+1) = n+1

endif

! --> check crs
call CONT_ELEC2%jacobi(psi_)%check

! --> complex matrix for AC
if (AC_SIM) then
    call copy_crs(CONT_ELEC2%jacobi(psi_), CONT_ELEC2%jacobi_ac(psi_))
    CONT_ELEC2%jacobi_ac(psi_)%active =.true.
endif

endsubroutine

!jacobian with respect to lattice temperature
subroutine init_cont_elec2_tl

integer :: n

! --> get size
call CONT_ELEC2%jacobi(tl_)%init(nr=N_SP, nc=1, nz=N_SP)
CONT_ELEC2%jacobi(tl_)%active=.true.

! --> set row_index/columns
CONT_ELEC2%jacobi(tl_)%columns     = 1 !all values in first column
do n=1,N_SP !every value new row
  CONT_ELEC2%jacobi(tl_)%row_index(n)   = n
enddo
CONT_ELEC2%jacobi(tl_)%row_index(N_SP+1)   = N_SP + 1

! --> complex matrix for AC
if (AC_SIM) then !todo
  call copy_crs(CONT_ELEC2%jacobi(tl_), CONT_ELEC2%jacobi_ac(tl_))
  CONT_ELEC2%jacobi_ac(tl_)%active =.true.
endif

endsubroutine

! set values of jacobians of continuity equations for second conduction band
subroutine set_cont_elec2(simul)

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx,px,my,py,n,m
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_px,j_px_dqfm,j_px_dqfp,j_px_dpsim,j_px_dpsip,j_px_dtlp,j_px_dtlm
    real(8) :: j_mx,j_mx_dqfm,j_mx_dqfp,j_mx_dpsim,j_mx_dpsip,j_mx_dtlp,j_mx_dtlm
    real(8) :: j_py,j_py_dqfm,j_py_dqfp,j_py_dpsim,j_py_dpsip
    real(8) :: j_my,j_my_dqfm,j_my_dqfp,j_my_dpsim,j_my_dpsip
    real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension

    if (.not.(CONT_ELEC2%active)) return
    
    allocate(scal(CHARGE_DIM))
    
    ! ---> nullify data fields
    CONT_ELEC2%rhs        = 0 ! right hand side
    CONT_ELEC2%jacobi(phin2_)%values = 0 ! nonzero elements in matrix
    if (simul) then
      do n=1,CONT_ELEC2%jacobi(psi_)%nz
        CONT_ELEC2%jacobi(psi_)%values(n) = 0
      enddo
    endif
    if (DD_HOLE.and.simul) then
      do n=1,CONT_ELEC2%jacobi(phip_)%nz
        CONT_ELEC2%jacobi(phip_)%values(n) = 0
      enddo
    endif
    if (DD_ELEC.and.simul) then
      do n=1,CONT_ELEC2%jacobi(phin_)%nz
        CONT_ELEC2%jacobi(phin_)%values(n) = 0
      enddo
    endif
    if (DD_TN.and.simul) then
      do n=1,CONT_ELEC2%jacobi(tn_)%nz
        CONT_ELEC2%jacobi(tn_)%values(n) = 0
      enddo
    endif
    if (DD_TL.and.simul) then
      do n=1,CONT_ELEC2%jacobi(tl_)%nz
        CONT_ELEC2%jacobi(tl_)%values(n) = 0
      enddo
    endif
    if (DD_CONT_BOHM_N.and.simul) then
      do n=1,CONT_ELEC2%jacobi(ln_)%nz
        CONT_ELEC2%jacobi(ln_)%values(n) = 0
      enddo
    endif

    
    is_2d      = SEMI_DIM.ge.2
    dy         = 1
    j_py       = 0
    j_py_dqfm  = 0
    j_py_dpsim = 0
    j_py_dqfp  = 0
    j_py_dpsip = 0
    j_my       = 0
    j_my_dqfm  = 0
    j_my_dpsim = 0
    j_my_dqfp  = 0
    j_my_dpsip = 0
    
    ! --> loop over position
    do n=1,N_SP
      mx = SP(n)%id_sx_low
      px = SP(n)%id_sx_upp
      dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
      if (is_2d) then
        my = SP(n)%id_sy_low
        py = SP(n)%id_sy_low
        dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
      endif
      
      if (SP(n)%set_qfn2) then
        ! ohmic contact, qfn=psi
        CONT_ELEC2%rhs(n) = CONT_ELEC2%var(n) - get_bias(SP(n)%cont_id)
        
        ! A(i,i  ) ---------------------------------------------
        CONT_ELEC2%jacobi(phin2_)%values(SP(n)%id_cont_qfn) = dble(1)
    
      elseif (SP(n)%set_neutrality_n) then
        ! electrons are majorities
        CONT_ELEC2%rhs(n) = (CD(n) - CN(n)- CN2(n) + CP(n))*Q*dx*dy
        ! A(i,i  ) ---------------------------------------------
        CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn ) = -DN2(n)%dens_dqf*Q*dx*dy
        ! if (simul) then
        !   CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi)   = (DN(n)%dens_dqf + DN2(n)%dens_dqf - DP(n)%dens_dqf)*Q*dx*dy
        ! endif
        ! if (DD_HOLE.and.simul) then
        !   CONT_ELEC2%jacobi(phip_)%values(n)                  =  DP(n)%dens_dqf*Q*dx*dy
        ! endif
        if (DD_ELEC.and.simul) then
          CONT_ELEC2%jacobi(phin_)%values(  SP(n)%id_cont_qfn )              = -DN2(n)%dens_dqf*Q*dx*dy
        endif        

        ! !intervalley neutrality
        ! CONT_ELEC2%rhs(n) = CONT_ELEC2%rhs(n) +  (CD(n)/(1+DN2(n)%zeta) - CN2(n))*Q*dx*dy
        ! ! A(i,i  ) ---------------------------------------------
        ! CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn )  = CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn ) + (-DN2(n)%dens_dqf)*Q*dx*dy
        ! if (simul) then
        !   CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi)     = (+DN2(n)%dens_dqf)*Q*dx*dy
        ! endif
        ! if (DD_HOLE.and.simul) then
        !   CONT_ELEC2%jacobi(phip_)%values(n)                  =  DP(n)%dens_dqf*Q*dx*dy
        ! endif
        ! if (DD_ELEC.and.simul) then
        !   CONT_ELEC2%jacobi(phin_)%values(SP(n)%id_cont_qfn) = (-CD(n)/(1+DN2(n)%zeta)**2*DN(n)%zeta_dqf(2) ) *Q*dx*dy
        !   if (SP(n)%x_low_exist) then
        !     CONT_ELEC2%jacobi(phin_)%values(  SP(n)%id_cont_qfn_x_low ) = (-CD(n)/(1+DN2(n)%zeta)**2*DN2(n)%zeta_dqf(1) ) *Q*dx*dy
        !   endif
        !   if (SP(n)%x_upp_exist) then
        !     CONT_ELEC2%jacobi(phin_)%values(  SP(n)%id_cont_qfn_x_upp ) = (-CD(n)/(1+DN2(n)%zeta)**2*DN2(n)%zeta_dqf(3) ) *Q*dx*dy
        !   endif
        ! endif

      else
        ! current boundary
        j_px       = DN2(n)%j_xh           ! (i+1/2)
        j_px_dqfm  = DN2(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
        j_px_dpsim = DN2(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
        j_px_dtlm  = DN2(n)%j_xh_dtl(1)   ! (i+1/2)/(i+1)
        j_px_dqfp  = DN2(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
        j_px_dpsip = DN2(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
        j_px_dtlp  = DN2(n)%j_xh_dtl(2)   ! (i+1/2)/(i+1)
        
        if (SP(n)%x_low_exist) then
          j_mx       = DN2(mx)%j_xh          ! (i-1/2)
          j_mx_dqfm  = DN2(mx)%j_xh_dqf(1)   ! (i-1/2)/(i-1)
          j_mx_dpsim = DN2(mx)%j_xh_dpsi(1)  ! (i-1/2)/(i-1)
          j_mx_dtlm  = DN2(mx)%j_xh_dtl(1)  ! (i-1/2)/(i)
          j_mx_dqfp  = DN2(mx)%j_xh_dqf(2)   ! (i-1/2)/(i)
          j_mx_dpsip = DN2(mx)%j_xh_dpsi(2)  ! (i-1/2)/(i)
          j_mx_dtlp  = DN2(mx)%j_xh_dtl(2)  ! (i-1/2)/(i)
          
        else
          j_mx       = 0  ! (i-1/2)
          j_mx_dqfm  = 0  ! (i-1/2)/(i-1)
          j_mx_dpsim = 0  ! (i-1/2)/(i-1)
          j_mx_dqfp  = 0  ! (i-1/2)/(i)
          j_mx_dpsip = 0  ! (i-1/2)/(i)
          
        endif
    
        if (is_2d) then
          j_py       = DN2(n)%j_yh           ! (i+1/2)
          j_py_dqfm  = DN2(n)%j_yh_dqf(1)    ! (i+1/2)/(i)
          j_py_dpsim = DN2(n)%j_yh_dpsi(1)   ! (i+1/2)/(i)
          j_py_dqfp  = DN2(n)%j_yh_dqf(2)    ! (i+1/2)/(i+1)
          j_py_dpsip = DN2(n)%j_yh_dpsi(2)   ! (i+1/2)/(i+1)
        
          if (SP(n)%y_low_exist) then
            j_my       = DN2(my)%j_yh          ! (i-1/2)
            j_my_dqfm  = DN2(my)%j_yh_dqf(1)   ! (i-1/2)/(i-1)
            j_my_dpsim = DN2(my)%j_yh_dpsi(1)  ! (i-1/2)/(i-1)
            j_my_dqfp  = DN2(my)%j_yh_dqf(2)   ! (i-1/2)/(i)
            j_my_dpsip = DN2(my)%j_yh_dpsi(2)  ! (i-1/2)/(i)
          
          else
            j_my       = 0  ! (i-1/2)
            j_my_dqfm  = 0  ! (i-1/2)/(i-1)
            j_my_dpsim = 0  ! (i-1/2)/(i-1)
            j_my_dqfp  = 0  ! (i-1/2)/(i)
            j_my_dpsip = 0  ! (i-1/2)/(i)
          
          endif
        endif
    
        ! --> schottky contact point
        if (SP(n)%cont_left) then
          ! --> contact at left side
          j_mx       = DN2(n)%j_con       ! contact (i)
          j_mx_dqfm  = 0
          j_mx_dqfp  = DN2(n)%j_con_dqf   ! contact (i)/(i)
          j_mx_dpsim = 0                 
          j_mx_dpsip = DN2(n)%j_con_dpsi  ! contact (i)/(i)
        endif
    
        if (SP(n)%cont_right) then
          ! --> contact at right side
          j_px       = -DN2(n)%j_con       ! contact (i)
          j_px_dqfm  = -DN2(n)%j_con_dqf   ! contact (i)/(i)
          j_px_dqfp  = 0
          j_px_dpsim = -DN2(n)%j_con_dpsi  ! contact (i)/(i)
          j_px_dpsip = 0  
        endif
    
        if (is_2d) then
          if (SP(n)%cont_lower) then
            ! --> contact at lower side
            j_my       = DN2(n)%j_con       ! contact (i)
            j_my_dqfm  = 0
            j_my_dqfp  = DN2(n)%j_con_dqf   ! contact (i)/(i)
            j_my_dpsim = 0  
            j_my_dpsip = DN2(n)%j_con_dpsi  ! contact (i)/(i)
          endif
    
          if (SP(n)%cont_upper) then
            ! --> contact at upper side
            j_py       = -DN2(n)%j_con       ! contact (i)
            j_py_dqfm  = -DN2(n)%j_con_dqf   ! contact (i)/(i)
            j_py_dqfp  = 0
            j_py_dpsim = -DN2(n)%j_con_dpsi
            j_py_dpsip = 0
          endif
    
        endif

        ! ! bulk contact experimental
        ! ! --> schottky contact point
        ! ! current boundary
        ! if (.not.SP(n)%x_upp_exist) then
        !   j_px       = DN2(1)%j_xh           ! (i+1/2)
        !   j_px_dqfm  = DN2(1)%j_xh_dqf(1)    ! (i+1/2)/(i)
        !   j_px_dpsim = DN2(1)%j_xh_dpsi(1)   ! (i+1/2)/(i)
        !   j_px_dqfp  = DN2(1)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
        !   j_px_dpsip = DN2(1)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
        
        ! elseif (.not.SP(n)%x_low_exist) then
        !   j_mx       = DN2(NX1-1)%j_xh          ! (i-1/2)
        !   j_mx_dqfm  = DN2(NX1-1)%j_xh_dqf(1)   ! (i-1/2)/(i-1)
        !   j_mx_dpsim = DN2(NX1-1)%j_xh_dpsi(1)  ! (i-1/2)/(i-1)
        !   j_mx_dqfp  = DN2(NX1-1)%j_xh_dqf(2)   ! (i-1/2)/(i)
        !   j_mx_dpsip = DN2(NX1-1)%j_xh_dpsi(2)  ! (i-1/2)/(i)
          
        ! endif
    
        ! --> RHS -------------------------------------------------
        !no recombination in valley2
        CONT_ELEC2%rhs(n) = (j_px - j_mx)*dy + (j_py - j_my)*dx + dx*dy*(-DN2(n)%intervalley  )!+ DN2(n)%g_tun)
    
        ! --> A(i,i-NX) ---------------------------------------------
        if (SP(n)%y_low_exist) then
          CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn_y_low ) = -j_my_dqfm *dx
          if (simul) then
            if (SP(n)%psi_mean) then ! is_2d=fake
            elseif (SP(n)%psi_offset.or.SP(my)%psi_offset) then ! is_2d=fake
              CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) - ( - j_my_dpsip)*dx ! no y-dependence (sub from main)
            else
              CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_y_low) = -j_my_dpsim*dx
            endif
          endif
        endif
    
        ! --> A(i-1,i) ---------------------------------------------
        if (SP(n)%x_low_exist) then
          CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn_x_low ) = -j_mx_dqfm *dy &
          & + dx*dy*(-DN2(n)%recomb_dqf(1)-DN2(n)%intervalley_dqf2(1) )
          if (simul) then
            CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_low) = -j_mx_dpsim*dy + dx*dy*(-DN2(n)%recomb_dpsi(1)-DN2(n)%intervalley_dpsi(1))
          endif
          if (DD_ELEC.and.simul) then
            CONT_ELEC2%jacobi(phin_)%values(SP(n)%id_cont_qfn_x_low) = -dx*dy*DN2(n)%intervalley_dqf(1)
          endif
          if (simul.and.DD_TL) CONT_ELEC2%jacobi(tl_)%values(n) = CONT_ELEC2%jacobi(tl_)%values(n) - j_mx_dtlm*dy
        endif

        ! --> A(i,i  ) ---------------------------------------------
        CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn ) = (j_px_dqfm  - j_mx_dqfp )*dy + (j_py_dqfm  - j_my_dqfp )*dx + dx*dy*(- DN2(n)%intervalley_dqf2(2)   )
        if (simul) then
            CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) = (j_px_dpsim - j_mx_dpsip)*dy + (j_py_dpsim - j_my_dpsip)*dx + dx*dy*(-DN2(n)%intervalley_dpsi(2))
        endif
        if (DD_HOLE.and.simul) then
          CONT_ELEC2%jacobi(phip_)%values(n)                 = 0 !second band not coupled to first in recombination term
        endif
        if (DD_ELEC.and.simul) then
          CONT_ELEC2%jacobi(phin_)%values(SP(n)%id_cont_qfn) = -dx*dy*DN2(n)%intervalley_dqf(2)
        endif
        if (simul.and.DD_TL) then 
          CONT_ELEC2%jacobi(tl_)%values(n) =  0 ! not coupled to TL
        endif
    
    
        ! --> A(i+1,i) ---------------------------------------------
        if (SP(n)%x_upp_exist) then
          CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn_x_upp ) =  j_px_dqfp *dy  + dx*dy*(-DN2(n)%recomb_dqf(3)-DN2(n)%intervalley_dqf2(3) )!+ DN2(n)%g_tun_dqf(3))
          if (simul) then
              CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_upp) =  j_px_dpsip*dy  + dx*dy*(-DN2(n)%recomb_dpsi(3)-DN2(n)%intervalley_dpsi(3))
          endif
          if (DD_ELEC.and.simul) then
              CONT_ELEC2%jacobi(phin_)%values(SP(n)%id_cont_qfn_x_upp) = -dx*dy*DN2(n)%intervalley_dqf(3)
          endif
          if (simul.and.DD_TL) CONT_ELEC2%jacobi(tl_)%values(n) = CONT_ELEC2%jacobi(tl_)%values(n) + j_px_dtlp*dy
        endif

    
        ! --> A(i,i+NX) ---------------------------------------------
        if (SP(n)%y_upp_exist) then
            CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn_y_upp ) =  j_py_dqfp *dx
            if (simul) then
                if (SP(n)%psi_mean) then ! is_2d=fake
                elseif (SP(n)%psi_offset.or.SP(SP(n)%id_sy_upp)%psi_offset) then ! is_2d=fake
                  CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) - (j_py_dpsim )*dx ! no y-dependence (sub from main)
                else
                  CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_y_upp) =  j_py_dpsip*dx
                endif
            endif
        endif
    
        ! --> TRANSIENT ---------------------------------
        if (TR_IT.ge.1) then
          ! backward euler:
          CONT_ELEC2%rhs(n) = CONT_ELEC2%rhs(n) - dx*dy*Q*(CN2(n) - CN2_LAST(n) )/TR_DT(TR_IT) 
    
          CONT_ELEC2%jacobi(phin2_)%values(  SP(n)%id_cont_qfn ) = CONT_ELEC2%jacobi(phin2_)%values(SP(n)%id_cont_qfn ) - dx*dy*Q*DN(n)%dens_dqf/TR_DT(TR_IT)
    
          if (simul) then
            if (SP(n)%psi_mean) then ! is_2d=.false.
              do m=1,size(SP(n)%id_p_get_mean)
                CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) + dx*dy*Q*DN2(n)%dens_dqf/TR_DT(TR_IT)/size(SP(n)%id_p_get_mean)
              enddo
            else
              CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi) + dx*dy*Q*DN2(n)%dens_dqf/TR_DT(TR_IT)
            endif
    
          endif
        endif
    
        ! ! --> Tunneling simul to psi ---------------
        ! if (simul) then
        !   if (DD_TUNNEL) then
        !     if (SP(n)%psi_mean) then ! is_2d=.false.
        !       do m=1,size(SP(n)%id_p_get_mean)
        !         do j=1,size(DN(n)%g_tun_dpsi)
        !           CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun_mean(m)-1+j) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun_mean(m)-1+j) + dx*dy*DN2(n)%g_tun_dpsi(j)/size(SP(n)%id_p_get_mean)
        !         enddo
        !       enddo
        !     else
        !       do j=1,size(DN(n)%g_tun_dpsi)
        !         CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun-1+j) = CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi_x_tun-1+j) + dx*dy*DN2(n)%g_tun_dpsi(j)
        !       enddo
        !     endif
        !   endif
        ! endif
    
      endif
    
    enddo
    
    CONT_ELEC2%rhs  = -CONT_ELEC2%rhs
    
    deallocate(scal)
    
    endsubroutine

! set values of jacobians of second continuity equations for second conduction band
subroutine set_cont_elec2_ac(omega)
! calculate: CONT_ELEC_RHS/CONT_ELEC%jacobi(phin_)%values

real(8),intent(in) :: omega !< 2*pi*frequency

integer :: n
real(8) :: dx,dy
logical :: is_2d

if (.not.(CONT_ELEC2%active)) return

is_2d = SEMI_DIM.ge.2
dy    = 1


do n=1,N_SP
              dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
  if (is_2d)  dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)

  if (.not.(SP(n)%set_qfn2.or.SP(n)%set_neutrality_n)) then !todo:allgeiner Fall
      CONT_ELEC2%jacobi_ac(phin2_)%values(SP(n)%id_cont_qfn ) = cmplx(CONT_ELEC2%jacobi(phin2_)%values(SP(n)%id_cont_qfn ),-dx*dy*omega*DN2(n)%dens_dqf,8)
      CONT_ELEC2%jacobi_ac(psi_)%values(SP(n)%id_cont_psi)    = cmplx(CONT_ELEC2%jacobi(psi_)%values(SP(n)%id_cont_psi)   ,-dx*dy*omega*(-DN2(n)%dens_dqf),8)
  endif
enddo

endsubroutine

endmodule
