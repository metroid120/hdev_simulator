!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



! implementation of poisson equation
module continuity_poi

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

use structure, only     : DXX => DX   !distance between points x direction
use structure, only     : DYY => DY   !distance between points y direction

implicit none

type, extends(cont) :: cont_poi_ !electron continuity equation of DD equ. system
  contains
  !these methods of the parent class are overwritten
  procedure,nopass   :: init   => init_poi
  procedure,nopass   :: set    => set_poi
  procedure,nopass   :: set_ac => set_poi_ac
  procedure,nopass   :: init_poi_qfn
  procedure,nopass   :: init_poi_qfn2
  procedure,nopass   :: init_poi_qfp
  procedure,nopass   :: init_poi_psi
  procedure,nopass   :: init_poi_tl
  procedure,nopass   :: init_poi_tn
  procedure,nopass   :: init_poi_ln
endtype

type(cont_poi_),target             :: CONT_POI          !< singleton continuity equation of electrons object

public CONT_POI

contains

subroutine init_poi(var)
integer :: var
    if (.not.CONT_POI%active) then
      allocate(CONT_POI%jacobi(nvar-1))
      allocate(CONT_POI%jacobi_ac(nvar-1))
      allocate(CONT_POI%rhs(NXYZ))
      allocate(CONT_POI%delta(NXYZ))
      allocate(CONT_POI%var(NXYZ))
      allocate(CONT_POI%var_last(NXYZ))
      CONT_POI%var                 = 0
      CONT_POI%var_last            = 0
      CONT_POI%active              = .true.
      CONT_POI%jacobi(:)%active    = .false.
      CONT_POI%jacobi_ac(:)%active = .false.

      CONT_POI%valley = 0 ! poisson equation has no valley
      CONT_POI%i_var  = psi_

      CONT_POI%atol      = US%poisson%atol
      CONT_POI%rtol      = US%poisson%rtol
      CONT_POI%ctol      = US%poisson%ctol
      CONT_POI%delta_max = US%poisson%delta_max

    endif
    if (var.eq.phin_) then
        call CONT_POI%init_poi_qfn
    elseif (var.eq.phin2_) then
        call CONT_POI%init_poi_qfn2
    elseif (var.eq.tn_) then
        call CONT_POI%init_poi_tn
    elseif (var.eq.phip_) then
        call CONT_POI%init_poi_qfp
    elseif (var.eq.psi_) then
        call CONT_POI%init_poi_psi
    elseif (var.eq.tl_) then
        call CONT_POI%init_poi_tl
    elseif (var.eq.ln_) then
        call CONT_POI%init_poi_ln
    else
        write(*,*) '***error*** initialization routine of CONT_POI not implemented for this case.'
        stop
    endif
endsubroutine

subroutine init_poi_psi
    ! -----------------------------------------------------
    ! init jacobian with respect to electrostatic Potential
    ! -----------------------------------------------------

    integer :: n,i,j,k!,x,y,z
    integer, dimension(:), allocatable:: cols, rows

    ! --> count nonzero matrix entries
    n=0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1

                if (POINT(i,j,k)%diri) then
                    n = n + 1

                else
                    n = n + 1
                    ! lower points
                    if (i.gt.1)     n = n + 1  !left point exists
                    if (j.gt.1)     n = n + 1  !lower point exists
                    if (k.gt.1)     n = n + 1  !point toward  exists

                    ! right points
                    if (i.lt.NX1)   n = n + 1  !right point exists
                    if (j.lt.NY1)   n = n + 1  !top point exists
                    if (k.lt.NZ1)   n = n + 1  !point away  exists

                endif

            enddo
        enddo
    enddo

    ! --> get size
    call CONT_POI%jacobi(psi_)%init(nr=NXYZ, nc=NXYZ, nz=n)
    CONT_POI%jacobi(psi_)%active=.true.

    ! --> set row_index/columns
    allocate(cols(size(CONT_POI%jacobi(psi_)%columns)))
    allocate(rows(size(CONT_POI%jacobi(psi_)%row_index)))
    rows = 0
    cols = 0
    ! --> very slow for large structure ... maybe use POINT struct instead of SP?
    n=0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                CONT_POI%jacobi(psi_)%row_index(POINT(i,j,k)%id) = n+1
                rows(POINT(i,j,k)%id) = n+1
                if (POINT(i,j,k)%diri) then
                    n = n+1
                    CONT_POI%jacobi(psi_)%columns(n) = POINT(i,j,k)%id

                else
                    if (j.gt.1) then
                        n = n+1
                        CONT_POI%jacobi(psi_)%columns(n) = POINT(i,j-1,k)%id
                    endif
                    if (i.gt.1) then
                        n = n+1
                        CONT_POI%jacobi(psi_)%columns(n) = POINT(i-1,j,k)%id
                    endif
                    n = n+1
                    CONT_POI%jacobi(psi_)%columns(n) = POINT(i,j,k)%id
                    if (i.lt.NX1) then
                        n = n+1
                        CONT_POI%jacobi(psi_)%columns(n) = POINT(i+1,j,k)%id
                    endif
                    if (j.lt.NY1) then
                        n = n+1
                        CONT_POI%jacobi(psi_)%columns(n) = POINT(i,j+1,k)%id
                    endif

                endif
            enddo
        enddo
    enddo
    CONT_POI%jacobi(psi_)%row_index(NXYZ+1) = n+1


    cols = CONT_POI%jacobi(psi_)%columns


    if (any(CONT_POI%jacobi(psi_)%row_index(:).gt.1e30)) then
        write(*,*) '***error*** problem with jacobi'
    endif

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(psi_), CONT_POI%jacobi_ac(psi_))
        CONT_POI%jacobi_ac(psi_)%active=.true.
        allocate(CONT_POI%delta_ac(size(CONT_POI%delta)))
    endif

endsubroutine

subroutine init_poi_qfp
    ! ---------------------------------------------------------
    ! init jacobian with respect to quasi Fermi potential holes
    ! ---------------------------------------------------------
    integer :: i,j,k,p,n

    ! --> get size
    call CONT_POI%jacobi(phip_)%init(nr=NXYZ, nc=N_SP, nz=N_SP)
    CONT_POI%jacobi(phip_)%active=.true.

    n = 0
    p = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                p=p+1
                CONT_POI%jacobi(phip_)%row_index(p) = n+1
                if (POINT(i,j,k)%sp_idx.gt.0) then
                    n = n+1
                    CONT_POI%jacobi(phip_)%columns(n) = POINT(i,j,k)%sp_idx
                endif
            enddo
        enddo
    enddo
    CONT_POI%jacobi(phip_)%row_index(NXYZ+1) = n+1


    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(phip_), CONT_POI%jacobi_ac(phip_))
        CONT_POI%jacobi_ac(phip_)%active =.true.
    endif

endsubroutine

subroutine init_poi_qfn
    ! -------------------------------------------------------------
    ! init jacobian with respect to quasi Fermi potential electrons
    ! -------------------------------------------------------------
    integer :: i,j,k,p,n

    ! --> get size
    call CONT_POI%jacobi(phin_)%init(nr=NXYZ, nc=N_SP, nz=N_SP)
    CONT_POI%jacobi(phin_)%active=.true.

    n = 0
    p = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                p=p+1
                CONT_POI%jacobi(phin_)%row_index(p) = n+1
                if (POINT(i,j,k)%sp_idx.gt.0) then
                    n = n+1
                    CONT_POI%jacobi(phin_)%columns(n) = POINT(i,j,k)%sp_idx
                endif
            enddo
        enddo
    enddo
    CONT_POI%jacobi(phin_)%row_index(NXYZ+1) = n+1


    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(phin_), CONT_POI%jacobi_ac(phin_))
        CONT_POI%jacobi_ac(phin_)%active =.true.
    endif

endsubroutine

subroutine init_poi_ln
    ! -------------------------------------------------------------
    ! init jacobian with respect to bohm potential electrons
    ! => not correctly implemeneted for 2D case, need to copy from init_poi_qfn
    ! -------------------------------------------------------------
    integer :: i

    ! --> get size
    call CONT_POI%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_POI%jacobi(ln_)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
        CONT_POI%jacobi(ln_)%row_index(i)  = i
        CONT_POI%jacobi(ln_)%columns(i)    = i
    enddo
    CONT_POI%jacobi(ln_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(ln_), CONT_POI%jacobi_ac(ln_))
        CONT_POI%jacobi_ac(ln_)%active =.true.
    endif

endsubroutine

subroutine init_poi_qfn2
    ! --------------------------------------------------------------------------
    ! init jacobian with respect to quasi Fermi potential second conduction band
    ! => not correctly implemeneted for 2D case, need to copy from init_poi_qfn
    ! --------------------------------------------------------------------------

    integer :: i

    ! --> get size
    call CONT_POI%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_POI%jacobi(phin2_)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
        CONT_POI%jacobi(phin2_)%row_index(i)  = i
        CONT_POI%jacobi(phin2_)%columns(i)    = i
    enddo
    CONT_POI%jacobi(phin2_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(phin2_), CONT_POI%jacobi_ac(phin2_))
        CONT_POI%jacobi_ac(phin2_)%active =.true.
    endif

endsubroutine

subroutine init_poi_tn
    ! -------------------------------------------------------------
    ! init jacobian with respect to quasi Fermi potential electrons
    ! -------------------------------------------------------------
    integer :: i,j,k,p,n

    ! --> get size
    call CONT_POI%jacobi(tn_)%init(nr=NXYZ, nc=N_SP, nz=N_SP)
    CONT_POI%jacobi(tn_)%active=.true.

    n = 0
    p = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                p=p+1
                CONT_POI%jacobi(tn_)%row_index(p) = n+1
                if (POINT(i,j,k)%sp_idx.gt.0) then
                    n = n+1
                    CONT_POI%jacobi(tn_)%columns(n) = POINT(i,j,k)%sp_idx
                endif
            enddo
        enddo
    enddo
    CONT_POI%jacobi(tn_)%row_index(NXYZ+1) = n+1


    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_POI%jacobi(tn_), CONT_POI%jacobi_ac(tn_))
        CONT_POI%jacobi_ac(tn_)%active =.true.
    endif

endsubroutine

subroutine init_poi_tl
    ! --------------------------------------------------------------------------
    ! init jacobian with respect to lattice temperature
    ! --------------------------------------------------------------------------

    integer :: n

    ! --> get size
    call CONT_POI%jacobi(tl_)%init(nr=N_SP, nc=1, nz=N_SP)
    CONT_POI%jacobi(tl_)%active=.true.

    ! --> set row_index/columns
    CONT_POI%jacobi(tl_)%columns     = 1 !all values in first column
    do n=1,N_SP !every value new row
    CONT_POI%jacobi(tl_)%row_index(n)   = n
    enddo
    CONT_POI%jacobi(tl_)%row_index(N_SP+1)   = N_SP + 1

    ! --> complex matrix for AC
    if (AC_SIM) then !todo
        call copy_crs(CONT_POI%jacobi(tl_), CONT_POI%jacobi_ac(tl_))
        CONT_POI%jacobi_ac(tl_)%active =.true.
    endif

endsubroutine

! set values of poisson equation
subroutine set_poi(simul)
    logical, intent(in) :: simul
    integer :: i,j,k,n
    real(8) :: eps = 0, pot
    real(8) :: e_px=0,e_mx=0
    real(8) :: e_py=0,e_my=0
    real(8) :: e_px_dpsi_p=0,e_px_dpsi_m=0
    real(8) :: e_mx_dpsi_p=0,e_mx_dpsi_m=0
    real(8) :: e_py_dpsi_p=0,e_py_dpsi_m=0
    real(8) :: e_my_dpsi_p=0,e_my_dpsi_m=0
    real(8) :: eps_mx=0,eps_px=0
    real(8) :: eps_my=0,eps_py=0
    real(8) :: dx=0,dy=0
    !real(8),dimension(:),allocatable :: rhs
    logical :: is_2d = .false.

    if (.not.(CONT_POI%active)) return

    ! ---> nullify data fields
    CONT_POI%rhs                    = 0 
    CONT_POI%jacobi(psi_)%values    = 0 
    if (simul) then
        if (DD_TL) CONT_POI%jacobi(tl_)%values           = 0 
        if (DD_ELEC) CONT_POI%jacobi(phin_)%values       = 0 
        if (DD_ELEC2) CONT_POI%jacobi(phin2_)%values     = 0 
        if (DD_TN) CONT_POI%jacobi(tn_)%values           = 0 
        if (DD_HOLE) CONT_POI%jacobi(phip_)%values       = 0 
        if (DD_CONT_BOHM_N) CONT_POI%jacobi(ln_)%values  = 0 
    endif

    is_2d      = STRUC_DIM.ge.2

    ! debug for 1D Resistor
    ! do i=1,NX1
    !     do j=1,NY1
    !         bias_con = get_bias(POINT(1,1,1)%cont_id)
    !         bias = PSI_BUILDIN(POINT(1,1,1)%sp_idx) + (bias_con-(i-1)*bias_con/(NX1-1))*     bias_con
    !         CONT_POI%var(POINT(i,j,1)%id) = bias
    !     enddo
    ! enddo
    ! CONT_POI%rhs  = 0
    ! return

    ! --> loop over position
    n = 0
    do k=1,NZ1
        do j=1,NY1
            do i=1,NX1
                dx = 0
                if (i.gt.1)   then
                    dx  = dx  + DXX(i-1)*0.5
                endif
                if (i.lt.NX1) then
                    dx  = dx  + DXX(i)*0.5
                endif
                if (is_2d) then
                    dy = 0
                    if (j.gt.1) then   
                        dy = dy + DYY(j-1)*0.5
                    endif
                    if (j.lt.NY1) then
                        dy = dy + DYY(j)*0.5
                    endif
                else
                    dy = 1
                endif

                if (POINT(i,j,k)%diri) then
                    ! ohmic contact
                    pot = get_bias(POINT(i,j,k)%cont_id)
                    CONT_POI%rhs(POINT(i,j,k)%id)   = CONT_POI%var(POINT(i,j,k)%id) - (CON(POINT(i,j,k)%cont_id)%efi + pot )
                    
                    ! A(i,i  ) ---------------------------------------------
                    n = n + 1
                    CONT_POI%jacobi(psi_)%values(n) = dble(1)
                    if (DD_TL) CONT_POI%jacobi(tl_)%values(i)  = -CON(POINT(i,j,k)%cont_id)%efi_dtl

                else
                    e_px        = 0
                    e_py        = 0
                    e_mx        = 0
                    e_my        = 0
                    e_px_dpsi_m = 0
                    e_px_dpsi_p = 0
                    e_mx_dpsi_m = 0
                    e_mx_dpsi_p = 0
                    e_py_dpsi_m = 0
                    e_py_dpsi_p = 0
                    e_my_dpsi_m = 0
                    e_my_dpsi_p = 0

                    eps = POINT(i,j,k)%eps
                    if (i.gt.1) then  !linker Punkt existiert 
                        eps_mx      = (POINT(i-1,j,k)%eps + POINT(i,j,k)%eps)*0.5
                        e_mx        = ( CONT_POI%var(POINT(i,j,k)%id)   - CONT_POI%var(POINT(i-1,j,k)%id) ) /DXX(i-1) * eps_mx * dy
                        e_mx_dpsi_m = - 1 /DXX(i-1) * eps_mx * dy
                        e_mx_dpsi_p = - e_mx_dpsi_m  
                    endif
                    if (j.gt.1)   then 
                        eps_my      = (POINT(i,j,k)%eps + POINT(i,j-1,k)%eps)*0.5
                        e_my        = ( CONT_POI%var(POINT(i,j,k)%id)   - CONT_POI%var(POINT(i,j-1,k)%id) ) /DYY(j-1) * eps_my * dx
                        e_my_dpsi_m = - 1 /DYY(j-1) * eps_my * dx
                        e_my_dpsi_p = - e_my_dpsi_m 
                    endif
                    if (i.lt.NX1) then 
                        eps_px      = (POINT(i+1,j,k)%eps + POINT(i,j,k)%eps)*0.5
                        e_px        = ( CONT_POI%var(POINT(i+1,j,k)%id) - CONT_POI%var(POINT(i,j,k)%id)   ) /DXX(i) * eps_px * dy
                        e_px_dpsi_m = - 1 /DXX(i) * eps_px * dy
                        e_px_dpsi_p = - e_px_dpsi_m 
                    endif
                    if (j.lt.NY1) then 
                        eps_py      = (POINT(i,j,k)%eps + POINT(i,j+1,k)%eps)*0.5
                        e_py        = ( CONT_POI%var(POINT(i,j+1,k)%id) - CONT_POI%var(POINT(i,j,k)%id)   ) /DYY(j) * eps_py * dx
                        e_py_dpsi_m = - 1 /DYY(j) * eps_py * dx
                        e_py_dpsi_p = - e_py_dpsi_m 
                    endif

                    ! --> RHS -------------------------------------------------
                    if (POINT(i,j,k)%semi) then
                        CONT_POI%rhs(POINT(i,j,k)%id) = NORM%lambda_N*(e_px - e_mx) + NORM%lambda_N*(e_py - e_my) + dx*dy*(CD(POINT(i,j,k)%sp_idx) - CN(POINT(i,j,k)%sp_idx) - CN2(POINT(i,j,k)%sp_idx) + CP(POINT(i,j,k)%sp_idx))
                    else
                        CONT_POI%rhs(POINT(i,j,k)%id) = NORM%lambda_N*(e_px - e_mx) + NORM%lambda_N*(e_py - e_my) 
                    endif

                    ! --> A(i,i-NX) ---------------------------------------------
                    if (j.gt.1) then
                        n = n + 1
                        CONT_POI%jacobi(psi_)%values(n) = -NORM%lambda_N*e_my_dpsi_m
                    endif

                    ! --> A(i,i-1) ---------------------------------------------
                    if (i.gt.1) then
                        n = n + 1
                        CONT_POI%jacobi(psi_)%values(n) = -NORM%lambda_N*e_mx_dpsi_m
                    endif

                    ! --> A(i,i  ) ---------------------------------------------
                    n = n + 1
                    if (POINT(i,j,k)%semi) then
                        CONT_POI%jacobi(psi_)%values( n ) = NORM%lambda_N*(e_px_dpsi_m  - e_mx_dpsi_p ) + NORM%lambda_N*(e_py_dpsi_m  - e_my_dpsi_p ) - dx*dy*(- DN(POINT(i,j,k)%sp_idx)%dens_dqf - DN2(POINT(i,j,k)%sp_idx)%dens_dqf + DP(POINT(i,j,k)%sp_idx)%dens_dqf)
                    else
                        CONT_POI%jacobi(psi_)%values( n ) = NORM%lambda_N*(e_px_dpsi_m  - e_mx_dpsi_p ) + NORM%lambda_N*(e_py_dpsi_m  - e_my_dpsi_p )
                    endif
                    if (simul.and.(POINT(i,j,k)%semi)) then
                        if (DD_ELEC) then
                            CONT_POI%jacobi(phin_)%values( POINT(i,j,k)%sp_idx )  =  + dx*dy*(- DN(POINT(i,j,k)%sp_idx)%dens_dqf  )
                        endif
                        if (DD_CONT_BOHM_N) then
                            CONT_POI%jacobi(ln_)%values( POINT(i,j,k)%sp_idx )    =  - dx*dy*(- DN(POINT(i,j,k)%sp_idx)%dens_dqf  ) !same as with respect to psi, but not electric field
                        endif
                        if (DD_HOLE) then
                            CONT_POI%jacobi(phip_)%values( POINT(i,j,k)%sp_idx )  =  + dx*dy*(+ DP(POINT(i,j,k)%sp_idx)%dens_dqf  )
                        endif
                        if (DD_ELEC2) then
                            CONT_POI%jacobi(phin2_)%values( POINT(i,j,k)%sp_idx ) =  + dx*dy*(- DN2(POINT(i,j,k)%sp_idx)%dens_dqf  )
                        endif
                        if (DD_TL) then
                            CONT_POI%jacobi(tl_)%values( i )                  =    dx*dy*( - DN2(POINT(i,j,k)%sp_idx)%dens_dtl - DN(POINT(i,j,k)%sp_idx)%dens_dtl + DP(POINT(i,j,k)%sp_idx)%dens_dtl )
                        endif
                        if (DD_TN) then
                            CONT_POI%jacobi(tn_)%values( i )                  =    dx*dy*( - DN(POINT(i,j,k)%sp_idx)%dens_dtl )
                        endif
                    endif

                    ! --> A(i,i+1) ---------------------------------------------
                    if (i.lt.NX1) then
                        n = n + 1
                        CONT_POI%jacobi(psi_)%values( n ) =  NORM%lambda_N*e_px_dpsi_p 
                    endif

                    ! --> A(i,i+NX) ---------------------------------------------
                    if (j.lt.NY1) then
                      n = n + 1
                      CONT_POI%jacobi(psi_)%values( n ) =  NORM%lambda_N*e_py_dpsi_p 
                    endif
                endif
            enddo
        enddo
    enddo
    ! allocate(rhs(size(CONT_POI%rhs)))
    n           = CONT_POI%jacobi(psi_)%nr
    CONT_POI%rhs  = -CONT_POI%rhs
endsubroutine

! set values of ac poisson equation
subroutine set_poi_ac(omega)
  real(8),intent(in) :: omega
  real(8) :: dummy
  dummy = omega

endsubroutine



endmodule
