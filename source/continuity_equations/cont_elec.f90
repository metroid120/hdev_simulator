!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!continuity equation of electrons
module continuity_elec

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

implicit none

type, extends(cont) :: cont_elec_ 
  contains
  !these methods of the parent class are overwritten
  procedure,nopass   :: init   => init_cont_elec
  procedure,nopass   :: set    => set_cont_elec
  procedure,nopass   :: set_ac => set_cont_elec_ac
  procedure,nopass   :: init_cont_elec_qfn
  procedure,nopass   :: init_cont_elec_qfn2
  procedure,nopass   :: init_cont_elec_qfp
  procedure,nopass   :: init_cont_elec_psi
  procedure,nopass   :: init_cont_elec_tl
  procedure,nopass   :: init_cont_elec_tn
  procedure,nopass   :: init_cont_elec_ln
endtype

type(cont_elec_),target             :: CONT_ELEC

public CONT_ELEC

contains

subroutine init_cont_elec(var)
integer :: var,n
    if (.not.CONT_ELEC%active) then
      allocate(CONT_ELEC%jacobi(nvar-1))
      allocate(CONT_ELEC%jacobi_ac(nvar-1))
      allocate(CONT_ELEC%rhs(N_SP))
      allocate(CONT_ELEC%delta(N_SP))
      allocate(CONT_ELEC%var(N_SP))
      allocate(CONT_ELEC%var_last(N_SP))
      CONT_ELEC%var                 = 0
      CONT_ELEC%var_last            = 0
      CONT_ELEC%rhs                 = 0
      CONT_ELEC%delta               = 0
      CONT_ELEC%active              = .true.
      CONT_ELEC%jacobi(:)%active    = .false.
      CONT_ELEC%jacobi_ac(:)%active = .false.
      CONT_ELEC%i_var               = phin_

      CONT_ELEC%atol   = US%cont_elec%atol
      CONT_ELEC%rtol   = US%cont_elec%rtol
      CONT_ELEC%ctol   = US%cont_elec%ctol
      CONT_ELEC%delta_max   = US%cont_elec%delta_max


      !obtain band for electron continuity equation
      do n=1,size(SEMI(1)%band_pointer)
        if (SEMI(1)%band_pointer(n)%band%iscb) then
          CONT_ELEC%valley = get_conduction_band_index(SEMI(1))
          exit
        endif
      enddo

    endif
    if (var.eq.phin_) then
        call CONT_ELEC%init_cont_elec_qfn
    elseif (var.eq.phin2_) then
        call CONT_ELEC%init_cont_elec_qfn2
    elseif (var.eq.phip_) then
        call CONT_ELEC%init_cont_elec_qfp
    elseif (var.eq.psi_) then
        call CONT_ELEC%init_cont_elec_psi
    elseif (var.eq.tl_) then
        call CONT_ELEC%init_cont_elec_tl
    elseif (var.eq.ln_) then
        call CONT_ELEC%init_cont_elec_ln
    elseif (var.eq.tn_) then
        call CONT_ELEC%init_cont_elec_tn
    else
        write(*,*) '***error*** initialization routine of CONT_ELEC not implemented for this case.'
        stop
    endif
endsubroutine

subroutine init_cont_elec_ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init bohm potential equation with respect to ln
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_ELEC%jacobi(ln_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_ELEC%jacobi(ln_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_ELEC%jacobi(ln_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_ELEC%jacobi(ln_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_ELEC%jacobi(ln_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_ELEC%jacobi(ln_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_ELEC%jacobi(ln_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_ELEC%jacobi(ln_)%columns(n) = SP(i)%id_sy_upp
        endif

    enddo
    CONT_ELEC%jacobi(ln_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(ln_), CONT_ELEC%jacobi_ac(ln_))
        CONT_ELEC%jacobi_ac(ln_)%active=.true.
    endif
endsubroutine

subroutine init_cont_elec_tn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to tn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_ELEC%jacobi(tn_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_ELEC%jacobi(tn_)%active=.true.

    ! --> set row_index/columns
    ! --> very slow for large structure ... maybe use POINT struct instead of SP?
    n=0
    do i=1,N_SP
        CONT_ELEC%jacobi(tn_)%row_index(i) = n+1
        if (SP(i)%y_low_exist) then
            n = n+1
            CONT_ELEC%jacobi(tn_)%columns(n) = SP(i)%id_sy_low
        endif
        if (SP(i)%x_low_exist) then
            n = n+1
            CONT_ELEC%jacobi(tn_)%columns(n) = SP(i)%id_sx_low
        endif
        n = n+1
        CONT_ELEC%jacobi(tn_)%columns(n) = i
        if (SP(i)%x_upp_exist) then
            n = n+1
            CONT_ELEC%jacobi(tn_)%columns(n) = SP(i)%id_sx_upp
        endif
        if (SP(i)%y_upp_exist) then
            n = n+1
            CONT_ELEC%jacobi(tn_)%columns(n) = SP(i)%id_sy_upp
        endif
    enddo
    CONT_ELEC%jacobi(tn_)%row_index(N_SP+1) = n+1


    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(tn_), CONT_ELEC%jacobi_ac(tn_))
        CONT_ELEC%jacobi_ac(tn_)%active=.true.
    endif

endsubroutine

subroutine init_cont_elec_qfn
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to phin
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n,i,j


    if (.not.DD_TUNNEL) then
        ! --> number of nonzeros
        n = 0
        do i=1,N_SP
            n = n+1
            if (SP(i)%x_low_exist) n = n+1
            if (SP(i)%x_upp_exist) n = n+1
            if (SP(i)%y_low_exist) n = n+1
            if (SP(i)%y_upp_exist) n = n+1
        enddo

        ! --> get size
        call CONT_ELEC%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=n)
        CONT_ELEC%jacobi(phin_)%active=.true.

        ! --> set row_index/columns
        ! --> very slow for large structure ... maybe use POINT struct instead of SP?
        n=0
        do i=1,N_SP
            CONT_ELEC%jacobi(phin_)%row_index(i) = n+1
            if (SP(i)%y_low_exist) then
                n = n+1
                CONT_ELEC%jacobi(phin_)%columns(n) = SP(i)%id_sy_low
                SP(i)%id_cont_qfn_y_low   = n
            endif
            if (SP(i)%x_low_exist) then
                n = n+1
                CONT_ELEC%jacobi(phin_)%columns(n) = SP(i)%id_sx_low
                SP(i)%id_cont_qfn_x_low   = n
            endif
            n = n+1
            CONT_ELEC%jacobi(phin_)%columns(n) = i
            SP(i)%id_cont_qfn                  = n
            if (SP(i)%x_upp_exist) then
                n = n+1
                CONT_ELEC%jacobi(phin_)%columns(n) = SP(i)%id_sx_upp
                SP(i)%id_cont_qfn_x_upp   = n
            endif
            if (SP(i)%y_upp_exist) then
                n = n+1
                CONT_ELEC%jacobi(phin_)%columns(n) = SP(i)%id_sy_upp
                SP(i)%id_cont_qfn_y_upp   = n
            endif

        enddo
        CONT_ELEC%jacobi(phin_)%row_index(N_SP+1) = n+1
    else !with tunneling
        ! --> number of nonzeros
        n = 0
        do i=1,N_SP
            if (SP(i)%set_qfn) then
                n = n+1

            else
                n = n+N_SP

            endif
        enddo

        ! --> get size
        call CONT_ELEC%jacobi(phin_)%init(nr=N_SP, nc=N_SP, nz=n)
        CONT_ELEC%jacobi(phin_)%active=.true.

        ! --> set row_index/columns
        ! --> very slow for large structure ... maybe use POINT struct instead of SP?
        n=0
        do i=1,N_SP
            CONT_ELEC%jacobi(phin_)%row_index(i) = n+1
            if (SP(i)%set_qfn) then
                n                                  = n+1
                CONT_ELEC%jacobi(phin_)%columns(n) = i
                SP(i)%id_cont_qfn                  = n
            else
                do j=1,N_SP
                    n = n+1
                    CONT_ELEC%jacobi(phin_)%columns(n) = j

                    if (j.eq.i) then
                        SP(i)%id_cont_qfn         = n

                    elseif (j.eq.SP(i)%id_sx_low) then
                        SP(i)%id_cont_qfn_x_low   = n

                    elseif (j.eq.SP(i)%id_sx_upp) then
                        SP(i)%id_cont_qfn_x_upp   = n

                    endif

                enddo
            endif
        enddo
        CONT_ELEC%jacobi(phin_)%row_index(N_SP+1) = n+1

    endif

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(phin_), CONT_ELEC%jacobi_ac(phin_))
        CONT_ELEC%jacobi_ac(phin_)%active=.true.
        allocate(CONT_ELEC%delta_ac(size(CONT_ELEC%delta)))
    endif

endsubroutine

subroutine init_cont_elec_qfp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to phip
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: i

    ! --> get size
    call CONT_ELEC%jacobi(phip_)%init(nr=N_SP, nc=N_SP, nz=N_SP)
    CONT_ELEC%jacobi(phip_)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
        CONT_ELEC%jacobi(phip_)%row_index(i)  = i
        CONT_ELEC%jacobi(phip_)%columns(i)    = i
    enddo
    CONT_ELEC%jacobi(phip_)%row_index(N_SP+1) = N_SP+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(phip_), CONT_ELEC%jacobi_ac(phip_))
        CONT_ELEC%jacobi_ac(phip_)%active =.true.
    endif

endsubroutine

subroutine init_cont_elec_psi
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! electron continuity equation coupling to electrostatic potential
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j

    if (.not.DD_TUNNEL) then ! for testing
        !if (.false.) then 
        ! --> get size
        n = 0
        do i=1,N_SP
            n = n + 1
            if (SP(i)%x_low_exist) then
                n = n + 1
            endif

            if (SP(i)%x_upp_exist) then
                n = n + 1
            endif

            if (SP(i)%y_low_exist) then
                n = n + 1
            endif

            if (SP(i)%y_upp_exist) then
                n = n + 1
            endif

        enddo

        call CONT_ELEC%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_ELEC%jacobi(psi_)%active =.true.

        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_ELEC%jacobi(psi_)%row_index(i) = n+1
            if (SP(i)%y_low_exist) then
                n = n+1
                CONT_ELEC%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_low)%id_p
                SP(i)%id_cont_elec_psi_y_low   = n
            endif
            if (SP(i)%x_low_exist) then
                n = n+1
                CONT_ELEC%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_low)%id_p
                SP(i)%id_cont_elec_psi_x_low   = n
            endif
            n = n+1
            CONT_ELEC%jacobi(psi_)%columns(n)  = SP(i)%id_p
            SP(i)%id_cont_elec_psi                  = n
            if (SP(i)%x_upp_exist) then
                n = n+1
                CONT_ELEC%jacobi(psi_)%columns(n) = SP(SP(i)%id_sx_upp)%id_p
                SP(i)%id_cont_elec_psi_x_upp   = n
            endif
            if (SP(i)%y_upp_exist) then
                n = n+1
                CONT_ELEC%jacobi(psi_)%columns(n) = SP(SP(i)%id_sy_upp)%id_p
                SP(i)%id_cont_elec_psi_y_upp   = n
            endif
        enddo
        CONT_ELEC%jacobi(psi_)%row_index(N_SP+1) = n+1
            
    else 
        ! --> get size
        n = 0
        do i=1,N_SP
            n = n + N_SP !depends on every point ...

        enddo

        call CONT_ELEC%jacobi(psi_)%init(nr=N_SP, nc=NXYZ, nz=n)
        CONT_ELEC%jacobi(psi_)%active =.true.

        ! --> set row_index/columns
        n=0
        do i=1,N_SP
            CONT_ELEC%jacobi(psi_)%row_index(i) = n+1
            do j=1,N_SP
                n                                  = n+1
                CONT_ELEC%jacobi(psi_)%columns(n)  = SP(j)%id_p
                if (j.eq.i) then
                    SP(i)%id_cont_elec_psi         = n

                elseif (j.eq.SP(i)%id_sx_low) then
                    SP(i)%id_cont_elec_psi_x_low   = n

                elseif (j.eq.SP(i)%id_sx_upp) then
                    SP(i)%id_cont_elec_psi_x_upp   = n
                
                endif
            enddo
        enddo
        CONT_ELEC%jacobi(psi_)%row_index(N_SP+1) = n+1
        
    endif

    ! --> check crs
    call CONT_ELEC%jacobi(psi_)%check

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(psi_), CONT_ELEC%jacobi_ac(psi_))
        CONT_ELEC%jacobi_ac(psi_)%active =.true.
    endif

endsubroutine

subroutine init_cont_elec_tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init hole continuituity jacobi with respect to phip
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n

    ! --> get size
    call CONT_ELEC%jacobi(tl_)%init(nr=N_SP, nc=1, nz=N_SP)
    CONT_ELEC%jacobi(tl_)%active=.true.

    ! --> set row_index/columns
    CONT_ELEC%jacobi(tl_)%columns     = 1 !all values in first column
    do n=1,N_SP !every value new row
        CONT_ELEC%jacobi(tl_)%row_index(n)   = n
    enddo
    CONT_ELEC%jacobi(tl_)%row_index(N_SP+1)   = N_SP + 1

    ! --> complex matrix for AC
    if (AC_SIM) then !todo
        call copy_crs(CONT_ELEC%jacobi(tl_), CONT_ELEC%jacobi_ac(tl_))
        CONT_ELEC%jacobi_ac(tl_)%active =.true.
    endif
endsubroutine

subroutine init_cont_elec_qfn2
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! init electron continuituity jacobi with respect to phin
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    integer :: n,i,j

    ! --> number of nonzeros
    n = 0
    do i=1,N_SP
        n = n+1
        if (SP(i)%x_low_exist) n = n+1
        if (SP(i)%x_upp_exist) n = n+1
        if (SP(i)%y_low_exist) n = n+1
        if (SP(i)%y_upp_exist) n = n+1
    enddo

    ! --> get size
    call CONT_ELEC%jacobi(phin2_)%init(nr=N_SP, nc=N_SP, nz=n)
    CONT_ELEC%jacobi(phin2_)%active=.true.

    ! --> set row_index/columns
    n=0
    do i=1,N_SP
        CONT_ELEC%jacobi(phin2_)%row_index(i) = n+1
        do j=1,N_SP
            if     (j.eq.i) then
                n = n+1
                CONT_ELEC%jacobi(phin2_)%columns(n) = i
                !SP(i)%id_cont_qfn         = n

            elseif (j.eq.SP(i)%id_sx_low) then
                n = n+1
                CONT_ELEC%jacobi(phin2_)%columns(n) = SP(i)%id_sx_low
                !SP(i)%id_cont_qfn_x_low   = n

            elseif (j.eq.SP(i)%id_sx_upp) then
                n = n+1
                CONT_ELEC%jacobi(phin2_)%columns(n) = SP(i)%id_sx_upp
                !SP(i)%id_cont_qfn_x_upp   = n

            elseif (j.eq.SP(i)%id_sy_low) then
                n = n+1
                CONT_ELEC%jacobi(phin2_)%columns(n) = SP(i)%id_sy_low
                !SP(i)%id_cont_qfn_y_low   = n

            elseif (j.eq.SP(i)%id_sy_upp) then
                n = n+1
                CONT_ELEC%jacobi(phin2_)%columns(n) = SP(i)%id_sy_upp
                !SP(i)%id_cont_qfn_y_upp   = n

            endif
        enddo
    enddo
    CONT_ELEC%jacobi(phin2_)%row_index(N_SP+1) = n+1

    ! --> complex matrix for AC
    if (AC_SIM) then
        call copy_crs(CONT_ELEC%jacobi(phin2_), CONT_ELEC%jacobi_ac(phin2_))
        CONT_ELEC%jacobi_ac(phin2_)%active=.true.
    endif
endsubroutine

subroutine set_cont_elec(simul)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set matrix values for solution of electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx=0,px=0,my=0,py=0,j=0,n=0,m=0
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_px,j_px_dqfm,j_px_dqfp,j_px_dpsim,j_px_dpsip,j_px_dtlm, j_px_dtlp
    real(8) :: j_mx,j_mx_dqfm,j_mx_dqfp,j_mx_dpsim,j_mx_dpsip,j_mx_dtlm, j_mx_dtlp
    real(8) :: j_py,j_py_dqfm,j_py_dqfp,j_py_dpsim,j_py_dpsip,j_py_dtlm, j_py_dtlp
    real(8) :: j_my,j_my_dqfm,j_my_dqfp,j_my_dpsim,j_my_dpsip,j_my_dtlm, j_my_dtlp
    real(8) :: rhs
    integer :: index_g_tun_dphi

    if (.not.(CONT_ELEC%active)) return
    
    ! ---> nullify data fields
    CONT_ELEC%rhs                     = 0 ! right hand side
    CONT_ELEC%jacobi(phin_)%values    = 0 ! nonzero elements in matrix
    if (simul) then
      CONT_ELEC%jacobi(psi_)%values   = 0
    endif
    if (DD_HOLE.and.simul) then
      CONT_ELEC%jacobi(phip_)%values  = 0
    endif
    if (DD_TN.and.simul) then
      CONT_ELEC%jacobi(tn_)%values  = 0
    endif
    if (DD_ELEC2.and.simul) then
      CONT_ELEC%jacobi(phin2_)%values = 0
    endif
    if (DD_TL.and.simul) then
      CONT_ELEC%jacobi(tl_)%values = 0
    endif
    if (DD_CONT_BOHM_N.and.simul) then
      CONT_ELEC%jacobi(ln_)%values = 0
    endif
    
    is_2d      = SEMI_DIM.ge.2
    dy         = 1
    j_py       = 0
    j_py_dqfm  = 0
    j_py_dpsim = 0
    j_py_dqfp  = 0
    j_py_dpsip = 0
    j_py_dtlm  = 0
    j_py_dtlp  = 0
    j_my       = 0
    j_my_dqfm  = 0
    j_my_dpsim = 0
    j_my_dqfp  = 0
    j_my_dpsip = 0
    j_my_dtlp  = 0
    j_my_dtlm  = 0

    index_g_tun_dphi = 0

    if (is_2d.and.DD_TN.and.(DD_ELEC2.or.DD_TL)) then
        ! for this we need to implement some more Jacobian entries
        write(*,*) '***error*** 2D ET Simulations with second electron band or self-heating not implemented. '
        stop
    endif

    ! --> loop over position
    do n=1,N_SP
      mx = SP(n)%id_sx_low
      px = SP(n)%id_sx_upp
      dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
      if (is_2d) then
        my = SP(n)%id_sy_low
        py = SP(n)%id_sy_upp
        dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)
      endif
      
      ! scal=get_scal(dx,dy,CHARGE_DIM)
      
      if ((SP(n)%set_qfn)) then
            ! ohmic contact, qfn=psi
            CONT_ELEC%rhs(n) = CONT_ELEC%var(n) - get_bias(SP(n)%cont_id)
            
            ! A(i,i  ) ---------------------------------------------
            CONT_ELEC%jacobi(phin_)%values(SP(n)%id_cont_qfn) = dble(1)
    
      elseif (SP(n)%set_neutrality_n) then
        ! electrons are majorities
        CONT_ELEC%rhs(n) = (CD(n) - CN(n)- CN2(n) + CP(n))*Q*dx*dy
        ! A(i,i  ) ---------------------------------------------
        CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn ) = -DN(n)%dens_dqf*Q*dx*dy
        if (simul) then
          CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi)   = (DN(n)%dens_dqf + DN2(n)%dens_dqf - DP(n)%dens_dqf)*Q*dx*dy
        endif
        if (DD_HOLE.and.simul) then
          CONT_ELEC%jacobi(phip_)%values(n)                  =  DP(n)%dens_dqf*Q*dx*dy
        endif
        if (DD_ELEC2.and.simul) then
          CONT_ELEC%jacobi(phin2_)%values(  SP(n)%id_cont_qfn )              = -DN2(n)%dens_dqf*Q*dx*dy
        endif

        if (simul) then
            CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi)     = (+DN(n)%dens_dqf )*Q*dx*dy
        endif
        ! if (DD_ELEC2.and.simul) then
        !   CONT_ELEC%jacobi(phin2_)%values( SP(n)%id_cont_qfn )       = 0
        ! endif

        ! !intervalley zeta
        ! CONT_ELEC%rhs(n) = CN(n)/CN2(n) - DN(n)%zeta
        ! ! A(i,i  ) ---------------------------------------------
        ! CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn ) = DN(n)%dens_dqf/CN2(n) 
   
      else
        ! current boundary
        j_px       = DN(n)%j_xh           ! (i+1/2)

        j_px_dqfm  = DN(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
        j_px_dpsim = DN(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
        j_px_dtlm  = DN(n)%j_xh_dtl(1)    ! (i+1/2)/(i)

        j_px_dqfp  = DN(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
        j_px_dpsip = DN(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
        j_px_dtlp  = DN(n)%j_xh_dtl(2)    ! (i+1/2)/(i+1)
       
        
        if (SP(n)%x_low_exist) then
            j_mx       = DN(mx)%j_xh          ! (i-1/2)

            j_mx_dqfm  = DN(mx)%j_xh_dqf(1)   ! (i-1/2)/(i-1)
            j_mx_dpsim = DN(mx)%j_xh_dpsi(1)  ! (i-1/2)/(i-1)
            j_mx_dtlm  = DN(mx)%j_xh_dtl(1)   ! (i-1/2)/(i-1)
            
            j_mx_dqfp  = DN(mx)%j_xh_dqf(2)   ! (i-1/2)/(i)
            j_mx_dpsip = DN(mx)%j_xh_dpsi(2)  ! (i-1/2)/(i)
            j_mx_dtlp  = DN(mx)%j_xh_dtl(2)   ! (i-1/2)/(i)
          
        else
            j_mx       = 0  ! (i-1/2)
            j_mx_dqfm  = 0  ! (i-1/2)/(i-1)
            j_mx_dpsim = 0  ! (i-1/2)/(i-1)
            j_mx_dqfp  = 0  ! (i-1/2)/(i)
            j_mx_dpsip = 0  ! (i-1/2)/(i)
            j_mx_dtlp  = 0
            j_mx_dtlm  = 0
          
        endif
    
        if (is_2d) then
            j_py       = DN(n)%j_yh           ! (i+1/2)

            j_py_dqfm  = DN(n)%j_yh_dqf(1)    ! (i+1/2)/(i)
            j_py_dpsim = DN(n)%j_yh_dpsi(1)   ! (i+1/2)/(i)
            j_py_dtlm  = DN(n)%j_yh_dtl(1)    ! (i-1/2)/(i-1)

            j_py_dqfp  = DN(n)%j_yh_dqf(2)    ! (i+1/2)/(i+1)
            j_py_dpsip = DN(n)%j_yh_dpsi(2)   ! (i+1/2)/(i+1)
            j_py_dtlp  = DN(n)%j_yh_dtl(2)   ! (i-1/2)/(i)
            
            if (SP(n)%y_low_exist) then
                j_my       = DN(my)%j_yh          ! (i-1/2)

                j_my_dqfm  = DN(my)%j_yh_dqf(1)   ! (i-1/2)/(i-1)
                j_my_dpsim = DN(my)%j_yh_dpsi(1)  ! (i-1/2)/(i-1)
                j_my_dtlm  = DN(my)%j_yh_dtl(1)   ! (i-1/2)/(i-1)

                j_my_dqfp  = DN(my)%j_yh_dqf(2)   ! (i-1/2)/(i)
                j_my_dpsip = DN(my)%j_yh_dpsi(2)  ! (i-1/2)/(i)
                j_my_dtlp  = DN(my)%j_yh_dtl(2)   ! (i-1/2)/(i)
            else
                j_my       = 0  ! (i-1/2)
                j_my_dqfm  = 0  ! (i-1/2)/(i-1)
                j_my_dpsim = 0  ! (i-1/2)/(i-1)
                j_my_dtlm  = 0
                j_my_dqfp  = 0  ! (i-1/2)/(i)
                j_my_dpsip = 0  ! (i-1/2)/(i)
                j_my_dtlp  = 0
            
            endif
        endif
    
        ! --> schottky contact point
        if (SP(n)%cont_left) then
            ! --> contact at left side
            j_mx       = DN(n)%j_con       ! contact (i)
            j_mx_dqfm  = 0
            j_mx_dqfp  = DN(n)%j_con_dqf   ! contact (i)/(i)
            j_mx_dpsim = 0                 
            j_mx_dpsip = DN(n)%j_con_dpsi  ! contact (i)/(i)
            j_mx_dtlm  = 0                 
            j_mx_dtlp  = DN(n)%j_con_dtl
        endif
    
        if (SP(n)%cont_right) then
            ! --> contact at right side
            j_px       = -DN(n)%j_con       ! contact (i)
            j_px_dqfm  = -DN(n)%j_con_dqf   ! contact (i)/(i)
            j_px_dqfp  = 0
            j_px_dpsim = -DN(n)%j_con_dpsi  ! contact (i)/(i)
            j_px_dpsip = 0  
            j_px_dtlm  = -DN(n)%j_con_dtl                 
            j_px_dtlp  = 0 
        endif
    
        if (is_2d) then
            if (SP(n)%cont_lower) then
                ! --> contact at lower side
                j_my       = DN(n)%j_con       ! contact (i)
                j_my_dqfm  = 0
                j_my_dqfp  = DN(n)%j_con_dqf   ! contact (i)/(i)
                j_my_dpsim = 0  
                j_my_dpsip = DN(n)%j_con_dpsi  ! contact (i)/(i)
                j_my_dtlm  = 0                 
                j_my_dtlp  = DN(n)%j_con_dtl
            endif
        
            if (SP(n)%cont_upper) then
                ! --> contact at upper side
                j_py       = -DN(n)%j_con       ! contact (i)
                j_py_dqfm  = -DN(n)%j_con_dqf   ! contact (i)/(i)
                j_py_dqfp  = 0
                j_py_dpsim = -DN(n)%j_con_dpsi
                j_py_dpsip = 0
                j_py_dtlm  = -DN(n)%j_con_dtl                 
                j_py_dtlp  = 0
            endif
        endif

        !set zero derivatives of current if boundary condition
        ! if (mx.gt.0) then
        !   if (SP(mx)%set_qfn) then
        !     j_mx_dqfm = 0
        !   endif
        ! endif
        ! if (px.gt.0) then
        !   if (SP(px)%set_qfn) then
        !     j_px_dqfp = 0
        !   endif
        ! endif
        ! if (my.gt.0) then
        !   if (SP(my)%set_qfn) then
        !     j_my_dqfm = 0
        !   endif
        ! endif
        ! if (py.gt.0) then
        !   if (SP(py)%set_qfn) then
        !     j_py_dqfp = 0
        !   endif
        ! endif

        ! bulk contact experimental
        ! --> schottky contact point
        ! current boundary
        ! if (.not.SP(n)%x_upp_exist) then
        !   j_px       = DN(1)%j_xh           ! (i+1/2)
        !   j_px_dqfm  = DN(1)%j_xh_dqf(1)    ! (i+1/2)/(i)
        !   j_px_dpsim = DN(1)%j_xh_dpsi(1)   ! (i+1/2)/(i)
        !   j_px_dqfp  = DN(1)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
        !   j_px_dpsip = DN(1)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
        
        ! elseif (.not.SP(n)%x_low_exist) then
        !   j_mx       = DN(NX1-1)%j_xh          ! (i-1/2)
        !   j_mx_dqfm  = DN(NX1-1)%j_xh_dqf(1)   ! (i-1/2)/(i-1)
        !   j_mx_dpsim = DN(NX1-1)%j_xh_dpsi(1)  ! (i-1/2)/(i-1)
        !   j_mx_dqfp  = DN(NX1-1)%j_xh_dqf(2)   ! (i-1/2)/(i)
        !   j_mx_dpsip = DN(NX1-1)%j_xh_dpsi(2)  ! (i-1/2)/(i)
          
        ! endif
    
        ! --> RHS -------------------------------------------------
        rhs              = (j_px - j_mx)*dy + (j_py - j_my)*dx + dx*dy*(-DN(n)%recomb - DN(n)%intervalley - DN(n)%g_tun)
        CONT_ELEC%rhs(n) = rhs

        ! --> A(i,i-NX) ---------------------------------------------
        if (SP(n)%y_low_exist) then
            CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn_y_low ) = -j_my_dqfm *dx
            if (simul) then
                CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_y_low) = -j_my_dpsim*dx
                if (DD_TN) CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn_y_low) = - j_my_dtlm*dx
            endif
        endif
    
        ! --> A(i-1,i) ---------------------------------------------
        if (SP(n)%x_low_exist) then
            CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn_x_low ) = -j_mx_dqfm *dy &
            & + dx*dy*(-DN(n)%recomb_dqf(1)-DN(n)%intervalley_dqf(1) )!+ DN(n)%g_tun_dqf(1))
            if (simul) then
                CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_low)                   = -j_mx_dpsim*dy + dx*dy*(-DN(n)%recomb_dpsi(1)-DN(n)%intervalley_dpsi(1))
                if (DD_ELEC2)        CONT_ELEC%jacobi(phin2_)%values(SP(n)%id_cont_qfn_x_low) = -dx*dy*DN(n)%intervalley_dqf2(1) ! other derivatives ignored...
                if (DD_TL)           CONT_ELEC%jacobi(tl_)%values(n)                          = CONT_ELEC%jacobi(tl_)%values(n) - j_mx_dtlm*dy
                if (DD_TN)           CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn_x_low)    = - j_mx_dtlm*dy
            endif
        endif
    
        ! --> A(i,i  ) ---------------------------------------------
        CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn ) = (j_px_dqfm  - j_mx_dqfp )*dy + (j_py_dqfm  - j_my_dqfp )*dx + dx*dy*(-DN(n)%recomb_dqf(2) - DN(n)%intervalley_dqf(2)   )!+ DN(n)%g_tun_dqf(2))
        if (simul) then
            CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi) = (j_px_dpsim - j_mx_dpsip)*dy + (j_py_dpsim - j_my_dpsip)*dx + dx*dy*(-DN(n)%recomb_dpsi(2)-DN(n)%intervalley_dpsi(2))
            if (DD_TL) then 
                CONT_ELEC%jacobi(tl_)%values(n) =  CONT_ELEC%jacobi(tl_)%values(n) + (j_px_dtlm - j_mx_dtlp)*dy + (j_py_dtlm - j_mx_dtlp)*dx + dx*dy*(-DN(n)%recomb_dtl(2))
            endif
            if (DD_HOLE) then
                CONT_ELEC%jacobi(phip_)%values(n)                    = -dx*dy*DP(n)%recomb_dqf(2) ! other derivatives ignored...
            endif
            if (DD_ELEC2) then
                CONT_ELEC%jacobi(phin2_)%values(SP(n)%id_cont_qfn)   = -dx*dy*DN(n)%intervalley_dqf2(2) ! other derivatives ignored...
            endif
            if (DD_TN) then
                CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn) =  (j_px_dtlm - j_mx_dtlp)*dy + (j_py_dtlm - j_my_dtlp)*dx - dx*dy*DN(n)%recomb_dtn
            endif
        endif
    
        ! --> A(i+1,i) ---------------------------------------------
        if (SP(n)%x_upp_exist) then
            CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn_x_upp ) =  j_px_dqfp *dy  + dx*dy*(-DN(n)%recomb_dqf(3)-DN(n)%intervalley_dqf(3) )!+ DN(n)%g_tun_dqf(3))
            if (simul) then
                CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_x_upp) =  j_px_dpsip*dy  + dx*dy*(-DN(n)%recomb_dpsi(3)-DN(n)%intervalley_dpsi(3))
                if (DD_ELEC2) CONT_ELEC%jacobi(phin2_)%values(SP(n)%id_cont_qfn_x_upp)  = -dx*dy*DN(n)%intervalley_dqf2(3) ! other derivatives ignored...
                if (DD_TL)    CONT_ELEC%jacobi(tl_)%values(n) = CONT_ELEC%jacobi(tl_)%values(n) + j_px_dtlp*dy
                if (DD_TN)    CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn_x_upp) = + j_px_dtlp*dy
            endif
        endif
    
        ! --> A(i,i+NX) ---------------------------------------------
        if (SP(n)%y_upp_exist) then
          CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn_y_upp ) =  j_py_dqfp *dx
          if (simul) then
            CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi_y_upp) =  j_py_dpsip*dx
            if (DD_TN) CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn_y_upp) =  j_py_dtlp*dx
          endif
        endif
    
        ! --> TRANSIENT ---------------------------------
        if (TR_IT.ge.1) then
            ! backward euler:
            CONT_ELEC%rhs(n) = CONT_ELEC%rhs(n) - dx*dy*Q*(CN(n) - CN_LAST(n) )/TR_DT(TR_IT) 
            ! CONT_ELEC%rhs(n) = CONT_ELEC%rhs(n) - dot_product(scal, CN_TRAP(n,:)-CN_TRAP_LAST(n,:) )/TR_DT(TR_IT) !all traps 
        
        
            CONT_ELEC%jacobi(phin_)%values(  SP(n)%id_cont_qfn ) = CONT_ELEC%jacobi(phin_)%values(SP(n)%id_cont_qfn ) - dx*dy*Q*DN(n)%dens_dqf/TR_DT(TR_IT)
        
            if (simul) then
                if (SP(n)%psi_mean) then ! is_2d=.false.
                    do m=1,size(SP(n)%id_p_get_mean)
                        CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) = CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_psi_mean(m)) + dx*dy*Q*DN(n)%dens_dqf/TR_DT(TR_IT)/size(SP(n)%id_p_get_mean)
                    enddo
                else
                    CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_psi) = CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_psi) + dx*dy*Q*DN(n)%dens_dqf/TR_DT(TR_IT)
                endif
        
            endif
        endif
    
        ! --> Tunneling simul to psi ---------------
        if (simul.and.DD_TUNNEL) then
            do j=1,size(DN(n)%g_tun_dpsi)
                !use SP(n)%id_cont_elec_psi ?
                CONT_ELEC%jacobi(psi_)%values((n-1)*N_SP+j) = CONT_ELEC%jacobi(psi_)%values((n-1)*N_SP+j) - dx*dy*DN(n)%g_tun_dpsi(j)
            enddo
            do j=1,size(DN(n)%g_tun_dqf)
                index_g_tun_dphi = index_g_tun_dphi + 1
                CONT_ELEC%jacobi(phin_)%values(index_g_tun_dphi) = CONT_ELEC%jacobi(phin_)%values(index_g_tun_dphi) - dx*dy*DN(n)%g_tun_dqf(j)
            enddo
        endif
    
      endif
    
    enddo


    !derivatives with respect to quantum bohm potential are the same as with respect to psi -> copy over values
    if (simul.and.DD_CONT_BOHM_N) then
      do n=1,N_SP
        if (SP(n)%set_qfn) then
          cycle
        endif
        if (SP(n)%y_low_exist) then
          CONT_ELEC%jacobi(ln_)%values(  SP(n)%id_cont_qfn_y_low ) = CONT_ELEC%jacobi(psi_)%values(  SP(n)%id_cont_elec_psi_y_low)
        endif
        if (SP(n)%y_upp_exist) then
          CONT_ELEC%jacobi(ln_)%values(  SP(n)%id_cont_qfn_y_upp ) = CONT_ELEC%jacobi(psi_)%values(  SP(n)%id_cont_elec_psi_y_upp)
        endif
        CONT_ELEC%jacobi(ln_)%values(  SP(n)%id_cont_qfn )       = CONT_ELEC%jacobi(psi_)%values(  SP(n)%id_cont_elec_psi)
        if (SP(n)%x_low_exist) then
          CONT_ELEC%jacobi(ln_)%values(  SP(n)%id_cont_qfn_x_low ) = CONT_ELEC%jacobi(psi_)%values(  SP(n)%id_cont_elec_psi_x_low)
        endif
        if (SP(n)%x_upp_exist) then
          CONT_ELEC%jacobi(ln_)%values(  SP(n)%id_cont_qfn_x_upp ) = CONT_ELEC%jacobi(psi_)%values(  SP(n)%id_cont_elec_psi_x_upp)
        endif
      enddo
    endif
    
    CONT_ELEC%rhs  = -CONT_ELEC%rhs

    !print maximum of rhs
    ! id_max = maxloc(CONT_ELEC%rhs(:),1)
    ! id_max = 256
    ! write(*,*) 'max is at', id_max
    ! write(*,*) 'jmx is', DN(id_max-1)%j_xh*NORM%J_N
    ! write(*,*) 'jpx is', DN(id_max)%j_xh*NORM%J_N
    ! write(*,*) 'rhs is', CONT_ELEC%rhs(id_max)*NORM%J_N
    
    ! deallocate(scal)
    
endsubroutine

subroutine set_cont_elec_ac(omega)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set matrix values for ac solution of electron continuity equation
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    real(8),intent(in) :: omega !< 2*pi*frequency

    integer :: n
    real(8) :: dx,dy
    logical :: is_2d

    if (.not.(CONT_ELEC%active)) return

    is_2d = SEMI_DIM.ge.2
    dy    = 1

    do n=1,N_SP
                    dx = (SP(n)%dx_low + SP(n)%dx_upp)*dble(0.5)
        if (is_2d)  dy = (SP(n)%dy_low + SP(n)%dy_upp)*dble(0.5)

        !todo: second electron continuity equation
        if (.not.(SP(n)%set_qfn.or.SP(n)%set_neutrality_n)) then !todo:allgeiner Fall
            CONT_ELEC%jacobi_ac(phin_)%values(SP(n)%id_cont_qfn ) = cmplx(CONT_ELEC%jacobi(phin_)%values(SP(n)%id_cont_qfn ),-dx*dy*omega*DN(n)%dens_dqf,8)
            CONT_ELEC%jacobi_ac(psi_)%values(SP(n)%id_cont_elec_psi) = cmplx(CONT_ELEC%jacobi(psi_)%values(SP(n)%id_cont_elec_psi),-dx*dy*omega*(-DN(n)%dens_dqf),8)
            if (DD_TN) then !todo: check influence of this
                CONT_ELEC%jacobi_ac(tn_)%values(SP(n)%id_cont_qfn ) = cmplx(CONT_ELEC%jacobi(tn_)%values(SP(n)%id_cont_qfn ),-dx*dy*omega*DN(n)%dens_dtl,8)
            endif

        endif
    enddo

    ! very important!
    !CONT_ELEC%jacobi_ac(psi_)%values = 0

endsubroutine

endmodule
