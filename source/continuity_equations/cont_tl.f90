!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Hdev
! Copyright (C) 2019  Markus Müller, Sven Mothes
!
! This file is part of hdev.
!
! Hdev is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! Hdev is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! lattice temperature continuity equation
module continuity_tl

use crs_matrix
use contact
use phys_const
use semiconductor
use profile
use semiconductor_globals
use read_user
use continuity
use continuity_elec
use continuity_hole
use continuity_elec2
use bandstructure
use heterostructure
use math_oper
use structure
use save_global
use utils
use dd_types

implicit none

type, extends(cont) :: cont_tl_ 
  contains
  !these methods of the parent class are overwritten
  procedure,nopass   :: init   => init_cont_tl
  procedure,nopass   :: set    => set_cont_tl
  procedure,nopass   :: set_ac => set_cont_tl_ac
  procedure,nopass   :: init_cont_tl_var
  procedure,nopass   :: init_cont_tl_tl
endtype

type(cont_tl_),target             :: CONT_TL

public CONT_TL

contains

subroutine init_cont_tl(var)
integer :: var
    if (.not.CONT_TL%active) then
      allocate(CONT_TL%jacobi(nvar-1))
      allocate(CONT_TL%jacobi_ac(nvar-1))
      allocate(CONT_TL%rhs(1))
      allocate(CONT_TL%delta(1))
      allocate(CONT_TL%var(1))
      allocate(CONT_TL%var_last(1))
      CONT_TL%var                 = TEMP_AMBIENT
      CONT_TL%var_last            = 0
      CONT_TL%delta               = 0
      CONT_TL%rhs                 = 0
      CONT_TL%active              = .true.
      CONT_TL%jacobi(:)%active    = .false.
      CONT_TL%jacobi_ac(:)%active = .false.
      CONT_TL%i_var               = tl_

      CONT_TL%valley    = 0 !has no valley

      CONT_TL%atol      = US%cont_tl%atol
      CONT_TL%rtol      = US%cont_tl%rtol
      CONT_TL%ctol      = US%cont_tl%ctol
      CONT_TL%delta_max = US%cont_tl%delta_max

    endif
    if (var.eq.phin_) then
        call CONT_TL%init_cont_tl_var(var)
    elseif (var.eq.phin2_) then
        call CONT_TL%init_cont_tl_var(var)
    elseif (var.eq.phip_) then
        call CONT_TL%init_cont_tl_var(var)
    elseif (var.eq.psi_) then
        call CONT_TL%init_cont_tl_var(var)
    elseif (var.eq.tl_) then
        call CONT_TL%init_cont_tl_tl
    else
        write(*,*) '***error*** initialization routine of CONT_TL not implemented for this case.'
        stop
    endif
endsubroutine

subroutine init_cont_tl_tl
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobian with respect to lattice temperature
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: n

    ! --> number of nonzeros
    n = 1

    ! --> get size
    call CONT_TL%jacobi(tl_)%init(nr=1, nc=1, nz=1)
    CONT_TL%jacobi(tl_)%active=.true.

    ! --> set row_index/columns
    n=0
    CONT_TL%jacobi(tl_)%row_index(1)      = 1
    CONT_TL%jacobi(tl_)%columns(1)        = 1
    CONT_TL%jacobi(tl_)%row_index(1+1)    = 2

    ! --> complex matrix for AC
    if (AC_SIM) then !todo
    call copy_crs(CONT_TL%jacobi(tl_), CONT_TL%jacobi_ac(tl_))
    CONT_TL%jacobi_ac(tl_)%active =.true.
    endif

endsubroutine

subroutine init_cont_tl_var(var)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! jacobian with respect to all variables but lattice temperature
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    integer :: i,var

    ! --> get size
    call CONT_TL%jacobi(var)%init(nr=1, nc=N_SP, nz=N_SP)
    CONT_TL%jacobi(var)%active=.true.

    ! --> set row_index/columns
    do i=1,N_SP
    CONT_TL%jacobi(var)%columns(i) = i
    enddo
    CONT_TL%jacobi(var)%row_index(1) = 1
    CONT_TL%jacobi(var)%row_index(2) = N_SP + 1 

    ! --> complex matrix for AC
    if (AC_SIM) then
    call copy_crs(CONT_TL%jacobi(var), CONT_TL%jacobi_ac(var))
    CONT_TL%jacobi_ac(var)%active=.true.
    endif

endsubroutine

! set values of lattice temperature equation
subroutine set_cont_tl(simul)

    logical,intent(in) :: simul !< if true: set couple matrix intries for simultaneous solution
    
    integer :: mx=0,px=0,n=0
    logical :: is_2d
    real(8) :: dx,dy
    real(8) :: j_nx, j_n2x, j_px, rth, dt
    real(8) :: j_px_dtlp, j_px_dphip, j_px_dpsip , j_px_dtlm, j_px_dphim, j_px_dpsim
    real(8) :: j_nx_dtlp, j_nx_dphip, j_nx_dpsip , j_nx_dtlm, j_nx_dphim, j_nx_dpsim
    real(8) :: j_n2x_dtlp, j_n2x_dphip, j_n2x_dpsip , j_n2x_dtlm, j_n2x_dphim, j_n2x_dpsim
    real(8) :: en_px = 0 , en_px_dphim = 0 , en_px_dphip=0
    real(8) :: ep_px = 0 , ep_px_dphim = 0 , ep_px_dphip=0
    real(8) :: en2_px = 0, en2_px_dphim = 0, en2_px_dphip=0
    real(8) :: rhs
    real(8),dimension(:),allocatable :: pdiss

    ! real(8),dimension(:),pointer :: scal    ! used to multiply trap charges by correct scaling according to their dimension

    if (.not.(CONT_TL%active)) return
    if (US%dd%rth.eq.0) return
    
    ! allocate(scal(CHARGE_DIM))
    
    ! ---> nullify data fields
    CONT_TL%rhs                     = 0 ! right hand side
    CONT_TL%jacobi(tl_)%values      = 0 ! nonzero elements in matrix
    if (simul) then
      CONT_TL%jacobi(psi_)%values   = 0
    endif
    if (DD_HOLE.and.simul) then
      CONT_TL%jacobi(phip_)%values  = 0
    endif
    if (DD_ELEC.and.simul) then
      CONT_TL%jacobi(phin_)%values = 0
    endif
    if (DD_ELEC2.and.simul) then
      CONT_TL%jacobi(phin2_)%values = 0
    endif
    
    is_2d      = SEMI_DIM.ge.2
    dy         = 1
    
    allocate(pdiss(N_SP))
    pdiss = 0
    
    ! --> loop over position
    ! equation =>   0 = Tamb + rth*J*E - TL
    rth = US%dd%rth
    CONT_TL%rhs(1)                  =  TEMP_AMBIENT - CONT_TL%var(1)
    CONT_TL%jacobi(tl_)%values( 1 ) = -1
    do n=1,N_SP-1
      mx = SP(n)%id_sx_low
      px = SP(n)%id_sx_upp
      dx = SP(n)%dx_upp
      
      ! dx is canceled out here, since field = 1/dx and power *dx
      ! rth is written since we may later need its derivative
      ! field n type
      if (DD_ELEC) then
        en_px       = +( CONT_ELEC%var(n+1) - CONT_ELEC%var(n) )
        en_px_dphim = - CONT_ELEC%var(n)   
        en_px_dphip = + CONT_ELEC%var(n+1)
      endif
      ! field n2 type
      if (DD_ELEC2) then
        en2_px       = +( CONT_ELEC2%var(n+1) - CONT_ELEC2%var(n) ) 
        en2_px_dphim = - CONT_ELEC2%var(n)  
        en2_px_dphip = + CONT_ELEC2%var(n+1) 
      endif
      ! field n2 type
      if (DD_HOLE) then
        ep_px       = +( CONT_HOLE%var(n+1) - CONT_HOLE%var(n) ) 
        ep_px_dphim = - CONT_HOLE%var(n)   
        ep_px_dphip = + CONT_HOLE%var(n+1)
      endif

      ! n type current
      j_nx       = -DN(n)%j_xh           ! (i+1/2)
      j_nx_dphim = -DN(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
      j_nx_dpsim = -DN(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
      j_nx_dtlm  = -DN(n)%j_xh_dtl(1)    ! (i+1/2)/(i)
      j_nx_dphip = -DN(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
      j_nx_dpsip = -DN(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
      j_nx_dtlp  = -DN(n)%j_xh_dtl(2)    ! (i+1/2)/(i+1)

      ! n2 type current
      j_n2x      = -DN2(n)%j_xh           ! (i+1/2)
      j_n2x_dphim= -DN2(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
      j_n2x_dpsim= -DN2(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
      j_n2x_dtlm = -DN2(n)%j_xh_dtl(1)    ! (i+1/2)/(i)
      j_n2x_dphip= -DN2(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
      j_n2x_dpsip= -DN2(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
      j_n2x_dtlp = -DN2(n)%j_xh_dtl(2)    ! (i+1/2)/(i+1)

      ! p type current
      j_px       = -DP(n)%j_xh           ! (i+1/2)
      j_px_dphim = -DP(n)%j_xh_dqf(1)    ! (i+1/2)/(i)
      j_px_dpsim = -DP(n)%j_xh_dpsi(1)   ! (i+1/2)/(i)
      j_px_dtlm  = -DP(n)%j_xh_dtl(1)    ! (i+1/2)/(i)
      j_px_dphip = -DP(n)%j_xh_dqf(2)    ! (i+1/2)/(i+1)
      j_px_dpsip = -DP(n)%j_xh_dpsi(2)   ! (i+1/2)/(i+1)
      j_px_dtlp  = -DP(n)%j_xh_dtl(2)    ! (i+1/2)/(i+1)
 
      
      ! --> RHS -------------------------------------------------
      CONT_TL%rhs(1) = CONT_TL%rhs(1) +  (j_nx * en_px + j_n2x * en2_px + j_px * ep_px) * rth
      pdiss(n) = (j_nx * en_px + j_n2x * en2_px + j_px * ep_px) * rth
  
      ! --> A(i,i  ) ---------------------------------------------
      CONT_TL%jacobi(tl_)%values( 1 )                     = CONT_TL%jacobi(tl_)%values( 1 ) + (j_nx_dtlm * en_px  + j_n2x_dtlm * en2_px  + j_px_dtlm * ep_px) * rth
      if (simul) then
        CONT_TL%jacobi(psi_)%values(n)                    = CONT_TL%jacobi(psi_)%values(n) + (j_nx_dpsim * en_px + j_n2x_dpsim * en2_px + j_px_dpsim * ep_px)  * rth
        !if (DD_HOLE.and.(.not.SP(n)%set_qfp)) then
        if (DD_HOLE) then
          CONT_TL%jacobi(phip_)%values(n)                   = CONT_TL%jacobi(phip_)%values(n) + j_px_dphim * ep_px * rth   + j_px * ep_px_dphim*rth
        endif
        if (DD_ELEC.and.simul) then
          CONT_TL%jacobi(phin_)%values(n)                   = CONT_TL%jacobi(phin_)%values(n) + j_nx_dphim * en_px * rth   + j_nx * en_px_dphim*rth
        endif
        if (DD_ELEC2.and.simul) then
          CONT_TL%jacobi(phin2_)%values(n)                  = CONT_TL%jacobi(phin2_)%values(n) + j_n2x_dphim * en2_px * rth   + j_n2x * en2_px_dphim*rth
        endif
      endif
  
      ! --> A(i,i  ) ---------------------------------------------
      CONT_TL%jacobi(tl_)%values( 1 )                   = CONT_TL%jacobi(tl_)%values( 1 ) + (j_nx_dtlp * en_px + j_n2x_dtlp* en2_px + j_px_dtlp* ep_px)  * rth
      if (simul) then
        CONT_TL%jacobi(psi_)%values( n + 1)             = CONT_TL%jacobi(psi_)%values( n + 1) + (j_nx_dpsip * en_px + j_n2x_dpsip * en2_px  + j_px_dpsip * ep_px)  * rth
        !if (DD_HOLE.and.(.not.SP(n+1)%set_qfp)) then
        if (DD_HOLE) then
          CONT_TL%jacobi(phip_)%values( n + 1)            = CONT_TL%jacobi(phip_)%values( n + 1) + j_px_dphip * ep_px * rth + j_px * ep_px_dphip * rth
        endif
        if (DD_ELEC) then
          CONT_TL%jacobi(phin_)%values( n + 1)            = CONT_TL%jacobi(phin_)%values( n + 1) + j_nx_dphip * en_px * rth + j_nx * en_px_dphip * rth
        endif
        if (DD_ELEC2) then
          CONT_TL%jacobi(phin2_)%values( n + 1)           = CONT_TL%jacobi(phin2_)%values( n + 1)+ j_n2x_dphip * en2_px * rth + j_n2x * en2_px_dphip * rth
        endif
      endif
  
      ! --> TRANSIENT ---------------------------------
      if (TR_IT.ge.1) then
        !todo

      endif
    
    enddo
    rhs          = CONT_TL%rhs(1)
    PDISSRTH     = sum(pdiss)
    dt           = CONT_TL%jacobi(tl_)%values(1)
    CONT_TL%rhs  = -CONT_TL%rhs
    

    ! if (simul) then
    !   CONT_TL%jacobi(psi_)%values   = 0
    ! endif
    ! if (DD_HOLE.and.simul) then
    !   CONT_TL%jacobi(phip_)%values  = 0
    ! endif
    ! if (DD_ELEC.and.simul) then
    !   CONT_TL%jacobi(phin_)%values = 0
    ! endif
    ! if (DD_ELEC2.and.simul) then
    !   CONT_TL%jacobi(phin2_)%values = 0
    ! endif

  endsubroutine

subroutine set_cont_tl_ac(omega)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! set ac values lattice continuity equation -> thermal capacitance not implemented
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real(8),intent(in) :: omega !< 2*pi*frequency
    real(8) :: dummy
    dummy = omega

endsubroutine

endmodule
