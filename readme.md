# Hdev

Hdev (heterostructure device) is a TCAD simulator focused on 1D and 2D simulations of HBTs. 
The purpose of Hdev is to allow easy technology analysis and optimization of heterostructure semiconductor devices. 

Features:

* true 1D simulations => time efficient 
* box discretization => easy to use
* semiconductor alloys => SiGe and III-V HBTs
* DC, AC and transient analysis
* augmented DD equation
* degenerate semiconductor statistics
* read and write hdf5 files
* GPL license


# 1 Installation

A Hdev binary is only provided for Unix and can be [downloaded here](https://gitlab.com/metroid120/hdev_simulator/-/jobs/artifacts/master/raw/builddir_docker/hdev?job=build:linux).
Nevertheless, it is recommended to compile the software oneself. 

Independent of your OS, if you want to use Hdev for simulation purposes, it is recommended to use the Docker file for running simulations. 
This should, in principle, also work on Windows. 

If you want to debug and develop the simulator, compilation on your host machine is recommended. 
The compilation is generally not supported on Windows at this time, however, an Hdev version for Windows can be built by cross-compilation from Unix.

## 1.1 Using the Docker File for Compilation

Due to the difficult compilation process on some distributions, Hdev comes with a fully-featured Dockerfile. 
This is handy e.g. to run Hdev on Windows. To build the image run:

    docker-compose -p hdev_simulator up --build  -d   

Hereafter you have a container that can run the Hdev simulator via the script *hdev* provided in the hdev_simulator/ directory.
The usage of the docker image is elaborated later in this document. 

If you encounter problems, make sure that you followed the official Docker installation [manual](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository) and the [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).


## 1.2 Compiling Hdev on Unix

Compilation is performed with [ meson ](https://mesonbuild.com/index.html). You need 
a meson version greater than 0.5.  Make sure you have installed:

* suitesparse library
* hdf5 Fortran libraries

To build Hdev, run the following commands from the main directory, where this "readme.md" file exists:

    cd /path/to/hdev
    meson setup builddir 
    cd builddir
    ninja

## 1.3 Compiling Hdev for Windows using Mingw-gfortran on Linux

Sometimes it can be useful to compile Hdev under Linux for Windows users, as the direct compilation under 
Windows is difficult and there is no Hdev developer working on Windows at this time. 
Make sure you have installed:

Compilation is performed with [ meson ](https://mesonbuild.com/index.html). You need 
a meson version greater than 0.5.

* mingw-gfortran: the mingw version of gfortran, it should be located here /usr/bin/x86_64-w64-mingw32-gfortran
* hdf5: mingw-w64-hdf5, specifically the hdf5 library must be built with DHDF5_BUILD_FORTRAN=ON
* mingw-w64-suitesparse
* hdf5 fortran libraries

To build Hdev, run the following commands from the main directory, where this "readme.md" file exists:

    cd /path/to/hdev
    meson setup builddir --cross-file cross_file.txt -Dstatic=true   
    cd builddir
    ninja


# 2. Installation of Python Module HdevPy

HdevPy is a suite of Python functions for generating Hdev input files, running Hdev simulations and reading back 
the simulation results. Installation is performed by invoking

    pip install -e . 

in the folder HdevPy/. If you encounter folder access permission-related issues, append the "--user" flag to the pip command.

Usage of Python version 3.8 is recommended, but higher Python versions should work, too. 

If you want to use the plotting capabilities of HdevPy with TeX as a backend, a recent Tex-live installation 
on your computer is required, including the TeX packages:

* inputenc 
* siunitx
* type1ec.sty

# 3. Running Hdev from the Command Line

If you have compiled Hdev yourself: 
For running the Hdev executable add the folder /path/to/hdev_simulator/builddir to your PATH, where the 
hdev executable resides after compilation. 
If you want to use the docker version, add the folder /path/to/hdev_simulator to your PATH, where the 
hdev bash script resides.

Then the executable is called as:

    hdev input_file.din

Input_file.din specifies a semiconductor device as explained in the Hdev manual or the examples. 
There are many test cases available in the hdev_simulator/test/ folder, try to run a few of them!

# 4. Running Hdev from Python using DMT_core and HdevPy

Hdev is most conveniently invoked using HdevPy. HdevPy facilitates calling the simulator and 
analyzing the simulation results. Make sure that a folder ~/.DMT/simulation_results exists, you need 
to manually create this folder. Here the simulations will take place. 

Several examples are in the folder examples/. 
The Hdev input file is specified using Python dictionaries and default material parameters are easily 
set using the function set_hdev_material_def. The default materials can be found in the 
file "hdevpy/materials/material_database.py".

# 5. Running the Tests

With HdevPy installed, run the script located in 

    HdevPy/scripts/run_tests.py

, which will execute several test cases and produce output for the documentation. 
Note that the folder ~/.DMT/simulaton_results must exist, manually create it if not.

# 6. The Manual 

The manual can always be [downloaded here](https://gitlab.com/metroid120/hdev_simulator/-/jobs/artifacts/master/raw/documentation/manual/hdev.pdf?job=manual).
It is always compiled by the Gitlab CI and therefore corresponds to the most recent version of Hdev.

# 7. History

Hdev has been started as a TCAD simulator by Martin Claus at TU Dresden (initially named COOS). Later it has been adopted 
by Sven Mothes who also used it for CNTFET simulations. Then, it has been applied to organic semiconductors by Markus 
Müller in his bachelor and diploma theses, where the initial 2D drift-diffusion solver has been implemented.

For his dissertation on InP HBTs, Markus Müller later created a fork of COOS aimed at HBT simulation. 
The fork has been renamed Hdev (heterostructure device) for highlighting the new focus of the software.
Hdev is now applied to SiGe and III-V HBTs by Markus Müller in the scope of his PhD thesis.



