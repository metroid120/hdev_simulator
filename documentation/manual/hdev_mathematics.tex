%!TEX root = hdev.tex 

\chapter{Simulation Algorithm }

This chapter is intended to give an overview of the simulation algorithm. 

\section{Grid Definition}
In order to solve the semiconductor equations, 
they need to be discretized on a grid. 
This is due to the fact that the computer can inherently only deal with discrete values. 
The method for discretization in Hdev is the Box Integration Method. Its advantages over other methods are:
\begin{itemize}
\item applicable to non-rectangular structures
\item interfaces can be included in a natural way compared to the finite difference method
\item simple mathematics
\item relatively easy to implement compared to finite element method
\end{itemize}
In Hdev, the (2D) discretization grid is defined according to fig. \ref{discset}.
\FloatBarrier
\begin{figure}[ht]
	\centering
    \includegraphics[width=1\textwidth]{drawio/Koordinatensystem}
	\caption{Discretization of the solution domain used for 2D device simulation.}
	\label{discset}
\end{figure}
\FloatBarrier

\section{Discretization of Differential Equations}
For each solution variable there is one differential equation. They are listed below:
\begin{itemize}
    \item The continuity equation of electrons $f_{\varphi_{\mathrm{n}}}$ with solution variable $\varphi_{\mathrm{n}}$
    \item The continuity equation of holes $f_{\varphi_{\mathrm{p}}}$ with solution variable $\varphi_{\mathrm{p}}$
    \item The Poisson equation $f_{\psi}$ with solution variable $\psi$
    \item The energy flux continuity equation $f_{T_{\mathrm{n}}}$ with solution variable $T_{\mathrm{n}}$
\end{itemize}

Only for the continuity equation of electrons the derivation of the discrete version is shown in detail. 
All other differential equations have a similar structure and are, therefore, discretized in the exact same way. 
For the discretization of the energy flux continuity equation, the reader is referred to \cite{Gnudi1988}. 
The final discretized equations can be found in the Fortran files in the "source/continuity\_equations" folder, 
therein the "set" routines. 
E.g. the discrete continuity equation of electrons is implemented in the file "cont\_elec.f90", therein the routine 
"set\_cont\_elec".

The continuity equation for electrons is 
\begin{equation}
\frac{\partial n}{\partial t}-r=\nabla \mathbf{J_{\mathrm{n}}}\,\, .
\label{cont_n}
\end{equation} 
After integration, the r.h.s becomes
\begin{equation}
\int_V \nabla \mathbf{J_{\mathrm{n}}} \mathrm{d}V=\oint_{\partial G} \mathbf{J_{\mathrm{n}}} \mathrm{d}G\,\, .
\end{equation}
Now one assumes a constant current density on the boundary between two neighbouring points and obtains
\begin{equation}
\oint_{\partial G} \mathbf{J_{\mathrm{n}}} \mathrm{d}G= J_{nx,i+\frac{1}{2},j}\Delta \tilde{y}-J_{ny,i,j-\frac{1}{2}}\Delta \tilde{x} - J_{nx,i-\frac{1}{2},j}\Delta \tilde{y}+ J_{ny,i,j+\frac{1}{2}}\Delta \tilde{x} 
\end{equation}
where $\Delta \tilde{y}=\frac{\Delta y_i + \Delta y_{i-1}}{2}$, $\Delta \tilde{x}=\frac{\Delta x_i + \Delta x_{i-1}}{2}$ and the currents are defined as visualized in figure \ref{currdef}.
\FloatBarrier
\begin{figure}[ht]
	\centering
    \includegraphics[width=0.5\textwidth]{drawio/continuity_pic.eps}
	\caption{Sketch of a point $i,j$ with its domain and the current densities considered in the discrete continuity equation of electrons at each discrete point.}
	\label{currdef}
\end{figure}
\FloatBarrier
For the time being, recombination is neglected and the carrier density inside the rectangle $i,j$ is assumed to be spatially 
constant. Then, the discrete version of the l.h.s of equation \ref{cont_n} is written as
 \begin{equation}
 \frac{\partial n}{\partial t}-r=\frac{\partial n_{i,j}}{\partial t}
 \end{equation}
Now one can write down the continuity equation in its discrete form. 
First the DC version $f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}(\varphi_{\mathrm{p}},\phi_{\mathrm{n}},\Psi)$
is written as:
\begin{equation}
f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}(\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=0= J_{nx,i+\frac{1}{2},j}\Delta \tilde{y}-J_{ny,i,j-\frac{1}{2}}\Delta \tilde{x}- J_{nx,i-\frac{1}{2},j}\Delta \tilde{y}+ J_{ny,i,j+\frac{1}{2}}\Delta \tilde{x} 
\end{equation}
It is convenient to define the transient continuity equations $f_{\varphi_{\mathrm{n}},\mathrm{TR},i,j}(\varphi_{\mathrm{p}},\phi_{\mathrm{n}},\Psi)$ 
\begin{equation}
f_{\phi_{\mathrm{n},i,j},\mathrm{TR}}(\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=0= f_{\phi_{\mathrm{n},i,j},\mathrm{DC}}(\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)-\frac{\partial n_{i,j}}{\partial t} \, ,
\end{equation}
which allows to write the equation system in a more compact way, later. 
In the transient case the equations are solved between user specified time steps, 
the time derivatives are calculated explicitly using the backward Euler method.
In the DC case all time derivatives are equal to zero, obviously the most simple case. 
Notice how the DC case is basically Kirchhoff's current law in a more general form. 
The Fortran version of the DC continuity equations of electrons is written as
\FloatBarrier
\begin{lstlisting}[language=Fortran]
        rhs              = (j_px - j_mx)*dy + (j_py - j_my)*dx 
\end{lstlisting}
\FloatBarrier

In the AC case sinusoidal varying solution variables are assumed
\begin{equation}
\Psi (t)=\Re\left[\Psi_0+\overline{\Psi}\exp(jwt)\right]
\end{equation}
\begin{equation}
\phi_{\mathrm{n/p}} (t)=\Re\left[\phi_{\mathrm{n/p},0}+\overline{\phi_{\mathrm{n/p}}}\exp(jwt)\right]
\end{equation}
and $f_\Psi,\,f_{\varphi_{\mathrm{n}},\mathrm{TR}}$ and $f_{\varphi_{\mathrm{p}},\mathrm{TR}}$ are linearized around the DC operating point. Therefore one obtains the AC version of the DD equations as:
\begin{multline}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=f_{\Psi,\mathrm{DC},i,j} (\phi_{\mathrm{n}0},\phi_{\mathrm{p0}},\Psi_0)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)\\+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\phi_{\mathrm{n}},\Psi)=f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j} (\phi_{\mathrm{n}0},\phi_{\mathrm{p}0},\Psi_0)+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)\\+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)-\frac{\partial \mathrm{n}}{\partial \psi}jw\overline{\Psi}\exp(jwt)-\frac{\partial \mathrm{n}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}\exp(jwt)
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j} (\phi_{\mathrm{p}0},\Psi_0)+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)\\+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)+\frac{\partial \mathrm{p}}{\partial \psi}jw\overline{\Psi}\exp(jwt)+\frac{\partial \mathrm{p}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}\exp(jwt)
\end{multline}
These equations can be simplified since the DC solutions are equal to zero:
\begin{multline}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\phi_{\mathrm{n}},\Psi)=\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)\\-\frac{\partial \mathrm{n}}{\partial \psi}jw\overline{\Psi}\exp(jwt)-\frac{\partial \mathrm{n}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}\exp(jwt)
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)\\+\frac{\partial \mathrm{p}}{\partial \psi}jw\overline{\Psi}\exp(jwt)+\frac{\partial \mathrm{p}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}\exp(jwt)
\end{multline}
All equations must be equal to zero, which allows to divide them by $\exp(jwt)$ and 
the final version of the discrete AC equation system is obtained:
\begin{equation}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}
\end{equation}
\begin{equation}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\phi_{\mathrm{n}},\Psi)=\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}-\frac{\partial \mathrm{n}}{\partial \psi}jw\overline{\Psi}-\frac{\partial \mathrm{n}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}
\end{equation}
\begin{equation}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}+\frac{\partial \mathrm{p}}{\partial \psi}jw\overline{\Psi}+\frac{\partial \mathrm{p}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}
\end{equation}
The derivative of the continuity equation $f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j}$ with respect to $\varphi_{\mathrm{p}}$ and the derivative of $f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j}$ with respect to $\phi_{\mathrm{n}}$ are omitted here since $r=0$ 
was assumed.


\section{Solution of the Equation System}
Now all differential equations to be solved are available in their discrete form. 
They have been written such that their value equals zero at every point, 
provided that the solution fulfils the differential equation.
For the solution of the equation system one can therefore apply Newton's method.  
In the following discussion, the energy-flux continuity equation is not taken into account, 
however its implementation is completely analogous. 

The functions $f_{\Psi},\,f_{\varphi_{\mathrm{n}}}$ and $f_{\varphi_{\mathrm{p}}}$ are defined for the DC, AC and transient case 
for every point $i,j$ inside the simulation domain. 
The three functions need to be equal to zero at each discrete point $i,j$. 
Expressions for all derivatives are easily derived and calculated using dual numbers. 
The equations form a large system of equations that is solved as discussed in the following. 

First, a brief review on solving an equation system is given. 
This review is just intended as a refresher. 
Then the difference between a successive solution and a simultaneous solution of the equations is presented. 
Afterward, the structure of the equation system for the DD system is given in matrix notation. 
Thereafter the AC, DC and transient solution cases are elaborated. 
In the end some insight into the actual Fortran implementation is given.

\subsection{Newton's Method}
One may use Newton's method to solve systems of $k$ nonlinear equations. 
First the case of one nonlinear equation that needs to be solved inside some simulation domain with $n$ discrete points is discussed.

Let $\vec{g}:\, D \subseteq \mathbb{R}^n\rightarrow \mathbb{R}^n$ be a function that is differentiable on $D$, then one may write 
the discrete function as:
\begin{equation}
\vec{g}(\vec{x})=
\left(
\begin{array}{c}
g_1(\vec{x})\\
g_2(\vec{x})\\
... \\
g_n(\vec{x})
\end{array}
\right)
\end{equation}
The Newton method is an iterative method to solve $\vec{g}(\,\vec{x}\,)=0$ based on an initial guess 
$\vec{x}^{\mathnormal{(0)}}$. 
Then subsequent iterates $\vec{x}^{\mathnormal{(1)}},\,\vec{x}^{\mathnormal{(2)}}$ that hopefully converge to 
the correct solution $\vec{x}^{(*)}$ are computed.

The idea is to approximate $\vec{g}(\,\vec{x}\,)$ near the current $\vec{x}^{\mathnormal{(i)}}$ as  
\begin{equation}
\label{approx_jacob}
\vec{g}_{\mathnormal{i}}(x)=\vec{g} ( \, \vec{x}^{(\mathnormal{i} \,)})\mathnormal{+}\mathbf{J_g}(\vec{x}^{(\mathnormal{i})})(\vec{x}\mathnormal{-}\vec{x}^{(\mathnormal{i})})
\end{equation}
and use the solution of $\vec{g}_{\mathnormal{i}}(\,\vec{x}\,)=0$ as the next  $\mathbf{x^{\mathnormal{(i+1)}}}$. The matrix $\mathbf{J_g}$ is called the Jacobian of $\vec{g}(\,\vec{x}\,)$. Its elements are defined as
\begin{equation}
\left[\mathbf{J_g}\right]_{ml}=\frac{\partial g_m(x_l)}{\partial x_l}\Big{|}_{x=x_l^i}\,\, .
\end{equation}
The solution of equation \ref{approx_jacob} can be computed by solving
\begin{equation}
\label{tosolve}
\mathbf{J_g}(\vec{x}^{(\mathnormal{i})})(\vec{x}\mathnormal{-}\vec{x}^{(\mathnormal{i})}) = 
-\vec{g}(\,\vec{x}^{(\mathnormal{i})})
\,\, .
\end{equation}
Obviously the rank of the Jacobian is given by $n$.

If more differential equations need to be solved, possible even coupled, the method can be extended. 
The case of two equations on a 2D simulation domain will be discussed here as an example. 
Note that there are several ways to perform this extension, the way presented here is just one of them.

Let $\vec{g}:\, D \subseteq \mathbb{R}^n\rightarrow I\!R^n$ be a function that is differentiable on $D$ and $\vec{e}:\, D \subseteq \mathbb{R}^m\rightarrow I\!R^m$ be another function that is differentiable on $D$:
\begin{equation}
\vec{g}(\vec{x},\vec{y})=
\left(
\begin{array}{c}
g_1(\vec{x},\vec{y})\\
g_2(\vec{x},\vec{y})\\
... \\
g_n(\vec{x},\vec{y})
\end{array}
\right)
\end{equation}
\begin{equation}
\vec{e}(\vec{x},\vec{y})=
\left(
\begin{array}{c}
e_1(\vec{x},\vec{y})\\
e_2(\vec{x},\vec{y})\\
... \\
e_m(x_m,\vec{y})
\end{array}
\right)
\end{equation}
One defines the function $\vec{G}(\vec{x},\vec{y})$  as 
\begin{equation}
\vec{G}(\vec{x},\vec{y})=
\left(
\begin{array}{c}
g_1(\vec{x},\vec{y})\\
g_2(\vec{x},\vec{y})\\
... \\
g_n(\vec{x},\vec{y})\\
e_1(\vec{x},\vec{y})\\
e_2(\vec{x},\vec{y})\\
... \\
e_m(\vec{x},\vec{y})
\end{array}
\right)\,\, .
\end{equation}
and the solution vector $\vec{\bar{X}}$ as
\begin{equation}
\vec{\bar{X}}=
\left(
\begin{array}{c}
\vec{x} \\
\vec{x} \\
... \\
\vec{x} \\
y_1 \\
y_2 \\
... \\
y_m
\end{array}
\right)=
\left(
\begin{array}{c}
\bar{x}_1 \\
\bar{x}_2 \\
... \\
\bar{x}_n \\
\bar{x}_{n+1} \\
\bar{x}_{n+2} \\
... \\
\bar{x}_{n+m}
\end{array}
\right)\,\, .
\end{equation}
which enables to write down the approximation to $\vec{G}(\bar{\vec{X}})$ at the current solution vector $\vec{X}^{\mathnormal{(i)}}$ as
\begin{equation}
\vec{G}_{\mathnormal{i}}(\vec{\bar{X}})=\vec{G}(\vec{\bar{X}}^{(\mathnormal{i})})\mathnormal{+}\mathbf{J_G}(\vec{\bar{X}}^{(\mathnormal{i})})(\vec{\bar{X}}\mathnormal{-}\vec{\bar{X}}^{(\mathnormal{i})})\,\, .
\end{equation}
The elements of the Jacobian are now given by
\begin{equation}
\left[\mathbf{J_G}\right]_{ml}=\frac{\partial G_m(\bar{x}_l)}{\partial \bar{x}_l}\Big{|}_{\bar{x}=\bar{x}_m^i,y=\bar{x}_{m+n}^i}
\end{equation}
for $l\leq n$ and
\begin{equation}
\left[\mathbf{J_G}\right]_{ml}=\frac{\partial G_m(\bar{x}_l)}{\partial \bar{x}_l}\Big{|}_{\bar{x}=\bar{x}_{m-n}^i,y=\bar{x}_{m}^i}
\end{equation}
else.
The rank of the Jacobian is $n+m$. From here the solution procedure is analogous to the case of just one equation.
\FloatBarrier
\newpage
\subsection{Simultaneous Solution versus Successive Solution}
Generally the system of equations can either be solved successively or simultaneously. The solution procedure for both cases is shown in figure \ref{fig:f1} and \ref{fig:f2}.\\
\FloatBarrier
\begin{figure}[h]
  \begin{subfigure}[h]{0.45\textwidth}
    \includegraphics[width=\textwidth]{drawio/procedure}
    \caption{Flow diagram for the successive solution procedure.}
    \label{fig:f1}
  \end{subfigure}
  \hfill
  \begin{subfigure}[h]{0.45\textwidth}
    \includegraphics[width=\textwidth]{drawio/procedure_simul.eps}
    \caption{Flow diagram for the simultaneous solution procedure.}
    \label{fig:f2}
  \end{subfigure}
  \caption{Flow diagrams of the implemented solution methods for the DD equation system.}
\end{figure}
\FloatBarrier
The simultaneous solution converges much faster, especially when a high coupling between the equations exists. 
On the other hand, the memory needed to store the sparse matrix is much higher than for the simultaneous solution 
as the rank of the Jacobian is higher. In general, on modern computers with several GBs of RAM, the higher memory requirement 
is not an issue.
\FloatBarrier
\subsection{Structure of the Jacobian for the DC-DD Equation System}
In order to solve the equations it is necessary to know as much as possible about the structure of the Jacobian. 
This allows to write the Jacobian in a sparse matrix format of choice and therefore saves valuable memory.

The DD equation system for each point $i,\,j$ reads as follows:
\begin{equation}
f_{\Psi,i,j,\mathrm{DC}}(\Psi_{i,j},\Psi_{i-1,j},\Psi_{i,j-1},\Psi_{i+1,j},\Psi_{i,j+1},\phi_{\mathrm{n},i,j},\phi_{\mathrm{p},i,j})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{p},i,j,\mathrm{DC}}}(\Psi_{i,j},\Psi_{i-1,j},\Psi_{i,j-1},\Psi_{i+1,j},\Psi_{i,j+1},\phi_{\mathrm{p},i,j},\phi_{\mathrm{p},i-1,j},\phi_{\mathrm{p},i,j-1},\phi_{\mathrm{p},i+1,j},\phi_{\mathrm{p},i,j+1},\phi_{\mathrm{n},i,j})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{n},i,j,\mathrm{DC}}}(\Psi_{i,j},\Psi_{i-1,j},\Psi_{i,j-1},\Psi_{i+1,j},\Psi_{i,j+1},\phi_{\mathrm{n},i,j},\phi_{\mathrm{n},i-1,j},\phi_{\mathrm{n},i,j-1},\phi_{\mathrm{n},i+1,j},\phi_{\mathrm{n},i,j+1},\phi_{\mathrm{p},i,j})=0\,\, .
\end{equation}
In the following these equations will be abbreviated as
\begin{equation}
f_{\Psi,i,j,\mathrm{DC}}(\Psi,\phi_{\mathrm{n}},\varphi_{\mathrm{p}})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{p},i,j,\mathrm{DC}}}(\Psi,\varphi_{\mathrm{p}},\phi_{\mathrm{n}})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{n},i,j,\mathrm{DC}}}(\Psi,\phi_{\mathrm{n}},\varphi_{\mathrm{p}})=0\,\, .
\end{equation}
The dependence of the continuity equation of electrons (holes) on $\varphi_{\mathrm{n}}$ ($\varphi_{\mathrm{p}}$) 
is due to the recombination term. 
If recombination is neglected further simplification are possible and one obtains
\begin{equation}
f_{\Psi,i,j,\mathrm{DC}}(\Psi,\phi_{\mathrm{n}},\varphi_{\mathrm{p}})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{p},i,j,\mathrm{DC}}}(\Psi,\varphi_{\mathrm{p}})=0
\end{equation}
\begin{equation}
f_{\phi_{\mathrm{n},i,j,\mathrm{DC}}}(\Psi,\phi_{\mathrm{n}})=0\,\, .
\end{equation}
Each equation only depends on the value of the solution variables at the point itself 
and the solution variables of the direct neighbours. Also the coupling of the equations is apparent.

It is necessary to choose an ordering algorithm for the points for which the equations shall be solved. 
The implemented ordering algorithm is sketched in figure \ref{ordering}. 
\FloatBarrier
\begin{figure}[h]
    \includegraphics[width=\textwidth]{drawio/ordering}
    \caption{Ordering algorithm for a $N\cdot M$ example structure.}
    \label{ordering}
\end{figure}
\FloatBarrier
Now one can sketch the general structure of the Jacobian for the successive solution in the 2D case:
\FloatBarrier
\begin{figure}[ht]
	\centering
    \includegraphics[width=0.6\textwidth]{drawio/poisson.eps}
	\caption{
    Generic structure of the Jacobian of any of the non-linear differential equations with respect to the corresponding solution variable. 
    The diagonal lines mark non-zero matrix entries. 
    For example, this could be the Poisson equation with respect to the electrostatic potential for a rectangular structure.
}
\end{figure}
\FloatBarrier
The structure originates from the fact that the non-linear equation depends only on the value of the solution variable at each point and its direct neighbours.
Notice that almost all entries are zero and therefore need not be stored in memory!

Using the results from the previous chapter one can write down explicitly the elements of the Jacobians, they are 
all written in the corresponding Fortran codes. 
For the continuity equation of electrons the Jacobian with respect to $\phi_{\mathrm{n}}$ has the entries
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i,j}=\Delta\tilde{y}[\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \phi_{n,i,j}}-\frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \phi_{n,i,j}} ]+\Delta\tilde{x}[\frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \phi_{n,i,j}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \phi_{n,i,j}} ]
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i-1,j}=\frac{\partial f_{n,i,j}}{\partial \phi_{n,i-1,j}}=-\tilde{y}\frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \phi_{n,i-1,j}}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i+1,j}=\frac{\partial f_{n,i,j}}{\partial \phi_{n,i+1,j}}=\tilde{y}\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \phi_{n,i-1,j}}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i,j+1}=\frac{\partial f_{n,i,j}}{\partial \phi_{n,i,j+1}}=\tilde{x}\frac{\partial J_{nx,i,j+\frac{1}{2}}}{\partial \phi_{n,i,j+1}}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i,j-1}=\frac{\partial f_{n,i,j}}{\partial \phi_{n,i,j-1}}=-\tilde{x}\frac{\partial J_{nx,i,j-\frac{1}{2}}}{\partial \phi_{n,i,j-1}}\,\, .
\end{equation}
Similarly with respect to the electrostatic potential one has
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i,j}=\left(\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \Psi_{i,j}} - \frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \Psi_{i,j}}\right)\Delta \tilde{y}+ \left(\frac{\partial}{\partial \Psi_{i,j}}J_{ny,i,j+\frac{1}{2}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \Psi_{i,j}}\right)\Delta \tilde{x} 
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i-1,j}=- \frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \Psi_{i-1,j}}\Delta \tilde{y}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i+1,j}=\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \Psi_{i+1,j}} \Delta \tilde{y}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i,j+1}= \frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \Psi_{i,j+1}}\Delta \tilde{x}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i,j-1}=-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \Psi_{i,j-1}}\Delta \tilde{x}\,\, .
\end{equation}
With respect to $\varphi_{\mathrm{p}}$ the Jacobian has no entries if recombination is neglected: 
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\varphi_{\mathrm{p}})]_{i,j}=0\,\, .
\end{equation}

The Jacobian of the simultaneous solution is composed of all equations' Jacobians with respect to all solution variables:
\FloatBarrier
\begin{figure}[ht]
	\centering
	\label{jacobi_succ}
  \includegraphics[width=0.8\textwidth]{drawio/simul.eps}
	\caption{
    Structure of the full Jacobian for the simultaneous solution of the DD equation system. 
  }
	\label{bandfig}
\end{figure}
\FloatBarrier
\subsection{Structure of the Jacobian for the AC-DD Equation System}
For the AC equation system one can build up on the knowledge of the DC case. 
It is assumed that the DC solution $\phi_{\mathrm{n}0},\phi_{\mathrm{p0}},\Psi_0$ is already known. 
Similarly to the DC case one must solve
\begin{multline}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=f_{\Psi,\mathrm{DC},i,j} (\phi_{\mathrm{n}0},\phi_{\mathrm{p0}},\Psi_0)\\+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)=0
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\Psi)=f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j} (\phi_{\mathrm{n}0},\Psi_0)\\+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)\\-\frac{\partial n_{i,j}}{\partial \psi}jw\overline{\Psi}\exp(jwt)-\frac{\partial n_{i,j}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}\exp(jwt)=0
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\Psi)=f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j} (\phi_{\mathrm{p}0},\Psi_0)\\+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)\\+\frac{\partial p_{i,j}}{\partial \psi}jw\overline{\Psi}\exp(jwt)+\frac{\partial p_{i,j}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}\exp(jwt)=0\,\, .
\end{multline}
The DC solutions are already equal to zero and can be eliminated:
\begin{equation}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)=0
\end{equation}
\begin{multline}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\Psi)=\\\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{n}},\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}\exp(jwt)-\frac{\partial n_{i,j}}{\partial \psi}jw\overline{\Psi}\exp(jwt)-\frac{\partial n_{i,j}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}\exp(jwt)=0
\end{multline}
\begin{multline}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\Psi)=\\\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}\exp(jwt)+\frac{\partial f_{\varphi_{\mathrm{p}},\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}\exp(jwt)+\frac{\partial p_{i,j}}{\partial \psi}jw\overline{\Psi}\exp(jwt)+\frac{\partial p_{i,j}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}\exp(jwt)=0\,\, .
\end{multline}
This allows to cancel the $\exp(jwt)$ terms:
\begin{equation}
f_{\Psi,\mathrm{AC},i,j} (\phi_{\mathrm{n}},\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}+\frac{\partial f_{\Psi,\mathrm{DC},i,j}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}=0
\end{equation}
\begin{equation}
f_{\varphi_{\mathrm{n}},\mathrm{AC},i,j} (\phi_{\mathrm{n}},\Psi)=\frac{\partial f_{\phi_{\mathrm{n},\mathrm{DC},i,j}}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\phi_{\mathrm{n},\mathrm{DC},i,j}}}{\partial \phi_{\mathrm{n}}}\overline{\phi_{\mathrm{n}}}-\frac{\partial n_{i,j}}{\partial \psi}jw\overline{\Psi}-\frac{\partial n_{i,j}}{\partial \phi_{\mathrm{n}}}jw\overline{\phi_{\mathrm{n}}}=0
\end{equation}
\begin{equation}
f_{\varphi_{\mathrm{p}},\mathrm{AC},i,j} (\varphi_{\mathrm{p}},\Psi)=\frac{\partial f_{\phi_{\mathrm{p},\mathrm{DC},i,j}}}{\partial \Psi}\overline{\Psi}+\frac{\partial f_{\phi_{\mathrm{p},\mathrm{DC},i,j}}}{\partial \varphi_{\mathrm{p}}}\overline{\varphi_{\mathrm{p}}}+\frac{\partial p_{i,j}}{\partial \psi}jw\overline{\Psi}+\frac{\partial p_{i,j}}{\partial \varphi_{\mathrm{p}}}jw\overline{\varphi_{\mathrm{p}}}=0\,\, .
\end{equation}
The resulting equations are inherently linear and thus can be solved in one single Newton iteration per frequency point. 
Almost all derivatives have already been calculated for the DC solution. 
Apart from the following elements, the Jacobians needed for solving for $\bar{\Psi}$, $\bar{\phi_{\mathrm{n}}}$ and $\bar{\varphi_{\mathrm{p}}}$ are the same as in the DC case:
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i,j}=\left(\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \Psi_{i,j}} - \frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \Psi_{i,j}}\right)\Delta \tilde{y}+ \left(\frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \Psi_{i,j}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \Psi_{i,j}}\right)\Delta \tilde{x} -\frac{\partial n_{i,j}}{\partial \Psi_{i,j}}jw
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i,j}=\left(\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \phi_{n,i,j}}-\frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \phi_{n,i,j}} \right)\Delta \tilde{y}+\left(\frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \phi_{n,i,j}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \phi_{n,i,j}} \right)\Delta \tilde{x}-\frac{\partial n_{i,j}}{\partial \phi_{\mathrm{n},i,j}}jw
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{p}}}}(\Psi)]_{i,j}=\left(\frac{\partial J_{px,i+\frac{1}{2},j}}{\partial \Psi_{i,j}} - \frac{\partial J_{px,i-\frac{1}{2},j}}{\partial \Psi_{i,j}}\right)\Delta \tilde{y}+ \left(\frac{\partial J_{py,i,j+\frac{1}{2}}}{\partial \Psi_{i,j}}-\frac{\partial J_{py,i,j-\frac{1}{2}}}{\partial \Psi_{i,j}}\right)\Delta \tilde{x} +\frac{\partial p_{i,j}}{\partial \Psi_{i,j}}jw
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{p}}}}(\varphi_{\mathrm{p}})]_{i,j}=\left(\frac{\partial J_{px,i+\frac{1}{2},j}}{\partial \phi_{p,i,j}}-\frac{\partial J_{px,i-\frac{1}{2},j}}{\partial \phi_{p,i,j}} \right)\Delta \tilde{y}+\left(\frac{\partial J_{py,i,j+\frac{1}{2}}}{\partial \phi_{p,i,j}}-\frac{\partial J_{py,i,j-\frac{1}{2}}}{\partial \phi_{p,i,j}} \right)\Delta \tilde{x}+\frac{\partial p_{i,j}}{\partial \phi_{\mathrm{p},i,j}}jw
\end{equation}
In the AC case also the displacement current has to be considered. 
Ampère's law states that
\begin{equation}
\label{amperelaw}
\oint_{\partial l}\vec{B}\mathrm{d}l=\mu_0 \int\int_{\partial \vec{A}}\vec{J} \mathrm{d}A +\mu_0 \epsilon_0 \frac{d}{dt}\int\int_{\partial \vec{A}} \vec{E} \mathrm{d}\vec{A}
\end{equation}
where $\vec{B}$ is the magnetic flux and $\vec{J}$ is a current through plane $A$ with boundary $\mathrm{d}l$ 
(given by the DD current density inside a semiconductor). 

As an example consider two points in a 2D semiconductor structure that are $\Delta \bar{x}$ away from each other. 
In the DC case the time derivative vanishes, but in the AC case it has to be considered. 
In the AC case the displacement current from equation \ref{amperelaw} becomes:
\begin{equation}
\vec{J}_{\mathrm{Displacement}}= \mu_0\epsilon_0 j w \, \exp(jwt) \vec{\bar{E}} 
\end{equation}
Here $\vec{E}(t)=\Re{\left[\vec{E}_{\mathrm{DC}}+\vec{\bar{E}}\exp(jwt)\right]}$ was used to eliminate the time derivative.

Furthermore one can directly calculate the total displacement current of a 2D contact by integrating $\vec{J}_{\mathrm{Displacement}}$ around the contact
\begin{equation}
\vec{I}_{\mathrm{Displacement,contact}}=j w \exp (jwt) \int_{\partial \mathrm{contact}} \vec{D}_{\perp}  d\vec{l}= j w \exp (jwt) Q_{\mathrm{contact}}\,\, .
\end{equation}
This integration is also implemented in Hdev for the case of 2D contacts.

\subsection{Structure of the Jacobian for the Transient Equation System}
The transient Jacobians are equal to the DC Jacobians. Only the main diagonal of the Jacobians changes, because now also $\frac{\partial n}{\partial t}$ and $\frac{\partial p}{\partial t}$ have to be considered in the continuity equations. The displacement current is obtained similar to the AC case:
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\Psi)]_{i,j}=\left(\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \Psi_{i,j}} - \frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \Psi_{i,j}}\right)\Delta \tilde{y}+\left( \frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \Psi_{i,j}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \Psi_{i,j}}\right)\Delta \tilde{x} -\frac{\partial n_{i,j}}{\partial t}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{n}}}}(\phi_{\mathrm{n}})]_{i,j}=\left(\frac{\partial J_{nx,i+\frac{1}{2},j}}{\partial \phi_{n,i,j}}-\frac{\partial J_{nx,i-\frac{1}{2},j}}{\partial \phi_{n,i,j}} \right)\Delta\tilde{y}+\left(\frac{\partial J_{ny,i,j+\frac{1}{2}}}{\partial \phi_{n,i,j}}-\frac{\partial J_{ny,i,j-\frac{1}{2}}}{\partial \phi_{n,i,j}} \right)\Delta\tilde{x}-\frac{\partial n_{i,j}}{\partial t}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{p}}}}(\Psi)]_{i,j}=\left(\frac{\partial J_{px,i+\frac{1}{2},j}}{\partial \Psi_{i,j}} - \frac{\partial J_{px,i-\frac{1}{2},j}}{\partial \Psi_{i,j}}\right)\Delta \tilde{y}+\left( \frac{\partial J_{py,i,j+\frac{1}{2}}}{\partial \Psi_{i,j}}-\frac{\partial J_{py,i,j-\frac{1}{2}}}{\partial \Psi_{i,j}}\right)\Delta \tilde{x} +\frac{\partial p_{i,j}}{\partial t}
\end{equation}
\begin{equation}
[J_{f_{\varphi_{\mathrm{p}}}}(\varphi_{\mathrm{p}})]_{i,j}=\left(\frac{\partial J_{px,i+\frac{1}{2},j}}{\partial \phi_{p,i,j}}-\frac{\partial J_{px,i-\frac{1}{2},j}}{\partial \phi_{p,i,j}} \right)\Delta\tilde{y}+\left(\frac{\partial J_{py,i,j+\frac{1}{2}}}{\partial \phi_{p,i,j}}-\frac{\partial J_{py,i,j-\frac{1}{2}}}{\partial \phi_{p,i,j}} \right)\Delta\tilde{x}+\frac{\partial p_{i,j}}{\partial t}
\end{equation}
\subsection{Solution of the Resulting Linear Equation System with Fortran}
The solution of equation \ref{tosolve} is computed using 
the open-source UMFPACK package. The general solution procedure is as follows.

The solution $\vec{x}$ of the following linear equation system is needed:
\begin{equation}
\label{gen_ex}
\mathbf{A}\vec{x}=\vec{b}
\end{equation}
Therefore one performs a factorization such that 
\begin{equation}
\mathbf{A}=\mathbf{LU}
\end{equation}
where $\mathbf{L}$ is a lower triangular matrix and $\mathbf{U}$ is an upper triangular matrix. Then one can rewrite equation \ref{gen_ex} as
\begin{equation}
\mathbf{LU}\vec{x}=\vec{b}\,\, .
\end{equation}
This allows to first solve
\begin{equation}
\label{eins}
\mathbf{L}\vec{y}=\vec{b}
\end{equation}
and then solve 
\begin{equation}
\label{zwei}
\mathbf{U}\vec{x}=\vec{y}\,\, .
\end{equation}
The solution of equation \ref{eins} and \ref{zwei} is very easy as $\mathbf{L},\,\mathbf{U}$ are triangular.
