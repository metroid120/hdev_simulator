%!TEX root = hdev.tex 
\chapter{Simulation Results}

All simulation results are stored in the file "simulation\_data.h5". It can be opened with other tools like Matlab, Python or 
HDFviewer. 
The file contains several datasets. The names of the columns in each dataset are stored as HDF attributes of the dataset. 
The HdevPy package contains routines to read-in the data directly. 
The columns contained in each dataset are described in the next section.    

\section{iv} \label{sec:_iv.elpa}
This dataset contains semiconductor device terminal characteristics calculated from DC simulations. 
The meaning of the columns is:

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
NITER & - &  Number of Iterations to reach convergence. \tabularnewline
\hline 
V\_{[con]} & V & bias voltage of contact con \tabularnewline
\hline 
I\_{[con]} & $\left(\si{\ampere\per\meter}\right)^{3-d}$ & Terminal current of contact (counted positive into contact) \tabularnewline
\hline 
I\_{[con]}|ELECTRONS & $\left(\si{\ampere\per\meter}\right)^{3-d}$ & Electron related terminal current \tabularnewline
\hline 
I\_{[con]}|HOLES & $\left(\si{\ampere\per\meter}\right)^{3-d}$ & Hole related terminal current \tabularnewline
\hline 
T\_LATTICE & K & Lattice Temperature \tabularnewline
\hline 
Q\_CHANNEL & $\left(\si{\coulomb\per\meter}\right)^{3-d}$ & mobile charge in the semiconductor  \tabularnewline
\hline 
Q\_CHANNEL|ELECTRONS & $\left(\si{\coulomb\per\meter}\right)^{3-d}$ & mobile electron charge in semiconductor  \tabularnewline
\hline 
Q\_CHANNEL|HOLES & $\left(\si{\coulomb\per\meter}\right)^{3-d}$ & mobile hhole charge in semiconductor  \tabularnewline
\hline 
Q\_{[con]} & $\left(\si{\ampere\per\meter}\right)^{3-d}$ & charge on contacts \tabularnewline
\hline 
dpsi\_norm & V & normalized difference in electrostatic potential to last iteration in newton method \tabularnewline
\hline 
drhs\_norm & $\left(\si{\ampere\per\meter}\right)^{3-d}$  & normalized rhs of continuity equation \tabularnewline
\hline 
t\_cpu& s& cpu time for calculating the bias point \tabularnewline
\hline 
\caption{Output columns of iv dataset for \textbf{elpa\_lev}$\geq$0. $d$ corresponds to the dimension of the simulation domain.}
\label{tab:iv1}
\end{longtable} 
\end{center}

\section{ac/opX} \label{sec:_ac.elpa}
This dataset contains all Y-parameters and related quantities from AC-simulations at the DC operating point with number X. 
Y Parameters between all contacts are calculated. 
The columns of this dataset are given in Tab.\,\ref{tab:ac}.

\begin{center} 
\begin{longtable}{|c|c|R{10cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning}\tabularnewline
\hline 
\hline 
FREQ & Hz & Frequency \tabularnewline
\hline 
Y\_IJ|REAL & $\si{\siemens}^{3-d}$ & Real part of $\underline{Y}_{ij}$ \tabularnewline
\hline 
Y\_IJ|IMAG & $\si{\siemens}^{3-d}$ & Imaginary part of $\underline{Y}_{ij}$ \tabularnewline
\hline 
\caption{
    Output variables stored in ac dataset for AC-simulations, written for every operating point. $d$ corresponds to the dimension of the simulation domain.
    I and J are replace with the names of the contacts. E.g. A 1D bipolar transistor has Y\_BB, Y\_CB, Y\_BC and Y\_CC. 
}
\label{tab:ac}
\end{longtable} 
\end{center}

%-----------------------------------------------------------------------------
\section{inqu/opX} \label{sec:_inqu.elpa}
This dataset contains the internal quantities of the semiconductor after 
convergence has been achieved for DC operating point X. 
It is generated if the parameter \textbf{inqu\_lev}$\geq$1. 
Depending on the activated equations in the SOLVE block (\ref{block:solve}) 
and \textbf{inqu\_lev}, the data given in Tab.\,\ref{tab:inqu1} - \ref{tab:inqu7} is stored in the inqu dataset.

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
X(Y) & m & Position along x(y)-axis \tabularnewline
\hline 
psi\_semi & $\si{\volt}$ & electrostatic potential  \tabularnewline
\hline 
eps       & - & relative permittity  \tabularnewline
\hline 
NNET & $\si{\meter}^{-3}$ & net doping concentration \tabularnewline
\hline 
semi\_id & - & internal Hdev index of semiconductor at position \tabularnewline
\hline 
BGN& $\si{\eV}$ & band gap narrowing \tabularnewline
\hline 
N\_I & $\si{\meter}^{-3}$ & intrinsic carrier density $n_{\mathrm{i}}$ \tabularnewline
\hline 
N & $\si{\meter}^{-3}$ & electron carrier density in highest conduction band \tabularnewline
\hline 
N2 & $\si{\meter}^{-3}$ & electron carrier density in highest conduction band \tabularnewline
\hline 
P & $\si{\meter}^{-3}$ & hole carrier density in highest conduction band \tabularnewline
\hline 
ACC & $\si{\meter}^{-3}$ & acceptor doping concentration \tabularnewline
\hline 
DON & $\si{\meter}^{-3}$ & donor doping concentration \tabularnewline
\hline 
MOL & - & grading \tabularnewline
\hline 
MOL2 & - & possible secondary grading (e.g. Carbon for a SiGeC HBT) \tabularnewline
\hline 
STRAIN & - & strain, see \ref{block:strain} \tabularnewline
\hline 
J|X(Y)DIR & $\si{\ampere}^{3-d}$ & current density in x-direction \tabularnewline
\hline 
J|N|X(Y)DIR & $\si{\ampere}^{3-d}$ & electron related current density in x-direction \tabularnewline
\hline 
J|N2|X(Y)DIR & $\si{\ampere}^{3-d}$ & second electron population related current density in x-direction \tabularnewline
\hline 
J|P|X(Y)DIR & $\si{\ampere}^{3-d}$ & hole related current density in x-direction \tabularnewline
\hline 
f\_x(y) & $\si{\volt\per\meter}$ & electrostatic field in x(y)-direction \tabularnewline
\hline 
rho & $\si{\coulomb\per\cubic\meter}$ & charge density \tabularnewline
\hline 
j\_tun\_x & $\si{\ampere}^{3-d}$ & tunneling current density in x-direction \tabularnewline
\hline 
gen\_n(p)\_tun & $\si{\per\cubic\meter\per\second}$ & electron (hole) related tunneling recombination rate \tabularnewline
\hline 
rec & $\si{\per\cubic\meter\per\second}$ & recombination rate  \tabularnewline
\hline 
interv & $\si{\per\cubic\meter\per\second}$ & intervalley recombination rate \tabularnewline
\hline 
TEMP & $\si{\kelvin}$ & lattice temperature \tabularnewline
\hline 
V|N|XDIR & $\si{\meter\per\second}$ & electron velocity in x-direction  \tabularnewline
\hline 
MU|N|X(Y)DIR & $\si{\square\meter\per\volt\second}$ & electron population mobility in x(y)-direction  \tabularnewline
\hline 
MU|N2|X(Y)DIR & $\si{\square\meter\per\volt\second}$ & second electron population mobility in x(y)-direction \tabularnewline
\hline 
MU|P|X(Y)DIR & $\si{\square\meter\per\volt\second}$ & hole mobility in x(y)-direction \tabularnewline
\hline 
V|N|XDIR & $\si{\meter\per\second}$ & electron velocity in x-direction \tabularnewline
\hline 
\caption{Quantities stored in inqu dataset independent of activated equations for \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu1}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
E\_C & $\si{\eV}$ & conduction band edge \tabularnewline
\hline 
EF|ELECTRONS & $\si{\eV}$ & quasi-Fermi level of electrons \tabularnewline
\hline 
PHI|N & $\si{\volt}$ & quasi-Fermi potential level of electrons \tabularnewline
\hline 
GPHI|N & $\si{\volt\per\meter}$ & gradient of quasi-Fermi potential in x-direction \tabularnewline
\hline 
n\_eff & $\si{\per\cubic\meter}$ & conduction band effective density of states \tabularnewline
\hline 
gn & - & degeneracy factor \tabularnewline
\hline 
rhs\_n & $\si{\per\meter\per\second}$ & right hand side of electron continuity equation  \tabularnewline
\hline 
U\_N & $\si{\eV}$ & band potential of conduction band \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if elec=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu2}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
E\_V & $\si{\eV}$ & valence band edge \tabularnewline
\hline 
EF|HOLES & $\si{\eV}$ & quasi-Fermi level of holes \tabularnewline
\hline 
PHI|P & $\si{\volt}$ & quasi-Fermi potential level of holes \tabularnewline
\hline 
GPHI|P & $\si{\volt\per\meter}$ & gradient of hole quasi-Fermi potential in x-direction \tabularnewline
\hline 
p\_eff & $\si{\per\cubic\meter}$ & valence band effective density of states \tabularnewline
\hline 
rhs\_p & $\si{\per\meter\per\second}$ & right hand side of hole continuity equation  \tabularnewline
\hline 
U\_P & $\si{\eV}$ & band potential of valence band \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if hole=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu3}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
E\_C2 & $\si{\eV}$ & second conduction band edge \tabularnewline
\hline 
EF|ELECTRONS2 & $\si{\eV}$ & second quasi-Fermi level of electrons \tabularnewline
\hline 
PHI|N2 & $\si{\volt}$ & second quasi-Fermi potential level of electrons \tabularnewline
\hline 
GPHI|N2 & $\si{\volt\per\meter}$ & gradient of second quasi-Fermi potential in x-direction \tabularnewline
\hline 
rhs\_n2 & $\si{\per\meter\per\second}$ & right hand side of second electron continuity equation  \tabularnewline
\hline 
U\_N2 & $\si{\eV}$ & band potential of second conduction band \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if elec2=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu4}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
L\_N & $\si{\volt}$ & bohm potential \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if bohm\_n=1 and elec=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu5}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
NL & - & non local pre-factor for driving force \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if non\_local=1 and elec=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu6}
\end{longtable} 
\end{center}

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
TN & $\si{\kelvin}$ & electron temperature \tabularnewline
\hline 
H & $\si{\watt\per\cubic\meter}$ & joule heat-density \tabularnewline
\hline 
\caption{Additional quantities stored in inqu dataset if t\_n=1 and elec=1 in the SOLVE block and \textbf{inqu\_lev}$\geq$1}. 
\label{tab:inqu7}
\end{longtable} 
\end{center}


\section{acinqu/opX} \label{sec:_acinqu.elpa}

This dataset contains internal quantities of AC simulations. 
It is generated for every frequency at operating point $X$ 
if the parameter \textbf{inqu\_lev}$\geq$0.

\begin{center} 
\begin{longtable}{|c|c|R{6cm}|}
\hline 
\textbf{Parameter name} & \textbf{unit} & \textbf{meaning} \tabularnewline
\hline 
\hline 
X & $\si{\meter}$ & x-coordinate of point \tabularnewline
\hline 
Y & $\si{\meter}$ & y-coordinate of point \tabularnewline
\hline 
re\_qfn & $\si{\eV}$ & real part of complex quasi-Fermi potential of electrons \tabularnewline
\hline 
im\_qfn & $\si{\eV}$ & imaginary part of complex quasi-Fermi potential of electrons \tabularnewline
\hline 
re\_qfp & $\si{\eV}$ & real part of complex quasi-Fermi potential of holes \tabularnewline
\hline 
im\_qfp & $\si{\eV}$ & imaginary part of complex quasi-Fermi potential of holes \tabularnewline
\hline 
re\_qfn2 & $\si{\eV}$ & real part of complex quasi-Fermi potential of electrons for second electron band \tabularnewline
\hline 
im\_qfn2 & $\si{\eV}$ & imaginary part of complex quasi-Fermi potential of electrons for second electron band \tabularnewline
\hline 
re\_psi & $\si{\eV}$ & real part of complex electrostatic potential \tabularnewline
\hline 
im\_psi & $\si{\eV}$ & imaginary part of complex electrostatic potential \tabularnewline
\hline 
re\_curr\_x & $\si{\ampere\per\square\meter}$ & real part of complex current density in x-direction \tabularnewline
\hline 
im\_curr\_x & $\si{\ampere\per\square\meter}$ & imaginary part of complex current density in x-direction \tabularnewline
\hline 
re\_dn\_x & $\si{\per\cubic\meter}$ & real part of complex electron density \tabularnewline
\hline 
im\_dn\_x & $\si{\per\cubic\meter}$ & imaginary part of complex hole density \tabularnewline
\hline 
re\_dn2\_x & $\si{\per\cubic\meter}$ & real part of complex electron density in second conduction band \tabularnewline
\hline 
im\_dn2\_x & $\si{\per\cubic\meter}$ & imaginary part of complex electron density in second conduction band \tabularnewline
\hline 
re\_dp\_x & $\si{\per\cubic\meter}$ & real part of complex hole density \tabularnewline
\hline 
im\_dp\_x & $\si{\per\cubic\meter}$ & imaginary part of complex hole density \tabularnewline
\hline 
\caption{Additional quantities stored in acinqu dataset if \textbf{inqu\_lev}$\geq$0.}
\label{tab:acinqu}
\end{longtable} 
\end{center}
