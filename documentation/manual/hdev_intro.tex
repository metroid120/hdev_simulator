%!TEX root = hdev.tex 

\chapter{About Hdev}

Hdev is a numerical device simulator that can be used to simulate 1D and 2D semiconductor structures. 
Originally, it was developed by Dr.-Ing. Sven Mothes for CNTFET research. 
The current version is being developed and applied by Dipl.-Ing. Markus Mueller to SiGe and III-V HBTs. 

Hdev is intended to be a fast and versatile engineering tool for 1D analysis and optimization of HBTs. 
It implements an augmented Drift-Diffusion (aDD) transport equation and Energy-Transport (ET) equation solver, sometimes referred to as 
Hydro-Dynamic (HD) transport equation. 
The DD transport equation is an ideal stepping stone between the more rigorous BTE and compact models, 
suitable to capture the physics of real fabricated devices. 
The ET transport equation improves upon the DD transport with improved carrier heating modeling, 
at the cost of computational effort and additional material parameters.

Highly predictive simulations of nanometer-scale HBTs are not the purpose of Hdev. 
For such simulations a more rigorous transport formalism, such as the BTE, is absolutely necessary. 
Numerical device simulators that are suitable for this purpose are SPRING from RWTH Aachen or ViennaSHE. 

A console program like Hdev is not meant to be used on its own but be controlled by another program. 
HdevPy, a Python package for running and evaluating simulations with Hdev is provided alongside the software.
Default material parameters for most semiconductors are also provided.

Hdev is provided through Gitlab.com, where a Readme.MD file can be found. It explains installation 
and basic usage. 

\section{Features}
Implemented:
\begin{itemize}
\item 1D/2D - Drift-Diffusion Equation (aDDE) - DC, AC 
\item 1D - Energy-Transport Equation (ET) for electrons - DC, AC 
\item all equations self consistently coupled to the Poisson equation
\item full simultaneous solution of the equation system
\item different band-structure models
\item Boltzmann and Fermi-Dirac distribution functions
\item heterostructures of both group IV and III-V semiconductors
\item several mobility and recombination models
\item self-consistent solution of an additional thermal impedance network 
\item output of simulation data in HDF5 format
\end{itemize}

Features that are in development:
\begin{itemize}
    \item tunneling based on WKB approximation (implemented, needs testing)
    \item cryogenic simulation capability (implemented, needs testing)
    \item transient simulations are broken at the moment and require to be fixed
\end{itemize}

\section{General Simulation Procedure}
For every simulation run the following general steps need to be taken:
\begin{itemize}
\item generate an input file with all relevant simulation parameters 
\item start Hdev with the input file as the only argument
\end{itemize}
Hdev solves the specified equations for all required operating points and prints verbose information to the terminal. 
An HDF5 output file is generated that contains the simulation results. 

\subsection{Input File}
The input file is a plain ASCII file that defines the simulation. 
The information is passed with different blocks. 
Each block has several parameters. In the following table all blocks and their meaning are listed. 
Some of the blocks can occur multiple times in the input file.

\begin{center}
\begin{longtable}{|l|L{1.5cm}|L{9cm}|}
\hline
\textbf{Parameter block name} & \textbf{Max. number} & \textbf{Meaning}\tabularnewline
\hline 
\hline 
\&REGION\_INFO & 1 & General structure information \\
\hline  
\&REGION\_DEF & 100 & Regional box definition \\
\hline 
\hline 
\hline
\&RANGE\_GRID & 100 & Equidistant mesh definition  \\
\hline
\&AREA\_GRID & 100 & Non-equidistant mesh definition \\
\hline
\hline 
\hline
\&DOPING & 100 & Define doping profile\\
\hline 
\&GRADING & 100 & Define grading profile\\
\hline 
\&OXIDE & 100 & Model parameters of oxides \\
\hline
\hline
\hline
\&SEMI & 100 & Model parameters of semiconductor regions \\
\hline 
\&BAND\_DEF & 100 & Band structure definition\\
\hline 
\&MOB\_DEF & 100 & Mobility model parameters \\
\hline 
\&TAU\_E & 100 & Energy relaxation time model parameters \\
\hline 
\&RECOMB\_DEF & 100 & Parameters for recombination model\\
\hline
\hline 
\&CONTACT & 100 & Contact model parameters \\
\hline
\&SUPPLY & 100 & Supply contact model parameters \\
\hline
\&THERMIONIC\_EMISSION & 100 & Thermionic emission boundary model definition \\
\hline
\&BIAS\_DEF & 1 & General parameter for bias definition \\
\hline 
\&BIAS\_INFO & 100 & DC bias values \\
\hline 
\&TR\_INFO & 100 & Transient (TR) bias values \\
\hline
\&AC\_INFO & 1 & AC simulation \\
\hline
\hline 
\hline 
\&SOLVE & 1 & Solver general parameters \\
\hline 
\&CONT\_ELEC & 1 & parameters for continuity equation of electrons \\
\hline 
\&CONT\_HOLE & 1 & parameters for continuity equation of holes \\
\hline 
\&CONT\_ELEC2 & 1 & parameters for second continuity equation of electrons \\
\hline 
\&CONT\_TL & 1 & parameters for thermal impedance network \\
\hline 
\&CONT\_TN & 1 & parameters for the energy transport equation \\
\hline
\&POISSON & 1 & parameters of poisson equation \\
\hline 
\&TUNNEL & 1 & Parameters for tunneling model \\
\hline
\hline
\hline 
\&OUTPUT & 1 & Parameter for output files \\
\hline 
\end{longtable}
\end{center}


\subsection{Input File Block Notation}
In the Hdev input file, each block is written this way:
\begin{lstlisting}
&BLOCK_1 parameter_a=value parameter_b=value parameter='string' / comments...
&BLOCK_2 parameter_c=value /
&BLOCK_3 parameter_d=value parameter_e=value / comments...
&BLOCK_4 parameter_f=value1 /
&BLOCK_4 parameter_f=value2 /
*this is a comment
...
\end{lstlisting}
Real example:
\begin{lstlisting}
&RANGE_GRID disc_dir='x' disc_set='pnts' intv_pnts=0 1.06e-07  n_pnts=21/
\end{lstlisting}

\section{Output Files} \label{sec:z-file}
Hdev creates a HDF5 file to store its output data. Such files are extremely efficient for large amounts of data and the format 
is well established in the scientific community. 
You can find out more on this format 
\href{https://www.hdfgroup.org/solutions/hdf5/}{here}. 