include "../GLE/template/shape.gle"
include "../GLE/template/griddef.gle"
include "../GLE/template/tpl_paper_size.gle"

set_fig_size_abs 4.1 4 1 ! w h  ratio

include "../GLE/template/tpl_paper.gle"
include "../GLE/template/cfaedcolors.gle"
include "../GLE/template/grey.gle"

include "../GLE/template/cntstrain.gle"


x0=0.7
y0=2.3

lds=0.5
hds=0.6
rcnt=0.04
lch=2.1
lg=0.9
hg=0.6
lsp=0.6
tox=0.4
hupp = 1.1
hlow = 1.4
hsi = 0.4




! -------------> background oxide 
gsave
xlen = lch+2*lds
ylen = hupp+hlow
!!! Oxide box
amove x0-lds y0-hlow
set lwidth 0.02
box xlen ylen nobox fill cfaedgreen1$
amove x0+lsp/2 y0-0.2
set just lc
set hei 0.8*text_height
set color white
tex "high-$\kappa$"
grestore

amove x0+lsp/2 y0+hupp-0.2
set hei 0.8*text_height
@clabel "SiO2" lc 0.0 0 white

! -------------> gate oxide
gsave
xlen = lch+2*lds
ylen = -tox
amove x0-lds y0
set lwidth 0.02
box xlen ylen nobox fill cfaedorange$
rmove xlen/2-0.7 ylen/2


! ----------> source box
gsave
xlen = lds
ylen = hds
!!! metal contact Source box
amove x0-lds y0
set lwidth 0.02
box xlen ylen nobox fill cfaedblue1$
rmove xlen/2 ylen/2
set just cc
set hei 0.8*text_height
set color white
tex "s"
grestore


! -----------> CNT box
gsave
xlen = lch
ylen = rcnt
!!! CNT box
amove x0 y0
set lwidth 0.02
box xlen ylen nobox fill black
rmove xlen/2+0.4 ylen/2+0.25
set just cc
set color black
set hei 0.8*text_height
tex "CNT"
grestore
set arrowsize 0.15
amove x0+lch/2 y0+0.25
aline x0+lch/2-0.2 y0+0.03 arrow end


! -----------> DRAIN box
gsave
xlen = lds
ylen = hds
!!! metal contact Drain box
amove x0+lch y0
set lwidth 0.02
box xlen ylen nobox fill cfaedblue1$
rmove xlen/2 ylen/2
set just cc
set hei 0.8*text_height
set color white
tex "d"
grestore

! -------------> GATE box
gsave
xlen = lg
ylen = -hg
!!! metal contact Gate box
amove x0+lsp y0-tox
set lwidth 0.02
box xlen ylen nobox fill cfaedblue1$
rmove xlen/2 ylen/2
set just cc
set hei 0.8*text_height
set color white
tex "gate"
grestore





! -------------------- labels --------------------
gsave
set color black
set lstyle 42
set lwidth 0.01
set hei 0.6*text_height
set arrowsize 0.1

! --------------lg ---------------
amove x0+lsp y0-tox-hg-0.2
aline x0+lsp y0-tox-hg+0.1
amove x0+lg+lsp y0-tox-hg-0.2
aline x0+lg+lsp y0-tox-hg+0.1

set lstyle 1
amove x0+lsp y0-tox-hg-0.1
@my_arrow_v lg black
@my_arrow_v -lg black
rmove lg/2 0
@clabel "$l_{\rm g}$" tc 0.0 -0.05 black


! --------------lsp ---------------
set lstyle 42
amove x0 y0+0.1
aline x0 y0-tox-hg-0.2
amove x0 y0-tox-hg-0.1
@my_arrow_v lsp black
@my_arrow_v -lsp black
rmove lsp/2 0
@clabel "$l_{\rm sp}$" tc 0.0 -0.05 black

! --------------lsp ---------------
amove x0+lch y0+0.1
aline x0+lch y0-tox-hg-0.2
amove x0+lch-lsp y0-tox-hg-0.1
@my_arrow_v lsp black
@my_arrow_v -lsp black
rmove lsp/2 0
@clabel "$l_{\rm sp}$" tc 0.0 -0.05 black


! --------------lch ---------------
set lstyle 1
amove x0 y0+hupp+0.1
@my_arrow_v lch black
@my_arrow_v -lch black
rmove lch/2 0
@clabel "$l_{\rm ch}$" bc 0.0 0.05 black
set lstyle 42
set lstyle 42
! --------------lcon---------------
amove x0-lds y0+hds-0.1
aline x0-lds y0+hupp+0.2
amove x0 y0+hds-0.1
aline x0 y0+hupp+0.2

amove x0-lds y0+hupp+0.1
@my_arrow_v lds black
@my_arrow_v -lds black
rmove lds/2 0
@clabel "$l_{\rm con}$" bc 0.0 0.05 black

! --------------lcon---------------
amove x0+lch+lds y0+hds-0.1
aline x0+lch+lds y0+hupp+0.2
amove x0+lch     y0+hds-0.1
aline x0+lch     y0+hupp+0.2

amove x0+lch y0+hupp+0.1
@my_arrow_v lds black
@my_arrow_v -lds black
rmove lds/2 0
@clabel "$l_{\rm con}$" bc 0.0 0.05 black


! --------------hg ---------------
amove x0+lsp+lg-0.1 y0-tox
aline x0+lch+lds+0.2 y0-tox
amove x0+lsp+lg-0.1 y0-tox-hg
aline x0+lch+lds+0.2 y0-tox-hg

amove x0+lch+lds+0.1 y0-tox-hg
@my_arrow_h -hg black
@my_arrow_h hg black
rmove 0 hg/2
@clabel "$h_{\rm g}$" lc 0.1 0 black

! -------------- tox ---------------
amove x0+lch+lds-0.1 y0
aline x0+lch+lds+0.2 y0
amove x0+lch+lds+0.1 y0-tox
@my_arrow_h -tox black
@my_arrow_h tox black
rmove 0 tox/2
@clabel "$t_{\rm ox}$" lc 0.1 0 black

! -------------- rcon ---------------
amove x0+lch+lds-0.1 y0+hds
aline x0+lch+lds+0.2 y0+hds
amove x0+lch+lds+0.1 y0
@my_arrow_h -hds black
@my_arrow_h hds black
rmove 0 hds/2
@clabel "$h_{\rm con}$" lc 0.1 0 black


!amove 0 0 
!grid fw fh 
!minorgrid fw fh
