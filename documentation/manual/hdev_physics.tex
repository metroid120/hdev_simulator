%!TEX root = hdev.tex 

\chapter{Physics }

This chapter is intended to give an overview of the physical equations implemented in Hdev.  
Details and simulator input parameters are explained in Chap. \ref{chap:input}.

The differential equations that are solved are activated in the SOLVE block \ref{block:solve}. 
Solution control parameters for each of the differential equations are set in the blocks described in \ref{block:cont}. 

\section{Semiconductor Differential Equations}
Depending on the user input, Hdev can simultaneously solve several differential equations that are needed to model carrier 
transport in semiconductors. 
All equations are coupled self-consistently, and all derivatives are calculated analytically using dual numbers.

\subsection{Carrier Density Continuity Equations}
The particle current continuity equation of electrons and holes reads 
\begin{equation}\label{equ:cont_elec}
\frac{\partial n}{\partial t}=\frac{1}{\mathrm{q}}\nabla \vec{J}_{\mathrm{n}}-r
\end{equation}
and
\begin{equation}\label{equ:cont_hole}
\frac{\partial p}{\partial t}=-\frac{1}{\mathrm{q}}\nabla \vec{J}_{\mathrm{p}}+r\, .
\end{equation}
Here $n$ is the electron density, $p$ is the hole density, $\mathrm{q}$ is the elementary charge, $J_{\mathrm{n}}$ is the 
electron current density and $J_{\mathrm{p}}$ is the hole current density. 
The term $r$ couples the continuity equations and accounts for 
recombination (see \ref{block:recombination}) and 
tunneling (see \ref{block:tunnel}). 
For modeling III-V semiconductors, another continuity equation can be activated 
\begin{equation}\label{equ:cont_elec2}
\frac{\partial n_2}{\partial t}=\frac{1}{\mathrm{q}}\nabla \vec{J}_{\mathrm{n2}}-\left( \frac{\mathrm{d}n_2}{\mathrm{d}t}  \right)_{\mathrm{coll}} \, .
\end{equation}
This equation can be used to model the current in an energetically higher electron band. 
The last term models the intervalley transfer and is added with opposite sign to the r.h.s of \eqref{equ:cont_elec} \cite{Muller}.

The electron and hole particle current densities are given as
\begin{equation}\label{equ:transport_elec}
\vec{J}_{\mathrm{n}}=-\mathrm{q}\mu_{\mathrm{n}}n \left( \nabla \phi_{\mathrm{n}} + P_{\mathrm{n}} \nabla T_{\mathrm{n}} \right)
\end{equation}
and
\begin{equation}
\vec{J}_{\mathrm{p}}=-\mathrm{q}\mu_{\mathrm{p}}p\nabla \varphi_{\mathrm{p}} \, .
\end{equation}
The electron/hole current is related to the gradient of the quasi-Fermi potential $\phi_{\mathrm{n/p}}$ and the mobility $\mu_{\mathrm{n/p}}$.
The mobilities are defined using MOB\_DEF blocks \ref{block:mobility}. 
The electron current density \eqref{equ:transport_elec} depends also on the gradient of the electron temperature $T_{\mathrm{n}}$ and 
the Seebeck coefficient $P_{\mathrm{n}}$. 
The latter is calculated as defined in \cite{Kantner2019}. No further user input is required.



\subsection{Energy Flux Continuity Equations}
The modeling of hot carriers is mainly relevant for electrons in high-speed SiGe HBTs. 
Therefore, the following energy flux continuity equation
\begin{equation}\label{equ:cont_energy}
    \nabla \vec{S}_{\mathrm{n}} = 
    \underbrace{ - \nabla \varphi_{\mathrm{n}} \vec{J}_{\mathrm{n}} }_{\mathrm{Joule-Heat}}
    - \frac{3}{2}k_{\mathrm{B}} n  \frac{T_{\mathrm{n}}-T_{\mathrm{L}}}{\tau_{\mathrm{e}}} \, .
\end{equation}
is implemented \cite{Wachutka1990}.
This continuity equations relates the energy flux density $\vec{S_{\mathrm{n}}}$ to the Joule heat and 
the energy relaxation time $\tau_{\mathrm{e}}$. The energy relaxation time is defined in block TAU\_E \ref{block:tau_e}.  
Equation \eqref{equ:cont_energy} neglects heating due to recombination and transient effects.

The energy flux current of electrons is given as
\begin{equation}
\vec{S}_{\mathrm{n}}=
-\kappa_{\mathrm{n}}\nabla T_{\mathrm{n}} 
- \frac{5}{2} \frac{k_{\mathrm{B}}T_{\mathrm{n}}}{\mathrm{q}} \vec{J}_{\mathrm{n}}
\, .
\end{equation}
The thermal conductivity is assumed to obey a generalized Wiedemann-Franz law
\begin{equation}
    \kappa_{\mathrm{n}} = 
    \left( \frac{5}{2} + c_{\mathrm{n}} \right) 
    \frac{k_{\mathrm{B}}^2}{\mathrm{q}} T_{\mathrm{n}} \mu_{\mathrm{n}} n \, ,
\end{equation}
however the factor $c_{\mathrm{n}}$ is assumed to be equal to zero in Hdev.

\subsection{Poisson Equation}
The Poisson equation
\begin{equation}
\nabla(\varepsilon\nabla \Psi)=-\mathrm{q}(p+N_{\mathrm{D}}-n-N_{\mathrm{A}}) 
\label{Poisson}
\end{equation}
couples the electrostatic potential $\Psi$, 
the permittivity $\varepsilon=\varepsilon_0\varepsilon_{\mathrm{r}}$ 
and the charge densities in the semiconductor. 
The ionized donor and acceptor doping density $N_{\mathrm{D}}$ and $N_{\mathrm{A}}$ are defined by the user in \ref{block:doping}.

\subsection{Temperature and Self-Heating}
Self-heating due to the dissipated power in the simulation domain can be modeled by a 
single thermal resistance $R_{\mathrm{th}}$.
The isothermal increase of the lattice temperature within the simulation domain is expressed as
\begin{equation}
    T_{\mathrm{L}} = T + R_{\mathrm{th}} P_{\mathrm{diss}}\, ,
\end{equation}
where $T$ is the ambient temperature defined by the "T" contact. 
The "T" contact is an artificial contact that has no location in the simulation domain. 
Its voltage equals the ambient temperature of the simulation.
The dissipated power is computed as
\begin{equation}
    P_{\mathrm{diss}} = \int \left( J_{\mathrm{n}} \nabla \phi_{\mathrm{n}} + J_{\mathrm{p}} \nabla \varphi_{\mathrm{p}} \right) dx \, .
\end{equation}
The model can be used as described in block \ref{block:solve}. 

If the energy flux continuity equation \eqref{equ:cont_energy} is not activated, it is assumed that 
\begin{equation}
    T_{\mathrm{n}} = T_{\mathrm{L}} \, .
\end{equation}
If the self-heating model is not activated it is assumed that
\begin{equation}
    T_{\mathrm{L}} = T \, .
\end{equation}

\subsection{Density Gradient Model}
A first order correction to include quantum effects within the DD framework is to introduce two new 
potentials \cite{Wettstein2002}
\begin{equation} \label{equ:bohm_n}
    \Lambda_{\mathrm{n}} = - \frac{\gamma\hbar^2}{6qm^*}\frac{\nabla^2\sqrt{n} }{\sqrt{n}}
\end{equation}
and
\begin{equation} \label{equ:bohm_p}
    \Lambda_{\mathrm{p}} = + \frac{\gamma\hbar^2}{6qm^*}\frac{\nabla^2\sqrt{p} }{\sqrt{p}} \, .
\end{equation}
These potentials are related to the gradients of the carrier densities, hence this model is called the density gradient model. 
$\Lambda_{\mathrm{n(p)}}$ shift $E_{\mathrm{C(V)}}$ 
and has been shown to provide excellent agreement with solutions of quantum simulation approaches \cite{Wettstein2002}.

One possibility to incorporate 
\eqref{equ:bohm_n}-\eqref{equ:bohm_p} into a DD solver is to add the Bohm potentials to the band-edges by replacing
\begin{equation}
    E_{\mathrm{C}} \rightarrow E_{\mathrm{C}} - \Lambda_{\mathrm{n}}
\end{equation}
and
\begin{equation}
    E_{\mathrm{V}} \rightarrow E_{\mathrm{V}} - \Lambda_{\mathrm{p}} \, .
\end{equation}
However, this approach introduces density gradients of fourth order into the equation system. 
Such derivatives are both difficult to implement and may render the equation system numerically 
unstable \cite{Ancona2002}. 

Instead, $\Lambda_{\mathrm{n(p)}}$ are introduced as new solution variables of the DD equation system. 
These new solution variables are subject to the differential equations \eqref{equ:bohm_n} and \eqref{equ:bohm_p}, 
which are solved simultaneously with all other differential equations.


\section{Semiconductor Material Properties}
The properties of the band-structure are physically divided into
\begin{itemize}
    \item valley-independent properties, which are defined in the SEMI block, see \ref{block:semi}.
    \item valley-dependent properties, which are defined in the BAND\_DEF blocks for each valley, see \ref{block:band}.
\end{itemize}
This section describes the material properties that are set using these blocks. 
The physical models described in the following are implemented in the file "heterostructure.f90".

\subsection{General Approach}
To calculate material properties of an alloy $\mathrm{A}_{x}\mathrm{B}_{x-1}$ one needs to define the properties of its 
constituents A and B and the spatially dependent grading $x$:
\begin{itemize}
\item $x=1$ implies that the alloy is a pure semiconductor of material B 
\item $x=0$ implies that the alloy is a pure semiconductor of material A 
\end{itemize}
The constituents of a semiconductor are defined with SEMI blocks, just as for the alloy itself, see \ref{block:semi}. 

\subsection{Relative Permittivity}
The relative permittivity is a property that 
is needed, amongst other things, for the solution of the Poisson equation.
In the case of a pure semiconductor $\varepsilon_{\mathrm{r}}$ is a scalar. 
In the case of an alloy with constituents A and B the permittivity alloy's permittivity is given as
\begin{equation}
    \varepsilon_{\mathrm{r}} = \left( 1 - x \right) \varepsilon_{\mathrm{r,A}} + x \varepsilon_{\mathrm{r,B}} - x \left( 1-x \right) C_{\varepsilon}\, ,
\end{equation}
where $C_{\varepsilon}$ is an alloy specific bowing parameter.
The relative permittivity is defined in the SEMI block as described in \ref{block:semi}.

% \subsection{Thermal Conductivity}
% For a pure semiconductor the thermal conductivity $\kappa$ of a semiconductor is a scalar. 
% In the case of an alloy, the thermal conductivity is calculated as
% \begin{equation}
%     \kappa = \left( 1 - x \right) \kappa_{\mathrm{A}} + x \kappa_{\mathrm{B}} - x \left( 1-x \right) C_{\kappa}\, ,
% \end{equation}
% where $C_{\kappa}$ is an alloy specific bowing parameter.


% The thermal conductivity is defined in the SEMI block as described in \ref{block:semi}.


\subsection{Bandgap}
The bandgap of a semiconductor is defined as
\begin{equation}
    E_{\mathrm{G}} = \min_{j,k} \left\{  E_{\mathrm{C},j} - E_{\mathrm{V},k} \right\} \, ,
\end{equation}
where the index $j$ iterates over all conduction band minima and $k$ iterates over all valence band minima. 
For an alloy, the band edge energies $E_{\mathrm{0}}$ (equal to $E_{\mathrm{C}}$ for an electron band and equal to $E_{\mathrm{V}}$ for a hole band) 
depends on its constituents as
\begin{equation}
    E_{\mathrm{0,AB}} = E_{\mathrm{0,A}} \left(1-x\right) +  E_{\mathrm{0,B}} x + E_{\mathrm{0,bow}} \left(1-x\right)x\, .
\end{equation}
Here $E_{\mathrm{0,bow}}$ is a bowing parameter. These values are defined in the BAND\_DEF blocks \ref{block:band}.

At high impurity concentrations additional states beneath the band-edges need to be taken into account.  
These states cause an effective shift of the band edges, 
modeled within the rigid band approximation by $\Delta E_{\mathrm{bgn}}>0$, 
a function of the impurity concentration $N_{\mathrm{I}}$.
The resulting bandgap is thus
\begin{equation}
E_{\mathrm{G}} \left(  N_{\mathrm{I}} \right) = E_{\mathrm{G}} - \Delta E_{\mathrm{bgn}}\left( N_{\mathrm{I}} \right)\, .
\end{equation}
The $\Delta E_{\mathrm{bgn}}$ model and its parameters are discussed in Sec. \ref{block:semi}.

\subsection{Carrier Densities}
To electron density within each valley of a semiconductor is given as
\begin{equation}
    n = \int g \left( E \right) f_{\mathrm{n}} dE\, .
\end{equation}
The density-of-states $g$ and the distribution function need to be defined by the user in the BAND\_DEF block 
for every valley \ref{block:band}.
For parabolic bands the DOS may be written as 
\begin{equation}
    g \left( E' \right) = 
    \begin{cases}
    \frac{Z}{2\pi^2} \left( \frac{2m_{\mathrm{d}}}{\hbar^2} \right)^{3/2} 
    \sqrt{E' }   & \text{for } E'>0 \\ 
    0 & \text{else.} \\
    \end{cases} \, , 
\end{equation}
where 
\begin{equation}
    E' = E-E_{\mathrm{0}} \, .
\end{equation}
Hence one needs to define the band edge $E_{\mathrm{0}}$, the valley degeneracy factor $Z$ 
and the effective DOS mass $m_{\mathrm{d}}$ as described in Sec. \ref{block:band} for every valley.
In Hdev there are two possible choices for the distribution function. 
One may either use a Boltzmann distribution function
\begin{equation}
    f_{\mathrm{boltz},n} = \exp\left(  \frac{E-E_{\mathrm{Fn}}}{k_{\mathrm{B}}T_{\mathrm{n}}} \right)
\end{equation}
or a Fermi-Dirac distribution function
\begin{equation}
    f_{\mathrm{F},n} = \left[ 1 + \exp\left(  -\frac{E-E_{\mathrm{Fn}}}{k_{\mathrm{B}}T_{\mathrm{n}}} \right) \right]^{-1} \, .
\end{equation}
The distribution function is simultaneously specified for all valleys of a semiconductor in the SEMI block, 
as described in Sec. \ref{block:semi}.

\subsection{Band Alignment between different Semiconductors}
The band alignment between different semiconductors depends on the so-called offset-energy $E_{\mathrm{off}}$, 
which is defined in the SEMI block \cite{Palanovski2000}.
The offset energy specifies the valence band energy difference between a given semiconductor and a reference material. 
The band diagram of an exemplary heterostructure is sketched in Fig. \ref{fig:banddef}. 
For an alloy the offset energy is calculated as
\begin{equation}
    E_{\mathrm{off}} = \left( 1 - x \right) E_{\mathrm{off,A}} + x E_{\mathrm{off,B}} - x \left( 1-x \right) C_{\mathrm{eoff}}\, ,
\end{equation}
where $C_{\mathrm{eoff}}$ is an alloy specific bowing parameter.

\FloatBarrier
\begin{figure}[!htb]
    \centering
	\includegraphics[width=0.8\textwidth]{drawio/banddef.pdf}
    \caption{
    Illustration of the bandstructure definition of heterostructures in Hdev. 
    Note that in this sketch, the two semiconductors are not in thermal equilibrium with each other.
    }
    \label{fig:banddef}
\end{figure}
\FloatBarrier

\section{Device Structure}
The device structure in Hdev requires the definition of 
\begin{itemize}
    \item a grid, see block \ref{block:region_info} and the grid definition blocks.
    \item definition of semiconductor material blocks, see block \ref{block:region_def}.
    \item the spatially dependent doping density, see block \ref{block:doping}.
    \item the spatially dependent grading, see block \ref{block:grading}.
    \item oxide regions, see block \ref{block:oxide}.
    \item contact regions, see block \ref{block:cont}.
\end{itemize}

\section{Boundary Conditions}
One needs to define boundary conditions between 
\begin{itemize}
    \item contact and semiconductor regions \ref{block:contact} and \ref{block:supply}.
    \item abrupt boundaries between different semiconductor regions \ref{block:thermionic_emission}.
\end{itemize}

\section{Operating Point and Ambient Temperature Definition}
In Hdev the definition of an operating point is 
the bias for every contact and general sweep information, see blocks \ref{block:bias_def} and \ref{block:bias_info}. 
This includes the "T" contact, whose voltage equals the assumed ambient temperature. 
Additionally, for AC simulations one needs to define the parameters in block \ref{block:ac_info}.

TODO: Transient
