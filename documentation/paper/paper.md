---
title: 'Hdev: A 1D and 2D Hydrodynamic/Drift-Diffusion Solver for SiGe and III-V HBTs'
tags:
  - Python
  - Fortran
  - semiconductor
  - HBT
  - SiGe
  - III-V
  - TCAD
  - drift-diffusion
  - Poisson
authors:
  - name: Markus Müller 
    orcid: 0000-0003-1058-1649
    affiliation: "1, 2" # (Multiple affiliations must be quoted)
  - name: Sven Mothes 
    affiliation: 3
  - name: Martin Claus
    affiliation: 4
  - name: Michael Schröter
    affiliation: 1
affiliations:
 - name: CEDIC, TU Dresden, 01062 Dresden, Germany
   index: 1
 - name: SemiMod UG (h.b.), 01159 Dresden, Germany
   index: 2
 - name: GlobalFoundries, 01109 Dresden, Germany
   index: 3
 - name: Infineon Technologies AG, 85579 Neubiberg, Germany
   index: 4
date: 29 December 2021
bibliography: paper.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
# aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it.
# aas-journal: Astrophysical Journal <- The name of the AAS journal.
---

# Summary

Numerical device simulation plays a central role for understanding semiconductor devices and developing new technologies. 
Several open-source simulators have been published, but none of them allows the simulation of 
SiGe and III-V heterojunction bipolar transistors (HBTs). 
Here, the open-source TCAD simulator `Hdev` is presented that enables 2D and true 1D HBT simulation. 
The software is implemented in Fortran and offers an easy-to-use Python-based interface called `HdevPy`. 

# Statement of need

Technology computer-aided design (TCAD) has played a central role for a deeper understanding of the physics of 
semiconductor devices and their performance optimization as well as the development of new process technology 
and associated compact models for circuit design 
[@Selberherr1984; @Vasileska2017; @Schroter2010;@T.Rosenbaum2011; @Korn2018;@Phillips2021].
Various commercial TCAD tools exist that are used mostly in industry and partially also in academia, but they have significant disadvantages. 

Foremost, research results based on these tools are difficult to *reproduce* because (i) the software is only available for significant license cost and (ii) the simulation input files are typically not available (and not required for most journals to be provided by the authors). 
This situation is unsatisfactory as it is incompatible with the principles of the evaluation of scientific methods, 
in particular open review and reproducibility of results [@John2017].

Furthermore, since the source code is unavailable for commercial tools, extending them towards specific research needs is generally
impossible or at least very difficult for users. 
This also stifles innovation and the rapid adaptation of TCAD tools to research needs, e.g., from emerging technologies. 
Some commercial tools have tackled this issue, e.g. by providing interfaces for user-defined model equations [@sdevice], 
or a modular and flexible interface for "constructing" the solver by the user [@comsol_multiphysics]. 
Yet, these approaches can inherently 
never achieve the same degree of freedom for the user as if he had the source code readily available.

These problems have also been identified by others [@Dutta2021]. 
Several open-source TCAD simulators have been published [@Dutta2021;@LLC.2021], but none of these tools 
implement the necessary physics and features for modeling HBTs. `Hdev` aims to fill this gap.

# Implementation details

`Hdev` has been implemented in Fortran for TCAD simulation of HBTs. 
A Python package `HdevPy` is provided for executing the simulator from within Python. 
A detailed manual is also supplied that explains all physical models and the application programming interface (API) of `Hdev`. 

`Hdev` can simultaneously (and successively) solve

* Poisson's equation, 
* continuity equations for holes and electrons,
* an energy continuity equation of electrons,
* a second continuity equation for electrons in an energetically higher band (e.g. for III-V semiconductors)

on a rectangular grid, based on the box-integration method. A special boundary condition for the majority quasi-Fermi potential enables the simulation of true 1D HBT structures without artifacts stemming from an artificial 2D structure. 

The drift-diffusion (DD) transport equation is used for holes. For electrons, the DD transport equation and, if required, also an energy-transport (ET) equation can be used.
The resulting coupled non-linear equation system is discretized and solved using sparse 
compressed-row- and -column-storage type matrices, making the simulation extremely fast. 
For solving the equation system the popular and numerically efficient UMFPACK [@Davis2004] direct sparse 
solver is employed. 
All derivatives are calculated using dual numbers [@Yu2013], making the source code easy to read, less
error-prone to program and, in particular, easily extendable towards new physical models (no need to provide analytical formulations for the often complicated derivatives). 

One of the main features of `Hdev` is that the necessary mathematics to simulate degenerately doped 
semiconductor heterostructures are implemented. Details regarding all material models can be found in the manual. 
This includes advanced transport models, e.g. [@Yang2004;@Michaillat2010;@Klaassen1992;@Shi2003;@Jain1990], 
heterostructure transport equations [@Palanovski2000;@Marshak1987;@Kantner2020] based on state-of-the-art 
numerical discretization methods, in particular for the ET equation [@Gnudi1988;@Kantner2019]. 

Material parameters for many important semiconductors and their alloys are provided as described in the file 
`material_database.py` in the `HdevPy` sources. The materials include:

* "classical" semiconductors: Si, Ge
* III-V semiconductors: InP, GaAs, InAs, GaSb
* technologically important alloys: SiGe, InGaAs

The simulator is capable of calculating the carrier densities using either Boltzmann- or Fermi-Dirac-statistics, 
an important feature for the simulation of degenerately doped semiconductors or simulations at low temperatures. 

The simulation procedure is implemented in well-documented, modern, object-oriented, Fortran code. 
So far, static (DC) and small-signal frequency-dependent (AC) analysis have been available, while transient analysis is presently being implemented.
An overview of the mathematics behind `Hdev` is given in the manual. 
Although a command-line interface is provided by the simulator itself, a Python package named `HdevPy` is provided for easily creating, running and visualizing simulations. Several examples are available in the project repository. 
Furthermore, several test cases are available. 

# Usage

The use of `Hdev` is briefly discussed below and typical simulation results are presented. 
`Hdev` itself is invoked on the command line as follows:

``` bash
hdev input_file.din
```

Here `input_file.din` is an ASCII file that defines the structure to be simulated, all necessary physical parameters, 
the electrical operating conditions, and the parameters for controlling the solution of the equation system. 

The simulator is usually called from Python using `HdevPy`, which takes care of creating the complete simulation input file. 
Examples and templates can be found in the "examples" folder of the project. 

For instance, running the file  `examples/1D_hbt_hydrodynamic_simulation.py` 

* creates the input file of the 1D node three HBT from the roadmap [@Schroter2017a], 
* calls the `Hdev` simulator,
* reads back the results 
* and plots them.

Typical simulation results are visualized in the figures below. Fig. 1 shows the simulated doping and material (here SiGe) composition profile and the band diagram at one operating point as an example for internal variables. 
Fig. 2 shows important terminal characteristics obtained from AC analysis. 

![](figures/profile__standalone.pdf){width=45%}
![](figures/band_diagram_standalone.pdf){width=50%}
\begin{figure}[!h]
\label{fig:results_2}
\caption{(left) Vertical profile of the node three HBT and (right) band diagram at of the node three HBT from 1D energy-transport simulations at $V_{\mathrm{BE}}=0\,\mathrm{V}$, $V_{\mathrm{BC}}=-0.7\,\mathrm{V}$ and a substrate temperature of $T_{\mathrm{Sub}}=300\,\mathrm{K}$.}
\end{figure}

![](figures/I_C(V_BE)_standalone.pdf){width=45%}
![](figures/F_T(J_C)_standalone.pdf){width=45%}
\begin{figure}[!h]
\label{fig:results_1}
\caption{(left) Transfer characteristics and (right) transit frequency of the node three HBT from 1D energy-transport simulations at $V_{\mathrm{BC}}=-0.7\,\mathrm{V}$ and a substrate temperature of 
$T_{\mathrm{Sub}}=300\,\mathrm{K}$.}
\end{figure}


# Research conducted with Hdev 

`Hdev` has been used in several research projects, for example:

- [@Muller2021]: The simulation of III-V semiconductors with more than one conduction valley within the DD 
framework has been discussed with special emphasis on HBTs.
- [@Muller2021a]: An augmented DD transport formulation is described, allowing to take non-local 
effects into account. 
- [@Phillips2021]: The simulator was used to analyze vertical doping profiles of a foundry SiGe HBT technology and to provide recommendations for process technology improvement and development.



<!-- # Figures

Figures can be included like this:
![Caption for example figure.\label{fig:example}](figure.png)
and referenced from text using \autoref{fig:example}.

Figure sizes can be customized by adding an optional second parameter:
![Caption for example figure.](figure.png){ width=20% } --> 

# References



