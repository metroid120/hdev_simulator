# COOS
# Copyright (C) 2019  Markus Müller and Mario Krattenmacher and the COOS contributors <https://gitlab.hrz.tu-chemnitz.de/CEDIC_Bipolar/COOS/>
#
# This file is part of COOS.
#
# COOS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COOS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
name  =  "HdevInterface"
 
from .dummy import dummy
#rename so to COOSpy and import that in _COOSpy.py
from .classes import hdev_py
from .classes import dd_types
