! Module hdev_py defined in file ../../source/hdev_py.f90

subroutine f90wrap_set(ret_set, r)
    use hdev_py, only: set
    implicit none
    
    integer, intent(out) :: ret_set
    integer :: r
    ret_set = set(r=r)
end subroutine f90wrap_set

subroutine f90wrap_get(ret_get)
    use hdev_py, only: get
    implicit none
    
    integer, intent(out) :: ret_get
    ret_get = get()
end subroutine f90wrap_get

subroutine f90wrap_test2(ret_r, i)
    use hdev_py, only: test2
    implicit none
    
    integer, intent(out) :: ret_r
    integer, intent(in) :: i
    ret_r = test2(i=i)
end subroutine f90wrap_test2

subroutine f90wrap_init(path)
    use hdev_py, only: init
    implicit none
    
    character(1024), intent(in) :: path
    call init(path=path)
end subroutine f90wrap_init

subroutine f90wrap_get_mobility_py(semi_name, valley, f, ec_l, ec_r, dim, t, dens, don, acc, ret_mu, grad)
    use hdev_py, only: get_mobility_py
    implicit none
    
    character(10), intent(inout) :: semi_name
    character(1), intent(inout) :: valley
    real(8), intent(in) :: f
    real(8), intent(in) :: ec_l
    real(8), intent(in) :: ec_r
    integer, intent(in) :: dim
    real(8), intent(in) :: t
    real(8), intent(in) :: dens
    real(8), intent(in) :: don
    real(8), intent(in) :: acc
    real(8), intent(out) :: ret_mu
    real(8), intent(in) :: grad
    ret_mu = get_mobility_py(semi_name=semi_name, valley=valley, f=f, ec_l=ec_l, ec_r=ec_r, dim=dim, t=t, dens=dens, &
        don=don, acc=acc, grad=grad)
end subroutine f90wrap_get_mobility_py

subroutine f90wrap_get_mobility_paras_py(semi_name, valley, mob)
    use hdev_py, only: get_mobility_paras_py
    use dd_types, only: mob_model
    implicit none
    
    type mob_model_ptr_type
        type(mob_model), pointer :: p => NULL()
    end type mob_model_ptr_type
    character(10), intent(inout) :: semi_name
    character(1), intent(inout) :: valley
    type(mob_model_ptr_type) :: mob_ptr
    integer, intent(out), dimension(2) :: mob
    allocate(mob_ptr%p)
    call get_mobility_paras_py(semi_name=semi_name, valley=valley, mob=mob_ptr%p)
    mob = transfer(mob_ptr, mob)
end subroutine f90wrap_get_mobility_paras_py

subroutine f90wrap_get_recombination_paras_py(semi_name, type_bn, reco)
    use hdev_py, only: get_recombination_paras_py
    use dd_types, only: rec_model
    implicit none
    
    type rec_model_ptr_type
        type(rec_model), pointer :: p => NULL()
    end type rec_model_ptr_type
    character(10), intent(inout) :: semi_name
    character(10), intent(inout) :: type_bn
    type(rec_model_ptr_type) :: reco_ptr
    integer, intent(out), dimension(2) :: reco
    allocate(reco_ptr%p)
    call get_recombination_paras_py(semi_name=semi_name, type=type_bn, reco=reco_ptr%p)
    reco = transfer(reco_ptr, reco)
end subroutine f90wrap_get_recombination_paras_py

subroutine f90wrap_set_mobility_paras_py(semi_name, valley, mob_new)
    use hdev_py, only: set_mobility_paras_py
    use dd_types, only: mob_model
    implicit none
    
    type mob_model_ptr_type
        type(mob_model), pointer :: p => NULL()
    end type mob_model_ptr_type
    character(10), intent(inout) :: semi_name
    character(1), intent(inout) :: valley
    type(mob_model_ptr_type) :: mob_new_ptr
    integer, intent(in), dimension(2) :: mob_new
    mob_new_ptr = transfer(mob_new, mob_new_ptr)
    call set_mobility_paras_py(semi_name=semi_name, valley=valley, mob_new=mob_new_ptr%p)
end subroutine f90wrap_set_mobility_paras_py

subroutine f90wrap_set_recombination_paras_py(semi_name, rec_new)
    use hdev_py, only: set_recombination_paras_py
    use dd_types, only: rec_model
    implicit none
    
    type rec_model_ptr_type
        type(rec_model), pointer :: p => NULL()
    end type rec_model_ptr_type
    character(10), intent(inout) :: semi_name
    type(rec_model_ptr_type) :: rec_new_ptr
    integer, intent(in), dimension(2) :: rec_new
    rec_new_ptr = transfer(rec_new, rec_new_ptr)
    call set_recombination_paras_py(semi_name=semi_name, rec_new=rec_new_ptr%p)
end subroutine f90wrap_set_recombination_paras_py

subroutine f90wrap_get_intervalley_rate_py(semi_name, valley_1, valley_2, f, dop, temp, grad, rate)
    use hdev_py, only: get_intervalley_rate_py
    implicit none
    
    character(10), intent(inout) :: semi_name
    character(1), intent(inout) :: valley_1
    character(1), intent(inout) :: valley_2
    real(8), intent(in) :: f
    real(8), intent(in) :: dop
    real(8), intent(in) :: temp
    real(8), intent(in) :: grad
    real(8), intent(out) :: rate
    call get_intervalley_rate_py(semi_name=semi_name, valley_1=valley_1, valley_2=valley_2, f=f, dop=dop, temp=temp, &
        grad=grad, rate=rate)
end subroutine f90wrap_get_intervalley_rate_py

subroutine f90wrap_get_pop_py(semi_name, t, ret_pop, dop)
    use hdev_py, only: get_pop_py
    implicit none
    
    character(10), intent(inout) :: semi_name
    real(8), intent(in) :: t
    real(8), intent(out) :: ret_pop
    real(8), intent(in) :: dop
    ret_pop = get_pop_py(semi_name=semi_name, t=t, dop=dop)
end subroutine f90wrap_get_pop_py

subroutine f90wrap_hdev_py__get__var_test(f90wrap_var_test)
    use hdev_py, only: hdev_py_var_test => var_test
    implicit none
    integer, intent(out) :: f90wrap_var_test
    
    f90wrap_var_test = hdev_py_var_test
end subroutine f90wrap_hdev_py__get__var_test

subroutine f90wrap_hdev_py__set__var_test(f90wrap_var_test)
    use hdev_py, only: hdev_py_var_test => var_test
    implicit none
    integer, intent(in) :: f90wrap_var_test
    
    hdev_py_var_test = f90wrap_var_test
end subroutine f90wrap_hdev_py__set__var_test

! End of module hdev_py defined in file ../../source/hdev_py.f90

