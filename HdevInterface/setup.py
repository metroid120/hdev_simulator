import setuptools

#with open("README.md", "r") as fh:
    #long_description = fh.read()

setuptools.setup(
    name="HdevInterface",
    version="0.0.1",
    author="M.Mueller",
    author_email="markus.mueller3@tu-dresden.de",
    description="Hdev python interface",
    #long_description=long_description,
    #long_description_content_type="text/markdown",
    #url="https://gitlab.com/dmt-development/dmt",
    packages=setuptools.find_packages(),
    license="GNU GPLv3+",
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
    ],
    install_requires=[
    ],
)
