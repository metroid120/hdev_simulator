FROM archlinux

# user for non-root commands
RUN useradd -ms /bin/bash markus
# download intel-mkl and install. 1) Download with yay, 2) install as non-root with makepg, 3) pacman as root
RUN  pacman-key --refresh-keys 

RUN pacman -Sy --noconfirm sudo

RUN pacman --noconfirm -Syu \
  && sudo pacman --noconfirm -Sy archlinux-keyring 


RUN pacman -S --noconfirm --needed base-devel \
  && pacman -S --noconfirm glibc \
  && pacman -S --noconfirm meson gcc-fortran vim lapack \
  && pacman -S --noconfirm hdf5 suitesparse  
# && cd /opt \
# && pacman -S --noconfirm go
# && git clone https://aur.archlinux.org/yay.git \
# && chown -R markus:markus yay \
# && cd yay \
# && runuser -u markus -- makepkg \
# && ls \
# && pacman -U --noconfirm yay-10.1.2-1-x86_64.pkg.tar.xz \
# && cd /opt \ 
# && yay -G intel-mkl \ 
# && chown -R markus:markus intel-mkl \
# && cd intel-mkl \
# && runuser -u markus -- makepkg \
# && pacman -U --noconfirm intel-mkl-2020.4.304-1-x86_64.pkg.tar.xz \
# && pacman -U --noconfirm intel-mkl-static-2020.4.304-1-x86_64.pkg.tar.xz 

#working directory
RUN mkdir /app
WORKDIR /app
COPY . /app

#compile hdev
RUN meson builddir_docker  \
  && cd builddir_docker  \
  && ninja  \
  && ninja install

#CMD [ "tail -f /dev/null" ]